const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const newCusineFeaturesTwo = new Schema({
  vendor_id: {
    type: String,
    required: true,
    unique: true
  },
  selected_cusines_types: {
    type: Array
    //  required : true
  },
  primary_selected_icons: {
    type: Object
    // required : true
  }
});

module.exports = mongoose.model('CusineFeatureTwo', newCusineFeaturesTwo);
