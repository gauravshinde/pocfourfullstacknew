const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const nominationSchema = new Schema({
    google_place_id: {
        type: String
    },
    // user_id: {
    //     type: String
    //     //required: true
    // },
    restaurant_name: {
        type: String
    },
    address: {
        type: String
    },
    count: {
        type: Number
    },
    restaurant_logo: {
        type: String
    }
});

module.exports = mongoose.model('Nomination', nominationSchema);