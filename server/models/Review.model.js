const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reviewSchema = new Schema({
  vendor_id: {
    type: String
  },
  review: {
    type: String
  },
  rating: {
    type: Number
  }
});
module.exports = mongoose.model("review", reviewSchema);
