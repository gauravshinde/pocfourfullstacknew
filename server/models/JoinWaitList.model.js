const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const joinwaitlistSchema = new Schema({
  user_id: {
    type: String
  },
  name: {
    type: String
  },
  wait_time: {
    type: Number
  },
  vendor_id: {
    type: String
  },
  adults: {
    type: String,
    require: true
  },
  kids: {
    type: String,
    require: true
  },
  handicap: {
    type: String
  },
  highchair: {
    type: String
  },
  seating_preference: {
    type: Array
  },
  restaurant_name: {
    type: String
  },
  special_occasion: {
    type: String
  },
  ETA: {
    type: String
  },
  dinner_status: {
    type: String
  },
  booking_date: {
    type: Date
  }
});

module.exports = mongoose.model("JoinWaitList", joinwaitlistSchema);
