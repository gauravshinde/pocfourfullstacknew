const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const iconListSchema = new Schema({
    icon: {
        type: String
    },
    title:{
        type:String
    },
    category:{
        type:String
    }
})

module.exports = mongoose.model('IconList',iconListSchema);
