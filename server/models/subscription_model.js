const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const subscriptionSchema = new Schema({
  vendor_id: {
    type: String,
    unique: true
  },
  servicePage: {
    type: String
  },
  request_period: {
    type: String
  },
  placement_period: {
    type: String
  },
  foodItem_counter: {
    type: Boolean,
    default: false
  },
  takeaway_counter: {
    type: Boolean,
    default: false
  },
  accept_Limit: {
    type: String
  },
  distance: {
    type: String
  },
  seating_area: {
    type: String
  },
  seating_capacity: {
    type: String
  },
  reservation_capacity: {
    type: String
  },
  table_turn_around: {
    type: String
  },
  around_time: {
    type: String
  },
  total_seating: {
    type: String
  },
  facility: {
    type: Boolean,
    default: false
  },
  highchair_hadicap: {
    type: Boolean,
    default: false
  },
  uploadMenu: {
    type: Array
  },
  highchair: {
    type: Boolean,
    default: false
  },
  handicap: {
    type: Boolean,
    default: false
  },
  curb_side: {
    type: Boolean,
    default: false
  },
  self_serve: {
    type: Boolean,
    default: false
  },
  skip_line: {
    type: Boolean,
    default: false
  },
  take_away: {
    type: Boolean,
    default: false
  },
  walk_in: {
    type: Boolean,
    default: false
  },
  waitlist: {
    type: Boolean,
    default: false
  },
  reservation: {
    type: Boolean,
    default: false
  },
  room_number: {
    type: Boolean,
    default: false
  },
  auto_accept_pax: {
    type: String
  },
  cut_off_time: {
    type: String
  },
  sameDayAllowed: {
    type: Boolean,
    default: false
  },
  minimum_lead_time: {
    type: String
  },
  maximum_days: {
    type: String
  },
  reservation_time_slot: {
    type: String
  }
});

module.exports = mongoose.model("Subscription", subscriptionSchema);
