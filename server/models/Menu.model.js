const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const menuSchema = new Schema({
  vendor_id: {
    type: String
  },
  item_name: {
    type: String
  },
  category: {
    type: String
  },
  item_code: {
    type: String
  },
  item_price: {
    type: String
  },
  processing_time: {
    type: String
  },
  calorific_value: {
    type: String
  },
  serving_size: {
    type: String
  },
  description: {
    type: String
  },
  photo: {
    type: Array
  },
  specific_time_item: {
    type: Boolean,
    default: false
  },
  item_available_for: {
    type: Array
  },
  multiple_opening_time: {
    type: Boolean
  },
  monday: {
    type: Object
  },
  tuesday: {
    type: Object
  },
  wednesday: {
    type: Object
  },
  thursday: {
    type: Object
  },
  friday: {
    type: Object
  },
  saturday: {
    type: Object
  },
  sunday: {
    type: Object
  },
  food_type: {
    type: Array
  },
  allergy_info: {
    type: Array
  },
  available_take_away: {
    type: Object
  },
  available_curb_side: {
    type: Object
  },
  item_auto_accept: {
    type: Boolean
  },
  item_spicy: {
    type: Boolean
  },
  level_of_spiciness: {
    type: String
  },
  spice_level_customizable: {
    type: Boolean
  },
  nutrition_info: {
    type: Object
  },
  select_tag: {
    type: Object
  },
  item_unit: {
    type: String
  }
});

module.exports = mongoose.model("Menu", menuSchema);
