const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reviewratingSchema = new Schema({
  vendor_id: {
    type: String
  },
  user_id: {
    type: String
  },
  first_name: {
    type: String
  },
  last_name: {
    type: String
  },
  restaurant_name: {
    type: String
  },
  review: {
    type: String
  },
  rating: {
    type: Number
  },
  restaurant_logo: {
    type: String
  },
  review_status: {
    type: String
  },
  profile_photo: {
    type: String
  }
});
module.exports = mongoose.model("ReviewRateing", reviewratingSchema);
