const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reservationSchema = new Schema({
  mainTime: {
    type: String
  },
  vendor_id: {
    type: String
    //require: true
  },
  Date: {
    type: String
  },
  reservation: [{
    name: {
      type: String
      //require: true
    },
    phone_number: {
      type: String
      //require: true
    },
    adults: {
      type: String
      //require: true
    },
    kids: {
      type: String
      //require: true
    },
    highchair: {
      type: Number
    },
    handicap: {
      type: Number
    },
   
    time: {
      type: String
    },
    seating_preference: {
      type: Array
    },
    reasons_for_Cancel: {
      type: Array
    },
    Enter_reasons: {
      type: String
    },
    special_occassion: {
      type: String
    },
    dinner_status: {
      type: String
    },
    reservation_status: {
      type: String
    },
    total: {
      type: Number
      //require: true
    },
    
    wait_time: {
      type: String
      //require: true
    },
    status: {
      type: String
      // require: true
    },
    ETA: {
      type: String
    },
    Request_Date: {
      type: Date
    },
    Seating_Date: {
      type: Date
    },
    reject_reasons: {
      type: String
    },
    enter_reasons: {
      type: String
    },
    entry_point: {
      type: String
    },
    user_id: {
      type: String
    },
    restaurant_name: {
      type: String
    },
    booking_date: {
      type: String
    },
    order_type: {
      type: String
    },
    time: {
      type: String
    },
    register_date: {
      type: Date
    },
    token: {
      type: String
    },
    isVendor: {
      type: Boolean
    },
    default_wait_time: {
      type: String
    },
    orderId: {
      type: String
    },
    requestNumber: {
      type: String
    }
  }]
});
module.exports = mongoose.model("Reservation", reservationSchema);
