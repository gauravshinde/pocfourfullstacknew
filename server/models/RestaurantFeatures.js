const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const newRestaurantFeatures = new Schema({
  vendor_id: {
    type: String,
    //required : true,
    unique: true
  },
  selected_restaurant_features_type: {
    type: Array
    // required : true
  },
  primary_restaurant_features_type: {
    type: Object
    // required : true
  },
  selected_restaurant_access_type: {
    type: Array
    // required : true
  },
  primary_restaurant_access_type: {
    type: Object
    // required: true
  },
  selected_parking_type: {
    type: Array
    //required : true
  },
  primary_selecte_parking_type: {
    type: Object
    // required : true
  },
  parking_description: {
    type: String
    //  required : true
  }
});

module.exports = mongoose.model('RestaurantFeatures', newRestaurantFeatures);
