const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const newServiceDetails = new Schema({
  vendor_id: {
    type: String,
    //  required : true,
    unique: true
  },
  selected_faculty_type: {
    type: Array
    // required : true
  },
  selected_services_type: {
    type: Array
    //  required : true
  },
  primary_service_type: {
    type: Object
    //   required : true
  }
});

module.exports = mongoose.model('ServiceDetails', newServiceDetails);
