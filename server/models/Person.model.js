const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const personSchema = Schema({
  restaurant_id: {
    type: String
  },
  fname: {
    type: String
  },
  lname: {
    type: String
  },
  email: {
    type: String
  },
  position: {
    type: String
  },
  phone_numbers: {
    type: Array,
    required: true
  }
});

module.exports = mongoose.model('Persons', personSchema);
