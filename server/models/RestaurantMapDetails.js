const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const restaurantMapDetails = new Schema({
  vendor_id: {
    type: String,
    // required: true,
    unique: true
  },
  is_registered_with_google: {
    type: Boolean
  },
  google_place_id: {
    type: String
  },
  restaurant_name: {
    type: String
  },
  restaurant_description: {
    type: String
  },
  restaurant_area: {
    type: String
  },
  restaurant_city: {
    type: String
  },
  restaurant_mobile_number: {
    type: String
  },
  restaurant_email: {
    type: String
  },
  restaurant_address: {
    type: String
  },
  lat: {
    type: String
  },
  lon: {
    type: String
  },
  wait_time: {
    type: String
  },
  rating_star: {
    type: String
  },
  total_reviews: {
    type: String
  },
  closing_time: {
    type: String
  },
  primary_cuisine_type: {
    type: String
  },
  offer_active: {
    type: Boolean
  }
  // details_completed_page: {
  //     type: Number,
  //     default: 2,
  //     required: true
  // }
});

module.exports = mongoose.model("RestaurantMapDetails", restaurantMapDetails);