const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const newCusineFeaturesOne = new Schema({
  vendor_id: {
    type: String,
    required: true,
    unique: true
  },
  primary_food_category_icons: {
    type: Object
    // required: true
  },
  selected_food_items_type: {
    type: Array
    //required: true
  },
  primary_food_item_type: {
    type: Object
    // required: true
  },
  allergy_information: {
    type: Boolean,
    //required: true,
    default: false
  },
  serve_liquor: {
    type: Boolean,
    required: true,
    default: false
  },
  nutri_info: {
    type: Boolean,
    required: true,
    default: false
  }
});

module.exports = mongoose.model('CusineFeatureOne', newCusineFeaturesOne);
