const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const raiseticketSchema = new Schema({
  vendor_id: {
    type: String
  },
  user_id: {
    type: String
  },
  first_name: {
    type: String
  },
  last_name: {
    type: String
  },
  phone_number: {
    type: String
  },
  adults: {
    type: Number
  },
  kids: {
    type: Number
  },
  highchair: {
    type: Number
  },
  handicap: {
    type: Number
  },
  total: {
    type: Number
  },
  wait_time: {
    type: Number
  },
  special_occassion: {
    type: String
  },
  Seating_Date: {
    type: Date
  },
  Request_Date: {
    type: Date
  },
  seating_id: {
    type: String
  },
  Status: {
    type: String
  },
  ETA: {
    type: String
  },
  dinner_status: {
    type: String
  },
  register_date: {
    type: Date
  },
  issue_date: {
    type: Date
  },
  seating_preference: {
    type: Array
  },
  description: {
    type: String
  },
  issue: {
    type: String
  },
  issue_image: {
    type: Array
  },
  profile_photo: {
    type: Array
  },
  order_type: {
    type: String
  },
  entry_point: {
    type: String
  },
  restaurant_address: {
    type: String
  },
  restaurant_city: {
    type: String
  },
  restaurant_mobile_number: {
    type: String
  },
  restaurant_logo: {
    type: String
  },
  issue_comment: {
    type: String
  },
  reservationDate: {
    type: String
  },
  reservationTime: {
    type: String
  },
  issue_status: {
    type: String
  },
  restaurant_name: {
    type: String
  }
});
module.exports = mongoose.model("RaiseTicket", raiseticketSchema);
