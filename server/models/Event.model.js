const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const eventSchema = new Schema({
  vendor_id: {
    type: String
    // required: true
  },
  name: {
    type: String
    //required: true
  },
  description: {
    type: String
    //required: true
  },
  start_date: {
    type: String
  },
  end_date: {
    type: String
  },
  time: {
    type: String
  },
  venue: {
    type: String
  },
  address: {
    type: String
  },
  terms_condition: {
    type: String
  },
  event_photo: {
    type: Array
  },
  availability: {
    type: Boolean
  },
  view: {
    type: Number
  }
});

module.exports = mongoose.model("Event", eventSchema);
