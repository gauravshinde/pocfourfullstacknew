const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const newRestaurantTime = new Schema({
  vendor_id: {
    type: String,
    unique: true
  },
  all_seating_area_icons: {
    type: Object
  },
  selected_seating_area: {
    type: Array
  },
  primary_seating_area: {
    type: Object
  },
  table_management: {
    type: Boolean
  },
  total_seating_capacity: {
    type: String
  },
  part_of_chain: {
    type: Boolean
  },
  selectedChains: {
    type: Array
  },
  are_you_open_24_x_7: {
    type: Boolean
  },
  multiple_opening_time: {
    type: Boolean
  },
  monday: {
    type: Object
  },
  tuesday: {
    type: Object
  },
  wednesday: {
    type: Object
  },
  thursday: {
    type: Object
  },
  friday: {
    type: Object
  },
  saturday: {
    type: Object
  },
  sunday: {
    type: Object
  }
});

module.exports = mongoose.model("RestaurantTime", newRestaurantTime);
