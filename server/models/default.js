const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const defaultSchema = new Schema({
  default_time: {
    type: Number,
    require: true
  },
  vendor_id: {
    type: String
  }
});
module.exports = mongoose.model("default", defaultSchema);
