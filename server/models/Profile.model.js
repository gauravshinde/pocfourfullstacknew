const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profileSchema = new Schema({
    user_id:{
        type:String
    },
    are_you_over_21: {
        type: Boolean,
        default: false
    },
    when_is_your_birthday_day: {
        type: String
    },
    when_is_your_birthday_month: {
        type: String
    },
    when_is_your_anniversary_day: {
        type: String
    },
    when_is_your_anniversary_month: {
        type: String
    },
    occasion:[{
        special_occasion:{
            type:String
        },
        special_occasion_day: {
            type: String
        },
        special_occasion_month: {
            type: String
        }    
    }],
    
    gender: {
        type: String
    },
    profile_photo: {
        type: Array
    },
 
})


module.exports = mongoose.model('Profile', profileSchema)