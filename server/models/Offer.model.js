const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const offerSchema = new Schema({
  name: {
    type: String,
    require: true
  },
  description: {
    type: String,
    require: true
  },
  start_date: {
    type: String,
    require: true
  },
  end_date: {
    type: String,
    require: true
  },
  coupon_code: {
    type: String,
    require: true,
    unique: true
  },
  max_number_available: {
    type: String,
    require: true
  },
  max_usage: {
    type: String,
    require: true
  },
  day_between_each_use: {
    type: Number,
    require: true
  },
  terms_conditions: {
    type: String,
    require: true
  },
  add_photo: {
    type: Array,
    require: true
  },
  vendor_id: {
    type: String,
    require: true
  },
  status: {
    type: Boolean,
    default: false
  },
  coupon_apply: {
    type: Number
  },
  view: {
    type: Number
  },
  offer_status: {
    type: Boolean
  }
});

module.exports = mongoose.model("offers", offerSchema);
