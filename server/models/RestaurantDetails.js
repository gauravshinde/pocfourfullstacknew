const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const newRestaurantDetails = new Schema({
  vendor_id: {
    type: String,
    unique: true
  },
  selected_restaurant_type: {
    type: Array
  },
  primary_restaurant_type: {
    type: Object
  },
  selected_dress_code_type: {
    type: Array
  },
  primary_dress_code_type: {
    type: Object
  },
  selected_payment_method_type: {
    type: Array
  },
  primary_payment_method_type: {
    type: Object
  },
  cost: {
    type: String
  },
  revenue: {
    type: String
  },
  restaurant_logo: {
    type: String
  },
  restaurant_photo: {
    type: Array
  },
  lat: {
    type: String
  },
  lon: {
    type: String
  }
});

module.exports = mongoose.model("RestaurantDetails", newRestaurantDetails);
