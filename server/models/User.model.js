const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");
const SALT_I = 10;

const userSchema = new Schema({
  role: {
    type: String
  },
  email: {
    type: String,
    required: true
  },
  country_code: {
    type: String,
    required: true
  },
  mobile_number: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  have_submited_details: {
    type: Boolean,
    default: false,
    required: true
  },
  details_completed_page: {
    type: Number,
    default: 1,
    required: true
  },
  has_admin_approved: {
    type: Boolean
  },
  user_verified: {
    type: Boolean,
    default: false,
    required: true
  },
  OTP: {
    type: String
  },
  register_date: {
    type: Date
  },
  restaurant_current_status: {
    type: Boolean
  },
  FCMtoken:{
type: String
  } 
});

userSchema.pre("save", function(next) {
  var user = this;
  if (user.isModified("password")) {
    bcrypt.genSalt(SALT_I, function(err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

userSchema.methods.change_password = async function(candidatePassword) {
  //Creating a Hash
  var salt = bcrypt.genSaltSync(SALT_I);
  var hash = bcrypt.hashSync(candidatePassword, salt);
  return hash;
};

userSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model("User", userSchema);
