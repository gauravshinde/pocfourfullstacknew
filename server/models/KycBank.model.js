const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const newKycBank = new Schema({
  vendor_id: {
    type: String,
    required: true,
    unique: true
  },
  account_name: {
    type: String
    // required: true
  },
  bank_name: {
    type: String
    //required: true
  },
  account_number: {
    type: Number
    //required: true
  },
  branch_name: {
    type: String
    // required: true
  },
  GST_number: {
    type: String
    //required: true
  },
  IFSC_code: {
    type: String
    //required: true
  },
  PAN_number: {
    type: String
    //required: true
  },
  FSSAI_code: {
    type: String
    //required: true
  },
  imgPath: {
    type: Array
  },
  currentAccount: {
    type: Boolean
  }
});

module.exports = mongoose.model("KycBank", newKycBank);
