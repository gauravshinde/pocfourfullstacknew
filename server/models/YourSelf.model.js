const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const YourSelfSchema = new Schema({
  user_id: {
    type: String
  },
  do_you_have_allergies: {
    // your_allergies: {
    type: Array
    //}
  },
  dietary_preferences: {
    //your_dietary_preferences: {
    type: Array
    //}
  },
  day_you_eat_out: {
    type: Array
  },
  selected_cuisine: {
    type: Array
  },
  favourite_places: {
    type: Array
  },
  selected_chain: {
    type: Array
  },
  push_notification: {
    offers_and_Coupons: {
      type: Boolean,
      default: false
    },
    Seating_updates: {
      type: Boolean,
      default: false
    },
    Order_updates: {
      type: Boolean,
      default: false
    },
    Other_communication_updates: {
      type: Boolean,
      default: false
    }
  },
  sms_notification: {
    offers_and_Coupons: {
      type: Boolean,
      default: false
    },
    Seating_updates: {
      type: Boolean,
      default: false
    },
    Order_updates: {
      type: Boolean,
      default: false
    },
    Other_communication_updates: {
      type: Boolean,
      default: false
    }
  },
  email_notification: {
    offers_and_Coupons: {
      type: Boolean,
      default: false
    },
    Seating_updates: {
      type: Boolean,
      default: false
    },
    Order_updates: {
      type: Boolean,
      default: false
    },
    Other_communication_updates: {
      type: Boolean,
      default: false
    }
  }
});

module.exports = mongoose.model("yourselfinfo", YourSelfSchema);
