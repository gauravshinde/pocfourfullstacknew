const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const favouriteSchema = new Schema({
    restaurant_name: {
        type: String
    },
    dish: {
        type: Object
    },
    offer: {
        type: Object
    }
});

module.exports = mongoose.model('Favourite', favouriteSchema);