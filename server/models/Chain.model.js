const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const chainSchema = new Schema({
  vendor_id: {
    type: String
    // required: true
  },
  name_of_chain: {
    type: String
    //required: true
  },
  chain_logo: {
    type: String
    //required: true
  }
});

module.exports = mongoose.model('Chain', chainSchema);
