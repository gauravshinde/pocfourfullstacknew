const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SuggestNewSchema = new Schema({
  vendor_id: {
    type: String
  },
  title: {
    type: String
  },
  category: {
    type: String
  },
  comments: {
    type: String
  }
});

module.exports = mongoose.model("SuggestNew", SuggestNewSchema);
