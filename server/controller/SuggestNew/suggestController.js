const SuggestNew = require("../../models/SuggestNew.model");

exports.create_new_suggest = async (req, res, next) => {
  try {
    const body = req.body;
    const create_newSuggest = new SuggestNew({
      vendor_id: body.vendor_id,
      title: body.title,
      category: body.category,
      comments: body.comments
    });
    let saveNewData = await create_newSuggest.save();
    if (saveNewData) {
      return res.status(200).json({ message: "Suggest New Details Created" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
