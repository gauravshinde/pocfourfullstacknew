const Persons = require("../../models/Person.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

const createPOCVAlidator = require("./Validator/pocCreatValidator");

exports.create_new_poc = async (req, res, next) => {
  try {
    const body = req.body;
    const { errors, isValid } = await createPOCVAlidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      for (var i = 0; i < body.contacts_array.length; i++) {
        const form = body.contacts_array[i];
        let newData = new Persons({
          restaurant_id: body.vendor_id,
          fname: form.fname ? form.fname : "",
          lname: form.lname ? form.lname : "",
          email: form.email ? form.email : "",
          position: form.position ? form.position : "",
          phone_numbers: form.phone_numbers ? form.phone_numbers : ""
        });
        let saveData = await newData.save();
      }
      return res.status(200).json({ message: "POC" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_my_poc = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Persons.find({ restaurant_id: id });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_new_poc = async (req, res, next) => {
  try {
    const body = req.body;
    for (var i = 0; i < body.contacts_array.length; i++) {
      const form = body.contacts_array[i];
      let new_poc = await Persons.findByIdAndUpdate(
        { restaurant_id: body.id },
        {
          $set: {
            fname: form.fname ? form.fname : "",
            lname: form.lname ? form.lname : "",
            email: form.email ? form.email : "",
            position: form.position ? form.position : "",
            phone_numbers: form.phone_numbers ? form.phone_numbers : ""
          }
        }
      );
      if (new_poc) {
        res.status(200).json({
          message: "Person Contact Successfully Updated..!"
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.delete_poc = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    await Persons.findOneAndRemove({ restaurant_id: id, _id: body.p_id });
    res.status(200).json({
      message: "Person of Contact Successfully Deleted..!"
    });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
