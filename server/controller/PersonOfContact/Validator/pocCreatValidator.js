const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const createPOCValidation = async function ( data ) {
    let errors = { };

    data.mobile_number = !isEmpty( data. mobile_number) ? data.mobile_number : '';

    
    return {
        errors,
        isValid : isEmpty( errors )
    }
}

module.exports = createPOCValidation;