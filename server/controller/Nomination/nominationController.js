const Nomination = require("../../models/Nomination.model");
const vendorNomination = require("../../models/Vendor_nomination.model")
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.nomination = async (req, res, next) => {
    try {
        const body = req.body;
        const create_new_nomination = new Nomination({
            google_place_id: body.google_place_id ? body.google_place_id : '',
            restaurant_name: body.restaurant_name ? body.restaurant_name : '',
            address: body.address ? body.address : '',
            restaurant_logo: body.restaurant_logo ? body.restaurant_logo : '',
            count: 1
        });
        let saveNewData = await create_new_nomination.save();
        if (saveNewData) {
            return res.status(200).json({
                message: "You Are Nominate to " + body.restaurant_name
            })
        }
    } catch (err) {
        let errors = {};
        console.log(err);
        errors.SERVER_ERRORS = "Internal Errors Please Try Again";
        return res.status(500).json(errors);
    }
}

exports.vendornomination = async (req, res, next) => {
    try {
        const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
        const body = req.body;
        const create_new_nomination = new vendorNomination({
            user_id: id,
            google_place_id: body.google_place_id ? body.google_place_id : '',
            restaurant_name: body.restaurant_name ? body.restaurant_name : '',
            address: body.address ? body.address : '',
            restaurant_logo: body.restaurant_logo ? body.restaurant_logo : '',
            count: 1
        });
        let saveNewData = await create_new_nomination.save();
        if (saveNewData) {
            next()
        }
    } catch (err) {
        let errors = {};
        console.log(err);
        errors.SERVER_ERRORS = "Internal Errors Please Try Again";
        return res.status(500).json(errors);
    }
}


exports.check_unique_nomination = async (req, res, next) => {
    try {
        const body = req.body;
        const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
        let check_unique_nomination = await vendorNomination.findOne({
            $and: [{
                google_place_id: body.google_place_id
            }, {
                user_id: id
            }]
        });
        if (check_unique_nomination) {
            return res.status(400).json({
                message: "You already Nominate This Restaurant"
            });
        }
        try {
            const body = req.body;
            const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
            let check_unique_resto = await Nomination.findOne({
                    google_place_id: body.google_place_id
            });
            if (check_unique_resto) {
                let data = await Nomination.findOne({
                    google_place_id: body.google_place_id
                });
                if (body.google_place_id = data.google_place_id) {
                    let check_unique_nomination1 = await Nomination.findOneAndUpdate({
                        google_place_id: body.google_place_id
                    }, {
                        $set: {
                            count: data.count + 1
                        }
                    });
                    try {
                        const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
                        const body = req.body;
                        const create_new_nomination = new vendorNomination({
                            user_id: id,
                            google_place_id: body.google_place_id ? body.google_place_id : '',
                            restaurant_name: body.restaurant_name ? body.restaurant_name : '',
                            address: body.address ? body.address : '',
                            restaurant_logo: body.restaurant_logo ? body.restaurant_logo : '',
                            count: 1
                        });
                        let saveNewData = await create_new_nomination.save();
                        if (saveNewData) {
                            return res.status(200).json({
                                message: "You Are Nominate to " + body.restaurant_name
                            })
                        }
                    } catch (err) {
                        let errors = {};
                        console.log(err);
                        errors.SERVER_ERRORS = "Internal Errors Please Try Again";
                        return res.status(500).json(errors);
                    }
                }

            }
        } catch (err) {
            let errors = {};
            console.log(err);
            errors.SERVER_ERRORS = "Internal Errors Please Try Again";
            return res.status(500).json(errors);
        }
        next();
    } catch (err) {
        let errors = {};
        console.log(err);
        errors.SERVER_ERRORS = "Internal Errors Please Try Again";
        return res.status(500).json(errors);
    }
};

exports.get_all_nomination = async (req, res, next) => {
    try {
        let nomination = await Nomination.find();
        return res.status(200).json(nomination)

    } catch (err) {
        let errors = {};
        console.log(err);
        errors.SERVER_ERRORS = "Internal Errors Please Try Again";
        return res.status(500).json(errors);
    }
};