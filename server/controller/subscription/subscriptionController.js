const subscription = require("../../models/subscription_model");
const Users = require("../../models/User.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.SubscriptionDetails = async (req, res, next) => {
  try {
    const body = req.body;
    const subs = new subscription({
      vendor_id: body.vendor_id,
      servicePage: body.servicePage ? body.servicePage : "",
      request_period: body.request_period ? body.request_period : "",
      placement_period: body.placement_period ? body.placement_period : "",
      foodItem_counter: body.foodItem_counter ? body.foodItem_counter : false,
      takeaway_counter: body.takeaway_counter ? body.takeaway_counter : false,
      accept_Limit: body.accept_Limit ? body.accept_Limit : "",
      distance: body.distance ? body.distance : "",
      seating_area: body.seating_area ? body.seating_area : "",
      reservation_capacity: body.reservation_capacity
        ? body.reservation_capacity
        : "",
      around_time: body.around_time ? body.around_time : "",
      total_seating: body.total_seating ? body.total_seating : "",
      facility: body.facility ? body.facility : false,
      highchair_hadicap: body.highchair_hadicap
        ? body.highchair_hadicap
        : false,
      uploadMenu: body.uploadMenu ? body.uploadMenu : [],
      highchair: body.highchair ? body.highchair : false,
      handicap: body.handicap ? body.handicap : false,
      curb_side: body.curb_side ? body.curb_side : false,
      self_serve: body.self_serve ? body.self_serve : false,
      skip_line: body.skip_line ? body.skip_line : false,
      take_away: body.take_away ? body.take_away : false,
      walk_in: body.walk_in ? body.walk_in : false,
      waitlist: body.waitlist ? body.waitlist : false,
      reservation: body.reservation ? body.reservation : false,
      room_number: body.room_number ? body.room_number : false,
      seating_capacity: body.seating_capacity ? body.seating_capacity : "",
      table_turn_around: body.table_turn_around ? body.table_turn_around : "",
      auto_accept_pax: body.auto_accept_pax ? body.auto_accept_pax : "",
      cut_off_time: body.cut_off_time ? body.cut_off_time : "",
      sameDayAllowed: body.sameDayAllowed ? body.sameDayAllowed : false,
      minimum_lead_time: body.minimum_lead_time ? body.minimum_lead_time : "",
      maximum_days: body.maximum_days ? body.maximum_days : "",
      reservation_time_slot: body.reservation_time_slot
        ? body.reservation_time_slot
        : ""
    });
    let s = await subs.save();
    if (s) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_subscription_details = (req, res, next) => {
  const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
  subscription
    .find({ vendor_id: id })
    .exec()
    .then(subscriptionDetails => {
      res.status(200).json(subscriptionDetails);
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};

exports.update_have_submited_details = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let have_submited_details = await Users.findByIdAndUpdate(
      { _id: id },
      {
        $set: {
          have_submited_details: true
        }
      }
    );
    if (have_submited_details) {
      res.status(200).json({
        message: "Subsription Details Successfully Updated..!"
      });
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_subscription = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let Subscription = await subscription.findOneAndUpdate(
      {
        vendor_id: id
      },
      {
        $set: {
          servicePage: body.servicePage ? body.servicePage : "",
          request_period: body.request_period ? body.request_period : "",
          placement_period: body.placement_period ? body.placement_period : "",
          foodItem_counter: body.foodItem_counter
            ? body.foodItem_counter
            : false,
          takeaway_counter: body.takeaway_counter
            ? body.takeaway_counter
            : false,
          accept_Limit: body.accept_Limit ? body.accept_Limit : "",
          distance: body.distance ? body.distance : "",
          seating_area: body.seating_area ? body.seating_area : "",
          reservation_capacity: body.reservation_capacity
            ? body.reservation_capacity
            : "",
          around_time: body.around_time ? body.around_time : "",
          total_seating: body.total_seating ? body.total_seating : "",
          facility: body.facility ? body.facility : false,
          highchair_hadicap: body.highchair_hadicap
            ? body.highchair_hadicap
            : false,
          uploadMenu: body.uploadMenu ? body.uploadMenu : [],
          highchair: body.highchair ? body.highchair : false,
          handicap: body.handicap ? body.handicap : false,
          curb_side: body.curb_side ? body.curb_side : false,
          self_serve: body.self_serve ? body.self_serve : false,
          skip_line: body.skip_line ? body.skip_line : false,
          take_away: body.take_away ? body.take_away : false,
          walk_in: body.walk_in ? body.walk_in : false,
          waitlist: body.waitlist ? body.waitlist : false,
          reservation: body.reservation ? body.reservation : false,
          room_number: body.room_number ? body.room_number : false,
          seating_capacity: body.seating_capacity ? body.seating_capacity : "",
          table_turn_around: body.table_turn_around
            ? body.table_turn_around
            : "",
          auto_accept_pax: body.auto_accept_pax ? body.auto_accept_pax : "",
          cut_off_time: body.cut_off_time ? body.cut_off_time : "",
          sameDayAllowed: body.sameDayAllowed ? body.sameDayAllowed : false,
          minimum_lead_time: body.minimum_lead_time
            ? body.minimum_lead_time
            : "",
          maximum_days: body.maximum_days ? body.maximum_days : "",
          reservation_time_slot: body.reservation_time_slot
            ? body.reservation_time_slot
            : ""
        }
      }
    );
    if (Subscription) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_submited_details = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let have_submited_details = await Users.findByIdAndUpdate(
      { _id: id },
      {
        $set: {
          have_submited_details: true
        }
      }
    );
    if (have_submited_details) {
      res.status(200).json({
        message: "Subsription Details Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
//02-08-2020
