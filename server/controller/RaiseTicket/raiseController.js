const Raise = require("../../models/RaiseTicket.model");
const Dinner = require("../../models/add_dinner");
const RestaurantAddress = require("../../models/RestaurantMapDetails");
const RestaurantLogo = require("../../models/RestaurantDetails");
const profile = require("../../models/Profile.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.add_raise_ticket = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const first_name = jwt.verify(req.headers.authorization.split(" ")[1], keys)
      .first_name;
    const last_name = jwt.verify(req.headers.authorization.split(" ")[1], keys)
      .last_name;
    const mobile_number = jwt.verify(
      req.headers.authorization.split(" ")[1],
      keys
    ).mobile_number;
    const body = req.body;
    let vendor_info_dinner = await Dinner.findOne({
      vendor_id: body.vendor_id,
      user_id: id
    });
    let vendor_info_address = await RestaurantAddress.findOne({
      vendor_id: body.vendor_id
    });
    let user_profile = await profile.findOne({ user_id: id });
    let vendor_logo = await RestaurantLogo.findOne({
      vendor_id: body.vendor_id
    });
    const add_raise = new Raise({
      restaurant_logo: vendor_logo.restaurant_logo,
      issue_comment: body.issue_comment ? body.issue_comment : "",
      reservationDate: vendor_info_dinner.Date ? vendor_info_dinner.Date : "",
      reservationTime: vendor_info_dinner.time,
      user_id: id,
      vendor_id: body.vendor_id ? body.vendor_id : "",
      first_name: first_name,
      last_name: last_name,
      phone_number: mobile_number,
      restaurant_address: vendor_info_address.restaurant_address,
      restaurant_city: vendor_info_address.restaurant_city,
      restaurant_name: vendor_info_address.restaurant_name,
      restaurant_mobile_number: vendor_info_address.restaurant_mobile_number,
      adults: vendor_info_dinner.adults,
      kids: vendor_info_dinner.kids,
      highchair: vendor_info_dinner.highchair,
      handicap: vendor_info_dinner.handicap,
      total: vendor_info_dinner.total,
      wait_time: vendor_info_dinner.wait_time,
      special_occassion: vendor_info_dinner.special_occassion,
      Seating_Date: vendor_info_dinner.Seating_Date,
      Request_Date: vendor_info_dinner.Request_Date,
      seating_id: body.seating_id ? body.seating_id : "",
      Status: body.Status ? body.Status : "",
      ETA: vendor_info_dinner.ETA,
      dinner_status: vendor_info_dinner.dinner_status,
      register_date: vendor_info_dinner.register_date,
      issue_date: Date.now(),
      seating_preference: vendor_info_dinner.seating_preference,
      description: body.description ? body.description : "",
      issue: body.issue ? body.issue : "",
      issue_image: body.issue_image ? body.issue_image : [],
      profile_photo: user_profile.profile_photo,
      order_type: vendor_info_dinner.order_type,
      entry_point: body.entry_point ? body.entry_point : "",
      issue_status: "Unsolved"
    });
    let saveNewData = await add_raise.save();
    if (saveNewData) {
      return res.status(200).json({
        message: "Raise Ticket Successful Added..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_raise_ticket = async (req, res, next) => {
  try {
    let raiseticket = await Raise.find({
      issue_status: "Unsolved"
    });
    return res.status(200).json(raiseticket);
  } catch (err) {
    return res.status(400).json("Error");
  }
};

exports.get_raise_ticket_closed = async (req, res, next) => {
  try {
    let raiseticket = await Raise.find({
      issue_status: "Solved"
    });
    return res.status(200).json(raiseticket);
  } catch (err) {
    return res.status(400).json("Error");
  }
};

exports.update_issue_status = async (req, res, next) => {
  try {
    const body = req.body;
    let issue = await Raise.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          issue_status: body.issue_status
        }
      }
    );
    if (issue) {
      res.status(200).json({
        message: "Issue Successfully Solved..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
