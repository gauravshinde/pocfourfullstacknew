const AmaelioUsers = require("../../models/AmaelioUser.model");
const CompleteProfile = require("../../models/Profile.model");
const AboutYourSelf = require("../../models/YourSelf.model");
const loginvalidator = require("./validator/loginvalidator");
const userSignupValidator = require("./validator/signupvalidator");
const sendOtpValidator = require("./validator/sendotpvalidator");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;
const axios = require("axios");
const sgmail = require("@sendgrid/mail");
sgmail.setApiKey(
  "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"
);

const accountSid = "ACa7e8734f7243eb4e8b322481d1a4376a";
const authToken = "9f375582bf6960e4ec9e6c6ef5e8eeba";
const client = require("twilio")(accountSid, authToken);
const Dinner = require("../../models/add_dinner");

exports.user_signup = async (req, res, next) => {
  try {
    const body = req.body;
    const { errors, isValid } = await userSignupValidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      let newUser = new AmaelioUsers({
        email: body.email,
        first_name: body.first_name,
        last_name: body.last_name,
        country_code: body.country_code,
        mobile_number: body.mobile_number,
        password: body.password,
        register_date: Date.now(),
      });
      let newUserCreated = await newUser.save();
      if (newUserCreated) {
        // const otpNumber = Math.floor(100000 + Math.random() * 900000);
        // const message = `Please use this OTP: ${otpNumber} to access your account.`;
        // const msg1 = {
        //   to: newUserCreated.email,
        //   from: "support@amealio.com",
        //   subject: "OTP",
        //   text: message,
        // };
        // const send1 = sgmail.send(msg1);
        // if (send) {
        //   res.status(200).json({
        //     message: "Email Send Successfully"
        //   });
        // }
        const form = req.body;
        let newProfile = new CompleteProfile({
          user_id: newUserCreated._id,
          are_you_over_21: form.are_you_over_21 ? form.are_you_over_21 : false,
          when_is_your_birthday_day: form.when_is_your_birthday_day
            ? form.when_is_your_birthday_day
            : "",
          when_is_your_birthday_month: form.when_is_your_birthday_month
            ? form.when_is_your_birthday_month
            : "",
          when_is_your_anniversary_day: form.when_is_your_anniversary_day
            ? form.when_is_your_anniversary_day
            : "",
          when_is_your_anniversary_month: form.when_is_your_anniversary_month
            ? form.when_is_your_anniversary_month
            : "",
          occasion: form.occasion ? form.occasion : [],
          gender: form.gender ? form.gender : "",
          profile_photo: form.profile_photo ? form.profile_photo : [],
        });
        let saveProfile = await newProfile.save();
        const body = req.body;
        let newYourself = new AboutYourSelf({
          user_id: newUserCreated._id,
          do_you_have_allergies: body.do_you_have_allergies
            ? body.do_you_have_allergies
            : [],
          dietary_preferences: body.dietary_preferences
            ? body.dietary_preferences
            : "",
          day_you_eat_out: body.day_you_eat_out ? body.day_you_eat_out : "",
          selected_cuisine: body.selected_cuisine ? body.selected_cuisine : "",
          favourite_places: body.favourite_places ? body.favourite_places : "",
          selected_chain: body.selected_chain ? body.selected_chain : "",
          push_notification: body.push_notification
            ? body.push_notification
            : "",
          sms_notification: body.sms_notification ? body.sms_notification : "",
          email_notification: body.email_notification
            ? body.email_notification
            : "",
        });
        let newyourSelf = await newYourself.save();
        next();
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.send_otp = async (req, res, next) => {
  try {
    const body = req.body;
    let user = await AmaelioUsers.findOne({
      mobile_number: body.mobile_number,
    });
    const { errors, isValid } = await sendOtpValidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const otpNumber = Math.floor(100000 + Math.random() * 900000);
      // const message = encodeURIComponent(
      //   `Please use this OTP: ${otpNumber} to access your account.`
      // );
      const msg = {
        to: user.email,
        from: "support@amealio.com",
        subject: "OTP",
        html:
          '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Three</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style="padding: 10px; text-align: center; width: 100%;"> <img style="width: 170.5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/img3.png" /> <p style="font-weight:600; margin:20px auto; font-size:16px; color:#4B4B4B; font-family:"roboto";" > Verify Your OTP </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";" > <b>Your Verification Code: ' +
          otpNumber +
          '</b> <br />(This code will expire in 10 mins)<br /> <br />If you are having any issues with your account, please don’t<br /> hesitate to contact us by replying to this mail.<br /><br /> Thanks! </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
      };
      sgmail.send(msg);
      try {
        let sentOtp = client.messages
          .create({
            body:
              "Please use this OTP :" + otpNumber + " to access your account.",
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: body.mobile_number,
          })
          .then((message) => res.json(message.sid));
        if (sentOtp) {
          //console.log(sentOtp);
          let updateOtpOnDatabase = await AmaelioUsers.findOneAndUpdate(
            {
              mobile_number: body.mobile_number,
            },
            {
              $set: {
                OTP: otpNumber,
              },
            }
          );
          if (updateOtpOnDatabase) {
            return res.status(200).json({
              message: "OTP sent Successfully",
            });
          }
          //next();
        }
      } catch (err) {
        console.log(err);
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.verify_otp = async (req, res, next) => {
  try {
    const body = req.body;
    let user = await AmaelioUsers.findOne({
      mobile_number: body.mobile_number,
      OTP: body.OTP,
    });
    if (user) {
      let change_userStae = await AmaelioUsers.findOneAndUpdate(
        {
          mobile_number: body.mobile_number,
        },
        {
          $set: {
            user_verified: true,
            OTP: "sdasd@@#$#",
          },
        }
      );
      if (change_userStae) {
        const msg = {
          to: user.email,
          from: "support@amealio.com",
          subject: "Welcome aboard!",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template One</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 997px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style="padding: 10px; text-align: center; width: 100%;"> <img style="width: 250.41px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/img2.png" /> <p style="font-weight:600; margin:15px auto; font-size:16px; color:#4B4B4B; font-family:"roboto";" > Welcome Aboard </p> <p style="margin:5px 0px 24px 0px; font-size:8px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the<br /> printing and typesetting industry. Lorem<br /> Ipsum has been the industrys standard<br /> dummy text ever since the 1500s, when an<br /> unknown printer took a galley of type<br /> and scrambled it to make a type specimen book. </p> <p style="font-weight:600; margin:20px auto; font-size:16px; color:#4B4B4B; font-family:"roboto";" > Here’s what you can do with Amealio </p> <table style=" width:80%; margin:10px auto; margin-top:22px; text-align:center;" > <tr> <td width="30%"> <img style="width: 29.04px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/walk-in.png" /> <p style="margin:5px 0px 5px 0px; font-size:12px; color:#707070; font-family:"roboto";" > Walk In </p> <p style="margin:5px 0px 25px 0px; font-size:8px; line-height: 10px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the </p> </td> <td width="30%"> <img style="width: 29.04px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/clock.png" /> <p style="margin:5px 0px 5px 0px; font-size:12px; color:#707070; font-family:"roboto";" > Waitlist </p> <p style="margin:5px 0px 25px 0px; font-size:8px; line-height: 10px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the </p> </td> </tr> <tr> <td width="30%"> <img style="width: 29.04px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/reservation.png" /> <p style="margin:5px 0px 5px 0px; font-size:12px; color:#707070; font-family:"roboto";" > Reservation </p> <p style="margin:5px 0px 25px 0px; font-size:8px; line-height: 10px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the </p> </td> <td width="30%"> <img style="width: 29.04px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/events.png" /> <p style="margin:5px 0px 5px 0px; font-size:12px; color:#707070; font-family:"roboto";" > Events </p> <p style="margin:5px 0px 25px 0px; font-size:8px; line-height: 10px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the </p> </td> </tr> </table> <table style="margin: 0px auto; width: 50%;"> <tr> <td width="100%" style="margin:0 auto;"> <img style="width: 29.04px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/sale.png" /> <p style="margin:5px 0px 5px 0px; font-size:12px; color:#707070; font-family:"roboto";" > Offers </p> <p style="margin:5px 0px 25px 0px; font-size:8px; line-height: 10px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the </p> </td> </tr> </table> <table style=" width:80%; margin:0px auto; text-align:center;"> <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        const send = sgmail.send(msg);
        client.messages
          .create({
            body:
              "Welcome aboard! Your Amealio account has been successfully created. Thanks for signing up. We hope you love the app.",
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: user.mobile_number,
          })
          .then((message) => res.json(message.sid));
        return res.status(200).json({
          message: "User Verfied Successfully!!",
        });
      }
    } else {
      return res.status(400).json({
        message: "Unable to verify OTP",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_mobile_number = async (req, res, next) => {
  try {
    const body = req.body;
    let update_number = await AmaelioUsers.findOneAndUpdate(
      {
        mobile_number: body.mobile_number,
      },
      {
        $set: {
          mobile_number: body.newmobile_number,
        },
      }
    );
    if (update_number) {
      req.body.mobile_number = body.newmobile_number;
      next();
    } else {
      res.status(400).json({
        message: "Mobile Number Not Successfully Updated..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.check_unique_mobile_number = async (req, res, next) => {
  try {
    const body = req.body;
    let check_unique_mobile_number = await AmaelioUsers.findOne({
      mobile_number: body.mobile_number,
    });
    if (check_unique_mobile_number) {
      return res.status(400).json({
        mobile_number: "Mobile Number already exists",
      });
    }
    next();
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.verify_forgot_password = async (req, res, next) => {
  try {
    const body = req.body;
    const user = new AmaelioUsers();
    const hashed = await user.change_password(body.password);
    let verify_user = await AmaelioUsers.findOneAndUpdate(
      {
        mobile_number: body.mobile_number,
      },
      {
        $set: {
          password: hashed,
        },
      }
    );
    if (verify_user) {
      const msg = {
        to: verify_user.email,
        from: "support@amealio.com",
        subject: "Welcome aboard!",
        html:
          '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Two</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style="padding: 10px; text-align: center; width: 100%;"> <img style="width: 151px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/img1.png" /> <p style="font-weight:600; margin:20px auto; font-size:16px; color:#4B4B4B; font-family:"roboto";" > Change Your Password? </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";" > Hi, ' +
          verify_user.first_name +
          " " +
          verify_user.last_name +
          ' !<br /> There was a request to change your password.<br /> If did not make this request, just ignore this email.<br /> </p> <table style=" width:80%; margin:0px auto; margin-top:40px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
      };
      sgmail.send(msg);
      return res.status(200).json({
        message: "User successfully verified",
      });
      //}
    } else {
      return res.status(400).json({
        message: "Unable to verify OTP",
      });
    }
    // })
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.user_login = async (req, res, next) => {
  try {
    const body = req.body;
    const mobile_number = body.mobile_number;
    const password = body.password;
    const { errors, isValid } = await loginvalidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const user = await AmaelioUsers.findOne({
        mobile_number: mobile_number,
      });
      if (user && user.user_verified === true) {
        user.comparePassword(password, (err, isMatch) => {
          if (isMatch) {
            // Payload
            const payload = {
              _id: user._id,
              first_name: user.first_name,
              last_name: user.last_name,
              email: user.email,
              mobile_number: user.mobile_number,
              have_submited_details: user.have_submited_details,
              has_admin_approved: user.has_admin_approved,
            };
            jwt.sign(payload, keys, (err, token) => {
              res.json({
                success: true,
                token: "Bearer " + token,
              });
            });
          } else {
            return res.status(400).json({
              mobile_number: "Invalid Credentials",
            });
          }
        });
      } else {
        return res.status(400).json({
          mobile_number: "Invalid Credentials",
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_all_user = async (req, res, next) => {
  try {
    let UserData = await AmaelioUsers.find();
    if (UserData) {
      return res.status(200).json(UserData);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.user_has_completed_flow = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await AmaelioUsers.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $set: {
          have_submited_details: true,
        },
      }
    );
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_each_user_info = async (req, res, next) => {
  let array = [];
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let UserData = await AmaelioUsers.find({
      _id: id,
    });
    let CompleteProfile1 = await CompleteProfile.find({
      user_id: id,
    });
    for (let i = 0; i < UserData.length; i++) {
      for (let j = 0; j < CompleteProfile.length; j++) {
        if (i == j) {
          array[i] = MergeRecursive(UserData[i], CompleteProfile1[j]);
        }
      }
    }
    if (array) {
      return res.status(200).json(array);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

function MergeRecursive(obj1, obj2) {
  for (var p in obj2) {
    try {
      if (obj2[p].constructor == Object) {
        obj1[p] = MergeRecursive(obj1[p], obj2[p]);
      } else {
        obj1[p] = obj2[p];
      }
    } catch (e) {
      obj1[p] = obj2[p];
    }
  }
  return obj1;
}

/************************************************
 * Social Media Login Code
 * *********************************************/

exports.google_user_signup = async (req, res, next) => {
  try {
    const body = req.body;
    let check_unique_email = await AmaelioUsers.findOne({
      email: body.email,
      mobile_number: body.mobile_number,
    });
    if (check_unique_email) {
      return res.status(200).json({
        message: "Please Enter Mobile Number",
      });
    }
    const user = await AmaelioUsers.findOne({
      email: body.email,
    });
    if (user) {
      if (user && user.user_verified === true) {
        // Payload
        const payload = {
          _id: user._id,
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          mobile_number: user.mobile_number,
          have_submited_details: user.have_submited_details,
          has_admin_approved: user.has_admin_approved,
        };
        jwt.sign(
          payload,
          keys,
          {
            expiresIn: 3600,
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token,
            });
          }
        );
      } else {
        return res.status(400).json({
          mobile_number: "Invalid Credentials",
        });
      }
    } else {
      let newUser = new AmaelioUsers({
        email: body.email ? body.email : "",
        first_name: body.first_name ? body.first_name : "",
        last_name: body.last_name ? body.last_name : "",
        country_code: body.country_code ? body.country_code : "",
        mobile_number: body.mobile_number ? body.mobile_number : "",
        password: body.password ? body.password : "",
        googleid: body.googleid ? body.googleid : "",
        profile_pic_google: body.profile_pic_google
          ? body.profile_pic_google
          : "",
        facebookid: body.facebookid ? body.facebookid : "",
        profile_pic_facebook: body.profile_pic_facebook
          ? body.profile_pic_facebook
          : "",
      });
      let newUserCreated = await newUser.save();
      if (newUserCreated) {
        const msg = {
          to: newUserCreated.email,
          from: "support@amealio.com",
          subject: "Welcome aboard!",
          text:
            "Welcome aboard! We at Amealio are thrilled to have you. You've successfully signed up and created your Amealio account. Now, here's what you can do with Amealio: - Wait Lift: That's right! Lift the waiting time at restaurants. Just waitlist or check-in on Amealio.- Quick Pick: Skip the queue to get your order. Order on Amealio & pick up when you or your order is ready! Use coupon code 'PICKUPFIRST' for 10% discount on your first food order.- Reservation: Dinner dates or business brekkies, reserve a table on the app for now or later.- Refer your friends & win great discounts on your order.Look out for the QR code at restaurants. Scan it to view menu, order, waitlist or reserve on Amealio.At Amealio, we are always finding ways to improve your dining experience, so stay tuned for newer features. Got something to say about Amealio? We'd love to hear from you!  Write to us at support@Amealio.inNeed help to use Amealio? We are right here.Dine Differently",
        };
        const send = sgmail.send(msg);
        if (newUserCreated.mobile_number === "") {
          res.status(201).json({
            message: "Please Enter Mobile Number",
          });
        }
        const form = req.body;
        let newProfile = new CompleteProfile({
          user_id: newUserCreated._id,
          are_you_over_21: form.are_you_over_21 ? form.are_you_over_21 : false,
          when_is_your_birthday_day: form.when_is_your_birthday_day
            ? form.when_is_your_birthday_day
            : "",
          when_is_your_birthday_month: form.when_is_your_birthday_month
            ? form.when_is_your_birthday_month
            : "",
          when_is_your_anniversary_day: form.when_is_your_anniversary_day
            ? form.when_is_your_anniversary_day
            : "",
          when_is_your_anniversary_month: form.when_is_your_anniversary_month
            ? form.when_is_your_anniversary_month
            : "",
          occasion: form.occasion ? form.occasion : [],
          gender: form.gender ? form.gender : "",
          profile_photo: form.profile_photo ? form.profile_photo : [],
        });
        let saveProfile = await newProfile.save();
        const body = req.body;
        let newYourself = new AboutYourSelf({
          user_id: newUserCreated._id,
          do_you_have_allergies: body.do_you_have_allergies
            ? body.do_you_have_allergies
            : [],
          dietary_preferences: body.dietary_preferences
            ? body.dietary_preferences
            : "",
          day_you_eat_out: body.day_you_eat_out ? body.day_you_eat_out : "",
          selected_cuisine: body.selected_cuisine ? body.selected_cuisine : "",
          favourite_places: body.favourite_places ? body.favourite_places : "",
          selected_chain: body.selected_chain ? body.selected_chain : "",
          push_notification: body.push_notification
            ? body.push_notification
            : "",
          sms_notification: body.sms_notification ? body.sms_notification : "",
          email_notification: body.email_notification
            ? body.email_notification
            : "",
        });
        let newyourSelf = await newYourself.save();
        next();
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.check_unique_email = async (req, res, next) => {
  try {
    const body = req.body;
    let check_unique_email = await AmaelioUsers.findOne({
      email: body.email,
      mobile_number: body.mobile_number,
    });
    if (check_unique_email) {
      return res.status(200).json({
        message: "Please Enter Mobile Number",
      });
    } else {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.google_user_mobile_number_update = async (req, res, next) => {
  try {
    const body = req.body;
    let check_unique_mobile = await AmaelioUsers.findOne({
      mobile_number: body.mobile_number,
    });
    if (check_unique_mobile) {
      return res.status(400).json({
        message: "User Already Exites",
      });
    }
    let data = await AmaelioUsers.findOneAndUpdate(
      {
        email: body.email,
      },
      {
        $set: {
          mobile_number: body.mobile_number,
        },
      }
    );
    if (data) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_user_name = async (req, res, next) => {
  try {
    const body = req.body;
    let UserArray = [];
    let data = await AmaelioUsers.findOne({
      mobile_number: body.mobile_number,
    });
    var Arraydata = {
      first_name: data.first_name,
      last_name: data.last_name,
    };
    UserArray.push(Arraydata);
    return res.status(200).json(UserArray);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.delete_user_account = async (req, res, next) => {
  try {
    const body = req.body;
    let User = await AmaelioUsers.findOneAndRemove({
      _id: body._id,
    });
    let profile = await CompleteProfile.findOneAndRemove({
      user_id: body._id,
    });
    let about = await AboutYourSelf.findOneAndRemove({
      user_id: body._id,
    });
    let dinner = await Dinner.findOneAndRemove({
      user_id: body._id,
    });
    return res.status(200).json({ message: "Success" });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_user_order = async (req, res, next) => {
  try {
    const body = req.body;
    let user_order = await Dinner.find({
      user_id: body.user_id,
    });
    if (user_order) {
      return res.status(200).json(user_order);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.user_locked = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await AmaelioUsers.findOneAndUpdate(
      {
        _id: body.user_id,
      },
      {
        $set: {
          user_verified: body.user_verified,
        },
      }
    );
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.user_update = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await AmaelioUsers.findOneAndUpdate(
      {
        _id: body.user_id,
      },
      {
        $set: {
          email: body.email,
          first_name: body.first_name,
          last_name: body.last_name,
          country_code: body.country_code,
          mobile_number: body.mobile_number,
        },
      }
    );
    return res.status(200).json({ message: "Success" });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.user_password_update = async (req, res, next) => {
  try {
    const body = req.body;
    const user = new AmaelioUsers();
    const hashed = await user.change_password(body.password);
    let data = await AmaelioUsers.findOneAndUpdate(
      {
        _id: body.user_id,
      },
      {
        $set: {
          password: hashed,
        },
      }
    );
    return res.status(200).json({ message: "Success" });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

//09-04-2020
