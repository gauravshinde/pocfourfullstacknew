const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const sendOtpValidation = async function ( data ) {
    let errors = { };

    data.mobile_number = !isEmpty( data. mobile_number) ? data.mobile_number : '';

    if(!Validator.isLength( data.mobile_number,{
        min:8,max:16
    })){
        errors.mobile_number ="Enter Valid Mobile Number";
    }
    if( Validator.isEmpty( data.mobile_number ) ){
        errors.mobile_number = 'Mobile Number is Required';
    }
    
    return {
        errors,
        isValid : isEmpty( errors )
    }
}

module.exports = sendOtpValidation;