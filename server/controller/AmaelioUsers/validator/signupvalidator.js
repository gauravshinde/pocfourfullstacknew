const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const signupValidation = async function (data) {
    let errors = {};

    data.email = !isEmpty(data.email) ? data.email : '';
    data.first_name = !isEmpty(data.first_name) ? data.first_name : '';
    data.last_name = !isEmpty(data.last_name) ? data.last_name : '';
    data.country_code = !isEmpty(data.country_code) ? data.country_code : '';
    data.mobile_number = !isEmpty(data.mobile_number) ? data.mobile_number : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.confirm_password = !isEmpty(data.confirm_password) ? data.confirm_password : '';

    if (!Validator.isLength(data.first_name, {
        min: 1, max: 20
    })) {
        errors.first_name = "Invalid First Name";
    }
    if (Validator.isEmpty(data.first_name)) {
        errors.first_name = 'Frist Name is Required';
    }

    if (!Validator.isLength(data.last_name, {
        min: 1, max: 20
    })) {
        errors.last_name = "Invalid Last Name";
    }
    if (Validator.isEmpty(data.last_name)) {
        errors.last_name = 'Last Name is Required';
    }

    if (!Validator.isEmail(data.email)) {
        errors.email = 'Provide a valid email'
    }
    if (Validator.isEmpty(data.email)) {
        errors.email = 'Email is Required';
    }

    if (Validator.isEmpty(data.country_code)) {
        errors.country_code = 'Country Code is Required';
    }

    if (!Validator.isLength(data.mobile_number, {
        min: 8, max: 16
    })) {
        errors.mobile_number = "Invalid Mobile Number";
    }

    if (Validator.isEmpty(data.mobile_number)) {
        errors.mobile_number = 'Mobile Number is Required';
    }
    if (Validator.isEmpty(data.password)) {
        errors.password = 'Password is Required';
    }
    if (!Validator.equals(data.password, data.confirm_password)) {
        errors.confirm_password = 'Password should match'
    }
    if (Validator.isEmpty(data.confirm_password)) {
        errors.confirm_password = 'Confirm Password is Required';
    }
    return {
        errors,
        isValid: isEmpty(errors)
    }
}

module.exports = signupValidation;