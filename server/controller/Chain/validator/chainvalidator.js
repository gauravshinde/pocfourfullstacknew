const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const chainValidation = async function ( data ) {
    let errors = { };

    data.name_of_chain = !isEmpty( data. name_of_chain) ? data.name_of_chain : '';


    if(!Validator.isLength( data.name_of_chain,{
        min:2,max:30
    })){
        errors.name_of_chain = "Invalid Chain Name";
    }

    return {
        errors,
        isValid : isEmpty( errors )
    }
}

module.exports = chainValidation;