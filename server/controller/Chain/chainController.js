const Chain = require('../../models/Chain.model');
const chainValidation = require('./validator/chainvalidator');
const jwt = require('jsonwebtoken');
const keys = require('../../../config/keys').secretIOkey;


exports.add_chain = async (req, res, next) => {
    try {
        const body = req.body
        const { errors, isValid } = await chainValidation(body);
        if (!isValid) {
            return res.status(400).json(errors);
        } else {
            const create_new_chain = new Chain({
                vendor_id: body.vendor_id,
                name_of_chain: body.name_of_chain,
                chain_logo: body.chain_logo
            });
            let chaindata = await create_new_chain.save()
            if (chaindata) {
                res.status(200).json({
                    message: "Chain Details Successfully Updated..!"
                })
            }
        }
    } catch (err) {
        let errors = {};
        console.log(err);
        errors.SERVER_ERRORS = 'Internal Errors Please Try Again';
        return res.status(500).json(errors);
    }
}


exports.get_chain = async (req, res, next) => {
    try {
       // const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
        let data = await Chain.find({ category:'chains'});
        return res.status(200).json(data);
    } catch (err) {
        let errors = {};
        console.log(err);
        errors.SERVER_ERRORS = 'Internal Errors Please Try Again';
        return res.status(500).json(errors);
    }
}