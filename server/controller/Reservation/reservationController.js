const Reservation = require("../../models/Reservation");
const Dinner = require("../../models/add_dinner");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

const accountSid = "ACa7e8734f7243eb4e8b322481d1a4376a";
const authToken = "9f375582bf6960e4ec9e6c6ef5e8eeba";
const client = require("twilio")(accountSid, authToken);
const SendPush = require("./../../SendPush");

exports.reservation = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const create_new_reservation = new Reservation({
      vendor_user_id: body.vendor_id,
      user_id: id,
      order_type: "reservation",
      entry_point: body.entry_point,
      name: body.name,
      phone_number: body.phone_number,
      adults: body.adults,
      kids: body.kids,
      handicap: body.handicap,
      highchair: body.highchair,
      Date: body.Date,
      time: body.time,
      seating_preference: body.seating_preference,
      special_occassion: body.special_occassion,
      Status: "Pending",
      dinner_status: "Pending",
      restaurant_name: body.restaurant_name ? body.restaurant_name : "",
      register_date: Date.now(),
    });
    let saveNewData = await create_new_reservation.save();
    if (saveNewData) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_reservation = async (req, res, next) => {
  console.log('get_rcreservationdata')
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Reservation.find({
      vendor_id: id,
      Date: body.Date,
    });
    console.log('reservedata', data)
    // let data1 = await Reservation.find({
    //   vendor_id: id,
    //   Date: body.Date,
    // });
    // console.log('reservedata1', data1)
    // var array1 = data.concat(data1);
    console.log('reservedataaarray1', data)
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.cancel_reservation = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Reservation.findByIdAndUpdate(
      {
        _id: body.id,
      },
      {
        $set: {
          Status: "Cancel",
        },
      }
    );
    if (data) {
      res.status(200).json({
        message: "Order Successfully Cancel..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_reservation_pending = async (req, res, next) => {
  console.log('get_rcpreservationdata')
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data1 = await Reservation.find({
      vendor_id: id,
      status: "Pending",
    });
    return res.status(200).json(data1);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_status_reservation = async (req, res, next) => {
  try {
    const body = req.body;
    console.log("bodystatus", body.status);
    if (body.status === "Accepted") {
      let data = await Dinner.findByIdAndUpdate(
        {
          _id: body.id,
        },
        {
          $set: {
            status: body.status,
            ETA: body.ETA ? body.ETA : "",
            reject_reasons: body.reject_reasons ? body.reject_reasons : "",
            enter_reasons: body.enter_reasons ? body.enter_reasons : "",
            Seating_Date: Date.now(),
            dinner_status: "Nonseated",
          },
        }
      );
      if (data) {
        console.log("accepteddata", data);
        // var messagepush = {
        //   data: {
        //     Route: "seating",
        //     id: data._id.toString(),
        //   },
        //   notification: {
        //     title: "Amealio App",
        //     body: "Accepted",
        //   },
        //   token: data.token,
        // };

        // SendPush.sendPush(messagepush);

        res.status(200).json({
          message: "Status Update SuccessFully..!",
        });
        //next();
      }
    } else if (body.status === "Cancelled") {
      let data = await Dinner.findByIdAndUpdate(
        {
          _id: body.id,
        },
        {
          $set: {
            status: body.status,
            ETA: body.ETA ? body.ETA : "",
            reject_reasons: body.reject_reasons ? body.reject_reasons : "",
            enter_reasons: body.enter_reasons ? body.enter_reasons : "",
            Seating_Date: Date.now(),
            dinner_status: "Cancelled",
          },
        }
      );
      if (data) {
        res.status(200).json({
          message: "Status Update SuccessFully..!",
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.send_sms_twilio = (req, res, next) => {
  const body = req.body;
  client.messages
    .create({
      body:
        "Yes! We've received your reservation request. Your request is pending confirmation by the restaurant.",
      messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
      form: "+14152003837",
      to: body.phone_number,
    })
    .then((message) => res.json(message.sid));
};

exports.add_dinner = async (req, res, next) => {
  console.log('new res')
  try {
    let date = new Date().getHours() + 1;
    let dinner = new Reservation({
      mainTime: date,
      reservation: {
        vendor_id: req.body.id,
        user_id: req.body.user_id ? req.body.user_id : "",
        order_type: "reservation",
        requestNumber: Math.floor(Math.random() * 10000000000),
        name: req.body.name ? req.body.name : "",
        phone_number: req.body.phone_number ? req.body.phone_number : "",
        adults: req.body.adults ? req.body.adults : "",
        kids: req.body.kids ? req.body.kids : "",
        highchair: req.body.highchair ? req.body.highchair : "",
        handicap: req.body.handicap ? req.body.handicap : "",
        seating_preference: req.body.seating_preference
          ? req.body.seating_preference
          : "",
        total: parseInt(req.body.adults) + parseInt(req.body.kids),
        wait_time: req.body.wait_time ? req.body.wait_time : "",
        special_occassion: req.body.special_occassion
          ? req.body.special_occassion
          : "",
        Request_Date: Date.now(),
        Seating_Date: Date.now(),
        Date: req.body.Date ? req.body.Date : "",
        time: req.body.time ? req.body.time : "",
        ETA: req.body.ETA ? req.body.ETA : "",
        dinner_status: "Nonseated",
        reservation_status: "Nonseated",
        register_date: Date.now(),
      }
    });
    let newdinner = await dinner.save();
    console.log('resrevation', newDinner)
    if (newdinner) {
      res.status(200).json({
        message: "Dinner Successfully Added",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_user_reservation = async (req, res, next) => {
  console.log('get_ureservationdata')
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      user_id: id,
      $or: [
        { dinner_status: "Pending" },
        { status: "Pending" },
        { status: "Accepted" },
      ],
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
