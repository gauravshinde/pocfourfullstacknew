const RestaurantFeatures = require("../../models/RestaurantFeatures");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.create_new_restaurant_features = async (req, res, next) => {
  try {
    const body = req.body;
    const create_new_details = new RestaurantFeatures({
      vendor_id: body.vendor_id,
      selected_restaurant_features_type: body.selected_restaurant_features_type
        ? body.selected_restaurant_features_type
        : [],
      primary_restaurant_features_type: body.primary_restaurant_features_type
        ? body.primary_restaurant_features_type
        : "",
      selected_restaurant_access_type: body.selected_restaurant_access_type
        ? body.selected_restaurant_access_type
        : [],
      primary_restaurant_access_type: body.primary_restaurant_access_type
        ? body.primary_restaurant_access_type
        : "",
      selected_parking_type: body.selected_parking_type
        ? body.selected_parking_type
        : [],
      primary_selecte_parking_type: body.primary_selecte_parking_type
        ? body.primary_selecte_parking_type
        : "",
      parking_description: body.parking_description
        ? body.parking_description
        : ""
    });
    let saveNewData = await create_new_details.save();
    if (saveNewData) {
      return res.status(200).json({ message: "New Services Details Created" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_restaurant_features = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await RestaurantFeatures.findOne({ vendor_id: id });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_restaurant_features = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let new_restaurant_features = await RestaurantFeatures.findOneAndUpdate(
      { vendor_id: id },
      {
        $set: {
          selected_restaurant_features_type: body.selected_restaurant_features_type
            ? body.selected_restaurant_features_type
            : [],
          primary_restaurant_features_type: body.primary_restaurant_features_type
            ? body.primary_restaurant_features_type
            : "",
          selected_restaurant_access_type: body.selected_restaurant_access_type
            ? body.selected_restaurant_access_type
            : [],
          primary_restaurant_access_type: body.primary_restaurant_access_type
            ? body.primary_restaurant_access_type
            : "",
          selected_parking_type: body.selected_parking_type
            ? body.selected_parking_type
            : [],
          primary_selecte_parking_type: body.primary_selecte_parking_type
            ? body.primary_selecte_parking_type
            : "",
          parking_description: body.parking_description
            ? body.parking_description
            : ""
        }
      }
    );
    if (new_restaurant_features) {
      res.status(200).json({
        message: "Restaurant Feature Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
