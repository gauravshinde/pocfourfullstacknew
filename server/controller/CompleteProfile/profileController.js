const CompleteProfile = require("../../models/Profile.model");
const AboutYourSelf = require("../../models/YourSelf.model");
const AmaelioUsers = require("../../models/AmaelioUser.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;
const axios = require("axios");
const array = [];
exports.create_new_profile = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const form = req.body;
    let newProfile = new CompleteProfile({
      user_id: id,
      are_you_over_21: form.are_you_over_21,
      when_is_your_birthday_day: form.when_is_your_birthday_day,
      when_is_your_birthday_month: form.when_is_your_birthday_month,
      when_is_your_anniversary_day: form.when_is_your_anniversary_day,
      when_is_your_anniversary_month: form.when_is_your_anniversary_month,
      occasion: form.occasion ? form.occasion : [],
      gender: form.gender,
      profile_photo: form.profile_photo ? form.profile_photo : []
    });
    let saveProfile = await newProfile.save();
    if (saveProfile) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.about_yourself = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let newYourself = new AboutYourSelf({
      user_id: id,
      do_you_have_allergies: body.do_you_have_allergies
        ? body.do_you_have_allergies
        : [],
      dietary_preferences: body.dietary_preferences
        ? body.dietary_preferences
        : "",
      day_you_eat_out: body.day_you_eat_out ? body.day_you_eat_out : "",
      selected_cuisine: body.selected_cuisine ? body.selected_cuisine : "",
      favourite_places: body.favourite_places ? body.favourite_places : "",
      selected_chain: body.selected_chain ? body.selected_chain : "",
      push_notification: body.push_notification ? body.push_notification : "",
      sms_notification: body.sms_notification ? body.sms_notification : "",
      email_notification: body.email_notification ? body.email_notification : ""
    });
    let newyourSelf = await newYourself.save();
    if (newyourSelf) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.have_submited_details_profile = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await AmaelioUsers.findOneAndUpdate(
      {
        _id: id
      },
      {
        $set: {
          have_submited_details_profile: true
        }
      }
    );
    if (data)
      return res.status(200).json({
        message: "Profile Successfully Created..!"
      });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.have_submited_details_yourself = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await AmaelioUsers.findOneAndUpdate(
      {
        _id: id
      },
      {
        $set: {
          have_submited_details_yourself: true
        }
      }
    );
    if (data)
      return res.status(200).json({
        message: "Successfully Added About Yourself Details"
      });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_all_user_about_yourself = async (req, res, next) => {
  try {
    let AboutYourSelfData = await AboutYourSelf.find({});
    if (AboutYourSelfData) {
      return res.status(200).json(AboutYourSelfData);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.map = async (req, res, next) => {
  const body = req.body;
  try {
    var { data } = await axios.get(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${body.lat},${body.lon}&radius=1000&type=restaurant&keyword=cruise&key=AIzaSyBKaOKvpvylo_hx-icZWxmjtQB6WJAk1Hs`
    );
    if (data) {
      return res.status(200).json(data);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_each_user_info = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let profile = await CompleteProfile.find({
      user_id: id
    });
    let yourself = await AboutYourSelf.find({
      user_id: id
    });
    for (let i = 0; i < profile.length; i++) {
      for (let j = 0; j < yourself.length; j++) {
        if (i == j) {
          array[i] = MergeRecursive(profile[i], yourself[j]);
        }
      }
    }
    if (array) {
      return res.status(200).json(array);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_user_info = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let profile = await CompleteProfile.find({
      user_id: id
    });
    if (profile) {
      return res.status(200).json(profile);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_each_user_yourself = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let yourself = await AboutYourSelf.find({
      user_id: id
    });
    if (yourself) {
      return res.status(200).json(yourself);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

function MergeRecursive(obj1, obj2) {
  for (var p in obj2) {
    try {
      // Property in destination object set; update its value.
      if (obj2[p].constructor == Object) {
        obj1[p] = MergeRecursive(obj1[p], obj2[p]);
      } else {
        obj1[p] = obj2[p];
      }
    } catch (e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];
    }
  }
  return obj1;
}

exports.update_profile = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const form = req.body;
    let data = await CompleteProfile.findOneAndUpdate(
      {
        user_id: id
      },
      {
        $set: {
          are_you_over_21: form.are_you_over_21,
          when_is_your_birthday_day: form.when_is_your_birthday_day,
          when_is_your_birthday_month: form.when_is_your_birthday_month,
          when_is_your_anniversary_day: form.when_is_your_anniversary_day,
          when_is_your_anniversary_month: form.when_is_your_anniversary_month,
          occasion: form.occasion ? form.occasion : [],
          gender: form.gender,
          profile_photo: form.profile_photo ? form.profile_photo : []
        }
      }
    );
    if (data) {
      next();
    }
    // return res.status(200).json({
    //   message: "Successfully Update Profile Details"
    // });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_aboutyourself = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const body = req.body;
    let data = await AboutYourSelf.findOneAndUpdate(
      {
        user_id: id
      },
      {
        $set: {
          do_you_have_allergies: body.do_you_have_allergies
            ? body.do_you_have_allergies
            : [],
          dietary_preferences: body.dietary_preferences
            ? body.dietary_preferences
            : "",
          day_you_eat_out: body.day_you_eat_out ? body.day_you_eat_out : "",
          selected_cuisine: body.selected_cuisine ? body.selected_cuisine : "",
          favourite_places: body.favourite_places ? body.favourite_places : "",
          selected_chain: body.selected_chain ? body.selected_chain : "",
          push_notification: body.push_notification
            ? body.push_notification
            : "",
          sms_notification: body.sms_notification ? body.sms_notification : "",
          email_notification: body.email_notification
            ? body.email_notification
            : ""
        }
      }
    );
    if (data) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};