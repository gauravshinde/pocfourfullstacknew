const IconList = require("../../models/IconList.model");

exports.get_all_icons = async (req, res, next) => {
  let allIcons = await IconList.find({ category: { $ne: "chain" } });
  return res.status(200).json(allIcons);
};

// exports.get_all_icons = async (req, res, next) => {
//   try {
//     let restaurant_feature = [];
//     let payment_method = [],
//       restaurant_type = [],
//       dress_code = [],
//       premises = [],
//       services = [],
//       food_item = [],
//       food_categories = [],
//       parking_services = [],
//       accessible_restaurants = [],
//       cuisine_type = [],
//       chains = [],
//       service_subscriptions = [],
//       seating_area = [],
//       allergy_information = [],
//       food_type = [];

//     let allIcons = await IconList.find();
//     for (let i = 0; i < allIcons.length; i++) {
//       var Arraydata = {
//         title: allIcons[i].title,
//         category: allIcons[i].category,
//       };
//       if (allIcons[i].category == "Restaurant Features") {
//         restaurant_feature.push(Arraydata);
//       } else if (allIcons[i].category == "Payment Method") {
//         payment_method.push(Arraydata);
//       } else if (allIcons[i].category == "Restaurant Type") {
//         restaurant_type.push(Arraydata);
//       } else if (allIcons[i].category == "Dress Code") {
//         dress_code.push(Arraydata);
//       } else if (allIcons[i].category == "Premises") {
//         premises.push(Arraydata);
//       } else if (allIcons[i].category == "Services") {
//         services.push(Arraydata);
//       } else if (allIcons[i].category == "Food Item") {
//         food_item.push(Arraydata);
//       } else if (allIcons[i].category == "Food Category") {
//         food_categories.push(Arraydata);
//       } else if (allIcons[i].category == "Parking Services") {
//         parking_services.push(Arraydata);
//       } else if (allIcons[i].category == "Accessible restaurant") {
//         accessible_restaurants.push(Arraydata);
//       } else if (allIcons[i].category == "Cuisine Type") {
//         cuisine_type.push(Arraydata);
//       } else if (allIcons[i].category == "chains") {
//         chains.push(Arraydata);
//       } else if (allIcons[i].category == "Service Subscribe") {
//         service_subscriptions.push(Arraydata);
//       } else if (allIcons[i].category == "Seating Area") {
//         seating_area.push(Arraydata);
//       } else if (allIcons[i].category == "Allergy info") {
//         allergy_information.push(Arraydata);
//       } else if (allIcons[i].category == "Food Type") {
//         food_type.push(Arraydata);
//       }
//     }
//     return res
//       .status(200)
//       .json([
//         {
//           Restaurant_Features: restaurant_feature,
//           Payment_method: payment_method,
//           Restaurant_type: restaurant_type,
//           Dress_Code: dress_code,
//           Premises: premises,
//           Services: services,
//           Food_Item: food_item,
//           Food_Categories: food_categories,
//           Parking_Services: parking_services,
//           Accessible_Restaurants: accessible_restaurants,
//           Cuisine_Type: cuisine_type,
//           Chains: chains,
//           Service_Subscriptions: service_subscriptions,
//           Seating_Area: seating_area,
//           Allergy_Information: allergy_information,
//           Food_Type: food_type,
//         },
//       ]);
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.create_new_chain = async (req, res, next) => {
  try {
    const body = req.body;
    const newIcon = new IconList({
      icon: body.icon,
      title: body.title,
      category: "chains",
    });
    let saveIcon = await newIcon.save();
    if (saveIcon) {
      return res
        .status(200)
        .json({ message: "New Chain Added Successfully!!" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_all_chains = async (req, res, next) => {
  try {
    let allChains = await IconList.find({ category: "chain" });
    return res.status(200).json(allChains);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.create_new_icon = async (req, res, next) => {
  try {
    const body = req.body;
    const newIcon = new IconList({
      icon: body.icon,
      title: body.title,
      category: body.category,
    });
    let saveIcon = await newIcon.save();
    if (saveIcon) {
      return res.status(200).json({ message: "New Icon Added Successfully!!" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_chain = async (req, res, next) => {
  try {
    // const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await IconList.find({ category: "chains" });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_food_type = async (req, res, next) => {
  try {
    let data = await IconList.find({ category: "Food Type" });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_allergy_info = async (req, res, next) => {
  try {
    let data = await IconList.find({ category: "Allergy info" });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
