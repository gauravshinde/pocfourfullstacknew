const ReviewRateing = require("../../models/ReviewRating.model");
const Review = require("../../models/Review.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.add_review_rateing = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const first_name = jwt.verify(req.headers.authorization.split(" ")[1], keys)
      .first_name;
    const last_name = jwt.verify(req.headers.authorization.split(" ")[1], keys)
      .last_name;
    const create_new_review = new ReviewRateing({
      vendor_id: body.vendor_id,
      user_id: id,
      profile_photo: body.profile_photo,
      first_name: first_name,
      last_name: last_name,
      restaurant_name: body.restaurant_name,
      review: body.review,
      rating: body.rating,
      restaurant_logo: body.restaurant_logo,
      review_status: "Pending"
    });
    let saveNewData = await create_new_review.save();
    if (saveNewData) {
      return res.status(200).json({
        message: "Thank You For Review And Rateing "
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_review_rateing = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await ReviewRateing.find({
      vendor_id: id,
      review_status: "Pending"
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_approve_review_rateing = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await ReviewRateing.find({
      vendor_id: id,
      review_status: "Approved"
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.approve_review = async (req, res, next) => {
  try {
    const body = req.body;
    let approve_review_rateing = await ReviewRateing.findOneAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          review_status: body.review_status
        }
      }
    );
    if (approve_review_rateing) {
      // return res.status(201).json({
      //   message: "Review Approve Successfully..!"
      // });
      next();
    }
  } catch (err) {
    return res.status(400).json("Error");
  }
};

exports.reject_review = async (req, res, next) => {
  try {
    const body = req.body;
    let reject_review_rateing = await ReviewRateing.findOneAndRemove({
      _id: body.id
    });
    if (reject_review_rateing) {
      return res.status(201).json({
        message: "Review Successfully Reject..!"
      });
    }
  } catch (err) {
    return res.status(400).json("Error");
  }
};

exports.get_approve_review_rateing_user = async (req, res, next) => {
  try {
    const body = req.body;
    var a = 0;
    var avg = 0;
    let data = await ReviewRateing.find({
      vendor_id: body.vendor_id,
      review_status: "Approved"
    });
    for (let i = 0; i < data.length; i++) {
      a = a + data[i].rating;
    }
    avg = a / data.length;
    total_Avg = avg.toFixed(1);
    var review_data = {
      review_data: data,
      total_review: data.length,
      review_avg: total_Avg
    };
    return res.status(200).json([review_data]);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.review = async (req, res, next) => {
  try {
    const body = req.body;
    var review_rating = 0;
    let review = await ReviewRateing.find({ vendor_id: body.vendor_id });
    for (let i = 0; i < review.length; i++) {
      review_rating = review_rating + review[i].rating / review.length;
    }
    let total_review = review.length;
    let review_count = review_rating.toFixed(1);
    let update = await Review.findOneAndUpdate(
      { vendor_id: body.vendor_id },
      {
        $set: {
          review: total_review,
          rating: review_count
        }
      }
    );
    if (update) {
      return res.status(200).json({ message: "sucess" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

//10-12-19
