const Offer = require("../../models/Offer.model");
const RestaurantDetails = require("../../models/RestaurantMapDetails");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.add_New_Offer = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const body = req.body;
    const newOffer = new Offer({
      vendor_id: id,
      name: body.name ? body.name : "",
      description: body.description ? body.description : "",
      start_date: body.start_date ? body.start_date : "",
      end_date: body.end_date ? body.end_date : "",
      coupon_code: body.coupon_code ? body.coupon_code : "",
      max_number_available: body.max_number_available
        ? body.max_number_available
        : "",
      max_usage: body.max_usage ? body.max_usage : "",
      day_between_each_use: body.day_between_each_use
        ? body.day_between_each_use
        : "",
      terms_conditions: body.terms_conditions ? body.terms_conditions : "",
      add_photo: body.add_photo ? body.add_photo : [],
      status: true,
      coupon_apply: 0,
      view: 0,
      offer_status: body.offer_status ? body.offer_status : true
    });
    let saveNewOffer = await newOffer.save();
    if (saveNewOffer) {
      let newOffer = await RestaurantDetails.findOneAndUpdate(
        {
          vendor_id: saveNewOffer.vendor_id
        },
        {
          $set: {
            offer_active: true
          }
        }
      );
      if (newOffer) {
        res.status(200).json({
          message: "Successfully Updated..!"
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.activate_offer_resto = async (req, res, next) => {
  try {
    let newOffer = await RestaurantDetails.findByIdAndUpdate(
      {
        $and: [
          {
            vendor_id: saveNewOffer.vendor_id
          },
          {
            offer_status: true
          }
        ]
      },
      {
        $set: {
          offer_active: true
        }
      }
    );
    if (newOffer) {
      res.status(200).json({
        message: "Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.Offer_list = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Offer.find({
      vendor_id: id
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.deactivate_offer = async (req, res, next) => {
  try {
    const body = req.body;
    let newOffer = await Offer.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          status: body.status
        }
      }
    );
    if (newOffer) {
      res.status(200).json({
        message: "Offer Successfully Deactivated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_offer = async (req, res, next) => {
  try {
    const body = req.body;
    let newOffer = await Offer.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          name: body.name ? body.name : "",
          description: body.description ? body.description : "",
          start_date: body.start_date ? body.start_date : "",
          end_date: body.end_date ? body.end_date : "",
          coupon_code: body.coupon_code ? body.coupon_code : "",
          max_number_available: body.max_number_available
            ? body.max_number_available
            : "",
          max_usage: body.max_usage ? body.max_usage : "",
          day_between_each_use: body.day_between_each_use
            ? body.day_between_each_use
            : "",
          terms_conditions: body.terms_conditions ? body.terms_conditions : "",
          add_photo: body.add_photo ? body.add_photo : [],
          status: true
        }
      }
    );
    if (newOffer) {
      res.status(200).json({
        message: "Offer Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_all_offer_amaelio = async (req, res, next) => {
  try {
    let data = await Offer.find({
      offer_status: true
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.delete_offer = async (req, res, next) => {
  try {
    const body = req.body;
    let newOffer = await Offer.findByIdAndRemove({
      _id: body.id
    });
    if (newOffer) {
      res.status(200).json({
        message: "Offer Successfully Delete..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_offer_view = async (req, res, next) => {
  try {
    const body = req.body;
    let viewOffer = await Offer.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          view: body.view + 1
        }
      }
    );
    if (viewOffer) {
      res.status(200).json({
        message: "Offer View Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_coupon_view = async (req, res, next) => {
  try {
    const body = req.body;
    let coupanOffer = await Offer.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          coupon_apply: body.coupon_apply + 1
        }
      }
    );
    if (coupanOffer) {
      res.status(200).json({
        message: "Offer Coupan Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_offer_availability = async (req, res, next) => {
  try {
    const body = req.body;
    console.log("body" + body);
    let data = await Offer.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          offer_status: body.offer_status
        }
      }
    );
    if (body.offer_status == true) {
      let newOffer = await RestaurantDetails.findOneAndUpdate(
        {
          vendor_id: data.vendor_id
        },
        {
          $set: {
            offer_active: true
          }
        }
      );
      if (newOffer) {
        res.status(200).json({
          message: "Successfully Updated..!"
        });
      }
    } else if (body.offer_status == false) {
      let newOffer = await RestaurantDetails.findOneAndUpdate(
        {
          vendor_id: data.vendor_id
        },
        {
          $set: {
            offer_active: false
          }
        }
      );
      if (newOffer) {
        res.status(200).json({
          message: "Successfully Updated..!"
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_each_vendor_offer_amaelio = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Offer.find({
      vendor_id: body.id,
      offer_status: true
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
