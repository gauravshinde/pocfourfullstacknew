const ServiceDetails = require("../../models/ServiceDetails");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.create_new_service_details = async (req, res, next) => {
  try {
    const body = req.body;
    const create_new_details = new ServiceDetails({
      vendor_id: body.vendor_id,
      selected_faculty_type: body.selected_faculty_type
        ? body.selected_faculty_type
        : [],
      selected_services_type: body.selected_services_type
        ? body.selected_services_type
        : [],
      primary_service_type: body.primary_service_type
        ? body.primary_service_type
        : ""
    });
    let saveNewData = await create_new_details.save();
    if (saveNewData) {
      return res.status(200).json({ message: "New Services Details Created" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_services_details = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await ServiceDetails.findOne({ vendor_id: id });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_service_details = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let new_service_details = await ServiceDetails.findOneAndUpdate(
      { vendor_id: id },
      {
        $set: {
          selected_faculty_type: body.selected_faculty_type
            ? body.selected_faculty_type
            : [],
          selected_services_type: body.selected_services_type
            ? body.selected_services_type
            : [],
          primary_service_type: body.primary_service_type
            ? body.primary_service_type
            : ""
        }
      }
    );
    if (new_service_details) {
      res.status(200).json({
        message: "Service Details Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
