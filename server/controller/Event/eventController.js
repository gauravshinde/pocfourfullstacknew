const Event = require("../../models/Event.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;
//const schedule = require('node-schedule');

exports.add_event = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const body = req.body;
    const add_event = new Event({
      vendor_id: id,
      name: body.name ? body.name : "",
      description: body.description ? body.description : "",
      start_date: body.start_date,
      end_date: body.end_date ? body.end_date : "",
      time: body.time ? body.time : "",
      venue: body.venue ? body.venue : "",
      address: body.address ? body.address : "",
      terms_condition: body.terms_condition ? body.terms_condition : "",
      event_photo: body.event_photo ? body.event_photo : [],
      availability: body.availability ? body.availability : false,
      view: 0
    });
    let saveNewData = await add_event.save();
    if (saveNewData) {
      return res.status(200).json({
        message: "New Event Details Added..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_event_details = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Event.find({
      vendor_id: id
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_event_details = async (req, res, next) => {
  try {
    const body = req.body;
    let new_event_details = await Event.findOneAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          name: body.name ? body.name : "",
          description: body.description ? body.description : "",
          start_date: body.start_date,
          end_date: body.end_date ? body.end_date : "",
          time: body.time ? body.time : "",
          venue: body.venue ? body.venue : "",
          address: body.address ? body.address : "",
          terms_condition: body.terms_condition ? body.terms_condition : "",
          event_photo: body.event_photo ? body.event_photo : [],
          availability: body.availability ? body.availability : false
        }
      }
    );
    if (new_event_details) {
      res.status(200).json({
        message: "Event Details Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.delete_event = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Event.findByIdAndRemove({
      _id: body.id
    });
    return res.status(200).json({
      message: "Event Successfully Delete..!"
    });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_availability = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Event.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          availability: body.availability
        }
      }
    );
    if (data)
      return res.status(200).json({
        message: "Success"
      });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.cron = async (req, res, next) => {
  var date = new Date(2019, 11, 26, 12, 50, 0);
  try {
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_event_view = async (req, res, next) => {
  try {
    const body = req.body;
    let viewOffer = await Event.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          view: body.view + 1
        }
      }
    );
    if (viewOffer) {
      res.status(200).json({
        message: "Event View Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_event_show = async (req, res, next) => {
  try {
    let data = await Event.find({
      availability: true
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
