const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const kycbankValidation = async function(data) {
  let errors = {};
  data.account_name = !isEmpty(data.account_name) ? data.account_name : '';
  data.bank_name = !isEmpty(data.bank_name) ? data.bank_name : '';
  data.account_number = !isEmpty(data.account_number)
    ? data.account_number
    : '';
  data.branch_name = !isEmpty(data.branch_name) ? data.branch_name : '';
  data.GST_number = !isEmpty(data.GST_number) ? data.GST_number : '';
  data.IFSC_code = !isEmpty(data.IFSC_code) ? data.IFSC_code : '';
  data.PAN_number = !isEmpty(data.PAN_number) ? data.PAN_number : '';
  data.FSSAI_code = !isEmpty(data.FSSAI_code) ? data.FSSAI_code : '';
  data.imgPath = !isEmpty(data.imgPath) ? data.imgPath : '';
  data.currentAccount = !isEmpty(data.currentAccount)
    ? data.currentAccount
    : '';

  //Account Name

  if (
    !Validator.isLength(data.account_name, {
      min: 2,
      max: 30
    })
  ) {
    errors.account_name = 'Invalid Account Name ';
  }
  if (Validator.isEmpty(data.account_name)) {
    errors.account_name = 'Account Name is required';
  }

  //Bank Name

  if (
    !Validator.isLength(data.bank_name, {
      min: 2,
      max: 30
    })
  ) {
    errors.bank_name = 'Invalid Bank Name ';
  }
  if (Validator.isEmpty(data.bank_name)) {
    errors.bank_name = 'Bank Name is required';
  }

  //Account Number

  if (
    !Validator.isLength(data.account_number, {
      min: 12,
      max: 16
    })
  ) {
    errors.account_number = 'Invalid Account Number ';
  }
  if (Validator.isEmpty(data.account_number)) {
    errors.account_number = 'Account Number is required';
  }

  //Branch Name
  if (
    !Validator.isLength(data.branch_name, {
      min: 2,
      max: 30
    })
  ) {
    errors.branch_name = 'Invalid Branch Name';
  }
  if (Validator.isEmpty(data.branch_name)) {
    errors.branch_name = 'Branch Name is required';
  }

  //GST Number
  if (
    !Validator.isLength(data.GST_number, {
      min: 15,
      max: 15
    })
  ) {
    errors.GST_number = 'Invalid GST Number';
  }
  if (Validator.isEmpty(data.GST_number)) {
    errors.GST_number = 'GST Number is required';
  }

  //IFSC Code
  if (
    !Validator.isLength(data.IFSC_code, {
      min: 11,
      max: 11
    })
  ) {
    errors.IFSC_code = 'Invalid IFSC Code';
  }
  if (Validator.isEmpty(data.IFSC_code)) {
    errors.IFSC_code = 'IFSC Code is required';
  }

  //PAN Number
  if (
    !Validator.isLength(data.PAN_number, {
      min: 10,
      max: 10
    })
  ) {
    errors.PAN_number = 'Invalid PAN Number';
  }
  if (Validator.isEmpty(data.PAN_number)) {
    errors.PAN_number = 'PAN Number is required';
  }

  //FSSAI Code
  if (
    !Validator.isLength(data.FSSAI_code, {
      min: 14,
      max: 14
    })
  ) {
    errors.FSSAI_code = 'Invalid FSSAI Code';
  }
  if (Validator.isEmpty(data.FSSAI_code)) {
    errors.FSSAI_code = 'FSSAI Code is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};

module.exports = kycbankValidation;
