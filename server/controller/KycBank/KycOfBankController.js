const KycBank = require("../../models/KycBank.model");
const KycBankValidator = require("./validator/kycbankValidator");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.create_new_kycbank = async (req, res, next) => {
  try {
    const body = req.body;
    const { errors, isValid } = await KycBankValidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const create_new_details = new KycBank({
        vendor_id: body.vendor_id,
        account_name: body.account_name ? body.account_name : "",
        bank_name: body.bank_name ? body.bank_name : "",
        account_number: body.account_number ? body.account_number : "",
        branch_name: body.branch_name ? body.branch_name : "",
        GST_number: body.GST_number ? body.GST_number : "",
        IFSC_code: body.IFSC_code ? body.IFSC_code : "",
        PAN_number: body.PAN_number ? body.PAN_number : "",
        FSSAI_code: body.FSSAI_code ? body.FSSAI_code : "",
        imgPath: body.imgPath ? body.imgPath : [],
        currentAccount: body.currentAccount ? body.currentAccount : false
      });
      let saveBankData = await create_new_details.save();
      if (saveBankData) {
        return res
          .status(200)
          .json({ message: "New KYC and Bank Details Created" });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_kycbank_details = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await KycBank.findOne({ vendor_id: id });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_kyc_bank_details = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let kycbank = await KycBank.findOneAndUpdate(
      { vendor_id: id },
      {
        $set: {
          account_name: body.account_name ? body.account_name : "",
          bank_name: body.bank_name ? body.bank_name : "",
          account_number: body.account_number ? body.account_number : "",
          branch_name: body.branch_name ? body.branch_name : "",
          GST_number: body.GST_number ? body.GST_number : "",
          IFSC_code: body.IFSC_code ? body.IFSC_code : "",
          PAN_number: body.PAN_number ? body.PAN_number : "",
          FSSAI_code: body.FSSAI_code ? body.FSSAI_code : "",
          imgPath: body.imgPath ? body.imgPath : [],
          currentAccount: body.currentAccount ? body.currentAccount : false
        }
      }
    );
    if (kycbank) {
      res.status(200).json({
        message: "Bank Details Successfully Updated..!"
      });
    } else {
      res.status(400).json({
        message: "Bank Details Not Successfully Updated 1 ..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
