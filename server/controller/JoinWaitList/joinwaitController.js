const WaitList = require("../../models/JoinWaitList.model");
const default_time = require("../../models/default");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.add_join_wait_list = async (req, res, next) => {
  const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
  try {
    let default_time_data = await default_time.findById(
      "5d664e0d75f24b217fade056"
    );
    if (default_time_data.default_time > 0) {
      const body = req.body;
      let newJoinList = new WaitList({
        user_id: id,
        wait_time: default_time_data.default_time,
        vendor_id: body.vendor_id,
        adults: body.adults ? body.adults : "",
        kids: body.kids ? body.kids : "",
        handicap: body.handicap ? body.handicap : "",
        highchair: body.highchair ? body.highchair : "",
        seating_preference: body.seating_preference
          ? body.seating_preference
          : [],
        restaurant_name: body.restaurant_name ? body.restaurant_name : "",
        special_occasion: body.special_occasion ? body.special_occasion : "",
        ETA: body.ETA ? body.ETA : "",
        name: body.name,
        dinner_status: "Nonseated",
        booking_date: Date.now()
      });
      let saveData = await newJoinList.save();
      if (saveData) {
        res.status(200).json({
          message: "Join Wait List Successfully Added..!"
        });
      }
    } else {
      return res.status(500).json({
        message: "Wait Is Currentlly Not Open"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
