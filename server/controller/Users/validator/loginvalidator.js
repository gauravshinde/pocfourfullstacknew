const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const loginValidation = async function ( data ) {
    let errors = { };

    data.mobile_number = !isEmpty( data. mobile_number) ? data.mobile_number : '';
    data.password = !isEmpty( data. password) ? data.password : '';


    if(!Validator.isLength( data.mobile_number,{
        min:10,max:10
    })){
        errors.mobile_number = "Invalid Mobile Number";
    }
    if( Validator.isEmpty( data.mobile_number ) ){
        errors.mobile_number = 'Mobile Number is Required';
    }
    if( Validator.isEmpty( data.password ) ){
        errors.password = 'Password is Required';
    }

    return {
        errors,
        isValid : isEmpty( errors )
    }
}

module.exports = loginValidation;