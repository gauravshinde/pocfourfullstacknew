const axios = require("axios");
const Users = require("../../models/User.model");
const userSignupValidator = require("./validator/signupvalidator");
const RestaurantDetails = require("../../models/RestaurantMapDetails");
const Persons = require("../../models/Person.model");
const RestaurantTypeDetails = require("../../models/RestaurantDetails");
const ServiceDetails = require("../../models/ServiceDetails");
const RestaurantFeatures = require("../../models/RestaurantFeatures");
const CusineOne = require("../../models/CusineFeaturesOne");
const CusineTwo = require("../../models/CusineFeatureTwo");
const RestaurantTime = require("../../models/RestaurantTime.model");
const subscription = require("../../models/subscription_model");
const KycBank = require("../../models/KycBank.model");
const default_time = require("../../models/default");
const Review = require("../../models/Review.model");
const loginvalidator = require("./validator/loginvalidator");
const sendOtpValidator = require("./validator/sendotpvalidator");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;
let FCM = require("fcm-call");

const accountSid = "ACa7e8734f7243eb4e8b322481d1a4376a";
const authToken = "9f375582bf6960e4ec9e6c6ef5e8eeba";
const client = require("twilio")(accountSid, authToken);
const QRCode = require("qrcode");
const sgmail = require("@sendgrid/mail");
sgmail.setApiKey(
  "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"
);

/* TEST CONTROLLER */
exports.user_test = async (req, res, next) => {
  try {
    res.send("Test Working");
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.send_twilio_email = (req, res, next) => {
  const body = req.body;
  const msg = {
    to: body.To_email,
    from: body.From_email,
    subject:
      "Welcome aboard! Your Amealio account has been successfully created. Thanks for signing up. We hope you love the app.",
    text:
      "Welcome aboard! We at Amealio are thrilled to have you. You've successfully signed up and created your Amealio account. Now, here's what you can do with Amealio: - Wait Lift: That's right! Lift the waiting time at restaurants. Just waitlist or check-in on Amealio.- Quick Pick: Skip the queue to get your order. Order on Amealio & pick up when you or your order is ready! Use coupon code 'PICKUPFIRST' for 10% discount on your first food order.- Reservation: Dinner dates or business brekkies, reserve a table on the app for now or later.- Refer your friends & win great discounts on your order.Look out for the QR code at restaurants. Scan it to view menu, order, waitlist or reserve on Amealio.At Amealio, we are always finding ways to improve your dining experience, so stay tuned for newer features. Got something to say about Amealio? We'd love to hear from you!  Write to us at support@Amealio.inNeed help to use Amealio? We are right here.Dine Differently",
  };
  const send = sgmail.send(msg);
  if (send) {
    res.status(200).json({
      message: "Email Send Successfully",
    });
  }
};
exports.user_signup = async (req, res, next) => {
  try {
    const body = req.body;
    const { errors, isValid } = await userSignupValidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      let newUser = new Users({
        email: body.email,
        country_code: body.country_code,
        mobile_number: body.mobile_number,
        password: body.password,
        role: "vendor",
        user_verified: true,
        has_admin_approved: false,
        register_date: Date.now(),
        restaurant_current_status: false,
        FCMtoken: body.FCMtoken ? body.FCMtoken : "",
        // body.has_admin_approved
        //   ? body.has_admin_approved
        //   : true

        //////////////////////
        /////////         After Start OTP Remove This Line {user_verified: true}
        //////////////////////
      });
      let newUserCreated = await newUser.save();
      if (newUserCreated) {
        // console.log(send);
        // const otpNumber = Math.floor(100000 + Math.random() * 900000);
        // const message = `Please use this OTP: ${otpNumber} to access your account.`;

        // const msg1 = {
        //   to: newUserCreated.email,
        //   from: "support@amealio.com",
        //   subject: "OTP",
        //   text: message
        // };
        // const send1 = sgmail.send(msg1);
        // console.log(send1);
        const create_new_details = new RestaurantDetails({
          vendor_id: newUserCreated._id,
          is_registered_with_google: body.is_registered_with_google
            ? body.is_registered_with_google
            : false,
          google_place_id: body.google_place_id ? body.google_place_id : "",
          restaurant_name: body.restaurant_name ? body.restaurant_name : "",
          restaurant_description: body.restaurant_description
            ? body.restaurant_description
            : "",
          restaurant_area: body.restaurant_area ? body.restaurant_area : "",
          restaurant_city: body.restaurant_city ? body.restaurant_city : "",
          restaurant_mobile_number: body.restaurant_mobile_number
            ? body.restaurant_mobile_number
            : "",
          restaurant_email: body.restaurant_email ? body.restaurant_email : "",
          restaurant_address: body.restaurant_address
            ? body.restaurant_address
            : "",
          lat: body.lat ? body.lat : "",
          lon: body.lon ? body.lon : "",
          offer_active: false,
        });
        let saveNewData = await create_new_details.save();
        const create_new_details1 = new RestaurantTypeDetails({
          vendor_id: newUserCreated._id,
          selected_restaurant_type: body.selected_restaurant_type
            ? body.selected_restaurant_type
            : [],
          primary_restaurant_type: body.primary_restaurant_type
            ? body.primary_restaurant_type
            : {},
          selected_dress_code_type: body.selected_dress_code_type
            ? body.selected_dress_code_type
            : [],
          primary_dress_code_type: body.primary_dress_code_type
            ? body.primary_dress_code_type
            : {},
          selected_payment_method_type: body.selected_payment_method_type
            ? body.selected_payment_method_type
            : [],
          primary_payment_method_type: body.primary_payment_method_type
            ? body.primary_payment_method_type
            : {},
          cost: body.cost ? body.cost : "0",
          revenue: body.revenue ? body.revenue : "0",
          restaurant_logo: body.restaurant_logo ? body.restaurant_logo : "",
          restaurant_photo: body.restaurant_photo ? body.restaurant_photo : [],
        });
        let saveNewData1 = await create_new_details1.save();

        const create_new_details2 = new ServiceDetails({
          vendor_id: newUserCreated._id,
          selected_faculty_type: body.selected_faculty_type
            ? body.selected_faculty_type
            : [],
          selected_services_type: body.selected_services_type
            ? body.selected_services_type
            : [],
          primary_service_type: body.primary_service_type
            ? body.primary_service_type
            : {},
        });
        let saveNewData6 = await create_new_details2.save();

        const create_new_details3 = new RestaurantFeatures({
          vendor_id: newUserCreated._id,
          selected_restaurant_features_type: body.selected_restaurant_features_type
            ? body.selected_restaurant_features_type
            : [],
          primary_restaurant_features_type: body.primary_restaurant_features_type
            ? body.primary_restaurant_features_type
            : {},
          selected_restaurant_access_type: body.selected_restaurant_access_type
            ? body.selected_restaurant_access_type
            : [],
          primary_restaurant_access_type: body.primary_restaurant_access_type
            ? body.primary_restaurant_access_type
            : {},
          selected_parking_type: body.selected_parking_type
            ? body.selected_parking_type
            : [],
          primary_selecte_parking_type: body.primary_selecte_parking_type
            ? body.primary_selecte_parking_type
            : {},
          parking_description: body.parking_description
            ? body.parking_description
            : "",
        });
        let saveNewData2 = await create_new_details3.save();
        const create_new_details4 = new CusineOne({
          vendor_id: newUserCreated._id,
          primary_food_category_icons: body.primary_food_category_icons
            ? body.primary_food_category_icons
            : {},
          selected_food_items_type: body.selected_food_items_type
            ? body.selected_food_items_type
            : [],
          primary_food_item_type: body.primary_food_item_type
            ? body.primary_food_item_type
            : {},
          allergy_information: body.allergy_information
            ? body.allergy_information
            : false,
          serve_liquor: body.serve_liquor ? body.serve_liquor : false,
          nutri_info: body.nutri_info ? body.nutri_info : false,
        });
        let saveNewData3 = await create_new_details4.save();
        const create_new_details5 = new CusineTwo({
          vendor_id: newUserCreated._id,
          selected_cusines_types: body.selected_cusines_types
            ? body.selected_cusines_types
            : [],
          primary_selected_icons: body.primary_selected_icons
            ? body.primary_selected_icons
            : {},
        });
        let saveNewData4 = await create_new_details5.save();
        const create_new_restaurant_time = new RestaurantTime({
          vendor_id: newUserCreated._id,
          all_seating_area_icons: body.all_seating_area_icons
            ? body.all_seating_area_icons
            : "",
          part_of_chain: body.part_of_chain ? body.part_of_chain : false,
          selectedChains: body.selectedChains ? body.selectedChains : [],
          are_you_open_24_x_7: body.are_you_open_24_x_7
            ? body.are_you_open_24_x_7
            : false,
          multiple_opening_time: body.multiple_opening_time
            ? body.multiple_opening_time
            : false,
          monday: body.monday ? body.monday : { open: false },
          tuesday: body.tuesday ? body.tuesday : { open: false },
          wednesday: body.wednesday ? body.wednesday : { open: false },
          thursday: body.thursday ? body.thursday : { open: false },
          friday: body.friday ? body.friday : { open: false },
          saturday: body.saturday ? body.saturday : { open: false },
          sunday: body.sunday ? body.sunday : { open: false },
          selected_seating_area: body.selected_seating_area
            ? body.selected_seating_area
            : [],
          primary_seating_area: body.primary_seating_area
            ? body.primary_seating_area
            : {},
          total_seating_capacity: body.total_seating_capacity
            ? body.total_seating_capacity
            : "",
        });
        let saveNewData5 = await create_new_restaurant_time.save();
        const create_new_details6 = new KycBank({
          vendor_id: newUserCreated._id,
          account_name: body.account_name ? body.account_name : "",
          bank_name: body.bank_name ? body.bank_name : "",
          account_number: body.account_number ? body.account_number : "",
          branch_name: body.branch_name ? body.branch_name : "",
          GST_number: body.GST_number ? body.GST_number : "",
          IFSC_code: body.IFSC_code ? body.IFSC_code : "",
          PAN_number: body.PAN_number ? body.PAN_number : "",
          FSSAI_code: body.FSSAI_code ? body.FSSAI_code : "",
          imgPath: body.imgPath ? body.imgPath : [],
          currentAccount: body.currentAccount ? body.currentAccount : false,
        });
        let saveBankData = await create_new_details6.save();
        const subs = new subscription({
          vendor_id: newUserCreated._id,
          servicePage: body.servicePage ? body.servicePage : "",
          request_period: body.request_period ? body.request_period : "",
          placement_period: body.placement_period ? body.placement_period : "",
          foodItem_counter: body.foodItem_counter
            ? body.foodItem_counter
            : false,
          takeaway_counter: body.takeaway_counter
            ? body.takeaway_counter
            : false,
          accept_Limit: body.accept_Limit ? body.accept_Limit : "",
          distance: body.distance ? body.distance : "",
          seating_area: body.seating_area ? body.seating_area : "",
          reservation_Limit: body.reservation_Limit
            ? body.reservation_Limit
            : "",
          around_time: body.around_time ? body.around_time : "",
          total_seating: body.total_seating ? body.total_seating : "",
          facility: body.facility ? body.facility : false,
          highchair_hadicap: body.highchair_hadicap
            ? body.highchair_hadicap
            : false,
          uploadMenu: body.uploadMenu ? body.uploadMenu : [],
          curb_side: body.curb_side ? body.curb_side : false,
          self_serve: body.self_serve ? body.self_serve : false,
          skip_line: body.skip_line ? body.skip_line : false,
          take_away: body.take_away ? body.take_away : false,
          walk_in: body.walk_in ? body.walk_in : false,
          waitlist: body.waitlist ? body.waitlist : false,
          reservation: body.reservation ? body.reservation : false,
        });
        let s = await subs.save();
        const review = new Review({
          vendor_id: newUserCreated._id,
          review: "0",
          rating: "0",
        });
        let savereview = await review.save();
        const newDefault = new default_time({
          vendor_id: newUserCreated._id,
          default_time: 0,
        });
        newDefault.save();
        return res.status(200).json({
          message: "User Sign Up Successfully Completed",
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.user_login = async (req, res, next) => {
  try {
    const body = req.body;
    const mobile_number = body.mobile_number;
    const password = body.password;
    const { errors, isValid } = await loginvalidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const user = await Users.findOne({ mobile_number: mobile_number });
      if (user && user.user_verified === true) {
        user.comparePassword(password, async (err, isMatch) => {
          if (isMatch) {
            const payload = {
              _id: user._id,
              email: user.email,
              mobile_number: user.mobile_number,
              have_submited_details: user.have_submited_details,
              has_admin_approved: user.has_admin_approved,
              role: user.role,
              FCMtoken: body.FCMtoken ? body.FCMtoken : "",
            };
            if (body.FCMtoken !== "Web") {
              await Users.findByIdAndUpdate(
                {
                  _id: user._id,
                },
                {
                  $set: {
                    _id: user._id,
                    email: user.email,
                    mobile_number: user.mobile_number,
                    have_submited_details: user.have_submited_details,
                    has_admin_approved: user.has_admin_approved,
                    role: user.role,
                    FCMtoken: body.FCMtoken ? body.FCMtoken : "",
                  },
                }
              );
            }
            jwt.sign(
              payload,
              keys,
              {
                expiresIn: 3600,
              },
              (err, token) => {
                res.json({
                  success: true,
                  token: "Bearer " + token,
                });
              }
            );
          } else {
            return res.status(400).json({
              mobile_number: "Invalid Credentials",
            });
          }
        });
      } else {
        return res.status(400).json({
          mobile_number: "Invalid Credentials",
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.send_otp = async (req, res, next) => {
  try {
    const body = req.body;
    let user = await Users.findOne({
      mobile_number: body.mobile_number,
    });
    const { errors, isValid } = await sendOtpValidator(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const otpNumber = Math.floor(100000 + Math.random() * 900000);
      const message = encodeURIComponent(
        `Please use this OTP: ${otpNumber} to access your account.`
      );
      try {
        let sentOtp = await axios.get(
          `https://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=${body.mobile_number}&msg=${message}&msg_type=TEXT&userid=2000177667&auth_scheme=plain&password=2019AABBcc!&v=1.1&format=text`
        );
        const msg = {
          to: user.email,
          from: "support@amealio.com",
          subject: "OTP",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Three</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style="padding: 10px; text-align: center; width: 100%;"> <img style="width: 170.5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/img3.png" /> <p style="font-weight:600; margin:20px auto; font-size:16px; color:#4B4B4B; font-family:"roboto";" > Verify Your OTP </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";" > <b>Your Verification Code: ' +
            otpNumber +
            '</b> <br />(This code will expire in 10 mins)<br /> <br />If you are having any issues with your account, please don’t<br /> hesitate to contact us by replying to this mail.<br /><br /> Thanks! </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        sgmail.send(msg);
        if (sentOtp) {
          let updateOtpOnDatabase = await Users.findOneAndUpdate(
            { mobile_number: body.mobile_number },
            { $set: { OTP: otpNumber } }
          );
          if (updateOtpOnDatabase) {
            return res.status(200).json({ message: "OTP sent Successfully" });
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.verify_otp = async (req, res, next) => {
  try {
    const body = req.body;
    let user = await Users.findOne({
      mobile_number: body.mobile_number,
      OTP: body.OTP,
    });
    if (user) {
      let change_userStae = await Users.findOneAndUpdate(
        { mobile_number: body.mobile_number },
        { $set: { user_verified: true, OTP: "sdasd@@#$#" } }
      );
      if (change_userStae) {
        const msg = {
          to: user.email,
          from: "support@amealio.com",
          subject: "Welcome aboard!",
          html:
            '<html lang="en"><head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Document</title> <style> @import url("https://fonts.googleapis.com/css?family=Roboto&display=swap"); .password-header { font-size: 42px; } .main-img { width: 30%; } .para-code { font-size: 20px; font-family: "Roboto", sans-serif; line-height: 1px; } .code-expire { font-size: 18px; font-family: "Roboto:300", sans-serif; color: #707070; } .love-us-text { font-size: 28px; } .footer-part { padding: 45px; font-size: 22px; line-height: 27px; font-family: "Roboto:500", sans-serif; } /********************** Mediaaaa Query ***********************/ @media (max-width: 800px) { .password-header { font-size: 25px; } .main-img { width: 50%; } .para-code { font-size: 12px; font-family: "Roboto", sans-serif; line-height: 1px; } .code-expire { font-size: 12px; font-family: "Roboto:300", sans-serif; color: #707070; } .love-us-text { font-size: 10px; } .footer-part { padding: 15px; font-size: 14px; line-height: 17px; } } </style></head><body> <div class="main-otp"> <img style=" width: 20%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png"> <div style="text-align: center;" class="center-part"> <img class="main-img" src="https://voiceicons.s3.ap-south-1.amazonaws.com/otp.png"> <h1 class="password-header" style="color: #4B4B4B; font-family: "Roboto", sans-serif;">Verify Your OTP</h1> <p style="color: #707070;" class="para-code">Your Verification Code : ' +
            otpNumber +
            ' </p> <span class="code-expire">(This code will expire in 10 minutes)</span> <h6 style="line-height: 15px; color:#707070 ;" class="para-code"> If you are having any issues with your account, please don’t hesitate to contact us by replying to this mail. <br></br><br></br>Thanks!</h6> </div> </div> <div style="display: flex; justify-content:center;margin-top: 10%; margin-bottom: 8%;" class="love-us-more"> <h2 class="love-us-text" style="margin-right: 2%;">Love us more ? <span style="color:#EE3A23;">Star us on</span> </h2> <img style="margin-right: 2%;width: 20%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png"> <img style="width: 20%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png"> </div> <div style="background-color: #B5B5B53C; color: #707070; text-align: center;" class="footer-part"> <img style="width: 7%; margin-right: 2%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png"> <img style="width: 7%; margin-right: 2%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png"> <img style="width: 7%; margin-right: 2%" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png"> <img style="width: 7%; margin-right: 2%" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png"> <img style="width: 7%; margin-right: 2%" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png"> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> </div></body></html>',
        };
        const send = sgmail.send(msg);
        return res.status(200).json({ message: "User Verfied Successfully!!" });
      }
    } else {
      return res.status(400).json({ message: "Unable to verify OTP" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_mobile_number = async (req, res, next) => {
  try {
    const body = req.body;
    let update_number = await Users.findOneAndUpdate(
      { mobile_number: body.mobile_number },
      { $set: { mobile_number: body.newmobile_number } }
    );
    if (update_number) {
      req.body.mobile_number = body.newmobile_number;
      next();
    } else {
      res.status(400).json({
        message: "Mobile Number Not Successfully Updated..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.check_unique_mobile_number = async (req, res, next) => {
  try {
    const body = req.body;
    let check_unique_mobile_number = await Users.findOne({
      mobile_number: body.mobile_number,
    });
    if (check_unique_mobile_number) {
      return res.status(400).json({
        mobile_number: "Mobile Number already exists",
      });
    }
    next();
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.verify_forgot_password = async (req, res, next) => {
  try {
    const body = req.body;
    let forgot_user = await Users.findOne({
      mobile_number: body.mobile_number,
      OTP: body.OTP,
    });
    const hashed = await forgot_user.change_password(body.password);
    if (forgot_user) {
      let verify_forgot_user = await Users.findOneAndUpdate(
        { mobile_number: body.mobile_number },
        { $set: { password: hashed } }
      );
      if (verify_forgot_user) {
        const msg = {
          to: forgot_user.email,
          from: "support@amealio.com",
          subject: "Change Password Verfied",
          html:
            '<!DOCTYPE html><html lang="en"><head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Document</title> <style> @import url("https://fonts.googleapis.com/css?family=Roboto&display=swap"); .password-header { font-size: 42px; } .main-img { width: 30%; } .greetings-para { font-size: 20px; font-family: "Roboto", sans-serif; color: #70707099; } button { background-color: #F85032; width: 14%; height: 44px; border-radius: 5px; border: none; color: #FFFFFF; font-family: "Roboto", sans-serif; font-size: 18px; } .love-us-text { font-size: 28px; } .footer-part { padding: 45px; font-size: 22px; line-height: 27px; font-family: "Roboto:500", sans-serif; } /********************** Mediaaaa Query ***********************/ @media (max-width: 800px) { .main-img { width: 60%; } .password-header { font-size: 28px; } .greetings-para { font-size: 15px; } .love-us-text { font-size: 10px; } .footer-part { padding: 15px; font-size: 14px; line-height: 17px; } button { width: 27%; height: 30px; font-size: 12px; } } </style></head><body> <div class="main"> <div style="margin-bottom: 5%;" class="amealio-log"> <img style=" width: 20%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png"> <div style="text-align: center;" class="center-part"> <img class="main-img" src="https://voiceicons.s3.ap-south-1.amazonaws.com/think.png"> <h1 class="password-header" style="color: #4B4B4B; font-family: "Roboto", sans-serif;">Change Your Password ?</h1> <p class="greetings-para">Hi, Sumit Joshi !</p> <p class="greetings-para">There was a request to change your password</p> <button>Reset Password</button> </div> </div> <div style="display: flex; justify-content:center;margin-top: 10%; margin-bottom: 8%;" class="love-us-more"> <h2 class="love-us-text" style="margin-right: 2%;">Love us more ? <span style="color:#EE3A23;">Star us on</span> </h2> <img style="margin-right: 2%;width: 20%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png"> <img style="width: 20%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png"> </div> </div> <div style="background-color: #B5B5B53C; color: #707070; text-align: center;" class="footer-part"> <img style="width: 7%; margin-right: 2%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png"> <img style="width: 7%; margin-right: 2%;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png"> <img style="width: 7%; margin-right: 2%" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png"> <img style="width: 7%; margin-right: 2%" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png"> <img style="width: 7%; margin-right: 2%" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png"> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> </div></body></html>',
        };
        sgmail.send(msg);
        return res.status(200).json({ message: "User Verfied Successfully!!" });
      }
    } else {
      return res.status(400).json({ message: "Unable to verify OTP" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

// exports.user_login = async (req, res, next) => {
//   try {
//     const body = req.body;
//     const mobile_number = body.mobile_number;
//     const password = body.password;
//     const { errors, isValid } = await loginvalidator(body);
//     if (!isValid) {
//       return res.status(400).json(errors);
//     } else {
//       const user = await Users.findOne({ mobile_number: mobile_number });
//       if (user && user.user_verified === true) {
//         user.comparePassword(password, (err, isMatch) => {
//           if (isMatch) {
//             const payload = {
//               _id: user._id,
//               email: user.email,
//               mobile_number: user.mobile_number,
//               have_submited_details: user.have_submited_details,
//               has_admin_approved: user.has_admin_approved,
//               role: user.role,
//               FCMtoken:body.FCMtoken
//             };
//             jwt.sign(
//               payload,
//               keys,
//               {
//                 expiresIn: 3600
//               },
//               (err, token) => {
//                 res.json({
//                   success: true,
//                   token: "Bearer " + token
//                 });
//               }
//             );
//             try {
//               let data = Users.findOneAndUpdate(
//                 {
//                   _id: user._id
//                 },
//                 {
//                   $set: {
//                     _id: user._id,
//                     email: user.email,
//                     mobile_number: user.mobile_number,
//                     have_submited_details: user.have_submited_details,
//                     has_admin_approved: user.has_admin_approved,
//                     role: user.role,
//                     FCMtoken:body.FCMtoken
//                   }
//                 }
//               );
//             } catch (err) {
//               let errors = {};
//               console.log(err);
//               errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//               return res.status(500).json(errors);
//             }
//           } else {
//             return res.status(400).json({
//               mobile_number: "Invalid Credentials"
//             });
//           }
//         });
//       } else {
//         return res.status(400).json({
//           mobile_number: "Invalid Credentials"
//         });
//       }
//     }
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.user_has_completed_flow = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Users.findOneAndUpdate(
      { _id: id },
      { $set: { have_submited_details: true } }
    );
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.call_twilio = (req, res, next) => {
  const body = req.body;
  client.calls.create(
    {
      url: "http://demo.twilio.com/docs/voice.xml",
      to: body.mob,
      from: "+14152003837",
    },
    (err, call) => {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(call.sid);
      }
    }
  );
};

exports.update_hotel_availability = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Users.findOneAndUpdate(
      {
        _id: body.vendor_id,
      },
      {
        $set: {
          has_admin_approved: body.has_admin_approved,
        },
      }
    );
    if (data) console.log(data);
    return res.status(200).json({
      message: "Success",
    });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.vendor_approved = async (req, res, next) => {
  try {
    const body = req.body;
    let approved = await Users.findByIdAndUpdate(
      {
        _id: body.vendor_id,
      },
      {
        $set: {
          has_admin_approved: body.has_admin_approved,
        },
      }
    );
    if (approved) {
      res.status(200).json({
        message: "Vendor Successfully Approved",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_restaurant_current_status = async (req, res, next) => {
  try {
    let current_status_array = [];
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Users.findOne({
      _id: id,
    });
    var Arraydata = {
      _id: data._id,
      restaurant_current_status: data.restaurant_current_status,
    };
    current_status_array.push(Arraydata);
    return res.status(200).json(current_status_array);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_resto_current_status = async (req, res, next) => {
  try {
    const body = req.body;
    let restoUser = await Users.findByIdAndUpdate(
      {
        _id: body.id,
      },
      {
        $set: {
          restaurant_current_status: body.restaurant_current_status,
        },
      }
    );
    if (restoUser) {
      res.status(200).json({
        message: "Restaurant Status Changed..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
