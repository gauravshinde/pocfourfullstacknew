const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const cusineTwoValidation = async function ( data ) {
    let errors = { };

    data.selected_cusines_types = !isEmpty( data. selected_cusines_types) ? data.selected_cusines_types : '';
    data.primary_selected_icons = !isEmpty( data. primary_selected_icons) ? data.primary_selected_icons : '';
    
    if( Validator.isEmpty( data.selected_cusines_types ) ){
        errors.selected_cusines_types = 'Selected Cusines Types is Required';
    }
    if( Validator.isEmpty( data.primary_food_item_type ) ){
        errors.primary_selected_icons = 'Primary Selected Icons is Required';
    }

    return {
        errors,
        isValid : isEmpty( errors )
    }
}

module.exports = cusineTwoValidation;