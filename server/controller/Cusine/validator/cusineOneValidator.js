const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

const cusineOneValidation = async function ( data ) {
    let errors = { };

    data.selected_food_items_type = !isEmpty( data. selected_food_items_type) ? data.selected_food_items_type : '';
    data.primary_food_item_type = !isEmpty( data. primary_food_item_type) ? data.primary_food_item_type : '';
    data.primary_food_item_type = !isEmpty( data. primary_food_item_type) ? data.primary_food_item_type : '';
    
    if( Validator.isEmpty( data.selected_food_items_type ) ){
        errors.selected_food_items_type = 'Selected Food Items Type is Required';
    }
    if( Validator.isEmpty( data.primary_food_item_type ) ){
        errors.primary_food_item_type = 'Primary Food Item Type is Required';
    }

    return {
        errors,
        isValid : isEmpty( errors )
    }
}

module.exports = cusineOneValidation;