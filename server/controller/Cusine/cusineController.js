const CusineOne = require("../../models/CusineFeaturesOne");
//const cusineOneValidator = require('./validator/cusineOneValidator');

const CusineTwo = require("../../models/CusineFeatureTwo");
//const cusineTwoValidator = require('./validator/cusineTwoValidator');

const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.create_new_cusine_One = async (req, res, next) => {
  try {
    const body = req.body;
    const create_new_details = new CusineOne({
      vendor_id: body.vendor_id,
      primary_food_category_icons: body.primary_food_category_icons
        ? body.primary_food_category_icons
        : "",
      selected_food_items_type: body.selected_food_items_type
        ? body.selected_food_items_type
        : [],
      primary_food_item_type: body.primary_food_item_type
        ? body.primary_food_item_type
        : "",
      allergy_information: body.allergy_information,
      serve_liquor: body.serve_liquor,
      nutri_info: body.nutri_info
    });
    let saveNewData = await create_new_details.save();
    if (saveNewData) {
      return res.status(200).json({ message: "New Cusine Details Created" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.create_new_cusine_Two = async (req, res, next) => {
  try {
    const body = req.body;
    const create_new_details = new CusineTwo({
      vendor_id: body.vendor_id,
      selected_cusines_types: body.selected_cusines_types
        ? body.selected_cusines_types
        : [],
      primary_selected_icons: body.primary_selected_icons
        ? body.primary_selected_icons
        : ""
    });
    let saveNewData = await create_new_details.save();
    if (saveNewData) {
      return res.status(200).json({ message: "New Cusine Details Created" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_cusine_one = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await CusineOne.findOne({ vendor_id: id });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_cusine_two = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await CusineTwo.findOne({ vendor_id: id });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_cuisine_one = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let Cusineone = await CusineOne.findOneAndUpdate(
      { vendor_id: id },
      {
        $set: {
          primary_food_category_icons: body.primary_food_category_icons
            ? body.primary_food_category_icons
            : "",
          selected_food_items_type: body.selected_food_items_type
            ? body.selected_food_items_type
            : [],
          primary_food_item_type: body.primary_food_item_type
            ? body.primary_food_item_type
            : "",
          allergy_information: body.allergy_information,
          serve_liquor: body.serve_liquor,
          nutri_info: body.nutri_info
        }
      }
    );
    if (Cusineone) {
      res.send("success");
      next();
      // res.send('success');
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_cuisine_two = async (req, res, next) => {
  try {
    const body = req.body;
    // const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let Cusinetwo = await CusineTwo.findOneAndUpdate(
      { vendor_id: body.id },
      {
        $set: {
          selected_cusines_types: body.selected_cusines_types
            ? body.selected_cusines_types
            : [],
          primary_selected_icons: body.primary_selected_icons
            ? body.primary_selected_icons
            : ""
        }
      }
    );
    if (Cusinetwo) {
      res.status(200).json({
        message: "Restaurant Feature Successfully Updated..!"
      });
    } else {
      res.status(400).json({
        message: "Restaurant Feature Not Successfully Updated..!"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_cuisine_two_data = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let Cusinetwo = await CusineTwo.findOneAndUpdate(
      { vendor_id: id },
      {
        $set: {
          selected_cusines_types: body.selected_cusines_types
            ? body.selected_cusines_types
            : [],
          primary_selected_icons: body.primary_selected_icons
            ? body.primary_selected_icons
            : ""
        }
      }
    );
    if (Cusinetwo) {
      res.send("success");
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
