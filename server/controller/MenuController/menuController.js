const Menu = require("../../models/Menu.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;

exports.add_New_Menu = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const body = req.body;
    const newMenu = new Menu({
      vendor_id: id,
      item_name: body.item_name ? body.item_name : "",
      category: body.category ? body.category : "",
      item_code: body.item_code ? body.item_code : "",
      item_price: body.item_price ? body.item_price : "",
      processing_time: body.processing_time ? body.processing_time : "",
      calorific_value: body.calorific_value ? body.calorific_value : "",
      serving_size: body.serving_size ? body.serving_size : "",
      description: body.description ? body.description : "",
      photo: body.photo ? body.photo : [],
      specific_time_item: body.specific_time_item,
      item_available_for: body.item_available_for
        ? body.item_available_for
        : [],
      multiple_opening_time: body.multiple_opening_time,
      monday: body.monday ? body.monday : "",
      tuesday: body.tuesday ? body.tuesday : "",
      wednesday: body.wednesday ? body.wednesday : "",
      thursday: body.thursday ? body.thursday : "",
      friday: body.friday ? body.friday : "",
      saturday: body.saturday ? body.saturday : "",
      sunday: body.sunday ? body.sunday : "",
      food_type: body.food_type ? body.food_type : "",
      allergy_info: body.allergy_info ? body.allergy_info : "",
      available_take_away: body.available_take_away,
      available_curb_side: body.available_curb_side,
      item_auto_accept: body.item_auto_accept,
      item_spicy: body.item_spicy,
      level_of_spiciness: body.level_of_spiciness
        ? body.level_of_spiciness
        : "",
      spice_level_customizable: body.spice_level_customizable,
      nutrition_info: body.nutrition_info ? body.nutrition_info : "",
      select_tag: body.select_tag ? body.select_tag : "",
      item_unit: body.item_unit ? body.item_unit : "",
      status: true
    });
    let saveNewMenu = await newMenu.save();
    if (saveNewMenu) {
      return res.status(200).json({
        message: "New Menu Added Successfully"
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_all_menu = async (req, res, next) => {
  try {
    //const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let MenuData = await Menu.find({});
    if (MenuData) {
      return res.status(200).json(MenuData);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_menu = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Menu.findByIdAndUpdate(
      {
        _id: body.id
      },
      {
        $set: {
          item_name: body.item_name ? body.item_name : "",
          category: body.category ? body.category : "",
          item_code: body.item_code ? body.item_code : "",
          item_price: body.item_price ? body.item_price : "",
          processing_time: body.processing_time ? body.processing_time : "",
          calorific_value: body.calorific_value ? body.calorific_value : "",
          serving_size: body.serving_size ? body.serving_size : "",
          description: body.description ? body.description : "",
          photo: body.photo ? body.photo : [],
          specific_time_item: body.specific_time_item,
          item_available_for: body.item_available_for
            ? body.item_available_for
            : [],
          multiple_opening_time: body.multiple_opening_time,
          monday: body.monday ? body.monday : "",
          tuesday: body.tuesday ? body.tuesday : "",
          wednesday: body.wednesday ? body.wednesday : "",
          thursday: body.thursday ? body.thursday : "",
          friday: body.friday ? body.friday : "",
          saturday: body.saturday ? body.saturday : "",
          sunday: body.sunday ? body.sunday : "",
          food_type: body.food_type ? body.food_type : "",
          allergy_info: body.allergy_info ? body.allergy_info : "",
          available_take_away: body.available_take_away,
          available_curb_side: body.available_curb_side,
          item_auto_accept: body.item_auto_accept,
          item_spicy: body.item_spicy,
          level_of_spiciness: body.level_of_spiciness
            ? body.level_of_spiciness
            : "",
          spice_level_customizable: body.spice_level_customizable,
          nutrition_info: body.nutrition_info ? body.nutrition_info : "",
          select_tag: body.select_tag ? body.select_tag : "",
          item_unit: body.item_unit ? body.item_unit : "",
          status: true
        }
      }
    );
    if (data)
      return res.status(200).json({
        message: "Successfully Update Item Details"
      });
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_each_item_details = async (req, res, next) => {
  try {
    const body = req.body;
    let MenuData = await Menu.find({
      _id: body.id
    });
    if (MenuData) {
      return res.status(200).json(MenuData);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
