const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;
const Dinner = require("../../models/add_dinner");
const subscription = require("../../models/subscription_model");
const default_time = require("../../models/default");
const RestaurantTime = require("../../models/RestaurantTime.model");
const add_dinner_validation = require("./validator/add_dinner");
const msg91 = require("msg91")("231660ASmgnCOLK5b728a5e", "SomiDb", "4");
const Reservation = require("../../models/Reservation");
const InnerReservation = require("../../models/InnerReservation");

const JoinWaitList = require("../../models/JoinWaitList.model");
const moment = require("moment");
var date = new Date();

const sgmail = require("@sendgrid/mail");
sgmail.setApiKey(
  "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"
);

const accountSid = "ACa7e8734f7243eb4e8b322481d1a4376a";
const authToken = "9f375582bf6960e4ec9e6c6ef5e8eeba";
const client = require("twilio")(accountSid, authToken);

exports.email = async (req, res, next) => {
  try {
    const body = req.body;
    const msg = {
      to: body.email,
      from: "support@amealio.com",
      subject: "Welcome aboard!",
      text:
        "Welcome aboard! We at Amealio are thrilled to have you. You've successfully signed up and created your Amealio account. Now, here's what you can do with Amealio: - Wait Lift: That's right! Lift the waiting time at restaurants. Just waitlist or check-in on Amealio.- Quick Pick: Skip the queue to get your order. Order on Amealio & pick up when you or your order is ready! Use coupon code 'PICKUPFIRST' for 10% discount on your first food order.- Reservation: Dinner dates or business brekkies, reserve a table on the app for now or later.- Refer your friends & win great discounts on your order.Look out for the QR code at restaurants. Scan it to view menu, order, waitlist or reserve on Amealio.At Amealio, we are always finding ways to improve your dining experience, so stay tuned for newer features. Got something to say about Amealio? We'd love to hear from you!  Write to us at support@Amealio.inNeed help to use Amealio? We are right here.Dine Differently",
      html: `<div style={{ width: '100%', height: '100%', background: 'red'}}>
        <p>Dear user, new</p>
      <p>Here is your email.</p>
        <div>`,
    };
    const send = sgmail.send(msg);
    if (send) {
      return res.status(200).json({ message: "Success" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_dinner_list = (req, res, next) => {
  Dinner.find()
    .exec()
    .then((DinnerList) => {
      res.status(200).json(DinnerList);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};

exports.add_wake_in = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let dinner = new Dinner({
      vendor_id: req.body.vendor_id,
      user_id: id,
      order_type: "walkin",
      entry_point: req.body.entry_point,
      name: req.body.name ? req.body.name : "",
      phone_number: req.body.phone_number ? req.body.phone_number : "",
      adults: req.body.adults ? req.body.adults : "0",
      kids: req.body.kids ? req.body.kids : "0",
      highchair: req.body.highchair ? req.body.highchair : "0",
      handicap: req.body.handicap ? req.body.handicap : "0",
      seating_preference: req.body.seating_preference
        ? req.body.seating_preference
        : "",
      total: parseInt(req.body.adults) + parseInt(req.body.kids),
      wait_time: "0",
      special_occassion: req.body.special_occassion
        ? req.body.special_occassion
        : "",
      Request_Date: Date.now(),
      Seating_Date: Date.now(),
      Date: req.body.Date ? req.body.Date : "",
      time: req.body.time ? req.body.time : "",
      ETA: req.body.ETA ? req.body.ETA : "",
      dinner_status: "Nonseated",
      reservation_status: "Nonseated",
      token: req.body.token,
      restaurant_name: req.body.restaurant_name ? req.body.restaurant_name : "",
    });
    let newdinner = await dinner.save();
    if (newdinner) {
      return res.status(200).json(
        //message: "Dinner Successfully Added"
        next()
      );
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.walkin_msg = async (req, res, next) => {
  try {
    const body = req.body;
    client.messages
      .create({
        body: "You have been added to walkin. Your wait time is 00 minutes",
        messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
        form: "+14152003837",
        to: body.phone_number,
      })
      .then(
        res.status(200).json({
          message: "Dinner Successfully Added",
        })
      );
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.update_status_reservation = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Dinner.findByIdAndUpdate(
      {
        _id: body.id,
      },
      {
        $set: {
          reservation_status: body.reservation_status,
          ETA: body.ETA ? body.ETA : "",
          reject_reasons: body.reject_reasons ? body.reject_reasons : "",
          enter_reasons: body.enter_reasons ? body.enter_reasons : "",
          Seating_Date: Date.now(),
        },
      }
    );
    if (data) {
      res.status(200).json({
        message: "Status Update SuccessFully..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_status = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Dinner.findByIdAndUpdate(
      {
        _id: body.dinnerId,
      },
      {
        $set: {
          dinner_status: body.dinner_status,
          Seating_Date: Date.now(),
        },
      }
    );
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

// exports.update_status = async (req, res, next) => {
//   try {
//     const body = req.body;
//     let data = await Dinner.findByIdAndUpdate(
//       {
//         _id: body.dinnerId
//       },
//       {
//         $set: {
//           dinner_status: body.dinner_status,
//           Seating_Date: Date.now()
//         }
//       }
//     );
//     if (body.token !== "" && body.mobileNo !== "" && !body.isVendor) {
//       var message = {
//         notification: {
//           title: "Amealio App",
//           body: body.dinner_status
//         },
//         token: body.token
//       };
//       FCM.send(message, (err, response) => {});
//       next();
//     }
//     if(body.token !== "" && body.mobileNo !== "" && body.isVendor) {
//       var message = {
//         notification: {
//           title: "Amealio App",
//           body: body.dinner_status
//         },
//         token: body.token
//       };
//       FCM.send(message, (err, response) => {});
//       next();
//     }
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.sms = async (req, res, next) => {
  const body = req.body;
  client.messages
    .create({
      body: "You Are Now" + body.dinner_status,
      messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
      form: "+14152003837",
      to: body.mobileNo,
    })
    .then((message) => res.json(message.sid));
};

exports.send_sms = (req, res) => {
  const body = req.body;
  client.messages
    .create({
      body: body.message,
      messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
      form: "+14152003837",
      to: body.mobileNo,
    })
    .then((message) => res.json(message.sid));
};

exports.get_wait_time = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let defaultWaitTime = await default_time.findOne(
      {
        vendor_id: id,
      },
      {
        default_time: 1,
        _id: 0,
      }
    );
    return res.status(200).json(defaultWaitTime);
  } catch (err) {
    return res.status(400).json("Error");
  }
};

exports.add_default_wait_time = (req, res, next) => {
  const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
  const newDefault = new default_time({
    vendor_id: id,
    default_time: 0,
  });
  newDefault
    .save()
    .then((result) => {
      return res.status(201).json({
        message: "Default Time Added Successfully...!",
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Something Wrong",
      });
    });
};

exports.update_default_wait_time = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const body = req.body;
    let updateDefaultWaitTime = await default_time.findOneAndUpdate(
      {
        vendor_id: id,
      },
      {
        $set: {
          default_time: body.default_time ? body.default_time : 0,
        },
      },
      {
        new: true,
      }
    );
    if (updateDefaultWaitTime) {
      return res.status(201).json(updateDefaultWaitTime);
    }
  } catch (err) {
    return res.status(400).json("Error");
  }
};

exports.add_dinner = async (req, res, next) => {
  const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
  try {
    if (req.body.wait_time > 0) {
      let user_find = await Dinner.find({
        phone_number: req.body.phone_number,
        vendor_id: id,
        dinner_status: "Nonseated",
        $or: [
          {
            order_type: "waitlist",
          },
          {
            order_type: "walkin",
          },
        ],
      });
      var newdate = moment(new Date(), "DD-MM-YYYY hh:mm:ss A")
        .add(req.body.wait_time * 60, "seconds")
        .format("DD-MM-YYYY hh:mm:ss A");
      if (user_find == 0) {
        let dinner = new Dinner({
          vendor_id: id,
          orderId: req.body.orderId,
          user_id: req.body.user_id ? req.body.user_id : "",
          order_type: "waitlist",
          name: req.body.name ? req.body.name : "",
          phone_number: req.body.phone_number ? req.body.phone_number : "",
          adults: req.body.adults,
          kids: req.body.kids,
          highchair: req.body.highchair,
          handicap: req.body.handicap,
          seating_preference: req.body.seating_preference
            ? req.body.seating_preference
            : "",
          total: parseInt(req.body.adults) + parseInt(req.body.kids),
          wait_time: newdate,
          default_wait_time: req.body.wait_time,
          special_occassion: req.body.special_occassion
            ? req.body.special_occassion
            : "",
          Request_Date: Date.now(),
          Seating_Date: Date.now(),
          booking_date: moment(new Date()).format("DD-MM-YYYY"),
          Date: req.body.Date ? req.body.Date : "",
          time: req.body.time ? req.body.time : "",
          ETA: req.body.ETA ? req.body.ETA : "",
          dinner_status: "Nonseated",
          reservation_status: "Nonseated",
          register_date: Date.now(),
          entry_point: req.body.entry_point,
          token: req.body.token ? req.body.token : "",
        });

        let newdinner = await dinner.save();
        if (newdinner) {
          res.status(200).json({
            message: "Dinner Successfully Added",
          });
          next();
        }
      } else {
        return res.status(401).json({ message: "User All Ready Exites" });
      }
    } else {
      let user_find = await Dinner.find({
        phone_number: req.body.phone_number,
        vendor_id: id,
        dinner_status: "Nonseated",
        $or: [
          {
            order_type: "waitlist",
          },
          {
            order_type: "walkin",
          },
        ],
      });
      if (user_find == 0) {
        let dinner = new Dinner({
          vendor_id: id,
          orderId: req.body.orderId,
          user_id: req.body.user_id ? req.body.user_id : "",
          order_type: "walkin",
          name: req.body.name ? req.body.name : "",
          phone_number: req.body.phone_number ? req.body.phone_number : "",
          adults: req.body.adults,
          kids: req.body.kids,
          highchair: req.body.highchair,
          handicap: req.body.handicap,
          seating_preference: req.body.seating_preference
            ? req.body.seating_preference
            : "",
          total: parseInt(req.body.adults) + parseInt(req.body.kids),
          wait_time: 0,
          default_wait_time: "0",
          special_occassion: req.body.special_occassion
            ? req.body.special_occassion
            : "",
          Request_Date: Date.now(),
          Seating_Date: Date.now(),
          booking_date: moment(new Date()).format("DD-MM-YYYY"),
          Date: req.body.Date ? req.body.Date : "",
          time: req.body.time ? req.body.time : "",
          ETA: req.body.ETA ? req.body.ETA : "",
          dinner_status: "Nonseated",
          reservation_status: "Nonseated",
          register_date: Date.now(),
          entry_point: req.body.entry_point,
          token: req.body.token ? req.body.token : "",
        });
        let newdinner = await dinner.save();
        if (newdinner) {
          res.status(200).json({
            message: "Dinner Successfully Added",
          });
          next();
        }
      } else {
        return res.status(401).json({ message: "User All Ready Exites" });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

// exports.add_dinner = async (req, res, next) => {
//   const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
//   try {
//     if (req.body.wait_time > 0) {
//       var newdate = moment(new Date(), "DD-MM-YYYY hh:mm:ss A")
//         .add(req.body.wait_time * 60, "seconds")
//         .format("DD-MM-YYYY hh:mm:ss A");
//       let dinner = new Dinner({
//         vendor_id: id,
//         orderId: req.body.orderId,
//         user_id: req.body.user_id ? req.body.user_id : "",
//         order_type: "waitlist",
//         name: req.body.name ? req.body.name : "",
//         requestNumber: Math.floor(Math.random() * 10000000000),
//         phone_number: req.body.phone_number ? req.body.phone_number : "",
//         adults: req.body.adults,
//         kids: req.body.kids,
//         highchair: req.body.highchair,
//         handicap: req.body.handicap,
//         seating_preference: req.body.seating_preference
//           ? req.body.seating_preference
//           : "",
//         total: parseInt(req.body.adults) + parseInt(req.body.kids),
//         wait_time: newdate,
//         default_wait_time: req.body.wait_time,
//         special_occassion: req.body.special_occassion
//           ? req.body.special_occassion
//           : "",
//         Request_Date: Date.now(),
//         Seating_Date: Date.now(),
//         booking_date: moment(new Date()).format("DD-MM-YYYY"),
//         Date: req.body.Date ? req.body.Date : "",
//         time: req.body.time ? req.body.time : "",
//         ETA: req.body.ETA ? req.body.ETA : "",
//         dinner_status: "Nonseated",
//         reservation_status: "Nonseated",
//         register_date: Date.now(),
//         entry_point: req.body.entry_point,
//         token: req.body.token ? req.body.token : "",
//       });
//       let newdinner = await dinner.save();
//       if (newdinner) {
//         res.status(200).json({
//           message: "Dinner Successfully Added",
//         });
//         next();
//       }
//     } else {
//       let dinner = new Dinner({
//         vendor_id: id,
//         orderId: req.body.orderId,
//         user_id: req.body.user_id ? req.body.user_id : "",
//         order_type: "walkin",
//         name: req.body.name ? req.body.name : "",
//         requestNumber: Math.floor(Math.random() * 10000000000),
//         phone_number: req.body.phone_number ? req.body.phone_number : "",
//         adults: req.body.adults,
//         kids: req.body.kids,
//         highchair: req.body.highchair,
//         handicap: req.body.handicap,
//         seating_preference: req.body.seating_preference
//           ? req.body.seating_preference
//           : "",
//         total: parseInt(req.body.adults) + parseInt(req.body.kids),
//         wait_time: 0,
//         default_wait_time: "0",
//         special_occassion: req.body.special_occassion
//           ? req.body.special_occassion
//           : "",
//         Request_Date: Date.now(),
//         Seating_Date: Date.now(),
//         booking_date: moment(new Date()).format("DD-MM-YYYY"),
//         Date: req.body.Date ? req.body.Date : "",
//         time: req.body.time ? req.body.time : "",
//         ETA: req.body.ETA ? req.body.ETA : "",
//         dinner_status: "Nonseated",
//         reservation_status: "Nonseated",
//         register_date: Date.now(),
//         entry_point: req.body.entry_point,
//         token: req.body.token ? req.body.token : "",
//       });
//       let newdinner = await dinner.save();
//       if (newdinner) {
//         res.status(200).json({
//           message: "Dinner Successfully Added",
//         });
//         next();
//       }
//     }
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.restaurant_info = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await RestaurantTime.findOne({
      vendor_id: id,
    });
    total_seating = data.total_seating_capacity;
    return res.status(200).json(total_seating);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_each_dinner = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      vendor_id: id,
      $or: [
        {
          dinner_status: "Nonseated",
        },
        {
          dinner_status: "Seated",
        },
      ],
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_dinner = async (req, res, next) => {
  try {
    const body = req.body;
    //console.log(body)
    let dinner = await Dinner.findByIdAndUpdate(
      {
        _id: body.id,
      },
      {
        $set: {
          name: body.name,
          phone_number: body.phone_number,
          adults: body.adults,
          kids: body.kids,
          highchair: body.highchair,
          handicap: body.handicap,
          seating_preference: body.seating_preference,
          total: parseInt(body.adults) + parseInt(body.kids),
          wait_time: body.wait_time,
          special_occassion: body.special_occassion,
          Request_Date: body.Request_Date,
          Seating_Date: body.Seating_Date,
        },
      }
    );

    if (dinner) {
      res.status(200).json({
        message: "Dinner Details Successfully Updated..!",
      });
    } else {
      res.status(400).json({
        message: "Dinner Details Not Successfully Updated..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.cancel_reservation = async (req, res, next) => {
  try {
    const body = req.body;
    let data = await Dinner.findByIdAndUpdate(
      {
        _id: body.id,
      },
      {
        $set: {
          Status: "Cancel",
        },
      }
    );
    if (data) {
      res.status(200).json({
        message: "Order Successfully Cancel..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_reservation = async (req, res, next) => {
  console.log('get_reservationdata')
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      vendor_id: id,
      reservation_status: "Pending",
      Date: body.Date,
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_each_dinner_pending = async (req, res, next) => {
  console.log('get_preservationdata')
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      vendor_id: id,
      $or: [
        {
          reservation_status: "Pending",
        },
        {
          dinner_status: "Pending",
        },
      ],
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.send_sms_twilio = (req, res, next) => {
  const body = req.body;
  client.messages
    .create({
      body:
        "Your request has been confirmed and your wait time is " +
        body.wait_time +
        "minutes",
      messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
      form: "+14152003837",
      to: body.phone_number,
    })
    .then((message) => res.json(message.sid));
};

exports.update_wait_time_twilio_sms = (req, res, next) => {
  const body = req.body;
  client.messages
    .create({
      body: "Your New Waiting Time is Updated " + body.wait_time + "Minutes",
      messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
      form: "+14152003837",
      to: body.phone_number,
    })
    .then((message) => res.json(message.sid));
};
exports.get_history = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      vendor_id: id,
    });
    let basic = data.reverse();
    return res.status(200).json(basic);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.add_join_wait_list = async (req, res, next) => {
  const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
  const body = req.body;
  try {
    let default_time_data = await default_time.findOne({
      vendor_id: body.vendor_id,
    });
    var newdate = moment(new Date(), "DD/MM/YYYY hh:mm:ss A")
      .add(default_time_data.default_time * 60, "seconds")
      .format("DD/MM/YYYY hh:mm:ss A");
    if (default_time_data.default_time > 0) {
      let newJoinList = new Dinner({
        user_id: id,
        order_type: "waitlist",
        wait_time: newdate,
        vendor_id: body.vendor_id,
        adults: body.adults ? body.adults : 0,
        kids: body.kids ? body.kids : 0,
        total: parseInt(body.adults) + parseInt(body.kids),
        handicap: body.handicap ? body.handicap : 0,
        highchair: body.highchair ? body.highchair : 0,
        seating_preference: body.seating_preference
          ? body.seating_preference
          : [],
        restaurant_name: body.restaurant_name ? body.restaurant_name : "",
        special_occassion: body.special_occassion ? body.special_occassion : "",
        ETA: body.ETA ? body.ETA : "",
        dinner_status: "Nonseated",
        Request_Date: Date.now(),
        Seating_Date: Date.now(),
        booking_date: Date.now(),
        name: body.name,
        phone_number: body.phone_number,
        time: body.time ? body.time : "",
        token: body.token,
      });
      let saveData = await newJoinList.save();
      if (saveData) {
        return res.status(200).json(
          //message: "Join Wait List Successfully Added..!"
          next()
        );
      }
    } else {
      return res.status(500).json({
        message: "Wait Is Currentlly Not Open",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.waitlist_msg = async (req, res, next) => {
  try {
    const body = req.body;
    client.messages
      .create({
        body: "You have been added to waitlist",
        messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
        form: "+14152003837",
        to: body.phone_number,
      })
      .then(
        res.status(200).json({
          message: "Join Wait List Successfully Added..!",
        })
      );
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

// exports.get_waitlist = async (req, res, next) => {
//   try {
//     const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
//     let data = await Dinner.find({
//       vendor_id: id,
//       order_type: "waitlist",
//     });
//     return res.status(200).json(data);
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.get_waitlist = async (req, res, next) => {
  try {
    const body = req.body;
    let currentdate = moment(new Date()).format("DD-MM-YYYY");
    let data = await Dinner.find({
      phone_number: body.phone_number,
      $and: [
        {
          $and: [
            {
              order_type: "waitlist",
            },
            {
              dinner_status: "Nonseated",
            },
          ],
        },
      ],
      booking_date: currentdate,
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.call_twilio = (req, res, next) => {
  const body = req.body;
  client.calls.create(
    {
      url: "http://demo.twilio.com/docs/voice.xml",
      to: body.mobileNo,
      from: "+14152003837",
    },
    (err, call) => {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(call.sid);
      }
    }
  );
};
exports.get_walkin = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      user_id: id,
      order_type: "walkin",
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

//new changes

exports.get_user_history_completed = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      user_id: id,
      $or: [
        {
          dinner_status: "Completed",
        },
        {
          dinner_status: "Cancelled",
        },
      ],
    });
    let basic = data.reverse();
    return res.status(200).json(basic);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_user_history_pending = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await Dinner.find({
      user_id: id,
      $or: [
        {
          dinner_status: "Nonseated",
        },
        {
          dinner_status: "Seated",
        },
        {
          dinner_status: "Pending",
        },
      ],
    });
    let basic = data.reverse();
    return res.status(200).json(basic);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

/**********************
 * 10-01-2020
 **********************/
exports.user_reservation = async (req, res, next) => {
  console.log('userres', req.body.hour)
  console.log("realtime", req.body.time)
  const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
  try {
    
    
    let reserve = await Reservation.find({
      vendor_id: id,
      Date: req.body.Date,
      mainTime: req.body.hour
    })
    var dinner = null;
    console.log('reserve', reserve)
    if(reserve.length === 0){
      console.log('if')

      dinner = new Reservation({
        mainTime: req.body.hour,
        vendor_id: req.body.vendor_id,
        Date: req.body.Date ? req.body.Date : "",
        reservation: [{
        user_id: id,
        order_type: "reservation",
        entry_point: req.body.entry_point ? req.body.entry_point : "",
        name: req.body.name ? req.body.name : "",
        requestNumber: Math.floor(Math.random() * 10000000000),
        phone_number: req.body.phone_number ? req.body.phone_number : "",
        adults: req.body.adults ? req.body.adults : 0,
        kids: req.body.kids ? req.body.kids : 0,
        highchair: req.body.highchair ? req.body.highchair : 0,
        handicap: req.body.handicap ? req.body.handicap : 0,
        seating_preference: req.body.seating_preference
          ? req.body.seating_preference
          : "",
        total: parseInt(req.body.adults) + parseInt(req.body.kids),
        wait_time: req.body.wait_time ? req.body.wait_time : "",
        special_occassion: req.body.special_occassion
          ? req.body.special_occassion
          : "",
        Request_Date: Date.now(),
        Seating_Date: Date.now(),
        
        time: req.body.time ? req.body.time : "",
        ETA: req.body.ETA ? req.body.ETA : "",
        restaurant_name: req.body.restaurant_name ? req.body.restaurant_name : "",
        dinner_status: "Pending",
        status: "Pending",
        register_date: Date.now(),
        token: req.body.token ? req.body.token : "",
      }]});
      let newdinner = await dinner.save();
      console.log('saveddata', newdinner)
      if (newdinner) {
        next();
      }
    } else {
      console.log('else')
      await Reservation.findByIdAndUpdate({
        _id: reserve[0]._id,
        vendor_id: id,
        Date: req.body.Date,
        mainTime: req.body.hour
      },
      {
        $push: {
          reservation: {
            user_id: id,
            order_type: "reservation",
            entry_point: req.body.entry_point ? req.body.entry_point : "",
            name: req.body.name ? req.body.name : "",
            requestNumber: Math.floor(Math.random() * 10000000000),
            phone_number: req.body.phone_number ? req.body.phone_number : "",
            adults: req.body.adults ? req.body.adults : 0,
            kids: req.body.kids ? req.body.kids : 0,
            highchair: req.body.highchair ? req.body.highchair : 0,
            handicap: req.body.handicap ? req.body.handicap : 0,
            seating_preference: req.body.seating_preference
              ? req.body.seating_preference
              : "",
            total: parseInt(req.body.adults) + parseInt(req.body.kids),
            wait_time: req.body.wait_time ? req.body.wait_time : "",
            special_occassion: req.body.special_occassion
              ? req.body.special_occassion
              : "",
            Request_Date: Date.now(),
            Seating_Date: Date.now(),
            
            time: req.body.time ? req.body.time : "",
            ETA: req.body.ETA ? req.body.ETA : "",
            restaurant_name: req.body.restaurant_name ? req.body.restaurant_name : "",
            dinner_status: "Pending",
            status: "Pending",
            register_date: Date.now(),
            token: req.body.token ? req.body.token : "",
          }
        }
      }
      ).then(()=>{
        next();
      }).catch(err=>{
        
      })
      
      
      
    }
    
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.user_reservation_sms_twilio = (req, res, next) => {
  const body = req.body;
  client.messages
    .create({
      body:
        "Yes! We've received your reservation request. Your request is pending confirmation by the restaurant.",
      messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
      form: "+14152003837",
      to: body.phone_number,
    })
    .then((message) => res.json(message.sid));
};

exports.push = async (req, res, next) => {
  const body = req.body;
  messagepush = {
    notification: {
      title: "Amealio App",
      body: body.message,
    },
    token: body.token,
  };
};

// exports.get_reservation_data = async (req, res, next) => {
//   try {
//     const body = req.body;
//     const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
//     let data = await Dinner.find({
//       vendor_id: id,
//       $or: [
//         {
//           reservation_status: "Pending",
//         },
//         {
//           dinner_status: "Nonseated",
//         },
//       ],
//       Date: body.Date,
//     });
//     return res.status(200).json(data);
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.get_reservation_data = async (req, res, next) => {
  console.log('getreservationdata')
  try {
    const body = req.body;
    let reservation_day_limit = await subscription.findOne({
      vendor_id: body.vendor_id,
    });
    let days = reservation_day_limit.minimum_lead_time;
    let i;
    let record = [];
    let currentdate = moment(new Date()).format("DD-MM-YYYY");
    let nextday = moment(currentdate, "DD-MM-YYYY")
      .add(days, "days")
      .format("DD-MM-YYYY");
    // console.log("next",nextday);
    // console.log("curre",currentdate);
    let data = await Dinner.find({
      phone_number: body.phone_number,
      dinner_status: "Nonseated",
      $and: [
        {
          $or: [
            {
              status: "Pending",
            },
            {
              status: "Accepted",
            },
          ],
        },
      ],
    });
    for (i = 0; i < data.length; i++) {
      if (
        moment(data[i].Date, "DD-MM-YYYY").isBetween(
          moment(new Date(), "DD-MM-YYYY"),
          moment(nextday, "DD-MM-YYYY"),
          "days",
          "[)"
        )
      ) {
        record.push(data[i]);
      } else {
        console.log(false);
      }
    }

    return res.status(200).json(record);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.update_wait_time = async (req, res, next) => {
  try {
    const body = req.body;
    var newdate = moment(new Date(), "DD/MM/YYYY hh:mm:ss A")
      .add(body.wait_time * 60, "seconds")
      .format("DD/MM/YYYY hh:mm:ss A");
    let updateDinnerWaitTime = await Dinner.findOneAndUpdate(
      {
        _id: body._id,
      },
      {
        $set: {
          wait_time: newdate,
        },
      }
    );
    if (updateDinnerWaitTime) {
      return res.status(200).json({ message: "Success" });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.update_dinner_info = async (req, res, next) => {
  try {
    const body = req.body;
    let updateDinnerInfo = await Dinner.findOneAndUpdate(
      {
        _id: body._id,
      },
      {
        $set: {
          name: body.name ? body.name : "",
          phone_number: body.phone_number ? body.phone_number : "",
          adults: body.adults ? body.adults : " ",
          kids: body.kids ? body.kids : " ",
          highchair: body.highchair ? body.highchair : " ",
          handicap: body.handicap ? body.handicap : " ",
          seating_preference: body.seating_preference
            ? body.seating_preference
            : "",
          total: parseInt(body.adults) + parseInt(body.kids),
          special_occassion: body.special_occassion
            ? body.special_occassion
            : "",
          booking_date: moment(new Date()).format("DD-MM-YYYY"),
          dinner_status: "Nonseated",
          reservation_status: "Nonseated",
          token: body.token ? body.token : "",
        },
      }
    );
    if (updateDinnerInfo) {
      return res.status(200).json({ message: "Success" });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

//changes Data: 08-04-2020 07:11pm
