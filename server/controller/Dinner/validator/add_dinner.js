const Validator = require('validator');
const isEmpty = require('../../../utils/isEmpty');

module.exports = function validateRegisterInput(data){
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name: ''; 
  data.phone_number = !isEmpty(data.phone_number) ? data.phone_number: '';
  data.adults = !isEmpty(data.adults) ? data.adults: '';
  data.kids = !isEmpty(data.kids) ? data.kids: '';
  data.special_occassion = !isEmpty(data.special_occassion) ? data.special_occassion: '';

  // First name

  if(!Validator.isLength(data.name, { min:2, max:30 })){
    errors.name = 'Name must be between 2 and 30 characters';
  }

  if(Validator.isEmpty(data.name)){
      errors.name = 'Name is required';
  }

  //Special_occassion
    if(!Validator.isLength(data.special_occassion, { min:4, max:30 })){
        errors.special_occassion = 'Special Occassion must be between 4 and 30 characters';
      }
    
      if(Validator.isEmpty(data.special_occassion)){
          errors.special_occassion = 'Special Occassion is required';
      } 

  // Kids

  if(Validator.isEmpty(data.kids)){
      errors.kids = 'Kids is required';
  } 
    // Adults

    if(Validator.isEmpty(data.adults)){
        errors.adults = 'Adults is required';
    } 


// Mobile no

  if(!Validator.isLength(data.phone_number, { min:10, max:10 })){
    errors.phone_number = 'Mobile no is in valid';
  }

  if(Validator.isEmpty(data.phone_number)){
    errors.phone_number = 'mobile no is required';
  }





  return{
    errors,
    isValid: isEmpty(errors)
  }
}