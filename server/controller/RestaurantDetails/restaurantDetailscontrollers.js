const User = require("../../models/User.model");
const RestaurantDetails = require("../../models/RestaurantMapDetails");
const ReviewRating = require("../../models/ReviewRating.model");
const default_time = require("../../models/default");
const RestaurantDetailsValidation = require("./validator/restaurantValidator");
const RestaurantTypeDetails = require("../../models/RestaurantDetails");
const RestaurantTime = require("../../models/RestaurantTime.model");
const RestaurantFeatures = require("../../models/RestaurantFeatures");
const Services = require("../../models/ServiceDetails");
const Subscription = require("../../models/subscription_model");
const CusineFeatureOne = require("../../models/CusineFeaturesOne");
const CusineFeatureTwo = require("../../models/CusineFeatureTwo");
const Favourite = require("../../models/Favourite.model");
const Person = require("../../models/Person.model");
const KycBank = require("../../models/KycBank.model");
const Review = require("../../models/Review.model");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys").secretIOkey;
const axios = require("axios");
const geolib = require("geolib");
const Moment = require("moment");
const Countdown = require("countdown-js");

exports.create_new_restaurant_details = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    const body = req.body;
    const { errors, isValid } = await RestaurantDetailsValidation(body);
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const create_new_details = new RestaurantDetails({
        vendor_id: id,
        is_registered_with_google: body.is_registered_with_google,
        google_place_id: body.google_place_id ? body.google_place_id : "",
        restaurant_name: body.restaurant_name ? body.restaurant_name : "",
        restaurant_description: body.restaurant_description
          ? body.restaurant_description
          : "",
        restaurant_area: body.restaurant_area ? body.restaurant_area : "",
        restaurant_city: body.restaurant_city ? body.restaurant_city : "",
        restaurant_mobile_number: body.restaurant_mobile_number
          ? body.restaurant_mobile_number
          : "",
        restaurant_email: body.restaurant_email ? body.restaurant_email : "",
        restaurant_address: body.restaurant_address
          ? body.restaurant_address
          : "",
        lat: body.lat ? body.lat : "",
        lon: body.lon ? body.lon : "",
        offer_active: false,
      });
      let saveNewData = await create_new_details.save();
      if (saveNewData) {
        return res.status(200).json({
          message: "New Restaurant Details Created",
        });
      }
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
// exports.get_my_restaurant_details = async (req, res, next) => {
//   try {
//     const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
//     let data = await RestaurantDetails.findOne({
//       vendor_id: id,
//     });
//     return res.status(200).json(data);
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.get_my_restaurant_details = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let subscription_data = await Subscription.findOne({
      vendor_id: id,
    });
    let data = await RestaurantDetails.findOne({
      vendor_id: id,
    });
    let arrayData = {
      vendor_id: id,
      is_registered_with_google: data.is_registered_with_google,
      google_place_id: data.google_place_id,
      restaurant_name: data.restaurant_name,
      restaurant_description: data.restaurant_description,
      restaurant_area: data.restaurant_area,
      restaurant_city: data.restaurant_city,
      restaurant_mobile_number: data.restaurant_mobile_number,
      restaurant_email: data.restaurant_email,
      restaurant_address: data.restaurant_address,
      lat: data.lat,
      lon: data.lon,
      offer_active: data.offer_active,
      maximum_days: subscription_data.maximum_days,
    };
    return res.status(200).json(arrayData);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

// exports.get_my_restaurant_details = async (req, res, next) => {
//   try {
//     let myRestaurant = [];
//     const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
//     let subscription_data = await Subscription.findOne({
//       vendor_id: id,
//     });
//     let data = await RestaurantDetails.findOne({
//       vendor_id: id,
//     });
//     let arrayData = {
//       is_registered_with_google: data.is_registered_with_google,
//       google_place_id: data.google_place_id,
//       restaurant_name: data.restaurant_name,
//       restaurant_description: data.restaurant_description,
//       restaurant_area: data.restaurant_area,
//       restaurant_city: data.restaurant_city,
//       restaurant_mobile_number: data.restaurant_mobile_number,
//       restaurant_email: data.restaurant_email,
//       restaurant_address: data.restaurant_address,
//       lat: data.lat,
//       lon: data.lon,
//       offer_active: data.offer_active,
//       maximum_days: subscription_data.maximum_days,
//     };
//     myRestaurant.push(arrayData);
//     return res.status(200).json(myRestaurant);
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.update_new_restaurant_details = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let new_restaurant_details = await RestaurantDetails.findOneAndUpdate(
      {
        vendor_id: id,
      },
      {
        $set: {
          is_registered_with_google: body.is_registered_with_google,
          google_place_id: body.google_place_id ? body.google_place_id : "",
          restaurant_name: body.restaurant_name ? body.restaurant_name : "",
          restaurant_description: body.restaurant_description
            ? body.restaurant_description
            : "",
          restaurant_area: body.restaurant_area ? body.restaurant_area : "",
          restaurant_city: body.restaurant_city ? body.restaurant_city : "",
          restaurant_mobile_number: body.restaurant_mobile_number
            ? body.restaurant_mobile_number
            : "",
          restaurant_email: body.restaurant_email ? body.restaurant_email : "",
          restaurant_address: body.restaurant_address
            ? body.restaurant_address
            : "",
          lat: body.lat ? body.lat : "",
          lon: body.lon ? body.lon : "",
        },
      }
    );
    if (new_restaurant_details) {
      res.status(200).json({
        message: "Restaurant Details Successfully Updated..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.create_new_restaurant_type = async (req, res, next) => {
  try {
    const body = req.body;
    const create_new_details = new RestaurantTypeDetails({
      vendor_id: body.vendor_id,
      selected_restaurant_type: body.selected_restaurant_type
        ? body.selected_restaurant_type
        : [],
      primary_restaurant_type: body.primary_restaurant_type
        ? body.primary_restaurant_type
        : "",
      selected_dress_code_type: body.selected_dress_code_type
        ? body.selected_dress_code_type
        : [],
      primary_dress_code_type: body.primary_dress_code_type
        ? body.primary_dress_code_type
        : "",
      selected_payment_method_type: body.selected_payment_method_type
        ? body.selected_payment_method_type
        : [],
      primary_payment_method_type: body.primary_payment_method_type
        ? body.primary_payment_method_type
        : "",
      cost: body.cost ? body.cost : "",
      revenue: body.revenue ? body.revenue : "",
      restaurant_logo: body.restaurant_logo ? body.restaurant_logo : "",
      restaurant_photo: body.restaurant_photo ? body.restaurant_photo : [],
    });
    let saveNewData = await create_new_details.save();
    if (saveNewData) {
      return res.status(200).json({
        message: "New Restaurant Details Created",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_restaurant_Type = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await RestaurantTypeDetails.findOne({
      vendor_id: id,
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.update_restaurant_details = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let new_restaurant_details = await RestaurantTypeDetails.findOneAndUpdate(
      {
        vendor_id: id,
      },
      {
        $set: {
          selected_restaurant_type: body.selected_restaurant_type
            ? body.selected_restaurant_type
            : [],
          primary_restaurant_type: body.primary_restaurant_type
            ? body.primary_restaurant_type
            : "",
          selected_dress_code_type: body.selected_dress_code_type
            ? body.selected_dress_code_type
            : [],
          primary_dress_code_type: body.primary_dress_code_type
            ? body.primary_dress_code_type
            : "",
          selected_payment_method_type: body.selected_payment_method_type
            ? body.selected_payment_method_type
            : [],
          primary_payment_method_type: body.primary_payment_method_type
            ? body.primary_payment_method_type
            : "",
          cost: body.cost ? body.cost : "",
          revenue: body.revenue ? body.revenue : "",
          restaurant_photo: body.restaurant_photo ? body.restaurant_photo : [],
          restaurant_logo: body.restaurant_logo ? body.restaurant_logo : "",
        },
      }
    );
    if (new_restaurant_details) {
      res.status(200).json({
        message: "Restaurant Details Successfully Updated..!",
      });
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.add_restaurantTime = async (req, res, next) => {
  try {
    const body = req.body;
    const create_new_restaurant_time = new RestaurantTime({
      vendor_id: body.vendor_id,
      all_seating_area_icons: body.all_seating_area_icons
        ? body.all_seating_area_icons
        : "",
      part_of_chain: body.part_of_chain,
      selectedChains: body.selectedChains,
      table_management: body.table_management,
      are_you_open_24_x_7: body.are_you_open_24_x_7,
      multiple_opening_time: body.multiple_opening_time,
      monday: body.monday ? body.monday : "",
      tuesday: body.tuesday ? body.tuesday : "",
      wednesday: body.wednesday ? body.wednesday : "",
      thursday: body.thursday ? body.thursday : "",
      friday: body.friday ? body.friday : "",
      saturday: body.saturday ? body.saturday : "",
      sunday: body.sunday ? body.sunday : "",
      selected_seating_area: body.selected_seating_area
        ? body.selected_seating_area
        : [],
      primary_seating_area: body.primary_seating_area
        ? body.primary_seating_area
        : "",
      total_seating_capacity: body.total_seating_capacity
        ? body.total_seating_capacity
        : "",
    });
    let saveNewData = await create_new_restaurant_time.save();
    if (saveNewData) {
      res.status(200).json({
        message: "Restaurant Time Details Successfully Added..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_restaurant_Time = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let data = await RestaurantTime.findOne({
      vendor_id: id,
    });
    return res.status(200).json(data);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_restaurant_Time_user = async (req, res, next) => {
  try {
    const body = req.body;
    let timeDetailsArray = [];
    let data = await RestaurantTime.findOne({
      vendor_id: body.vendor_id,
    });
    var Arraydata = {
      _id: data._id,
      vendor_id: data.vendor_id,
      are_you_open_24_x_7: data.are_you_open_24_x_7,
      multiple_opening_time: data.multiple_opening_time,
      friday: data.friday,
      monday: data.monday,
      saturday: data.saturday,
      sunday: data.sunday,
      thursday: data.thursday,
      tuesday: data.tuesday,
      wednesday: data.wednesday,
    };
    timeDetailsArray.push(Arraydata);
    return res.status(200).json(timeDetailsArray);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.update_restaurantTime = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let update_restaurantTime = await RestaurantTime.findOneAndUpdate(
      {
        vendor_id: id,
      },
      {
        $set: {
          all_seating_area_icons: body.all_seating_area_icons
            ? body.all_seating_area_icons
            : "",
          part_of_chain: body.part_of_chain,
          selectedChains: body.selectedChains,
          table_management: body.table_management,
          are_you_open_24_x_7: body.are_you_open_24_x_7,
          multiple_opening_time: body.multiple_opening_time,
          monday: body.monday ? body.monday : "",
          tuesday: body.tuesday ? body.tuesday : "",
          wednesday: body.wednesday ? body.wednesday : "",
          thursday: body.thursday ? body.thursday : "",
          friday: body.friday ? body.friday : "",
          saturday: body.saturday ? body.saturday : "",
          sunday: body.sunday ? body.sunday : "",
          selected_seating_area: body.selected_seating_area
            ? body.selected_seating_area
            : [],
          primary_seating_area: body.primary_seating_area
            ? body.primary_seating_area
            : "",
          total_seating_capacity: body.total_seating_capacity
            ? body.total_seating_capacity
            : "",
        },
      }
    );
    if (update_restaurantTime) {
      next();
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.update_restaurant_features = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let new_restaurant_features = await RestaurantFeatures.findOneAndUpdate(
      {
        vendor_id: id,
      },
      {
        $set: {
          selected_restaurant_features_type: body.selected_restaurant_features_type
            ? body.selected_restaurant_features_type
            : [],
          primary_restaurant_features_type: body.primary_restaurant_features_type
            ? body.primary_restaurant_features_type
            : "",
          selected_restaurant_access_type: body.selected_restaurant_access_type
            ? body.selected_restaurant_access_type
            : [],
          primary_restaurant_access_type: body.primary_restaurant_access_type
            ? body.primary_restaurant_access_type
            : "",
          selected_parking_type: body.selected_parking_type
            ? body.selected_parking_type
            : [],
          primary_selecte_parking_type: body.primary_selecte_parking_type
            ? body.primary_selecte_parking_type
            : "",
          parking_description: body.parking_description
            ? body.parking_description
            : "",
        },
      }
    );
    if (new_restaurant_features) {
      res.status(200).json({
        message: "Restaurant Feature Successfully Updated..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.update_kyc_bank_details = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let kycbank = await KycBank.findOneAndUpdate(
      {
        vendor_id: id,
      },
      {
        $set: {
          account_name: body.account_name ? body.account_name : "",
          bank_name: body.bank_name ? body.bank_name : "",
          account_number: body.account_number ? body.account_number : "",
          branch_name: body.branch_name ? body.branch_name : "",
          GST_number: body.GST_number ? body.GST_number : "",
          IFSC_code: body.IFSC_code ? body.IFSC_code : "",
          PAN_number: body.PAN_number ? body.PAN_number : "",
          FSSAI_code: body.FSSAI_code ? body.FSSAI_code : "",
          imgPath: body.imgPath ? body.imgPath : [],
          currentAccount: body.currentAccount,
        },
      }
    );
    if (kycbank) {
      res.status(200).json({
        message: "Bank Details Successfully Updated..!",
      });
    } else {
      res.status(400).json({
        message: "Bank Details Not Successfully Updated..!",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

exports.get_all_details = async (req, res, next) => {
  try {
    const body = req.body;
    let array7 = [];
    let array2 = [];
    var d = new Date();
    let currentDay = Moment(new Date()).format("dddd").toLowerCase();
    var days = [
      "sunday",
      "monday",
      "tuesday",
      "wednesday",
      "thursday",
      "friday",
      "saturday",
    ];
    let index = days.indexOf(currentDay);
    let check_index = index;
    //
    //let all_data=await Promise.all([])
    let review = await Review.find();
    let user_main = await User.find();
    let default_time1 = await default_time.find();
    let RestaurantDetails1 = await RestaurantDetails.find();
    let Restaurant_time = await RestaurantTime.find();
    let RestaurantFeatures1 = await RestaurantFeatures.find();
    let RestaurantTypeDetails1 = await RestaurantTypeDetails.find();
    let Services1 = await Services.find();
    let Subscription1 = await Subscription.find();
    let CusineFeatureOne1 = await CusineFeatureOne.find();
    let CusineFeatureTwo1 = await CusineFeatureTwo.find();
    var Dayy = days[d.getDay()];
    for (let i = 0; i < Subscription1.length; i++) {
      let openingTime = " ",
        closingTime = " ";
      if (Restaurant_time[i].are_you_open_24_x_7 == false) {
        let {
          open,
          main_opening_time,
          main_closing_time,
          breakfast,
          lunch,
          dinner,
        } = Restaurant_time[i][currentDay];
        if (open == true) {
          openingTime = main_opening_time;
          closingTime = main_closing_time;
          if (openingTime == "") {
            if (
              breakfast.closing_time > Moment().format("HH:mm") &&
              breakfast.open == true
            ) {
              openingTime = ("Found breakfast", breakfast.opening_time);
              closingTime = ("Found breakfast", breakfast.closing_time);
            } else if (
              lunch.closing_time > Moment().format("HH:mm") &&
              lunch.open == true
            ) {
              openingTime = ("Found lunch", lunch.opening_time);
              closingTime = ("Found lunch", lunch.closing_time);
            } else if (
              dinner.closing_time > Moment().format("HH:mm") &&
              dinner.open == true
            ) {
              openingTime = ("Found dinner", dinner.opening_time);
              closingTime = ("Found dinner", dinner.closing_time);
            } else {
              check_index++;
              while (check_index < days.length) {
                let is_open =
                  Restaurant_time[i][days[check_index]].open == true;
                if (is_open) {
                  if (
                    Restaurant_time[i][days[check_index]].breakfast.open == true
                  ) {
                    openingTime =
                      ("Opens at",
                      days[check_index] +
                        " " +
                        Restaurant_time[i][days[check_index]].breakfast
                          .opening_time);
                    closingTime =
                      ("Opens at",
                      days[check_index] +
                        " " +
                        Restaurant_time[i][days[check_index]].breakfast
                          .closing_time);
                    check_index = days.length;
                  } else if (
                    Restaurant_time[i][days[check_index]].lunch.open == true
                  ) {
                    openingTime =
                      ("Opens at",
                      Restaurant_time[i][days[check_index]].lunch.opening_time);
                    closingTime =
                      ("Opens at",
                      Restaurant_time[i][days[check_index]].lunch.closing_time);
                    check_index = days.length;
                  } else if (
                    Restaurant_time[i][days[check_index]].dinner.open == true
                  ) {
                    openingTime =
                      ("Opens at",
                      Restaurant_time[i][days[check_index]].dinner
                        .opening_time);
                    closingTime =
                      ("Opens at",
                      Restaurant_time[i][days[check_index]].dinner
                        .closing_time);
                    check_index = days.length;
                  }
                }
                if (check_index == index) {
                  break;
                }
                if (check_index == 7) check_index = 0;
                break;
              }
            }
          }
        } else {
          while (check_index < days.length) {
            let is_open = Restaurant_time[i][days[check_index]].open == true;
            if (is_open) {
              if (
                Restaurant_time[i][days[check_index]].breakfast.open == true
              ) {
                openingTime =
                  ("Opens at",
                  days[check_index] +
                    " " +
                    Restaurant_time[i][days[check_index]].breakfast
                      .opening_time);
                closingTime =
                  ("Opens at",
                  days[check_index] +
                    " " +
                    Restaurant_time[i][days[check_index]].breakfast
                      .closing_time);
                check_index = days.length;
              } else if (
                Restaurant_time[i][days[check_index]].lunch.open == true
              ) {
                openingTime =
                  ("Opens at",
                  Restaurant_time[i][days[check_index]].lunch.opening_time);
                closingTime =
                  ("Opens at",
                  Restaurant_time[i][days[check_index]].lunch.closing_time);
                check_index = days.length;
              } else if (
                Restaurant_time[i][days[check_index]].dinner.open == true
              ) {
                openingTime =
                  ("Opens at",
                  Restaurant_time[i][days[check_index]].dinner.opening_time);
                closingTime =
                  ("Opens at",
                  Restaurant_time[i][days[check_index]].dinner.closing_time);
                check_index = days.length;
              }
            }
            check_index++;

            if (check_index == index) {
              break;
            }
            if (check_index == 7) check_index = 0;
          }
        }
      }
      var SubscriptionDetailsArray = {
        review_count: review[i].review,
        rating_count: review[i].rating,
        restaurant_current_status: user_main[i].restaurant_current_status,
        are_you_open_24_x_7: Restaurant_time[i].are_you_open_24_x_7,
        multiple_opening_time: Restaurant_time[i].multiple_opening_time,
        offer_active: RestaurantDetails1[i].offer_active,
        isOpen: Restaurant_time[i][Dayy].open,
        openingTime,
        closingTime,
        selected_restaurant_type:
          RestaurantTypeDetails1[i].selected_restaurant_type,

        google_place_id: RestaurantDetails1[i].google_place_id,
        selected_seating_area: Restaurant_time[i].selected_seating_area,
        default_time: default_time1[i].default_time,
        servicePage: Subscription1[i].servicePage,
        highchair: Subscription1[i].highchair,
        handicap: Subscription1[i].handicap,
        curb_side: Subscription1[i].curb_side,
        self_serve: Subscription1[i].self_serve,
        skip_line: Subscription1[i].skip_line,
        take_away: Subscription1[i].take_away,
        walk_in: Subscription1[i].walk_in,
        waitlist: Subscription1[i].waitlist,
        reservation: Subscription1[i].reservation,
        restaurant_name: RestaurantDetails1[i].restaurant_name,
        lat: RestaurantDetails1[i].lat,
        lon: RestaurantDetails1[i].lon,
        restaurant_mobile_number:
          RestaurantDetails1[i].restaurant_mobile_number,
        has_admin_approved: user_main[i].has_admin_approved,
        closing_time: RestaurantDetails1[i].closing_time,
        wait_time: RestaurantDetails1[i].wait_time,
        total_reviews: RestaurantDetails1[i].total_reviews,
        rating_star: RestaurantDetails1[i].rating_star,
        restaurant_address: RestaurantDetails1[i].restaurant_address,
        restaurant_description: RestaurantDetails1[i].restaurant_description,
        cost: RestaurantTypeDetails1[i].cost,
        selected_dress_code_type:
          RestaurantTypeDetails1[i].selected_dress_code_type,
        primary_payment_method_type:
          RestaurantTypeDetails1[i].primary_payment_method_type,
        selected_payment_method_type:
          RestaurantTypeDetails1[i].selected_payment_method_type,

        restaurant_logo: RestaurantTypeDetails1[i].restaurant_logo,
        primary_restaurant_type:
          RestaurantTypeDetails1[i].primary_restaurant_type,
        restaurant_photo: RestaurantTypeDetails1[i].restaurant_photo,
        vendor_id: Services1[i].vendor_id,
        primary_restaurant_features_type:
          RestaurantFeatures1[i].primary_restaurant_features_type,
        selected_restaurant_features_type:
          RestaurantFeatures1[i].selected_restaurant_features_type,
        primary_restaurant_access_type:
          RestaurantFeatures1[i].primary_restaurant_access_type,
        selected_restaurant_access_type:
          RestaurantFeatures1[i].selected_restaurant_access_type,
        selected_cusines_types: RestaurantFeatures1[i].selected_cusines_types,
        selected_parking_type: RestaurantFeatures1[i].selected_parking_type,
        primary_selecte_parking_type:
          RestaurantFeatures1[i].primary_selecte_parking_type,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_item_type: CusineFeatureOne1[i].primary_food_item_type,
        selected_food_items_type: CusineFeatureOne1[i].selected_food_items_type,
        nutri_info: CusineFeatureOne1[i].nutri_info,
        allergy_information: CusineFeatureOne1[i].allergy_information,
        serve_liquor: CusineFeatureOne1[i].serve_liquor,
        primary_selected_icons: CusineFeatureTwo1[i].primary_selected_icons,
        selected_cusines_types: CusineFeatureTwo1[i].selected_cusines_types,
      };
      array2.push(SubscriptionDetailsArray);
    }

    for (let i = 0; i < array2.length; i++) {
      if (
        geolib.isPointWithinRadius(
          {
            latitude: body.lat,
            longitude: body.lon,
          },
          {
            latitude: array2[i].lat,
            longitude: array2[i].lon,
          },
          10000
        )
      ) {
        if (array2[i].has_admin_approved === true) {
          array7.push(array2[i]);
        }
      }
    }

    //Filter Code

    for (let f = 0; f < body.filter.length; f++) {
      let bool = ["serve_liquor", "nutri_info", "allergy_information"];

      if (bool.includes(Object.keys(body.filter[f])[0])) {
        array7 = array7.filter(
          (element) =>
            element[Object.keys(body.filter[f])[0]] ==
            body.filter[f][Object.keys(body.filter[f])[0]]
        );
      } else {
        array7 = array7
          .filter((element) =>
            element[Object.keys(body.filter[f])[0]].some(
              (subElement) =>
                subElement.title ==
                body.filter[f][Object.keys(body.filter[f])[0]]
            )
          )
          .map((element) => {
            return Object.assign({}, element, {
              [Object.keys(body.filter[f])[0]]:
                element[Object.keys(body.filter[f])[0]],
            });
          });
      }
    }
    //Sorting

    //Default Sorting is Distance
    if (body.sort == "Relevance") {
      array7 = geolib.orderByDistance(
        { latitude: body.lat, longitude: body.lon },
        array7
      );
    }

    //Sort by Cost
    if (body.sort == "Cost for Two") {
      array7.sort(function (a, b) {
        let aKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: a.lat,
              longitude: a.lon,
            }
          ) / 1000
        );
        let bKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: b.lat,
              longitude: b.lon,
            }
          ) / 1000
        );
        if (a.cost == b.cost) {
          return aKm - bKm;
        } else {
          return a.cost - b.cost;
        }
      });
    }

    //Sort By Wait
    if (body.sort == "Wait Time") {
      array7.sort(function (a, b) {
        let aKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: a.lat,
              longitude: a.lon,
            }
          ) / 1000
        );
        let bKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: b.lat,
              longitude: b.lon,
            }
          ) / 1000
        );
        if (a.default_time == b.default_time) {
          return aKm - bKm;
        } else {
          return a.default_time - b.default_time;
        }
      });
    }

    //Sort By Ratings
    if (body.sort == "Ratings") {
      array7.sort(function (a, b) {
        let aKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: a.lat,
              longitude: a.lon,
            }
          ) / 1000
        );
        let bKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: b.lat,
              longitude: b.lon,
            }
          ) / 1000
        );
        if (a.rating_count == b.rating_count) {
          return aKm - bKm;
        } else {
          return b.rating_count - a.rating_count;
        }
      });
    }

    //Sort By Has Ratings
    if (body.sort == "Has Ratings") {
      array7.sort(function (a, b) {
        let aKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: a.lat,
              longitude: a.lon,
            }
          ) / 1000
        );
        let bKm = Math.round(
          geolib.getPreciseDistance(
            {
              latitude: body.lat,
              longitude: body.lon,
            },
            {
              latitude: b.lat,
              longitude: b.lon,
            }
          ) / 1000
        );
        if (a.review_count == b.review_count) {
          return aKm - bKm;
        } else {
          return b.review_count - a.review_count;
        }
      });
    }

    return res.status(200).json(array7);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};

// exports.get_all_details = async (req, res, next) => {
//   try {
//     const body = req.body;
//     let array7 = [];
//     let array2 = [];
//     var d = new Date();
//     let currentDay = Moment(new Date()).format("dddd").toLowerCase();
//     var days = [
//       "sunday",
//       "monday",
//       "tuesday",
//       "wednesday",
//       "thursday",
//       "friday",
//       "saturday",
//     ];
//     let index = days.indexOf(currentDay);
//     let check_index = index;
//     //
//     //let all_data=await Promise.all([])
//     let review = await Review.find();
//     let user_main = await User.find();
//     let default_time1 = await default_time.find();
//     let RestaurantDetails1 = await RestaurantDetails.find();
//     let Restaurant_time = await RestaurantTime.find();
//     let RestaurantFeatures1 = await RestaurantFeatures.find();
//     let RestaurantTypeDetails1 = await RestaurantTypeDetails.find();
//     let Services1 = await Services.find();
//     let Subscription1 = await Subscription.find();
//     let CusineFeatureOne1 = await CusineFeatureOne.find();
//     let CusineFeatureTwo1 = await CusineFeatureTwo.find();
//     var Dayy = days[d.getDay()];
//     for (let i = 0; i < Subscription1.length; i++) {
//       let openingTime = " ",
//         closingTime = " ";
//       if (Restaurant_time[i].are_you_open_24_x_7 == false) {
//         let {
//           open,
//           main_opening_time,
//           main_closing_time,
//           breakfast,
//           lunch,
//           dinner,
//         } = Restaurant_time[i][currentDay];
//         if (open == true) {
//           openingTime = main_opening_time;
//           closingTime = main_closing_time;
//           if (openingTime == "") {
//             if (
//               breakfast.closing_time > Moment().format("HH:mm") &&
//               breakfast.open == true
//             ) {
//               openingTime = ("Found breakfast", breakfast.opening_time);
//               closingTime = ("Found breakfast", breakfast.closing_time);
//             } else if (
//               lunch.closing_time > Moment().format("HH:mm") &&
//               lunch.open == true
//             ) {
//               openingTime = ("Found lunch", lunch.opening_time);
//               closingTime = ("Found lunch", lunch.closing_time);
//             } else if (
//               dinner.closing_time > Moment().format("HH:mm") &&
//               dinner.open == true
//             ) {
//               openingTime = ("Found dinner", dinner.opening_time);
//               closingTime = ("Found dinner", dinner.closing_time);
//             } else {
//               check_index++;
//               while (check_index < days.length) {
//                 let is_open =
//                   Restaurant_time[i][days[check_index]].open == true;
//                 if (is_open) {
//                   if (
//                     Restaurant_time[i][days[check_index]].breakfast.open == true
//                   ) {
//                     openingTime =
//                       ("Opens at",
//                       days[check_index] +
//                         " " +
//                         Restaurant_time[i][days[check_index]].breakfast
//                           .opening_time);
//                     closingTime =
//                       ("Opens at",
//                       days[check_index] +
//                         " " +
//                         Restaurant_time[i][days[check_index]].breakfast
//                           .closing_time);
//                     check_index = days.length;
//                   } else if (
//                     Restaurant_time[i][days[check_index]].lunch.open == true
//                   ) {
//                     openingTime =
//                       ("Opens at",
//                       Restaurant_time[i][days[check_index]].lunch.opening_time);
//                     closingTime =
//                       ("Opens at",
//                       Restaurant_time[i][days[check_index]].lunch.closing_time);
//                     check_index = days.length;
//                   } else if (
//                     Restaurant_time[i][days[check_index]].dinner.open == true
//                   ) {
//                     openingTime =
//                       ("Opens at",
//                       Restaurant_time[i][days[check_index]].dinner
//                         .opening_time);
//                     closingTime =
//                       ("Opens at",
//                       Restaurant_time[i][days[check_index]].dinner
//                         .closing_time);
//                     check_index = days.length;
//                   }
//                 }
//                 if (check_index == index) {
//                   break;
//                 }
//                 if (check_index == 7) check_index = 0;
//                 break;
//               }
//             }
//           }
//         } else {
//           while (check_index < days.length) {
//             let is_open = Restaurant_time[i][days[check_index]].open == true;
//             if (is_open) {
//               if (
//                 Restaurant_time[i][days[check_index]].breakfast.open == true
//               ) {
//                 openingTime =
//                   ("Opens at",
//                   days[check_index] +
//                     " " +
//                     Restaurant_time[i][days[check_index]].breakfast
//                       .opening_time);
//                 closingTime =
//                   ("Opens at",
//                   days[check_index] +
//                     " " +
//                     Restaurant_time[i][days[check_index]].breakfast
//                       .closing_time);
//                 check_index = days.length;
//               } else if (
//                 Restaurant_time[i][days[check_index]].lunch.open == true
//               ) {
//                 openingTime =
//                   ("Opens at",
//                   Restaurant_time[i][days[check_index]].lunch.opening_time);
//                 closingTime =
//                   ("Opens at",
//                   Restaurant_time[i][days[check_index]].lunch.closing_time);
//                 check_index = days.length;
//               } else if (
//                 Restaurant_time[i][days[check_index]].dinner.open == true
//               ) {
//                 openingTime =
//                   ("Opens at",
//                   Restaurant_time[i][days[check_index]].dinner.opening_time);
//                 closingTime =
//                   ("Opens at",
//                   Restaurant_time[i][days[check_index]].dinner.closing_time);
//                 check_index = days.length;
//               }
//             }
//             check_index++;

//             if (check_index == index) {
//               break;
//             }
//             if (check_index == 7) check_index = 0;
//           }
//         }
//       }
//       var SubscriptionDetailsArray = {
//         review_count: review[i].review,
//         rating_count: review[i].rating,
//         restaurant_current_status: user_main[i].restaurant_current_status,
//         are_you_open_24_x_7: Restaurant_time[i].are_you_open_24_x_7,
//         multiple_opening_time: Restaurant_time[i].multiple_opening_time,
//         offer_active: RestaurantDetails1[i].offer_active,
//         isOpen: Restaurant_time[i][Dayy].open,
//         openingTime,
//         closingTime,
//         selected_restaurant_type:
//           RestaurantTypeDetails1[i].selected_restaurant_type,

//         google_place_id: RestaurantDetails1[i].google_place_id,
//         selected_seating_area: Restaurant_time[i].selected_seating_area,
//         default_time: default_time1[i].default_time,
//         servicePage: Subscription1[i].servicePage,
//         highchair: Subscription1[i].highchair,
//         handicap: Subscription1[i].handicap,
//         curb_side: Subscription1[i].curb_side,
//         self_serve: Subscription1[i].self_serve,
//         skip_line: Subscription1[i].skip_line,
//         take_away: Subscription1[i].take_away,
//         walk_in: Subscription1[i].walk_in,
//         waitlist: Subscription1[i].waitlist,
//         reservation: Subscription1[i].reservation,
//         restaurant_name: RestaurantDetails1[i].restaurant_name,
//         lat: RestaurantDetails1[i].lat,
//         lon: RestaurantDetails1[i].lon,
//         restaurant_mobile_number:
//           RestaurantDetails1[i].restaurant_mobile_number,
//         has_admin_approved: user_main[i].has_admin_approved,
//         closing_time: RestaurantDetails1[i].closing_time,
//         wait_time: RestaurantDetails1[i].wait_time,
//         total_reviews: RestaurantDetails1[i].total_reviews,
//         rating_star: RestaurantDetails1[i].rating_star,
//         restaurant_address: RestaurantDetails1[i].restaurant_address,
//         restaurant_description: RestaurantDetails1[i].restaurant_description,
//         cost: RestaurantTypeDetails1[i].cost,
//         selected_dress_code_type:
//           RestaurantTypeDetails1[i].selected_dress_code_type,
//         primary_payment_method_type:
//           RestaurantTypeDetails1[i].primary_payment_method_type,
//         selected_payment_method_type:
//           RestaurantTypeDetails1[i].selected_payment_method_type,

//         restaurant_logo: RestaurantTypeDetails1[i].restaurant_logo,
//         primary_restaurant_type:
//           RestaurantTypeDetails1[i].primary_restaurant_type,
//         restaurant_photo: RestaurantTypeDetails1[i].restaurant_photo,
//         vendor_id: Services1[i].vendor_id,
//         primary_restaurant_features_type:
//           RestaurantFeatures1[i].primary_restaurant_features_type,
//         selected_restaurant_features_type:
//           RestaurantFeatures1[i].selected_restaurant_features_type,
//         primary_restaurant_access_type:
//           RestaurantFeatures1[i].primary_restaurant_access_type,
//         selected_restaurant_access_type:
//           RestaurantFeatures1[i].selected_restaurant_access_type,
//         selected_cusines_types: RestaurantFeatures1[i].selected_cusines_types,
//         selected_parking_type: RestaurantFeatures1[i].selected_parking_type,
//         primary_selecte_parking_type:
//           RestaurantFeatures1[i].primary_selecte_parking_type,
//         primary_food_category_icons:
//           CusineFeatureOne1[i].primary_food_category_icons,
//         primary_food_category_icons:
//           CusineFeatureOne1[i].primary_food_category_icons,
//         primary_food_item_type: CusineFeatureOne1[i].primary_food_item_type,
//         selected_food_items_type: CusineFeatureOne1[i].selected_food_items_type,
//         nutri_info: CusineFeatureOne1[i].nutri_info,
//         allergy_information: CusineFeatureOne1[i].allergy_information,
//         serve_liquor: CusineFeatureOne1[i].serve_liquor,
//         primary_selected_icons: CusineFeatureTwo1[i].primary_selected_icons,
//         selected_cusines_types: CusineFeatureTwo1[i].selected_cusines_types,
//       };
//       array2.push(SubscriptionDetailsArray);
//     }

//     for (let i = 0; i < array2.length; i++) {
//       if (
//         geolib.isPointWithinRadius(
//           {
//             latitude: body.lat,
//             longitude: body.lon,
//           },
//           {
//             latitude: array2[i].lat,
//             longitude: array2[i].lon,
//           },
//           10000
//         )
//       ) {
//         if (array2[i].has_admin_approved === true) {
//           array7.push(array2[i]);
//         }
//       }
//     }

//     //Filter Code

//     for (let f = 0; f < body.filter.length; f++) {
//       let bool = ["serve_liquor", "nutri_info", "allergy_information"];

//       if (bool.includes(Object.keys(body.filter[f])[0])) {
//         array7 = array7.filter(
//           (element) =>
//             element[Object.keys(body.filter[f])[0]] ==
//             body.filter[f][Object.keys(body.filter[f])[0]]
//         );
//       } else {
//         array7 = array7
//           .filter((element) =>
//             element[Object.keys(body.filter[f])[0]].some(
//               (subElement) =>
//                 subElement.title ==
//                 body.filter[f][Object.keys(body.filter[f])[0]]
//             )
//           )
//           .map((element) => {
//             return Object.assign({}, element, {
//               [Object.keys(body.filter[f])[0]]:
//                 element[Object.keys(body.filter[f])[0]],
//             });
//           });
//       }
//     }
//     //Sorting

//     //10-4-2020

//     //Default Sorting is Distance
//     if (body.sort == "Relevance") {
//       array7 = geolib.orderByDistance(
//         { latitude: body.lat, longitude: body.lon },
//         array7
//       );
//     }

//     //Sort by Cost
//     if (body.sort == "Cost for Two") {
//       array7.sort(function (a, b) {
//         return a.cost - b.cost;
//       });
//     }

//     //Sort By Wait
//     if (body.sort == "Wait Time") {
//       array7.sort(function (a, b) {
//         return a.default_time - b.default_time;
//       });
//     }

//     //Sort By Ratings
//     if (body.sort == "Ratings") {
//       array7.sort(function (a, b) {
//         return b.rating_count - a.rating_count;
//       });
//     }

//     //Sort By Has Ratings
//     if (body.sort == "Has Ratings") {
//       array7.sort(function (a, b) {
//         return b.review_count - a.review_count;
//       });
//     }

//     return res.status(200).json(array7);
//   } catch (err) {
//     let errors = {};
//     console.log(err);
//     errors.SERVER_ERRORS = "Internal Errors Please Try Again";
//     return res.status(500).json(errors);
//   }
// };

exports.get_superadmin_all_details = async (req, res, next) => {
  try {
    const body = req.body;
    let array2 = [];
    let user_main = await User.find();
    let default_time1 = await default_time.find();
    let RestaurantDetails1 = await RestaurantDetails.find();
    let RestaurantFeatures1 = await RestaurantFeatures.find();
    let RestaurantTime1 = await RestaurantTime.find();
    let RestaurantTypeDetails1 = await RestaurantTypeDetails.find();
    let Services1 = await Services.find();
    let Subscription1 = await Subscription.find();
    let CusineFeatureOne1 = await CusineFeatureOne.find();
    let CusineFeatureTwo1 = await CusineFeatureTwo.find();
    for (let i = 0; i < Subscription1.length; i++) {
      var Arraydata = {
        google_place_id: RestaurantDetails1[i].google_place_id,
        offer_active: RestaurantDetails1[i].offer_active,
        default_time: default_time1[i].default_time,
        servicePage: Subscription1[i].servicePage,
        curb_side: Subscription1[i].curb_side,
        self_serve: Subscription1[i].self_serve,
        skip_line: Subscription1[i].skip_line,
        take_away: Subscription1[i].take_away,
        walk_in: Subscription1[i].walk_in,
        waitlist: Subscription1[i].waitlist,
        reservation: Subscription1[i].reservation,
        restaurant_name: RestaurantDetails1[i].restaurant_name,
        restaurant_city: RestaurantDetails1[i].restaurant_city,
        lat: RestaurantDetails1[i].lat,
        lon: RestaurantDetails1[i].lon,
        restaurant_mobile_number:
          RestaurantDetails1[i].restaurant_mobile_number,
        email: user_main[i].email,
        role: user_main[i].role,
        has_admin_approved: user_main[i].has_admin_approved,
        closing_time: RestaurantDetails1[i].closing_time,
        wait_time: RestaurantDetails1[i].wait_time,
        total_reviews: RestaurantDetails1[i].total_reviews,
        rating_star: RestaurantDetails1[i].rating_star,
        restaurant_address: RestaurantDetails1[i].restaurant_address,
        restaurant_description: RestaurantDetails1[i].restaurant_description,
        cost: RestaurantTypeDetails1[i].cost,
        selected_dress_code_type:
          RestaurantTypeDetails1[i].selected_dress_code_type,
        primary_payment_method_type:
          RestaurantTypeDetails1[i].primary_payment_method_type,
        selected_payment_method_type:
          RestaurantTypeDetails1[i].selected_payment_method_type,
        restaurant_logo: RestaurantTypeDetails1[i].restaurant_logo,
        primary_restaurant_type:
          RestaurantTypeDetails1[i].primary_restaurant_type,
        restaurant_photo: RestaurantTypeDetails1[i].restaurant_photo,
        vendor_id: Services1[i].vendor_id,
        primary_restaurant_features_type:
          RestaurantFeatures1[i].primary_restaurant_features_type,
        selected_restaurant_features_type:
          RestaurantFeatures1[i].selected_restaurant_features_type,
        primary_restaurant_access_type:
          RestaurantFeatures1[i].primary_restaurant_access_type,
        selected_restaurant_access_type:
          RestaurantFeatures1[i].selected_restaurant_access_type,
        selected_cusines_types: RestaurantFeatures1[i].selected_cusines_types,
        selected_parking_type: RestaurantFeatures1[i].selected_parking_type,
        primary_selecte_parking_type:
          RestaurantFeatures1[i].primary_selecte_parking_type,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_item_type: CusineFeatureOne1[i].primary_food_item_type,
        selected_food_items_type: CusineFeatureOne1[i].selected_food_items_type,
        nutri_info: CusineFeatureOne1[i].nutri_info,
        allergy_information: CusineFeatureOne1[i].allergy_information,
        serve_liquor: CusineFeatureOne1[i].serve_liquor,
        primary_selected_icons: CusineFeatureTwo1[i].primary_selected_icons,
        selected_cusines_types: CusineFeatureTwo1[i].selected_cusines_types,
      };
      array2.push(Arraydata);
    }
    return res.status(200).json(array2);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.add_to_favourite = async (req, res, next) => {
  try {
    const body = req.body;
    const add_favourite = new Favourite({
      restaurant_name: body.restaurant_name,
      dish: body.dish,
      offer: body.offer,
    });
    let saveNewData = await add_favourite.save();
    if (saveNewData) {
      return res.status(200).json({
        message: "Favourite Restaurant Details Created",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.map = async (req, res, next) => {
  const body = req.body;
  try {
    var { data } = await axios.get(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${body.lat},${body.lon}&radius=1000&type=restaurant&keyword=cruise&key=AIzaSyBKaOKvpvylo_hx-icZWxmjtQB6WJAk1Hs`
    );
    if (data) {
      return res.status(200).json(data);
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.update_restaurantTime_data = async (req, res, next) => {
  try {
    const body = req.body;
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let update_restaurantTime = await RestaurantTime.findOneAndUpdate(
      {
        vendor_id: id,
      },
      {
        $set: {
          all_seating_area_icons: body.all_seating_area_icons
            ? body.all_seating_area_icons
            : "",
          part_of_chain: body.part_of_chain,
          selectedChains: body.selectedChains,
          are_you_open_24_x_7: body.are_you_open_24_x_7,
          multiple_opening_time: body.multiple_opening_time,
          table_management: body.table_management,
          monday: body.monday ? body.monday : "",
          tuesday: body.tuesday ? body.tuesday : "",
          wednesday: body.wednesday ? body.wednesday : "",
          thursday: body.thursday ? body.thursday : "",
          friday: body.friday ? body.friday : "",
          saturday: body.saturday ? body.saturday : "",
          sunday: body.sunday ? body.sunday : "",
          selected_seating_area: body.selected_seating_area
            ? body.selected_seating_area
            : [],
          primary_seating_area: body.primary_seating_area
            ? body.primary_seating_area
            : "",
          total_seating_capacity: body.total_seating_capacity
            ? body.total_seating_capacity
            : "",
        },
      }
    );
    if (update_restaurantTime) {
      return res.status(200).json({
        message: " Restaurant Details Updated",
      });
    }
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_restaurant_details = async (req, res, next) => {
  try {
    let array3 = [];
    const body = req.body;
    var review_rating = 0;
    let review = await ReviewRating.find({ vendor_id: body.vendor_id });
    for (let i = 0; i < review.length; i++) {
      review_rating = review_rating + review[i].rating / review.length;
    }
    let vendor_info_address = await RestaurantDetails.findOne({
      vendor_id: body.vendor_id,
    });
    let vendor_time = await default_time.findOne({ vendor_id: body.vendor_id });
    let CusineFeature = await CusineFeatureTwo.findOne({
      vendor_id: body.vendor_id,
    });
    let RestaurantType = await RestaurantTypeDetails.findOne({
      vendor_id: body.vendor_id,
    });
    let Restorant_time = await RestaurantTime.findOne({
      vendor_id: body.vendor_id,
    });
    var Arraydata = {
      restaurant_name: vendor_info_address.restaurant_name,
      rating_star: review_rating.toFixed(1),
      restaurant_logo: RestaurantType.restaurant_logo,
      restaturant_type: RestaurantType.primary_restaurant_type.title,
      closing_time_monday: Restorant_time.monday,
      closing_time_tuesday: Restorant_time.tuesday,
      closing_time_wednesday: Restorant_time.wednesday,
      closing_time_thursday: Restorant_time.thursday,
      closing_time_friday: Restorant_time.friday,
      closing_time_saturday: Restorant_time.saturday,
      closing_time_sunday: Restorant_time.sunday,
      total_review: review.length,
      cusineType: CusineFeature.primary_selected_icons.title,
      phoneNumber: vendor_info_address.restaurant_mobile_number,
      lat: vendor_info_address.lat,
      lon: vendor_info_address.lon,
      wait_time: vendor_time.default_time,
      Address: vendor_info_address.restaurant_address,
    };
    array3.push(Arraydata);
    return res.status(200).json(array3);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_all_unregister_resto = async (req, res, next) => {
  try {
    const body = req.body;
    let array7 = [];
    let array2 = [];
    let array1 = [];
    let user_main = await User.find();
    let default_time1 = await default_time.find();
    let RestaurantDetails1 = await RestaurantDetails.find();
    let RestaurantFeatures1 = await RestaurantFeatures.find();
    let RestaurantTime1 = await RestaurantTime.find();
    let RestaurantTypeDetails1 = await RestaurantTypeDetails.find();
    let Services1 = await Services.find();
    let Subscription1 = await Subscription.find();
    let CusineFeatureOne1 = await CusineFeatureOne.find();
    let CusineFeatureTwo1 = await CusineFeatureTwo.find();
    for (let i = 0; i < Subscription1.length; i++) {
      var Arraydata = {
        google_place_id: RestaurantDetails1[i].google_place_id,
        default_time: default_time1[i].default_time,
        servicePage: Subscription1[i].servicePage,
        curb_side: Subscription1[i].curb_side,
        self_serve: Subscription1[i].self_serve,
        skip_line: Subscription1[i].skip_line,
        take_away: Subscription1[i].take_away,
        walk_in: Subscription1[i].walk_in,
        waitlist: Subscription1[i].waitlist,
        reservation: Subscription1[i].reservation,
        restaurant_name: RestaurantDetails1[i].restaurant_name,
        lat: RestaurantDetails1[i].lat,
        lon: RestaurantDetails1[i].lon,
        restaurant_mobile_number:
          RestaurantDetails1[i].restaurant_mobile_number,
        has_admin_approved: user_main[i].has_admin_approved,
        closing_time: RestaurantDetails1[i].closing_time,
        wait_time: RestaurantDetails1[i].wait_time,
        total_reviews: RestaurantDetails1[i].total_reviews,
        rating_star: RestaurantDetails1[i].rating_star,
        restaurant_address: RestaurantDetails1[i].restaurant_address,
        restaurant_description: RestaurantDetails1[i].restaurant_description,
        cost: RestaurantTypeDetails1[i].cost,
        selected_dress_code_type:
          RestaurantTypeDetails1[i].selected_dress_code_type,
        primary_payment_method_type:
          RestaurantTypeDetails1[i].primary_payment_method_type,
        selected_payment_method_type:
          RestaurantTypeDetails1[i].selected_payment_method_type,
        restaurant_logo: RestaurantTypeDetails1[i].restaurant_logo,
        primary_restaurant_type:
          RestaurantTypeDetails1[i].primary_restaurant_type,
        restaurant_photo: RestaurantTypeDetails1[i].restaurant_photo,
        vendor_id: Services1[i].vendor_id,
        primary_restaurant_features_type:
          RestaurantFeatures1[i].primary_restaurant_features_type,
        selected_restaurant_features_type:
          RestaurantFeatures1[i].selected_restaurant_features_type,
        primary_restaurant_access_type:
          RestaurantFeatures1[i].primary_restaurant_access_type,
        selected_restaurant_access_type:
          RestaurantFeatures1[i].selected_restaurant_access_type,
        selected_cusines_types: RestaurantFeatures1[i].selected_cusines_types,
        selected_parking_type: RestaurantFeatures1[i].selected_parking_type,
        primary_selecte_parking_type:
          RestaurantFeatures1[i].primary_selecte_parking_type,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_item_type: CusineFeatureOne1[i].primary_food_item_type,
        selected_food_items_type: CusineFeatureOne1[i].selected_food_items_type,
        nutri_info: CusineFeatureOne1[i].nutri_info,
        allergy_information: CusineFeatureOne1[i].allergy_information,
        serve_liquor: CusineFeatureOne1[i].serve_liquor,
        primary_selected_icons: CusineFeatureTwo1[i].primary_selected_icons,
        selected_cusines_types: CusineFeatureTwo1[i].selected_cusines_types,
      };
      array2.push(Arraydata);
    }
    for (let i = 0; i < array2.length; i++) {
      if (
        geolib.isPointWithinRadius(
          { latitude: body.lat, longitude: body.lon },
          { latitude: array2[i].lat, longitude: array2[i].lon },
          10000
        )
      ) {
        array7.push(array2[i]);
      }
    }
    let { data } = await axios.get(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${body.lat},${body.lon}&radius=3000&type=restaurant&keyword=cruise&key=AIzaSyD1QCZaEI06yG2-aAooddFuuPkZ0INJ7Ag`
    );
    let array5 = [];

    array5 = data.results.filter(
      (entry1) =>
        !array7.some((entry2) => entry1.place_id === entry2.google_place_id)
    );
    return res.status(200).json(array5);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_superadmin_approved_details = async (req, res, next) => {
  try {
    let array8 = [];
    let array2 = [];
    let user_main = await User.find();
    let default_time1 = await default_time.find();
    let RestaurantDetails1 = await RestaurantDetails.find();
    let RestaurantFeatures1 = await RestaurantFeatures.find();
    let RestaurantTime1 = await RestaurantTime.find();
    let RestaurantTypeDetails1 = await RestaurantTypeDetails.find();
    let Services1 = await Services.find();
    let Subscription1 = await Subscription.find();
    let CusineFeatureOne1 = await CusineFeatureOne.find();
    let CusineFeatureTwo1 = await CusineFeatureTwo.find();
    for (let i = 0; i < Subscription1.length; i++) {
      var Arraydata = {
        google_place_id: RestaurantDetails1[i].google_place_id,
        offer_active: RestaurantDetails1[i].offer_active,
        default_time: default_time1[i].default_time,
        servicePage: Subscription1[i].servicePage,
        curb_side: Subscription1[i].curb_side,
        self_serve: Subscription1[i].self_serve,
        skip_line: Subscription1[i].skip_line,
        take_away: Subscription1[i].take_away,
        walk_in: Subscription1[i].walk_in,
        waitlist: Subscription1[i].waitlist,
        reservation: Subscription1[i].reservation,
        restaurant_name: RestaurantDetails1[i].restaurant_name,
        restaurant_city: RestaurantDetails1[i].restaurant_city,
        restaurant_email: RestaurantDetails1[i].restaurant_email,
        lat: RestaurantDetails1[i].lat,
        lon: RestaurantDetails1[i].lon,
        restaurant_mobile_number:
          RestaurantDetails1[i].restaurant_mobile_number,
        has_admin_approved: user_main[i].has_admin_approved,
        register_date: user_main[i].register_date,
        closing_time: RestaurantDetails1[i].closing_time,
        wait_time: RestaurantDetails1[i].wait_time,
        total_reviews: RestaurantDetails1[i].total_reviews,
        rating_star: RestaurantDetails1[i].rating_star,
        restaurant_address: RestaurantDetails1[i].restaurant_address,
        restaurant_description: RestaurantDetails1[i].restaurant_description,
        cost: RestaurantTypeDetails1[i].cost,
        selected_dress_code_type:
          RestaurantTypeDetails1[i].selected_dress_code_type,
        primary_payment_method_type:
          RestaurantTypeDetails1[i].primary_payment_method_type,
        selected_payment_method_type:
          RestaurantTypeDetails1[i].selected_payment_method_type,
        restaurant_logo: RestaurantTypeDetails1[i].restaurant_logo,
        primary_restaurant_type:
          RestaurantTypeDetails1[i].primary_restaurant_type,
        restaurant_photo: RestaurantTypeDetails1[i].restaurant_photo,
        vendor_id: Services1[i].vendor_id,
        primary_restaurant_features_type:
          RestaurantFeatures1[i].primary_restaurant_features_type,
        selected_restaurant_features_type:
          RestaurantFeatures1[i].selected_restaurant_features_type,
        primary_restaurant_access_type:
          RestaurantFeatures1[i].primary_restaurant_access_type,
        selected_restaurant_access_type:
          RestaurantFeatures1[i].selected_restaurant_access_type,
        selected_cusines_types: RestaurantFeatures1[i].selected_cusines_types,
        selected_parking_type: RestaurantFeatures1[i].selected_parking_type,
        primary_selecte_parking_type:
          RestaurantFeatures1[i].primary_selecte_parking_type,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_item_type: CusineFeatureOne1[i].primary_food_item_type,
        selected_food_items_type: CusineFeatureOne1[i].selected_food_items_type,
        nutri_info: CusineFeatureOne1[i].nutri_info,
        allergy_information: CusineFeatureOne1[i].allergy_information,
        serve_liquor: CusineFeatureOne1[i].serve_liquor,
        primary_selected_icons: CusineFeatureTwo1[i].primary_selected_icons,
        selected_cusines_types: CusineFeatureTwo1[i].selected_cusines_types,
      };
      array2.push(Arraydata);
    }
    for (let i = 0; i < array2.length; i++) {
      if (array2[i].has_admin_approved === true) {
        array8.push(array2[i]);
      }
    }
    return res.status(200).json(array8);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_superadmin_not_approved_details = async (req, res, next) => {
  try {
    let array9 = [];
    let array2 = [];
    let user_main = await User.find();
    let default_time1 = await default_time.find();
    let RestaurantDetails1 = await RestaurantDetails.find();
    let RestaurantFeatures1 = await RestaurantFeatures.find();
    let RestaurantTime1 = await RestaurantTime.find();
    let RestaurantTypeDetails1 = await RestaurantTypeDetails.find();
    let Services1 = await Services.find();
    let Subscription1 = await Subscription.find();
    let CusineFeatureOne1 = await CusineFeatureOne.find();
    let CusineFeatureTwo1 = await CusineFeatureTwo.find();
    for (let i = 0; i < Subscription1.length; i++) {
      var Arraydata = {
        google_place_id: RestaurantDetails1[i].google_place_id,
        offer_active: RestaurantDetails1[i].offer_active,
        default_time: default_time1[i].default_time,
        servicePage: Subscription1[i].servicePage,
        curb_side: Subscription1[i].curb_side,
        self_serve: Subscription1[i].self_serve,
        skip_line: Subscription1[i].skip_line,
        take_away: Subscription1[i].take_away,
        walk_in: Subscription1[i].walk_in,
        waitlist: Subscription1[i].waitlist,
        reservation: Subscription1[i].reservation,
        restaurant_name: RestaurantDetails1[i].restaurant_name,
        restaurant_city: RestaurantDetails1[i].restaurant_city,
        restaurant_email: RestaurantDetails1[i].restaurant_email,
        lat: RestaurantDetails1[i].lat,
        lon: RestaurantDetails1[i].lon,
        restaurant_mobile_number:
          RestaurantDetails1[i].restaurant_mobile_number,
        has_admin_approved: user_main[i].has_admin_approved,
        register_date: user_main[i].register_date,
        closing_time: RestaurantDetails1[i].closing_time,
        wait_time: RestaurantDetails1[i].wait_time,
        total_reviews: RestaurantDetails1[i].total_reviews,
        rating_star: RestaurantDetails1[i].rating_star,
        restaurant_address: RestaurantDetails1[i].restaurant_address,
        restaurant_description: RestaurantDetails1[i].restaurant_description,
        cost: RestaurantTypeDetails1[i].cost,
        selected_dress_code_type:
          RestaurantTypeDetails1[i].selected_dress_code_type,
        primary_payment_method_type:
          RestaurantTypeDetails1[i].primary_payment_method_type,
        selected_payment_method_type:
          RestaurantTypeDetails1[i].selected_payment_method_type,
        restaurant_logo: RestaurantTypeDetails1[i].restaurant_logo,
        primary_restaurant_type:
          RestaurantTypeDetails1[i].primary_restaurant_type,
        restaurant_photo: RestaurantTypeDetails1[i].restaurant_photo,
        vendor_id: Services1[i].vendor_id,
        primary_restaurant_features_type:
          RestaurantFeatures1[i].primary_restaurant_features_type,
        selected_restaurant_features_type:
          RestaurantFeatures1[i].selected_restaurant_features_type,
        primary_restaurant_access_type:
          RestaurantFeatures1[i].primary_restaurant_access_type,
        selected_restaurant_access_type:
          RestaurantFeatures1[i].selected_restaurant_access_type,
        selected_cusines_types: RestaurantFeatures1[i].selected_cusines_types,
        selected_parking_type: RestaurantFeatures1[i].selected_parking_type,
        primary_selecte_parking_type:
          RestaurantFeatures1[i].primary_selecte_parking_type,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_category_icons:
          CusineFeatureOne1[i].primary_food_category_icons,
        primary_food_item_type: CusineFeatureOne1[i].primary_food_item_type,
        selected_food_items_type: CusineFeatureOne1[i].selected_food_items_type,
        nutri_info: CusineFeatureOne1[i].nutri_info,
        allergy_information: CusineFeatureOne1[i].allergy_information,
        serve_liquor: CusineFeatureOne1[i].serve_liquor,
        primary_selected_icons: CusineFeatureTwo1[i].primary_selected_icons,
        selected_cusines_types: CusineFeatureTwo1[i].selected_cusines_types,
      };
      array2.push(Arraydata);
    }
    for (let i = 0; i < array2.length; i++) {
      if (array2[i].has_admin_approved !== true) {
        array9.push(array2[i]);
      }
    }
    return res.status(200).json(array9);
  } catch (err) {
    let errors = {};
    console.log(err);
    errors.SERVER_ERRORS = "Internal Errors Please Try Again";
    return res.status(500).json(errors);
  }
};
exports.get_handicap_highchair = async (req, res, next) => {
  try {
    const body = req.body;
    let handi_high = [];
    let handicap_highchair = await RestaurantFeatures.findOne({
      vendor_id: body.vendor_id,
    });
    var arraydata = {
      selected_restaurant_access_type:
        handicap_highchair.selected_restaurant_access_type,
    };
    handi_high.push(arraydata);
    return res.status(200).json(handi_high);
  } catch (err) {
    return res.status(400).json("Error");
  }
};
exports.get_selected_seating_area = async (req, res, next) => {
  try {
    const id = jwt.verify(req.headers.authorization.split(" ")[1], keys)._id;
    let seating_area = [];
    let selected_seating = await RestaurantTime.findOne({ vendor_id: id });
    let subscription_handicap = await Subscription.findOne({ vendor_id: id });
    var arraydata = {
      selected_seating_area: selected_seating.selected_seating_area,
      primary_seating_area: selected_seating.primary_seating_area,
      highchair: subscription_handicap.highchair,
      handicap: subscription_handicap.handicap,
    };
    seating_area.push(arraydata);
    return res.status(200).json(seating_area);
  } catch (err) {
    return res.status(400).json("Error");
  }
};

exports.get_time_cutoff = async (req, res, next) => {
  try {
    const body = req.body; //date or vendor_id
    let get_time_cutoff = await Subscription.findOne({
      vendor_id: body.vendor_id,
    });
    let aa = parseInt(get_time_cutoff.cut_off_time);
    let bb = parseInt(get_time_cutoff.reservation_time_slot);
    var times = [];
    var currentDate = Moment(new Date()).format("DD-MM-YYYY");
    let x = bb; //minutes interval
    let cutOff = aa;
    let day = Moment(body.selectedDate, "DD-MM-YYYY")
      .format("dddd")
      .toLowerCase();
    //vendor id using get all data
    let get_time = await RestaurantTime.find({
      vendor_id: body.vendor_id,
    });
    if (get_time[0].are_you_open_24_x_7 == true) {
      if (body.selectedDate === currentDate) {
        //persent t
        let begin = Moment(new Date());
        let remainder = 30 - (begin.minute() % 30);
        let dateTime = Moment(begin)
          .add(remainder, "minutes")
          .format("HH:mm:ss");
        let start = Moment("00:00:00", "HH:mm:ss");
        let minutesPassed = Moment(dateTime, "HH:mm:ss").diff(start, "minutes");
        let tt = minutesPassed;
        let ap = ["AM", "PM"];
        for (var i = 0; tt < 24 * 60; i++) {
          var hh = Math.floor(tt / 60);
          var mm = tt % 60;
          times[i] =
            ("" + (hh == 12 ? 12 : hh % 12)).slice(-2) +
            ":" +
            ("0" + mm).slice(-2) +
            " " +
            ap[Math.floor(hh / 12)];
          tt = tt + x;
        }
        return res.status(200).json({ timeSlots: times }); //return to times array
      } else {
        let tt = 0;
        let ap = ["AM", "PM"];
        for (var i = 0; tt < 24 * 60; i++) {
          var hh = Math.floor(tt / 60);
          var mm = tt % 60;

          times[i] =
            ("" + (hh == 12 ? 12 : hh % 12)).slice(-2) +
            ":" +
            ("0" + mm).slice(-2) +
            " " +
            ap[Math.floor(hh / 12)];
          tt = tt + x;
        }
        return res.status(200).json({ timeSlots: times });
      }
    } else if (get_time[0].multiple_opening_time == true) {
      console.log("multipleOpentime");
      let multipleTime = [];
      if (get_time[0][day].open == true) {
        if (get_time[0][day].breakfast.open == true) {
          let obj = {};
          obj["openTime"] = get_time[0][day].breakfast.opening_time;
          obj["closeTime"] = get_time[0][day].breakfast.closing_time;
          multipleTime.push(obj);
        }
        if (get_time[0][day].lunch.open == true) {
          let obj = {};
          obj["openTime"] = get_time[0][day].lunch.opening_time;
          obj["closeTime"] = get_time[0][day].lunch.closing_time;
          multipleTime.push(obj);
        }
        if (get_time[0][day].dinner.open == true) {
          let obj = {};
          obj["openTime"] = get_time[0][day].dinner.opening_time;
          obj["closeTime"] = get_time[0][day].dinner.closing_time;
          multipleTime.push(obj);
        }
        let finalSlots = [];
        for (let t = 0; t < multipleTime.length; t++) {
          let open = multipleTime[t].openTime;
          let close = multipleTime[t].closeTime;
          let begin;
          if (body.selectedDate === currentDate) {
            if (
              Moment(new Date(), "HH:mm").isSameOrAfter(Moment(open, "HH:mm"))
            ) {
              begin = Moment(new Date());
            } else {
              begin = Moment(open, "HH:mm");
            }
          } else {
            begin = Moment(open, "HH:mm");
          }

          let remainder = 30 - (begin.minute() % 30);

          let dateTime = Moment(begin)
            .add(remainder, "minutes")
            .format("HH:mm:ss");

          let start = Moment("00:00:00", "HH:mm:ss");

          let minutesPassed = Moment(dateTime, "HH:mm:ss").diff(
            start,
            "minutes"
          );
          let end = Moment(close, "HH:mm").diff(start, "minutes") - cutOff;
          let tt = minutesPassed;
          let ap = ["AM", "PM"];

          for (var i = 0; tt <= end; i++) {
            var hh = Math.floor(tt / 60);
            var mm = tt % 60;

            times[i] =
              ("" + (hh == 12 ? 12 : hh % 12)).slice(-2) +
              ":" +
              ("0" + mm).slice(-2) +
              " " +
              ap[Math.floor(hh / 12)];
            tt = tt + x;
          }
          finalSlots = finalSlots.concat(times);
        }
        return res.status(200).json({ timeSlots: finalSlots });
      } else {
        return res.status(200).json({ timeSlots: ["closed"] });
      }
    } else {
      let open = get_time[0][day].main_opening_time;
      let close = get_time[0][day].main_closing_time;
      let begin;
      if (body.selectedDate == currentDate) {
        if (Moment(new Date(), "HH:mm").isSameOrAfter(Moment(open, "HH:mm"))) {
          begin = Moment(new Date());
        } else {
          begin = Moment(open, "HH:mm");
        }
      } else {
        begin = Moment(open, "HH:mm");
      }
      let remainder = 30 - (begin.minute() % 30);
      let dateTime = Moment(begin).add(remainder, "minutes").format("HH:mm:ss");
      let start = Moment("00:00:00", "HH:mm:ss");
      let minutesPassed = Moment(dateTime, "HH:mm:ss").diff(start, "minutes");
      let end = Moment(close, "HH:mm").diff(start, "minutes");
      let tt = minutesPassed;
      let ap = ["AM", "PM"];
      for (var i = 0; tt < end; i++) {
        var hh = Math.floor(tt / 60);
        var mm = tt % 60;
        times[i] =
          ("" + (hh == 12 ? 12 : hh % 12)).slice(-2) +
          ":" +
          ("0" + mm).slice(-2) +
          " " +
          ap[Math.floor(hh / 12)];
        tt = tt + x;
      }
      return res.status(200).json({ timeSlots: times });
    }
  } catch (err) {
    return res.status(400).json("Error");
  }
};

//13-04-2020
