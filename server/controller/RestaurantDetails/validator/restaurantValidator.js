const Validator = require("validator");
const isEmpty = require("../../../utils/isEmpty");

const restaruntValidation = async function(data) {
  let errors = {};

  data.restaurant_description = !isEmpty(data.restaurant_description)
    ? data.restaurant_description
    : "";

  if (
    !Validator.isLength(data.restaurant_description, {
      min: 100,
      max: 5000
    })
  ) {
    errors.restaurant_description = "Invalid Mobile Number";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};

module.exports = restaruntValidation;
