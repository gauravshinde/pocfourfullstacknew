const express = require('express');
const router = express.Router();


/*************************************************************
 * @DESC - DETAILS CONTROLLERS
 *************************************************************/
const waitlistController = require('../controller/JoinWaitList/joinwaitController');


/**************************************************************
 * @ROUTE       - /waitlist/add-waitlist
 * @METHOD      - POST
 * @DESC        - TO CREATE NEW WAIT LIST
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.post('/add-waitlist', waitlistController.add_join_wait_list);


module.exports = router;