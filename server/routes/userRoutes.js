const express = require("express");
const router = express.Router();
const passport = require("passport");

/*************************************************************
 * @DESC - USER CONTROLLERS
 *************************************************************/
const userControllers = require("../controller/Users/userController");

/**************************************************************
 * @ROUTE       - /users/test
 * @METHOD      - GET
 * @DESC        - TO TEST USER ROUTES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/test", userControllers.user_test);

// /**************************************************************
//  * @ROUTE       - /users/signup
//  * @METHOD      - POST
//  * @DESC        - TO CREATE USER
//  * @ACCESS      - PUBLIC
//  * @PROTECTION  - NONE
//  ***************************************************************/
// router.post(
//   "/signup",
//   userControllers.check_unique_mobile_number,
//   userControllers.user_signup
//   //userControllers.send_otp
// );

/**************************************************************
 * @ROUTE       - /users/signup
 * @METHOD      - POST
 * @DESC        - TO CREATE USER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/signup",
  userControllers.check_unique_mobile_number,
  userControllers.user_signup
  //userControllers.send_twilio_email
  //userControllers.send_otp
);

/**************************************************************
 * @ROUTE       - /users/sendOtp
 * @METHOD      - POST
 * @DESC        - TO SEND OTP TO REGISTER MOBILE NUMBER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/sendOtp", userControllers.send_otp);

/**************************************************************
 * @ROUTE       - /users/verifyOtp
 * @METHOD      - POST
 * @DESC        - TO VERIFY OTP TO REGISTER MOBILE NUMBER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/verifyOtp", userControllers.verify_otp);

/**************************************************************
 * @ROUTE       - /users/update_mobile_number
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE REGISTER MOBILE NUMBER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update_mobile_number",
  userControllers.update_mobile_number,
  userControllers.send_otp
);

/**************************************************************
 * @ROUTE       - /users/forgot-password
 * @METHOD      - PATCH
 * @DESC        - TO SET FORGOT PASSWORD
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/forgot-password", userControllers.verify_forgot_password);

/**************************************************************
 * @ROUTE       - /users/login
 * @METHOD      - POST
 * @DESC        - USER LOGIN
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
// router.post("/login", userControllers.user_login, userControllers.Qrcode);
router.post("/login", userControllers.user_login);

/**************************************************************
 * @ROUTE       - /users/call_twilio
 * @METHOD      - POST
 * @DESC        - CALL
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/call_twilio", userControllers.call_twilio);

/**************************************************************
 * @ROUTE       - /users/update_hotel_status
 * @METHOD      - PATCH
 * @DESC        - TO UPADET HOTEL STATUS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update_hotel_status", userControllers.update_hotel_availability);

/**************************************************************
 * @ROUTE       - /users/vendor_approved
 * @METHOD      - PATCH
 * @DESC        - TO APPROVED USER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/vendor_approved", userControllers.vendor_approved);

/**************************************************************
 * @ROUTE       - /users/restaurant_current_status
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT CURRENT STATUS
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/restaurant_current_status",
  userControllers.get_restaurant_current_status
);

/**************************************************************
 * @ROUTE       - /users/resto_open_close
 * @METHOD      - PATCH
 * @DESC        - TO UPADET HOTEL STATUS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/resto_open_close", userControllers.update_resto_current_status);

module.exports = router;
