const express = require("express");
const router = express.Router();

/*************************************************************
 * @DESC - DETAILS CONTROLLERS
 *************************************************************/
const raiseController = require("../controller/RaiseTicket/raiseController");

/**************************************************************
 * @ROUTE       - /raise/add-raise
 * @METHOD      - POST
 * @DESC        - TO ADD NEW RAISE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-raise", raiseController.add_raise_ticket);

/**************************************************************
 * @ROUTE       - /raise/get-raise
 * @METHOD      - GET
 * @DESC        - TO GET ALL RAISE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-raise", raiseController.get_raise_ticket);

/**************************************************************
 * @ROUTE       - /raise/get-raise-closed
 * @METHOD      - GET
 * @DESC        - TO GET ALL RAISE CLOSE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-raise-closed", raiseController.get_raise_ticket_closed);

/**************************************************************
 * @ROUTE       - /raise/update-issue
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE ISSUE STATUS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-issue", raiseController.update_issue_status);

module.exports = router;
