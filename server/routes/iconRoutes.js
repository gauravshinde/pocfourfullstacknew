const express = require("express");
const router = express.Router();
const passport = require("passport");

/*************************************************************
 * @DESC - ICON CONTROLLERS
 *************************************************************/
const iconController = require("../controller/IconList/iConListController");
const chainController = require("../controller/Chain/chainController");
/**************************************************************
 * @ROUTE       - /icons/
 * @METHOD      - GET
 * @DESC        - TO TEST USER ROUTES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/", iconController.get_all_icons);

/**************************************************************
 * @ROUTE       - /icons/chains
 * @METHOD      - POST
 * @DESC        - TO CREATE A NEW CHAIN
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/chains", iconController.create_new_chain);

/**************************************************************
 * @ROUTE       - /icons/chains
 * @METHOD      - GET
 * @DESC        - TO GET ALL NEW CHAIN
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/chains", iconController.get_all_chains);

/**************************************************************
 * @ROUTE       - /icons/get-chains
 * @METHOD      - GET
 * @DESC        - TO GET ALL NEW CHAIN
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-chains", iconController.get_chain);

router.post("/icon", iconController.create_new_icon);

/**************************************************************
 * @ROUTE       - /icons/food-type
 * @METHOD      - GET
 * @DESC        - TO GET ALL Food Type
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/food-type", iconController.get_food_type);

/**************************************************************
 * @ROUTE       - /icons/allergy-info
 * @METHOD      - GET
 * @DESC        - TO GET ALL ALLERGY INFO
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/allergy-info", iconController.get_allergy_info);

module.exports = router;
