const express = require("express");
const router = express.Router();
const menuController = require("../controller/MenuController/menuController");

/**************************************************************
 * @ROUTE       - /menu/add-menu
 * @METHOD      - POST
 * @DESC        - TO ADD NEW MENU
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-menu", menuController.add_New_Menu);

/**************************************************************
 * @ROUTE       - /menu/get-menu
 * @METHOD      - GET
 * @DESC        - TO GET ALL MENU
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-menu", menuController.get_all_menu);

/**************************************************************
 * @ROUTE       - /dinner/all_dinner
 * @METHOD      - GET
 * @DESC        - TO GET DINNER LIST
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
// router.get("/all_dinner", dinnerController.get_dinner_list);

/**************************************************************
 * @ROUTE       - /menu/update-menu
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE MENU DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-menu", menuController.update_menu);

/**************************************************************
 * @ROUTE       - /menu/get-each-menu
 * @METHOD      - GET
 * @DESC        - TO GET EACH ITEM DETAILS
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get-each-menu", menuController.get_each_item_details);

module.exports = router;
