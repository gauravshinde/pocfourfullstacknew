const express = require('express');
const router = express.Router();
// IMAGE UPLOADER
const upload = require('../functions/aws_image_uploader');

// @ ROUTE POST /image/upload-content-images
// @ DESC - UPLOAD the Content Images
// @ ACCESS - CSRF PROTECTION
router.post(
  '/upload-content-images',
  upload.single('image'),
  (req, res, next) => {
    return res.json({ image_URL: req.file.location });
  }
);

module.exports = router;
