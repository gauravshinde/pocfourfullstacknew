const express = require("express");
const router = express.Router();

/*************************************************************
 * @DESC - DETAILS CONTROLLERS
 *************************************************************/
const eventController = require("../controller/Event/eventController");

/**************************************************************
 * @ROUTE       - /event/add-event
 * @METHOD      - POST
 * @DESC        - TO CREATE NEW EVENT
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-event", eventController.add_event);

/**************************************************************
 * @ROUTE       - /event/get-event
 * @METHOD      - GET
 * @DESC        - TO GET EVENT
 * @ACCESS      - PROTECTED
 * @PROTECTION  - PASSPORT
 ***************************************************************/
router.get("/get-event", eventController.get_event_details);

/**************************************************************
 * @ROUTE       - /event/update-event
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE EVENT DATEILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-event", eventController.update_event_details);

/**************************************************************
 * @ROUTE       - /event/delete-event
 * @METHOD      - DELETE
 * @DESC        - TO DELETE EVENT
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/delete-event", eventController.delete_event);

/**************************************************************
 * @ROUTE       - /event/update-availability
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE AVAILABILITY
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-availability", eventController.update_availability);

/**************************************************************
 * @ROUTE       - /event/update-event-view
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE EVENT VIEW
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-event-view", eventController.update_event_view);

/**************************************************************
 * @ROUTE       - /event/get-event-user
 * @METHOD      - GET
 * @DESC        - TO GET EVENT USER
 * @ACCESS      - PROTECTED
 * @PROTECTION  - PASSPORT
 ***************************************************************/
router.get("/get-event-user", eventController.get_event_show);

router.get("/get-cron", eventController.cron);

module.exports = router;
