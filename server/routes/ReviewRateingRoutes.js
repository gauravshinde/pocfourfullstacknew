const express = require("express");
const router = express.Router();

/*************************************************************
 * @DESC - DETAILS CONTROLLERS
 *************************************************************/
const reviewrateingController = require("../controller/ReviewRateing/reviewrateingController");

/**************************************************************
 * @ROUTE       - /review/add-review-rateing
 * @METHOD      - POST
 * @DESC        - TO ADD NEW REVIEW AND RATEING
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-review-rateing", reviewrateingController.add_review_rateing);

/**************************************************************
 * @ROUTE       - /review/get-restaurant-review
 * @METHOD      - GET
 * @DESC        - TO GET REVIEW AND RATEING
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-restaurant-review",
  reviewrateingController.get_review_rateing
);

/**************************************************************
 * @ROUTE       - /review/get-restaurant-review-approve
 * @METHOD      - GET
 * @DESC        - TO GET REVIEW AND RATEING
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-restaurant-review-approve",
  reviewrateingController.get_approve_review_rateing
);

/**************************************************************
 * @ROUTE       - /review/approve-restaurant-review
 * @METHOD      - PATCH
 * @DESC        - TO APPROVE REVIEW AND RATEING
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/approve-restaurant-review",
  reviewrateingController.approve_review,
  reviewrateingController.review
);

/**************************************************************
 * @ROUTE       - /review/reject-restaurant-review
 * @METHOD      - POST
 * @DESC        - TO REJECT REVIEW AND RATEING
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/reject-restaurant-review", reviewrateingController.reject_review);

/**************************************************************
 * @ROUTE  - /review/get-restaurant-review-approve-user
 * @METHOD - POST
 * @DESC - TO GET REVIEW AND RATEING
 * @ACCESS- PUBLIC *
 * @PROTECTION  - NONE
 * ***************************************************************/
router.post(
  "/get-restaurant-review-approve-user",
  reviewrateingController.get_approve_review_rateing_user
);

module.exports = router;
