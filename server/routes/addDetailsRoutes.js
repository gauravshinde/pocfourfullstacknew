const express = require("express");
const router = express.Router();
const passport = require("passport");

/*************************************************************
 * @DESC - DETAILS CONTROLLERS
 *************************************************************/
const suggestController = require("../controller/SuggestNew/suggestController");
const pocController = require("../controller/PersonOfContact/PersonofContactController");
const restaurantDetails = require("../controller/RestaurantDetails/restaurantDetailscontrollers");
const serviceDetailsController = require("../controller/ServiceDetails/serviceDetailsController");
const restaurantFeaturesController = require("../controller/RestaurantFeatures/restaurantFeaturesController");
const CuisineOneController = require("../controller/Cusine/cusineController");
const KycBankController = require("../controller/KycBank/KycOfBankController");
const ChainController = require("../controller/Chain/chainController");
const SubscriptionController = require("../controller/subscription/subscriptionController");
const OfferController = require("../controller/Offer/OfferController");
const dinnerController = require("../controller/Dinner/dinnerController");

/**************************************************************
 * @ROUTE       - /details/add-poc
 * @METHOD      - POST
 * @DESC        - TO CREATE NEW POC
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-poc", pocController.create_new_poc);

/**************************************************************
 * @ROUTE       - /details/get-poc
 * @METHOD      - GET
 * @DESC        - TO GET POC
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-poc", pocController.get_my_poc);

/**************************************************************
 * @ROUTE       - /details/add-restaurant
 * @METHOD      - POST
 * @DESC        - TO CREATE Restaurant Location
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-restaurant", restaurantDetails.create_new_restaurant_details);

/**************************************************************
 * @ROUTE       - /details/get-restaurant
 * @METHOD      - GET
 * @DESC        - TO GET MY RESTAURANT DETIALS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-restaurant", restaurantDetails.get_my_restaurant_details);

/**************************************************************
 * @ROUTE       - /details/add-restoType
 * @METHOD      - GET
 * @DESC        - TO GET MY RESTAURANT DETIALS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-restoType", restaurantDetails.create_new_restaurant_type);

/**************************************************************
 * @ROUTE       - /details/get-restoType
 * @METHOD      - GET
 * @DESC        - TO GET MY RESTAURANT DETIALS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-restoType", restaurantDetails.get_restaurant_Type);

/**************************************************************
 * @ROUTE       - /details/add-service-details
 * @METHOD      - GET
 * @DESC        - TO GET MY SERVICE DETIALS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/add-service-details",
  serviceDetailsController.create_new_service_details
);

/**************************************************************
 * @ROUTE       - /details/get-service-details
 * @METHOD      - GET
 * @DESC        - TO GET MY SERVICE DETIALS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-service-details",
  serviceDetailsController.get_services_details
);

/**************************************************************
 * @ROUTE       - /details/add-restoFeatures
 * @METHOD      - GET
 * @DESC        - TO AD MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/add-restoFeatures",
  restaurantFeaturesController.create_new_restaurant_features
);

/**************************************************************
 * @ROUTE       - /details/get-restoFeatures
 * @METHOD      - GET
 * @DESC        - TO GET MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-restoFeatures",
  restaurantFeaturesController.get_restaurant_features
);

/**************************************************************
 * @ROUTE       - /details/add-cusineOne
 * @METHOD      - POST
 * @DESC        - TO GET MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-cusineOne", CuisineOneController.create_new_cusine_One);

/**************************************************************
 * @ROUTE       - /details/add-cusineTwo
 * @METHOD      - POST
 * @DESC        - TO GET MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-cusineTwo", CuisineOneController.create_new_cusine_Two);

/**************************************************************
 * @ROUTE       - /details/get-cusineOne
 * @METHOD      - GET
 * @DESC        - TO GET MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-cusineOne", CuisineOneController.get_cusine_one);

/**************************************************************
 * @ROUTE       - /details/get-cusineTwo
 * @METHOD      - GET
 * @DESC        - TO GET MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-cusineTwo", CuisineOneController.get_cusine_two);

/**************************************************************
 * @ROUTE       - /details/update-cusineOne
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-cusineOne",
  CuisineOneController.update_cuisine_one,
  CuisineOneController.update_cuisine_two
);

/**************************************************************
 * @ROUTE       - /details/update-cusineTwo
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-cusineTwo", CuisineOneController.update_cuisine_two);

/**************************************************************
 * @ROUTE       - /details/update-poc
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE NEW POC
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-poc", pocController.update_new_poc);

/**************************************************************
 * @ROUTE       - /details/_new
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE RESTAURANT DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-restaurant",
  restaurantDetails.update_restaurant_details,
  restaurantDetails.update_restaurant_features
);

/**************************************************************
 * @ROUTE       - /details/update-restoFeatures
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-restoFeatures",
  restaurantFeaturesController.update_restaurant_features
);

/**************************************************************
 * @ROUTE       - /details/update-service-details
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE SERVICE DETIALS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-service-details",
  serviceDetailsController.update_service_details
);

/**************************************************************
 * @ROUTE       - /details/add-kycbank-details
 * @METHOD      - POST
 * @DESC        - TO ADD KYC BANK DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-kycbank-details", KycBankController.create_new_kycbank);

/**************************************************************
 * @ROUTE       - /details/get-kycbank-details
 * @METHOD      - GET
 * @DESC        - TO GET KYC BANK DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-kycbank-details", KycBankController.get_kycbank_details);

/**************************************************************
 * @ROUTE       - /details/update-kycbank-details
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE KYC AND BANK DETIALS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-kycbank-details",
  KycBankController.update_kyc_bank_details
);

/**************************************************************
 * @ROUTE       - /details/add-chain-details
 * @METHOD      - POST
 * @DESC        - TO ADD CHAIN DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-chain-details", ChainController.add_chain);

/**************************************************************
 * @ROUTE       - /details/get-chain
 * @METHOD      - GET
 * @DESC        - TO GET CHAIN DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-chain", ChainController.get_chain);

/**************************************************************
 * @ROUTE       - /details/add-subscription-details
 * @METHOD      - POST
 * @DESC        - TO ADD SUBSCRIPTION DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/add-subscription-details",
  SubscriptionController.SubscriptionDetails,
  SubscriptionController.update_have_submited_details,
  dinnerController.add_default_wait_time
);

/**************************************************************
 * @ROUTE       - /details/add-restaurantTime
 * @METHOD      - POST
 * @DESC        - TO ADD RESTAURANT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-restaurantTime", restaurantDetails.add_restaurantTime);

/**************************************************************
 * @ROUTE       - /details/get-restaurantTime
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-restaurantTime", restaurantDetails.get_restaurant_Time);

/**************************************************************
 * @ROUTE       - /details/get-restaurantTime
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get-restaurantTime", restaurantDetails.get_restaurant_Time_user);

/**************************************************************
 * @ROUTE       - /details/get-subscription-details
 * @METHOD      - GET
 * @DESC        - TO GET SUBSCRIPTION DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-subscription-details",
  SubscriptionController.get_subscription_details
);

/**************************************************************
 * @ROUTE       - /details/update-restaurant-details
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE NEW RESTAURANT MAP DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-restaurant-details",
  restaurantDetails.update_new_restaurant_details
);

/**************************************************************
 * @ROUTE       - /details/delete-poc
 * @METHOD      - DELETE
 * @DESC        - TO DELETE POC
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/delete-poc", pocController.delete_poc);

/**************************************************************
 * @ROUTE       - /details/update-restaurantTime
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE NEW RESTAURANT TIME DETAILSs
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-restaurantTime",
  restaurantDetails.update_restaurantTime,
  restaurantDetails.update_kyc_bank_details
);

/**************************************************************
 * @ROUTE       - /details/suggest-new
 * @METHOD      - POST
 * @DESC        - TO ADD SUGGEST NEW DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/suggest-new", suggestController.create_new_suggest);

/**************************************************************
 * @ROUTE       - /details/update-subscription
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE SUBSCRIPTION DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-subscription",
  SubscriptionController.update_subscription,
  SubscriptionController.update_submited_details
);

/**************************************************************
 * @ROUTE       - /details/get_all_details
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT ALL DETAILS
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get_all_details", restaurantDetails.get_all_details);

/**************************************************************
 * @ROUTE       - /details/get_all_details
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT ALL DETAILS
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get_all_details", restaurantDetails.get_superadmin_all_details);

// /**************************************************************
//  * @ROUTE       - /details/add-offer
//  * @METHOD      - POST
//  * @DESC        - TO ADD NEW OFFER TO EACH VENDOR
//  * @ACCESS      - PUBLIC
//  * @PROTECTION  - NONE
//  ***************************************************************/
// router.post("/add-offer", OfferController.add_New_Offer);

/**************************************************************
 * @ROUTE       - /details/add-offer
 * @METHOD      - POST
 * @DESC        - TO ADD NEW OFFER TO EACH VENDOR
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/add-offer", OfferController.add_New_Offer);

/**************************************************************
 * @ROUTE       - /details/offer-list
 * @METHOD      - GET
 * @DESC        - TO GET THE ALL OFFER TO EACH VENDOR
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/offer-list", OfferController.Offer_list);

/**************************************************************
 * @ROUTE       - /details/offer-list-amaelio
 * @METHOD      - GET
 * @DESC        - TO GET THE ALL OFFER TO ALL VENDOR
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/offer-list-amaelio", OfferController.get_all_offer_amaelio);

/**************************************************************
 * @ROUTE       - /details/deactivate-offer
 * @METHOD      - PATCH
 * @DESC        - TO DEACTIVETED THE OFFER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/deactivate-offer", OfferController.deactivate_offer);

/**************************************************************
 * @ROUTE       - /details/delete-offer
 * @METHOD      - POST
 * @DESC        - TO DELETE THE OFFER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/delete-offer", OfferController.delete_offer);

/**************************************************************
 * @ROUTE       - /details/deactivate-offer
 * @METHOD      - PATCH
 * @DESC        - TO DEACTIVETED THE OFFER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-offer", OfferController.update_offer);

/**************************************************************
 * @ROUTE       - /details/update_offer_status
 * @METHOD      - PATCH
 * @DESC        - TO UPADET OFFER STATUS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update_offer_status", OfferController.update_offer_availability);

/**************************************************************
 * @ROUTE       - /details/add-favourite
 * @METHOD      - POST
 * @DESC        - TO ADD FAVOURITE RESTAURANT TO EACH USER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-favourite", restaurantDetails.add_to_favourite);

/**************************************************************
 * @ROUTE       - /details/map
 * @METHOD      - POST
 * @DESC        - TO GET THE NEARBY RESTAURANT LIST
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/map", restaurantDetails.map);

/**************************************************************
 * @ROUTE       - /details/update-offer-view
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE OFFER VIEW
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-offer-view", OfferController.update_offer_view);

/**************************************************************
 * @ROUTE       - /details/update-offer-coupan-view
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE OFFER COUPAN VIEW
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-offer-coupan-view", OfferController.update_coupon_view);

/**************************************************************
 * @ROUTE       - /details/update-restaurant_new
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE NEW MAP DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-restaurant_new",
  restaurantDetails.update_new_restaurant_details
);

/**************************************************************
 * @ROUTE       - /details/update-restaurant-detailsone
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE RESTAURANT DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-restaurant-detailsone",
  restaurantDetails.update_restaurant_details
);

/**************************************************************
 * @ROUTE       - /details/update-cusineOnedata
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-cusineOnedata", CuisineOneController.update_cuisine_one);

/**************************************************************
 * @ROUTE       - /details/update-cusineTwo_data
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE MY RESTAURANT FEATUES
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-cusineTwo_data",
  CuisineOneController.update_cuisine_two_data
);

/**************************************************************
 * @ROUTE       - /details/update-restaurantTime-data
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE NEW RESTAURANT TIME DETAILSs
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-restaurantTime-data",
  restaurantDetails.update_restaurantTime_data
);

/**************************************************************
 * @ROUTE       - /details/get_each_vendor_offer_amaelio
 * @METHOD      - POST
 * @DESC        - TO GET THE EACH VENDOR RESTAURANT OFFER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/get_each_vendor_offer_amaelio",
  OfferController.get_each_vendor_offer_amaelio
);

/**************************************************************
 * @ROUTE       - /details/resto
 * @METHOD      - POST
 * @DESC        - TO GET THE EACH RESTAURANT INFORMATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/resto", restaurantDetails.get_restaurant_details);

/**************************************************************
 * @ROUTE       - /details/unregister
 * @METHOD      - POST
 * @DESC        - TO GET UNREGISTER RESTORANT
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/unregister", restaurantDetails.get_all_unregister_resto);

/**************************************************************
 * @ROUTE       - /details/get_approve_vendor_details
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT APPROVED FROM SUPERADMIN
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get_approve_vendor_details",
  restaurantDetails.get_superadmin_approved_details
);

/**************************************************************
 * @ROUTE       - /details/get_not_approve_vendor_details
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT NOT APPROVED FROM SUPERADMIN
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get_not_approve_vendor_details",
  restaurantDetails.get_superadmin_not_approved_details
);

/**************************************************************
 * @ROUTE       - /details/get-highchair-handicap
 * @METHOD      - POST
 * @DESC        - TO GET HIGHCHAIR AND HANDICAP VALUE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/get-highchair-handicap",
  restaurantDetails.get_handicap_highchair
);

/**************************************************************
 * @ROUTE       - /details/get-seating-area-info
 * @METHOD      - GET
 * @DESC        - TO GET HIGHCHAIR AND HANDICAP VALUE AND SELECTED SEATING AREA
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-seating-area-info",
  restaurantDetails.get_selected_seating_area
);

/**************************************************************
 * @ROUTE       - /details/get_cutoff_time
 * @METHOD      - POST
 * @DESC        - TO DELETE THE RESTORANT TYPE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get_cutoff_time", restaurantDetails.get_time_cutoff);

module.exports = router;

//new 21-01-2020
