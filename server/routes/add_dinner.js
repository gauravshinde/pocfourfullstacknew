const express = require("express");
const router = express.Router();
const dinnerController = require("../controller/Dinner/dinnerController");

/**************************************************************
 * @ROUTE       - /dinner/push
 * @METHOD      - POST
 * @DESC        - TO push
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/push", dinnerController.push);

/*************Add dinner route***************/
router.post("/email", dinnerController.email);

/**************************************************************
 * @ROUTE       - /dinner/update_waitTime
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE WAIT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update_waitTime", dinnerController.update_wait_time);

/**************************************************************
 * @ROUTE       - /dinner/all_dinner
 * @METHOD      - GET
 * @DESC        - TO GET DINNER LIST
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/all_dinner", dinnerController.get_dinner_list);

/**************************************************************
 * @ROUTE       - /dinner/add_dinner
 * @METHOD      - POST
 * @DESC        - TO ADD NEW DINNER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/add_dinner",
  dinnerController.add_dinner,
  dinnerController.send_sms_twilio
);

// /**************************************************************
//  * @ROUTE       - /dinner/add_dinner
//  * @METHOD      - POST
//  * @DESC        - TO ADD NEW DINNER
//  * @ACCESS      - PUBLIC
//  * @PROTECTION  - NONE
//  ***************************************************************/
// router.post(
//   "/add_dinner",
//   dinnerController.add_dinner,
//   dinnerController.send_sms_twilio,
//   dinnerController.timer
// );

// /**************************************************************
//  * @ROUTE       - /dinner/add_dinner
//  * @METHOD      - POST
//  * @DESC        - TO ADD NEW DINNER
//  * @ACCESS      - PUBLIC
//  * @PROTECTION  - NONE
//  ***************************************************************/
// router.post('/add_dinner', dinnerController.add_dinner);

/**************************************************************
 * @ROUTE       - /dinner/add-wake-in
 * @METHOD      - POST
 * @DESC        - TO ADD NEW DINNER with wake in
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/add-wake-in",
  dinnerController.add_wake_in,
  dinnerController.walkin_msg
);

/**************************************************************
 * @ROUTE       - /dinner/update-dinner
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE  DINNER DETAILS
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-dinner", dinnerController.update_dinner);

/**************************************************************
 * @ROUTE       - /dinner/status-update
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE STATUS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/status-update", dinnerController.update_status_reservation);

/**************************************************************
 * @ROUTE       - /dinner/send
 * @METHOD      - POST
 * @DESC        - TO SEND SMS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/send", dinnerController.send_sms);

/**************************************************************
 * @ROUTE       - /dinner/get_wait_time
 * @METHOD      - GET
 * @DESC        - TO GET WAIT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get_wait_time", dinnerController.get_wait_time);

/**************************************************************
 * @ROUTE       - /dinner/default
 * @METHOD      - POST
 * @DESC        - TO ADD DEFAULT WAIT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/default", dinnerController.add_default_wait_time);

/**************************************************************
 * @ROUTE       - /dinner/default_wait_time
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE DEFAULT WAIT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/default_wait_time", dinnerController.update_default_wait_time);

/**************************************************************
 * @ROUTE       - /dinner/get-each-dinner
 * @METHOD      - GET
 * @DESC        - TO GET DINNER DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-each-dinner", dinnerController.get_each_dinner);

/**************************************************************
 * @ROUTE       - /dinner/get-cap
 * @METHOD      - GET
 * @DESC        - TO GET TOTAL SEATING CAPACITY
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-cap", dinnerController.restaurant_info);

/**************************************************************
 * @ROUTE       - /dinner/get-each-dinner-pending
 * @METHOD      - GET
 * @DESC        - TO GET PENDING DINNER DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-each-dinner-pending",
  dinnerController.get_each_dinner_pending
);

/**************************************************************
 * @ROUTE       - /dinner/cancel-reservation
 * @METHOD      - PATCH
 * @DESC        - TO CANCEL THE  RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/cancel-reservation", dinnerController.cancel_reservation);

/**************************************************************
 * @ROUTE       - /dinner/get-reservation
 * @METHOD      - GET
 * @DESC        - TO GET THE ALL RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get-reservation", dinnerController.get_reservation);

router.patch("/status/", dinnerController.update_status, dinnerController.sms);

/**************************************************************
 * @ROUTE       - /dinner/get-history
 * @METHOD      - GET
 * @DESC        - TO GET HISTORY
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-history", dinnerController.get_history);

/**************************************************************
 * @ROUTE       - /dinner/add-waitlist
 * @METHOD      - POST
 * @DESC        - TO CREATE NEW WAIT LIST
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/add-waitlist",
  dinnerController.add_join_wait_list,
  dinnerController.waitlist_msg
);

/**************************************************************
 * @ROUTE       - /dinner/send_sms_twilio
 * @METHOD      - POST
 * @DESC        - TO SEND SMS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/send_sms_twilio", dinnerController.send_sms_twilio);

/**************************************************************
 * @ROUTE       - /dinner/get-waitlist
 * @METHOD      - POST
 * @DESC        - TO POST waitlist
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get-waitlist", dinnerController.get_waitlist);

/**************************************************************
 * @ROUTE       - /dinner/call_twilio
 * @METHOD      - POST
 * @DESC        - TO CALL
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/call_twilio", dinnerController.call_twilio);

/**************************************************************
 * @ROUTE       - /dinner/get-walkin
 * @METHOD      - GET
 * @DESC        - TO GET WALK_IN
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-walkin", dinnerController.get_walkin);

/**************************************************************
 * @ROUTE       - /dinner/get-user-history-completed
 * @METHOD      - GET
 * @DESC        - TO GET HISTORY
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-user-history-completed",
  dinnerController.get_user_history_completed
);

/**************************************************************
 * @ROUTE       - /dinner/get-user-history-pending
 * @METHOD      - GET
 * @DESC        - TO GET HISTORY
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get(
  "/get-user-history-pending",
  dinnerController.get_user_history_pending
);

/**************************************************************
 * @ROUTE       - /dinner/get-reservation-data
 * @METHOD      - POST
 * @DESC        - TO POST THE ALL RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get-reservation-data", dinnerController.get_reservation_data);

/**************************************************************
 * @ROUTE       - /dinner/update-dinner-order
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE DINNER ORDER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/update-dinner-order", dinnerController.update_dinner_info);

module.exports = router;

//21-12-19
