const express = require("express");
const router = express.Router();
const reservationController = require("../controller/Reservation/reservationController");
const dinnerController = require("../controller/Dinner/dinnerController");

/**************************************************************
 * @ROUTE       - /reservation/reservation
 * @METHOD      - POST
 * @DESC        - TO ADD RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/reservation",
  dinnerController.user_reservation,
  dinnerController.user_reservation_sms_twilio
);
// router.post(
//   "/reservation",
//   reservationController.reservation,
//   reservationController.send_sms_twilio
// );

// /**************************************************************
//  * @ROUTE       - /reservation/reservation-user
//  * @METHOD      - POST
//  * @DESC        - TO ADD RESERVATION
//  * @ACCESS      - PUBLIC
//  * @PROTECTION  - NONE
//  ***************************************************************/
// router.post('/reservation-user', reservationController.reservation_user,reservationController.send_sms_twilio);

/**************************************************************
 * @ROUTE       - /reservation/get-reservation
 * @METHOD      - POST
 * @DESC        - TO GET THE ALL RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get-reservation", reservationController.get_reservation);

/**************************************************************
 * @ROUTE       - /reservation/cancel-reservation
 * @METHOD      - PATCH
 * @DESC        - TO CANCEL THE  RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/cancel-reservation", reservationController.cancel_reservation);

/**************************************************************
 * @ROUTE       - /reservation/get-reservation
 * @METHOD      - GET
 * @DESC        - TO GET THE ALL RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-reservation", reservationController.get_reservation_pending);

/**************************************************************
 * @ROUTE       - /reservation/status-update
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE STATUS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/status-update", reservationController.update_status_reservation);

// /**************************************************************
//  * @ROUTE       - /reservation/status-update
//  * @METHOD      - PATCH
//  * @DESC        - TO UPDATE STATUS
//  * @ACCESS      - PUBLIC
//  * @PROTECTION  - NONE
//  ***************************************************************/
// router.patch("/status-update", reservationController.update_status_reservation);

/**************************************************************
 * @ROUTE       - /dinner/cancel-reservation
 * @METHOD      - PATCH
 * @DESC        - TO CANCEL THE  RESERVATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/cancel-reservation", reservationController.cancel_reservation);

/**************************************************************
 * @ROUTE       - /reservation/get-user-reservation
 * @METHOD      - GET
 * @DESC        - TO GET THE USER RESERVATION
 * @ACCESS      - PASSPORT
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-user-reservation", reservationController.get_user_reservation);

module.exports = router;
