const express = require("express");
const router = express.Router();

/*************************************************************
 * @DESC - PROFILE CONTROLLERS
 *************************************************************/
const profileControllers = require("../controller/CompleteProfile/profileController");
//const dashboard = require('../controller/Dashboard/dashboardController');

/**************************************************************
 * @ROUTE       - /profile/dashboard-details
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
//router.get('/dashboard-details', dashboard.get_restaurant_details);

/**************************************************************
 * @ROUTE       - /profile/dashboard-feature
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT FEATURE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
//router.get('/dashboard-feature', dashboard.get_restaurant_feature);

/**************************************************************
 * @ROUTE       - /profile/dashboard-time
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT TIME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
//router.get('/dashboard-time', dashboard.get_restaurant_Time);

/**************************************************************
 * @ROUTE       - /profile/profile
 * @METHOD      - POST
 * @DESC        - TO ADD USER PROFILE DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/profile",
  profileControllers.create_new_profile,
  profileControllers.have_submited_details_profile
);

/**************************************************************
 * @ROUTE       - /profile/about-yourself
 * @METHOD      - POST
 * @DESC        - TO ADD ABOUT YOUR SELF DETAILS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/about-yourself",
  profileControllers.about_yourself,
  profileControllers.have_submited_details_yourself
);

/**************************************************************
 * @ROUTE       - /profile/add-join-wait-list
 * @METHOD      - POST
 * @DESC        - TO ADD JOIN WAIT LIST INFO
 * @ACCESS      - PUBLIC
 * @PROTECTION  - PASSPORT
 ***************************************************************/
//router.post('/add-join-wait-list',dashboard.add_join_wait_list);

/**************************************************************
 * @ROUTE       - /profile/all-user-aboutyourself
 * @METHOD      - GET
 * @DESC        - TO GET RESTAURANT FEATURE
 * @ACCESS      - PROTECTED
 * @PROTECTION  - PASSPORT
 ***************************************************************/
router.get(
  "/all-user-aboutyourself",
  profileControllers.get_all_user_about_yourself
);

/**************************************************************
 * @ROUTE       - /profile/map
 * @METHOD      - POST
 * @DESC        - TO GET THE NEARBY RESTAURANT LIST
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/map", profileControllers.map);

/**************************************************************
 * @ROUTE       - /profile/each
 * @METHOD      - GET
 * @DESC        - TO GET EACH USER INFORMATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/each", profileControllers.get_each_user_info);

/**************************************************************
 * @ROUTE       - /profile/update-profile
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE EACH USER INFORMATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-profile",
  profileControllers.update_profile,
  profileControllers.have_submited_details_profile
);

/**************************************************************
 * @ROUTE       - /profile/update-yourself
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE EACH USER INFORMATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-yourself",
  profileControllers.update_aboutyourself,
  profileControllers.have_submited_details_yourself
);

/**************************************************************
 * @ROUTE       - /profile/get-user-info
 * @METHOD      - GET
 * @DESC        - TO GET EACH USER INFORMATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-user-info", profileControllers.get_user_info);

/**************************************************************
 * @ROUTE       - /profile/get-aboutyourself
 * @METHOD      - GET
 * @DESC        - TO GET EACH USER INFORMATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-aboutyourself", profileControllers.get_each_user_yourself);

module.exports = router;
