const express = require("express");
const router = express.Router();
const passport = require("passport");

/*************************************************************
 * @DESC - USER CONTROLLERS
 *************************************************************/
const userControllers = require("../controller/AmaelioUsers/userController");

/**************************************************************
 * @ROUTE       - /amaeliousers/sendOtp
 * @METHOD      - POST
 * @DESC        - TO SEND OTP TO REGISTER MOBILE NUMBER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/sendOtp", userControllers.send_otp);

/**************************************************************
 * @ROUTE       - /amaeliousers/signup
 * @METHOD      - POST
 * @DESC        - TO CREATE USER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post(
  "/signup",
  userControllers.check_unique_mobile_number,
  userControllers.user_signup,
  userControllers.send_otp
);

/**************************************************************
 * @ROUTE       - /amaeliousers/verifyOtp
 * @METHOD      - POST
 * @DESC        - TO VERIFY OTP TO REGISTER MOBILE NUMBER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/verifyOtp", userControllers.verify_otp);

/**************************************************************
 * @ROUTE       - /amaeliousers/update-mobile-number
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE REGISTER MOBILE NUMBER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/update-mobile-number",
  userControllers.update_mobile_number,
  userControllers.send_otp
);

/**************************************************************
 * @ROUTE       - /amaeliousers/forgot-password
 * @METHOD      - PATCH
 * @DESC        - TO SET FORGOT PASSWORD
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/forgot-password", userControllers.verify_forgot_password);

/**************************************************************
 * @ROUTE       - /amaeliousers/login
 * @METHOD      - POST
 * @DESC        - USER LOGIN
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/login", userControllers.user_login);

/**************************************************************
 * @ROUTE       - /amaeliousers/get-all-user
 * @METHOD      - GET
 * @DESC        - GET ALL REGISTER USER IN AMAELIO APP
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-all-user", userControllers.get_all_user);

/**************************************************************
 * @ROUTE       - /amaeliousers/get-each-user
 * @METHOD      - GET
 * @DESC        - GET EACH REGISTER USER IN AMAELIO APP
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-each-user", userControllers.get_each_user_info);

/**************************************************************
 * @ROUTE       - /amaeliousers/google_signup
 * @METHOD      - POST
 * @DESC        - TO SOCIAL MEDIA SIGN UP PROCESS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/google_signup", userControllers.google_user_signup);

/**************************************************************
 * @ROUTE       - /amaeliousers/mobile_number_update
 * @METHOD      - PATCH
 * @DESC        - TO UPDATE MOBILE NUMBER
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch(
  "/mobile_number_update",
  userControllers.google_user_mobile_number_update,
  userControllers.send_otp
);

/**************************************************************
 * @ROUTE       - /amaeliousers/get_user_name
 * @METHOD      - POST
 * @DESC        - TO GET USER NAME
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get_user_name", userControllers.get_user_name);

/**************************************************************
 * @ROUTE       - /amaeliousers/delete_user_account
 * @METHOD      - POST
 * @DESC        - TO DELETE ALL USER RECORD
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/delete_user_account", userControllers.delete_user_account);

/**************************************************************
 * @ROUTE       - /amaeliousers/get_user_order
 * @METHOD      - POST
 * @DESC        - TO GET USER ALL ORDERS
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/get_user_order", userControllers.get_user_order);

/**************************************************************
 * @ROUTE       - /amaeliousers/user_locked
 * @METHOD      - PATCH
 * @DESC        - TO  USER LOCKED
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/user_locked", userControllers.user_locked);

/**************************************************************
 * @ROUTE       - /amaeliousers/user_update
 * @METHOD      - PATCH
 * @DESC        - TO  USER DETAILS UPDATE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/user_update", userControllers.user_update);

/**************************************************************
 * @ROUTE       - /amaeliousers/user_password_update
 * @METHOD      - PATCH
 * @DESC        - TO  USER DETAILS UPDATE
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.patch("/user_password_update", userControllers.user_password_update);

module.exports = router;
