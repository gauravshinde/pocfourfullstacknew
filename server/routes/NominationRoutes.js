const express = require("express");
const router = express.Router();


/*************************************************************
 * @DESC - DETAILS CONTROLLERS
 *************************************************************/
const nominationController = require("../controller/Nomination/nominationController");


/**************************************************************
 * @ROUTE       - /nomination/add-nomination
 * @METHOD      - POST
 * @DESC        - TO CREATE NEW NOMINATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.post("/add-nomination",nominationController.check_unique_nomination,nominationController.vendornomination,nominationController.nomination);

/**************************************************************
 * @ROUTE       - /nomination/get-nomination
 * @METHOD      - GET
 * @DESC        - TO GET NOMINATION
 * @ACCESS      - PUBLIC
 * @PROTECTION  - NONE
 ***************************************************************/
router.get("/get-nomination", nominationController.get_all_nomination);

module.exports = router;