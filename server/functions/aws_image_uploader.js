const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");

aws.config.update({
  accessKeyId: "AKIAXRPZCMSVNNPX353P",
  secretAccessKey: "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",
  region: "us-east-1"
});

const s3 = new aws.S3();

// Filter With File Type
const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(
      new Error(
        "Invalid Format for Image Please Upload only Image for Png or Jpeg"
      ),
      false
    );
  }
};

var upload = multer({
  storage: multerS3({
    fileFilter: fileFilter,
    s3: s3,
    bucket: "chainlist",
    metadata: function(req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function(req, file, cb) {
      cb(null, Date.now().toString());
    }
  })
});

module.exports = upload;
