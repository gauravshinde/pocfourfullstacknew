const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const passport = require("passport");
const cors = require("cors");
const path = require("path");
var Countdown = require("countdown-js");

const moment = require("moment");
var date = new Date();

const socketIO = require("socket.io");

const Dinner = require("./server/models/add_dinner");
const Users = require("./server/models/User.model");
const AmaelioUsers = require("./server/models/AmaelioUser.model");
const default_time = require("./server/models/default");
const RestaurantDetails = require("./server/models/RestaurantMapDetails");

const jwt = require("jsonwebtoken");
const keys = require("./config/keys").secretIOkey;

const accountSid = "ACa7e8734f7243eb4e8b322481d1a4376a";
const authToken = "9f375582bf6960e4ec9e6c6ef5e8eeba";
const client = require("twilio")(accountSid, authToken);
const SendPush = require("./server/SendPush");

// const fcm = require("fcm-notification");
// const fcmfile = require("./server/privatekey.json");
// var FCM = new fcm(fcmfile);

/************************************
 * @DESC - MIDDLEWARE INIITILIZATION
 * @PACKAGE - EXPRESS
 ***********************************/
const app = express(),
  server = require("http").createServer(app),
  io = socketIO.listen(server);
app.use(cors());

/*************************
 *@DESC - MULTER IMAGE STORE STATIC
 **************************/
app.use("/restaurantImages", express.static("restaurantImages"));

/************************************
 * @DESC    - PARSER JSON BODY
 * @PACKAGE - body-parser
 ***********************************/
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());

/************************************
 * @DESC    - COOKIE PARSER & SESSION
 * @PACKAGE - cookie-parser & express-session
 ***********************************/
app.use(cookieParser());
app.use(
  session({
    secret: "secretkeypocfour14555444",
    resave: false,
    saveUninitialized: false,
  })
);

/************************************
 * @DESC    - DATABASE CONFIGURATION
 * @PACKAGE - mongoose
 ***********************************/
const db = require("./config/keys").mongoURI;
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => console.log("Database Connected Successfully!!!"))
  .catch((err) => console.log("Error while connecting to the database" + err));

/************************************
 * @DESC    - PASSPORT INITLIZATION
 * @PACKAGE - passport
 ***********************************/
app.use(passport.initialize());
require("./config/passport")(passport);

/************************************
 * @DESC    -  ROUTERS
 * @PACKAGE -  EXPRESS
 ***********************************/
const userRouter = require("./server/routes/userRoutes");
const iconRouter = require("./server/routes/iconRoutes");
const addDetailsRouter = require("./server/routes/addDetailsRoutes");
const imageUploaderForPost = require("./server/routes/imageUploaderRoute");
const dinnerRouter = require("./server/routes/add_dinner");
const reservationRouter = require("./server/routes/reservationRoute");
const menuRouter = require("./server/routes/MenuRoutes");
const AmaeliouserRoutes = require("./server/routes/AmaeliouserRoutes");
const profileRouter = require("./server/routes/profileRoutes");
const joinwaitlistRoutes = require("./server/routes/joinwaitlistRoutes");
const eventRoutes = require("./server/routes/EventRoutes");
const nominationRoutes = require("./server/routes/NominationRoutes");
const reviewRoutes = require("./server/routes/ReviewRateingRoutes");
const raiseRoute = require("./server/routes/RaiseTicketRoute");

/************************************
 * @DESC    -  ROUTES
 * @PACKAGE -  EXPRESS
 ***********************************/
app.use("/users", userRouter);
app.use("/icons", iconRouter);
app.use("/details", addDetailsRouter);
app.use("/image/", imageUploaderForPost);
app.use("/reservation", reservationRouter);
app.use("/menu", menuRouter);
app.use("/amaeliousers", AmaeliouserRoutes);
app.use("/profile", profileRouter);
app.use("/waitlist", joinwaitlistRoutes);
app.use("/event", eventRoutes);
app.use("/nomination", nominationRoutes);
app.use("/review", reviewRoutes);
app.use("/raise", raiseRoute);

/*************************
 * @DESC POCONE ROUTES
 ************************/
app.use("/dinner", dinnerRouter);

// SET STATIC FOLDER FOR PRODUCTTION BUILD
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
  app.use("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

io.on("connection", (socket) => {
  socket.on("subscribeData", (data) => {
    let currentdate = moment(new Date()).format("DD-MM-YYYY");
    let nextday = moment(currentdate, "DD-MM-YYYY")
      .add(1, "days")
      .format("DD-MM-YYYY");
    Dinner.find({
      vendor_id: data,
      $and: [
        {
          $or: [
            {
              dinner_status: "Nonseated",
            },
            {
              dinner_status: "Seated",
            },
          ],
        },
        {
          $or: [
            {
              booking_date: currentdate,
            },
            {
              booking_date: nextday,
            },
            {
              Date: currentdate,
            },
            {
              Date: nextday,
            },
          ],
        },
      ],
    })
      .exec()
      .then((DinnerList) => {
        let basic = DinnerList.reverse();
        io.emit(["getSeatingData", data], basic);
      });
  });
  socket.on("usersubscribeDataWaitlist", (data) => {
    const waitid = jwt.verify(data.split(" ")[1], keys)._id;
    Dinner.find({
      user_id: waitid,
      order_type: "waitlist",
    })
      .exec()
      .then((UserDinnerList) => {
        io.emit(["wait", waitid], UserDinnerList);
      });
  });
  socket.on("usersubscribeDatawalkin", (data) => {
    const walkid = jwt.verify(data.split(" ")[1], keys)._id;
    Dinner.find({
      user_id: walkid,
      order_type: "walkin",
    })
      .exec()
      .then((UserDinnerList) => {
        io.emit(["walk", walkid], UserDinnerList);
      });
  });
  socket.on("usersubscribeDataReservation", (data) => {
    const reservationid = jwt.verify(data.split(" ")[1], keys)._id;
    Dinner.find({
      user_id: reservationid,
      order_type: "reservation",
    })
      .exec()
      .then((UserDinnerList) => {
        io.emit(["reservation", reservationid], UserDinnerList);
      });
  });
  socket.on("pendingData", (data) => {
    Dinner.find({
      vendor_id: data,
      order_type: "reservation",
      status: "Pending",
    })
      .exec()
      .then((UserDinnerList) => {
        let basic = UserDinnerList.reverse();
        io.emit(["reservationPending", data], basic);
      });
  });
  socket.on("updateData", async (data) => {
    let data1 = await Dinner.findByIdAndUpdate(
      {
        _id: data.dinnerId,
      },
      {
        $set: {
          dinner_status: data.dinner_status,
          Seating_Date: Date.now(),
        },
      }
    );
    let get_vendor_id = await RestaurantDetails.findOne({
      vendor_id: data1.vendor_id,
    });

    let get_user_email = await AmaelioUsers.findOne({
      _id: data.user_id,
    });
    if (data1.order_type === "waitlist") {
      if (data1.order_type === "waitlist" && data.dinner_status === "Seated") {
        await Dinner.findByIdAndUpdate(
          {
            _id: data.dinnerId,
          },
          {
            $set: {
              wait_time: moment(new Date(), "DD-MM-YYYY hh:mm:ss A").format(
                "DD-MM-YYYY hh:mm:ss A"
              ),
            },
          }
        );
        client.messages
          .create({
            body:
              "Bon Appetit! " +
              data1.name +
              " You have now been seated at " +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "WaitlistTrack",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                "Bon Appetit! " +
                data1.name +
                " You have now been seated on " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        if (data1.isVendor) {
          messagepush = {
            data: {
              Route: "",
              id: data.dinnerId.toString(),
            },
            notification: {
              title: "Amealio App",
              body: data1.name + "You have a new request",
            },
            token: data1.token,
          };
        }
        SendPush.sendPush(messagepush);
        // FCM.send(messagepush, (err, response) => {});
      } else if (
        data1.order_type === "waitlist" &&
        data.dinner_status === "Completed"
      ) {
        client.messages
          .create({
            body:
              data1.name +
              "You have been completed. We hope your have enjoyed our food. Visit us again to " +
              data1.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        const msg = {
          to: get_user_email.email,
          from: "support@amealio.com",
          subject: "Thank You",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Four</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style=" text-align: center; width: 100%;"> <img style="width: 231.77px;" src="https://iconslist.s3.amazonaws.com/thankyou-usertemplate.png" /> <p style="margin:30px 0px 30px 0px; font-size:12px; color:#707070; font-family:"roboto"; font-weight: bold;" > Thank you for using Amealio app<br /> For ' +
            data1.order_type +
            " service at " +
            get_vendor_id.restaurant_name +
            ' place<br /> We thrive to get your feedback to improve experience </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";"> We hope we were able to make your dining out experience<br/> easy and convenient. Thank you for your patronage, please<br/> take a moment to review our app on App Store link<br/> And refer your friends to enjoy this convenience. </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        // sgmail.send(msg);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "WaitlistTrack",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                data1.name +
                "You have been completed. We hope your have enjoyed our food. Visit us again to " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        if (data1.isVendor) {
          messagepush = {
            data: {
              Route: "",
              id: data.dinnerId.toString(),
            },
            notification: {
              title: "Amealio App",
              body: data1.name + "You have a new request",
            },
            token: data1.token,
          };
        }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      } else if (
        data1.order_type === "waitlist" &&
        data.dinner_status === "Cancelled"
      ) {
        let currentdate = moment(new Date()).format("DD-MM-YYYY");
        let nextday = moment(currentdate, "DD-MM-YYYY")
          .add(1, "days")
          .format("DD-MM-YYYY");
        Dinner.find({
          vendor_id: data.vendor_id,
          $and: [
            {
              $or: [
                {
                  dinner_status: "Nonseated",
                },
                {
                  dinner_status: "Seated",
                },
              ],
            },
            {
              $or: [
                {
                  booking_date: currentdate,
                },
                {
                  booking_date: nextday,
                },
                {
                  Date: currentdate,
                },
              ],
            },
          ],
        })
          .exec()
          .then((UserDinnerList) => {
            let vId = data.vendor_id;
            io.emit(["getSeatingData", vId], UserDinnerList.reverse());
          });

        var waitid = data.user_id;

        Dinner.find({
          user_id: data1.user_id,
          order_type: data1.order_type,
        })
          .exec()
          .then((UserDinnerList) => {
            io.emit(["wait", waitid], UserDinnerList);
          });
        client.messages
          .create({
            body:
              "Oops, we're sorry!" +
              data1.name +
              " Your waitlist request is rejected on.Request again to" +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        const msg = {
          to: get_user_email.email,
          from: "support@amealio.com",
          subject: "Request Cancel",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Five</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style="padding:10px; text-align: center; width: 100%;"> <img style="width: 183.59px;" src="https://iconslist.s3.amazonaws.com/cancel-usertemplate.png" /> <p style="margin:25px 0px 30px 0px; font-size:12px; color:#707070; font-family:"roboto"; font-weight: bold;" > Thank you for using Amealio app<br/> For ' +
            data1.order_type +
            " service at " +
            get_vendor_id.restaurant_name +
            " place<br/> Your request " +
            data1.order_type +
            ' Has been cancelled </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";"> We hope to make your dining out experience easy and<br/> convenient. Thank you for your patronage, please take a<br/> moment to review our app on App Store link<br/> And refer your friends to enjoy this convenience next time. </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        // sgmail.send(msg);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "WaitlistTrack",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                "Oops, we're sorry!" +
                data1.name +
                " Your waitlist request is rejected. Request again for." +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        let find_user_data = await Users.findOne({
          _id: data.vendor_id,
        });
        if (data.isVendor) {
          messagepush = {
            data: {
              Route: "seating",
              id: data.vendor_id.toString(),
            },
            notification: {
              title: "Vendor App",
              body: `${data1.name} has cancelled Waitlist Request.`,
            },
            token: find_user_data.FCMtoken,
          };
        }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      }
    } else if (data1.order_type === "walkin") {
      if (data1.order_type === "walkin" && data.dinner_status === "Seated") {
        client.messages
          .create({
            body:
              "Bon Appetit!" +
              data1.name +
              " You have now been seated at " +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "WaitlistTrack",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                "Bon Appetit! " +
                data1.name +
                " You have now been seated at " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        if (data1.isVendor) {
          messagepush = {
            data: {
              Route: "",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body: data1.name + "You have a new request",
            },
            token: data1.token,
          };
        }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      } else if (
        data1.order_type === "walkin" &&
        data.dinner_status === "Completed"
      ) {
        client.messages
          .create({
            body:
              data1.name +
              "You have been completed. We hope your have enjoyed our food. Visit us again to" +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        const msg = {
          to: get_user_email.email,
          from: "support@amealio.com",
          subject: "Thank You",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Four</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style=" text-align: center; width: 100%;"> <img style="width: 231.77px;" src="https://iconslist.s3.amazonaws.com/thankyou-usertemplate.png" /> <p style="margin:30px 0px 30px 0px; font-size:12px; color:#707070; font-family:"roboto"; font-weight: bold;" > Thank you for using Amealio app<br /> For ' +
            data1.order_type +
            " service at " +
            get_vendor_id.restaurant_name +
            ' place<br /> We thrive to get your feedback to improve experience </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";"> We hope we were able to make your dining out experience<br/> easy and convenient. Thank you for your patronage, please<br/> take a moment to review our app on App Store link<br/> And refer your friends to enjoy this convenience. </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        // sgmail.send(msg);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "Walkin",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                data1.name +
                "You have been completed. We hope your have enjoyed our food. Visit us again to " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }

        if (data1.isVendor) {
          messagepush = {
            data: {
              Route: "",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body: data1.name + "You have a new request",
            },
            token: data1.token,
          };
        }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      } else if (
        data1.order_type === "walkin" &&
        data.dinner_status === "Cancelled"
      ) {
        client.messages
          .create({
            body:
              "Oops, we're sorry!" +
              data1.name +
              " Your waitlist request is rejected. Request again for " +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        const msg = {
          to: get_user_email.email,
          from: "support@amealio.com",
          subject: "Request Cancel",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Five</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style="padding:10px; text-align: center; width: 100%;"> <img style="width: 183.59px;" src="https://iconslist.s3.amazonaws.com/cancel-usertemplate.png" /> <p style="margin:25px 0px 30px 0px; font-size:12px; color:#707070; font-family:"roboto"; font-weight: bold;" > Thank you for using Amealio app<br/> For ' +
            data1.order_type +
            " service at " +
            get_vendor_id.restaurant_name +
            " place<br/> Your request " +
            data1.order_type +
            ' Has been cancelled </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";"> We hope to make your dining out experience easy and<br/> convenient. Thank you for your patronage, please take a<br/> moment to review our app on App Store link<br/> And refer your friends to enjoy this convenience next time. </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        // sgmail.send(msg);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "Walkin",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                "Oops, we're sorry!" +
                data1.name +
                " Your waitlist request is rejected. Request again for " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        let find_user_data = await Users.findOne({
          _id: data.vendor_id,
        });
        if (data1.isVendor) {
          messagepush = {
            data: {
              Route: "",
              id: data.dinnerId,
            },
            notification: {
              title: "Vendor App",
              body: data1.name + "You have a new request",
            },
            token: find_user_data.FCMtoken,
          };
        }

        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      }
      var walkid = data.user_id;
      Dinner.find({
        user_id: data1.user_id,
        order_type: data1.order_type,
      })
        .exec()
        .then((UserDinnerList) => {
          io.emit(["walk", walkid], UserDinnerList);
        });
    } else if (data1.order_type === "reservation") {
      if (
        data1.order_type === "reservation" &&
        data.dinner_status === "Seated"
      ) {
        client.messages
          .create({
            body:
              "Bon Appetit!" +
              data1.name +
              " You have now been seated  at " +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "ReservationTrack",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                "Bon Appetit!" +
                data1.name +
                " You have now been seated at " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        if (data1.isVendor) {
          messagepush = {
            data: {
              Route: "",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body: data1.name + "You have a new request",
            },
            token: data1.token,
          };
        }

        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      } else if (
        data1.order_type === "reservation" &&
        data.dinner_status === "Completed"
      ) {
        client.messages
          .create({
            body:
              data1.name +
              "You have been completed. We hope your have enjoyed our food. Visit us again to " +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        const msg = {
          to: get_user_email.email,
          from: "support@amealio.com",
          subject: "Thank You",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Four</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style=" text-align: center; width: 100%;"> <img style="width: 231.77px;" src="https://iconslist.s3.amazonaws.com/thankyou-usertemplate.png" /> <p style="margin:30px 0px 30px 0px; font-size:12px; color:#707070; font-family:"roboto"; font-weight: bold;" > Thank you for using Amealio app<br /> For ' +
            data1.order_type +
            " service at " +
            get_vendor_id.restaurant_name +
            ' place<br /> We thrive to get your feedback to improve experience </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";"> We hope we were able to make your dining out experience<br/> easy and convenient. Thank you for your patronage, please<br/> take a moment to review our app on App Store link<br/> And refer your friends to enjoy this convenience. </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        // sgmail.send(msg);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "ReservationTrack",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                data1.name +
                "You have been completed. We hope your have enjoyed our food. Visit us again " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        if (data1.isVendor) {
          messagepush = {
            data: {
              Route: "",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body: data1.name + "You have a new request",
            },
            token: data1.token,
          };
        }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      } else if (
        data1.order_type === "reservation" &&
        data.dinner_status === "Cancelled"
      ) {
        client.messages
          .create({
            body:
              "Oops, we're sorry!" +
              data1.name +
              " Your waitlist request is rejected. Request again for " +
              get_vendor_id.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: data1.phone_number,
          })
          .then((message) => message.sid);
        const msg = {
          to: get_user_email.email,
          from: "support@amealio.com",
          subject: "Request Cancel",
          html:
            '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Five</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/logo.png" /> <div style="padding:10px; text-align: center; width: 100%;"> <img style="width: 183.59px;" src="https://iconslist.s3.amazonaws.com/cancel-usertemplate.png" /> <p style="margin:25px 0px 30px 0px; font-size:12px; color:#707070; font-family:"roboto"; font-weight: bold;" > Thank you for using Amealio app<br/> For ' +
            data1.order_type +
            " service at " +
            get_vendor_id.restaurant_name +
            " place<br/> Your request " +
            data1.order_type +
            ' Has been cancelled </p> <p style="margin:5px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";"> We hope to make your dining out experience easy and<br/> convenient. Thank you for your patronage, please take a<br/> moment to review our app on App Store link<br/> And refer your friends to enjoy this convenience next time. </p> <table style=" width:80%; margin:10px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/g.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="https://voiceicons.s3.ap-south-1.amazonaws.com/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
        };
        // sgmail.send(msg);
        var messagepush = {};
        if (!data1.isVendor) {
          messagepush = {
            data: {
              Route: "ReservationTrack",
              id: data.dinnerId,
            },
            notification: {
              title: "Amealio App",
              body:
                "Oops, we're sorry!" +
                data1.name +
                " Your waitlist request is rejected. Request again for " +
                get_vendor_id.restaurant_name,
            },
            token: data1.token,
          };
        }
        let find_user_data = await Users.findOne({
          _id: data.vendor_id,
        });
        if (data.isVendor) {
          messagepush = {
            data: {
              Route: "seating",
              id: data.vendor_id.toString(),
            },
            notification: {
              title: "Vendor App",
              body: `${data1.name} has cancelled Reservation Request for ${data1.Date} at ${data1.time}.`,
            },
            token: find_user_data.FCMtoken,
          };
        }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
      }
      var reservationid = data.user_id;
      Dinner.find({
        user_id: data1.user_id,
        order_type: data1.order_type,
      })
        .exec()
        .then((UserDinnerList) => {
          io.emit(["reservation", reservationid], UserDinnerList);
        });
    }
  });
  socket.on("reservationStatusUpdate", async (data) => {
    if (data.status === "Accepted") {
      let a = await Dinner.findByIdAndUpdate(
        {
          _id: data.id,
        },
        {
          $set: {
            status: data.status,
            ETA: data.ETA ? data.ETA : "",
            reject_reasons: data.reject_reasons ? data.reject_reasons : "",
            enter_reasons: data.enter_reasons ? data.enter_reasons : "",
            Seating_Date: Date.now(),
            dinner_status: "Nonseated",
          },
        }
      );
      if (a) {
        client.messages
          .create({
            body:
              "Yay! " +
              a.name +
              " Your reservation request is accepted by " +
              a.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: a.phone_number,
          })
          .then((message) => message.sid);
        var messagepush = {};
        if (!data.isVendor) {
          console.log("myven", a);
          messagepush = {
            data: {
              Route: "ReservationTrack",
              id: data.id,
            },
            notification: {
              title: "Amealio App",
              body: `Your Reservation request with ${a.restaurant_name} for ${a.Date} at ${a.time} has been accepted.`,
              // html:
              //   '<!DOCTYPE html><html> <head> <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" /> <title>Email Template Six</title> </head> <body style="background-color: rgba(0,0,0,0.1);"> <div style="background-color: #fff; width:360px; height: 640px; margin:0px auto;" > <img style="padding: 15px 0px 0px 15px; width: 78px;" src="img/logo.png" /> <div style="padding:10px; text-align: center; width: 100%;"> <img style="width: 193.49px;" src="https://iconslist.s3.amazonaws.com/confirm-usertemplate.png" /> <p style="margin:20px 0px 20px 0px; font-size:12px; color:#707070; font-family:"roboto"; font-weight: bold;" > Thank you for using Amealio app<br/> For ' +
              //   a.order_type +
              //   " service at " +
              //   a.restaurant_name +
              //   ' place </p> <p style=" font-size:10px; color:#707070; font-family:"roboto"; font-weight: bold;" > Your Reservation request Has been confirmed<br/> For ' +
              //   a.Date +
              //   " at " +
              //   a.time +
              //   ' </p> <p style="margin:17px 0px 30px 0px; font-size:8px; color:#707070; font-family:"roboto";"> We hope to make your dining out experience easy and<br/> convenient. Thank you for your patronage, please take a<br/> moment to review our app on App Store link<br/> And refer your friends to enjoy this convenience next time. </p> <table style=" width:80%;margin: 0px auto; margin-top:64px; text-align:center;" > <tr> <td width="33%"> <p style="font-weight:600; font-size:8px; font-family:"roboto";"> Love us more? <span style="color:#EE3A23">Star us on</span> </p> </td> <td width="33%"> <img style="width: 65px;" src="img/google.png" /> </td> <td width="33%"> <img style="width: 65px; margin-left: -40px;" src="img/apple.png" /> </td> </tr> </table> </div> <div style="text-align: center; padding: 20px 40px 20px 40px; background-color: #B5B5B53C;" > <table style="margin: 0px auto; text-align: center;"> <tr> <td> <img style="width: 26px; padding:5px;" src="img/facebook.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="img/google-plus.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="img/instagram.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="img/twitter.png" /> </td> <td> <img style="width: 26px; padding:5px;" src="img/whatsapp.png" /> </td> </tr> </table> <p style="text-align: center; font-weight:600; margin:15px auto; font-size:6px; color:#707070; font-family:"roboto";" > Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p> </div> </div> </body></html>',
            },
            token: a.token,
          };
        }
        // if (a.isVendor) {
        //   messagepush = {
        //     data: {
        //       Route: "",
        //       id: data.dinnerId,
        //     },
        //     notification: {
        //       title: "Amealio App",
        //       body: "You have a new request by " + a.name,
        //     },
        //     token: data1.token,
        //   };
        // }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
        Dinner.find({
          vendor_id: a.vendor_id,
          order_type: "reservation",
          status: "Pending",
        })
          .exec()
          .then((UserDinnerList) => {
            let basic = UserDinnerList.reverse();
            let vId = a.vendor_id;
            io.emit(["reservationPending", vId], basic);
          });

        var reservationid = data.user_id;
        Dinner.find({
          user_id: data.user_id,
          order_type: "reservation",
        })
          .exec()
          .then((UserDinnerList) => {
            io.emit(["reservation", reservationid], UserDinnerList);
          });
      }
    } else if (data.status === "Cancelled") {
      let b = await Dinner.findByIdAndUpdate(
        {
          _id: data.id,
        },
        {
          $set: {
            status: data.status,
            ETA: data.ETA ? data.ETA : "",
            reject_reasons: data.reject_reasons ? data.reject_reasons : "",
            enter_reasons: data.enter_reasons ? data.enter_reasons : "",
            Seating_Date: Date.now(),
            dinner_status: "Cancelled",
          },
        }
      );
      if (b) {
        client.messages
          .create({
            body:
              "Oops! " +
              b.name +
              " Your reservation request is rejected by " +
              b.restaurant_name,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: b.phone_number,
          })
          .then((message) => message.sid);
        var messagepush = {};
        if (!data.isVendor) {
          messagepush = {
            data: {
              Route: "ReservationTrack",
              id: data.id,
            },
            notification: {
              title: "Amealio App",
              body:
                "Uh-oh! " +
                b.name +
                " Your reservation request is rejected by " +
                b.restaurant_name,
            },
            token: b.token,
          };
        }

        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
        Dinner.find({
          vendor_id: b.vendor_id,
          order_type: "reservation",
          status: "Pending",
        })
          .exec()
          .then((UserDinnerList) => {
            let basic = UserDinnerList.reverse();
            let vId = b.vendor_id;
            io.emit(["reservationPending", vId], basic);
          });
        var reservationid = data.user_id;
        Dinner.find({
          user_id: data.user_id,
          order_type: "reservation",
        })
          .exec()
          .then((UserDinnerList) => {
            io.emit(["reservation", reservationid], UserDinnerList);
          });
      }
    }
  });
  socket.on("updateWaitTimeData", async (data) => {
    var newdate = moment(new Date(), "DD/MM/YYYY hh:mm:ss A")
      .add(data.wait_time * 60, "seconds")
      .format("DD/MM/YYYY hh:mm:ss A");
    console.log("Updatedata", data);
    let updateDinnerWaitTime = await Dinner.findOneAndUpdate(
      {
        _id: data._id,
      },
      {
        $set: {
          wait_time: newdate,
          ETA: data.wait_time,
        },
      }
    );
    if (updateDinnerWaitTime.order_type === "waitlist") {
      var waitid = updateDinnerWaitTime.user_id;
      Dinner.find({
        user_id: updateDinnerWaitTime.user_id,
        order_type: updateDinnerWaitTime.order_type,
      })
        .exec()
        .then((UserDinnerList) => {
          io.emit(["wait", waitid], UserDinnerList);
        });
    } else if (updateDinnerWaitTime.order_type === "walkin") {
      var walkid = updateDinnerWaitTime.user_id;
      Dinner.find({
        user_id: updateDinnerWaitTime.user_id,
        order_type: updateDinnerWaitTime.order_type,
      })
        .exec()
        .then((UserDinnerList) => {
          io.emit(["walk", walkid], UserDinnerList);
        });
    }

    let currentdate = moment(new Date()).format("DD-MM-YYYY");
    let nextday = moment(currentdate, "DD-MM-YYYY")
      .add(1, "days")
      .format("DD-MM-YYYY");
    Dinner.find({
      vendor_id: data.vendor_id,
      $and: [
        {
          $or: [
            {
              dinner_status: "Nonseated",
            },
            {
              dinner_status: "Seated",
            },
          ],
        },
        {
          $or: [
            {
              booking_date: currentdate,
            },
            {
              booking_date: nextday,
            },
            {
              Date: currentdate,
            },
          ],
        },
      ],
    })
      .exec()
      .then((UserDinnerList) => {
        console.log("updatedddddataaaaaaaaaaaa", UserDinnerList);
        let vId = data.vendor_id;
        let basic = UserDinnerList.reverse();
        io.emit(["getSeatingData", vId], basic);
      });

    client.messages
      .create({
        body:
          "You've been reassigned on the waitlist. Your new wait time is" +
          data.wait_time +
          "mins.",
        messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
        form: "+14152003837",
        to: updateDinnerWaitTime.phone_number,
      })
      .then((message) => res.json(message.sid));
    let find_user_data = await Users.findOne({
      _id: data.vendor_id,
    });
    var messagepush = {};
    if (!data.isVendor) {
      messagepush = {
        notification: {
          title: "Amealio App",
          body:
            "You've been reassigned on the waitlist. Your new wait time is" +
            data.wait_time +
            "mins.",
        },
        token: updateDinnerWaitTime.token,
      };
      SendPush.sendPush(messagepush);
    }
    console.log("Finduserdata", find_user_data);
    if (
      data.isVendor &&
      updateDinnerWaitTime.order_type === "waitlist" &&
      find_user_data.FCMtoken
    ) {
      console.log("Myvendoretadata", data);
      messagepush = {
        data: {
          Route: "seating",
          id: data.vendor_id,
        },
        notification: {
          title: "Vendor App",
          body: `${updateDinnerWaitTime.name} has updated ETA, will be arriving in ${data.wait_time} minutes.`,
        },
        token: find_user_data.FCMtoken,
      };
      SendPush.sendPush(messagepush);
    }
    // FCM.send(messagepush, (err, response) => {});
  });
  socket.on("postWalkinData", async (postWalkin) => {
    let user_id = postWalkin.user_id;

    let user_find = await Dinner.find({
      user_id: postWalkin.user_id,
      vendor_id: postWalkin.vendor_id,
      dinner_status: "Nonseated",
    });

    if (user_find == 0) {
      console.log("addwalklist");
      let dinner = new Dinner({
        vendor_id: postWalkin.vendor_id,
        user_id: postWalkin.user_id,
        order_type: "walkin",
        orderId: postWalkin.orderId,
        requestNumber: Math.floor(Math.random() * 10000000000),
        entry_point: postWalkin.entry_point,
        name: postWalkin.name ? postWalkin.name : "",
        phone_number: postWalkin.phone_number ? postWalkin.phone_number : "",
        adults: postWalkin.adults ? postWalkin.adults : "0",
        kids: postWalkin.kids ? postWalkin.kids : "0",
        highchair: postWalkin.highchair ? postWalkin.highchair : "0",
        handicap: postWalkin.handicap ? postWalkin.handicap : "0",
        seating_preference: postWalkin.seating_preference
          ? postWalkin.seating_preference
          : "",
        total: parseInt(postWalkin.adults) + parseInt(postWalkin.kids),
        wait_time: "0",
        special_occassion: postWalkin.special_occassion
          ? postWalkin.special_occassion
          : "",
        Request_Date: Date.now(),
        Seating_Date: Date.now(),
        booking_date: moment(new Date(), "DD-MM-YYYY hh:mm:ss A").format(
          "DD-MM-YYYY"
        ),
        Date: postWalkin.Date ? postWalkin.Date : "",
        time: postWalkin.time ? postWalkin.time : "",
        ETA: postWalkin.ETA ? postWalkin.ETA : "",
        dinner_status: "Nonseated",
        reservation_status: "Nonseated",
        token: postWalkin.token,
        restaurant_name: postWalkin.restaurant_name
          ? postWalkin.restaurant_name
          : "",
      });

      let newdinner = await dinner.save();
      io.emit(["walkin", user_id], "Success");
      let find_user_data = await Users.findOne({
        _id: newdinner.vendor_id,
      });
      if (newdinner) {
        client.messages.create({
          body:
            newdinner.name +
            " You have been added to walkin at " +
            newdinner.restaurant_name,
          messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
          form: "+14152003837",
          to: newdinner.phone_number,
        });

        var messagepush = {};
        if (postWalkin.isVendor) {
          messagepush = {
            data: {
              Route: "seating",
              id: postWalkin.vendor_id.toString(),
            },
            notification: {
              title: "Vendor App",
              body:
                "You received a Walkin request from " + postWalkin.name + ".",
            },
            token: find_user_data.FCMtoken,
          };
        }
        // FCM.send(messagepush, (err, response) => {});
        SendPush.sendPush(messagepush);
        Dinner.find({
          vendor_id: newdinner.vendor_id,
          $or: [
            {
              dinner_status: "Nonseated",
            },
            {
              dinner_status: "Seated",
            },
          ],
        })
          .exec()
          .then((DinnerList) => {
            let basic = DinnerList.reverse();
            let data = postWalkin.vendor_id;
            io.emit(["getSeatingData", data], basic);
          });
      }
    } else {
      io.emit(["walkin", user_id], "Error");
    }
  });
  socket.on("postWaitlistData", async (postWaitlist) => {
    let user_id = postWaitlist.user_id;
    let default_time_data = await default_time.findOne({
      vendor_id: postWaitlist.vendor_id,
    });
    let user_find = await Dinner.find({
      user_id: postWaitlist.user_id,
      vendor_id: postWaitlist.vendor_id,
      dinner_status: "Nonseated",
    });
    var newdate = moment(new Date(), "DD-MM-YYYY hh:mm:ss A")
      .add(default_time_data.default_time * 60, "seconds")
      .format("DD-MM-YYYY hh:mm:ss A");
    if (user_find == 0) {
      console.log("addwaitlist");
      let newJoinList = new Dinner({
        user_id: postWaitlist.user_id,
        order_type: "waitlist",
        entry_point: postWaitlist.entry_point,
        wait_time: newdate,
        orderId: postWaitlist.orderId,
        requestNumber: Math.floor(Math.random() * 10000000000),
        default_wait_time: default_time_data.default_time,
        vendor_id: postWaitlist.vendor_id,
        adults: postWaitlist.adults ? postWaitlist.adults : 0,
        kids: postWaitlist.kids ? postWaitlist.kids : 0,
        total: parseInt(postWaitlist.adults) + parseInt(postWaitlist.kids),
        handicap: postWaitlist.handicap ? postWaitlist.handicap : 0,
        highchair: postWaitlist.highchair ? postWaitlist.highchair : 0,
        seating_preference: postWaitlist.seating_preference
          ? postWaitlist.seating_preference
          : [],
        restaurant_name: postWaitlist.restaurant_name
          ? postWaitlist.restaurant_name
          : "",
        special_occassion: postWaitlist.special_occassion
          ? postWaitlist.special_occassion
          : "",
        ETA: postWaitlist.ETA ? postWaitlist.ETA : "0",
        dinner_status: "Nonseated",
        Request_Date: Date.now(),
        Seating_Date: Date.now(),
        booking_date: moment(new Date(), "DD-MM-YYYY hh:mm:ss A").format(
          "DD-MM-YYYY"
        ),
        name: postWaitlist.name,
        phone_number: postWaitlist.phone_number,
        time: postWaitlist.time ? postWaitlist.time : "",
        token: postWaitlist.token,
      });
      let saveData = await newJoinList.save();
      if (saveData) {
        let currentdate = moment(new Date()).format("DD-MM-YYYY");
        let nextday = moment(currentdate, "DD-MM-YYYY")
          .add(1, "days")
          .format("DD-MM-YYYY");
        Dinner.find({
          vendor_id: postWaitlist.vendor_id,
          $and: [
            {
              $or: [
                {
                  dinner_status: "Nonseated",
                },
                {
                  dinner_status: "Seated",
                },
              ],
            },
            {
              $or: [
                {
                  booking_date: currentdate,
                },
                {
                  booking_date: nextday,
                },
                {
                  Date: currentdate,
                },
              ],
            },
          ],
        })
          .exec()
          .then((DinnerList) => {
            let basic = DinnerList.reverse();
            let data = postWaitlist.vendor_id;
            console.log("postvendoriddd", data);
            io.emit(["getSeatingData", data], basic);
          });
      }
      io.emit(["waitlist", user_id], "Success");
      let find_user_data = await Users.findOne({
        _id: postWaitlist.vendor_id,
      });

      var messagepush1 = {};
      var ten_days = 1000 * 60 * default_time_data.default_time;
      var end = new Date(new Date().getTime() + ten_days);
      var timer = Countdown.timer(end, async (timeLeft) => {
        let min = timeLeft.minutes;
        let sec = timeLeft.seconds;
        if (min + ":" + sec === 6 + ":" + 00) {
          var messagepush = {
            data: {
              Route: "WaitlistTrack",
              id: saveData._id.toString(),
            },
            notification: {
              title: "Amealio App",
              body:
                "Hey! We are eagerly waiting for you. Please update us for your arival time.",
            },
            token: postWaitlist.token,
          };
          // FCM.send(messagepush, (err, response) => {});
          SendPush.sendPush(messagepush);
        }
      });
      if (postWaitlist.isVendor) {
        messagepush1 = {
          data: {
            Route: "seating",
            id: saveData._id.toString(),
          },
          notification: {
            title: "Vendor App",
            body:
              "You received a Waitlist request from " + postWaitlist.name + ".",
          },
          // android: {
          //   notification: {
          //     title: "test",
          //     body: "hello world",
          //     channel_id: "AmealioChannel",
          //   },
          // },
          token: find_user_data.FCMtoken,
        };
      }
      // FCM.send(messagepush1, (err, response) => {});
      SendPush.sendPush(messagepush1);
    } else {
      io.emit(["waitlist", user_id], "Error");
    }
  });
  socket.on("postReservationData", async (postReservation) => {
    console.log("Postreservationdataaaaaaaaaaaa", postReservation);
    let user_id = postReservation.user_id;
    let user_find = await Dinner.find({
      user_id: postReservation.user_id,
      vendor_id: postReservation.vendor_id,
      order_type: "reservation",
      Date: postReservation.Date,
      time: postReservation.time,
    });
    if (user_find.length >= 1) {
      for (let i = 0; i < user_find.length; i++) {
        if (
          user_find[i].Date == postReservation.Date &&
          user_find[i].time == postReservation.time
        ) {
          if ((i = 1)) {
            io.emit(["reservation", user_id], "Error");
          }
        } else {
          console.log("addreservationlist");
          let dinner = new Dinner({
            vendor_id: postReservation.vendor_id,
            user_id: postReservation.user_id,
            order_type: "reservation",
            entry_point: postReservation.entry_point
              ? postReservation.entry_point
              : "",
            name: postReservation.name ? postReservation.name : "",
            phone_number: postReservation.phone_number
              ? postReservation.phone_number
              : "",
            adults: postReservation.adults ? postReservation.adults : 0,
            kids: postReservation.kids ? postReservation.kids : 0,
            highchair: postReservation.highchair
              ? postReservation.highchair
              : 0,
            handicap: postReservation.handicap ? postReservation.handicap : 0,
            seating_preference: postReservation.seating_preference
              ? postReservation.seating_preference
              : "",
            total:
              parseInt(postReservation.adults) + parseInt(postReservation.kids),
            wait_time: postReservation.wait_time
              ? postReservation.wait_time
              : "",
            special_occassion: postReservation.special_occassion
              ? postReservation.special_occassion
              : "",
            Request_Date: Date.now(),
            Seating_Date: Date.now(),
            orderId: postReservation.orderId,
            requestNumber: Math.floor(Math.random() * 10000000000),
            Date: postReservation.Date ? postReservation.Date : "",
            time: postReservation.time ? postReservation.time : "",
            ETA: postReservation.ETA ? postReservation.ETA : "",
            restaurant_name: postReservation.restaurant_name
              ? postReservation.restaurant_name
              : "",
            dinner_status: "Pending",
            status: "Pending",
            register_date: Date.now(),
            token: postReservation.token ? postReservation.token : "",
          });
          let newdinner = await dinner.save();
          io.emit(["reservation", user_id], "Success");
          client.messages.create({
            body:
              "Yes! " +
              newdinner.name +
              " We've received your reservation request. Your request is pending confirmation by." +
              newdinner.restaurant_name +
              " at " +
              newdinner.Date +
              " " +
              newdinner.time,
            messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
            form: "+14152003837",
            to: newdinner.phone_number,
          });

          let find_user_data = await Users.findOne({
            _id: postReservation.vendor_id,
          });
          let todayDate = moment(new Date()).format("DD-MM-YYYY-HH-mm");
          let reserve =
            moment(newdinner.Date, "DD-MM-YYYY").format("DD-MM-YYYY") +
            "-" +
            moment(newdinner.time, "h:mm A").format("HH-mm");

          var ms = moment(reserve, "DD-MM-YYYY-HH-mm").diff(
            moment(todayDate, "DD-MM-YYYY-HH-mm-ss")
          );
          var messagepush1 = {};
          var secs = Math.floor(ms / 1000 / 60);
          var ten_days = 1000 * 60 * secs;
          var end = new Date(new Date().getTime() + ten_days);
          var timer = Countdown.timer(end, async (timeLeft) => {
            let min = timeLeft.minutes;
            let sec = timeLeft.seconds;
            if (
              min + ":" + sec === 59 + ":" + 59 ||
              min + ":" + sec === 6 + ":" + 00
            ) {
              if (postReservation.token) {
                console.log("tokennnnnnnnnnnnnnnnnnnnnn1");
                var messagepush = {
                  data: {
                    Route: "ReservationTrack",
                    id: newdinner._id.toString(),
                  },
                  notification: {
                    title: "Amealio App",
                    body:
                      "Hey! We are eagerly waiting for you. Please update us for your arival time.",
                  },
                  token: postReservation.token,
                };
                SendPush.sendPush(messagepush);
              }
              // FCM.send(messagepush, (err, response) => {});
            }
          });
          console.log("myreservations", postReservation);
          if (postReservation.isVendor) {
            console.log("tokennnnnnnnnnnnnnnnnnnnnn1vvvv");
            messagepush1 = {
              data: {
                Route: "pending",
                id: newdinner._id.toString(),
              },
              notification: {
                title: "Vendor App",
                body:
                  "You received a Reservation request from " +
                  postReservation.name +
                  ".",
              },
              token: find_user_data.FCMtoken,
            };
            SendPush.sendPush(messagepush1);
          }
          // FCM.send(messagepush1, (err, response) => {});

          if (newdinner) {
            console.log("newDinnerif", newdinner);
            Dinner.find({
              vendor_id: postReservation.vendor_id,
              order_type: "reservation",
              status: "Pending",
            })
              .exec()
              .then((UserDinnerList) => {
                let basic = UserDinnerList.reverse();
                let data = postReservation.vendor_id;
                io.emit(["reservationPending", data], basic);
              });
          }
        }
      }
    } else {
      console.log("addreslist");
      let dinner = new Dinner({
        vendor_id: postReservation.vendor_id,
        user_id: postReservation.user_id,
        order_type: "reservation",
        entry_point: postReservation.entry_point
          ? postReservation.entry_point
          : "",
        name: postReservation.name ? postReservation.name : "",
        phone_number: postReservation.phone_number
          ? postReservation.phone_number
          : "",
        adults: postReservation.adults ? postReservation.adults : 0,
        kids: postReservation.kids ? postReservation.kids : 0,
        highchair: postReservation.highchair ? postReservation.highchair : 0,
        handicap: postReservation.handicap ? postReservation.handicap : 0,
        seating_preference: postReservation.seating_preference
          ? postReservation.seating_preference
          : "",
        total:
          parseInt(postReservation.adults) + parseInt(postReservation.kids),
        wait_time: postReservation.wait_time ? postReservation.wait_time : "",
        special_occassion: postReservation.special_occassion
          ? postReservation.special_occassion
          : "",
        Request_Date: Date.now(),
        Seating_Date: Date.now(),
        orderId: postReservation.orderId,
        requestNumber: Math.floor(Math.random() * 10000000000),
        Date: postReservation.Date ? postReservation.Date : "",
        time: postReservation.time ? postReservation.time : "",
        ETA: postReservation.ETA ? postReservation.ETA : "",
        restaurant_name: postReservation.restaurant_name
          ? postReservation.restaurant_name
          : "",
        dinner_status: "Pending",
        status: "Pending",
        register_date: Date.now(),
        token: postReservation.token ? postReservation.token : "",
      });
      let newdinner = await dinner.save();
      io.emit(["reservation", user_id], "Success");
      client.messages.create({
        body:
          "Yes! " +
          newdinner.name +
          " We've received your reservation request. Your request is pending confirmation by ." +
          newdinner.restaurant_name,
        messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
        form: "+14152003837",
        to: newdinner.phone_number,
      });

      let find_user_data = await Users.findOne({
        _id: postReservation.vendor_id,
      });
      let todayDate = moment(new Date()).format("DD-MM-YYYY-HH-mm");
      let reserve =
        moment(newdinner.Date, "DD-MM-YYYY").format("DD-MM-YYYY") +
        "-" +
        moment(newdinner.time, "h:mm A").format("HH-mm");

      var ms = moment(reserve, "DD-MM-YYYY-HH-mm").diff(
        moment(todayDate, "DD-MM-YYYY-HH-mm-ss")
      );
      var messagepush1 = {};
      var secs = Math.floor(ms / 1000 / 60);
      var ten_days = 1000 * 60 * secs;
      var end = new Date(new Date().getTime() + ten_days);
      var timer = Countdown.timer(end, async (timeLeft) => {
        let min = timeLeft.minutes;
        let sec = timeLeft.seconds;
        if (
          min + ":" + sec === 59 + ":" + 59 ||
          min + ":" + sec === 6 + ":" + 00
        ) {
          if (postReservation.token) {
            console.log("tokennnnnnnnnnnnnnnnnnnnnn2");
            var messagepush = {
              data: {
                Route: "ReservationTrack",
                id: newdinner._id.toString(),
              },
              notification: {
                title: "Amealio App",
                body:
                  "Hey! We are eagerly waiting for you. Please update us for your arival time.",
              },
              token: postReservation.token,
            };
            SendPush.sendPush(messagepush);
          }
          // FCM.send(messagepush, (err, response) => {});
        }
      });
      console.log("tokennnnnnnnnnnnnnnnnnnnnn2vvv", find_user_data);

      if (postReservation.isVendor && find_user_data.FCMtoken) {
        console.log("tokennnnnnnnnnnnnnnnnnnnnn2vvv", find_user_data.FCMtoken);
        messagepush1 = {
          data: {
            Route: "pending",
            id: newdinner._id.toString(),
          },
          notification: {
            title: "Vendor App",
            body:
              "You received a Reservation request from " +
              postReservation.name +
              ".",
          },
          token: find_user_data.FCMtoken,
        };
        SendPush.sendPush(messagepush1);
      }
      // FCM.send(messagepush1, (err, response) => {});
      if (newdinner) {
        console.log("newDinnerelse", newdinner);
        console.log("postReservation.vendor_id", postReservation.vendor_id);
        Dinner.find({
          vendor_id: postReservation.vendor_id,
          order_type: "reservation",
          status: "Pending",
        })
          .exec()
          .then((UserDinnerList) => {
            let basic = UserDinnerList.reverse();
            let data = postReservation.vendor_id;
            io.emit(["reservationPending", data], basic);
          });
      }
    }
  });
  socket.on("ETAUpdate", async (data) => {
    var updateETA =
      moment(data.Date, "DD-MM-YYYY").format("DD-MM-YYYY") +
      " " +
      moment(data.time, "h:mm A").format("HH:mm");
    let additional = moment(updateETA, "DD-MM-YYYY HH:mm")
      .add(data.ETA, "minutes")
      .format("DD-MM-YYYY HH:mm");
    let additional_Date = moment(additional, "DD-MM-YYYY").format("DD-MM-YYYY");
    let additional_time = moment(additional, "DD-MM-YYYY HH:mm").format(
      "HH:mm"
    );
    if (data.ETA > 20) {
      let newdinner = await Dinner.findOneAndUpdate(
        {
          _id: data._id,
        },
        {
          $set: {
            status: "Pending",
            dinner_status: "Pending",
            ETA: data.ETA,
            time: additional_time,
            Date: additional_Date,
          },
        }
      );
      let find_user_data = await Users.findOne({
        _id: data.vendor_id,
      });
      client.messages.create({
        body:
          "Yes! " +
          newdinner.name +
          " We've received your reservation request. Your request is pending confirmation by." +
          newdinner.restaurant_name +
          " at " +
          newdinner.Date +
          " " +
          newdinner.time,
        messagingServiceSid: "MG47e9a75679be5a94cf3c00e9fa1837dd",
        form: "+14152003837",
        to: find_user_data.mobile_number,
      });
      if (data.isVendor) {
        messagepush = {
          data: {
            Route: "pending",
            id: data.vendor_id.toString(),
          },
          notification: {
            title: "Vendor App",
            body: `${newdinner.name} is delayed ${data.wait_time} minutes. Reservation Request is pending.`,
          },
          token: find_user_data.FCMtoken,
        };
      }
      // FCM.send(messagepush, (err, response) => {});
      SendPush.sendPush(messagepush);
    } else {
      await Dinner.findOneAndUpdate(
        {
          _id: data._id,
        },
        {
          $set: {
            ETA: data.ETA,
            time: additional_time,
            Date: additional_Date,
          },
        }
      );
    }
    var reservationid = data.user_id;
    Dinner.find({
      user_id: data.user_id,
      order_type: "reservation",
    })
      .exec()
      .then((UserDinnerList) => {
        io.emit(["reservation", reservationid], UserDinnerList);
      });
  });
});
/************************************
 * @DESC    - PORT INITILIZATION
 * @PACKAGE - NODEJS
 ***********************************/
const PORT = process.env.PORT || 5001;
server.listen(PORT, () => console.log(`Started Server on Port`, PORT));
//07-04-2020 08:39Am
