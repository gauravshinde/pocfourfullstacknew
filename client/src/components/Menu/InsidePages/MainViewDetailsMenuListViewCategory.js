import React, { Component } from "react";
import "react-dropdown/style.css";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";

const totalRecordsInOnePage = 5;

class MainViewDetailsMenuListViewCategory extends Component {
  constructor() {
    super();
    this.state = {
      isRightSlideOpen: false,
      currentPagination: 1
    };
  }
  /*=========================
      handlers
 ==========================*/

  onChangePagination = page => {
    console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  /*===========================================
        renderTableRow
  ===========================================*/
  renderTableRow = () => {
    return (
      <tr>
        <td>Biryani</td>
        <td>Category Level</td>
        <td>Category Level</td>
        <td>Rs. 120</td>
        <td>5</td>
        <td>
          <img
            src={require("../../../assets/images/dashboard/menu/edit-icon-table.svg")}
            alt="file"
            className="p4v2-menu-table-icons3-shadow"
          />
        </td>
      </tr>
    );
  };

  render() {
    return (
      <>
        <table className="p4v2-menulist-table poc4v2-menu-block-scrollbar">
          <thead>
            <tr>
              <th>Category Name</th>
              <th>Cross Selection Category</th>
              <th>Tax Level</th>
              <th>Tax Charge</th>
              <th>Sub Categories</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.renderTableRow()}
            {this.renderTableRow()}
            {this.renderTableRow()}
            {this.renderTableRow()}
            {this.renderTableRow()}
          </tbody>
        </table>
        <div className="p4v2-menu-pagination">
          <Pagination
            onChange={this.onChangePagination}
            current={this.state.currentPagination}
            defaultPageSize={totalRecordsInOnePage}
            total={25}
            showTitle={false}
          />
        </div>
      </>
    );
  }
}

export default MainViewDetailsMenuListViewCategory;
