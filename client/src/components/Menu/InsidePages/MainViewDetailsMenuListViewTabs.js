import React, { Component } from "react";
import { Tabs, Tab, TabPanel, TabList } from "react-web-tabs";
import MainViewDetailsMenuListView from "./MainViewDetailsMenuListView";
import MainViewDetailsMenuListViewCategory from "./MainViewDetailsMenuListViewCategory";

class MainViewDetailsMenuListViewTabs extends Component {
  render() {
    return (
      <div className="p4v2-menu-block-view-tabs">
        <Tabs
          defaultTab="one"
          onChange={tabId => {
            console.log(tabId);
          }}
        >
          <TabList>
            <Tab tabFor="one">Items</Tab>
            <Tab tabFor="two">Categories</Tab>
          </TabList>
          <TabPanel tabId="one">
            <MainViewDetailsMenuListView search={this.props.search} />
          </TabPanel>
          <TabPanel tabId="two">
            <MainViewDetailsMenuListViewCategory />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

export default MainViewDetailsMenuListViewTabs;
