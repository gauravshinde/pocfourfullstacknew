import React, { Component } from "react";
// api
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { menu_get_items } from "../../../store/actions/SuperAdminAllAction/menu/MenuAllGetActions";
import isEmpty from "../../../store/validation/is-Empty";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
import ToggleReusable from "./ToggleReusable";
import MainViewDetailsMenuRightSlideReusable from "./MainViewDetailsMenuRightSlideReusable";

const options = ["Category", "Main Course", "Dessert"];
// pagination
const totalRecordsInOnePage = 5;

class MainViewDetailsMenuListView extends Component {
  constructor() {
    super();
    this.state = {
      isRightSlideOpen: false,
      currentPagination: 1,
      // api
      getItemsList: [],
      specificMenuLocal: []
    };
  }

  /*=========================
      lifecycle methods
 ==========================*/
  componentDidMount() {
    this.props.menu_get_items();
  }

  // api
  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.getMenuItems !== nextState.getItemsList) {
      return {
        getItemsList: nextProps.getMenuItems.menuItems
      };
    }
    return null;
  }

  /*=========================
      handlers
 ==========================*/

  onCategoryDropdownSelect = e => {
    console.log("Selected: " + e.value);
  };

  handleOpenRightSlideBlock = data => e => {
    e.preventDefault();
    this.setState({
      isRightSlideOpen: true,
      specificMenuLocal: data
    });
  };

  handleCloseRightSlideBlock = () => {
    this.setState({
      isRightSlideOpen: false
    });
  };

  onChangePagination = page => {
    // console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  /*===========================================
        renderSearchList
  ===========================================*/

  renderSearchList = () => {
    const { getItemsList } = this.state;
    let searchList = [];
    if (!isEmpty(getItemsList)) {
      if (this.props.search) {
        let search = new RegExp(this.props.search, "i");
        searchList = getItemsList.filter(getall => {
          if (search.test(getall.item_name)) {
            return getall;
          }
          if (search.test(getall.item_code)) {
            return getall;
          }
          if (search.test(getall.category)) {
            return getall;
          }
          if (search.test(getall.select_tag)) {
            return getall;
          }
          if (search.test(getall.processing_time)) {
            return getall;
          }
        });
        return searchList;
      }
      return getItemsList;
    }
  };

  /*===========================================
        renderTableRow
  ===========================================*/
  renderTableRow = () => {
    const { getItemsList, currentPagination } = this.state;

    return (
      <>
        {!isEmpty(getItemsList) &&
          this.renderSearchList().map(
            (data, index) =>
              index >= (currentPagination - 1) * totalRecordsInOnePage &&
              index < currentPagination * totalRecordsInOnePage && (
                <tr key={index}>
                  <td>{data.item_code}</td>
                  <td>{data.category}</td>
                  <td>
                    <div className="d-flex align-items-center">
                      <img
                        // src={require("../../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/burger.png")}
                        src={data.photo}
                        alt="food"
                        className="p4v2-menu-table-food-img"
                      />
                      {data.item_name}
                    </div>
                  </td>
                  <td>Rs. {data.item_price}</td>
                  <td>{data.processing_time}</td>
                  <td className="font-family-book">
                    <ToggleReusable
                      currentState={data.item_auto_accept}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      disabled={true}
                    />
                  </td>
                  <td>{data.select_tag}</td>
                  <td>
                    {data.spice_level_customizable ? (
                      <img
                        src={require("../../../assets/images/dashboard/menu/customize-icon-table.svg")}
                        alt="customize"
                        className="p4v2-menu-table-icons1"
                      />
                    ) : (
                      <img
                        src={require("../../../assets/images/dashboard/menu/customize-icon-table.svg")}
                        alt="customize"
                        className="p4v2-menu-table-icons1 invisible"
                      />
                    )}
                    <img
                      src={require("../../../assets/images/dashboard/menu/nonveg-food-icon-table.svg")}
                      alt="nonveg food"
                      className="p4v2-menu-table-icons1"
                    />
                  </td>
                  <td>
                    <img
                      src={require("../../../assets/images/dashboard/menu/file-icon-table.svg")}
                      alt="file"
                      className="p4v2-menu-table-icons2"
                    />
                    <img
                      src={require("../../../assets/images/dashboard/menu/eye-icon-table.svg")}
                      alt="view"
                      className="p4v2-menu-table-icons2 cursor-pointer"
                      onClick={this.handleOpenRightSlideBlock(data)}
                    />
                  </td>
                </tr>
              )
          )}
      </>
    );
  };

  render() {
    return (
      <>
        {this.state.isRightSlideOpen && (
          <MainViewDetailsMenuRightSlideReusable
            handleCloseRightSlideBlock={this.handleCloseRightSlideBlock}
            specificMenuLocal={this.state.specificMenuLocal}
          />
        )}

        {this.state.getItemsList.length === undefined ||
        isEmpty(this.renderSearchList()) ? (
          <p className="p4v2-font-21-medium">Empty List</p>
        ) : (
          <>
            <table className="p4v2-menulist-table poc4v2-menu-block-scrollbar">
              <thead>
                <tr>
                  <th>Code</th>
                  <th className="pl-0">
                    <Dropdown
                      className="poc4v2-menulist-categoryDropdown"
                      options={options}
                      onChange={this.onCategoryDropdownSelect}
                      placeholder="Category"
                    />
                  </th>
                  <th>Item Name</th>
                  <th>Item Price</th>
                  <th>Process Time</th>
                  <th>Availability</th>
                  <th>Tags</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>{this.renderTableRow()}</tbody>
            </table>
            <div className="p4v2-menu-pagination">
              <Pagination
                onChange={this.onChangePagination}
                current={this.state.currentPagination}
                defaultPageSize={totalRecordsInOnePage}
                total={this.state.getItemsList.length}
                showTitle={false}
              />
            </div>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  getMenuItems: state.menuAllGetReduers
});

export default connect(mapStateToProps, { menu_get_items })(
  withRouter(MainViewDetailsMenuListView)
);
