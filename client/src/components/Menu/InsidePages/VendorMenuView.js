import React, { Component } from "react";
import Dropdown from "react-dropdown";
import { Redirect } from "react-router-dom";
import "react-dropdown/style.css";
import MainViewDetailsMenuBlockView from "./MainViewDetailsMenuBlockView";
import MainViewDetailsMenuListViewTabs from "./MainViewDetailsMenuListViewTabs";

const options = [
  {
    type: "group",
    name: "",
    items: [
      { value: "addItem", label: "Add Item" },
      { value: "addCategory", label: "Add Category" }
    ]
  }
];

export class VendorMenuView extends Component {
  constructor() {
    super();
    this.state = {
      keepDropdownTitleStable: "",
      menuSearch: "",
      isBlockView: false,
      isListView: true,
      isAddItem: false,
      isAddCategory: false
    };
  }

  /*=========================
      handlers
 ==========================*/

  onDropdownSelect = e => {
    this.setState({
      keepDropdownTitleStable: e.value
    });
    if (e.value === "addItem") {
      this.setState({
        isAddItem: true
      });
    }
    if (e.value === "addCategory") {
      this.setState({
        isAddCategory: true
      });
    }
  };

  handleOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    // console.log(this.state.menuSearch);
  };

  handleOnSubmitSearch = e => {
    e.preventDefault();
    // console.log(this.state.menuSearch);
  };

  handleBlockView = () => {
    this.setState({
      isBlockView: true,
      isListView: false
    });
  };

  handleListView = () => {
    this.setState({
      isBlockView: false,
      isListView: true
    });
  };

  /*=========================
      render search
 ==========================*/
  renderSearch = () => {
    return (
      <>
        {/* search */}
        <div className="poc4v2-menulist-searchBlock">
          <form onSubmit={this.handleOnSubmitSearch}>
            <input
              type="text"
              id="menuSearch"
              name="menuSearch"
              className="poc4v2-menulist-searchBlock__input"
              placeholder="Search"
              onChange={this.handleOnChange}
              value={this.state.menuSearch}
            />
            <img
              src={require("../../../assets/images/dashboard/menu/search-icon.svg")}
              alt="search"
              className="poc4v2-menulist-searchBlock__icon"
              onClick={this.handleOnSubmitSearch}
            />
          </form>
        </div>
      </>
    );
  };

  render() {
    return (
      <>
        {this.state.isAddItem ? <Redirect to="/addMenuItem" /> : ""}
        {this.state.isAddCategory ? (
          <Redirect to="/vendor-addmenucategory" />
        ) : (
          ""
        )}
        <div className="row poc4v2-menutitleblock">
          <h1 className="poc4v2-menutitleblock-title">Menu</h1>
          <div className="row m-0 align-items-center">
            <Dropdown
              className="poc4v2-menulist-addDropdown"
              options={options}
              onChange={this.onDropdownSelect}
              placeholder="Add"
            />
            {this.renderSearch()}
            <div>
              {this.state.isListView && (
                <img
                  src={require("../../../assets/images/dashboard/menu/block-menu-icon.svg")}
                  alt="block view"
                  className="poc4v2-menulist-block-view-icon"
                  onClick={this.handleBlockView}
                />
              )}
              {this.state.isBlockView && (
                <img
                  src={require("../../../assets/images/dashboard/menu/list-menu-icon.svg")}
                  alt="list view"
                  className="poc4v2-menulist-block-view-icon"
                  onClick={this.handleListView}
                />
              )}
            </div>
          </div>
        </div>

        {this.state.isListView && (
          <div className="row poc4v2-menutitleblock mt-0">
            <MainViewDetailsMenuListViewTabs search={this.state.menuSearch} />
          </div>
        )}
        {this.state.isBlockView && (
          <div className="row poc4v2-menutitleblock poc4v2-menutitleblock--tabs mt-0">
            <MainViewDetailsMenuBlockView search={this.state.menuSearch} />
          </div>
        )}
      </>
    );
  }
}

export default VendorMenuView;
