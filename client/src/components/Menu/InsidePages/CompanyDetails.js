import React, { Component } from "react";
import SlidingComponent from "../../../../../reusableComponents/SlidingComponent";
import ToggleReusable from "./ToggleReusable";

const foodTime = ["Breakfast", "Lunch", "Dinner"];

const dayArray = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thrusday",
  "Friday",
  "Saturday",
  "Sunday"
];

const timeArray = [
  "00:00",
  "01:00",
  "02:00",
  "03:00",
  "04:00",
  "05:00",
  "06:00",
  "07:00",
  "08:00",
  "09:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00",
  "20:00",
  "21:00",
  "22:00",
  "23:00",
  "24:00"
];

export class CompanyDetails extends Component {
  constructor() {
    super();
    this.state = {
      Monday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        Breakfast: {
          open: false,
          breakfast_opening_time: "",
          breakfast_closing_time: ""
        },
        Lunch: {
          open: false,
          lunch_opening_time: "",
          lunch_closing_time: ""
        },
        Dinner: {
          open: false,
          dinner_opening_time: "",
          dinner_closing_time: ""
        }
      },
      Tuesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        Breakfast: {
          open: false,
          breakfast_opening_time: "",
          breakfast_closing_time: ""
        },
        Lunch: {
          open: false,
          lunch_opening_time: "",
          lunch_closing_time: ""
        },
        Dinner: {
          open: false,
          dinner_opening_time: "",
          dinner_closing_time: ""
        }
      },
      Wednesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        Breakfast: {
          open: false,
          breakfast_opening_time: "",
          breakfast_closing_time: ""
        },
        Lunch: {
          open: false,
          lunch_opening_time: "",
          lunch_closing_time: ""
        },
        Dinner: {
          open: false,
          dinner_opening_time: "",
          dinner_closing_time: ""
        }
      },
      Thrusday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        Breakfast: {
          open: false,
          breakfast_opening_time: "",
          breakfast_closing_time: ""
        },
        Lunch: {
          open: false,
          lunch_opening_time: "",
          lunch_closing_time: ""
        },
        Dinner: {
          open: false,
          dinner_opening_time: "",
          dinner_closing_time: ""
        }
      },
      Friday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        Breakfast: {
          open: false,
          breakfast_opening_time: "",
          breakfast_closing_time: ""
        },
        Lunch: {
          open: false,
          lunch_opening_time: "",
          lunch_closing_time: ""
        },
        Dinner: {
          open: false,
          dinner_opening_time: "",
          dinner_closing_time: ""
        }
      },
      Saturday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        Breakfast: {
          open: false,
          breakfast_opening_time: "",
          breakfast_closing_time: ""
        },
        Lunch: {
          open: false,
          lunch_opening_time: "",
          lunch_closing_time: ""
        },
        Dinner: {
          open: false,
          dinner_opening_time: "",
          dinner_closing_time: ""
        }
      },
      Sunday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        Breakfast: {
          open: false,
          opening_time: "",
          closing_time: ""
        },
        Lunch: {
          open: false,
          opening_time: "",
          closing_time: ""
        },
        Dinner: {
          open: false,
          opening_time: "",
          closing_time: ""
        }
      }
    };
  }

  /**************************
   * timings handlers
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  onDayOpenCloseHanlder = day => e => {
    let state = this.state;
    let dayData = state[day];
    dayData.open = !dayData.open;
    this.setState({
      state: state
    });
  };

  onTimeSelectHandlerSingle = day => e => {
    let state = this.state;
    let dayData = state[day];
    dayData[e.target.name] = e.target.value;
    this.setState({
      state: state
    });
  };

  onFoodTypeOpenClose = (day, food) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData.open = !foodTimeData.open;
    this.setState({
      state: state
    });
  };

  onTimeSelectHandlerMultiple = (day, food) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData[e.target.name] = e.target.value;
    this.setState({
      state: state
    });
  };

  onSubmit = e => {
    e.preventDefault();
    console.log(this.state);
  };

  render() {
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <form className="pl-3 pr-3">
            <Restauranttiming
              state={this.state}
              toggleFunction={this.toggleFunction}
            />
            {!this.state.multiple_opening_time &&
            this.state.are_you_open_24_x_7 ? (
              <NoMultipleTime
                state={this.state}
                toggleFunction={this.toggleFunction}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onTimeSelectHandlerSingle={this.onTimeSelectHandlerSingle}
              />
            ) : null}
            {this.state.multiple_opening_time &&
            this.state.are_you_open_24_x_7 ? (
              <YesMultipleTime
                state={this.state}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onFoodTypeOpenClose={this.onFoodTypeOpenClose}
                onTimeSelectHandlerMultiple={this.onTimeSelectHandlerMultiple}
              />
            ) : null}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default CompanyDetails;

const Restauranttiming = ({ state, toggleFunction }) => {
  return (
    <>
      <div className="cuisine-main">
        <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
          Do you have specific timings for the item?
        </h3>
        <ToggleReusable
          name="are_you_open_24_x_7"
          currentState={state.are_you_open_24_x_7}
          spantext1={"Yes"}
          spantext2={"No"}
          toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
          toggleinputclass={"toggle__switch ml-3 mr-3"}
          onChange={toggleFunction}
        />

        {state.are_you_open_24_x_7 ? (
          <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
            Do you have multiple opening timings ?
          </h3>
        ) : null}
        {state.are_you_open_24_x_7 ? (
          <SlidingComponent
            name="multiple_opening_time"
            currentState={state.multiple_opening_time}
            type={"checkbox"}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center mb-2"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={toggleFunction}
            defaultChecked={state.multiple_opening_time === true ? true : false}
          />
        ) : null}
      </div>
    </>
  );
};

const NoMultipleTime = ({
  state,
  toggleFunction,
  onDayOpenCloseHanlder,
  onTimeSelectHandlerSingle
}) => {
  return (
    <>
      <div className="cuisine-main">
        {dayArray.map((day, index) => (
          <table key={index} style={{ borderRadius: 0, width: "100%" }}>
            <tbody>
              <tr className="p4v2-timing-table-border-bottom">
                <td>
                  <p className="p4v2-add-menu-item-form__block-title p4v2-pb-20">
                    {day}
                  </p>
                  <ToggleReusable
                    name={day}
                    currentState={state[day].open}
                    spantext1={"Open"}
                    spantext2={"Close"}
                    toggleclass={"toggle d-flex align-items-center"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                  />
                </td>
                <td style={{ width: "30%" }}>
                  {/* OPEN AND CLOSE TIME */}
                  <p className="col-12 p-0 p4v2-font-24-medium">Opening Time</p>
                  <div className="opening_time_selector">
                    <select
                      name="main_opening_time"
                      onChange={onTimeSelectHandlerSingle(day)}
                      className="Selection_box"
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select>
                    <div className="dasheds">-</div>
                    <select
                      name="main_closing_time"
                      onChange={onTimeSelectHandlerSingle(day)}
                      className="Selection_box"
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const YesMultipleTime = ({
  state,
  onDayOpenCloseHanlder,
  onFoodTypeOpenClose,
  onTimeSelectHandlerMultiple
}) => {
  return (
    <>
      <div className="cuisine-main">
        {dayArray.map((day, index) => (
          <table key={index} style={{ borderRadius: 0, width: "100%" }}>
            <tbody>
              <tr className="p4v2-timing-table-border-bottom">
                <td style={{ width: "18%" }}>
                  <p className="p4v2-add-menu-item-form__block-title p4v2-pb-20">
                    {day}
                  </p>
                  <ToggleReusable
                    name={day}
                    currentState={state[day].open}
                    spantext1={"Open"}
                    spantext2={"Close"}
                    toggleclass={"toggle d-flex align-items-center"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                  />
                </td>
                {foodTime.map((food, index) => (
                  <td
                    key={index}
                    style={{
                      width: "200px",
                      paddingLeft: "20px",
                      padding: "15px"
                    }}
                  >
                    <div className="row m-0">
                      {/* OPEN AND CLOSE TIME */}
                      <span className="col-7 p-0 p4v2-font-24-medium">
                        {food}
                      </span>
                      <div className="col-5 p-0 text-right">
                        <ToggleReusable
                          name={day}
                          currentState={state[day].open}
                          spantext1={""}
                          spantext2={""}
                          toggleclass={
                            "toggle d-flex align-items-center p4v2-pb-20"
                          }
                          toggleinputclass={"toggle__switch ml-3 mr-3"}
                          onChange={onFoodTypeOpenClose(day, food)}
                        />
                      </div>
                    </div>
                    <div className="opening_time_selector">
                      <select
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select>
                      <div className="dasheds">-</div>
                      <select
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select>
                    </div>
                  </td>
                ))}
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};
