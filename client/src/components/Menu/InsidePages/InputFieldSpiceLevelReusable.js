import React from "react";

const InputFieldSpiceLevelReusable = ({
  inputlabelclass,
  labeltext,
  name,
  value,
  type,
  place,
  onChange,
  inputclass,
  error,
  disabled
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <div className="input-container row mx-0 align-items-center">
        <div className="col-6 px-0">
          <input
            id={name}
            name={name}
            value={value}
            type={type}
            className={inputclass}
            placeholder={place}
            onChange={onChange}
            disabled={disabled === true ? { disabled } : false}
          />
        </div>
        <div className="col-6">
          <p className="p4v2-font-18-light">(0-4)</p>
        </div>
      </div>
      {error ? <div className="error">{error}</div> : null}
    </React.Fragment>
  );
};

export default InputFieldSpiceLevelReusable;
