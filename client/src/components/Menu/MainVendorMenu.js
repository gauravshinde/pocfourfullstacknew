import React, { Component } from "react";
import NavbarDashboard from "../Dassboard/NavbarDashboard";
import VendorMenuView from "./InsidePages/VendorMenuView";

export class MainVendorMenu extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="container-fluid p-0">
          <div className="row">
            <NavbarDashboard />
            <VendorMenuView />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MainVendorMenu;
