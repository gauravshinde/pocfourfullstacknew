import React, { Component } from "react";
import axios from "axios";
import Modal from "react-responsive-modal";
import InputComponent from "./../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";
import ButtonComponent from "./../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import TextAreaComponent from "../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/TextAreaComponent";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { add_newOffers } from "./../../../store/actions/allOffersAction";
import { clear_error } from "./../../../store/actions/errorActions";
import { serverApi } from "./../../../config/Keys";

export class AddOffersComponents extends Component {
  constructor() {
    super();
    this.state = {
      openModal: false,
      openAddImage: false,
      name: "",
      description: "",
      startDate: "",
      endDate: "",
      couponCode: "",
      maxNumberAvailable: "",
      maxUsage: "",
      daysbetween: "",
      termAndCondition: "",
      addOfferImage: "",
      status: true,
      errors: {},
    };
  }

  /**************************
   * @DESC Pop Up Handlers
   *************************/
  onOpenModal = () => {
    this.setState({ openModal: true });
  };

  onCloseModal = () => {
    this.setState({
      openModal: false,
    });
  };

  onImageOpenModal = () => {
    this.setState({
      openModal: false,
      openAddImage: true,
    });
  };

  onImageCloseModal = () => {
    this.setState({
      openAddImage: false,
    });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = (e) => {
    e.preventDefault();
    if (e.target.name === "description") {
      let caps = e.target.value;
      caps = caps.charAt(0).toUpperCase() + caps.slice(1);
      let dataSet = caps;
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: dataSet,
      });
    } else if (e.target.name === "termAndCondition") {
      let caps = e.target.value;
      caps = caps.charAt(0).toUpperCase() + caps.slice(1);
      let dataSet = caps;
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: dataSet,
      });
    } else {
      let data = e.target.value.split(" ");
      let newarray1 = [];
      for (let x = 0; x < data.length; x++) {
        newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
      }
      let newData = newarray1.join(" ");
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: newData,
      });
    }
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageLogoUploadHandler = (e) => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post(`${serverApi}/image/upload-content-images`, data)
      .then((res) => {
        this.setState({ addOfferImage: res.data.image_URL, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  /********************************************
   * @DESC ADD RESERVATION POPUP HANDLE SUBMIT
   ********************************************/
  handleAddOffersSubmit = (e) => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user._id,
      name: this.state.name,
      description: this.state.description,
      start_date: this.state.startDate,
      end_date: this.state.endDate,
      coupon_code: this.state.couponCode,
      max_number_available: this.state.maxNumberAvailable,
      max_usage: this.state.maxUsage,
      day_between_each_use: this.state.daysbetween,
      terms_conditions: this.state.termAndCondition,
      add_photo: this.state.addOfferImage,
      status: this.state.status,
    };
    //console.log(formData);
    this.props.add_newOffers(formData);
    this.setState({
      name: "",
      description: "",
      startDate: "",
      endDate: "",
      couponCode: "",
      maxNumberAvailable: "",
      maxUsage: "",
      daysbetween: "",
      termAndCondition: "",
      addOfferImage: "",
    });
  };

  renderAddImageModal = () => {
    const { openAddImage, errors } = this.state;

    return (
      <React.Fragment>
        <Modal
          open={openAddImage}
          onClose={this.onImageCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-reservation-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="add-reservation-popup">
            <div className="popup-head">New Offer</div>

            <div className="container mt-3">
              <form noValidate onSubmit={this.handleAddOffersSubmit}>
                <div className="row mt-4 mb-4">
                  <div className="col-3">
                    <h2 className="heading-title">Add Photo</h2>
                  </div>
                  <div className="col-9">
                    <div
                      className="view_chainsBor mt-0"
                      style={{ width: "100%" }}
                    >
                      <div className="custom_file_upload">
                        <input
                          type="file"
                          name="icon"
                          id="file"
                          onChange={this.onImageLogoUploadHandler}
                          className="custom_input_upload"
                        />
                        <label
                          className="custom_input_label newChain_add"
                          htmlFor="file"
                        >
                          <div>
                            <i
                              className="fa fa-plus"
                              style={{ color: "#CCCCCC" }}
                            ></i>
                          </div>
                          <div className="add_new_text">Add New</div>
                        </label>
                      </div>

                      <div className="newChain_addthree mx-3">
                        {this.state.addOfferImage ? (
                          <img
                            src={this.state.addOfferImage}
                            className="newChain_addtwo"
                            style={{ height: "100%", width: "100%" }}
                            alt="chain"
                          />
                        ) : null}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onImageCloseModal}
                  />
                  <ButtonComponent
                    type={"submit"}
                    buttontext={"Save"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onImageCloseModal}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  };

  render() {
    const { openModal, errors } = this.state;
    return (
      <React.Fragment>
        <ButtonComponent
          type={"button"}
          buttontext={"Add Offer"}
          buttonclass={"btn button-width button-orange"}
          onClick={this.onOpenModal}
        />
        <Modal
          open={openModal}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-reservation-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="add-reservation-popup">
            <div className="popup-head">New Offer</div>

            <div className="container mt-3">
              <form>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Name"}
                      inputlabelclass={"label-class"}
                      name={"name"}
                      type={"text"}
                      value={this.state.name}
                      onChange={this.onChange}
                      place={"eg. Buy 1 Get 1 Free"}
                      inputclass={"form-control curve_input_field"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Description"}
                      inputlabelclass={"label-class"}
                      name={"description"}
                      type={"text"}
                      value={this.state.description}
                      onChange={this.onChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Start Date"}
                      inputlabelclass={"label-class"}
                      name={"startDate"}
                      type={"date"}
                      value={this.state.startDate}
                      onChange={this.onChange}
                      place={"eg. 10/25/2019"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"End Date"}
                      inputlabelclass={"label-class"}
                      name={"endDate"}
                      type={"date"}
                      value={this.state.endDate}
                      onChange={this.onChange}
                      place={"eg. 10/25/2019"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Coupon Code"}
                      inputlabelclass={"label-class"}
                      name={"couponCode"}
                      type={"text"}
                      value={this.state.couponCode}
                      onChange={this.onChange}
                      place={"eg. C-137"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Max Number Available"}
                      inputlabelclass={"label-class"}
                      name={"maxNumberAvailable"}
                      type={"number"}
                      value={this.state.maxNumberAvailable}
                      onChange={this.onChange}
                      place={"eg. 25"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Max Usage"}
                      inputlabelclass={"label-class"}
                      name={"maxUsage"}
                      type={"number"}
                      value={this.state.maxUsage}
                      onChange={this.onChange}
                      place={"eg. 3"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Days between each use"}
                      inputlabelclass={"label-class"}
                      name={"daysbetween"}
                      type={"number"}
                      value={this.state.daysbetween}
                      onChange={this.onChange}
                      place={"eg. 30"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Terms and Conditions"}
                      inputlabelclass={"label-class"}
                      name={"termAndCondition"}
                      type={"text"}
                      value={this.state.termAndCondition}
                      onChange={this.onChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div>
                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseModal}
                  />
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Next"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onImageOpenModal}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
        {this.renderAddImageModal()}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  add_newOffers,
})(withRouter(AddOffersComponents));
//18-01-19 new
