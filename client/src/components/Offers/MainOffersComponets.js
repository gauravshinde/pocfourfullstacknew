import React, { Component } from "react";
import NavbarDashboard from "../Dassboard/NavbarDashboard";
// import InputComponent from '../../pocsrcone/Components/SmallComponents/InputComponent';
import AddOffersComponents from "./InsideOffersComponent/AddOffersComponents";
import SlidingComponent from "./../../reusableComponents/SlidingComponent";
import dateFns from "date-fns";
import Modal from "react-responsive-modal";
import InputComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";
import ButtonComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import TextAreaComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/TextAreaComponent";
// import socketIOClient from "socket.io-client";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../store/validation/is-Empty";
import {
  get_offers_details,
  delete_newOffers,
  edit_offers_details,
  offers_update_availability_data
} from "./../../store/actions/allOffersAction";

export class MainOffersComponets extends Component {
  constructor() {
    super();
    this.state = {
      endpoint: "http://192.168.0.28:5001/",
      AllOffersDetails: [],
      currentState: true,
      openEditmodal: false,
      offerupdate: {},
      search: ""
    };
  }

  componentDidMount() {
    this.props.get_offers_details();
    // const socket = socketIOClient(this.state.endpoint);
    // socket.on("offers", data => {
    //   this.setState(prevState => ({
    //     AllOffersDetails: [data, ...prevState.AllOffersDetails]
    //   }));
    // });
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    // console.log(nextprops.allOffers);
    if (nextprops.allOffers !== nextstate.AllOffersDetails) {
      return { AllOffersDetails: nextprops.allOffers.get_all_offersdetails };
    }
    return null;
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************
   * @DESC - DELETE HANDLER
   ***************************/
  onClickDeleteOffer = data => {
    // console.log(data);
    // let deleteData = {
    //   id: data._id
    // };
    // this.props.delete_newOffers(deleteData);
    if (window.confirm("Are you sure delete this offer")) {
      let deleteData = {
        id: data._id
      };
      this.props.delete_newOffers(deleteData);
    }
  };

  /**************************
   * @DESC Pop Up Handlers
   *************************/
  onOpenEditModal = data => {
    this.setState({
      openEditmodal: true,
      offerupdate: data
    });
  };

  onCloseEditModal = () => {
    this.setState({
      openEditmodal: false
    });
  };

  handleEditOfferChange = e => {
    // console.log(e.target.value);
    let offernew = this.state.offerupdate;

    offernew[e.target.name] = e.target.value;
    // console.log(offernew.name);
    this.setState({
      offerupdate: offernew
    });
  };

  handleEditOffersSubmit = e => {
    e.preventDefault();

    let editupdateoffer = this.state.offerupdate;

    const editOffers = {
      id: editupdateoffer._id,
      name: editupdateoffer.name,
      description: editupdateoffer.description,
      start_date: editupdateoffer.start_date,
      end_date: editupdateoffer.end_date,
      coupon_code: editupdateoffer.coupon_code,
      max_number_available: editupdateoffer.max_number_available,
      max_usage: editupdateoffer.max_usage,
      day_between_each_use: editupdateoffer.day_between_each_use,
      terms_conditions: editupdateoffer.terms_conditions,
      add_photo: editupdateoffer.add_photo
    };
    // console.log(editOffers);
    this.props.edit_offers_details(editOffers, this.props.history);
    this.setState({
      openEditmodal: false
    });
  };

  renderEditModal() {
    const { openEditmodal, errors, offerupdate } = this.state;
    console.log(offerupdate);
    return (
      <React.Fragment>
        {/* <ButtonComponent
          type={'button'}
          buttontext={'Add Offer'}
          buttonclass={'btn button-width button-orange'}
          onClick={this.onOpenModal}
        /> */}
        <Modal
          open={openEditmodal}
          onClose={this.onCloseEditModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-reservation-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="add-reservation-popup">
            <div className="popup-head">Edit Offer</div>

            <div className="container mt-3">
              <form>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Name"}
                      inputlabelclass={"label-class"}
                      name={"name"}
                      type={"text"}
                      value={offerupdate.name}
                      onChange={this.handleEditOfferChange}
                      place={"eg. Buy 1 Get 1 Free"}
                      inputclass={"form-control curve_input_field"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Description"}
                      inputlabelclass={"label-class"}
                      name={"description"}
                      type={"text"}
                      value={offerupdate.description}
                      onChange={this.handleEditOfferChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Start Date"}
                      inputlabelclass={"label-class"}
                      name={"start_date"}
                      type={"date"}
                      value={offerupdate.start_date}
                      onChange={this.handleEditOfferChange}
                      place={"eg. 10/25/2019"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"End Date"}
                      inputlabelclass={"label-class"}
                      name={"end_date"}
                      type={"date"}
                      value={offerupdate.end_date}
                      onChange={this.handleEditOfferChange}
                      place={"eg. 10/25/2019"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Coupon Code"}
                      inputlabelclass={"label-class"}
                      name={"coupon_code"}
                      type={"text"}
                      value={offerupdate.coupon_code}
                      onChange={this.handleEditOfferChange}
                      place={"eg. C-137"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Max Number Available"}
                      inputlabelclass={"label-class"}
                      name={"max_number_available"}
                      type={"number"}
                      value={offerupdate.max_number_available}
                      onChange={this.handleEditOfferChange}
                      place={"eg. 25"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Max Usage"}
                      inputlabelclass={"label-class"}
                      name={"max_usage"}
                      type={"number"}
                      value={offerupdate.max_usage}
                      onChange={this.handleEditOfferChange}
                      place={"eg. 3"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Days between each use"}
                      inputlabelclass={"label-class"}
                      name={"day_between_each_use"}
                      type={"number"}
                      value={offerupdate.day_between_each_use}
                      onChange={this.handleEditOfferChange}
                      place={"eg. 30"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Terms and Conditions"}
                      inputlabelclass={"label-class"}
                      name={"terms_conditions"}
                      type={"text"}
                      value={offerupdate.terms_conditions}
                      onChange={this.handleEditOfferChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div>
                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseEditModal}
                  />
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Next"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.handleEditOffersSubmit}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
        {/* {this.renderAddImageModal()} */}
      </React.Fragment>
    );
  }

  /********************************************
   * @DESC -ON CHANGE TOGGLE STATUS CHANGE
   ********************************************/
  onSelecttogglestatusChange = data => e => {
    // e.preventDefault();
    const formData = {
      vendor_id: data.vendor_id,
      id: data._id,
      offer_status: data.offer_status === true ? false : true
    };
    //console.log(formData);
    this.props.offers_update_availability_data(formData);
  };

  renderNewOffers = () => {
    const { AllOffersDetails } = this.state;
    console.log(AllOffersDetails);
    let filtereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      filtereddata = AllOffersDetails.filter(getall => {
        if (search.test(getall.name)) {
          return getall;
        }
      });
      // console.log(filtereddata);
    } else {
      filtereddata = AllOffersDetails;
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.reverse().map((data, index) => (
        <React.Fragment key={index}>
          <div className="col-12 main-offers-inside">
            <div className="row">
              <div className="col-sm-3 d-flex justify-content-start align-items-center">
                <div className="row">
                  <div className="col-4">
                    <img
                      src={data.add_photo}
                      alt="product"
                      className="offers-images-photos"
                      style={{ border: "0.5px solid #E7E7E7" }}
                    />
                  </div>
                  <div className="col-8">
                    <div className="main_offer_description_divnew">
                      <h3>{data.name}</h3>
                    </div>
                    <div className="main_offer_description_div">
                      <h4>{data.description}</h4>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-9 d-flex align-items-center">
                <div className="col-8 main-offers-sectiontwo-divone">
                  <div className="row">
                    <div className="col-sm-3">
                      <h5>Coupon Code</h5>
                      <p className="couponcode_css ">{data.coupon_code}</p>
                    </div>
                    <div className="col-sm-3">
                      <h5>Start Date</h5>
                      <p>{dateFns.format(data.start_date, "DD-MM-YYYY")}</p>
                    </div>
                    <div className="col-sm-3">
                      <h5>End Date</h5>
                      <p>{dateFns.format(data.end_date, "DD-MM-YYYY")}</p>
                    </div>
                    <div className="col-sm-3 d-flex p-0">
                      <div className="w-50">
                        <h5>Views</h5>
                        <p>{data.view}</p>
                      </div>
                      <div className="w-50">
                        <h5>Used</h5>
                        <p>{data.coupon_apply}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-4 main-offers-sectiontwo-divtwo">
                  <div className="row">
                    <div className="col-sm-8">
                      <SlidingComponent
                        name="current_status"
                        currentState={data.offer_status}
                        type={"checkbox"}
                        spantext1={"Active"}
                        spantext2={"Inactive"}
                        toggleclass={"toggle d-flex align-items-center mb-2"}
                        toggleinputclass={"toggle__switch ml-3 mr-3"}
                        onChange={this.toggleFunction}
                        slidingonClick={this.onSelecttogglestatusChange(data)}
                        defaultChecked={
                          data.offer_status === true ? true : false
                        }
                      />
                    </div>
                    <div className="col-sm-4 d-flex">
                      <img
                        src={require("../../assets/images/dashboard/edit.png")}
                        alt="edit"
                        className="img-fluid offers-image-icon-button"
                        onClick={() => this.onOpenEditModal(data)}
                      />
                      <img
                        src={require("../../assets/images/dashboard/delete.png")}
                        alt="delete"
                        className="img-fluid offers-image-icon-button"
                        onClick={() => this.onClickDeleteOffer(data)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      ));
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  render() {
    console.log(this.state);
    return (
      <React.Fragment>
        <div className="container-fluid p-0">
          <div className="row">
            <NavbarDashboard />
          </div>
          <div className="row">
            <div className="container-fluid all-page-container-padding table-container">
              <div className="col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center">
                <div className="d-flex justify-content-start align-items-center">
                  <h4 className={"vendor"}>My Offers</h4>
                  <AddOffersComponents />
                </div>

                <div className="d-flex justify-content-between main-offers-sectionone">
                  {/*w-25*/}
                  <InputComponent
                    img={require("../../assets/images/search.png")}
                    labelClass={"mylabel"}
                    imageClass={"offers-image-class"}
                    onChange={this.onSearchChange}
                    value={this.state.search}
                    place={"search"}
                    inputclass={"form-control main-offers-input"}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row main-offers-sectiontwo">
            {this.renderNewOffers()}
            {this.renderEditModal()}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  allOffers: state.alloffers
});

export default connect(mapStateToProps, {
  get_offers_details,
  delete_newOffers,
  edit_offers_details,
  offers_update_availability_data
})(withRouter(MainOffersComponets));
