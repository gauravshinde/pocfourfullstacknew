import React, { Component } from "react";
import NavbarDashboard from "../Dassboard/NavbarDashboard";
import axios from "axios";
// import InputComponent from '../../pocsrcone/Components/SmallComponents/InputComponent';
import AddOffersComponents from "./InsideEventsComponent/AddEventsComponents";
import SlidingComponent from "./../../reusableComponents/SlidingComponent";
import dateFns from "date-fns";
import Modal from "react-responsive-modal";
import InputComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";
import ButtonComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import TextAreaComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/TextAreaComponent";

//import socketIOClient from "socket.io-client";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../store/validation/is-Empty";
import {
  get_events_details,
  delete_newEvents,
  edit_events_details,
  event_update_availability_data
} from "./../../store/actions/allEventsActions";

export class MainEventsComponents extends Component {
  constructor() {
    super();
    this.state = {
      //endpoint: "http://192.168.0.28:5001/",
      AllEventsDetails: [],
      currentState: true,
      openEditmodal: false,
      eventupdate: {},
      addEventImage: {},
      search: "",
      availability: ""
    };
  }

  componentDidMount() {
    this.props.get_events_details();
    // const socket = socketIOClient(this.state.endpoint);
    // socket.on("offers", data => {
    //   this.setState(prevState => ({
    //     AllEventsDetails: [data, ...prevState.AllEventsDetails]
    //   }));
    // });
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    //console.log(nextprops.allEvents);
    if (nextprops.allEvents !== nextstate.AllEventsDetails) {
      return { AllEventsDetails: nextprops.allEvents.get_all_eventsdetails };
    }
    if (nextprops.allEvents !== nextstate.addEventImage) {
      return {
        addEventImage: nextprops.allEvents.get_all_eventsdetails
      };
    }
    return null;
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************
   * @DESC - DELETE HANDLER
   ***************************/
  onClickDeleteOffer = data => {
    // console.log(data);
    // let deleteData = {
    //   id: data._id
    // };
    // console.log(deleteData);
    //this.props.delete_newEvents(deleteData);
    if (window.confirm("Are you sure delete this event")) {
      let deleteData = {
        id: data._id
      };
      this.props.delete_newEvents(deleteData);
    }
  };

  /**************************
   * @DESC Pop Up Handlers
   *************************/
  onOpenEditModal = data => {
    this.setState({
      openEditmodal: true,
      eventupdate: data
    });
  };

  onCloseEditModal = () => {
    this.setState({
      openEditmodal: false
    });
  };

  handleEditOfferChange = e => {
    // console.log(e.target.value);
    let eventnew = this.state.eventupdate;

    eventnew[e.target.name] = e.target.value;
    // console.log(eventnew.name);
    this.setState({
      eventupdate: eventnew
    });
  };

  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post("/image/upload-content-images", data)
      .then(res => {
        let addEventImage = this.state.eventupdate.event_photo;
        addEventImage.push(res.data.image_URL);
        this.setState({ eventupdate: addEventImage, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  handleEditOffersSubmit = e => {
    e.preventDefault();

    let editupdateoffer = this.state.eventupdate;

    const editEvents = {
      id: editupdateoffer._id,
      name: editupdateoffer.name,
      description: editupdateoffer.description,
      start_date: editupdateoffer.start_date,
      end_date: editupdateoffer.end_date,
      time: editupdateoffer.time,
      venue: editupdateoffer.venue,
      address: editupdateoffer.address,
      terms_condition: editupdateoffer.terms_condition,
      event_photo: editupdateoffer.event_photo
    };
    // console.log(editOffers);
    this.props.edit_events_details(editEvents, this.props.history);
    this.setState({
      openEditmodal: false
    });
  };

  renderEditModal() {
    const { openEditmodal, errors, eventupdate } = this.state;
    //console.log(eventupdate);
    return (
      <React.Fragment>
        {/* <ButtonComponent
          type={'button'}
          buttontext={'Add Offer'}
          buttonclass={'btn button-width button-orange'}
          onClick={this.onOpenModal}
        /> */}
        <Modal
          open={openEditmodal}
          onClose={this.onCloseEditModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-reservation-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="add-reservation-popup">
            <div className="popup-head">Edit Event</div>

            <div className="container mt-3">
              <form>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Name"}
                      inputlabelclass={"label-class"}
                      name={"name"}
                      type={"text"}
                      value={eventupdate.name}
                      onChange={this.handleEditOfferChange}
                      place={"eg. Buy 1 Get 1 Free"}
                      inputclass={"form-control curve_input_field"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Description"}
                      inputlabelclass={"label-class"}
                      name={"description"}
                      type={"text"}
                      value={eventupdate.description}
                      onChange={this.handleEditOfferChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Start Date"}
                      inputlabelclass={"label-class"}
                      name={"start_date"}
                      type={"date"}
                      value={eventupdate.start_date}
                      onChange={this.handleEditOfferChange}
                      place={"eg. 10/25/2019"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"End Date"}
                      inputlabelclass={"label-class"}
                      name={"end_date"}
                      type={"date"}
                      value={eventupdate.end_date}
                      onChange={this.handleEditOfferChange}
                      place={"eg. 10/25/2019"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Time"}
                      inputlabelclass={"label-class"}
                      name={"time"}
                      type={"time"}
                      value={eventupdate.time}
                      onChange={this.handleEditOfferChange}
                      place={"eg. Royal Palms"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Venue"}
                      inputlabelclass={"label-class"}
                      name={"venue"}
                      type={"text"}
                      value={eventupdate.venue}
                      onChange={this.handleEditOfferChange}
                      place={"eg. Royal Palms"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Address"}
                      inputlabelclass={"label-class"}
                      name={"address"}
                      type={"text"}
                      value={eventupdate.address}
                      onChange={this.handleEditOfferChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Terms and Conditions"}
                      inputlabelclass={"label-class"}
                      name={"terms_condition"}
                      type={"text"}
                      value={eventupdate.terms_condition}
                      onChange={this.handleEditOfferChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div>
                <div className="row mt-4 mb-4">
                  <div className="col-3">
                    <h2 className="heading-title-events">Add Photo</h2>
                  </div>
                  <div className="col-9">
                    <div
                      className="view_chainsBor mt-0"
                      style={{ width: "100%" }}
                    >
                      <div className="custom_file_upload">
                        <input
                          type="file"
                          name="iconone"
                          id="filetwo"
                          onChange={this.onImageUploadHandler}
                          className="custom_input_upload"
                        />
                        <label
                          className="custom_input_label newChain_add"
                          htmlFor="filetwo"
                        >
                          <div>
                            <i
                              className="fa fa-plus"
                              style={{ color: "#CCCCCC" }}
                            ></i>
                          </div>
                          <div className="add_new_text">Add New</div>
                        </label>
                      </div>

                      <div className="newChain_addthree mx-3">
                        {this.state.eventupdate.length > 0
                          ? this.state.eventupdate.map((image, index) => (
                              <img
                                key={index}
                                src={image}
                                className="newChain_addtwo"
                                style={{ height: "100%", width: "100%" }}
                                alt="chain "
                              />
                            ))
                          : null}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseEditModal}
                  />
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Next"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.handleEditOffersSubmit}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
        {/* {this.renderAddImageModal()} */}
      </React.Fragment>
    );
  }

  /********************************************
   * @DESC -ON CHANGE TOGGLE STATUS CHANGE
   ********************************************/
  onSelecttogglestatusChange = data => e => {
    // e.preventDefault();
    const formData = {
      id: data._id,
      availability: data.availability === true ? false : true
    };
    // console.log(formData);
    this.props.event_update_availability_data(formData);
  };

  renderNewEvents = () => {
    const { AllEventsDetails } = this.state;
    let filtereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      filtereddata = AllEventsDetails.filter(getall => {
        if (search.test(getall.name)) {
          return getall;
        }
      });
      // console.log(filtereddata);
    } else {
      filtereddata = AllEventsDetails;
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.reverse().map((data, index) => (
        <React.Fragment key={index}>
          <div className="col-12 main-offers-inside">
            <div className="row">
              <div className="col-sm-3 d-flex justify-content-start align-items-center">
                <div className="col-4 p-0">
                  <img
                    src={data.event_photo[0]}
                    alt="product"
                    className="img-fluid offers-images-photos"
                    style={{ border: "0.5px solid #E7E7E7" }}
                  />
                </div>
                <div className="col-8 p-0">
                  <div className="main_offer_description_divnewone">
                    <h3>{data.name}</h3>
                  </div>
                </div>
              </div>
              <div className="col-sm-9 d-flex align-items-center">
                <div className="col-7 main-offers-sectiontwo-divone">
                  <div className="row">
                    <div className="col-sm-3">
                      <h5>Venue</h5>
                      <p>{data.venue}</p>
                    </div>
                    <div className="col-sm-3">
                      <h5>Date</h5>
                      <p>{dateFns.format(data.start_date, "DD-MM-YYYY")}</p>
                    </div>
                    <div className="col-sm-3">
                      <h5>Duration</h5>
                      <p>{dateFns.format(data.end_date, "DD-MM-YYYY")}</p>
                    </div>
                    <div className="col-sm-3">
                      <h5>Views</h5>
                      <p>{data.view}</p>
                    </div>
                  </div>
                </div>
                <div className="col-5 main-offers-sectiontwo-divtwo">
                  <div className="row">
                    <div className="col-sm-8">
                      <SlidingComponent
                        name="availability"
                        currentState={data.availability}
                        type={"checkbox"}
                        spantext1={"Active"}
                        spantext2={"Inactive"}
                        toggleclass={"toggle d-flex align-items-center mb-2"}
                        toggleinputclass={"toggle__switch ml-3 mr-3"}
                        onChange={this.toggleFunction}
                        slidingonClick={this.onSelecttogglestatusChange(data)}
                        defaultChecked={
                          data.availability === true ? true : false
                        }
                      />
                    </div>
                    <div className="col-sm-4 d-flex">
                      <img
                        src={require("../../assets/images/dashboard/edit.png")}
                        alt="edit"
                        className="img-fluid offers-image-icon-button"
                        onClick={() => this.onOpenEditModal(data)}
                      />
                      <img
                        src={require("../../assets/images/dashboard/delete.png")}
                        alt="delete"
                        className="img-fluid offers-image-icon-button"
                        onClick={() => this.onClickDeleteOffer(data)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      ));
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  render() {
    //console.log(this.state);
    return (
      <React.Fragment>
        <div className="container-fluid p-0">
          <div className="row">
            <NavbarDashboard />
          </div>
          <div className="row">
            <div className="container-fluid all-page-container-padding table-container">
              <div className="col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center">
                <div className="d-flex justify-content-between align-items-center w-75">
                  <h4 className={"vendor"}>My Events</h4>
                  <AddOffersComponents />
                </div>

                <div className="d-flex justify-content-between main-offers-sectionone">
                  {/*w-25*/}
                  <InputComponent
                    img={require("../../assets/images/search.png")}
                    labelClass={"mylabel"}
                    imageClass={"offers-image-class"}
                    onChange={this.onSearchChange}
                    value={this.state.search}
                    place={"search"}
                    inputclass={"form-control main-offers-input mt-3"}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row main-offers-sectiontwo">
            {this.renderNewEvents()}
            {this.renderEditModal()}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  allEvents: state.allevents
});

export default connect(mapStateToProps, {
  get_events_details,
  delete_newEvents,
  edit_events_details,
  event_update_availability_data
})(withRouter(MainEventsComponents));
