import React, { Component } from "react";
import axios from "axios";
import Modal from "react-responsive-modal";
import InputComponent from "./../../../reusableComponents/InputComponent";
import ButtonComponent from "./../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import classnames from "classnames";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
// import { clear_error } from "./../../../store/actions/errorActions";

export class AddTeamMemberComponents extends Component {
  constructor() {
    super();
    this.state = {
      openModal: false,
      name: "",
      country_code: "",
      mobile_number: "",
      email: "",
      position: "",
      role: "",
      addteammemberimage: [],
      errors: {}
    };
  }

  /**************************
   * @DESC Pop Up Handlers
   *************************/
  onOpenModal = () => {
    this.setState({ openModal: true });
  };

  onCloseModal = () => {
    this.setState({
      openModal: false
    });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post("/image/upload-content-images", data)
      .then(res => {
        let addteammemberimage = this.state.addteammemberimage;
        addteammemberimage.push(res.data.image_URL);
        this.setState({
          addteammemberimage: addteammemberimage,
          loader: false
        });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  /********************************************
   * @DESC ADD RESERVATION POPUP HANDLE SUBMIT
   ********************************************/
  handleAddEventsSubmit = e => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user._id,
      name: this.state.name,
      country_code: this.state.country_code,
      mobile_number: this.state.mobile_number,
      email: this.state.email,
      position: this.state.position,
      role: this.state.role,
      team_member_photo: this.state.addteammemberimage
    };
    console.log(formData);
    // this.props.add_newEvent(formData);
    this.setState({
      name: "",
      country_code: "",
      mobile_number: "",
      email: "",
      position: "",
      role: "",
      addteammemberimage: []
    });
  };

  render() {
    const { openModal, errors } = this.state;
    return (
      <React.Fragment>
        <ButtonComponent
          type={"button"}
          buttontext={"Add Member"}
          buttonclass={"btn button-width button-orange"}
          onClick={this.onOpenModal}
        />
        <Modal
          open={openModal}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-team-member-modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="add-reservation-popup">
            <div className="popup-head">Add Member</div>

            <div className="container mt-3">
              <form noValidate onSubmit={this.handleAddEventsSubmit}>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext="Name"
                      inputlabelclass="input-label"
                      imgbox="img-box"
                      imgsrc={require("../../../assets/images/dashboard/usernamepic.svg")}
                      imgclass="img-fluid img"
                      name="name"
                      type="text"
                      place="eg. John Doe"
                      onChange={this.onChange}
                      value={this.state.name}
                      error={errors.name}
                      inputclass={classnames("input-field", {
                        invalid: errors.name
                      })}
                    />
                  </div>
                </div>
                <div className="row signup-select-form">
                  <label>Phone Number</label>
                  <div className="col-12 d-flex justify-content-between ">
                    <select
                      className="form-control select-custom"
                      name="country_code"
                      onChange={this.onChange}
                      error={errors.country_code}
                      inputclass={classnames("input-field", {
                        invalid: errors.country_code
                      })}
                    >
                      <option value="+91">+91</option>
                      <option value="+92">+92</option>
                      <option value="+977">+977</option>
                    </select>
                    <InputComponent
                      // labeltext='phone number'
                      // inputlabelclass='input-label'
                      imgbox="img-box"
                      imgsrc={require("../../../assets/images/icons/phone.svg")}
                      imgclass="img-fluid img pl-1"
                      name="mobile_number"
                      type="text"
                      place="eg. 1234567890"
                      onChange={this.onChange}
                      value={this.state.mobile_number}
                      error={errors.mobile_number}
                      inputclass={classnames("input-field", {
                        invalid: errors.mobile_number
                      })}
                    />
                  </div>
                </div>
                <InputComponent
                  labeltext="Email ID"
                  inputlabelclass="input-label"
                  imgbox="img-box"
                  imgsrc={require("../../../assets/images/icons/at-the-rate.svg")}
                  imgclass="img-fluid img"
                  name="email"
                  type="email"
                  place="eg. something@something.com"
                  onChange={this.onChange}
                  value={this.state.email}
                  error={errors.email}
                  inputclass={classnames("input-field", {
                    invalid: errors.email
                  })}
                />
                <div className="row">
                  <div className="col">
                    <label htmlFor="Position" className="select-box-label">
                      Position
                    </label>
                    <select
                      className="form-control select-custom"
                      name="position"
                      onChange={this.onChange}
                      error={errors.position}
                      inputclass={classnames("input-field", {
                        invalid: errors.position
                      })}
                    >
                      <option value="" style={{ color: "#e6e6e6" }}>
                        Select Position
                      </option>
                      <option value="Cook">Cook</option>
                      <option value="Delivery Boy">Delivery Boy</option>
                    </select>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <label htmlFor="Role" className="select-box-label">
                      Role
                    </label>
                    <select
                      className="form-control select-custom"
                      name="role"
                      onChange={this.onChange}
                      error={errors.role}
                      inputclass={classnames("input-field", {
                        invalid: errors.role
                      })}
                    >
                      <option value="" style={{ color: "#e6e6e6" }}>
                        Select Role
                      </option>
                      <option value="Cook">Cook</option>
                      <option value="Delivery Boy">Delivery Boy</option>
                    </select>
                  </div>
                </div>
                <div className="row mt-4 mb-4">
                  <div className="col-3">
                    <h2 className="heading-title-events">Add Photo</h2>
                  </div>
                  <div className="col-9">
                    <div
                      className="view_chainsBor mt-0"
                      style={{ width: "100%" }}
                    >
                      <div className="custom_file_upload">
                        <input
                          type="file"
                          name="iconone"
                          id="filetwo"
                          onChange={this.onImageUploadHandler}
                          className="custom_input_upload"
                        />
                        <label
                          className="custom_input_label newChain_add"
                          htmlFor="filetwo"
                        >
                          <div>
                            <i
                              className="fa fa-plus"
                              style={{ color: "#CCCCCC" }}
                            ></i>
                          </div>
                          <div className="add_new_text">Add New</div>
                        </label>
                      </div>

                      <div className="newChain_addthree mx-3">
                        {this.state.addteammemberimage.length > 0
                          ? this.state.addteammemberimage.map(
                              (image, index) => (
                                <img
                                  key={index}
                                  src={image}
                                  className="newChain_addtwo"
                                  style={{ height: "100%", width: "100%" }}
                                  alt="chain "
                                />
                              )
                            )
                          : null}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-12 mb-3">
                  <div className="row">
                    <ButtonComponent
                      type={"button"}
                      buttontext={"Cancel"}
                      buttonclass={"col btn button-width button-border-orange"}
                      onClick={this.onCloseModal}
                    />
                    <ButtonComponent
                      type={"submit"}
                      buttontext={"Save"}
                      buttonclass={"col btn button-width button-orange"}
                      onClick={this.onCloseModal}
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  errors: state.errors.errors,
  auth: state.auth
});

export default connect(mapStateToProps, {
  //   add_newEvent
})(withRouter(AddTeamMemberComponents));
