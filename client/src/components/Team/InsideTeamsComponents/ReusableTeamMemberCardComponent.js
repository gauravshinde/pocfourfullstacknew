import React from "react";

export const ReusableTeamMemberCardComponent = ({
  cardImage,
  cardTitle,
  cardPara,
  viewProfileClick,
  editProfileClick,
  deleteProfileClick
}) => {
  return (
    <React.Fragment>
      <div className="col-3 team-member-card">
        <div className="card inside-team-member-card">
          <img
            className="card-img-top img-fluid profile-image rounded-circle"
            src={cardImage}
            alt="Card icon"
          />
          <div className="card-body text-center">
            <h5 className="card-title">{cardTitle}</h5>
            <p className="card-text">{cardPara}</p>
            <div className="team-member-icon-image">
              <img
                src={require("../../../assets/images/dashboard/eye-icon-table.svg")}
                alt="View Profile"
                className="img-fluid icon-image"
                onClick={viewProfileClick}
              />
              <img
                src={require("../../../assets/images/dashboard/edit-icon-table.svg")}
                alt="Edit Profile"
                className="img-fluid icon-image"
                onClick={editProfileClick}
              />
              <img
                src={require("../../../assets/images/dashboard/deleteTeam.png")}
                alt="Delete Profile"
                className="img-fluid icon-image"
                onClick={deleteProfileClick}
              />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
