import React, { Component } from "react";
import NavbarDashboard from "../Dassboard/NavbarDashboard";
import axios from "axios";
// import SlidingComponent from "./../../reusableComponents/SlidingComponent";
// import dateFns from "date-fns";
import Modal from "react-responsive-modal";
import InputComponent from "./../../reusableComponents/InputComponent";
import ButtonComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import AddTeamMemberComponents from "./InsideTeamsComponents/AddTeamMemberComponents";
import classnames from "classnames";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../store/validation/is-Empty";
import { ReusableTeamMemberCardComponent } from "./InsideTeamsComponents/ReusableTeamMemberCardComponent";
import { send_test_email } from "../../pocsrcone/store/actions/dinerAction";

export class MainEventsComponents extends Component {
  constructor() {
    super();
    this.state = {
      AllEventsDetails: [],
      currentState: true,
      openEditmodal: false,
      openViewModal: false,
      eventupdate: {},
      addEventImage: {},
      search: "",
      errors: {}
    };
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************
   * @DESC - DELETE HANDLER
   ***************************/
  onClickDeleteOffer = data => {
    console.log(data);
    let deleteData = {
      id: data._id
    };
    // console.log(deleteData);
    this.props.delete_newEvents(deleteData);
  };

  /**************************
   * @DESC Pop Up Handlers
   *************************/
  onOpenEditModal = data => {
    this.setState({
      openEditmodal: true,
      eventupdate: data
    });
  };

  onCloseEditModal = () => {
    this.setState({
      openEditmodal: false
    });
  };

  /**************************
   * @DESC Pop Up Handlers
   *************************/
  onOpenViewModal = data => {
    this.setState({
      openViewModal: true,
      eventupdate: data
    });
  };

  onCloseViewModal = () => {
    this.setState({
      openViewModal: false
    });
  };

  handleEditOfferChange = e => {
    // console.log(e.target.value);
    let eventnew = this.state.eventupdate;

    eventnew[e.target.name] = e.target.value;
    // console.log(eventnew.name);
    this.setState({
      eventupdate: eventnew
    });
  };

  onImageUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post("/image/upload-content-images", data)
      .then(res => {
        let addEventImage = this.state.eventupdate.event_photo;
        addEventImage.push(res.data.image_URL);
        this.setState({ eventupdate: addEventImage, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  handleEditOffersSubmit = e => {
    e.preventDefault();

    let editUpdateTeamMember = this.state.eventupdate;

    const editTeamMember = {
      id: editUpdateTeamMember._id,
      name: editUpdateTeamMember.name,
      country_code: editUpdateTeamMember.country_code,
      mobile_number: editUpdateTeamMember.mobile_number,
      email: editUpdateTeamMember.email,
      position: editUpdateTeamMember.position,
      role: editUpdateTeamMember.role,
      team_member_photo: editUpdateTeamMember.addteammemberimage
    };
    console.log(editTeamMember);
    this.setState({
      openEditmodal: false
    });
  };

  renderEditModal() {
    const { openEditmodal, errors, eventupdate } = this.state;
    console.log(eventupdate);
    return (
      <React.Fragment>
        {/* <ButtonComponent
          type={'button'}
          buttontext={'Add Offer'}
          buttonclass={'btn button-width button-orange'}
          onClick={this.onOpenModal}
        /> */}
        <Modal
          open={openEditmodal}
          onClose={this.onCloseEditModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-team-member-modal ",
            closeButton: "customCloseButton"
          }}
        >
          <div className="add-reservation-popup">
            <div className="popup-head">Edit Team Member</div>

            <div className="container mt-3">
              <form>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext="Name"
                      inputlabelclass="input-label"
                      imgbox="img-box"
                      imgsrc={require("../../assets/images/dashboard/usernamepic.svg")}
                      imgclass="img-fluid img"
                      name="name"
                      type="text"
                      place="eg. John Doe"
                      onChange={this.onChange}
                      value={this.state.name}
                      error={errors.name}
                      inputclass={classnames("input-field", {
                        invalid: errors.name
                      })}
                    />
                  </div>
                </div>
                <div className="row signup-select-form">
                  <label>Phone Number</label>
                  <div className="col-12 d-flex justify-content-between ">
                    <select
                      className="form-control select-custom"
                      name="country_code"
                      onChange={this.onChange}
                      error={errors.country_code}
                      inputclass={classnames("input-field", {
                        invalid: errors.country_code
                      })}
                    >
                      <option value="+91">+91</option>
                      <option value="+92">+92</option>
                      <option value="+977">+977</option>
                    </select>
                    <InputComponent
                      // labeltext='phone number'
                      // inputlabelclass='input-label'
                      imgbox="img-box"
                      imgsrc={require("../../assets/images/icons/phone.svg")}
                      imgclass="img-fluid img pl-1"
                      name="mobile_number"
                      type="text"
                      place="eg. 1234567890"
                      onChange={this.onChange}
                      value={this.state.mobile_number}
                      error={errors.mobile_number}
                      inputclass={classnames("input-field", {
                        invalid: errors.mobile_number
                      })}
                    />
                  </div>
                </div>
                <InputComponent
                  labeltext="Email ID"
                  inputlabelclass="input-label"
                  imgbox="img-box"
                  imgsrc={require("../../assets/images/icons/at-the-rate.svg")}
                  imgclass="img-fluid img"
                  name="email"
                  type="email"
                  place="eg. something@something.com"
                  onChange={this.onChange}
                  value={this.state.email}
                  error={errors.email}
                  inputclass={classnames("input-field", {
                    invalid: errors.email
                  })}
                />
                <div className="row">
                  <div className="col">
                    <label htmlFor="Position" className="select-box-label">
                      Position
                    </label>
                    <select
                      className="form-control select-custom"
                      name="position"
                      onChange={this.onChange}
                      error={errors.position}
                      inputclass={classnames("input-field", {
                        invalid: errors.position
                      })}
                    >
                      <option value="" style={{ color: "#e6e6e6" }}>
                        Select Position
                      </option>
                      <option value="Cook">Cook</option>
                      <option value="Delivery Boy">Delivery Boy</option>
                    </select>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <label htmlFor="Role" className="select-box-label">
                      Role
                    </label>
                    <select
                      className="form-control select-custom"
                      name="role"
                      onChange={this.onChange}
                      error={errors.role}
                      inputclass={classnames("input-field", {
                        invalid: errors.role
                      })}
                    >
                      <option value="" style={{ color: "#e6e6e6" }}>
                        Select Role
                      </option>
                      <option value="Cook">Cook</option>
                      <option value="Delivery Boy">Delivery Boy</option>
                    </select>
                  </div>
                </div>
                {/* <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Terms and Conditions"}
                      inputlabelclass={"label-class"}
                      name={"terms_condition"}
                      type={"text"}
                      //value={eventupdate.terms_condition}
                      onChange={this.handleEditOfferChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. A bright Atmosphere"}
                    />
                  </div>
                </div> */}
                {/* <div className="row mt-4 mb-4">
                  <div className="col-3">
                    <h2 className="heading-title-events">Add Photo</h2>
                  </div>
                  <div className="col-9">
                    <div
                      className="view_chainsBor mt-0"
                      style={{ width: "100%" }}
                    >
                      <div className="custom_file_upload">
                        <input
                          type="file"
                          name="iconone"
                          id="filetwo"
                          onChange={this.onImageUploadHandler}
                          className="custom_input_upload"
                        />
                        <label
                          className="custom_input_label newChain_add"
                          htmlFor="filetwo"
                        >
                          <div>
                            <i
                              className="fa fa-plus"
                              style={{ color: "#CCCCCC" }}
                            ></i>
                          </div>
                          <div className="add_new_text">Add New</div>
                        </label>
                      </div>

                      <div className="newChain_addthree mx-3">
                        {this.state.eventupdate.length > 0
                          ? this.state.eventupdate.map((image, index) => (
                              <img
                                key={index}
                                src={image}
                                className="newChain_addtwo"
                                style={{ height: "100%", width: "100%" }}
                                alt="chain "
                              />
                            ))
                          : null}
                      </div>
                    </div>
                  </div>
                </div> */}
                <div className="col-12 mb-3 mt-3">
                  <div className="row">
                    <ButtonComponent
                      type={"button"}
                      buttontext={"Cancel"}
                      buttonclass={"col btn button-width button-border-orange"}
                      onClick={this.onCloseEditModal}
                    />
                    <ButtonComponent
                      type={"button"}
                      buttontext={"Next"}
                      buttonclass={"col btn button-width button-orange"}
                      onClick={this.handleEditOffersSubmit}
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </Modal>
        {/* {this.renderAddImageModal()} */}
      </React.Fragment>
    );
  }

  renderViewModal = () => {
    const { openViewModal, eventupdate } = this.state;
    // console.log(eventupdate);
    return (
      <Modal
        open={openViewModal}
        onClose={this.onCloseViewModal}
        classNames={{
          overlay: "customOverlay main-team-member-ViewNew",
          modal: "main-team-member-View",
          closeButton: "customCloseButton"
        }}
      >
        <React.Fragment>
          <div className="row">
            <div className="col-sm-4 text-center">
              <div className="col-12">
                <img
                  className="img-fluid view-profile-img  rounded-circle"
                  src={require("../../assets/images/dashboard/reviews/user.png")}
                  alt="Card icon"
                />
              </div>
              <div className="col-12 d-flex justify-content-center">
                <img
                  className="img-fluid icon-image"
                  src={require("../../assets/images/dashboard/edit-icon-table.svg")}
                  alt="Edit icon"
                />
                <img
                  className="img-fluid icon-image"
                  src={require("../../assets/images/dashboard/deleteTeam.png")}
                  alt="Delete icon"
                />
              </div>
            </div>
            <div className="col-sm-8">
              <div className="row">
                <div className="col-sm-10">
                  <h3>Zeeshan Sayyed</h3>
                  <hr className="m-0" />
                  <h2>
                    Phone Number: <span>+91 841285369</span>
                  </h2>
                  <h2>
                    Email ID: <span>something@something.com</span>
                  </h2>
                </div>
                <div className="col-sm-2">
                  <img
                    className="img-fluid closed-popup-team"
                    src={require("../../assets/images/dashboard/menu/close-menu-icon.svg")}
                    alt="Closed icon"
                    onClick={this.onCloseViewModal}
                  />
                </div>
              </div>
            </div>
          </div>
          <hr className="m-0" />
          <h3 className="mt-3 mb-3">History</h3>
          <hr className="m-0" />
        </React.Fragment>
      </Modal>
    );
  };

  /********************************************
   * @DESC -ON CHANGE TOGGLE STATUS CHANGE
   ********************************************/
  onSelecttogglestatusChange = data => e => {
    // e.preventDefault();
    const formData = {
      id: data._id,
      availability: data.availability === true ? false : true
    };
    // console.log(formData);
    this.props.event_update_availability_data(formData);
  };

  renderNewEvents = () => {
    // const { AllEventsDetails } = this.state;
    // let filtereddata = [];
    // if (this.state.search) {
    //   let search = new RegExp(this.state.search, "i");
    //   filtereddata = AllEventsDetails.filter(getall => {
    //     if (search.test(getall.name)) {
    //       return getall;
    //     }
    //   });
    //   // console.log(filtereddata);
    // } else {
    //   filtereddata = AllEventsDetails;
    // }
    // if (!isEmpty(filtereddata)) {
    //   return filtereddata.map((data, index) => (
    return (
      <React.Fragment>
        <ReusableTeamMemberCardComponent
          cardImage={require("../../assets/images/dashboard/reviews/user.png")}
          cardTitle="Gaurav Shinde"
          cardPara="Hello World"
          editProfileClick={() => this.onOpenEditModal()}
          viewProfileClick={() => this.onOpenViewModal()}
        />
        <ReusableTeamMemberCardComponent
          cardImage={require("../../assets/images/dashboard/reviews/user.png")}
          cardTitle="Gaurav Shinde"
          cardPara="Hello World"
        />
        <ReusableTeamMemberCardComponent
          cardImage={require("../../assets/images/dashboard/reviews/user.png")}
          cardTitle="Gaurav Shinde"
          cardPara="Hello World"
        />
        <ReusableTeamMemberCardComponent
          cardImage={require("../../assets/images/dashboard/reviews/user.png")}
          cardTitle="Gaurav Shinde"
          cardPara="Hello World"
        />
        <ReusableTeamMemberCardComponent
          cardImage={require("../../assets/images/dashboard/reviews/user.png")}
          cardTitle="Gaurav Shinde"
          cardPara="Hello World"
        />
        <ReusableTeamMemberCardComponent
          cardImage={require("../../assets/images/dashboard/reviews/user.png")}
          cardTitle="Gaurav Shinde"
          cardPara="Hello World"
        />
      </React.Fragment>
    );

    //   ));
    // } else {
    //   return (
    //     <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
    //       Loading...
    //     </h3>
    //   );
    // }
  };

  onEmailSend = e => {
    const formData = {
      vendor_id: this.props.auth.user._id,
      email: "shinde786gaurav@gmail.com"
    };
    this.props.send_test_email(formData);
  };

  render() {
    console.log(this.state);
    return (
      <React.Fragment>
        <div className="container-fluid p-0">
          <div className="row">
            <NavbarDashboard />
          </div>
          <div className="row">
            <div className="container-fluid all-page-container-padding table-container">
              <div className="col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center">
                <div className="d-flex justify-content-between align-items-center w-75">
                  <h4 className={"vendor"}>Team Members</h4>
                  <div className="float-right">
                    <button
                      className="btn btn-primary"
                      onClick={this.onEmailSend}
                    >
                      Email Send
                    </button>
                  </div>
                  <AddTeamMemberComponents />
                </div>

                <div className="d-flex justify-content-between main-offers-sectionone">
                  {/*w-25*/}
                  <InputComponent
                    imgbox="img-box"
                    imgsrc={require("../../assets/images/search.png")}
                    imgclass="img-fluid img"
                    labelClass={"mylabel"}
                    imageClass={"offers-image-class"}
                    onChange={this.onSearchChange}
                    value={this.state.search}
                    place={"search"}
                    inputclass={"form-control main-offers-input mt-3"}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row main-offers-sectiontwo">
            {this.renderNewEvents()}
            {this.renderEditModal()}
            {this.renderViewModal()}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {
  send_test_email
  //   get_events_details,
  //   delete_newEvents,
  //   edit_events_details,
  //   event_update_availability_data
})(withRouter(MainEventsComponents));
