import React, { Component } from "react";
import RestaurantImages from "./RestaurantImages";
import KycImages from "./KycImages";
import RestaurantMenuImages from "./RestaurantMenuImages";

export class MainViewAllImages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectPages: 1
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectPages: value
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="col-12 view_all_images_boxOne">
          <h1 className="main_view_heading_text">All Images</h1>
          <div className="d-flex justify-content-center mt-4 mb-4">
            <button
              type="button"
              onClick={this.onPageChange(1)}
              className={
                this.state.selectPages === 1
                  ? "view_all_images_boxOne_button_active"
                  : "view_all_images_boxOne_button"
              }
            >
              Restaurant Photo
            </button>
            <button
              type="button"
              onClick={this.onPageChange(2)}
              className={
                this.state.selectPages === 2
                  ? "view_all_images_boxOne_button_active"
                  : "view_all_images_boxOne_button"
              }
            >
              Restaurant Kyc
            </button>
            <button
              type="button"
              onClick={this.onPageChange(3)}
              className={
                this.state.selectPages === 3
                  ? "view_all_images_boxOne_button_active"
                  : "view_all_images_boxOne_button"
              }
            >
              Restaurant Menu
            </button>
          </div>
          <div className="col-12 p-0">
            <div id="style-4" className="view_all_images_boxOne_Body">
              {this.state.selectPages === 1 ? <RestaurantImages /> : ""}
              {this.state.selectPages === 2 ? <KycImages /> : ""}
              {this.state.selectPages === 3 ? <RestaurantMenuImages /> : ""}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MainViewAllImages;
