import React, { Component } from "react";
import Modal from "react-responsive-modal";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../../store/validation/is-Empty";
import { get_all_user_restaurant_kyc } from "./../../../../store/actions/ViewAllUserImages";

export class KycImages extends Component {
  constructor() {
    super();
    this.state = {
      userKycImages: {},
      // data: [
      //   {
      //     id: 1,
      //     images: "https://chainlist.s3.amazonaws.com/1580298478993"
      //   },
      //   {
      //     id: 2,
      //     images: "https://chainlist.s3.amazonaws.com/1580377002391"
      //   },
      //   {
      //     id: 3,
      //     images: "https://chainlist.s3.amazonaws.com/1580298337149"
      //   },
      //   {
      //     id: 4,
      //     images: "https://chainlist.s3.amazonaws.com/1580298345994"
      //   },
      //   {
      //     id: 5,
      //     images: "https://chainlist.s3.amazonaws.com/1580298352771"
      //   },
      //   {
      //     id: 6,
      //     images: "https://chainlist.s3.amazonaws.com/1580377046412"
      //   },
      //   {
      //     id: 7,
      //     images: "https://chainlist.s3.amazonaws.com/1580298506508"
      //   },
      //   {
      //     id: 8,
      //     images: "https://chainlist.s3.amazonaws.com/1580298513828"
      //   },
      //   {
      //     id: 9,
      //     images: "https://chainlist.s3.amazonaws.com/1580298478993"
      //   },
      //   {
      //     id: 10,
      //     images: "https://chainlist.s3.amazonaws.com/1580377002391"
      //   },
      //   {
      //     id: 11,
      //     images: "https://chainlist.s3.amazonaws.com/1580298337149"
      //   },
      //   {
      //     id: 12,
      //     images: "https://chainlist.s3.amazonaws.com/1580298345994"
      //   },
      //   {
      //     id: 13,
      //     images: "https://chainlist.s3.amazonaws.com/1580298352771"
      //   },
      //   {
      //     id: 14,
      //     images: "https://chainlist.s3.amazonaws.com/1580377046412"
      //   },
      //   {
      //     id: 15,
      //     images: "https://chainlist.s3.amazonaws.com/1580298506508"
      //   },
      //   {
      //     id: 16,
      //     images: "https://chainlist.s3.amazonaws.com/1580298513828"
      //   },
      //   {
      //     id: 17,
      //     images: "https://chainlist.s3.amazonaws.com/1580298478993"
      //   },
      //   {
      //     id: 18,
      //     images: "https://chainlist.s3.amazonaws.com/1580377002391"
      //   },
      //   {
      //     id: 19,
      //     images: "https://chainlist.s3.amazonaws.com/1580298337149"
      //   },
      //   {
      //     id: 20,
      //     images: "https://chainlist.s3.amazonaws.com/1580298345994"
      //   },
      //   {
      //     id: 21,
      //     images: "https://chainlist.s3.amazonaws.com/1580298352771"
      //   },
      //   {
      //     id: 22,
      //     images: "https://chainlist.s3.amazonaws.com/1580377046412"
      //   },
      //   {
      //     id: 23,
      //     images: "https://chainlist.s3.amazonaws.com/1580298506508"
      //   },
      //   {
      //     id: 24,
      //     images: "https://chainlist.s3.amazonaws.com/1580298513828"
      //   },
      //   {
      //     id: 25,
      //     images: "https://chainlist.s3.amazonaws.com/1580298478993"
      //   },
      //   {
      //     id: 26,
      //     images: "https://chainlist.s3.amazonaws.com/1580377002391"
      //   },
      //   {
      //     id: 27,
      //     images: "https://chainlist.s3.amazonaws.com/1580298337149"
      //   },
      //   {
      //     id: 28,
      //     images: "https://chainlist.s3.amazonaws.com/1580298345994"
      //   },
      //   {
      //     id: 29,
      //     images: "https://chainlist.s3.amazonaws.com/1580298352771"
      //   },
      //   {
      //     id: 30,
      //     images: "https://chainlist.s3.amazonaws.com/1580377046412"
      //   },
      //   {
      //     id: 31,
      //     images: "https://chainlist.s3.amazonaws.com/1580298506508"
      //   },
      //   {
      //     id: 32,
      //     images: "https://chainlist.s3.amazonaws.com/1580298513828"
      //   }
      // ],
      showModal: false
    };
  }

  componentDidMount() {
    if (!isEmpty(this.props.get_all_user_restaurant_kyc)) {
      this.props.get_all_user_restaurant_kyc();
    }
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    console.log(nextProps.kycImage);
    if (!isEmpty(nextProps.kycImage !== nextState.userKycImages)) {
      return {
        userKycImages: nextProps.kycImage
      };
    }
    return null;
  }

  getModal = value => {
    this.setState({ showModal: value });
  };

  hideModal = value => {
    console.log(value);
    this.setState({ showModal: 0 });
  };

  render() {
    const { userKycImages, showModal } = this.state;
    console.log(userKycImages);
    const newData = { ...userKycImages[0] };
    const oldData = newData.imgPath;
    const data = [{ ...oldData }];
    // console.log(oldData);
    console.log(data);
    return (
      <React.Fragment>
        <div className="col-12 p-0 view_images_restaurant_body_header">
          <h1>Restaurant KYC Gallery</h1>
        </div>
        <div className="row">
          <div className="col-12">
            <div id="photos">
              {data.map((images, index) => (
                <div key={index}>
                  <img
                    src={images}
                    className="popup-button-images img-thumbnail"
                    alt="Grid Photos"
                    onClick={() => this.getModal(index)}
                  />
                  <Modal
                    open={showModal === index}
                    onClose={() => this.hideModal(index)}
                    classNames={{
                      overlay: "customOverlay",
                      modal: "add-image-modal-popup",
                      closeButton: "customCloseButton"
                    }}
                    center
                  >
                    <div className="image-restaurant-gallery">
                      <img
                        src={images}
                        className="img-fluid"
                        alt="Grid Photos"
                      />
                      <button
                        className="closed-button"
                        onClick={() => this.hideModal(index)}
                      >
                        <i
                          class="fa fa-times-circle-o closed-button-font-image"
                          aria-hidden="true"
                        ></i>
                      </button>
                    </div>
                  </Modal>
                </div>
              ))}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  kycImage: state.viewAllUserImages.restaurant_kyc
});

export default connect(mapStateToProps, {
  get_all_user_restaurant_kyc
})(withRouter(KycImages));
