import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AddNewChain } from "./../../ParentComment/RestaurantTimings/AddNewChain";
import { ViewAllChains } from "./../../ParentComment/RestaurantTimings/ViewAllChains";
import Toggle from "./../../../reusableComponents/SlidingComponent";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import SlidingComponent from "./../../../reusableComponents/SlidingComponent";
import InputComponent from "./../../../reusableComponents/InputComponent";
import classnames from "classnames";

import IconBox from "./../../../reusableComponents/IconBoxButtonComponent";
import AddMoreComponent from "./../../../reusableComponents/AddMoreComponent";
import SelectedSeatingAreaPopup from "./AddMoreComponent/SelectedSeatingAreaPopup";
import {
  get_kyc_details,
  get_restauranttime,
  update_kyc_details,
  update_restaurant_timings_new,
} from "../../../store/actions/addDetailsActions";
import isEmpty from "./../../../store/validation/is-Empty";
import { serverApi } from "../../../config/Keys";
import Alert from "react-s-alert";

const seating_area = [
  { title: "Indoor" },
  { title: "Outdoor" },
  { title: "Private" },
  { title: "Bar" },
];

const foodTime = ["breakfast"];
const foodTimeTwo = ["lunch"];
const foodTimeThree = ["dinner"];

const dayArray = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday",
];

const timeArray = [
  "00:00",
  "00:30",
  "01:00",
  "01:30",
  "02:00",
  "02:30",
  "03:00",
  "03:30",
  "04:00",
  "04:30",
  "05:00",
  "05:30",
  "06:00",
  "06:30",
  "07:00",
  "07:30",
  "08:00",
  "08:30",
  "09:00",
  "09:30",
  "10:00",
  "10:30",
  "11:00",
  "11:30",
  "12:00",
  "12:30",
  "13:00",
  "13:30",
  "14:00",
  "14:30",
  "15:00",
  "15:30",
  "16:00",
  "16:30",
  "17:00",
  "17:30",
  "18:00",
  "18:30",
  "19:00",
  "19:30",
  "20:00",
  "20:30",
  "21:00",
  "21:30",
  "22:00",
  "22:30",
  "23:00",
  "23:30",
  "24:00",
  "24:30",
];

export class CompanyDetails extends Component {
  constructor() {
    super();
    this.state = {
      allRestaurantTiming: [],
      allRestaurantKycDetails: [],
      disabled: false,
      // all_seating_area_icons: seating_area,
      selected_seating_area: [],
      primary_seating_area: {},
      errors: {},
      hasSetDetails: false,

      total_seating_capacity: "",
      part_of_chain: false,
      selectedChains: [],
      are_you_open_24_x_7: false,
      multiple_opening_time: false,
      table_management: false,

      monday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          // breakfast_opening_time: "",
          // breakfast_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          // lunch_opening_time: "",
          // lunch_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          // dinner_opening_time: "",
          // dinner_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
      },
      tuesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          // breakfast_opening_time: "",
          // breakfast_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          // lunch_opening_time: "",
          // lunch_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          // dinner_opening_time: "",
          // dinner_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
      },
      wednesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          // breakfast_opening_time: "",
          // breakfast_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          // lunch_opening_time: "",
          // lunch_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          // dinner_opening_time: "",
          // dinner_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
      },
      thursday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          // breakfast_opening_time: "",
          // breakfast_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          // lunch_opening_time: "",
          // lunch_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          // dinner_opening_time: "",
          // dinner_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
      },
      friday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          // breakfast_opening_time: "",
          // breakfast_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          // lunch_opening_time: "",
          // lunch_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          // dinner_opening_time: "",
          // dinner_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
      },
      saturday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          // breakfast_opening_time: "",
          // breakfast_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          // lunch_opening_time: "",
          // lunch_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          // dinner_opening_time: "",
          // dinner_closing_time: ""
          opening_time: "",
          closing_time: "",
        },
      },
      sunday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },

      account_name: "",
      bank_name: "",
      account_number: "",
      branch_name: "",
      GST_number: "",
      IFSC_code: "",
      PAN_number: "",
      FSSAI_code: "",
      imgPath: [],
      currentAccount: false,
      seatingData: [],
    };
  }

  componentDidMount() {
    this.props.get_kyc_details();
    this.props.get_restauranttime();
    this.setState({
      // part_of_chain: this.props.details.restauranttime_details.part_of_chain,
      // are_you_open_24_x_7: this.props.details.restauranttime_details
      //   .are_you_open_24_x_7,
      // multiple_opening_time: this.props.details.restauranttime_details
      //   .multiple_opening_time,
      // table_management: this.props.details.restauranttime_details
      //   .table_management,
      // currentAccount: this.props.details.kyc_details.currentAccount
      seatingData: this.props.details.restauranttime_details
        .primary_seating_area,
    });
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.details.restauranttime_details);
    // console.log(nextProps.details.kyc_details);
    if (
      !isEmpty(nextProps.details.kyc_details) &&
      !isEmpty(nextProps.details.restauranttime_details) &&
      !nextState.hasSetDetails
    ) {
      return {
        allRestaurantTiming: nextProps.details.restauranttime_details,
        allRestaurantKycDetails: nextProps.details.kyc_details,
        part_of_chain: nextProps.details.restauranttime_details.part_of_chain,
        selectedChains: nextProps.details.restauranttime_details.selectedChains,
        are_you_open_24_x_7:
          nextProps.details.restauranttime_details.are_you_open_24_x_7,
        multiple_opening_time:
          nextProps.details.restauranttime_details.multiple_opening_time,
        table_management:
          nextProps.details.restauranttime_details.table_management,

        total_seating_capacity:
          nextProps.details.restauranttime_details.total_seating_capacity,

        selected_seating_area:
          nextProps.details.restauranttime_details.selected_seating_area,
        primary_seating_area:
          isEmpty(
            nextProps.details.restauranttime_details.primary_seating_area
          ) ||
          nextProps.details.restauranttime_details.primary_seating_area.title,

        monday: {
          open: nextProps.details.restauranttime_details.monday.open,
          main_opening_time:
            nextProps.details.restauranttime_details.monday.main_opening_time,
          main_closing_time:
            nextProps.details.restauranttime_details.monday.main_closing_time,
          breakfast: {
            open:
              nextProps.details.restauranttime_details.monday.breakfast.open,
            // breakfast_opening_time: "",
            // breakfast_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.monday.breakfast
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.monday.breakfast
                .closing_time,
          },
          lunch: {
            open: nextProps.details.restauranttime_details.monday.lunch.open,
            // lunch_opening_time: "",
            // lunch_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.monday.lunch
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.monday.lunch
                .closing_time,
          },
          dinner: {
            open: nextProps.details.restauranttime_details.monday.dinner.open,
            // dinner_opening_time: "",
            // dinner_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.monday.dinner
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.monday.dinner
                .closing_time,
          },
        },
        tuesday: {
          open: nextProps.details.restauranttime_details.tuesday.open,
          main_opening_time:
            nextProps.details.restauranttime_details.tuesday.main_opening_time,
          main_closing_time:
            nextProps.details.restauranttime_details.tuesday.main_closing_time,
          breakfast: {
            open:
              nextProps.details.restauranttime_details.tuesday.breakfast.open,
            // breakfast_opening_time: "",
            // breakfast_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.tuesday.breakfast
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.tuesday.breakfast
                .closing_time,
          },
          lunch: {
            open: nextProps.details.restauranttime_details.tuesday.lunch.open,
            // lunch_opening_time: "",
            // lunch_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.tuesday.lunch
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.tuesday.lunch
                .closing_time,
          },
          dinner: {
            open: nextProps.details.restauranttime_details.tuesday.dinner.open,
            // dinner_opening_time: "",
            // dinner_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.tuesday.dinner
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.tuesday.dinner
                .closing_time,
          },
        },
        wednesday: {
          open: nextProps.details.restauranttime_details.wednesday.open,
          main_opening_time:
            nextProps.details.restauranttime_details.wednesday
              .main_opening_time,
          main_closing_time:
            nextProps.details.restauranttime_details.wednesday
              .main_closing_time,
          breakfast: {
            open:
              nextProps.details.restauranttime_details.wednesday.breakfast.open,
            // breakfast_opening_time: "",
            // breakfast_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.wednesday.breakfast
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.wednesday.breakfast
                .closing_time,
          },
          lunch: {
            open: nextProps.details.restauranttime_details.wednesday.lunch.open,
            // lunch_opening_time: "",
            // lunch_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.wednesday.lunch
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.wednesday.lunch
                .closing_time,
          },
          dinner: {
            open:
              nextProps.details.restauranttime_details.wednesday.dinner.open,
            // dinner_opening_time: "",
            // dinner_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.wednesday.dinner
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.wednesday.dinner
                .closing_time,
          },
        },
        thursday: {
          open: nextProps.details.restauranttime_details.thursday.open,
          main_opening_time:
            nextProps.details.restauranttime_details.thursday.main_opening_time,
          main_closing_time:
            nextProps.details.restauranttime_details.thursday.main_closing_time,
          breakfast: {
            open:
              nextProps.details.restauranttime_details.thursday.breakfast.open,
            // breakfast_opening_time: "",
            // breakfast_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.thursday.breakfast
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.thursday.breakfast
                .closing_time,
          },
          lunch: {
            open: nextProps.details.restauranttime_details.thursday.lunch.open,
            // lunch_opening_time: "",
            // lunch_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.thursday.lunch
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.thursday.lunch
                .closing_time,
          },
          dinner: {
            open: nextProps.details.restauranttime_details.thursday.dinner.open,
            // dinner_opening_time: "",
            // dinner_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.thursday.dinner
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.thursday.dinner
                .closing_time,
          },
        },
        friday: {
          open: nextProps.details.restauranttime_details.friday.open,
          main_opening_time:
            nextProps.details.restauranttime_details.friday.main_opening_time,
          main_closing_time:
            nextProps.details.restauranttime_details.friday.main_closing_time,
          breakfast: {
            open:
              nextProps.details.restauranttime_details.friday.breakfast.open,
            // breakfast_opening_time: "",
            // breakfast_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.friday.breakfast
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.friday.breakfast
                .closing_time,
          },
          lunch: {
            open: nextProps.details.restauranttime_details.friday.lunch.open,
            // lunch_opening_time: "",
            // lunch_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.friday.lunch
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.friday.lunch
                .closing_time,
          },
          dinner: {
            open: nextProps.details.restauranttime_details.friday.dinner.open,
            // dinner_opening_time: "",
            // dinner_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.friday.dinner
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.friday.dinner
                .closing_time,
          },
        },
        saturday: {
          open: nextProps.details.restauranttime_details.saturday.open,
          main_opening_time:
            nextProps.details.restauranttime_details.saturday.main_opening_time,
          main_closing_time:
            nextProps.details.restauranttime_details.saturday.main_closing_time,
          breakfast: {
            open:
              nextProps.details.restauranttime_details.saturday.breakfast.open,
            // breakfast_opening_time: "",
            // breakfast_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.saturday.breakfast
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.saturday.breakfast
                .closing_time,
          },
          lunch: {
            open: nextProps.details.restauranttime_details.saturday.lunch.open,
            // lunch_opening_time: "",
            // lunch_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.saturday.lunch
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.saturday.lunch
                .closing_time,
          },
          dinner: {
            open: nextProps.details.restauranttime_details.saturday.dinner.open,
            // dinner_opening_time: "",
            // dinner_closing_time: ""
            opening_time:
              nextProps.details.restauranttime_details.saturday.dinner
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.saturday.dinner
                .closing_time,
          },
        },
        sunday: {
          open: nextProps.details.restauranttime_details.sunday.open,
          main_opening_time:
            nextProps.details.restauranttime_details.sunday.main_opening_time,
          main_closing_time:
            nextProps.details.restauranttime_details.sunday.main_closing_time,
          breakfast: {
            open:
              nextProps.details.restauranttime_details.sunday.breakfast.open,
            opening_time:
              nextProps.details.restauranttime_details.sunday.breakfast
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.sunday.breakfast
                .closing_time,
          },
          lunch: {
            open: nextProps.details.restauranttime_details.sunday.lunch.open,
            opening_time:
              nextProps.details.restauranttime_details.sunday.lunch
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.sunday.lunch
                .closing_time,
          },
          dinner: {
            open: nextProps.details.restauranttime_details.sunday.dinner.open,
            opening_time:
              nextProps.details.restauranttime_details.sunday.dinner
                .opening_time,
            closing_time:
              nextProps.details.restauranttime_details.sunday.dinner
                .closing_time,
          },
        },

        //Company Setup

        account_name: nextProps.details.kyc_details.account_name,
        bank_name: nextProps.details.kyc_details.bank_name,
        account_number: nextProps.details.kyc_details.account_number,
        branch_name: nextProps.details.kyc_details.branch_name,
        GST_number: nextProps.details.kyc_details.GST_number,
        IFSC_code: nextProps.details.kyc_details.IFSC_code,
        PAN_number: nextProps.details.kyc_details.PAN_number,
        FSSAI_code: nextProps.details.kyc_details.FSSAI_code,
        imgPath: nextProps.details.kyc_details.imgPath,
        currentAccount: nextProps.details.kyc_details.currentAccount,
        hasSetDetails: true,
      };
    }

    // if (!isEmpty(nextProps.details.restauranttime_details) && !nextState)
    return null;
  }

  /***********************
   * @DESC - PAGE CHANGER
   **********************/
  pageChangeHandle = (value) => (e) => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    let data = e.target.value.split(" ");
    let newarray1 = [];
    for (let x = 0; x < data.length; x++) {
      newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
    }
    let newData = newarray1.join(" ");
    this.setState({
      // [e.target.name]: e.target.value
      [e.target.name]: newData,
    });
    // this.setState({
    //   [e.target.name]: e.target.value
    // });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = (e) => {
    this.setState({
      [e.target.name]: e.target.checked,
    });
  };

  /**************************
   * @DESC - DISABLE BUTTON
   ***************************/
  onClickCompanySetupDisabled = (e) => {
    this.setState({
      disabled: true,
    });
  };

  /****************************
   * @DESC - DAY TOGGLER -
   ***************************/
  onDayOpenCloseHanlder = (day) => (e) => {
    let state = this.state;
    let dayData = state[day];
    dayData.open = !dayData.open;
    this.setState({
      state: state,
    });
  };

  onTimeSelectHandlerSingle = (day) => (e) => {
    let state = this.state;
    let dayData = state[day];
    dayData[e.target.name] = e.target.value;
    this.setState({
      state: state,
    });
  };

  onFoodTypeOpenClose = (day, food) => (e) => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData.open = !foodTimeData.open;
    this.setState({
      state: state,
    });
  };

  onTimeSelectHandlerMultiple = (day, food) => (e) => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData[e.target.name] = e.target.value;
    this.setState({
      state: state,
    });
  };

  /****************************************
   * @DESC - Seating Area CODE SELECTOR
   ****************************************/
  onSeatingAreaArraySelector = (icon) => (e) => {
    let SeatingArea = this.state.selected_seating_area;
    if (SeatingArea.length === 0) {
      SeatingArea.push(icon);
    } else {
      let isAlreadyPresent = SeatingArea.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = SeatingArea.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          SeatingArea.splice(indexOf, 1);
        }
      } else {
        SeatingArea.push(icon);
      }
    }
    this.setState({
      selected_seating_area: SeatingArea,
    });
  };

  onSelectedAreaPrimarySelector = (icon) => (e) => {
    this.setState({
      primary_seating_area: icon,
    });
  };

  primary_selected_icons = (iconNew) => {
    console.log(iconNew);
    this.setState({
      primary_seating_area: iconNew.title,
      seatingData: iconNew,
    });
    // console.log(icon);
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = (e) => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post(`${serverApi}/image/upload-content-images`, data)
      .then((res) => {
        let imgPath = this.state.imgPath;
        imgPath.push(res.data.image_URL);
        this.setState({ imgPath: imgPath, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  onSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
    let updateData = {
      vendor_id: this.props.auth.user._id,
      part_of_chain: this.state.part_of_chain,
      are_you_open_24_x_7: this.state.are_you_open_24_x_7,
      multiple_opening_time: this.state.multiple_opening_time,
      table_management: this.state.table_management,

      selected_seating_area: this.state.selected_seating_area,
      primary_seating_area: this.state.seatingData,

      total_seating_capacity: this.state.total_seating_capacity,
      monday: this.state.monday,
      tuesday: this.state.tuesday,
      wednesday: this.state.wednesday,
      thursday: this.state.thursday,
      friday: this.state.friday,
      saturday: this.state.saturday,
      sunday: this.state.sunday,
      // };
      // console.log(updateData);
      // // this.props.update_restaurant_timings_new(formData)
      // let updateData = {
      //vendor_id: this.props.auth.user._id,
      account_name: this.state.account_name,
      bank_name: this.state.bank_name,
      account_number: this.state.account_number,
      branch_name: this.state.branch_name,
      GST_number: this.state.GST_number,
      IFSC_code: this.state.IFSC_code,
      PAN_number: this.state.PAN_number,
      FSSAI_code: this.state.FSSAI_code,
      imgPath: this.state.imgPath,
      currentAccount: this.state.currentAccount,
    };
    //console.log(updateData);
    // this.props.update_kyc_details(updateData);
    this.props.update_restaurant_timings_new(updateData);
    Alert.success("<h4>Company Details Successfully Updated</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 5000,
      offset: 30,
    });
  };

  renderRestaurantTimings = () => {
    return (
      <React.Fragment>
        {/* <hr className='hr-global mt-5 mb-2' /> */}
        <div className="col-4 ml-auto mt-5 d-flex justify-content-between">
          {/* <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            // onClick={this.pageChangeHandle(9)}
          /> */}
          <ButtonComponent
            buttontext="Save"
            buttontype="button"
            buttonclass="btn button-main button-orange"
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  renderCompanySetup = () => {
    const { errors } = this.state;
    const { disabled, currentAccount } = this.state;
    // console.log(currentAccount);
    return (
      <React.Fragment>
        <h4 className="col-12 mb-4 mt-5 pl-0">
          Company Setup
          <img
            src={require("../../../assets/images/dashboard/edit.svg")}
            alt="edit"
            className="img-fluid float-right"
            onClick={this.onClickCompanySetupDisabled}
            style={{ width: "2%", height: "2%" }}
          />
        </h4>
        <div className="row">
          <div className="col-6">
            <InputComponent
              labeltext="Account Holder Name"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="account_name"
              type="text"
              place="eg. McDonalds"
              onChange={this.onChange}
              value={this.state.account_name}
              error={errors.account_name}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.account_name,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-6">
            <InputComponent
              labeltext="Bank Name"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="bank_name"
              type="text"
              place="eg. McDonalds"
              onChange={this.onChange}
              value={this.state.bank_name}
              error={errors.bank_name}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.bank_name,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-6">
            <InputComponent
              labeltext="Account Number"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="account_number"
              type="number"
              place="eg. McDonalds"
              onChange={this.onChange}
              value={this.state.account_number}
              error={errors.account_number}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.account_number,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-6">
            <InputComponent
              labeltext="Branch Name"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="branch_name"
              type="text"
              place="eg. McDonalds"
              onChange={this.onChange}
              value={this.state.branch_name}
              error={errors.branch_name}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.branch_name,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-6 toggleclass">
            <div className="account-type restaurant-title mt-0">
              Account Type?
            </div>
            <SlidingComponent
              name="currentAccount"
              currentState={this.state.currentAccount}
              type={"checkbox"}
              spantext1={"Current Account"}
              spantext2={"Savings Account"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={this.toggleFunction}
              defaultChecked={currentAccount === true ? true : false}
            />
          </div>
          <div className="col-6">
            <InputComponent
              labeltext="FSSAI Code"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="FSSAI_code"
              type="text"
              place="eg. admin@gmail.com"
              onChange={this.onChange}
              value={this.state.FSSAI_code}
              error={errors.FSSAI_code}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.FSSAI_code,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-4">
            <InputComponent
              labeltext="GST Number"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="GST_number"
              type="text"
              place="eg. Koregaon Park"
              onChange={this.onChange}
              value={this.state.GST_number}
              error={errors.GST_number}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.GST_number,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-4">
            <InputComponent
              labeltext="IFSC Code"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="IFSC_code"
              type="text"
              place="eg. Pune"
              onChange={this.onChange}
              value={this.state.IFSC_code}
              error={errors.IFSC_code}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.IFSC_code,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-4">
            <InputComponent
              labeltext="PAN Number"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="PAN_number"
              type="text"
              place="eg. 9898989898"
              onChange={this.onChange}
              value={this.state.PAN_number}
              error={errors.PAN_number}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.PAN_number,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="row w-100 mb-3 ">
            <div className="col-sm-4">
              <p className="restaurant-title pl-3">Upload Documents</p>
            </div>
            <div className="col-sm-8">
              <div
                className="view_chainsBor overflow-auto"
                style={{ width: "100%" }}
              >
                <div className="custom_file_upload">
                  <input
                    type="file"
                    name="icon"
                    id="file"
                    onChange={this.onImageUploadHandler}
                    className="custom_input_upload"
                  />
                  <label
                    className="custom_input_label newChain_add"
                    htmlFor="file"
                  >
                    <div>
                      <i
                        className="fa fa-plus"
                        style={{ color: "#CCCCCC" }}
                      ></i>
                    </div>
                    <div className="add_new_text">Add New</div>
                  </label>
                </div>

                <div className="newChain_addthree mx-3">
                  {this.state.imgPath.length > 0
                    ? this.state.imgPath.map((image, index) => (
                        <img
                          key={index}
                          src={image}
                          className="newChain_addtwo"
                          style={{ height: "100%", width: "100%" }}
                          alt="chain "
                        />
                      ))
                    : null}
                  {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
                  {/* <img
                    src={this.state.imgPath}
                    className='newChain_addtwo'
                    style={{ height: '100%', width: '100%' }}
                    alt='upload Kyc'
                  /> */}
                </div>
              </div>
              {/* <div className='multiImageInputFile-outerBlock multiImageInputFile-outerBlock--KYC'>
                <AddNewChain />
                <div className='multiImageInputFile-block'>
                  <img
                    src={require('../../../assets/images/add-file.svg')}
                    alt='add-file-img'
                  />
                  <input
                    className='multiImageInputFile'
                    type='file'
                    multiple
                    title=''
                    onChange={this.handleFilesOnChange}
                  />
                </div>
                <div className='multiImageInputFile-preview'>
                  {this.state.imgPath.map((val, index) => (
                    <div key={index}>
                      <img src={val} alt='list' />
                    </div>
                  ))}
                </div> 
              </div> */}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log(this.state);
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4 className="col-12 mb-3">Restaurant Details</h4>
          <form className="pl-3 pr-3">
            <PartOfChain
              state={this.state}
              toggleFunction={this.toggleFunction}
            />
            <Restauranttiming
              state={this.state}
              toggleFunction={this.toggleFunction}
            />
            {!this.state.multiple_opening_time &&
            !this.state.are_you_open_24_x_7 ? (
              <NoMultipleTime
                state={this.state}
                toggleFunction={this.toggleFunction}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onTimeSelectHandlerSingle={this.onTimeSelectHandlerSingle}
              />
            ) : null}
            {this.state.multiple_opening_time &&
            !this.state.are_you_open_24_x_7 ? (
              <YesMultipleTime
                state={this.state}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onFoodTypeOpenClose={this.onFoodTypeOpenClose}
                onTimeSelectHandlerMultiple={this.onTimeSelectHandlerMultiple}
              />
            ) : null}
            <SeatingArrangement
              state={this.state}
              onSeatingAreaArraySelector={this.onSeatingAreaArraySelector}
              onSelectedAreaPrimarySelector={this.onSelectedAreaPrimarySelector}
              toggleFunction={this.toggleFunction}
              onChange={this.onChange}
            />
            <div className="d-flex flex-wrap">
              <div className="col-12 p-0">
                <div className="d-flex justify-content-between">
                  <p className="p-0 restaurant-title">Selected Seating Area</p>
                  <SelectedSeatingAreaPopup
                    selectedSeatingArea={this.state.selected_seating_area}
                    stateData={this.state.allRestaurantTiming}
                    stateDataTwo={this.state.allRestaurantKycDetails}
                  />
                </div>
              </div>
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                {this.state.selected_seating_area.map((iconNew, index) => (
                  <IconBox
                    key={index}
                    src={iconNew.icon}
                    title={iconNew.title}
                    classsection={
                      iconNew.title === this.state.primary_seating_area
                        ? "main-icon-button_active"
                        : "main-icon-button"
                    }
                    onClick={() => this.primary_selected_icons(iconNew)}
                  />
                ))}
              </div>
              <AddMoreComponent
                title={"Add More"}
                classsection="main-suggest-button"
              />
            </div>
            {this.renderCompanySetup()}
            {this.renderRestaurantTimings()}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  details: state.details,
});

export default connect(mapStateToProps, {
  get_kyc_details,
  get_restauranttime,
  update_kyc_details,
  update_restaurant_timings_new,
})(withRouter(CompanyDetails));

const PartOfChain = ({ state, toggleFunction }) => {
  // console.log(state.part_of_chain);
  return (
    <>
      <div className="cuisine-main">
        <p className="col-12 p-0 restaurant-title">Are you a part of chain ?</p>
        {/* <Toggle
          name="part_of_chain"
          currentState={state.part_of_chain}
          type={"checkbox"}
          spantext1={"Yes"}
          spantext2={"No"}
          toggleclass={"toggle d-flex align-items-center mb-2"}
          toggleinputclass={"toggle__switch ml-3 mr-3"}
          onChange={toggleFunction}
          defaultChecked={state.part_of_chain === true ? true : false}
        /> */}
        <SlidingComponent
          name="part_of_chain"
          currentState={state.part_of_chain}
          type={"checkbox"}
          spantext1={"Yes"}
          spantext2={"No"}
          toggleclass={"toggle d-flex align-items-center mb-2"}
          toggleinputclass={"toggle__switch ml-3 mr-3"}
          onChange={toggleFunction}
          defaultChecked={state.part_of_chain === true ? true : false}
        />
      </div>
      {state.part_of_chain ? (
        <div className="cuisine-main mt-4">
          <table style={{ width: "100%", borderRadius: "0px" }}>
            <tbody>
              <tr>
                <td className="seleect_chain"> Select Chain </td>
                <td style={{ width: "10%" }}>
                  <ViewAllChains />
                </td>
              </tr>
            </tbody>
          </table>

          <div className="view_chainsBor">
            <AddNewChain />
          </div>
        </div>
      ) : null}
    </>
  );
};

const Restauranttiming = ({ state, toggleFunction }) => {
  return (
    <>
      <div className="cuisine-main">
        <p className="col-12 p-0 restaurant-title">
          Enter your restaurant timings ?
        </p>
        <p className="col-12 p-0 restaurant-title">Are you open 24 x 7 ?</p>
        <SlidingComponent
          name="are_you_open_24_x_7"
          currentState={state.are_you_open_24_x_7}
          type={"checkbox"}
          spantext1={"Yes"}
          spantext2={"No"}
          toggleclass={"toggle d-flex align-items-center mb-2"}
          toggleinputclass={"toggle__switch ml-3 mr-3"}
          onChange={toggleFunction}
          defaultChecked={state.are_you_open_24_x_7 === true ? true : false}
        />

        {!state.are_you_open_24_x_7 ? (
          <p className="col-12 p-0 restaurant-title">
            Do you have multiple opening timings ?
          </p>
        ) : null}
        {!state.are_you_open_24_x_7 ? (
          <SlidingComponent
            name="multiple_opening_time"
            currentState={state.multiple_opening_time}
            type={"checkbox"}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center mb-2"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={toggleFunction}
            defaultChecked={state.multiple_opening_time}
          />
        ) : null}
      </div>
    </>
  );
};

const NoMultipleTime = ({
  state,
  toggleFunction,
  onDayOpenCloseHanlder,
  onTimeSelectHandlerSingle,
}) => {
  return (
    <>
      <div className="cuisine-main">
        {dayArray.map((day, index) => (
          <table key={index} className="table" style={{ width: "100%" }}>
            <tbody>
              <tr>
                <td className="border-0">
                  <p className="col-12 p-0 restaurant-title">{day}</p>
                  <Toggle
                    name={day}
                    currentState={state[day].open}
                    type={"checkbox"}
                    spantext1={"Yes"}
                    spantext2={"No"}
                    toggleclass={"toggle d-flex align-items-center mb-2"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={state[day].open}
                  />
                </td>
                <td style={{ width: "30%" }} className="border-0">
                  {/* OPEN AND CLOSE TIME */}
                  <p className="col-12 p-0 restaurant-title">Opening Time</p>
                  <div className="opening_time_selector">
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="main_opening_time"
                        onChange={onTimeSelectHandlerSingle(day)}
                        placeholder="24 Hours Format"
                        value={state[day].main_opening_time}
                      />
                    </div>
                    {/* <select
                      name="main_opening_time"
                      onChange={onTimeSelectHandlerSingle(day)}
                      className="Selection_box"
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select> */}
                    <div className="dasheds">-</div>
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="main_closing_time"
                        onChange={onTimeSelectHandlerSingle(day)}
                        placeholder="24 Hours Format"
                        value={state[day].main_closing_time}
                      />
                    </div>
                    {/* <select
                      name="main_closing_time"
                      onChange={onTimeSelectHandlerSingle(day)}
                      className="Selection_box"
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select> */}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const YesMultipleTime = ({
  state,
  onDayOpenCloseHanlder,
  onFoodTypeOpenClose,
  onTimeSelectHandlerMultiple,
}) => {
  return (
    <>
      <div className="cuisine-main">
        {dayArray.map((day, index) => (
          <table key={index} style={{ width: "100%" }}>
            <tbody>
              <tr>
                <td style={{ width: "18%" }}>
                  <p className="col-12 p-0 restaurant-title">{day}</p>
                  <Toggle
                    name={day}
                    currentState={state[day].open}
                    type={"checkbox"}
                    spantext1={"Yes"}
                    spantext2={"No"}
                    toggleclass={"toggle d-flex align-items-center mb-2"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={state[day].open}
                  />
                </td>
                {foodTime.map((food, index) => (
                  <td
                    key={index}
                    style={{
                      width: "200px",
                      paddingLeft: "20px",
                      padding: "15px",
                    }}
                  >
                    {/* OPEN AND CLOSE TIME */}
                    <p className="col-12 p-0 restaurant-title d-flex justify-content-between">
                      {food}
                      {/* {console.log(state[day].breakfast)} */}
                      <Toggle
                        name={day}
                        currentState={state[day].breakfast.open}
                        type={"checkbox"}
                        spantext1={""}
                        spantext2={""}
                        toggleclass={"toggle d-flex align-items-center mb-2"}
                        toggleinputclass={"toggle__switch ml-3 mr-3"}
                        onChange={onFoodTypeOpenClose(day, food)}
                        defaultChecked={state[day].breakfast.open}
                      />
                    </p>
                    <div className="opening_time_selector">
                      <div className="form-group mb-0">
                        <input
                          type="text"
                          className="form-control Selection_box"
                          name="opening_time"
                          onChange={onTimeSelectHandlerMultiple(day, food)}
                          placeholder="24 Hours Format"
                          value={state[day].breakfast.opening_time}
                        />
                      </div>
                      {/* <select
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                      <div className="dasheds">-</div>
                      <div className="form-group mb-0">
                        <input
                          type="text"
                          className="form-control Selection_box"
                          name="closing_time"
                          onChange={onTimeSelectHandlerMultiple(day, food)}
                          placeholder="24 Hours Format"
                          value={state[day].breakfast.closing_time}
                        />
                      </div>
                      {/* <select
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                    </div>
                  </td>
                ))}
                {foodTimeTwo.map((food, index) => (
                  <td
                    key={index}
                    style={{
                      width: "200px",
                      paddingLeft: "20px",
                      padding: "15px",
                    }}
                  >
                    {/* OPEN AND CLOSE TIME */}
                    <p className="col-12 p-0 restaurant-title d-flex justify-content-between">
                      {food}
                      {/* {console.log(state[day].lunch)} */}
                      <Toggle
                        name={day}
                        currentState={state[day].lunch.open}
                        type={"checkbox"}
                        spantext1={""}
                        spantext2={""}
                        toggleclass={"toggle d-flex align-items-center mb-2"}
                        toggleinputclass={"toggle__switch ml-3 mr-3"}
                        onChange={onFoodTypeOpenClose(day, food)}
                        defaultChecked={state[day].lunch.open}
                      />
                    </p>
                    <div className="opening_time_selector">
                      <div className="form-group mb-0">
                        <input
                          type="text"
                          className="form-control Selection_box"
                          name="opening_time"
                          onChange={onTimeSelectHandlerMultiple(day, food)}
                          placeholder="24 Hours Format"
                          value={state[day].lunch.opening_time}
                        />
                      </div>
                      {/* <select
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                      <div className="dasheds">-</div>
                      <div className="form-group mb-0">
                        <input
                          type="text"
                          className="form-control Selection_box"
                          name="closing_time"
                          onChange={onTimeSelectHandlerMultiple(day, food)}
                          placeholder="24 Hours Format"
                          value={state[day].lunch.closing_time}
                        />
                      </div>
                      {/* <select
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                    </div>
                  </td>
                ))}
                {foodTimeThree.map((food, index) => (
                  <td
                    key={index}
                    style={{
                      width: "200px",
                      paddingLeft: "20px",
                      padding: "15px",
                    }}
                  >
                    {/* OPEN AND CLOSE TIME */}
                    <p className="col-12 p-0 restaurant-title d-flex justify-content-between">
                      {food}
                      {/* {console.log(state[day].dinner)} */}
                      <Toggle
                        name={day}
                        currentState={state[day].dinner.open}
                        type={"checkbox"}
                        spantext1={""}
                        spantext2={""}
                        toggleclass={"toggle d-flex align-items-center mb-2"}
                        toggleinputclass={"toggle__switch ml-3 mr-3"}
                        onChange={onFoodTypeOpenClose(day, food)}
                        defaultChecked={state[day].dinner.open}
                      />
                    </p>
                    <div className="opening_time_selector">
                      <div className="form-group mb-0">
                        <input
                          type="text"
                          className="form-control Selection_box"
                          name="opening_time"
                          onChange={onTimeSelectHandlerMultiple(day, food)}
                          placeholder="24 Hours Format"
                          value={state[day].dinner.opening_time}
                        />
                      </div>
                      {/* <select
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                      <div className="dasheds">-</div>
                      <div className="form-group mb-0">
                        <input
                          type="text"
                          className="form-control Selection_box"
                          name="closing_time"
                          onChange={onTimeSelectHandlerMultiple(day, food)}
                          placeholder="24 Hours Format"
                          value={state[day].dinner.closing_time}
                        />
                      </div>
                      {/* <select
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                    </div>
                  </td>
                ))}
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const SeatingArrangement = ({ state, toggleFunction, onChange }) => {
  // console.log(state);
  return (
    <React.Fragment>
      <p className="col-12 p-0 restaurant-title">
        Do you Offer Table Management
      </p>
      <SlidingComponent
        name="table_management"
        currentState={state.table_management}
        type={"checkbox"}
        spantext1={"Yes"}
        spantext2={"No"}
        toggleclass={"toggle d-flex align-items-center mb-2"}
        toggleinputclass={"toggle__switch ml-3 mr-3"}
        onChange={toggleFunction}
        defaultChecked={state.table_management === true ? true : false}
      />
      <p className="col-12 p-0 restaurant-title">Total Seating Capacity</p>
      <div style={{ width: "12%" }}>
        <InputComponent
          labeltext="Total Seating Capacity"
          inputlabelclass="input-label d-none"
          imgbox="d-none"
          name="total_seating_capacity"
          type="number"
          place="eg. 123"
          onChange={onChange}
          value={state.total_seating_capacity}
          inputclass={"map-inputfield"}
        />
      </div>
    </React.Fragment>
  );
};
