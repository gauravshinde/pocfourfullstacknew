import React, { Component } from "react";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
// import isEmpty from '../../../store/validation/is-Empty';
import CardComponent from "../../../reusableComponents/CardComponent";
import Toggle from "../../../reusableComponents/SlidingComponent";
import axios from "axios";
import isEmpty from "../../../store/validation/is-Empty";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  get_subscription_details,
  update_subscription_new,
} from "./../../../store/actions/addDetailsActions";
import IconBox from "./../../../reusableComponents/IconBoxButtonComponent";
import SuggestNew from "./../../../reusableComponents/SuggestNew";
import SlidingComponent from "../../../reusableComponents/SlidingComponent";
import { serverApi } from "../../../config/Keys";

import Alert from "react-s-alert";

// const CardContent = [
//   {
//     icon: ' ',
//     title: 'QSR',
//     des: ['Curb Side', 'Self Serve', 'Skip the Line', 'Take Away'],
//     src: 'https://chainlist.s3.amazonaws.com/dinner+(1)%402x.png'
//   }
// ];

export class SubscriptionSetup extends Component {
  constructor() {
    super();
    this.state = {
      room_number: false,
      highchair: false,
      handicap: false,
      walk_in: false,
      waitlist: false,
      reservation: false,
      curb_side: false,
      skip_line: false,
      self_serve: false,
      take_away: false,
      seating_capacity: "",
      reservation_capacity: "",
      auto_accept_pax: "",
      table_turn_around: "",
      reservation_time_slot: "",
      cut_off_time: "",
      sameDayAllowed: false,
      minimum_lead_time: "",
      maximum_days: "",
      uploadMenu: [],

      hasSetDetails: false,
    };
  }

  componentDidMount() {
    this.props.get_subscription_details();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.details);
    const SubscriptionData = { ...nextProps.details };
    // console.log(
    //   SubscriptionData.request_period,
    //   SubscriptionData.placement_period,
    //   SubscriptionData.accept_Limit,
    //   SubscriptionData.distance,
    //   SubscriptionData.uploadMenu,
    //   SubscriptionData.curb_side,
    //   SubscriptionData.self_serve,
    //   SubscriptionData.skip_line,
    //   SubscriptionData.take_away,
    //   SubscriptionData.walk_in,
    //   SubscriptionData.waitlist,
    //   SubscriptionData.reservation
    // );
    if (!isEmpty(SubscriptionData) && !nextState.hasSetDetails) {
      return {
        room_number: SubscriptionData.room_number,
        highchair: SubscriptionData.highchair,
        handicap: SubscriptionData.handicap,
        walk_in: SubscriptionData.walk_in,
        waitlist: SubscriptionData.waitlist,
        reservation: SubscriptionData.reservation,
        curb_side: SubscriptionData.curb_side,
        skip_line: SubscriptionData.skip_line,
        self_serve: SubscriptionData.self_serve,
        take_away: SubscriptionData.take_away,
        seating_capacity: SubscriptionData.seating_capacity,
        reservation_capacity: SubscriptionData.reservation_capacity,
        auto_accept_pax: SubscriptionData.auto_accept_pax,
        table_turn_around: SubscriptionData.table_turn_around,
        reservation_time_slot: SubscriptionData.reservation_time_slot,
        cut_off_time: SubscriptionData.cut_off_time,
        sameDayAllowed: SubscriptionData.sameDayAllowed,
        minimum_lead_time: SubscriptionData.minimum_lead_time,
        maximum_days: SubscriptionData.maximum_days,
        uploadMenu: SubscriptionData.uploadMenu,

        hasSetDetails: true,
      };
    }
    return null;
  }

  onClickServiceType = (data) => {
    this.setState({
      servicesType: data,
    });
    console.log(data);
  };

  pageChangeHandle = (value) => (e) => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  /**************************
   * @DESC - TOGGLE FUNCTION
   ***************************/
  toggleFunction = (e) => {
    this.setState({
      [e.target.name]: e.target.checked,
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = (e) => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post(`${serverApi}/image/upload-content-images`, data)
      .then((res) => {
        let uploadMenu = this.state.uploadMenu;
        uploadMenu.push(res.data.image_URL);
        this.setState({ uploadMenu: uploadMenu, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  /********************
   * Change plan
   *******************/
  onChangeplan = (e) => {
    e.preventDefault();
    this.setState({
      changeplan: true,
    });
  };

  onChangePlanSaveHandler = (e) => {
    e.preventDefault();
    this.setState({
      changeplan: false,
    });
  };

  onPlanSubmitHandler = (e) => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user._id,
      highchair: this.state.highchair,
      handicap: this.state.handicap,
      room_number: this.state.room_number,
      walk_in: this.state.walk_in,
      waitlist: this.state.waitlist,
      reservation: this.state.reservation,
      curb_side: this.state.curb_side,
      skip_line: this.state.skip_line,
      self_serve: this.state.self_serve,
      take_away: this.state.take_away,
      seating_capacity: this.state.seating_capacity,
      reservation_capacity: this.state.reservation_capacity,
      auto_accept_pax: this.state.auto_accept_pax,
      table_turn_around: this.state.table_turn_around,
      reservation_time_slot: this.state.reservation_time_slot,
      cut_off_time: this.state.cut_off_time,
      sameDayAllowed: this.state.sameDayAllowed,
      minimum_lead_time: this.state.minimum_lead_time,
      maximum_days: this.state.maximum_days,
      uploadMenu: this.state.uploadMenu,
    };
    //console.log(formData);
    this.props.update_subscription_new(formData);
    Alert.success("<h4>Subscription Details Successfully Updated</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 5000,
      offset: 30,
    });
  };

  renderSubscriptionDetails = () => {
    return (
      <React.Fragment>
        <div className="w-50 ml-auto mt-4 d-flex justify-content-between">
          {/* <ButtonComponent
            buttontext='Back'
            buttontype='button'
            buttonclass='btn button-main button-white'
            // onClick={this.pageChangeHandle(11)}
          /> */}
          <ButtonComponent
            buttontext="Save"
            buttontype="submit"
            buttonclass="btn button-main button-orange"
            // onClick={this.props.pageChanger(8)}
            onClick={this.onPlanSubmitHandler}
          />
        </div>
      </React.Fragment>
    );
  };

  renderActivePlan = () => {
    const {
      curb_side,
      self_serve,
      skip_line,
      take_away,
      walk_in,
      waitlist,
      reservation,
      uploadMenu,
    } = this.state;
    return (
      <React.Fragment>
        <h4>Company Setup</h4>
        <div className="row">
          <div className="col-sm-6">{this.renderSubscriptionLeft()}</div>
          <div className="col-sm-6 border-left">
            {this.renderCasualDiningPlanfield()}
          </div>
          {/* Photo Upload*/}
          <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <p className="restaurant-title">Upload Menu</p>
          </div>
          <div className="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
            <div
              className="view_chainsBor overflow-auto mt-0"
              style={{ width: "100%" }}
            >
              <div className="custom_file_upload">
                <input
                  type="file"
                  name="iconone"
                  id="filetwo"
                  onChange={this.onImageUploadHandler}
                  className="custom_input_upload"
                />
                <label
                  className="custom_input_label newChain_add"
                  htmlFor="filetwo"
                >
                  <div>
                    <i className="fa fa-plus" style={{ color: "#CCCCCC" }}></i>
                  </div>
                  <div className="add_new_text">Add New</div>
                </label>
              </div>
              <div className="newChain_addthree mx-3">
                {this.state.uploadMenu.length > 0
                  ? this.state.uploadMenu.map((image, index) => (
                      <img
                        key={index}
                        src={image}
                        className="newChain_addtwo"
                        style={{ height: "100%", width: "100%" }}
                        alt="chain "
                      />
                    ))
                  : null}
              </div>
            </div>
          </div>
          {/* <div className="col-sm-4">
            {this.state.servicesType === "QSR" ? (
              <Cards
                mainheading={"Your are subscribing to:"}
                state={this.state}
                onClickServiceType={this.onClickServiceType}
                onClickchangeplan={this.onChangeplan}
                imgsrc="https://chainlist.s3.amazonaws.com/dinner+(1)%402x.png"
                newtitle="QSR"
                description="Curb Side Self Serve Skip the Line Take Away"
                newclasses="subscription_card_container_active"
              />
            ) : this.state.servicesType === "casualDining" ? (
              <Cards
                mainheading={"Your are subscribing to:"}
                state={this.state}
                //onClickServiceType={this.onClickServiceType}
                //onClickchangeplan={this.onChangeplan}
                imgsrc="https://chainlist.s3.amazonaws.com/fast-food%402x.png"
                newtitle="Casual Dining"
                description="Walk in WaitList Curb Side Take Away Self Serve Reservation Skip the Line"
                newclasses="subscription_card_container_active"
              />
            ) : this.state.servicesType === "FSR" ? (
              <Cards
                mainheading={"Your are subscribing to:"}
                state={this.state}
                onClickServiceType={this.onClickServiceType}
                onClickchangeplan={this.onChangeplan}
                imgsrc="https://chainlist.s3.amazonaws.com/restaurant%402x.png"
                newtitle="FSR"
                description="Curb Side Self Serve Skip the Line Take Away Reservation"
                newclasses="subscription_card_container_active"
              />
            ) : null}
            {/* <div className="col-12 text-center">
              <ButtonComponent
                type="button"
                buttontext="Change Plan"
                buttonclass="btn button-main button-white mt-5"
                onClick={this.onChangeplan}
              />
            </div> *
          </div>
          <div className="col-sm-8" style={{ borderLeft: "1px solid #ccc" }}>
            {this.state.servicesType === "QSR"
              ? this.renderQSRPlanfield()
              : this.state.servicesType === "casualDining"
              ? this.renderCasualDiningPlanfield()
              : this.state.servicesType === "FSR"
              ? this.renderFSRPlanfield()
              : null}
            {this.renderSubscriptionDetails()}
          </div> */}
          {this.renderSubscriptionDetails()}
        </div>
      </React.Fragment>
    );
  };

  renderSubscriptionLeft = () => {
    const {
      highchair,
      handicap,
      room_number,
      seating_capacity,
      reservation_capacity,
      auto_accept_pax,
      table_turn_around,
      reservation_time_slot,
      cut_off_time,
      sameDayAllowed,
      minimum_lead_time,
      maximum_days,
    } = this.state;
    return (
      <React.Fragment>
        <div className="cuisine-main">
          <div className="row">
            <div className="col-6">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                High Chair Facility
              </h2>
              <SlidingComponent
                name={"highchair"}
                // value={this.state.walk_in}
                currentState={this.state.highchair}
                type={"checkbox"}
                spantext1={"Yes"}
                spantext2={"No"}
                toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={this.toggleFunction}
                defaultChecked={highchair === true ? true : false}
              />
            </div>
            <div className="col-6">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                Handicap Facility
              </h2>
              <SlidingComponent
                name={"handicap"}
                // value={this.state.walk_in}
                currentState={this.state.handicap}
                type={"checkbox"}
                spantext1={"Yes"}
                spantext2={"No"}
                toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={this.toggleFunction}
                defaultChecked={handicap === true ? true : false}
              />
            </div>
            <div className="col-12">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                Room number on seating request?
              </h2>
              <SlidingComponent
                name={"room_number"}
                // value={this.state.walk_in}
                currentState={this.state.room_number}
                type={"checkbox"}
                spantext1={"Yes"}
                spantext2={"No"}
                toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={this.toggleFunction}
                defaultChecked={room_number === true ? true : false}
              />
            </div>
            <div className="col-6">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                Seating Capacity
              </h2>
              <div className="d-flex">
                <input
                  type="number"
                  placeholder="eg. 100"
                  name="seating_capacity"
                  value={this.state.seating_capacity}
                  onChange={this.onChange}
                  className="curve_input_field"
                />
                <span
                  className="pt-3 pl-3"
                  style={{
                    fontSize: "15px",
                    color: "#ccc",
                    fontFamily: "AvenirLTStd-Black",
                  }}
                >
                  pax
                </span>
              </div>
            </div>
            <div className="col-6">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                Reservation Capacity
              </h2>
              <div className="d-flex">
                <input
                  type="number"
                  placeholder="eg. 10 %"
                  name="reservation_capacity"
                  value={this.state.reservation_capacity}
                  onChange={this.onChange}
                  className="curve_input_field"
                />
                <span
                  className="pt-3 pl-3"
                  style={{
                    fontSize: "15px",
                    color: "#ccc",
                    fontFamily: "AvenirLTStd-Black",
                  }}
                >
                  %
                </span>
              </div>
            </div>
            <div className="col-6">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                Auto accept Pax Limit
              </h2>
              <div className="d-flex">
                <input
                  type="number"
                  placeholder="eg. 10"
                  name="auto_accept_pax"
                  value={this.state.auto_accept_pax}
                  onChange={this.onChange}
                  className="curve_input_field"
                />
                <span
                  className="pt-3 pl-3"
                  style={{
                    fontSize: "15px",
                    color: "#ccc",
                    fontFamily: "AvenirLTStd-Black",
                  }}
                >
                  pax
                </span>
              </div>
            </div>
            <div className="col-6">
              <p className="heading-title-subscription-inside">
                Table turn Around Time?
              </p>
              <div className="input-container">
                <select
                  name="table_turn_around"
                  onChange={this.onChange}
                  value={this.state.table_turn_around}
                  className="subscription-custom-inputfield"
                >
                  <option value="15">15 Minutes</option>
                  <option value="30">30 Minutes</option>
                  <option value="45">45 Minutes</option>
                  <option value="60">60 Minutes</option>
                  <option value="90">90 Minutes</option>
                </select>
              </div>
            </div>
            <div className="col-6">
              <p className="heading-title-subscription-inside">
                Reservation Time Slot
              </p>
              <div className="input-container">
                <select
                  name="reservation_time_slot"
                  onChange={this.onChange}
                  value={this.state.reservation_time_slot}
                  className="subscription-custom-inputfield"
                >
                  <option value="15">15 Minutes</option>
                  <option value="30">30 Minutes</option>
                  <option value="60">60 Minutes</option>
                </select>
              </div>
            </div>
            <div className="col-6">
              <p className="heading-title-subscription-inside">
                Cut Off Time - Reservation
              </p>
              <div className="input-container">
                <select
                  name="cut_off_time"
                  onChange={this.onChange}
                  value={this.state.cut_off_time}
                  className="subscription-custom-inputfield"
                >
                  <option value="00">0 Minutes</option>
                  <option value="15">15 Minutes</option>
                  <option value="30">30 Minutes</option>
                  <option value="60">60 Minutes</option>
                </select>
              </div>
            </div>
            <div className="col-6">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                Reservation - Same Day Allowed?
              </h2>
              <SlidingComponent
                name={"sameDayAllowed"}
                // value={this.state.walk_in}
                currentState={this.state.sameDayAllowed}
                type={"checkbox"}
                spantext1={"Yes"}
                spantext2={"No"}
                toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={this.toggleFunction}
                defaultChecked={sameDayAllowed === true ? true : false}
              />
            </div>

            {this.state.sameDayAllowed === true ? (
              <div className="col-6">
                <p className="heading-title-subscription-inside">
                  Minimum Lead Time
                </p>
                <div className="input-container">
                  <select
                    name="minimum_lead_time"
                    onChange={this.onChange}
                    value={this.state.minimum_lead_time}
                    className="subscription-custom-inputfield"
                  >
                    <option value="60">01 Hrs</option>
                    <option value="120">02 Hrs</option>
                    <option value="180">03 Hrs</option>
                    <option value="240">04 Hrs</option>
                    <option value="300">05 Hrs</option>
                  </select>
                </div>
              </div>
            ) : (
              false
            )}

            <div className="col-6">
              <h2
                className="heading-title-subscription-inside"
                // style={{ paddingTop: "30px", fontSize: "20px" }}
              >
                Maximum Days ahead
              </h2>
              <div className="d-flex">
                <input
                  type="number"
                  placeholder="eg. 50"
                  name="maximum_days"
                  value={this.state.maximum_days}
                  onChange={this.onChange}
                  className="curve_input_field"
                />
                <span
                  className="pt-3 pl-3"
                  style={{
                    fontSize: "15px",
                    color: "#ccc",
                    fontFamily: "AvenirLTStd-Black",
                  }}
                >
                  Days
                </span>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  // renderQSRPlanfield = () => {
  //   const {
  //     curb_side,
  //     self_serve,
  //     skip_line,
  //     take_away,
  //     walk_in,
  //     waitlist,
  //     reservation,
  //     highchair,
  //     handicap
  //   } = this.state;
  //   return (
  //     <React.Fragment>
  //       <div className="cuisine-main">
  //         <table style={{ width: "100%", borderRadius: "0px" }}>
  //           <tbody>
  //             <tr>
  //               <td>
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Request Period
  //                 </h2>
  //                 <div className="d-flex">
  //                   <input
  //                     type="text"
  //                     placeholder="eg. 50"
  //                     name="request_period"
  //                     value={this.state.request_period}
  //                     onChange={this.onChange}
  //                     className="curve_input_field w-50"
  //                   />
  //                   <span
  //                     className="pt-3 pl-3"
  //                     style={{
  //                       fontSize: "15px",
  //                       color: "#ccc",
  //                       fontFamily: "AvenirLTStd-Black"
  //                     }}
  //                   >
  //                     Days
  //                   </span>
  //                 </div>
  //               </td>
  //               <td className="">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Placement Period
  //                 </h2>
  //                 <div className="d-flex">
  //                   <input
  //                     type="text"
  //                     placeholder="eg. 50"
  //                     name="placement_period"
  //                     value={this.state.placement_period}
  //                     onChange={this.onChange}
  //                     className="curve_input_field w-50"
  //                   />
  //                   <span
  //                     className="pt-3 pl-3"
  //                     style={{
  //                       fontSize: "15px",
  //                       color: "#ccc",
  //                       fontFamily: "AvenirLTStd-Black"
  //                     }}
  //                   >
  //                     Days
  //                   </span>
  //                 </div>
  //               </td>
  //             </tr>
  //           </tbody>
  //         </table>

  //         <h2 className="heading-title-subscription-inside" style={{ paddingTop: "30px" }}>
  //           Do you have different counters for different food items?
  //         </h2>
  //         <Toggle
  //           name="foodItem_counter"
  //           currentState={this.state.foodItem_counter}
  //           type={"checkbox"}
  //           spantext1={"Yes"}
  //           spantext2={"No"}
  //           toggleclass={"toggle d-flex align-items-center mb-2"}
  //           toggleinputclass={"toggle__switch ml-3 mr-3"}
  //           onChange={this.toggleFunction}
  //           defaultChecked={this.state.foodItem_counter === true ? true : false}
  //         />

  //         <h2 className="heading-title-subscription-inside" style={{ paddingTop: "30px" }}>
  //           Do you have a separate counter for takeaway ?
  //         </h2>
  //         <Toggle
  //           name="takeaway_counter"
  //           currentState={this.state.takeaway_counter}
  //           type={"checkbox"}
  //           spantext1={"Yes"}
  //           spantext2={"No"}
  //           toggleclass={"toggle d-flex align-items-center mb-2"}
  //           toggleinputclass={"toggle__switch ml-3 mr-3"}
  //           onChange={this.toggleFunction}
  //           defaultChecked={this.state.takeaway_counter === true ? true : false}
  //         />
  //       </div>
  //       <UploadMenu
  //         state={this.state}
  //         onImageUploadHandler={this.onImageUploadHandler}
  //       />
  //     </React.Fragment>
  //   );
  // };

  renderCasualDiningPlanfield = () => {
    const {
      curb_side,
      self_serve,
      skip_line,
      take_away,
      walk_in,
      waitlist,
      reservation,
    } = this.state;
    return (
      <React.Fragment>
        <div>
          <h2
            className="heading-title-subscription-inside"
            // style={{ paddingTop: "30px", fontSize: "20px" }}
          >
            You Can Choose To Select Or Deselect The Following Services.
          </h2>
          <table style={{ width: "100%", borderRadius: "0px" }}>
            <tbody>
              <tr>
                <td>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h2
                      className="heading-title-subscription-inside mt-2 mb-0"
                      // style={{ fontSize: "20px" }}
                    >
                      Walk in
                    </h2>
                    <SlidingComponent
                      name={"walk_in"}
                      // value={this.state.walk_in}
                      currentState={this.state.walk_in}
                      type={"checkbox"}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                      defaultChecked={walk_in === true ? true : false}
                    />
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h2
                      className="heading-title-subscription-inside mt-2 mb-0"
                      // style={{ fontSize: "20px" }}
                    >
                      WaitList
                    </h2>
                    <SlidingComponent
                      name="waitlist"
                      // value={this.state.waitlist}
                      currentState={this.state.waitlist}
                      type={"checkbox"}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                      defaultChecked={waitlist === true ? true : false}
                    />
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h2
                      className="heading-title-subscription-inside mt-2 mb-0"
                      // style={{ fontSize: "20px" }}
                    >
                      Curb Side
                    </h2>
                    <SlidingComponent
                      name="curb_side"
                      // value={this.state.allergy_information}
                      currentState={this.state.curb_side}
                      type={"checkbox"}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                      defaultChecked={curb_side === true ? true : false}
                    />
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h2
                      className="heading-title-subscription-inside mt-2 mb-0"
                      // style={{ fontSize: "20px" }}
                    >
                      Take Away
                    </h2>
                    <SlidingComponent
                      name="take_away"
                      // value={this.state.take_away}
                      currentState={this.state.take_away}
                      type={"checkbox"}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                      defaultChecked={take_away === true ? true : false}
                    />
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h2
                      className="heading-title-subscription-inside mt-2 mb-0"
                      // style={{ fontSize: "20px" }}
                    >
                      Reservation
                    </h2>
                    <SlidingComponent
                      name="reservation"
                      // value={this.state.reservation}
                      currentState={this.state.reservation}
                      type={"checkbox"}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                      defaultChecked={reservation === true ? true : false}
                    />
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h2
                      className="heading-title-subscription-inside mt-2 mb-0"
                      // style={{ fontSize: "20px" }}
                    >
                      Self Serve
                    </h2>
                    <SlidingComponent
                      name="self_serve"
                      // value={this.state.self_serve}
                      currentState={this.state.self_serve}
                      type={"checkbox"}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                      defaultChecked={self_serve === true ? true : false}
                    />
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h2
                      className="heading-title-subscription-inside mt-2 mb-0"
                      // style={{ fontSize: "20px" }}
                    >
                      Skip the Line
                    </h2>
                    <SlidingComponent
                      name="skip_line"
                      // value={this.state.skip_line}
                      currentState={this.state.skip_line}
                      type={"checkbox"}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={"toggle d-flex align-items-center mb-2 mt-2"}
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                      defaultChecked={skip_line === true ? true : false}
                    />
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  };

  // renderFSRPlanfield = () => {
  //   const {
  //     curb_side,
  //     self_serve,
  //     skip_line,
  //     take_away,
  //     walk_in,
  //     waitlist,
  //     reservation
  //   } = this.state;
  //   return (
  //     <React.Fragment>
  //       <div className="cuisine-main">
  //         <table style={{ width: "100%", borderRadius: "0px" }}>
  //           <tbody>
  //             <tr>
  //               <td className="w-50 px-3">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Auto Accept Limit
  //                 </h2>
  //                 <div className="d-flex align-items-center">
  //                   <input
  //                     name="accept_Limit"
  //                     value={this.state.accept_Limit}
  //                     onChange={this.onChange}
  //                     className="curve_input_field"
  //                     placeholder="eg. 50"
  //                   />
  //                   <span
  //                     style={{
  //                       fontSize: "15px",
  //                       paddingLeft: "10px",
  //                       fontFamily: "AvenirLTStd-Black",
  //                       color: "#ccc"
  //                     }}
  //                   >
  //                     Days
  //                   </span>
  //                 </div>
  //               </td>
  //               <td className="w-50 px-3">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                 style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Reservation Time Limit *
  //                 </h2>
  //                 <div className="d-flex align-items-center">
  //                   <input
  //                     name="reservation_Limit"
  //                     value={this.state.reservation_Limit}
  //                     onChange={this.onChange}
  //                     className="curve_input_field"
  //                     placeholder="eg. 50"
  //                   />
  //                   <span
  //                     style={{
  //                       fontSize: "15px",
  //                       paddingLeft: "10px",
  //                       fontFamily: "AvenirLTStd-Black",
  //                       color: "#ccc"
  //                     }}
  //                   >
  //                     Mins
  //                   </span>
  //                 </div>
  //               </td>
  //             </tr>
  //             <tr>
  //               <td className="w-50 px-3">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                  style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Turn Around Time?
  //                 </h2>
  //                 <div className="d-flex align-items-center">
  //                   <input
  //                     name="around_time"
  //                     value={this.state.around_time}
  //                     onChange={this.onChange}
  //                     className="curve_input_field"
  //                     placeholder="eg. 50"
  //                   />
  //                   <span
  //                     style={{
  //                       fontSize: "15px",
  //                       paddingLeft: "10px",
  //                       fontFamily: "AvenirLTStd-Black",
  //                       color: "#ccc"
  //                     }}
  //                   >
  //                     Mins
  //                   </span>
  //                 </div>
  //               </td>
  //               <td className="w-50 px-3">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                  style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   What is your total seating capacity?
  //                 </h2>
  //                 <div className="d-flex align-items-center">
  //                   <input
  //                     name="total_seating"
  //                     value={this.state.total_seating}
  //                     onChange={this.onChange}
  //                     className="curve_input_field"
  //                     placeholder="eg. 50"
  //                   />
  //                   <span
  //                     style={{
  //                       fontSize: "15px",
  //                       paddingLeft: "10px",
  //                       fontFamily: "AvenirLTStd-Black",
  //                       color: "#ccc"
  //                     }}
  //                   >
  //                     Num
  //                   </span>
  //                 </div>
  //               </td>
  //             </tr>
  //             <tr>
  //               <td className="w-50 px-3">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                  style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Placement Period
  //                 </h2>
  //                 <div className="d-flex align-items-center">
  //                   <input
  //                     name="placement_period"
  //                     value={this.state.placement_period}
  //                     onChange={this.onChange}
  //                     className="curve_input_field"
  //                     placeholder="eg. 50"
  //                   />
  //                   <span
  //                     style={{
  //                       fontSize: "15px",
  //                       paddingLeft: "10px",
  //                       fontFamily: "AvenirLTStd-Black",
  //                       color: "#ccc"
  //                     }}
  //                   >
  //                     Days
  //                   </span>
  //                 </div>
  //               </td>
  //               <td className="w-50 px-3">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                  style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Request Period
  //                 </h2>
  //                 <div className="d-flex align-items-center">
  //                   <input
  //                     name="request_period"
  //                     value={this.state.request_period}
  //                     onChange={this.onChange}
  //                     className="curve_input_field"
  //                     placeholder="eg. 50"
  //                   />
  //                   <span
  //                     style={{
  //                       fontSize: "15px",
  //                       paddingLeft: "10px",
  //                       fontFamily: "AvenirLTStd-Black",
  //                       color: "#ccc"
  //                     }}
  //                   >
  //                     Mtrs
  //                   </span>
  //                 </div>
  //               </td>
  //             </tr>
  //             <tr>
  //               <td colSpan="2">
  //                 <div className="col-12">
  //                   <h2
  //                     className="heading-title-subscription-inside"
  //                     style={{ paddingTop: "30px" }}
  //                   >
  //                     Is this facility inside a hotel?
  //                   </h2>
  //                   <Toggle
  //                     name="facility"
  //                     currentState={this.state.facility}
  //                     type={"checkbox"}
  //                     spantext1={"Yes"}
  //                     spantext2={"No"}
  //                     toggleclass={"toggle d-flex align-items-center mb-2"}
  //                     toggleinputclass={"toggle__switch ml-3 mr-3"}
  //                     onChange={this.toggleFunction}
  //                     defaultChecked={false}
  //                   />

  //                   <h2
  //                     className="heading-title-subscription-inside"
  //                     style={{ paddingTop: "30px" }}
  //                   >
  //                     Would you like to extend the request of highchair and
  //                     handicap?
  //                   </h2>
  //                   <Toggle
  //                     name="highchair_hadicap"
  //                     currentState={this.state.highchair_hadicap}
  //                     type={"checkbox"}
  //                     spantext1={"Yes"}
  //                     spantext2={"No"}
  //                     toggleclass={"toggle d-flex align-items-center mb-2"}
  //                     toggleinputclass={"toggle__switch ml-3 mr-3"}
  //                     onChange={this.toggleFunction}
  //                     defaultChecked={false}
  //                   />
  //                 </div>
  //               </td>
  //             </tr>
  //             <tr>
  //               <td colSpan="2" className="px-3">
  //                 <h2
  //                   className="heading-title-subscription-inside"
  //                  style={{ paddingTop: "30px", fontSize: "20px" }}
  //                 >
  //                   Where is your seating area?
  //                 </h2>
  //                 <div style={{ display: "flex", flexWrap: "wrap" }}>
  //                   {this.state.all_seating_area_icons.map((icon, index) => (
  //                     <IconBox
  //                       key={index}
  //                       src={icon.icon}
  //                       title={icon.title}
  //                       classsection="main-icon-button"
  //                       onClick={this.state.onSeatingAreaArraySelector(icon)}
  //                     />
  //                   ))}

  //                   <SuggestNew
  //                     title="Suggest New"
  //                     classsection="main-suggest-button"
  //                   />
  //                 </div>

  //                 {/** */}
  //                 {!isEmpty(this.state.selected_seating_area) ? (
  //                   <h2
  //                     className="heading-title-subscription-inside"
  //                     style={{ paddingTop: "30px" }}
  //                   >
  //                     Which is your Primary Type?
  //                   </h2>
  //                 ) : null}

  //                 <div style={{ display: "flex", flexWrap: "wrap" }}>
  //                   {this.state.selected_seating_area.map((icon, index) => (
  //                     <IconBox
  //                       key={index}
  //                       src={icon.icon}
  //                       title={icon.title}
  //                       classsection={
  //                         icon === this.state.primary_seating_area
  //                           ? "main-icon-button_active"
  //                           : "main-icon-button"
  //                       }
  //                       onClick={this.state.onSelectedAreaPrimarySelector(icon)}
  //                     />
  //                   ))}
  //                 </div>
  //               </td>
  //             </tr>
  //           </tbody>
  //         </table>
  //       </div>
  //       <UploadMenu
  //         state={this.state}
  //         onImageUploadHandler={this.onImageUploadHandler}
  //       />
  //     </React.Fragment>
  //   );
  // };

  // renderSelectActivePlan = () => {
  //   const {
  //     curb_side,
  //     self_serve,
  //     skip_line,
  //     take_away,
  //     walk_in,
  //     waitlist,
  //     reservation
  //   } = this.state;
  //   return (
  //     <React.Fragment>
  //       <h4>Change Plan</h4>
  //       <div className="row">
  //         <div className="col-sm-4">
  //           <Cards
  //             mainheading={"Your are subscribing to:"}
  //             state={this.state}
  //             onClickServiceType={() => this.onClickServiceType("QSR")}
  //             imgsrc="https://chainlist.s3.amazonaws.com/dinner+(1)%402x.png"
  //             newtitle="QSR"
  //             description="Curb Side Self Serve Skip the Line Take Away"
  //             newclasses={
  //               this.state.servicesType === "QSR"
  //                 ? "subscription_card_container_active"
  //                 : "subscription_card_container"
  //             }
  //           />
  //         </div>
  //         <div className="col-sm-8">
  //           <div className="row m-0">
  //             <div className="col-sm-6">
  //               <Cards
  //                 mainheading={"Available Plans:"}
  //                 state={this.state}
  //                 onClickServiceType={() =>
  //                   this.onClickServiceType("casualDining")
  //                 }
  //                 description="Walk in WaitList Curb Side Take Away Self Serve Reservation Skip the Line"
  //                 newtitle="Casual Dining"
  //                 imgsrc="https://chainlist.s3.amazonaws.com/fast-food%402x.png"
  //                 newclasses={
  //                   this.state.servicesType === "casualDining"
  //                     ? "subscription_card_container_active"
  //                     : "subscription_card_container"
  //                 }
  //               />
  //             </div>
  //             <div className="col-sm-6 pt-4">
  //               <Cards
  //                 state={this.state}
  //                 onClickServiceType={() => this.onClickServiceType("FSR")}
  //                 description="Curb Side Self Serve Skip the Line Take Away Reservation"
  //                 newtitle="FSR"
  //                 imgsrc="https://chainlist.s3.amazonaws.com/restaurant%402x.png"
  //                 newclasses={
  //                   this.state.servicesType === "FSR"
  //                     ? "subscription_card_container_active"
  //                     : "subscription_card_container"
  //                 }
  //               />
  //             </div>
  //             <div className="col-12 ">
  //               <ButtonComponent
  //                 buttontext="Save"
  //                 buttontype="submit"
  //                 buttonclass="btn button-main button-orange float-right mt-4"
  //                 // onClick={this.props.pageChanger(8)}
  //                 onClick={this.onChangePlanSaveHandler}
  //               />
  //             </div>
  //           </div>
  //         </div>
  //       </div>
  //     </React.Fragment>
  //   );
  // };

  render() {
    const {
      curb_side,
      self_serve,
      skip_line,
      take_away,
      walk_in,
      waitlist,
      reservation,
    } = this.state;
    // console.log(this.state);
    const { changeplan } = this.state;
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <form>
            {changeplan === true
              ? this.renderSelectActivePlan()
              : this.renderActivePlan()}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  details: state.details.subscription_details[0],
});

export default connect(mapStateToProps, {
  get_subscription_details,
  update_subscription_new,
})(withRouter(SubscriptionSetup));

// const Services = ({ state, onChange, toggleFunction }) => {
//   return (
//     <div className='cuisine-main'>
//       <table style={{ width: '100%', borderRadius: '0px' }}>
//         <tbody>
//           <tr>
//             <td>
//               <h2
//                 className='heading-title-subscription-inside'
//                 style={{ paddingTop: '30px', fontSize: '20px' }}
//               >
//                 Request Period
//               </h2>
//               <div className='d-flex'>
//                 <input
//                   name='request_period'
//                   value={state.request_period}
//                   onChange={onChange}
//                   className='curve_input_field w-50'
//                 />
//                 <span
//                   className='pt-3 pl-3'
//                   style={{
//                     fontSize: '15px',
//                     color: '#ccc',
//                     fontFamily: 'AvenirLTStd-Black'
//                   }}
//                 >
//                   Days
//                 </span>
//               </div>
//             </td>
//           </tr>
//           <tr>
//             <td className=''>
//               <h2
//                 className='heading-title-subscription-inside'
//                 style={{ paddingTop: '30px', fontSize: '20px' }}
//               >
//                 Placement Period
//               </h2>
//               <div className='d-flex'>
//                 <input
//                   name='placement_period'
//                   value={state.placement_period}
//                   onChange={onChange}
//                   className='curve_input_field w-50'
//                 />
//                 <span
//                   className='pt-3 pl-3'
//                   style={{
//                     fontSize: '15px',
//                     color: '#ccc',
//                     fontFamily: 'AvenirLTStd-Black'
//                   }}
//                 >
//                   Days
//                 </span>
//               </div>
//             </td>
//           </tr>
//         </tbody>
//       </table>

//       <h2 className='heading-title-subscription-inside' style={{ paddingTop: '30px' }}>
//         Do you have different counters for different food items?
//       </h2>
//       <Toggle
//         name='foodItem_counter'
//         currentState={state.foodItem_counter}
//         type={'checkbox'}
//         spantext1={'Yes'}
//         spantext2={'No'}
//         toggleclass={'toggle d-flex align-items-center mb-2'}
//         toggleinputclass={'toggle__switch ml-3 mr-3'}
//         onChange={toggleFunction}
//         defaultChecked={state.foodItem_counter === true ? true : false}
//       />

//       <h2 className='heading-title-subscription-inside' style={{ paddingTop: '30px' }}>
//         Do you have a separate counter for takeaway ?
//       </h2>
//       <Toggle
//         name='takeaway_counter'
//         currentState={state.takeaway_counter}
//         type={'checkbox'}
//         spantext1={'Yes'}
//         spantext2={'No'}
//         toggleclass={'toggle d-flex align-items-center mb-2'}
//         toggleinputclass={'toggle__switch ml-3 mr-3'}
//         onChange={toggleFunction}
//         defaultChecked={state.takeaway_counter === true ? true : false}
//       />
//     </div>
//   );
// };

const UploadMenu = ({ state, onImageUploadHandler }) => {
  return (
    <>
      <div className="cuisine-main col-12">
        <h2
          className="heading-title-subscription-inside"
          style={{ paddingTop: "30px" }}
        >
          Upload Menu
        </h2>
        <div
          className="view_chainsBor overflow-auto mb-5"
          style={{ width: "100%" }}
        >
          <div className="custom_file_upload">
            <input
              type="file"
              name="icon"
              id="file"
              onChange={onImageUploadHandler}
              className="custom_input_upload"
            />
            <label className="custom_input_label newChain_add" htmlFor="file">
              <div>
                <i className="fa fa-plus" style={{ color: "#CCCCCC" }}></i>
              </div>
              <div className="add_new_text">Add New</div>
            </label>
          </div>

          <div className="newChain_addthree mx-3">
            {!isEmpty(state.uploadMenu) && state.uploadMenu.length > 0
              ? state.uploadMenu.map((image, index) => (
                  <img
                    key={index}
                    src={image}
                    className="newChain_addtwo"
                    style={{ height: "100%", width: "100%" }}
                    alt="chain "
                  />
                ))
              : null}
            {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
          </div>
        </div>
      </div>
    </>
  );
};

// const Cards = ({
//   state,
//   onClickServiceType,
//   mainheading,
//   description,
//   newtitle,
//   imgsrc,
//   newclasses
// }) => {
//   return (
//     <div className="cuisine-main">
//       <h2 className="heading-title-subscription-inside" style={{ paddingTop: "30px" }}>
//         {mainheading}
//       </h2>

//       <div className="card_container">
//         <CardComponent
//           src={imgsrc}
//           title={newtitle}
//           des={description}
//           classes={newclasses}
//           onClick={onClickServiceType}
//         />
//         {/* {CardContent.map((item, index) => (
//           <CardComponent
//             key={index}
//             item={item}
//             src={item.src}
//             classes={
//               state === item.title
//                 ? 'main_card_container_active'
//                 : 'main_card_container'
//             }
//             onClick={onClickServiceType(item.title)}
//           />
//         ))} */}
//       </div>
//     </div>
//   );
// };
//27-12-19
