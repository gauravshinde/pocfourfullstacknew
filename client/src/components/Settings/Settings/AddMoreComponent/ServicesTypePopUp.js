import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import InputComponent from "../../../../reusableComponents/InputComponent";
import classnames from "classnames";
import TextAreaComponent from "../../../../reusableComponents/TextAreaComponent";
import ButtonComponent from "../../../../reusableComponents/ButtonComponent";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import IconBox from "../../../../reusableComponents/IconBoxButtonComponent";
import { getALLICONS } from "../../../../store/actions/iconAction";
import {
  get_restaurants_type_details,
  update_restaurant_features_popup
} from "../../../../store/actions/addDetailsActions";
import isEmpty from "./../../../../store/validation/is-Empty";
import Alert from "react-s-alert";

export class ServicesTypePopUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      all_services_type: [],
      selected_restaurant_access_type: this.props.selectedServices,
      primary_restaurant_access_type: this.props.stateData
        .primary_restaurant_access_type,
      stateData: this.props.stateData,
      stateData1: this.props.stateData1,
      errors: {}
    };
  }

  componentDidMount() {
    this.props.get_restaurants_type_details();
    this.props.getALLICONS();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.icons);
    if (
      nextProps.icons.get_restraunt_access_icons !== nextState.all_services_type
    ) {
      return {
        all_services_type: nextProps.icons.get_restraunt_access_icons
      };
    }
    return null;
  }

  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - onRestaurant Type Selector
   ****************************************/
  arrayRemove(arr, value) {
    return arr.filter(ele => {
      return ele._id !== value._id;
    });
  }

  onRestuarantTypeArraySelector = icon => e => {
    console.log(icon);
    let restaurantTypeArray = this.state.selected_restaurant_access_type;
    if (restaurantTypeArray.length === 0) {
      restaurantTypeArray.push(icon);
    } else {
      let isAlreadyPresent = restaurantTypeArray.find(ele => {
        if (ele._id === icon._id) {
          return true;
        }
        return false;
      });
      // let isAlreadyPresent = restaurantTypeArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = restaurantTypeArray.indexOf(isAlreadyPresent);
        // if (indexOf === 0 || indexOf) {
        //   restaurantTypeArray.splice(indexOf, 1);
        // }
        if (restaurantTypeArray.length === 1) {
          alert("Atleast one has to be selected");
        } else if (indexOf === 0 || indexOf) {
          restaurantTypeArray.splice(indexOf, 1);
        }
      } else {
        restaurantTypeArray.push(icon);
      }
    }
    this.setState({
      selected_restaurant_access_type: restaurantTypeArray
    });
  };

  onRestuarantAccessPrimarySelector = icon => e => {
    this.setState({
      primary_restaurant_access_type: icon
    });
  };

  onSubmit = e => {
    //console.log(this.state.stateData);
    if (this.state.primary_restaurant_access_type === undefined) {
      Alert.error("<h4>Please Select Primary Restaurant Access Type</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 30
      });
    } else {
      e.preventDefault();
      const formData = {
        vendor_id: this.props.auth.user._id,
        selected_restaurant_type: this.state.stateData1
          .selected_restaurant_type,
        primary_restaurant_type: this.state.stateData1.primary_restaurant_type,
        selected_dress_code_type: this.state.stateData1
          .selected_dress_code_type,
        primary_dress_code_type: this.state.stateData1.primary_dress_code_type,
        selected_payment_method_type: this.state.stateData1
          .selected_payment_method_type,
        primary_payment_method_type: this.state.stateData1
          .primary_payment_method_type,
        cost: this.state.stateData1.cost,
        revenue: this.state.stateData1.revenue,
        restaurant_photo: this.state.stateData1.restaurant_photo,
        restaurant_logo: this.state.stateData1.restaurant_logo,
        selected_restaurant_features_type: this.state.stateData
          .selected_restaurant_features_type,
        primary_restaurant_features_type: this.state.stateData
          .primary_restaurant_features_type,
        selected_restaurant_access_type: this.state
          .selected_restaurant_access_type,
        primary_restaurant_access_type: this.state
          .primary_restaurant_access_type,
        selected_parking_type: this.state.stateData.selected_parking_type,
        primary_selecte_parking_type: this.state.stateData
          .primary_selecte_parking_type,
        parking_description: this.state.stateData.parking_description
      };
      console.log(formData);
      this.props.update_restaurant_features_popup(formData);
      this.setState({
        modalShow: false
      });
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <button
          type="button"
          className="btn btn-danger rounded-circle d-flex align-items-center"
          style={{ height: "5vh" }}
          onClick={this.modalToggler}
        >
          <i className="fa fa-plus"></i>
        </button>
        <Modal
          show={this.state.modalShow}
          size="md"
          onHide={this.modalToggler}
          centered
        >
          <Modal.Body className="p-0">
            <div className="suggest-new-title">
              <h3>Add Service Accessibilites</h3>
            </div>
            <div className="inside-body-section">
              <div>
                <h2 className="heading-title">
                  Service Accessibilites
                  <small
                    id="emailHelp"
                    className="form-text text-muted input-help-textnew"
                  >
                    ( Select all the options through which your place is
                    accessible )
                  </small>
                </h2>
                <div style={{ display: "flex", flexWrap: "wrap" }}>
                  {!isEmpty(this.state.all_services_type) &&
                    this.state.all_services_type.map((icon, index) => (
                      <IconBox
                        key={index}
                        src={icon.icon}
                        title={icon.title}
                        classsection="main-icon-button"
                        onClick={this.onRestuarantTypeArraySelector(icon)}
                      />
                    ))}

                  {/* <SuggestNew
                    title="Suggest New"
                    classsection="main-suggest-button"
                  /> */}
                </div>

                {/** */}
                {!isEmpty(this.state.selected_restaurant_access_type) ? (
                  <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                    Which is your Primary Service Accessibilites?
                    <small
                      id="emailHelp"
                      className="form-text text-muted input-help-textnew"
                    >
                      selected service accessibilites
                    </small>
                  </h2>
                ) : null}

                <div style={{ display: "flex", flexWrap: "wrap" }}>
                  {this.state.selected_restaurant_access_type.map(
                    (icon, index) => (
                      <IconBox
                        key={index}
                        src={icon.icon}
                        title={icon.title}
                        classsection={
                          icon === this.state.primary_restaurant_access_type
                            ? "main-icon-button_active"
                            : "main-icon-button"
                        }
                        onClick={this.onRestuarantAccessPrimarySelector(icon)}
                      />
                    )
                  )}
                </div>
              </div>
              <div className="w-100 d-flex pt-3 justify-content-end">
                <ButtonComponent
                  buttontext="Cancel"
                  buttontype="button"
                  buttonclass="btn button-main button-white"
                  onClick={this.modalToggler}
                />
                <ButtonComponent
                  buttontext="Submit"
                  buttontype="submit"
                  buttonclass="btn button-main button-orange ml-3"
                  // onClick={this.pageChangeHandle(8)}
                  onClick={this.onSubmit}
                />
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  icons: state.icons,
  restaurantDetail: state.details.restaurant_details
});

export default connect(mapStateToProps, {
  getALLICONS,
  get_restaurants_type_details,
  update_restaurant_features_popup
})(withRouter(ServicesTypePopUp));

//13-03-2020
