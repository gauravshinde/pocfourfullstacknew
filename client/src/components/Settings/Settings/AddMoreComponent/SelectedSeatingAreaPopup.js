import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import InputComponent from "../../../../reusableComponents/InputComponent";
import classnames from "classnames";
import TextAreaComponent from "../../../../reusableComponents/TextAreaComponent";
import ButtonComponent from "../../../../reusableComponents/ButtonComponent";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import IconBox from "../../../../reusableComponents/IconBoxButtonComponent";
import { getALLICONS } from "../../../../store/actions/iconAction";
import {
  get_kyc_details,
  get_restauranttime,
  update_restaurant_timings_Popup
} from "../../../../store/actions/addDetailsActions";
import isEmpty from "../../../../store/validation/is-Empty";
import Alert from "react-s-alert";

export class SelectedSeatingAreaPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      all_seating_area_icons: [],
      selected_seating_area: this.props.selectedSeatingArea,
      primary_seating_area: this.props.stateData.primary_seating_area,
      stateData: this.props.stateData,
      stateDataTwo: this.props.stateDataTwo,
      errors: {}
    };
  }

  componentDidMount() {
    this.props.get_kyc_details();
    this.props.get_restauranttime();
    this.props.getALLICONS();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      nextProps.icons.get_subscription_icon !== nextState.all_seating_area_icons
    ) {
      return {
        all_seating_area_icons: nextProps.icons.get_subscription_icon
      };
    }
    return null;
  }

  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - onRestaurant Type Selector
   ****************************************/
  arrayRemove(arr, value) {
    return arr.filter(ele => {
      return ele._id !== value._id;
    });
  }

  onRestuarantTypeArraySelector = icon => e => {
    console.log(icon);
    let restaurantTypeArray = this.state.selected_seating_area;
    if (restaurantTypeArray.length === 0) {
      restaurantTypeArray.push(icon);
    } else {
      let isAlreadyPresent = restaurantTypeArray.find(ele => {
        if (ele._id === icon._id) {
          return true;
        }
        return false;
      });
      // let isAlreadyPresent = restaurantTypeArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = restaurantTypeArray.indexOf(isAlreadyPresent);
        // if (indexOf === 0 || indexOf) {
        //   restaurantTypeArray.splice(indexOf, 1);
        // }
        if (restaurantTypeArray.length === 1) {
          alert("Atleast one has to be selected");
        } else if (indexOf === 0 || indexOf) {
          restaurantTypeArray.splice(indexOf, 1);
        }
      } else {
        restaurantTypeArray.push(icon);
      }
    }
    this.setState({
      selected_seating_area: restaurantTypeArray
    });
  };

  onRestuarantSeatingAreaPrimarySelector = icon => e => {
    this.setState({
      primary_seating_area: icon
    });
  };

  onSubmit = e => {
    console.log(this.state.stateData);
    e.preventDefault();
    if (this.state.primary_seating_area === undefined) {
      Alert.error("<h4>Please Select Primary Seating Area</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 30
      });
    } else {
      const formData = {
        vendor_id: this.props.auth.user._id,
        part_of_chain: this.state.stateData.part_of_chain,
        are_you_open_24_x_7: this.state.stateData.are_you_open_24_x_7,
        multiple_opening_time: this.state.stateData.multiple_opening_time,
        table_management: this.state.stateData.table_management,

        selected_seating_area: this.state.selected_seating_area,
        primary_seating_area: this.state.primary_seating_area,

        total_seating_capacity: this.state.stateData.total_seating_capacity,
        monday: this.state.stateData.monday,
        tuesday: this.state.stateData.tuesday,
        wednesday: this.state.stateData.wednesday,
        thursday: this.state.stateData.thursday,
        friday: this.state.stateData.friday,
        saturday: this.state.stateData.saturday,
        sunday: this.state.stateData.sunday,

        account_name: this.state.stateDataTwo.account_name,
        bank_name: this.state.stateDataTwo.bank_name,
        account_number: this.state.stateDataTwo.account_number,
        branch_name: this.state.stateDataTwo.branch_name,
        GST_number: this.state.stateDataTwo.GST_number,
        IFSC_code: this.state.stateDataTwo.IFSC_code,
        PAN_number: this.state.stateDataTwo.PAN_number,
        FSSAI_code: this.state.stateDataTwo.FSSAI_code,
        imgPath: this.state.stateDataTwo.imgPath,
        currentAccount: this.state.stateDataTwo.currentAccount
      };
      //console.log(formData);
      this.props.update_restaurant_timings_Popup(formData);
      this.setState({
        modalShow: false
      });
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <button
          type="button"
          className="btn btn-danger rounded-circle d-flex align-items-center"
          style={{ height: "5vh" }}
          onClick={this.modalToggler}
        >
          <i className="fa fa-plus"></i>
        </button>
        <Modal
          show={this.state.modalShow}
          size="md"
          onHide={this.modalToggler}
          centered
        >
          <Modal.Body className="p-0">
            <div className="suggest-new-title">
              <h3>Add Seating Area</h3>
            </div>
            <div className="inside-body-section">
              <div>
                <h2 className="heading-title">
                  Seating Area
                  <small
                    id="emailHelp"
                    className="form-text text-muted input-help-textnew"
                  >
                    ( Select the type of food establishment you are, you can
                    select multiple and define one default )
                  </small>
                </h2>
                <div style={{ display: "flex", flexWrap: "wrap" }}>
                  {!isEmpty(this.state.all_seating_area_icons) &&
                    this.state.all_seating_area_icons.map((icon, index) => (
                      <IconBox
                        key={index}
                        src={icon.icon}
                        title={icon.title}
                        classsection="main-icon-button"
                        onClick={this.onRestuarantTypeArraySelector(icon)}
                      />
                    ))}

                  {/* <SuggestNew
                    title="Suggest New"
                    classsection="main-suggest-button"
                  /> */}
                </div>

                {/** */}
                {!isEmpty(this.state.selected_seating_area) ? (
                  <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                    Which is your Primary Seating Area Type?
                    <small
                      id="emailHelp"
                      className="form-text text-muted input-help-textnew"
                    >
                      selected seating area type
                    </small>
                  </h2>
                ) : null}

                <div style={{ display: "flex", flexWrap: "wrap" }}>
                  {this.state.selected_seating_area.map((icon, index) => (
                    <IconBox
                      key={index}
                      src={icon.icon}
                      title={icon.title}
                      classsection={
                        icon === this.state.primary_seating_area
                          ? "main-icon-button_active"
                          : "main-icon-button"
                      }
                      onClick={this.onRestuarantSeatingAreaPrimarySelector(
                        icon
                      )}
                    />
                  ))}
                </div>
              </div>
              <div className="w-100 d-flex pt-3 justify-content-end">
                <ButtonComponent
                  buttontext="Cancel"
                  buttontype="button"
                  buttonclass="btn button-main button-white"
                  onClick={this.modalToggler}
                />
                <ButtonComponent
                  buttontext="Submit"
                  buttontype="submit"
                  buttonclass="btn button-main button-orange ml-3"
                  onClick={this.onSubmit}
                />
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  icons: state.icons
});

export default connect(mapStateToProps, {
  getALLICONS,
  get_kyc_details,
  get_restauranttime,
  update_restaurant_timings_Popup
})(withRouter(SelectedSeatingAreaPopup));

//13-03-2020
