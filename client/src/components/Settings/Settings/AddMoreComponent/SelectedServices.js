import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import ButtonComponent from "../../../../reusableComponents/ButtonComponent";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import IconBox from "../../../../reusableComponents/IconBoxButtonComponent";
import { getALLICONS } from "../../../../store/actions/iconAction";
import {
  get_restaurants_type_details,
  update_service_details_new_popup,
  get_restaurant_features
} from "../../../../store/actions/addDetailsActions";
import isEmpty from "../../../../store/validation/is-Empty";
import Alert from "react-s-alert";

export class SelectedServices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      all_facility: [],
      selected_services_type: this.props.selectedServices,
      primary_service_type: this.props.stateData.primary_service_type,
      stateData: this.props.stateData,
      stateData1: this.props.stateData1,
      stateData2: this.props.stateData2,
      errors: {}
    };
  }

  componentDidMount() {
    this.props.get_restaurants_type_details();
    this.props.get_restaurant_features();
    this.props.getALLICONS();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.icons);
    if (nextProps.icons.get_services_icons !== nextState.all_facility) {
      return {
        all_facility: nextProps.icons.get_services_icons
      };
    }
    return null;
  }

  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - onRestaurant Type Selector
   ****************************************/
  arrayRemove(arr, value) {
    return arr.filter(ele => {
      return ele._id !== value._id;
    });
  }

  onRestuarantTypeArraySelector = icon => e => {
    // console.log(icon);
    let restaurantTypeArray = this.state.selected_services_type;
    if (restaurantTypeArray.length === 0) {
      restaurantTypeArray.push(icon);
    } else {
      let isAlreadyPresent = restaurantTypeArray.find(ele => {
        if (ele._id === icon._id) {
          return true;
        }
        return false;
      });
      // let isAlreadyPresent = restaurantTypeArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = restaurantTypeArray.indexOf(isAlreadyPresent);
        // if (indexOf === 0 || indexOf) {
        //   restaurantTypeArray.splice(indexOf, 1);
        // }
        if (restaurantTypeArray.length === 1) {
          alert("Atleast one has to be selected");
        } else if (indexOf === 0 || indexOf) {
          restaurantTypeArray.splice(indexOf, 1);
        }
      } else {
        restaurantTypeArray.push(icon);
      }
    }
    this.setState({
      selected_services_type: restaurantTypeArray
    });
  };

  onRestuarantFacilityPrimarySelector = icon => e => {
    this.setState({
      primary_service_type: icon
    });
  };

  onSubmit = e => {
    //console.log(this.state.stateData);
    e.preventDefault();
    if (this.state.primary_service_type === undefined) {
      Alert.error("<h4>Please Select Primary Service Type</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 30
      });
    } else {
      const formData = {
        vendor_id: this.props.auth.user._id,
        selected_faculty_type: this.state.stateData2.selected_faculty_type,
        selected_services_type: this.state.selected_services_type,
        primary_service_type: this.state.primary_service_type
      };
      //console.log(formData);
      this.props.update_service_details_new_popup(formData);
      this.setState({
        modalShow: false
      });
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <button
          type="button"
          className="btn btn-danger rounded-circle d-flex align-items-center"
          style={{ height: "5vh" }}
          onClick={this.modalToggler}
        >
          <i className="fa fa-plus"></i>
        </button>
        <Modal
          show={this.state.modalShow}
          size="md"
          onHide={this.modalToggler}
          centered
        >
          <Modal.Body className="p-0">
            <div className="suggest-new-title">
              <h3>Add Facility</h3>
            </div>
            <div className="inside-body-section">
              <div>
                <h2 className="heading-title">
                  Facility
                  <small
                    id="emailHelp"
                    className="form-text text-muted input-help-textnew"
                  >
                    ( Select all the options you provide )
                  </small>
                </h2>
                <div style={{ display: "flex", flexWrap: "wrap" }}>
                  {!isEmpty(this.state.all_facility) &&
                    this.state.all_facility.map((icon, index) => (
                      <IconBox
                        key={index}
                        src={icon.icon}
                        title={icon.title}
                        classsection="main-icon-button"
                        onClick={this.onRestuarantTypeArraySelector(icon)}
                      />
                    ))}

                  {/* <SuggestNew
                    title="Suggest New"
                    classsection="main-suggest-button"
                  /> */}
                </div>

                {/** */}
                {!isEmpty(this.state.selected_services_type) ? (
                  <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                    Which is your Primary Facility premises?
                    <small
                      id="emailHelp"
                      className="form-text text-muted input-help-textnew"
                    >
                      selected facility premises
                    </small>
                  </h2>
                ) : null}

                <div style={{ display: "flex", flexWrap: "wrap" }}>
                  {this.state.selected_services_type.map((icon, index) => (
                    <IconBox
                      key={index}
                      src={icon.icon}
                      title={icon.title}
                      classsection={
                        icon === this.state.primary_service_type
                          ? "main-icon-button_active"
                          : "main-icon-button"
                      }
                      onClick={this.onRestuarantFacilityPrimarySelector(icon)}
                    />
                  ))}
                </div>
              </div>
              <div className="w-100 d-flex pt-3 justify-content-end">
                <ButtonComponent
                  buttontext="Cancel"
                  buttontype="button"
                  buttonclass="btn button-main button-white"
                  onClick={this.modalToggler}
                />
                <ButtonComponent
                  buttontext="Submit"
                  buttontype="submit"
                  buttonclass="btn button-main button-orange ml-3"
                  onClick={this.onSubmit}
                />
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  icons: state.icons,
  restaurantDetail: state.details.restaurant_details,
  restaurantFeatures: state.details.restaurant_features
});

export default connect(mapStateToProps, {
  getALLICONS,
  get_restaurants_type_details,
  update_service_details_new_popup,
  get_restaurant_features
})(withRouter(SelectedServices));

//13-03-2020
