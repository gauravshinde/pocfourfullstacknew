import React, { Component } from "react";
import InputComponent from "./../../../reusableComponents/InputComponent";
import classnames from "classnames";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import TextAreaComponent from "./../../../reusableComponents/TextAreaComponent";
import isEmpty from "../../../store/validation/is-Empty";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  get_restaurant_details,
  get_poc_details,
  update_restaurant_details,
  delete_poc_contact_details,
} from "../../../store/actions/addDetailsActions";

import { RenderAddedContacts } from "../../ParentComment/PersonOfContact/MainPersonOfContact";
import EditMapFile from "./EditMapFile";

import Alert from "react-s-alert";

export class StoreSetting extends Component {
  constructor() {
    super();
    this.state = {
      place_id: "",
      lat: "",
      lng: "",
      restaurant_name: "",
      restaurant_description: "",
      restaurant_area: "",
      restaurant_city: "",
      restaurant_mobile_number: "",
      restaurant_email: "",
      restaurant_address: "",
      errors: {},
      hasSetDetails: false,
      contacts_array: [],
      disabled: false,
    };
  }

  componentDidMount() {
    this.props.get_restaurant_details();
    this.props.get_poc_details();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.details.mapDetails);
    if (!isEmpty(nextProps.details.mapDetails) && !nextState.hasSetDetails) {
      return {
        restaurant_name: nextProps.details.mapDetails.restaurant_name,
        restaurant_description:
          nextProps.details.mapDetails.restaurant_description,
        restaurant_area: nextProps.details.mapDetails.restaurant_area,
        restaurant_city: nextProps.details.mapDetails.restaurant_city,
        restaurant_mobile_number:
          nextProps.details.mapDetails.restaurant_mobile_number,
        restaurant_email: nextProps.details.mapDetails.restaurant_email,
        restaurant_address: nextProps.details.mapDetails.restaurant_address,
        // markerPosition: nextProps.markerPosition,
        lat: nextProps.details.mapDetails.lat,
        lng: nextProps.details.mapDetails.lon,
        hasSetDetails: true,
      };
    }

    if (
      !isEmpty(nextProps.details.poc_list) &&
      nextState.contacts_array !== nextProps.details.poc_list
    ) {
      return {
        contacts_array: nextProps.details.poc_list,
      };
    }

    if (
      !isEmpty(nextProps.allLang) &&
      nextProps.allLang !== nextState.allLang
    ) {
      return {
        markerPosition: nextProps.allLang,
      };
    }

    return null;
  }

  storeSubmitHandler = (e) => {
    e.preventDefault();
    // console.log(this.state);
  };

  onClickDisabled = (e) => {
    this.setState({
      disabled: true,
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    if (e.target.name === "restaurant_email") {
      this.setState({
        [e.target.name]: e.target.value,
      });
    } else if (e.target.name === "restaurant_description") {
      let caps = e.target.value;
      caps = caps.charAt(0).toUpperCase() + caps.slice(1);
      let dataSet = caps;
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: dataSet,
      });
    } else {
      this.setState({
        [e.target.name]: e.target.value,
      });
    }
    //  else {
    //   let data = e.target.value.split(" ");
    //   let newarray1 = [];
    //   for (let x = 0; x < data.length; x++) {
    //     newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
    //   }
    //   let newData = newarray1.join(" ");
    //   this.setState({
    //     // [e.target.name]: e.target.value
    //     [e.target.name]: newData,
    //   });
    // }
  };

  onSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state);
    let formData = {
      vendor_id: this.props.auth.user._id,
      lat: this.state.lat,
      lon: this.state.lng,
      restaurant_name: this.state.restaurant_name,
      restaurant_email: this.state.restaurant_email,
      restaurant_area: this.state.restaurant_area,
      restaurant_city: this.state.restaurant_city,
      restaurant_mobile_number: this.state.restaurant_mobile_number,
      restaurant_address: this.state.restaurant_address,
      restaurant_description: this.state.restaurant_description,
    };
    // console.log(formData);
    this.props.update_restaurant_details(formData);
    Alert.success("<h4>Map Details Successfully Updated</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 5000,
      offset: 30,
    });
  };

  /******************************
   * ONCLICK DELETE HANDLER
   ******************************/
  onClickContactDelete = (id) => (e) => {
    // console.log("Restaurant ID", this.props.auth.user._id);
    // console.log("Person ID", id);
    let deleteData = {
      p_id: id,
      restaurant_id: this.props.auth.user._id,
    };
    // console.log(deleteData);
    this.props.delete_poc_contact_details(deleteData);
  };

  renderMapSetup = () => {
    const { errors } = this.state;
    const { disabled } = this.state;

    const { lat, lng } = this.state;
    // console.log(lat, lng);
    // console.log(markerPosition);
    localStorage.setItem("latValue", lat);
    localStorage.setItem("lngValue", lng);
    // let lat = 18.514245;
    // let lng = 73.86554799999999;

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-8">
            <InputComponent
              labeltext="Restaurant Name"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="restaurant_name"
              type="text"
              place="eg. McDonalds"
              onChange={this.onChange}
              value={this.state.restaurant_name}
              error={errors.restaurant_name}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.restaurant_name,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-4">
            <InputComponent
              labeltext="Enquiry Email"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="restaurant_email"
              type="email"
              place="eg. McDonalds"
              onChange={this.onChange}
              value={this.state.restaurant_email}
              error={errors.restaurant_email}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.restaurant_email,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-4">
            <InputComponent
              labeltext="Area"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="restaurant_area"
              type="text"
              place="eg. Koregaon Park"
              onChange={this.onChange}
              value={this.state.restaurant_area}
              error={errors.restaurant_area}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.restaurant_area,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-4">
            <InputComponent
              labeltext="City"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="restaurant_city"
              type="text"
              place="eg. Pune"
              onChange={this.onChange}
              value={this.state.restaurant_city}
              error={errors.restaurant_city}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.restaurant_city,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-4">
            <InputComponent
              labeltext="Phone No./Landline"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="restaurant_mobile_number"
              type="number"
              place="eg. 9898989898"
              onChange={this.onChange}
              value={this.state.restaurant_mobile_number}
              error={errors.restaurant_mobile_number}
              inputclass={
                disabled === false
                  ? "disable-input-button-css"
                  : classnames("map-inputfield", {
                      invalid: errors.restaurant_mobile_number,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <TextAreaComponent
              labeltext={"Address"}
              inputlabelclass={"input-label"}
              name={"restaurant_address"}
              type={"text"}
              onChange={this.onChange}
              value={this.state.restaurant_address}
              place={"eg. Riverdale"}
              error={errors.restaurant_address}
              Textareaclass={
                disabled === false
                  ? "disable-textarea-custom-css"
                  : classnames("form-control textarea-custom", {
                      invalid: errors.restaurant_address,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
          <div className="col-6">
            <TextAreaComponent
              labeltext={"Restaurant Description"}
              inputlabelclass={"input-label"}
              name={"restaurant_description"}
              type={"text"}
              onChange={this.onChange}
              value={this.state.restaurant_description}
              place={"eg. A bright Atmosphere"}
              error={errors.restaurant_description}
              Textareaclass={
                disabled === false
                  ? "disable-textarea-custom-css"
                  : classnames("form-control textarea-custom", {
                      invalid: errors.restaurant_description,
                    })
              }
              disabled={disabled === true ? false : true}
            />
          </div>
        </div>
        <div className="row">
          <p className="col-12 drop-pin-css">Drop a pin to your address</p>
          {/* <iframe
            className="col-12"
            title="map"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3784.0380526504996!2d73.89602171436819!3d18.481935475179547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c02aa3a7e783%3A0x4f9e547980873653!2sMyrl%20Tech%20Web%20Development%20Company!5e0!3m2!1sen!2sin!4v1566625734283!5m2!1sen!2sin"
          ></iframe> */}
          <EditMapFile
            google={this.props.google}
            center={{ lat: 18.514245, lng: 73.86554799999999 }}
            height="36.5625VW"
            zoom={15}
          />
        </div>

        {/* Contact Person Section */}

        <div className="row">
          <h3 className="col-12 Cntact-person-css mt-3">Contact Person</h3>
        </div>
        <div className="p-0">
          {!isEmpty(this.state.contacts_array) ? (
            <RenderAddedContacts
              contacts_array={this.state.contacts_array}
              onClickContactDelete={this.onClickContactDelete}
              deletebutton={true}
            />
          ) : null}
        </div>

        {/* Button Section */}
        {/* <hr className='hr-global py-3 ' /> */}
        <div className="row">
          <div className="col-4 ml-auto mb-4 d-flex justify-content-between">
            {/* <ButtonComponent
              buttontext='Back'
              buttontype='button'
              buttonclass='btn button-main button-white'
            /> */}
            <ButtonComponent
              buttontext="Save"
              buttontype="submit"
              buttonclass="btn button-main button-orange"
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log(this.state.markerPosition);

    return (
      <React.Fragment>
        <h4 className="col-12 mb-3">
          Map Setup
          <img
            src={require("../../../assets/images/dashboard/edit.svg")}
            alt="edit"
            className="img-fluid float-right"
            onClick={this.onClickDisabled}
            style={{ width: "2%", height: "2%" }}
          />
        </h4>
        <form className="col-12" onSubmit={this.onSubmit}>
          {this.renderMapSetup()}
        </form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  details: state.details,
  markerPosition: state.details.mapDetails,
});

export default connect(mapStateToProps, {
  get_restaurant_details,
  get_poc_details,
  update_restaurant_details,
  delete_poc_contact_details,
})(withRouter(StoreSetting));

//07-04-2020 18:07pm
