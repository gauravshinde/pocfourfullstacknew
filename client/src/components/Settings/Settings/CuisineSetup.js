import React, { Component } from "react";
// import IconBoxButtonComponent from './../../../reusableComponents/IconBoxButtonComponent';
import AddMoreComponent from "./../../../reusableComponents/AddMoreComponent";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import SlidingComponent from "../../../reusableComponents/SlidingComponent";
import isEmpty from "../../../store/validation/is-Empty";
import IconBox from "../../../reusableComponents/IconBoxButtonComponent";
import CusineTypePopup from "./AddMoreComponent/CusineTypePopup.js";
import CusineTypePopupTwo from "./AddMoreComponent/CusineTypePopupTwo.js";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  get_cusine_one,
  get_cusine_two,
  update_cuisine_one,
  update_cuisine_two,
  update_restaurant_cuisine_features_new,
  update_restaurant_cuisine_features_newone
} from "../../../store/actions/addDetailsActions";

import Alert from "react-s-alert";

export class CuisineSetup extends Component {
  constructor() {
    super();
    this.state = {
      allCusineOne: [],
      allCusineTwo: [],
      primary_food_category_icons: {},
      selected_food_items_type: [],
      primary_food_item_type: {},

      allergy_information: false,
      serve_liquor: false,
      nutri_info: false,

      selected_cusines_types: [],
      primary_selected_icons: {},
      errors: {},
      hasSetDetails: false,
      iconData: [],
      iconDataNew: []
    };
  }

  componentDidMount() {
    this.props.get_cusine_one();
    this.props.get_cusine_two();
    this.setState({
      iconData: this.props.details.cuisine_two.primary_selected_icons,
      iconDataNew: this.props.details.cuisine_one.primary_food_item_type
    });
    // console.log(this.props.details.cuisine_one.primary_food_item_type);
    // console.log(this.props.details.cuisine_two.primary_selected_icons);
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.details.cuisine_two);
    // console.log(nextProps.details.cuisine_one);
    if (
      !isEmpty(nextProps.details.cuisine_one) &&
      !isEmpty(nextProps.details.cuisine_two) &&
      !nextState.hasSetDetails
    ) {
      return {
        allCusineOne: nextProps.details.cuisine_one,
        allCusineTwo: nextProps.details.cuisine_two,
        primary_food_category_icons:
          nextProps.details.cuisine_one.primary_food_category_icons,
        selected_cusines_types:
          nextProps.details.cuisine_two.selected_cusines_types,
        primary_selected_icons:
          isEmpty(nextProps.details.cuisine_two.primary_selected_icons) ||
          nextProps.details.cuisine_two.primary_selected_icons.title,
        selected_food_items_type:
          nextProps.details.cuisine_one.selected_food_items_type,
        primary_food_item_type:
          isEmpty(nextProps.details.cuisine_one.primary_food_item_type) ||
          nextProps.details.cuisine_one.primary_food_item_type.title,

        allergy_information: nextProps.details.cuisine_one.allergy_information,
        serve_liquor: nextProps.details.cuisine_one.serve_liquor,
        nutri_info: nextProps.details.cuisine_one.nutri_info,
        hasSetDetails: true
      };
    }
    return null;
  }

  primary_selected_icons = iconNew => {
    this.setState({
      primary_selected_icons: iconNew.title,
      iconData: iconNew
    });
    console.log(iconNew);
  };

  primary_food_item_type = iconTitleOne => {
    this.setState({
      primary_food_item_type: iconTitleOne.title,
      iconDataNew: iconTitleOne
    });
    console.log(iconTitleOne);
  };

  /**************************
   * @DESC - TOGGLE FUNCTION
   ***************************/
  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    // console.log(this.state);
    let formData = this.props.update_restaurant_cuisine_features_new({
      vendor_id: this.props.auth.user._id,
      primary_food_category_icons: this.state.primary_food_category_icons,
      selected_food_items_type: this.state.selected_food_items_type,
      primary_food_item_type: this.state.iconDataNew,
      allergy_information: this.state.allergy_information,
      serve_liquor: this.state.serve_liquor,
      nutri_info: this.state.nutri_info
    });
    let newData = this.props.update_restaurant_cuisine_features_newone({
      vendor_id: this.props.auth.user._id,
      selected_cusines_types: this.state.selected_cusines_types,
      primary_selected_icons: this.state.iconData
    });
    Alert.success("<h4>Cuisine Details Successfully Updated</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 5000,
      offset: 30
    });
    // console.log(formData);
    // console.log(newData);
    // this.props.update_restaurant_cuisine_features_new(formData);
    //this.props.update_restaurant_cuisine_features_newone(newData);
  };

  renderCuisineSetting = () => {
    // const { errors } = this.state;
    return (
      <React.Fragment>
        <div className="pl-3 pr-3">
          {/* What category food do you provide? */}

          <div className="row">
            <p className="restaurant-title col-12 p-0">
              What category food do you provide?
            </p>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              <IconBox
                src={this.state.primary_food_category_icons.icon}
                title={this.state.primary_food_category_icons.title}
                classsection={
                  this.state.primary_food_category_icons
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
              />
            </div>
          </div>

          {/* Do you have allergy information on your dishes? **/}

          <div className="row">
            <p className="col-12 p-0 restaurant-title">
              Do you have allergy information on your dishes?
            </p>
            <SlidingComponent
              name="allergy_information"
              // value={this.state.allergy_information}
              currentState={this.state.allergy_information}
              type={"checkbox"}
              spantext1={"Yes"}
              spantext2={"No"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={this.toggleFunction}
              defaultChecked={
                this.state.allergy_information === true ? true : false
              }
            />
          </div>

          {/* Do you serve liquor? */}

          <div className="row">
            <p className="col-12 p-0 restaurant-title">Do you serve liquor?</p>
            <SlidingComponent
              name="serve_liquor"
              // value={this.state.serve_liquor}
              currentState={this.state.serve_liquor}
              type={"checkbox"}
              spantext1={"Yes"}
              spantext2={"No"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={this.toggleFunction}
              defaultChecked={this.state.serve_liquor === true ? true : false}
            />
          </div>

          {/* Do you have nutrition information on your dishes? */}

          <div className="row">
            <p className="col-12 p-0 restaurant-title">
              Do you have nutrition information on your dishes?
            </p>
            <SlidingComponent
              name="nutri_info"
              // value={this.state.serve_liquor}
              currentState={this.state.nutri_info}
              type={"checkbox"}
              spantext1={"Yes"}
              spantext2={"No"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={this.toggleFunction}
              defaultChecked={this.state.nutri_info === true ? true : false}
            />
          </div>

          {/* Selected Cuisines */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="restaurant-title p-0">Selected Cuisines</p>
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                <CusineTypePopup
                  cusineTypeOne={this.state.selected_food_items_type}
                  stateData={this.state.allCusineOne}
                  stateDataTwo={this.state.allCusineTwo}
                />
              </div>
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_food_items_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  classsection={
                    icon.title === this.state.primary_food_item_type
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_food_item_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="restaurant-title p-0">Selected Cuisines</p>
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                <CusineTypePopupTwo
                  cusineTypeTwo={this.state.selected_cusines_types}
                  stateData={this.state.allCusineOne}
                  stateDataTwo={this.state.allCusineTwo}
                />
              </div>
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_cusines_types.map((iconNew, index) => (
                <IconBox
                  key={index}
                  src={iconNew.icon}
                  title={iconNew.title}
                  classsection={
                    iconNew.title === this.state.primary_selected_icons
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_selected_icons(iconNew)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* Selected Cuisines */}

          {/* Button Section */}
          <div className="row">
            <div className="col-4 ml-auto mt-4 d-flex justify-content-between">
              {/* <ButtonComponent
                buttontext='Back'
                buttontype='button'
                buttonclass='btn button-main button-white'
              /> */}
              <ButtonComponent
                buttontext="Save"
                buttontype="button"
                buttonclass="btn button-main button-orange"
                onClick={this.onSubmit}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log(this.state);
    return (
      <React.Fragment>
        <h4 className="col-12 mb-3">Cuisine Setup</h4>
        <form className="col-12">{this.renderCuisineSetting()}</form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  details: state.details
});

export default connect(mapStateToProps, {
  get_cusine_one,
  get_cusine_two,
  update_cuisine_one,
  update_cuisine_two,
  update_restaurant_cuisine_features_new,
  update_restaurant_cuisine_features_newone
})(withRouter(CuisineSetup));

//01-04-2020
