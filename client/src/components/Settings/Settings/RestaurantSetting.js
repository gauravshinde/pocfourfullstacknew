import React, { Component } from "react";
// import IconBoxButtonComponent from '../../../reusableComponents/IconBoxButtonComponent';
import TextAreaComponent from "../../../reusableComponents/TextAreaComponent";
import classnames from "classnames";
import AddMoreComponent from "../../../reusableComponents/AddMoreComponent";
import InputComponent from "./../../../reusableComponents/InputComponent";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import IconBox from "../../../reusableComponents/IconBoxButtonComponent";
import SelectedRestaurantTypePopup from "./AddMoreComponent/SelectedRestaurantTypePopup";
import SelectDressCodeType from "./AddMoreComponent/SelectDressCodeType";
import SelectPaymentMethodPopUp from "./AddMoreComponent/SelectPaymentMethodPopUp";
import SelectFacilityTypePopup from "./AddMoreComponent/SelectFacilityTypePopup";
import ParkingFeatureTypePopUp from "./AddMoreComponent/ParkingFeatureTypePopUp";
import ServicesTypePopUp from "./AddMoreComponent/ServicesTypePopUp";
import SelectedServices from "./AddMoreComponent/SelectedServices";
import FacilityServices from "./AddMoreComponent/FacilityServices";
import isEmpty from "../../../store/validation/is-Empty";
import axios from "axios";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  get_restaurants_type_details,
  get_restaurant_features,
  get_services_details,
  update_restaurant_setting,
  update_restaurant_features,
  update_restaurant_details_new,
  update_restaurant_features_new,
  update_service_details_new,
} from "../../../store/actions/addDetailsActions";
import { serverApi } from "../../../config/Keys";
import Alert from "react-s-alert";

export class RestaurantSetting extends Component {
  constructor() {
    super();
    this.state = {
      allRestaurantDetails: [],
      allRestaurantFeatures: [],
      allRestaurantServices: [],
      cost: "",
      revenue: "",
      errors: {},
      selected_restaurant_type: [],
      primary_restaurant_type: {},
      selected_dress_code_type: [],
      primary_dress_code_type: {},
      selected_payment_method_type: [],
      primary_payment_method_type: {},

      selected_faculty_type: {},
      selected_services_type: [],
      primary_service_type: {},

      selected_restaurant_features_type: [],
      primary_restaurant_features_type: {},
      selected_restaurant_access_type: [],
      primary_restaurant_access_type: {},
      selected_parking_type: [],
      primary_selecte_parking_type: {},

      parking_description: "",
      hasSetDetails: false,
      restaurant_photo: [],
      restaurant_logo: "",

      iconDataOne: [],
      iconDataTwo: [],
      iconDataThree: [],
      iconDataFour: [],
      iconDataFive: [],
      iconDataSix: [],
      iconDataSeven: [],
    };
  }

  componentDidMount() {
    this.props.get_restaurant_features();
    this.props.get_restaurants_type_details();
    this.props.get_services_details();
    this.setState({
      iconDataOne: this.props.details.restaurant_details
        .primary_restaurant_type,
      iconDataTwo: this.props.details.restaurant_details
        .primary_dress_code_type,
      iconDataThree: this.props.details.restaurant_details
        .primary_payment_method_type,
      iconDataFour: this.props.details.restaurant_features
        .primary_restaurant_features_type,
      iconDataFive: this.props.details.restaurant_features
        .primary_restaurant_access_type,
      iconDataSix: this.props.details.restaurant_features
        .primary_selecte_parking_type,
      iconDataSeven: this.props.details.service_details.primary_service_type,
    });
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.details);
    if (
      !isEmpty(nextProps.details.restaurant_details) &&
      !isEmpty(nextProps.details.service_details) &&
      !isEmpty(nextProps.details.restaurant_features) &&
      !nextState.hasSetDetails
    ) {
      return {
        allRestaurantDetails:
          isEmpty(nextProps.details.restaurant_details) ||
          nextProps.details.restaurant_details,
        allRestaurantFeatures:
          isEmpty(nextProps.details.restaurant_features) ||
          nextProps.details.restaurant_features,
        allRestaurantServices:
          isEmpty(nextProps.details.service_details) ||
          nextProps.details.service_details,
        selected_restaurant_type:
          nextProps.details.restaurant_details.selected_restaurant_type,
        primary_restaurant_type:
          isEmpty(
            nextProps.details.restaurant_details.primary_restaurant_type.title
          ) ||
          nextProps.details.restaurant_details.primary_restaurant_type.title,

        cost: nextProps.details.restaurant_details.cost,
        revenue: nextProps.details.restaurant_details.revenue,

        restaurant_photo: nextProps.details.restaurant_details.restaurant_photo,

        selected_dress_code_type:
          nextProps.details.restaurant_details.selected_dress_code_type,
        primary_dress_code_type:
          isEmpty(
            nextProps.details.restaurant_details.primary_dress_code_type.title
          ) ||
          nextProps.details.restaurant_details.primary_dress_code_type.title,

        selected_payment_method_type:
          nextProps.details.restaurant_details.selected_payment_method_type,
        primary_payment_method_type:
          isEmpty(
            nextProps.details.restaurant_details.primary_payment_method_type
          ) ||
          nextProps.details.restaurant_details.primary_payment_method_type
            .title,

        selected_faculty_type:
          nextProps.details.service_details.selected_faculty_type,
        selected_services_type:
          nextProps.details.service_details.selected_services_type,
        primary_service_type:
          isEmpty(nextProps.details.service_details.primary_service_type) ||
          nextProps.details.service_details.primary_service_type.title,

        selected_restaurant_features_type:
          nextProps.details.restaurant_features
            .selected_restaurant_features_type,
        primary_restaurant_features_type:
          isEmpty(
            nextProps.details.restaurant_features
              .primary_restaurant_features_type
          ) ||
          nextProps.details.restaurant_features.primary_restaurant_features_type
            .title,
        selected_restaurant_access_type:
          nextProps.details.restaurant_features.selected_restaurant_access_type,
        primary_restaurant_access_type:
          isEmpty(
            nextProps.details.restaurant_features.primary_restaurant_access_type
          ) ||
          nextProps.details.restaurant_features.primary_restaurant_access_type
            .title,
        selected_parking_type:
          nextProps.details.restaurant_features.selected_parking_type,
        primary_selecte_parking_type:
          isEmpty(
            nextProps.details.restaurant_features.primary_selecte_parking_type
          ) ||
          nextProps.details.restaurant_features.primary_selecte_parking_type
            .title,
        parking_description:
          nextProps.details.restaurant_features.parking_description,
        restaurant_logo: nextProps.details.restaurant_details.restaurant_logo,
        hasSetDetails: true,
      };
    }
    return null;
  }

  primary_restaurant_type = (iconTitle) => {
    this.setState({
      primary_restaurant_type: iconTitle.title,
      iconDataOne: iconTitle,
    });
    console.log(iconTitle);
  };

  primary_dress_code_type = (iconTitle) => {
    this.setState({
      primary_dress_code_type: iconTitle.title,
      iconDataTwo: iconTitle,
    });
    console.log(iconTitle);
  };

  primary_payment_method_type = (iconTitle) => {
    this.setState({
      primary_payment_method_type: iconTitle.title,
      iconDataThree: iconTitle,
    });
    console.log(iconTitle);
  };

  primary_service_type = (iconTitle) => {
    this.setState({
      primary_service_type: iconTitle.title,
      iconDataSeven: iconTitle,
    });
    console.log(iconTitle);
  };

  primary_restaurant_features_type = (iconTitle) => {
    this.setState({
      primary_restaurant_features_type: iconTitle.title,
      iconDataFour: iconTitle,
    });
    console.log(iconTitle);
  };

  primary_restaurant_access_type = (iconTitle) => {
    this.setState({
      primary_restaurant_access_type: iconTitle.title,
      iconDataFive: iconTitle,
    });
    console.log(iconTitle);
  };
  primary_selecte_parking_type = (iconTitle) => {
    this.setState({
      primary_selecte_parking_type: iconTitle.title,
      iconDataSix: iconTitle,
    });
    console.log(iconTitle);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    let caps = e.target.value;
    caps = caps.charAt(0).toUpperCase() + caps.slice(1);
    let dataSet = caps;
    this.setState({
      // [e.target.name]: e.target.value
      [e.target.name]: dataSet,
    });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = (e) => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post(`${serverApi}/image/upload-content-images`, data)
      .then((res) => {
        let restaurant_photo = this.state.restaurant_photo;
        restaurant_photo.push(res.data.image_URL);
        this.setState({ restaurant_photo: restaurant_photo, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  onSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state);
    let formData = {
      vendor_id: this.props.auth.user._id,
      selected_restaurant_type: this.state.selected_restaurant_type,
      primary_restaurant_type: this.state.iconDataOne,
      selected_dress_code_type: this.state.selected_dress_code_type,
      primary_dress_code_type: this.state.iconDataTwo,
      selected_payment_method_type: this.state.selected_payment_method_type,
      primary_payment_method_type: this.state.iconDataThree,
      selected_faculty_type: this.state.selected_faculty_type,
      selected_services_type: this.state.selected_services_type,
      primary_service_type: this.state.iconDataSeven,
      cost: this.state.cost,
      revenue: this.state.revenue,
      restaurant_photo: this.state.restaurant_photo,
      restaurant_logo: this.state.restaurant_logo,
      selected_restaurant_features_type: this.state
        .selected_restaurant_features_type,
      primary_restaurant_features_type: this.state.iconDataFour,
      selected_restaurant_access_type: this.state
        .selected_restaurant_access_type,
      primary_restaurant_access_type: this.state.iconDataFive,
      selected_parking_type: this.state.selected_parking_type,
      primary_selecte_parking_type: this.state.iconDataSix,
      parking_description: this.state.parking_description,
    };
    // console.log(formData);
    this.props.update_restaurant_details_new(formData);
    this.props.update_service_details_new(formData);
    this.props.update_restaurant_features_new(formData);

    Alert.success("<h4>Restaurant Details Successfully Updated</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 3000,
      offset: 30,
    });
  };

  renderRestaurantSetting = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <div className="pl-3 pr-3">
          {/* Selected Restaurent Type */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="restaurant-title p-0">Selected Restaurant Type</p>
              <SelectedRestaurantTypePopup
                preselecetedData={this.state.selected_restaurant_type}
                stateData={this.state.allRestaurantDetails}
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {/* {console.log(this.state.selected_restaurant_type)} */}
              {this.state.selected_restaurant_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  classsection={
                    icon.title === this.state.primary_restaurant_type
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_restaurant_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          <div className="row">
            <div className="col-6 p-0">
              <p className="restaurant-title">Approx. Cost for Two?</p>
              <InputComponent
                inputlabelclass="d-none"
                imgbox="img-box custom-input mt-2"
                imgsrc={require("../../../assets/images/maindetails/rupey.png")}
                imgclass="img-fluid img"
                name="cost"
                id="cost"
                type="number"
                place="Eg. Rs 1500"
                onChange={this.onChange}
                value={this.state.cost}
                error={errors.cost}
                inputclass={classnames("input-field custom-inputtwo mt-2", {
                  invalid: errors.cost,
                })}
              />
            </div>
            <div className="col-6 p-0">
              <p className="restaurant-title">Revenue (Yearly)</p>
              <div className="input-container">
                <div className="img-box custom-input mt-2">
                  <img
                    src={require("../../../assets/images/maindetails/rupey.png")}
                    alt="rupay"
                    className="img-fluid img"
                  />
                </div>
                <select
                  name="revenue"
                  onChange={this.onChange}
                  error={errors.revenue}
                  value={this.state.revenue}
                  className={classnames("input-field custom-inputtwo mt-2", {
                    invalid: errors.revenue,
                  })}
                >
                  <option value="0-1 Lacs">0-1 Lacs</option>
                  <option value="1-5 Lacs">1-5 Lacs</option>
                  <option value="5-10 Lacs">5-10 Lacs</option>
                  <option value="10 Lacs and above">10 Lacs and above</option>
                </select>
              </div>
            </div>
          </div>

          {/* Selected Dress Code */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="p-0 restaurant-title">Selected Dress Code</p>
              <SelectDressCodeType
                selectedDressCode={this.state.selected_dress_code_type}
                stateData={this.state.allRestaurantDetails}
                title={"Add Dress Code"}
                classsection="main-suggest-button"
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_dress_code_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  classsection={
                    icon.title === this.state.primary_dress_code_type
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_dress_code_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* Restaurant Photos */}

          {/* Selected Payment Method */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className=" p-0 restaurant-title">Selected Payment Method</p>
              <SelectPaymentMethodPopUp
                selectedPayment={this.state.selected_payment_method_type}
                stateData={this.state.allRestaurantDetails}
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_payment_method_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  classsection={
                    icon.title === this.state.primary_payment_method_type
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_payment_method_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* Photo Upload*/}

          <div className="row">
            <div className="col-3 p-0">
              <p className="restaurant-title">Restaurant Photo</p>
            </div>
            <div className="col-7 p-0">
              <div
                className="view_chainsBor overflow-auto"
                style={{ width: "100%" }}
              >
                <div className="custom_file_upload">
                  <input
                    type="file"
                    name="iconone"
                    id="filetwo"
                    onChange={this.onImageUploadHandler}
                    className="custom_input_upload"
                  />
                  <label
                    className="custom_input_label newChain_add"
                    htmlFor="filetwo"
                  >
                    <div>
                      <i
                        className="fa fa-plus"
                        style={{ color: "#CCCCCC" }}
                      ></i>
                    </div>
                    <div className="add_new_text">Add New</div>
                  </label>
                </div>
                <div className="newChain_addthree mx-3">
                  {this.state.restaurant_photo.length > 0
                    ? this.state.restaurant_photo.map((image, index) => (
                        <img
                          key={index}
                          src={image}
                          className="newChain_addtwo"
                          style={{ height: "100%", width: "100%" }}
                          alt="chain "
                        />
                      ))
                    : null}
                </div>
              </div>
            </div>
          </div>

          {/* Facility is part of following premises:*/}

          <div className="row">
            <h4 className="mb-3 mt-3">Restaurant Features</h4>
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="p-0 restaurant-title">
                Facility is part of following premises:
              </p>
              <FacilityServices
                selectedFacility={this.state.selected_faculty_type}
                stateData={this.state.allRestaurantFeatures}
                stateData1={this.state.allRestaurantDetails}
                stateData2={this.state.allRestaurantServices}
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_faculty_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  // classsection={
                  //   icon.title === this.state.primary_restaurant_features_type
                  //     ? "main-icon-button_active"
                  //     : "main-icon-button"
                  // }
                  classsection={"main-icon-button"}
                  //onClick={() => this.primary_restaurant_features_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* Selected Services */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="p-0 restaurant-title">Selected Services</p>
              <SelectedServices
                selectedServices={this.state.selected_services_type}
                stateData={this.state.allRestaurantFeatures}
                stateData1={this.state.allRestaurantDetails}
                stateData2={this.state.allRestaurantServices}
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_services_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  classsection={
                    icon.title === this.state.primary_service_type
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_service_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* Selected Features */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="p-0 restaurant-title">Selected Features</p>
              <SelectFacilityTypePopup
                selectedFacility={this.state.selected_restaurant_features_type}
                stateData={this.state.allRestaurantFeatures}
                stateData1={this.state.allRestaurantDetails}
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_restaurant_features_type.map(
                (icon, index) => (
                  <IconBox
                    key={index}
                    src={icon.icon}
                    title={icon.title}
                    classsection={
                      icon.title === this.state.primary_restaurant_features_type
                        ? "main-icon-button_active"
                        : "main-icon-button"
                    }
                    onClick={() => this.primary_restaurant_features_type(icon)}
                  />
                )
              )}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* Selected Accessibilities */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="p-0 restaurant-title">Selected Accessibilities</p>
              <ServicesTypePopUp
                selectedServices={this.state.selected_restaurant_access_type}
                stateData={this.state.allRestaurantFeatures}
                stateData1={this.state.allRestaurantDetails}
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_restaurant_access_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  classsection={
                    icon.title === this.state.primary_restaurant_access_type
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_restaurant_access_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* primary service for parking */}

          <div className="row">
            <div className="d-flex w-100 justify-content-between p-0">
              <p className="p-0 restaurant-title">
                Which is your primary service for parking?
              </p>
              <ParkingFeatureTypePopUp
                selectedParking={this.state.selected_parking_type}
                stateData={this.state.allRestaurantFeatures}
                stateData1={this.state.allRestaurantDetails}
              />
            </div>
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {this.state.selected_parking_type.map((icon, index) => (
                <IconBox
                  key={index}
                  src={icon.icon}
                  title={icon.title}
                  classsection={
                    icon.title === this.state.primary_selecte_parking_type
                      ? "main-icon-button_active"
                      : "main-icon-button"
                  }
                  onClick={() => this.primary_selecte_parking_type(icon)}
                />
              ))}
            </div>
            <AddMoreComponent
              title={"Add More"}
              classsection="main-suggest-button"
            />
          </div>

          {/* Selected Accessibilites */}

          {/* Description For Parking And Accessibility */}

          <div className="row">
            <p className="col-12 p-0 restaurant-title">
              Description For Parking And Accessibility
            </p>
            <div className="col-12 pl-0 mt-2">
              <TextAreaComponent
                inputlabelclass={"d-none"}
                name={"parking_description"}
                type={"text"}
                onChange={this.onChange}
                value={this.state.parking_description}
                place={"eg. A bright Atmosphere"}
                error={errors.restaurant_description}
                Textareaclass={classnames("form-control textarea-restaurant", {
                  invalid: errors.restaurant_description,
                })}
              />
            </div>
          </div>

          {/* Button Section */}
          <div className="row">
            <div className="col-4 ml-auto mb-4 d-flex justify-content-between">
              {/* <ButtonComponent
                buttontext='Back'
                buttontype='button'
                buttonclass='btn button-main button-white'
              /> */}
              <ButtonComponent
                buttontext="Save"
                buttontype="button"
                buttonclass="btn button-main button-orange"
                onClick={this.onSubmit}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        {/* {console.log(this.state.selected_restaurant_type)} */}
        <h4 className="col-12 mb-3">Restaurant Setup</h4>
        <form className="col-12">{this.renderRestaurantSetting()}</form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  details: state.details,
});

export default connect(mapStateToProps, {
  get_restaurants_type_details,
  get_restaurant_features,
  get_services_details,
  update_restaurant_setting,
  update_restaurant_features,
  update_restaurant_details_new,
  update_restaurant_features_new,
  update_service_details_new,
})(withRouter(RestaurantSetting));

//change data
//27-02-2020
