/*******************************
 * @DESC PAGE NOT FOUND
 ******************************/
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export class PageNotFound extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="mainPageNotFoundBody">
          <div className="mainPageNotFoundWidth">
            <img
              src={require("../../assets/images/pagenotfound/illustration.png")}
              className="img-fluid pageNotFoundImage"
              alt="Page Not Found"
              title="Page Not Found"
            />
          </div>
          <div className="mainPageNotFoundWidth">
            <div className="pageNotFoundOops">
              <h5>Oops</h5>
            </div>
            <div className="d-flex justify-content-between position-relative">
              <img
                src={require("../../assets/images/pagenotfound/shape.png")}
                className="img-fluid shape-image"
                alt="shape"
              />
              <p>
                Page not found! Click{" "}
                <NavLink
                  style={{ color: "#FF5757", textDecoration: "none" }}
                  to="/"
                >
                  Here
                </NavLink>{" "}
                to return Homepage
              </p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default PageNotFound;
