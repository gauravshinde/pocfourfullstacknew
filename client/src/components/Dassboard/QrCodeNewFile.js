import React from "react";
import { QRCode } from "react-qr-svg";

var QRCodeNew = require("qrcode.react");

const QrCodeNewFile = ({ dataValue, printableId, downloadQR }) => {
  return (
    <React.Fragment>
      <div id={printableId}>
        <QRCode
          bgColor="#FFFFFF"
          fgColor="#000000"
          level="H"
          style={{ width: 256 }}
          value={dataValue}
        />
        <div className="download-img-qrcode">
          <QRCodeNew
            id="123456"
            value={dataValue}
            size={256}
            level={"H"}
            includeMargin={true}
          />
        </div>
      </div>
      <div className="col-12 mt-2">
        <a onClick={downloadQR}>Download QR</a>
      </div>
    </React.Fragment>
  );
};

export default QrCodeNewFile;
//first