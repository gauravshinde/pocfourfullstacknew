import React, { Component } from "react";
import DashBoardLink from "./DashBoardLink";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "../../store/validation/is-Empty";

import { getRestaurantOpenClosed } from "../../store/actions/addDetailsActions";

export class NavbarDashboard extends Component {
  constructor() {
    super();
    this.state = {
      openNavbar: false
    };
  }

  componentDidMount() {
    if (!isEmpty(this.props.getRestaurantOpenClosed)) {
      this.props.getRestaurantOpenClosed();
    }
  }

  /****************************
   * @DESC OPEN HAMBURGUR CLICK
   ****************************/
  onClickOpennavbar = () => {
    // console.log("hii");
    this.setState({
      openNavbar: !this.state.openNavbar
    });
  };

  render() {
    return (
      <React.Fragment>
        <div
          className="container-fluid main-dashboard-back"
          // style={{ zIndex: 123123213 }}
        >
          <div className="d-flex">
            <i
              className="fa fa-bars hamburgur-class"
              aria-hidden="true"
              onClick={this.onClickOpennavbar}
            ></i>
            {this.state.openNavbar ? (
              <DashBoardLink toggler={this.onClickOpennavbar} />
            ) : null}
            <h4>
              <img
                src={require("../../assets/images/logoamealio.png")}
                alt="Logo Icon"
                className="img-fluid amelio_logo_imageClass"
              />
            </h4>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  restaurantTime: state.details.restaurantOpenClose
  //qrcode: state.qrCode
});

export default connect(mapStateToProps, {
  getRestaurantOpenClosed
})(withRouter(NavbarDashboard));

//02-14-2020
