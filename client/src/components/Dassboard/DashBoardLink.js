import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import SlidingComponent from "./../../reusableComponents/SlidingComponent";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { logOutUser } from "../../store/actions/authActions";
import Alert from "react-s-alert";
import ButtonComponent from "./../../reusableComponents/ButtonComponent";
import Modal from "react-bootstrap/Modal";
import QrCodeNewFile from "./QrCodeNewFile";
import isEmpty from "../../store/validation/is-Empty";

import {
  getRestaurantOpenClosed,
  updateRestaurantOpenClosed,
} from "../../store/actions/addDetailsActions";

class DashBoardLink extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
      restaurantOpen: false,
      restaurantStatus: {},
    };
  }

  componentDidMount() {
    if (!isEmpty(this.props.getRestaurantOpenClosed)) {
      this.props.getRestaurantOpenClosed();
    }
    this.setState({
      restaurantOpen: this.props.restaurantTime,
    });
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.restaurantTime);
    if (!isEmpty(nextProps.restaurantTime !== nextState.restaurantStatus)) {
      return {
        restaurantStatus: nextProps.restaurantTime,
      };
    }
  }

  printOrder = () => {
    const printableElements = document.getElementById("printme").innerHTML;
    const orderHtml =
      "<html><head><title></title></head><body>" +
      printableElements +
      "</body></html>";
    const oldPage = document.body.innerHTML;
    document.body.innerHTML = orderHtml;
    window.print();
    window.location.reload();
    document.body.innerHTML = oldPage;
  };

  downloadQR = () => {
    const canvas = document.getElementById("123456");
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = "QR_Code.png";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };

  /**************************
   * @DESC - MODAL TOOGLER
   ***************************/
  modalToggler = (e) => {
    this.setState({
      modalShow: !this.state.modalShow,
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  /********************************************
   * @DESC -ON CHANGE TOGGLE STATUS CHANGE
   ********************************************/
  onSelectToggleStatusChange = (value) => (e) => {
    // e.preventDefault();
    const formData = {
      id: value._id,
      restaurant_current_status:
        value.restaurant_current_status === false ? true : false,
    };
    //console.log(formData);
    this.props.updateRestaurantOpenClosed(formData);
  };

  render() {
    const { restaurantStatus } = this.state;
    const newData = { ...restaurantStatus };
    const newLatestData = newData[0];
    console.log(newLatestData);
    return (
      <React.Fragment>
        <div className="dashboard-menu-slide">
          <div className="row">
            <div className="col-12 p-0">
              <h4 className="float-left">Menu</h4>
              <img
                src={require("../../assets/images/dashboard/Close-Menu.svg")}
                alt="close"
                className="times-fontawesome float-right"
                onClick={this.props.toggler}
              />
            </div>
            <div className="col-12 restaurant-open">
              <h5>
                Restaurant
                {!isEmpty(newLatestData.restaurant_current_status) === true
                  ? "Open"
                  : "Closed"}
              </h5>
              <SlidingComponent
                name="restaurantOpen"
                false
                currentState={this.state.restaurantOpen}
                false
                type={"checkbox"}
                //spantext1={"Active"}
                //spantext2={"Inactive"}
                toggleclass={"toggle d-flex align-items-center mb-2"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={this.toggleFunction}
                slidingonClick={this.onSelectToggleStatusChange(newLatestData)}
                defaultChecked={newLatestData.restaurant_current_status}
              />
              {/* <SlidingComponent
                type={"checkbox"}
                toggleclass={"toggle"}
                toggleinputclass={"toggle__switch"}
              /> */}
            </div>
            <div className="col-12 menu-nav">
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/dashboard"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Dasshboard.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Dashboard
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/ordering" //change to ordering
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Ordering.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Ordering
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/vendor-seating"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Seating.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Seating
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/vendor-menu"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Menu.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Menu
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/team"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Team.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Team
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/issues"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Issues.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Issues
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/reports"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Reports.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Reports
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to=""
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Misc..svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Misc.
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/reviews&ratings"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Ratings_and_Reviews.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Ratings and Reviews
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/offers"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Offers.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Offers
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/events"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Events.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Events
                </NavLink>
              </div>
              <div className="total-navlink-div">
                {/* <div className="empty-div" /> */}
                <NavLink
                  exact
                  to="/mysettings"
                  className="ml-0 link-custom-hover"
                  activeClassName="selected"
                >
                  <img
                    src={require("../../assets/images/Menu_Icon/Settings.svg")}
                    alt="Dashboard Icon"
                    className="empty-div"
                  />
                  Settings
                </NavLink>
              </div>
            </div>
            <div className="logout-div">
              <button
                type="button"
                onClick={() =>
                  this.props.logOutUser(
                    Alert.success("<h4>Logout Successfully</h4>", {
                      position: "top-right",
                      effect: "bouncyflip",
                      beep: true,
                      html: true,
                      timeout: 5000,
                      offset: 35,
                    })
                  )
                }
                className="logout-text bg-transparent border-0 pl-0"
              >
                Logout
              </button>
              <div>
                <img
                  src={require("../../assets/images/dashboard/qr-code.svg")}
                  alt="QR Code"
                  className="img-fluid"
                  onClick={this.modalToggler}
                />
                <Modal
                  show={this.state.modalShow}
                  size="md"
                  onHide={this.modalToggler}
                  centered
                >
                  {/* <Modal.Body className='p-0'> */}
                  <div className="suggest-new-title">
                    <h3>QR Code</h3>
                  </div>
                  <div className="inside-body-section text-center">
                    {/* <img
                      src={require("../../assets/images/dashboard/qr-code.svg")}
                      alt="Qr Code"
                      className="img-fluid bg-dark"
                    /> */}
                    <div>
                      <QrCodeNewFile
                        printableId="printme"
                        dataValue={this.props.auth.user._id}
                        downloadQR={this.downloadQR}
                      />
                    </div>
                    <hr />
                    <p className="Qr-code-paragraph">
                      A QR code (short for "quick response" code) is a type of
                      barcode that contains a matrix of dots. It can be scanned
                      using a QR scanner or a smartphone with built-in camera.
                      Once scanned, software on the device converts the dots
                      within the code into numbers or a string of characters.
                      For example, scanning a QR code with your phone might open
                      a URL in your phone's web browser.
                    </p>
                    {/** */}
                    <div className="w-75 mt-3 ml-auto d-flex justify-content-between">
                      <ButtonComponent
                        buttontext="Close"
                        buttontype="button"
                        buttonclass="btn button-main button-white"
                        onClick={this.modalToggler}
                      />
                      <ButtonComponent
                        buttontext="Print"
                        buttontype="button"
                        buttonclass="btn button-main button-orange ml-3"
                        // onClick={this.pageChangeHandle(8)}
                        onClick={() => this.printOrder()}
                      />
                    </div>
                  </div>
                  {/* </Modal.Body> */}
                </Modal>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  details: state.details.mapDetails,
  restaurantTime: state.details.restaurantOpenClose,
  //qrcode: state.qrCode
});

export default connect(mapStateToProps, {
  logOutUser,
  getRestaurantOpenClosed,
  updateRestaurantOpenClosed,
})(withRouter(DashBoardLink));

//27-02-2020
