import React, { Component } from "react";
import BeautyStars from "beauty-stars";
import InputComponent from "./../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../store/validation/is-Empty";
import { get_vendor_approved_reviews } from "../../../store/actions/ReviewsAndRatingAction/ReviewsAndRatingAction";

export class ApprovedReviews extends Component {
  constructor() {
    super();
    this.state = {
      value: 3,
      allApprovedRatingReviews: [],
      search: ""
    };
  }

  componentDidMount() {
    if (this.props.get_vendor_approved_reviews()) {
      this.props.get_vendor_approved_reviews();
    }
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.allApprovedReviews);
    if (
      !isEmpty(nextProps.allApprovedReviews) !==
      nextState.allApprovedRatingReviews
    ) {
      return {
        allApprovedRatingReviews: nextProps.allApprovedReviews
      };
    }
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  renderApprovedReviews = () => {
    const { allApprovedRatingReviews } = this.state;
    let VendorApprovedReviewsFilteredData = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      VendorApprovedReviewsFilteredData = allApprovedRatingReviews.filter(
        getall => {
          if (search.test(getall.restaurant_name)) {
            return getall;
          }
        }
      );
      // console.log(VendorApprovedReviewsFilteredData);
    } else {
      VendorApprovedReviewsFilteredData = allApprovedRatingReviews;
    }
    if (!isEmpty(VendorApprovedReviewsFilteredData)) {
      return VendorApprovedReviewsFilteredData.reverse().map((data, index) => (
        <React.Fragment key={index}>
          <div className="col-12 main-offers-inside">
            <div className="row">
              <div className="col-sm-3 d-flex justify-content-start align-items-center">
                <div className="pending-review-image-main">
                  <img
                    src={data.restaurant_logo}
                    alt="product"
                    className="img-fluid review-img"
                  />
                </div>
                <div className="w-75">
                  <h3>{data.first_name + " " + data.last_name}</h3>
                </div>
              </div>
              <div className="col-sm-9 d-flex align-items-center">
                <div className="col-8 main-offers-sectiontwo-divone main-offers-sectiontwo-divone-hight">
                  <span>{data.review}</span>
                </div>
                <div className="col-4 main-offers-sectiontwo-divtwo main-offers-sectiontwo-divone-hight">
                  <div
                    className="d-flex align-items-center"
                    style={{ height: "8vh" }}
                  >
                    <div className="w-75">
                      <BeautyStars
                        value={data.rating}
                        size="20px"
                        inactiveColor="rgba(255, 213, 0, 0.25)"
                        activeColor="#FFD500"
                        editable={false}
                        onChange={value => this.setState({ value })}
                      />
                    </div>
                    {/* <div className="d-flex w-25">
                        <img
                          src={require("../../../assets/images/dashboard/reviews/done.png")}
                          alt="edit"
                          className="img-fluid mr-0"
                          // onClick={() => this.onOpenEditModal(data)}
                        />
                        <img
                          src={require("../../../assets/images/dashboard/reviews/remove.png")}
                          alt="delete"
                          className="img-fluid mr-0 ml-2"
                          //onClick={() => this.onClickDeleteOffer(data)}
                        />
                      </div> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      ));
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };
  render() {
    return (
      <React.Fragment>
        <div className="main-pending-review-search">
          <div className="main-offers-sectionone ">
            <InputComponent
              img={require("../../../assets/images/search.png")}
              labelClass={"mylabel"}
              imageClass={"offers-image-class"}
              onChange={this.onSearchChange}
              value={this.state.search}
              place={"search"}
              inputclass={"form-control main-offers-input"}
            />
          </div>
        </div>
        <div className="container-fluid all-page-container-padding main-offers-sectiontwo">
          {this.renderApprovedReviews()}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  allApprovedReviews: state.allRatingReviews.get_approved_reviews_details
});

export default connect(mapStateToProps, {
  get_vendor_approved_reviews
})(withRouter(ApprovedReviews));
