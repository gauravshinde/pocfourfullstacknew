import React, { Component } from "react";
import BeautyStars from "beauty-stars";
import InputComponent from "./../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../store/validation/is-Empty";
import {
  get_vendor_pending_reviews,
  update_vendor_pending_status,
  delete_vendor_pending_status
} from "../../../store/actions/ReviewsAndRatingAction/ReviewsAndRatingAction";

export class PendingReviews extends Component {
  constructor() {
    super();
    this.state = {
      value: 2,
      allPendingRatingReviews: [],
      search: ""
    };
  }

  componentDidMount() {
    if (this.props.get_vendor_pending_reviews()) {
      this.props.get_vendor_pending_reviews();
    }
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.allPendingReviews);
    if (
      !isEmpty(nextProps.allPendingReviews) !==
      nextState.allPendingRatingReviews
    ) {
      return {
        allPendingRatingReviews: nextProps.allPendingReviews
      };
    }
  }

  /**************************
   * @DESC - ON SEARCH CHANGE
   ***************************/
  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  /**************************
   * @DESC - DELETE HANDLER
   ***************************/
  onClickDeleteOffer = data => {
    console.log(data);
    let deleteData = {
      id: data._id
    };
    // console.log(deleteData);
    this.props.delete_vendor_pending_status(deleteData);
  };

  /**************************
   * @DESC - PENDING STATUS UPDATE HANDLER
   ***************************/
  onClickEditOffer = data => {
    console.log(data);
    let updateReview = {
      id: data._id,
      vendor_id: data.vendor_id,
      review_status: "Approved"
    };
    // console.log(updateReview);
    this.props.update_vendor_pending_status(updateReview);
  };

  renderPendingReviews = () => {
    const { allPendingRatingReviews } = this.state;
    console.log(allPendingRatingReviews);
    let VendorPendingReviewsfiltereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      VendorPendingReviewsfiltereddata = allPendingRatingReviews.filter(
        getall => {
          if (search.test(getall.restaurant_name)) {
            return getall;
          }
        }
      );
      // console.log(VendorPendingReviewsfiltereddata);
    } else {
      VendorPendingReviewsfiltereddata = allPendingRatingReviews;
    }
    if (!isEmpty(VendorPendingReviewsfiltereddata)) {
      return VendorPendingReviewsfiltereddata.reverse().map((data, index) => (
        <React.Fragment key={index}>
          <div className="col-12 main-offers-inside">
            <div className="row">
              <div className="col-sm-3 d-flex justify-content-start align-items-center">
                <div className="pending-review-image-main">
                  <img
                    src={data.restaurant_logo}
                    alt="product"
                    className="img-fluid review-img"
                  />
                </div>
                <div className="w-75">
                  <h3>{data.first_name + " " + data.last_name}</h3>
                </div>
              </div>
              <div className="col-sm-9 d-flex align-items-center">
                <div className="col-8 main-offers-sectiontwo-divone main-offers-sectiontwo-divone-hight">
                  <span>{data.review}</span>
                </div>
                <div className="col-4 main-offers-sectiontwo-divtwo main-offers-sectiontwo-divone-hight">
                  <div
                    className="d-flex align-items-center"
                    style={{ height: "8vh" }}
                  >
                    <div className="w-75">
                      <BeautyStars
                        value={data.rating}
                        size="20px"
                        inactiveColor="rgba(255, 213, 0, 0.25)"
                        activeColor="#FFD500"
                        editable={false}
                        onChange={value => this.setState({ value })}
                      />
                    </div>
                    <div className="d-flex w-25">
                      <img
                        src={require("../../../assets/images/dashboard/reviews/done.png")}
                        alt="edit"
                        className="img-fluid mr-0"
                        onClick={() => this.onClickEditOffer(data)}
                      />
                      <img
                        src={require("../../../assets/images/dashboard/reviews/remove.png")}
                        alt="delete"
                        className="img-fluid mr-0 ml-2"
                        onClick={() => this.onClickDeleteOffer(data)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      ));
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="main-pending-review-search">
          <div className="main-offers-sectionone ">
            <InputComponent
              img={require("../../../assets/images/search.png")}
              labelClass={"mylabel"}
              imageClass={"offers-image-class"}
              onChange={this.onSearchChange}
              value={this.state.search}
              place={"search"}
              inputclass={"form-control main-offers-input"}
            />
          </div>
        </div>
        <div className="container-fluid all-page-container-padding main-offers-sectiontwo">
          {this.renderPendingReviews()}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  allPendingReviews: state.allRatingReviews.get_pending_reviews_details
});

export default connect(mapStateToProps, {
  get_vendor_pending_reviews,
  update_vendor_pending_status,
  delete_vendor_pending_status
})(withRouter(PendingReviews));

//01-04-2020
