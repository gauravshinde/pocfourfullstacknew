import React, { Component } from "react";
import NavbarDashboard from "../Dassboard/NavbarDashboard";

//import InputComponent from "../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";
import ApprovedReviews from "./InsideReviewspage/ApprovedReviews";
import PendingReviews from "./InsideReviewspage/PendingReviews";

export class MainRatingandReviews extends Component {
  constructor() {
    super();
    this.state = {
      selectedPAge: 1
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectedPAge: value
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid p-0">
          <div className="row">
            <NavbarDashboard />
          </div>
          <div className="row">
            <div className="container-fluid all-page-container-padding table-container">
              <div className="col-12 vendor-search mt-3 px-0 d-flex justify-content-between align-items-center">
                <div className="d-flex justify-content-start align-items-center">
                  <h4 className={"vendor"}>All Reviews</h4>
                </div>
                {/* <div className="d-flex justify-content-between main-offers-sectionone">
                  <InputComponent
                    img={require("../../assets/images/search.png")}
                    labelClass={"mylabel"}
                    imageClass={"offers-image-class"}
                    onChange={this.onSearchChange}
                    //value={this.state.search}
                    place={"search"}
                    inputclass={"form-control main-offers-input"}
                  />
                </div> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="container-fluid all-page-container-padding d-flex">
              <tr
                onClick={this.onPageChange(1)}
                className={
                  this.state.selectedPAge === 1
                    ? "menu_container-review_active"
                    : "menu_container-review"
                }
              >
                <td className="circle_label">Pending Reviews</td>
              </tr>
              <tr
                onClick={this.onPageChange(2)}
                className={
                  this.state.selectedPAge === 2
                    ? "menu_container-review_active"
                    : "menu_container-review"
                }
              >
                <td className="circle_label">Approved Reviews</td>
              </tr>
            </div>
          </div>
          <div className="row">
            {this.state.selectedPAge === 1 ? <PendingReviews /> : ""}
            {this.state.selectedPAge === 2 ? <ApprovedReviews /> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MainRatingandReviews;
