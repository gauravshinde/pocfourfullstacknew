import React, { Component } from "react";
import NavbarDashboard from "../Dassboard/NavbarDashboard";
import MainSeatingReport from "../Reports/SeatingReports/MainSeatingReport";
import MainOrderingReport from "../Reports/OrderingReports/MainOrderingReport";

export class MainReportsComponents extends Component {
  constructor() {
    super();
    this.state = {
      activeLink: "seating"
    };
  }

  /**********************************
        Link Activation Handler
   **********************************/

  onClickHandler = linkText => {
    this.setState({
      activeLink: linkText
    });
  };

  /**********************************
        Render Seating Report 
   **********************************/
  renderSeatingReport = () => {
    return (
      <>
        <MainSeatingReport />
      </>
    );
  };

  /**********************************
        Render Ordering Report 
   **********************************/
  renderOrderingReport = () => {
    return (
      <>
        <MainOrderingReport />
      </>
    );
  };
  render() {
    const { activeLink } = this.state;
    return (
      <React.Fragment>
        <NavbarDashboard />

        <div className="container-fluid main-report">
          <div className="allreport-div">
            <h3>All Reports</h3>
            <div>
              <img
                src={require("../../assets/images/dashboard/reports/download-arrow.svg")}
                alt="download-arrow"
                className="img-fluid download-img"
              />
              <img
                src={require("../../assets/images/dashboard/reports/filter.svg")}
                alt="filter"
                className="img-fluid filter-img"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <span
                className={
                  activeLink === "seating" ? "active p-text" : "p-text"
                }
                onClick={() => this.onClickHandler("seating")}
              >
                Seating
              </span>
              <span
                className={
                  activeLink === "ordering" ? "active p-text" : "p-text"
                }
                onClick={() => this.onClickHandler("ordering")}
              >
                Ordering
              </span>
            </div>
          </div>
          {activeLink === "seating"
            ? this.renderSeatingReport()
            : activeLink === "ordering"
            ? this.renderOrderingReport()
            : null}
        </div>
      </React.Fragment>
    );
  }
}

export default MainReportsComponents;
