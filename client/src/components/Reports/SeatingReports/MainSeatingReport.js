import React, { Component } from "react";
import { Bar, Doughnut } from "react-chartjs-2";

/*************************************
              Chart One Data
*************************************/

const chartonedata = canvas => {
  const ctx = canvas.getContext("2d");
  const gradient1 = ctx.createLinearGradient(0, 0, 0, 500);
  gradient1.addColorStop(0, "rgba(140, 203, 245, 1)");
  gradient1.addColorStop(0.5, "rgba(0, 112, 189, 1)");
  return {
    labels: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ],

    // data: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],

    datasets: [
      {
        data: [15, 50, 45, 30, 43, 75, 41, 60, 80, 75, 55, 100],
        borderColor: "rgba(0,0,0,1)",

        barThickness: 18,

        backgroundColor: gradient1
      }
    ]
  };
};

const options1 = {
  scales: {
    yAxes: [
      {
        ticks: {
          callback: function(label, index, labels) {
            return label / 1;
          }
        },

        scaleLabel: {
          display: true,
          labelString: "Number of Seats (x10)",
          fontSize: 16,
          fontFamily: "K2D-Regular",
          fontColor: "#000"
        },
        gridLines: {
          display: false
        }
      }
    ],
    xAxes: [
      {
        gridLines: {
          display: false
        }
      }
    ]
  },
  legend: {
    display: false
  }
};

/*************************************
              Chart Two Data
*************************************/

const charttwodata = canvas => {
  const ctx = canvas.getContext("2d");
  const gradient1 = ctx.createLinearGradient(0, 0, 0, 500);
  gradient1.addColorStop(0, "rgba(140, 203, 245, 1)");
  gradient1.addColorStop(0.5, "rgba(0, 112, 189, 1)");
  return {
    labels: [
      "11am",
      "12pm",
      "1pm",
      "2pm",
      "3pm",
      "4pm",
      "5pm",
      "5pm",
      "7pm",
      "8pm",
      "9pm",
      "10pm"
    ],
    data: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
    datasets: [
      {
        data: [15, 20, 45, 55, 60, 75, 60, 55, 45, 40, 20, 15],

        borderWidth: 1,

        backgroundColor: gradient1,

        barThickness: 44
      }
    ]
  };
};
const options2 = {
  scales: {
    yAxes: [
      {
        ticks: {
          callback: function(label, index, labels) {
            return label / 1;
          }
        },
        scaleLabel: {
          display: true,
          labelString: "Number of Seats",
          fontSize: 16,
          fontFamily: "K2D-Regular",
          fontColor: "#000"
        },
        gridLines: {
          display: false
        }
      }
    ],
    xAxes: [
      {
        scaleLabel: {
          display: true,
          labelString: "Rushour",
          fontSize: 20,
          fontFamily: "K2D-Bold",
          fontColor: "#000"
        },
        gridLines: {
          display: false
        }
      }
    ]
  },
  legend: {
    display: false
  }
};

/*************************************
              Chart Three Data
*************************************/

const chartthreedata = canvas => {
  const ctx = canvas.getContext("2d");
  const gradient1 = ctx.createLinearGradient(0, 0, 0, 500);
  gradient1.addColorStop(0, "rgba(140, 203, 245, 1)");
  gradient1.addColorStop(0.5, "rgba(0, 112, 189, 1)");
  return {
    data: [25, 50, 75, 100],
    labels: ["Seated", "Unseated", "Completed", "Cancelled"],
    datasets: [
      {
        label: "September 2019",

        data: [50, 30, 20, 10],

        backgroundColor: [
          "rgba(31, 120, 180, 1)",
          "rgba(110, 172, 204, 1)",
          "rgba(166, 206, 227, 1)",
          "rgba(192, 223, 239, 1)"
        ]

        // backgroundColor: gradient1

        // borderWidth: 1,

        // weight: 20,
      }
    ]
  };
};

const options3 = {
  // scales: {
  //   xAxes: [
  //     {
  //       scaleLabel: {
  //         display: true,
  //         labelString: "Seat Status",
  //         fontSize: 20,
  //         fontFamily: "K2D-Bold",
  //         fontColor: "#000"
  //       }
  //     }
  //   ]
  // },
  title: {
    display: true,
    text: "September 2019",
    fontSize: 21,
    fontColor: "#000",
    fontFamily: "K2D-Regular"
  },
  legend: {
    display: true,
    position: "bottom"
  }
};

export class MainSeatingReport extends Component {
  /*****************************
        Render Chart One
   *****************************/
  renderChartOne = () => {
    return (
      <>
        <Bar data={chartonedata} width={100} height={52} options={options1} />
      </>
    );
  };

  /*****************************
        Render Chart Two
   *****************************/
  renderChartTwo = () => {
    return (
      <>
        <Bar data={charttwodata} width={100} height={52} options={options2} />
      </>
    );
  };

  /*****************************
        Render Chart Three
   *****************************/
  renderChartThree = () => {
    return (
      <>
        <Doughnut
          data={chartthreedata}
          width={100}
          height={100}
          options={options3}
        />
      </>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div className="row my-4">
          <div className="col-md-6">
            <div className="chart-one">
              {/* <img
                src={require("../../../assets/images/dashboard/reports/filter.svg")}
                alt="filter"
                className="img-fluid filter-img"
              /> */}
              {this.renderChartOne()}
            </div>
          </div>
          <div className="col-md-6">
            <div className="chart-two">Table</div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="chart-three">
              {/* <img
                src={require("../../../assets/images/dashboard/reports/filter.svg")}
                alt="filter"
                className="img-fluid filter-img"
              /> */}
              {this.renderChartTwo()}
            </div>
          </div>
          <div className="col-md-3 ">
            <div className="chart-four">
              {/* <img
                src={require("../../../assets/images/dashboard/reports/filter.svg")}
                alt="filter"
                className="img-fluid filter-img"
              /> */}
              {this.renderChartThree()}
            </div>
          </div>
          <div className="col-md-3 ">
            <div className="col-12 chart-five">
              <h3>Today</h3>
              <div className="total-revenue-div">
                <div className="d-flex">
                  <i className="fa fa-inr rupee-icon" aria-hidden="true"></i>
                  <h1>20k</h1>
                </div>
                <p>Total Revenue</p>
              </div>
              <hr />
              <div className="Pay-at-site-div">
                <div className="border-right pr-4">
                  <div className="d-flex">
                    <i className="fa fa-inr rupee-icon" aria-hidden="true"></i>
                    <h1>5k</h1>
                  </div>
                  <p>Pay at site</p>
                </div>
                <div>
                  <div className="d-flex">
                    <i className="fa fa-inr rupee-icon" aria-hidden="true"></i>
                    <h1>15k</h1>
                  </div>
                  <p>Online</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MainSeatingReport;
