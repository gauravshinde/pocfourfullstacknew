import React, { Component } from "react";
import InputComponent from "../../../reusableComponents/InputComponent";
import HeaderComponent from "../../../reusableComponents/HeaderComponent";
import isEmpty from "./../../../store/validation/is-Empty";
import OtpInput from "react-otp-input";
// import OtpInputSection from './../Otp/OtpInputSection';
import classnames from "classnames";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  forgot_sendotp,
  forgot_new_password
} from "../../../store/actions/authActions";

import Alert from "react-s-alert";

export class Forgot extends Component {
  constructor() {
    super();
    this.state = {
      //email: "",
      mobile_number: "",
      render: false,
      otp: "",
      password: "",
      confirm_password: "",
      errors: {}
    };
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleOtpChange = otp => {
    this.setState({ otp });
  };

  /******************************
   *  @DESC - ON NUMBER DATA SEND
   *******************************/
  onOtpSend = e => {
    e.preventDefault();
    if (!isEmpty(this.state.mobile_number)) {
      this.setState({
        render: true
      });
    } else {
      this.setState({
        render: false
      });
    }
    let formData = {
      // email: this.state.email,
      mobile_number: this.state.mobile_number
    };
    // console.log(formData);
    this.props.forgot_sendotp(formData);
  };

  /**************************
   *  @DESC - ON DATA SUBMIT
   ***************************/
  handleSendSubmit = e => {
    e.preventDefault();
    const { password, confirm_password } = this.state;

    let formData = {
      mobile_number: this.state.mobile_number,
      password: this.state.password,
      confirm_password: this.state.confirm_password,
      OTP: this.state.otp
    };
    if (password !== confirm_password) {
      // alert("Passwords don't match");
      Alert.warning("<h4>Passwords don't match</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 35
      });
    } else {
      this.props.forgot_new_password(formData);
      Alert.success("<h4>Password Reset Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 35
      });
      window.location.href = "/login";
    }
  };

  renderForgotSection = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        {/* <InputComponent
          labeltext="Enter Email"
          inputlabelclass="input-label"
          imgbox="img-box"
          imgsrc={require("../../../assets/images/icons/at-the-rate.svg")}
          imgclass="img-fluid img pl-1"
          name="email"
          type="email"
          place="eg amealio@gmail.com"
          onChange={this.onChange}
          value={this.state.email}
          error={errors.email}
          inputclass={classnames("input-field", {
            invalid: errors.email
          })}
        /> */}
        <InputComponent
          labeltext="phone number"
          inputlabelclass="input-label"
          imgbox="img-box"
          imgsrc={require("../../../assets/images/icons/phone.svg")}
          imgclass="img-fluid img pl-1"
          name="mobile_number"
          type="text"
          place="eg. 1234567890"
          onChange={this.onChange}
          value={this.state.mobile_number}
          error={errors.mobile_number}
          inputclass={classnames("input-field", {
            invalid: errors.mobile_number
          })}
        />
        <div className="row">
          <div className="col-12 text-right">
            <ButtonComponent
              buttontext="Send"
              buttontype="submit"
              buttonclass="btn button-main button-orange"
              onClick={this.onOtpSend}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  renderForgotPasswordSection = () => {
    const { otp } = this.state;
    return (
      <React.Fragment>
        {/* <h3 className='text-center'>OTP</h3> */}
        <div className="otp-inputfield-class">
          <div className="input-all-field">
            <OtpInput
              inputStyle={{
                width: "2.6rem",
                height: "2.6rem",
                margin: "0 0.5rem",
                fontSize: "1rem",
                borderRadius: 5,
                border: "1px solid rgba(0,0,0,0.3)"
              }}
              onChange={this.handleOtpChange}
              numInputs={6}
              separator={<span>-</span>}
              value={otp}
            />
          </div>
        </div>
        <InputComponent
          labeltext="Password"
          inputlabelclass="input-label pt-3"
          imgbox="img-box"
          imgsrc={require("../../../assets/images/icons/lock.svg")}
          imgclass="img-fluid img pl-1"
          name="password"
          type="password"
          place="******"
          onChange={this.onChange}
          value={this.state.password}
          inputclass="input-field"
        />
        <InputComponent
          labeltext="Confirm Password"
          inputlabelclass="input-label"
          imgbox="img-box"
          imgsrc={require("../../../assets/images/icons/lock.svg")}
          imgclass="img-fluid img pl-1"
          name="confirm_password"
          type="password"
          place="******"
          onChange={this.onChange}
          value={this.state.confirm_password}
          inputclass="input-field"
        />
        <div className="row">
          <div className="col-12 text-right">
            <ButtonComponent
              buttontext="Send"
              buttontype="submit"
              buttonclass="btn button-main button-orange"
              onClick={this.handleSendSubmit}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    console.log(this.state.render);
    const { render } = this.state;
    return (
      <React.Fragment>
        <HeaderComponent />
        <div className="container-fluid login-background-banner main-Login">
          <div className="row">
            <div className="login-background">
              <div className="login_orange">
                <h1>Forgot Password</h1>
              </div>
              <div className="login_padding">
                {render ? (
                  <form>{this.renderForgotPasswordSection()}</form>
                ) : (
                  <form onSubmit={this.handleSendSubmit}>
                    {this.renderForgotSection()}
                  </form>
                )}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(null, { forgot_sendotp, forgot_new_password })(
  withRouter(Forgot)
);
