import React, { Component } from "react";
// import { Link } from 'react-router-dom';
import InputComponent from "./../../../reusableComponents/InputComponent";
import HeaderComponent from "../../../reusableComponents/HeaderComponent";
import classnames from "classnames";
import ButtonComponent from "../../../reusableComponents/ButtonComponent";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { create_new_user } from "../../../store/actions/authActions";
import { clear_error } from "../../../store/actions/errorActions";

class SignUp extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      country_code: "+91",
      mobile_number: "",
      password: "",
      confirm_password: "",
      view: false,
      errors: {}
    };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.auth.isAuthenticated) {
      nextProps.history.push("/dashboard");
    }
    if (nextProps.errors !== nextState.errors) {
      return { errors: nextProps.errors };
    }
    return null;
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.props.clear_error();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC -DATA SUBMIT HANDLER
   ***************************/
  onSubmit = e => {
    e.preventDefault();
    const { password, confirm_password } = this.state;

    let formData = {
      email: this.state.email,
      country_code: this.state.country_code,
      mobile_number: this.state.mobile_number,
      password: this.state.password,
      confirm_password: this.state.confirm_password
    };
    console.log(formData);
    if (password !== confirm_password) {
      alert("Passwords don't match");
    } else {
      this.props.create_new_user(formData, this.props.history);
    }
  };

  // renderSignSection
  renderSignSection = () => {
    const { errors } = this.state;
    return (
      <div className="login_padding">
        <div className="row signup-select-form">
          <label>Phone Number</label>
          <div className="col-12 d-flex justify-content-between ">
            <select
              className="form-control select-custom"
              name="country_code"
              onChange={this.onChange}
              error={errors.country_code}
              inputclass={classnames("input-field", {
                invalid: errors.country_code
              })}
            >
              <option value="+91">+91</option>
              <option value="+92">+92</option>
              <option value="+977">+977</option>
            </select>
            <InputComponent
              // labeltext='phone number'
              // inputlabelclass='input-label'
              imgbox="img-box"
              imgsrc={require("../../../assets/images/icons/phone.svg")}
              imgclass="img-fluid img pl-1"
              name="mobile_number"
              type="text"
              place="eg. 1234567890"
              onChange={this.onChange}
              value={this.state.mobile_number}
              error={errors.mobile_number}
              inputclass={classnames("input-field", {
                invalid: errors.mobile_number
              })}
            />
          </div>
        </div>
        <InputComponent
          labeltext="Email ID"
          inputlabelclass="input-label"
          imgbox="img-box"
          imgsrc={require("../../../assets/images/icons/at-the-rate.svg")}
          imgclass="img-fluid img"
          name="email"
          type="email"
          place="eg. something@something.com"
          onChange={this.onChange}
          value={this.state.email}
          error={errors.email}
          inputclass={classnames("input-field", {
            invalid: errors.email
          })}
        />
        <div className="row">
          <div className="col-sm-10">
            <InputComponent
              labeltext="password"
              inputlabelclass="input-label"
              imgbox="img-box"
              imgsrc={require("../../../assets/images/icons/lock.svg")}
              imgclass="img-fluid img"
              name="password"
              type={!this.state.view ? "password" : "text"}
              place="******"
              onChange={this.onChange}
              value={this.state.password}
              error={errors.password}
              inputclass={classnames("input-field", {
                invalid: errors.password
              })}
            />
          </div>
          <div className="col-sm-2 d-flex justify-content-center align-items-center">
            <img
              src={require("../../../assets/images/icons/view.svg")}
              alt="view"
              className="img-fluid view-button"
              onClick={() => {
                this.setState({ view: !this.state.view });
              }}
            />
          </div>
          <div className="col-sm-10">
            <InputComponent
              labeltext="Confirm Password"
              inputlabelclass="input-label"
              imgbox="img-box"
              imgsrc={require("../../../assets/images/icons/lock.svg")}
              imgclass="img-fluid img"
              name="confirm_password"
              type="password"
              place="******"
              onChange={this.onChange}
              value={this.state.confirm_password}
              error={errors.confirm_password}
              inputclass={classnames("input-field", {
                invalid: errors.confirm_password
              })}
            />
          </div>
          <div className="col-sm-2 d-flex justify-content-center align-items-center">
            {/* <img
                    src={require('../../../assets/images/icons/view.svg')}
                    alt='view'
                    className='img-fluid view-button'
                  /> */}
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-right">
            <ButtonComponent
              buttontext="Sign Up"
              buttontype="submit"
              buttonclass="btn button-main button-orange"
            />
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <React.Fragment>
        <HeaderComponent />
        <div className="container-fluid signup-background-banner main-Login">
          <div className="row">
            <div className="login-background">
              <div className="login_orange">
                <h1>Sign Up</h1>
              </div>
              <form onSubmit={this.onSubmit}>{this.renderSignSection()}</form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  errors: state.errors.errors,
  auth: state.auth
});

export default connect(mapStateToProps, { create_new_user, clear_error })(
  withRouter(SignUp)
);
