import React, { Component } from 'react';
import HeaderComponent from '../../../reusableComponents/HeaderComponent';
import InputComponent from '../../../reusableComponents/InputComponent';
import OtpInput from 'react-otp-input';
// import OtpInputSection from './OtpInputSection';
import classnames from 'classnames';
import ButtonComponent from '../../../reusableComponents/ButtonComponent';
import Countdown from 'react-countdown-now';
import isEmpty from '../../../store/validation/is-Empty';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  verify_otp,
  update_mobile_number,
  resendOtp
} from '../../../store/actions/authActions';
import { clear_error } from '../../../store/actions/errorActions';

class Otp extends Component {
  constructor() {
    super();
    this.state = {
      editForm: false,
      mobile_number: '',
      newmobile_number: '',
      otp: '',
      // otpNumOne: '',
      // otpNumTwo: '',
      // otpNumThree: '',
      // otpNumFour: '',
      // otpNumFive: '',
      // otpNumSix: '',
      // OTP: [],
      errors: {}
    };
  }

  componentDidMount() {
    if (!this.props.history.location.state) {
      window.location.href = '/signup';
    } else {
      this.setState({
        mobile_number: this.props.history.location.state.user_mobile_number
      });
    }
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.auth.isAuthenticated) {
      nextProps.history.push('/dashboard');
    }
    if (nextProps.errors !== nextState.errors) {
      return {
        errors: nextProps.errors
      };
    }
    return null;
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.props.clear_error();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleOtpChange = otp => {
    this.setState({ otp });
  };

  /****************************
   * DESC EDIT PHONE NUMBER CLICK
   ****************************/
  handleEditPhoneNumber = () => {
    this.setState({
      editForm: !this.state.editForm
    });
  };

  /****************************
   * RESENT OTP
   ****************************/
  resendOTPHandler = e => {
    e.preventDefault();
    let formData = {
      mobile_number: this.state.mobile_number
    };
    this.props.resendOtp(
      formData,
      (document.getElementById('re_Sernt').innerHTML = 'OTP re-sent!')
    );
  };
  /**************************
   *  @DESC - ONLY EDIT NUMBER DATA SUBMIT
   ***************************/
  handleEditNumberSubmit = e => {
    e.preventDefault();
    let formData = {
      mobile_number: this.state.mobile_number,
      newmobile_number: this.state.newmobile_number
    };
    this.props.update_mobile_number(
      formData,
      this.setState({
        editForm: false,
        mobile_number: this.state.newmobile_number,
        newmobile_number: ''
      })
    );
  };

  /**************************
   *  @DESC - ON DATA SUBMIT
   ***************************/
  handleSendSubmit = e => {
    e.preventDefault();
    // alert(this.state.otp);
    // console.log(this.state);
    let formData = {
      mobile_number: this.state.mobile_number,
      OTP: this.state.otp
      //   this.state.otpNumOne +
      //   this.state.otpNumTwo +
      //   this.state.otpNumThree +
      //   this.state.otpNumFour +
      //   this.state.otpNumFive +
      //   this.state.otpNumSix
    };
    // console.log(formData);
    this.props.verify_otp(formData);
  };

  // renderOtpSection
  renderEditSection = () => {
    const { errors } = this.state;

    const Completionist = () => (
      <span>OTP has been expired. Press re-send otp !</span>
    );
    // Renderer callback with condition
    const renderer = ({ hours, minutes, seconds, completed }) => {
      if (completed) {
        // Render a completed state
        return <Completionist />;
      } else {
        // Render a countdown
        return (
          <span>
            {minutes}:{seconds}
          </span>
        );
      }
    };
    return (
      <React.Fragment>
        <p className='otp-para'>
          Your OTP has been sent to your email and your phone number{' '}
          {this.state.mobile_number}
        </p>
        <div is='re_Sernt'></div>
        {errors.message ? <div className='error'>{errors.message}</div> : null}
        <h3 className='otp-heading'>
          {isEmpty(this.state.errors) && !this.state.editForm ? (
            <Countdown date={Date.now() + 180000} renderer={renderer} />
          ) : null}
        </h3>
        <h4 className='otp-headingtwo'>
          Not your number?
          <b onClick={this.handleEditPhoneNumber}> Edit Phone Number</b>
        </h4>
        {this.state.editForm && (
          <div className='d-flex'>
            <InputComponent
              imgbox='img-box'
              imgsrc={require('../../../assets/images/icons/phone.svg')}
              imgclass='img-fluid img'
              name='newmobile_number'
              type='text'
              place='eg. 1234567890'
              onChange={this.onChange}
              value={this.state.newmobile_number}
              error={errors.newmobile_number}
              inputclass={classnames('input-field w-75', {
                invalid: errors.newmobile_number
              })}
            />
            <ButtonComponent
              onClick={this.handleEditNumberSubmit}
              buttontext='Send'
              buttontype='submit'
              buttonclass='btn button-main button-orange'
            />
          </div>
        )}
      </React.Fragment>
    );
  };

  renderEditInputSection = () => {
    const { otp } = this.state;
    return (
      <React.Fragment>
        <div className='input-all-field'>
          <OtpInput
            inputStyle={{
              width: '2.6rem',
              height: '2.6rem',
              margin: '0 0.5rem',
              fontSize: '1rem',
              borderRadius: 5,
              border: '1px solid rgba(0,0,0,0.3)'
            }}
            onChange={this.handleOtpChange}
            numInputs={6}
            separator={<span>-</span>}
            value={otp}
          />
          {/* <OtpInputSection
            name='otpNumOne'
            onChange={this.onChange}
            value={this.state.otpNumOne}
            error={errors.otpNumOne}
            inputclass={classnames('input-field w-75', {
              invalid: errors.otpNumOne
            })}
          />
          <OtpInputSection
            name='otpNumTwo'
            onChange={this.onChange}
            value={this.state.otpNumTwo}
            error={errors.otpNumTwo}
            inputclass={classnames('input-field w-75', {
              invalid: errors.otpNumTwo
            })}
          />
          <OtpInputSection
            name='otpNumThree'
            onChange={this.onChange}
            value={this.state.otpNumThree}
            error={errors.otpNumThree}
            inputclass={classnames('input-field w-75', {
              invalid: errors.otpNumThree
            })}
          />
          <OtpInputSection
            name='otpNumFour'
            onChange={this.onChange}
            value={this.state.otpNumFour}
            error={errors.otpNumFour}
            inputclass={classnames('input-field w-75', {
              invalid: errors.otpNumFour
            })}
          />
          <OtpInputSection
            name='otpNumFive'
            onChange={this.onChange}
            value={this.state.otpNumFive}
            error={errors.otpNumFive}
            inputclass={classnames('input-field w-75', {
              invalid: errors.otpNumFive
            })}
          />
          <OtpInputSection
            name='otpNumSix'
            onChange={this.onChange}
            value={this.state.otpNumSix}
            error={errors.otpNumSix}
            inputclass={classnames('input-field w-75', {
              invalid: errors.otpNumSix
            })}
          /> */}
        </div>

        <h5 className='have-received'>Haven't received an OTP?</h5>
        <div className='d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Resend OTP'
            buttontype='button'
            buttonclass='btn button-main button-white'
          />
          <ButtonComponent
            buttontext='Submit'
            buttontype='submit'
            buttonclass='btn button-main button-orange'
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <HeaderComponent />
        <div className='container-fluid otp-background-banner main-Login'>
          <div className='row'>
            <div className='login-background'>
              <div className='login_orange'>
                <h1>OTP</h1>
              </div>
              <div className='login_padding'>
                <form
                  onSubmit={this.handleEditNumberSubmit}
                  className='otp-classes'
                >
                  {this.renderEditSection()}
                </form>
                <form
                  onSubmit={this.handleSendSubmit}
                  className='otp-inputfield-class'
                >
                  {this.renderEditInputSection()}
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  errors: state.errors.errors,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { verify_otp, clear_error, update_mobile_number, resendOtp }
)(withRouter(Otp));
