import React, { Component } from "react";
import { Link } from "react-router-dom";
import InputComponent from "./../../../reusableComponents/InputComponent";
import HeaderComponent from "../../../reusableComponents/HeaderComponent";
import classnames from "classnames";
import ButtonComponent from "../../../reusableComponents/ButtonComponent";
import Alert from "react-s-alert";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { user_login, logOutUser } from "../../../store/actions/authActions";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      mobile_number: "",
      password: "",
      errors: {},
    };
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    // if (nextProps.auth.isAuthenticated) {
    //   if (nextProps.auth.user.role === "superadmin") {
    //     nextProps.history.push("/admindashboard");
    //     Alert.success("<h4>Super Admin Login Successfully</h4>", {
    //       position: "top-right",
    //       effect: "bouncyflip",
    //       beep: true,
    //       html: true,
    //       timeout: 5000,
    //       offset: 30
    //     });
    //   } else if (nextProps.auth.user.role === "vendor") {
    //     nextProps.history.push("/dashboard");
    //     Alert.success("<h4>Vendor Login Successfully</h4>", {
    //       position: "top-right",
    //       effect: "bouncyflip",
    //       beep: true,
    //       html: true,
    //       timeout: 5000,
    //       offset: 30
    //     });
    //   }
    // }

    // if (nextProps.auth.user.has_admin_approved === true) {
    if (nextProps.auth.isAuthenticated) {
      if (
        nextProps.auth.user.has_admin_approved === true &&
        nextProps.auth.user.have_submited_details === true &&
        nextProps.auth.user.role === "vendor"
      ) {
        nextProps.history.push("/dashboard");
        Alert.success("<h4>Vendor Login Successfully</h4>", {
          position: "top-right",
          effect: "bouncyflip",
          beep: true,
          html: true,
          timeout: 5000,
          offset: 30,
        });
      } else if (
        nextProps.auth.user.has_admin_approved === false &&
        nextProps.auth.user.have_submited_details === false &&
        nextProps.auth.user.role === "vendor"
      ) {
        nextProps.history.push("/maindetailpage");
        Alert.success("<h4>Please Follow SignUp Process</h4>", {
          position: "top-right",
          effect: "bouncyflip",
          beep: true,
          html: true,
          timeout: 5000,
          offset: 30,
        });
      } else if (
        nextProps.auth.user.has_admin_approved === true &&
        nextProps.auth.user.have_submited_details === true &&
        nextProps.auth.user.role === "superadmin"
      ) {
        Alert.success("<h4>Super Admin Login Successfully</h4>", {
          position: "top-right",
          effect: "bouncyflip",
          beep: true,
          html: true,
          timeout: 5000,
          offset: 30,
        });
        nextProps.history.push("/admindashboard");
      } else if (
        nextProps.auth.user.has_admin_approved === false &&
        nextProps.auth.user.have_submited_details === true &&
        nextProps.auth.user.role === "vendor"
      ) {
        Alert.success(
          "<h4>Super Admin Not Approved Your Details. Wait For Some time... </h4>",
          {
            position: "top-right",
            effect: "bouncyflip",
            beep: true,
            html: true,
            timeout: 5000,
            offset: 30,
          }
        );
        nextProps.logOutUser();
      }
    }
    // }

    if (nextProps.errors !== nextState.errors) {
      return { errors: nextProps.errors };
    }
    return null;
  }

  /**************************
   *  @DESC - ON DATA SUBMIT
   ***************************/
  onSubmit = (e) => {
    e.preventDefault();
    let formData = {
      mobile_number: this.state.mobile_number,
      password: this.state.password,
      FCMtoken: "Web",
    };
    this.props.user_login(formData);
  };

  // renderLoginSection
  renderLoginSection = () => {
    const { errors } = this.state;
    return (
      <div className="container-fluid login-background-banner main-Login">
        <div className="row">
          <div className="login-background">
            <div className="login_orange">
              <h1>Login</h1>
            </div>
            <div className="login_padding">
              <InputComponent
                labeltext="phone number"
                inputlabelclass="input-label"
                imgbox="img-box"
                imgsrc={require("../../../assets/images/icons/phone.svg")}
                imgclass="img-fluid img"
                name="mobile_number"
                type="number"
                place="eg. 1234567890"
                onChange={this.onChange}
                value={this.state.mobile_number}
                error={errors.mobile_number}
                inputclass={classnames("input-field", {
                  invalid: errors.mobile_number,
                })}
              />
              <div className="row">
                <div className="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                  <InputComponent
                    labeltext="password"
                    inputlabelclass="input-label"
                    imgbox="img-box"
                    imgsrc={require("../../../assets/images/icons/lock.svg")}
                    imgclass="img-fluid img"
                    name="password"
                    type={!this.state.view ? "password" : "text"}
                    place="******"
                    onChange={this.onChange}
                    value={this.state.password}
                    error={errors.password}
                    inputclass={classnames("input-field", {
                      invalid: errors.password,
                    })}
                  />
                </div>
                <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 d-flex justify-content-center align-items-center">
                  <img
                    src={require("../../../assets/images/icons/view.svg")}
                    alt="view"
                    className="img-fluid view-button"
                    onClick={() => {
                      this.setState({ view: true });
                    }}
                  />
                </div>
              </div>
              <div className="row">
                <Link to="/forgotpassword" className="forgot-password col-12">
                  forgot password?
                </Link>
                <div className="col-12 text-right">
                  <ButtonComponent
                    buttontext="Login"
                    buttontype="submit"
                    buttonclass="btn button-main button-orange"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div>
          <HeaderComponent />
          <form noValidate onSubmit={this.onSubmit}>
            {this.renderLoginSection()}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors.errors,
});

export default connect(mapStateToProps, { user_login, logOutUser })(
  withRouter(Login)
);
// 09-04-2020 11:37am
