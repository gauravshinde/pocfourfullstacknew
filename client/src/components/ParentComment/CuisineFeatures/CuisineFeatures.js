import React, { Component } from "react";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import SuggestNew from "./../../../reusableComponents/SuggestNew";
import isEmpty from "../../../store/validation/is-Empty";
import IconBox from "../../../reusableComponents/IconBoxButtonComponent";
import Toggle from "../../../reusableComponents/SlidingComponent";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  create_cusine_one,
  update_restaurant_cuisine_features_new
} from "../../../store/actions/addDetailsActions";

export class CuisineFeatures extends Component {
  constructor() {
    super();
    this.state = {
      all_food_category_icons: [],
      all_food_items_icons: [],

      primary_food_category_icons: {},
      selected_food_items_type: [],
      primary_food_item_type: {},

      allergy_information: false,
      serve_liquor: false,
      nutri_info: false
    };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      nextProps.icons.get_food_category_icons !==
        nextState.all_food_category_icons ||
      nextProps.icons.get_food_item_icons !== nextState.all_food_items_icons
    ) {
      return {
        all_food_category_icons: nextProps.icons.get_food_category_icons,
        all_food_items_icons: nextProps.icons.get_food_item_icons
      };
    }
    return null;
  }

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      primary_food_category_icons: this.state.primary_food_category_icons,
      selected_food_items_type: this.state.selected_food_items_type,
      primary_food_item_type: this.state.primary_food_item_type,
      allergy_information: this.state.allergy_information,
      serve_liquor: this.state.serve_liquor,
      nutri_info: this.state.nutri_info
    };
    // this.props.create_cusine_one(formData, this.pageChangeHandle(9));
    this.props.update_restaurant_cuisine_features_new(
      formData,
      this.pageChangeHandle(9)
    );
  };

  /****************************************
   * @DESC - FOOD ITEM FEATURES
   ****************************************/
  onFoodItemArraySelector = icon => e => {
    let FoodItem = this.state.selected_food_items_type;
    if (FoodItem.length === 0) {
      FoodItem.push(icon);
    } else {
      let isAlreadyPresent = FoodItem.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = FoodItem.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          FoodItem.splice(indexOf, 1);
        }
      } else {
        FoodItem.push(icon);
      }
    }
    this.setState({
      selected_food_items_type: FoodItem
    });
  };

  onFoodItemPrimarySelector = icon => e => {
    this.setState({
      primary_food_item_type: icon
    });
  };

  onFoodCategoryPrimarySelector = icon => e => {
    this.setState({
      primary_food_category_icons: icon
    });
  };

  renderCuisineFeatures = () => {
    return (
      <React.Fragment>
        <div className="cuisine-main">
          <h2 className="heading-title">
            What category food do you provide?
            <small
              id="emailHelp"
              class="form-text text-muted input-help-textnew"
            >
              ( Select Vegetarian if you are pure veg only )
            </small>
          </h2>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.all_food_category_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_food_category_icons
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
                onClick={this.onFoodCategoryPrimarySelector(icon)}
              />
            ))}
            <SuggestNew
              title="Suggest New"
              classsection="main-suggest-button"
            />
          </div>

          <h2 className="heading-title" style={{ paddingTop: "30px" }}>
            Do you have allergy information on your dishes?
            <small
              id="emailHelp"
              class="form-text text-muted input-help-textnew"
            >
              ( If you select this option, you will have to provide ingredient
              details setting up menu items )
            </small>
          </h2>
          <Toggle
            name="allergy_information"
            currentState={this.state.allergy_information}
            type={"checkbox"}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center mb-2"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
            defaultChecked={false}
          />

          <h2 className="heading-title" style={{ paddingTop: "30px" }}>
            Do you serve liquor?
          </h2>
          <Toggle
            name="serve_liquor"
            currentState={this.state.serve_liquor}
            type={"checkbox"}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center mb-2"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
            defaultChecked={false}
          />

          <h2 className="heading-title" style={{ paddingTop: "30px" }}>
            Do you have nutrition information on your dishes?
            <small
              id="emailHelp"
              class="form-text text-muted input-help-textnew"
            >
              ( If you select this option, you will have to provide ingredient
              details setting up menu items )
            </small>
          </h2>
          <Toggle
            name="nutri_info"
            currentState={this.state.nutri_info}
            type={"checkbox"}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center mb-2"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
            defaultChecked={false}
          />

          {/* FOOD ITEMS */}
          <h2 className="heading-title" style={{ paddingTop: "30px" }}>
            What type of food items do you serve?
            <small
              id="emailHelp"
              class="form-text text-muted input-help-textnew"
            >
              ( Select all options that you provide )
            </small>
          </h2>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.all_food_items_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection="main-icon-button"
                onClick={this.onFoodItemArraySelector(icon)}
              />
            ))}
            <SuggestNew
              title="Suggest New"
              classsection="main-suggest-button"
            />
          </div>

          {/** */}
          {!isEmpty(this.state.selected_food_items_type) ? (
            <h2 className="heading-title" style={{ paddingTop: "30px" }}>
              Out of the ones you selected, which one is primary?
              <small
                id="emailHelp"
                class="form-text text-muted input-help-textnew"
              >
                ( Select one of the options from below )
              </small>
            </h2>
          ) : null}

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.selected_food_items_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_food_item_type
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
                onClick={this.onFoodItemPrimarySelector(icon)}
              />
            ))}
          </div>
        </div>
        {/* <hr className='hr-global my-2' /> */}
        <div className=" mt-3 w-50 ml-auto mt-5 d-flex justify-content-between">
          <ButtonComponent
            buttontext="Back"
            buttontype="button"
            buttonclass="btn button-main button-white"
            onClick={this.pageChangeHandle(7)}
          />
          <ButtonComponent
            buttontext="Next"
            buttontype="button"
            buttonclass="btn button-main button-orange"
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4>Cuisine Features</h4>
          <p>
            Enter the information about the cuisines provided in your
            restaurant.
          </p>
          <hr className="hr-global" />
          <form>{this.renderCuisineFeatures()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  icons: state.icons,
  auth: state.auth
});

export default connect(mapStateToProps, {
  create_cusine_one,
  update_restaurant_cuisine_features_new
})(withRouter(CuisineFeatures));
