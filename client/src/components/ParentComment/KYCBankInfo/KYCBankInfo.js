import React, { Component } from "react";
import InputComponent from "./../../../reusableComponents/InputComponent";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import classnames from "classnames";
import SlidingComponent from "./../../../reusableComponents/SlidingComponent";
import axios from "axios";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  create_Kycdetails,
  update_restaurant_kyc_new,
  get_kyc_details,
} from "./../../../store/actions/addDetailsActions";
import { clear_error } from "../../../store/actions/errorActions";
import { serverApi } from "../../../config/Keys";
import isEmpty from "./../../../store/validation/is-Empty";

export class KYCBankInfo extends Component {
  constructor() {
    super();
    this.state = {
      account_name: "",
      bank_name: "",
      account_number: "",
      branch_name: "",
      GST_number: "",
      IFSC_code: "",
      PAN_number: "",
      FSSAI_code: "",
      imgPath: [],
      currentAccount: false,
      errors: {},
    };
  }

  componentDidMount() {
    this.props.get_kyc_details();
    this.setState({
      account_name: this.props.allKycDetails.account_name,
      bank_name: this.props.allKycDetails.bank_name,
      account_number: this.props.allKycDetails.account_number,
      branch_name: this.props.allKycDetails.branch_name,
      GST_number: this.props.allKycDetails.GST_number,
      IFSC_code: this.props.allKycDetails.IFSC_code,
      PAN_number: this.props.allKycDetails.PAN_number,
      FSSAI_code: this.props.allKycDetails.FSSAI_code,
      currentAccount: this.props.allKycDetails.currentAccount,
      hasSetDetails: true,
    });
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.errors !== nextState.errors) {
      return { errors: nextProps.errors };
    }
    if (!isEmpty(nextProps.allKycDetails) && !nextState.hasSetDetails) {
      console.log(nextProps.allKycDetails);
      return {
        account_name: nextProps.allKycDetails.account_name,
        bank_name: nextProps.allKycDetails.bank_name,
        account_number: nextProps.allKycDetails.account_number,
        branch_name: nextProps.allKycDetails.branch_name,
        GST_number: nextProps.allKycDetails.GST_number,
        IFSC_code: nextProps.allKycDetails.IFSC_code,
        PAN_number: nextProps.allKycDetails.PAN_number,
        FSSAI_code: nextProps.allKycDetails.FSSAI_code,
        currentAccount: nextProps.allKycDetails.currentAccount,
        hasSetDetails: true,
      };
    }
    return null;
  }

  pageChangeHandle = (value) => (e) => {
    this.props.pageChanger(value);
    if (value === 12) {
      this.props.pageCompletedHandler(4);
    }
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  // onImageUploadHandler = e => {
  //   this.setState({ loader: true });
  //   const data = new FormData();
  //   data.append('image', e.target.files[0]);
  //   axios
  //     .post('/image/upload-content-images', data)
  //     .then(res => {
  //       this.setState({ imgPath: res.data.image_URL, loader: false });
  //     })
  //     .catch(err => {
  //       this.setState({ loader: false });
  //       window.alert('Error while uploading the image');
  //     });
  // };
  onImageUploadHandler = (e) => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post(`${serverApi}/image/upload-content-images`, data)
      .then((res) => {
        let imgPath = this.state.imgPath;
        imgPath.push(res.data.image_URL);
        //URL.createObjectURL(event.target.files[0])
        this.setState({ imgPath: imgPath, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    this.props.clear_error();
    let data = e.target.value.split(" ");
    let newarray1 = [];
    for (let x = 0; x < data.length; x++) {
      newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
    }
    let newData = newarray1.join(" ");
    this.setState({
      // [e.target.name]: e.target.value
      [e.target.name]: newData,
    });
    // this.setState({
    //   [e.target.name]: e.target.value
    // });
  };

  /**************************
   * @DESC - Current and Saving HANDLER
   ***************************/
  toggleFunction = (e) => {
    this.setState({
      [e.target.name]: e.target.checked,
    });
  };

  backClickHandler = (e) => {
    e.preventDefault();
    this.setState({
      BackToRestaurantTimingPage: true,
    });
  };

  /**************************
   * @DESC - OnSubmit Handler
   ***************************/
  onSubmit = (e) => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      account_name: this.state.account_name,
      bank_name: this.state.bank_name,
      account_number: this.state.account_number,
      branch_name: this.state.branch_name,
      GST_number: this.state.GST_number,
      IFSC_code: this.state.IFSC_code,
      PAN_number: this.state.PAN_number,
      FSSAI_code: this.state.FSSAI_code,
      imgPath: this.state.imgPath,
      currentAccount: this.state.currentAccount,
    };
    // console.log(formData);
    // this.props.create_Kycdetails(formData, this.pageChangeHandle(12));
    this.props.update_restaurant_kyc_new(formData, this.pageChangeHandle(12));
  };

  renderKYCEdit = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <div className="col-10 mx-auto">
          <InputComponent
            labeltext="Account Holder Name"
            inputlabelclass="input-label"
            imgbox="d-none"
            name="account_name"
            type="text"
            place="eg. McDonalds"
            onChange={this.onChange}
            value={this.state.account_name}
            error={errors.account_name}
            inputclass={classnames("map-inputfield text-capitalize", {
              invalid: errors.account_name,
            })}
          />
          <InputComponent
            labeltext="Bank Name"
            inputlabelclass="input-label"
            imgbox="d-none"
            name="bank_name"
            type="text"
            place="e.g. Maharastra Bank"
            onChange={this.onChange}
            value={this.state.bank_name}
            error={errors.bank_name}
            inputclass={classnames("map-inputfield text-capitalize", {
              invalid: errors.bank_name,
            })}
          />
          <div className="toggleclass">
            <div className="account-type">Account Type?</div>
            <SlidingComponent
              name="currentAccount"
              currentState={this.state.currentAccount}
              type={"checkbox"}
              spantext1={"Current Account"}
              spantext2={"Savings Account"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={this.toggleFunction}
              defaultChecked={false}
            />
          </div>

          <InputComponent
            labeltext="Account Number"
            inputlabelclass="input-label mt-3"
            imgbox="d-none"
            name="account_number"
            type="number"
            place="eg.1234567890123456"
            onChange={this.onChange}
            value={this.state.account_number}
            error={errors.account_number}
            inputclass={classnames("map-inputfield", {
              invalid: errors.account_number,
            })}
            smalltext="( 12 Digits to Max 16 digits )"
          />
          <InputComponent
            labeltext="Branch Name"
            inputlabelclass="input-label"
            imgbox="d-none"
            name="branch_name"
            type="text"
            place="eg. Magarpatta Branch"
            onChange={this.onChange}
            value={this.state.branch_name}
            error={errors.branch_name}
            inputclass={classnames("map-inputfield text-capitalize", {
              invalid: errors.branch_name,
            })}
          />
          <div className="row">
            <div className="col">
              <InputComponent
                labeltext="GST Number"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="GST_number"
                type="text"
                place="eg. KKZB02343343"
                onChange={this.onChange}
                value={this.state.GST_number}
                error={errors.GST_number}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.GST_number,
                })}
                smalltext="( Max 15 Char )"
              />
            </div>
            <div className="col">
              <InputComponent
                labeltext="IFSC Code"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="IFSC_code"
                type="text"
                place="eg. KKZB02343343"
                onChange={this.onChange}
                value={this.state.IFSC_code}
                error={errors.IFSC_code}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.IFSC_code,
                })}
                smalltext="( 11 Digits Char Code )"
              />
            </div>
          </div>
          <div className="row">
            <div className="col">
              <InputComponent
                labeltext="PAN Number"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="PAN_number"
                type="text"
                place="eg. AZOZZE83GZD"
                onChange={this.onChange}
                value={this.state.PAN_number}
                error={errors.PAN_number}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.PAN_number,
                })}
                smalltext="( 10 Digits Char Code )"
              />
            </div>
            <div className="col">
              <InputComponent
                labeltext="FSSAI Code"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="FSSAI_code"
                type="text"
                place="eg. 1232SAAB#34322343"
                onChange={this.onChange}
                value={this.state.FSSAI_code}
                error={errors.FSSAI_code}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.FSSAI_code,
                })}
                smalltext="( 14 Digits Char Code )"
              />
            </div>
          </div>
          <div className="row mb-3">
            <div className="col-sm-4">
              <p>Upload KYC Documents</p>
              <span className="input-help-textnew">
                ( Upload supporting documents for PAN, GST, FSSAI and a
                Cancelled Cheque as applicable )
              </span>
            </div>
            <div className="col-sm-8">
              <div
                className="view_chainsBor overflow-auto"
                style={{ width: "100%" }}
              >
                <div className="custom_file_upload">
                  <input
                    type="file"
                    name="icon"
                    id="file"
                    onChange={this.onImageUploadHandler}
                    className="custom_input_upload"
                  />
                  <label
                    className="custom_input_label newChain_add"
                    htmlFor="file"
                  >
                    <div>
                      <i
                        className="fa fa-plus"
                        style={{ color: "#CCCCCC" }}
                      ></i>
                    </div>
                    <div className="add_new_text">Add New</div>
                  </label>
                </div>

                <div className="newChain_addthree mx-3">
                  {this.state.imgPath.length > 0
                    ? this.state.imgPath.map((image, index) => (
                        <img
                          key={index}
                          src={image}
                          className="newChain_addtwo"
                          style={{ height: "100%", width: "100%" }}
                          alt="chain "
                        />
                      ))
                    : null}
                  {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
                  {/* <img
                    src={this.state.imgPath}
                    className='newChain_addtwo'
                    style={{ height: '100%', width: '100%' }}
                    alt='upload Kyc'
                  /> */}
                </div>
              </div>
              {/* <div className='multiImageInputFile-outerBlock multiImageInputFile-outerBlock--KYC'>
                <div className='multiImageInputFile-block'>
                  <img
                    src={require('../../../assets/images/add-file.svg')}
                    alt='add-file-img'
                  />
                  <input
                    className='multiImageInputFile'
                    type='file'
                    multiple
                    title=''
                    onChange={this.handleFilesOnChange}
                  />
                </div>
                <div className='multiImageInputFile-preview'>
                  {this.state.imgPath.map((val, index) => (
                    <div key={index}>
                      <img src={val} alt='list' />
                    </div>
                  ))}
                </div>
                  </div> */}
            </div>
          </div>
          <div className="col-8 ml-auto mt-5 mb-4 d-flex justify-content-between">
            <ButtonComponent
              buttontext="Back"
              buttontype="button"
              buttonclass="btn button-main button-white"
              onClick={this.pageChangeHandle(10)}
            />
            <ButtonComponent
              buttontext="Next"
              buttontype="button"
              buttonclass="btn button-main button-orange"
              onClick={this.onSubmit}
            />
          </div>
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log(this.state);
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4>KYC and Bank Info</h4>
          <p>Enter banking information about your restaurant.</p>
          <hr className="hr-global" />
          <form>{this.renderKYCEdit()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  icons: state.icons,
  auth: state.auth,
  allKycDetails: state.details.kyc_details,
});

export default connect(mapStateToProps, {
  create_Kycdetails,
  clear_error,
  update_restaurant_kyc_new,
  get_kyc_details,
})(withRouter(KYCBankInfo));
//changes new
