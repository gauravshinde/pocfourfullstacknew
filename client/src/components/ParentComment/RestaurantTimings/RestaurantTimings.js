import React, { Component } from "react";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import SuggestNew from "./../../../reusableComponents/SuggestNew";
import isEmpty from "../../../store/validation/is-Empty";
import IconBox from "../../../reusableComponents/IconBoxButtonComponent";
import Toggle from "../../../reusableComponents/SlidingComponent";
import Switch from "react-switch";

import AddNewChain from "./AddNewChain";
import ViewAllChains from "./ViewAllChains";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  create_restaraunttime,
  update_restaurant_timings_new,
  get_restauranttime,
} from "./../../../store/actions/addDetailsActions";
import SlidingComponent from "./../../../reusableComponents/SlidingComponent";

// const [] = [
//   { title: "Indoor" },
//   { title: "Outdoor" },
//   { title: "Private" },
//   { title: "Bar" }
// ];

const foodTime = ["breakfast", "lunch", "dinner"];

let dayArray = ["monday"];

const timeArray = [
  "00:00",
  "00:30",
  "01:00",
  "01:30",
  "02:00",
  "02:30",
  "03:00",
  "03:30",
  "04:00",
  "04:30",
  "05:00",
  "05:30",
  "06:00",
  "06:30",
  "07:00",
  "07:30",
  "08:00",
  "08:30",
  "09:00",
  "09:30",
  "10:00",
  "10:30",
  "11:00",
  "11:30",
  "12:00",
  "12:30",
  "13:00",
  "13:30",
  "14:00",
  "14:30",
  "15:00",
  "15:30",
  "16:00",
  "16:30",
  "17:00",
  "17:30",
  "18:00",
  "18:30",
  "19:00",
  "19:30",
  "20:00",
  "20:30",
  "21:00",
  "21:30",
  "22:00",
  "22:30",
  "23:00",
  "23:30",
  "24:00",
  "24:30",
];

export class RestaurantTimings extends Component {
  constructor() {
    super();
    this.state = {
      allDaySet: false,
      allDaySetNew: false,
      dayArray: ["monday"],
      foodTime: ["breakfast", "lunch", "dinner"],
      all_seating_area_icons: [],
      selected_seating_area: [],
      primary_seating_area: {},
      total_seating_capacity: "",
      part_of_chain: false,
      selectedChains: [],

      are_you_open_24_x_7: true,
      multiple_opening_time: false,
      table_management: false,

      monday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      tuesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      wednesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      thursday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      friday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      saturday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      sunday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
    };
  }

  componentDidMount() {
    this.props.get_restauranttime();
    this.setState({
      total_seating_capacity: this.props.allRestaurentTime
        .total_seating_capacity,
      hasSetDetails: true,
    });
  }

  componentDidUpdate() {
    //console.log(this.state);
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.icons.get_subscription_icon);
    if (!isEmpty(nextProps.allRestaurentTime) && !nextState.hasSetDetails) {
      console.log(nextProps.allRestaurentTime);
      return {
        total_seating_capacity:
          nextProps.allRestaurentTime.total_seating_capacity,
        hasSetDetails: true,
      };
    }
    if (
      nextProps.icons.get_subscription_icon !== nextState.all_seating_area_icons
    ) {
      return {
        all_seating_area_icons: nextProps.icons.get_subscription_icon,
      };
    }
    return null;
  }

  /***********************
   * @DESC - PAGE CHANGER
   **********************/
  pageChangeHandle = (value) => (e) => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  /**************************
   * @DESC - TOGGLE FUNCTION
   ***************************/
  toggleFunction = (e) => {
    this.setState({
      [e.target.name]: e.target.checked,
    });
  };

  /****************************
   * @DESC - DAY TOGGLER -
   ***************************/
  onDayOpenCloseHanlder = (day, food) => (e) => {
    let state = this.state;
    let dayData = state[day];
    dayData.open = !dayData.open;

    this.setState({
      state: state,
    });
    if (dayData.open === false) {
      dayData.breakfast.open = false;
      dayData.lunch.open = false;
      dayData.dinner.open = false;
    } else {
      dayData.breakfast.open = true;
      dayData.lunch.open = true;
      dayData.dinner.open = true;
    }
    this.setState({
      state: state,
      // allDaySetNew: true,
    });
  };

  onTimeSelectHandlerSingle = (day) => (e) => {
    let state = this.state;
    let dayData = state[day];

    // console.log(e.target.name);

    dayData[e.target.name] = e.target.value;
    if (this.state.allDaySet === false) {
      let tuesdayData = state["tuesday"];
      let wednesday = state["wednesday"];
      let thursday = state["thursday"];
      let friday = state["friday"];
      let saturday = state["saturday"];
      let sunday = state["sunday"];

      tuesdayData[e.target.name] = e.target.value;
      wednesday[e.target.name] = e.target.value;
      thursday[e.target.name] = e.target.value;
      friday[e.target.name] = e.target.value;
      saturday[e.target.name] = e.target.value;
      sunday[e.target.name] = e.target.value;

      tuesdayData["open"] = true;
      wednesday["open"] = true;
      thursday["open"] = true;
      friday["open"] = true;
      saturday["open"] = true;
      sunday["open"] = true;
    }

    // console.log("tuesday Data ===>", tuesdayData);

    this.setState({
      state: state,
    });
  };

  onFoodTypeOpenClose = (day, food) => (e) => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData.open = !foodTimeData.open;

    console.log(day, food);
    let tuesdayData = state[day];

    if (this.state.allDaySetNew === false) {
      let tuesdayData = state["tuesday"];
      let wednesday = state["wednesday"];
      let thursday = state["thursday"];
      let friday = state["friday"];
      let saturday = state["saturday"];
      let sunday = state["sunday"];

      // let breakfast = dayData["breakfast"];
      // let lunch = dayData["lunch"];
      // let dinner = dayData["dinner"];

      tuesdayData.breakfast["open"] = true;
      wednesday.breakfast["open"] = true;
      thursday.breakfast["open"] = true;
      friday.breakfast["open"] = true;
      saturday.breakfast["open"] = true;
      sunday.breakfast["open"] = true;

      tuesdayData.lunch["open"] = true;
      wednesday.lunch["open"] = true;
      thursday.lunch["open"] = true;
      friday.lunch["open"] = true;
      saturday.lunch["open"] = true;
      sunday.lunch["open"] = true;

      tuesdayData.dinner["open"] = true;
      wednesday.dinner["open"] = true;
      thursday.dinner["open"] = true;
      friday.dinner["open"] = true;
      saturday.dinner["open"] = true;
      sunday.dinner["open"] = true;

      tuesdayData["open"] = true;
      wednesday["open"] = true;
      thursday["open"] = true;
      friday["open"] = true;
      saturday["open"] = true;
      sunday["open"] = true;
    }

    if (
      tuesdayData.breakfast["open"] === false &&
      tuesdayData.lunch["open"] === false &&
      tuesdayData.dinner["open"] === false
    ) {
      tuesdayData["open"] = false;
    } else {
      tuesdayData["open"] = true;
    }

    this.setState({
      state: state,
    });
  };

  onTimeSelectHandlerMultiple = (day, food) => (e) => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData[e.target.name] = e.target.value;

    if (this.state.allDaySetNew === false) {
      let tuesdayData = state["tuesday"];
      let wednesday = state["wednesday"];
      let thursday = state["thursday"];
      let friday = state["friday"];
      let saturday = state["saturday"];
      let sunday = state["sunday"];

      // console.log("t===>", tuesdayData);

      // console.log("e.target.name ==>", e.target.name);
      // console.log(food);
      if (food === "breakfast") {
        tuesdayData.breakfast[e.target.name] = e.target.value;
        wednesday.breakfast[e.target.name] = e.target.value;
        thursday.breakfast[e.target.name] = e.target.value;
        friday.breakfast[e.target.name] = e.target.value;
        saturday.breakfast[e.target.name] = e.target.value;
        sunday.breakfast[e.target.name] = e.target.value;
      } else if (food === "lunch") {
        tuesdayData.lunch[e.target.name] = e.target.value;
        wednesday.lunch[e.target.name] = e.target.value;
        thursday.lunch[e.target.name] = e.target.value;
        friday.lunch[e.target.name] = e.target.value;
        saturday.lunch[e.target.name] = e.target.value;
        sunday.lunch[e.target.name] = e.target.value;
      } else {
        tuesdayData.dinner[e.target.name] = e.target.value;
        wednesday.dinner[e.target.name] = e.target.value;
        thursday.dinner[e.target.name] = e.target.value;
        friday.dinner[e.target.name] = e.target.value;
        saturday.dinner[e.target.name] = e.target.value;
        sunday.dinner[e.target.name] = e.target.value;
      }
      tuesdayData["open"] = true;
      wednesday["open"] = true;
      thursday["open"] = true;
      friday["open"] = true;
      saturday["open"] = true;
      sunday["open"] = true;

      tuesdayData.breakfast["open"] = true;
      wednesday.breakfast["open"] = true;
      thursday.breakfast["open"] = true;
      friday.breakfast["open"] = true;
      saturday.breakfast["open"] = true;
      sunday.breakfast["open"] = true;

      tuesdayData.lunch["open"] = true;
      wednesday.lunch["open"] = true;
      thursday.lunch["open"] = true;
      friday.lunch["open"] = true;
      saturday.lunch["open"] = true;
      sunday.lunch["open"] = true;

      tuesdayData.dinner["open"] = true;
      wednesday.dinner["open"] = true;
      thursday.dinner["open"] = true;
      friday.dinner["open"] = true;
      saturday.dinner["open"] = true;
      sunday.dinner["open"] = true;
    }

    this.setState({
      state: state,
    });
  };

  /****************************************
   * @DESC - Seating Area CODE SELECTOR
   ****************************************/
  onSeatingAreaArraySelector = (icon) => (e) => {
    let SeatingArea = this.state.selected_seating_area;
    if (SeatingArea.length === 0) {
      SeatingArea.push(icon);
    } else {
      let isAlreadyPresent = SeatingArea.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = SeatingArea.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          SeatingArea.splice(indexOf, 1);
        }
      } else {
        SeatingArea.push(icon);
      }
    }
    this.setState({
      selected_seating_area: SeatingArea,
    });
  };

  onSelectedAreaPrimarySelector = (icon) => (e) => {
    this.setState({
      primary_seating_area: icon,
    });
  };

  /**************************
   * @DESC - OnSubmit Handler
   ***************************/
  onSubmit = (e) => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      all_seating_area_icons: this.state.all_seating_area_icons,
      selected_seating_area: this.state.selected_seating_area,
      primary_seating_area: this.state.primary_seating_area,
      part_of_chain: this.state.part_of_chain,
      selectedChains: this.state.selectedChains,
      are_you_open_24_x_7: this.state.are_you_open_24_x_7,
      multiple_opening_time: this.state.multiple_opening_time,
      table_management: this.state.table_management,
      monday: this.state.monday,
      tuesday: this.state.tuesday,
      wednesday: this.state.wednesday,
      thursday: this.state.thursday,
      friday: this.state.friday,
      saturday: this.state.saturday,
      sunday: this.state.sunday,
      total_seating_capacity: this.state.total_seating_capacity,
    };
    //console.log(formData);
    // this.props.create_restaraunttime(formData, this.pageChangeHandle(11));
    this.props.update_restaurant_timings_new(
      formData,
      this.pageChangeHandle(11)
    );
  };

  renderRestaurantTimings = () => {
    return (
      <React.Fragment>
        {/* <hr className='hr-global mt-5 mb-2' /> */}
        <div className="w-50 ml-auto mt-5 d-flex justify-content-between">
          <ButtonComponent
            buttontext="Back"
            buttontype="button"
            buttonclass="btn button-main button-white"
            onClick={this.pageChangeHandle(9)}
          />
          <ButtonComponent
            buttontext="Next"
            buttontype="button"
            buttonclass="btn button-main button-orange"
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  applyOnClick = (e) => {
    // console.log("new Data ===>", this.state.monday.open);
    if (
      this.state.monday.open === false ||
      this.state.monday.main_opening_time === "" ||
      this.state.monday.main_closing_time === ""
    ) {
      window.alert("Please Fill All Values");
    } else {
      if (
        this.state.multiple_opening_time === false ||
        this.state.allDaySet === false
      ) {
        this.setState({
          allDaySet: true,
          allDaySetNew: false,
          dayArray: [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday",
          ],
        });
      }
    }
    // else if (
    //   this.state.multiple_opening_time === true ||
    //   this.state.allDaySet === true
    // ) {
    //   this.setState({
    //     allDaySet: false,
    //     dayArray: [
    //       "monday",
    //       "tuesday",
    //       "wednesday",
    //       "thursday",
    //       "friday",
    //       "saturday",
    //       "sunday"
    //     ]
    //   });
    // }
  };

  applyOnClickOne = (e) => {
    let state = this.state;
    console.log(state);
    if (
      this.state.monday.open === false ||
      this.state.monday.breakfast.open === false ||
      this.state.monday.lunch.open === false ||
      this.state.monday.dinner.open === false ||
      this.state.monday.breakfast.opening_time === "" ||
      this.state.monday.breakfast.closing_time === ""
    ) {
      window.alert("Please Fill All Values");
    } else {
      if (
        this.state.multiple_opening_time === true ||
        this.state.allDaySetNew === false
      ) {
        this.setState({
          allDaySetNew: true,
          allDaySet: false,
          dayArray: [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday",
          ],
        });
      }
    }
  };

  onTogleChangeClick = (e) => {
    this.setState({
      [e.target.name]: e.target.checked,
    });
    this.setState({
      dayArray: ["monday"],
      foodTime: ["breakfast", "lunch", "dinner"],
      monday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      tuesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      wednesday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      thursday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      friday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      saturday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
      sunday: {
        open: false,
        main_opening_time: "",
        main_closing_time: "",
        breakfast: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        lunch: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
        dinner: {
          open: false,
          opening_time: "",
          closing_time: "",
        },
      },
    });
  };

  render() {
    // console.log( this.state );
    //console.log(this.state);
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4>Restaurant Timings</h4>
          <p>Enter the Details about the Locale of your restaurant</p>
          <hr className="hr-global" />
          <form>
            <PartOfChain
              state={this.state}
              toggleFunction={this.toggleFunction}
            />
            <Restauranttiming
              state={!isEmpty(this.state) && this.state}
              toggleFunction={this.toggleFunction}
              applyOnClick={this.applyOnClick}
              applyOnClickOne={this.applyOnClickOne}
              onTogleChangeClick={this.onTogleChangeClick}
            />
            {!this.state.multiple_opening_time &&
            !this.state.are_you_open_24_x_7 ? (
              <NoMultipleTime
                state={this.state}
                toggleFunction={this.toggleFunction}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onTimeSelectHandlerSingle={this.onTimeSelectHandlerSingle}
                onTimeSelectHandlerSingleOne={this.onTimeSelectHandlerSingleOne}
              />
            ) : null}
            {this.state.multiple_opening_time &&
            !this.state.are_you_open_24_x_7 ? (
              <YesMultipleTime
                state={this.state}
                onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
                onFoodTypeOpenClose={this.onFoodTypeOpenClose}
                onTimeSelectHandlerMultiple={this.onTimeSelectHandlerMultiple}
              />
            ) : null}
            <SeatingArrangement
              state={this.state}
              onSeatingAreaArraySelector={this.onSeatingAreaArraySelector}
              onSelectedAreaPrimarySelector={this.onSelectedAreaPrimarySelector}
              onChange={this.onChange}
              toggleFunction={this.toggleFunction}
            />
            {this.renderRestaurantTimings()}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  icons: state.icons,
  auth: state.auth,
  allRestaurentTime: state.details.restauranttime_details,
});

export default connect(mapStateToProps, {
  create_restaraunttime,
  update_restaurant_timings_new,
  get_restauranttime,
})(withRouter(RestaurantTimings));

const PartOfChain = ({ state, toggleFunction }) => {
  return (
    <>
      <div className="cuisine-main">
        <h2 className="heading-title" style={{ paddingTop: "30px" }}>
          Are you a part of chain ?
          <small
            id="emailHelp"
            className="form-text text-muted input-help-textnew"
          >
            ( Select this if you are part of a chain, if not request if the name
            is missing )
          </small>
        </h2>
        <Toggle
          name="part_of_chain"
          currentState={state.part_of_chain}
          type={"checkbox"}
          spantext1={"Yes"}
          spantext2={"No"}
          toggleclass={"toggle d-flex align-items-center mb-2"}
          toggleinputclass={"toggle__switch ml-3 mr-3"}
          onChange={toggleFunction}
          defaultChecked={false}
        />
      </div>
      {state.part_of_chain ? (
        <div className="cuisine-main mt-4">
          <table style={{ width: "100%", borderRadius: "0px" }}>
            <tbody>
              <tr>
                <td className="seleect_chain"> Select Chain </td>
                <td style={{ width: "10%" }}>
                  <ViewAllChains />
                </td>
              </tr>
            </tbody>
          </table>

          <div className="view_chainsBor">
            <AddNewChain />
          </div>
        </div>
      ) : null}
    </>
  );
};

const Restauranttiming = ({
  state,
  toggleFunction,
  applyOnClick,
  applyOnClickOne,
  onTogleChangeClick,
}) => {
  return (
    <>
      <div className="cuisine-main">
        <h2 className="heading-title" style={{ paddingTop: "30px" }}>
          Enter your restaurant timings ?
        </h2>
        <h2 className="heading-title">Are you open 24 x 7 ?</h2>
        <Toggle
          name="are_you_open_24_x_7"
          currentState={state.are_you_open_24_x_7}
          type={"checkbox"}
          spantext1={"Yes"}
          spantext2={"No"}
          toggleclass={"toggle d-flex align-items-center mb-2"}
          toggleinputclass={"toggle__switch ml-3 mr-3"}
          onChange={toggleFunction}
          defaultChecked={true}
        />

        {!state.are_you_open_24_x_7 ? (
          <div>
            <h2 className="heading-title" style={{ paddingTop: "30px" }}>
              Do you have multiple opening timings ?
            </h2>
          </div>
        ) : null}
        {!state.are_you_open_24_x_7 ? (
          <div className="d-flex justify-content-between">
            <div>
              <Toggle
                name="multiple_opening_time"
                currentState={state.multiple_opening_time}
                type={"checkbox"}
                spantext1={"Yes"}
                spantext2={"No"}
                toggleclass={"toggle d-flex align-items-center mb-2"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={onTogleChangeClick}
                defaultChecked={false}
              />
            </div>
            <div>
              {state.multiple_opening_time === false ? (
                <button
                  type="button"
                  className="btn btn-danger rounded-circle d-flex align-items-center"
                  style={{ height: "5vh" }}
                  onClick={applyOnClick}
                >
                  <i className="fa fa-plus"></i>
                </button>
              ) : (
                <button
                  type="button"
                  className="btn btn-danger rounded-circle d-flex align-items-center"
                  style={{ height: "5vh" }}
                  onClick={applyOnClickOne}
                >
                  <i className="fa fa-plus"></i>
                </button>
              )}
            </div>
          </div>
        ) : null}
      </div>
    </>
  );
};

const NoMultipleTime = ({
  state,
  toggleFunction,
  onDayOpenCloseHanlder,
  onTimeSelectHandlerSingle,
  onTimeSelectHandlerSingleOne,
}) => {
  // console.log("oall", state.dayArray);
  return (
    <>
      <div className="cuisine-main">
        {state.dayArray.map((day, index) => (
          <table key={index} style={{ width: "100%", borderRadius: "0px" }}>
            <tbody>
              <tr>
                <td>
                  <h2
                    className="heading-title text-capitalize"
                    style={{ fontSize: "15px" }}
                  >
                    {day}
                  </h2>
                  <Toggle
                    name={day}
                    currentState={state[day].open}
                    type={"checkbox"}
                    spantext1={"Yes"}
                    spantext2={"No"}
                    toggleclass={"toggle d-flex align-items-center mb-2"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={state[day].open}
                  />
                </td>
                <td style={{ width: "50%" }}>
                  {/* OPEN AND CLOSE TIME */}
                  <div className="row mt-3">
                    <h2 className="col heading-title">Opening Time</h2>
                    <h2 className="col heading-title">Closing Time</h2>
                  </div>
                  <div className="opening_time_selector">
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="main_opening_time"
                        onChange={onTimeSelectHandlerSingle(day)}
                        placeholder="24 Hours Format"
                        value={state[day].main_opening_time}
                        // value={
                        //   state.all ? state.oall : state[day].main_opening_time
                        // }
                      />
                    </div>
                    {/* <select
                      name="main_opening_time"
                      onChange={onTimeSelectHandlerSingle(day)}
                      className="Selection_box"
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select> */}
                    <div className="dasheds">-</div>
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="main_closing_time"
                        onChange={onTimeSelectHandlerSingle(day)}
                        placeholder="24 Hours Format"
                        value={state[day].main_closing_time}
                        // value={
                        //   state.all ? state.call : state[day].main_closing_time
                        // }
                      />
                    </div>
                    {/* <select
                      name="main_closing_time"
                      onChange={onTimeSelectHandlerSingle(day)}
                      className="Selection_box"
                    >
                      {timeArray.map((time, index) => (
                        <option key={index} value={time}>
                          {time}
                        </option>
                      ))}
                    </select> */}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const YesMultipleTime = ({
  state,
  onDayOpenCloseHanlder,
  onFoodTypeOpenClose,
  onTimeSelectHandlerMultiple,
}) => {
  return (
    <>
      <div className="cuisine-main">
        {state.dayArray.map((day, index) => (
          <table key={index} style={{ width: "100%" }}>
            <tbody>
              <tr>
                <td style={{ width: "18%" }}>
                  <h2
                    className="heading-title text-capitalize"
                    style={{ fontSize: "15px" }}
                  >
                    {day}
                  </h2>
                  <Switch
                    onChange={onDayOpenCloseHanlder(day, "breakfast")}
                    checked={state[day].open}
                  />
                  {/* <Toggle
                    name={day}
                    currentState={state[day].open}
                    type={"checkbox"}
                    spantext1={"Yes"}
                    spantext2={"No"}
                    toggleclass={"toggle d-flex align-items-center mb-2"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                    defaultChecked={state[day].open}
                  /> */}
                </td>
                {/* {state.foodTime.map((food, index) => ( */}
                <td
                  key={index}
                  style={{
                    width: "200px",
                    paddingLeft: "20px",
                    padding: "15px",
                  }}
                >
                  {/* OPEN AND CLOSE TIME */}
                  <h2
                    className="heading-title text-capitalize d-flex justify-content-between"
                    style={{ fontSize: "15px" }}
                  >
                    {/* {console.log(food)} */}
                    Breakfast
                    {state[day].open === true ? (
                      // <Toggle
                      //   name={day}
                      //   currentState={state[day].open}
                      //   type={"checkbox"}
                      //   spantext1={""}
                      //   spantext2={""}
                      //   toggleclass={"toggle d-flex align-items-center mb-2"}
                      //   toggleinputclass={"toggle__switch ml-3 mr-3"}
                      //   onChange={onFoodTypeOpenClose(day, "breakfast")}
                      //   defaultChecked={state[day].open === true ? true : false}
                      // />
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "breakfast")}
                        checked={state[day].breakfast.open}
                      />
                    ) : (
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "breakfast")}
                        checked={state[day].breakfast.open}
                      />
                    )}
                  </h2>
                  <div className="opening_time_selector">
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, "breakfast")}
                        placeholder="Opening Time"
                        value={state[day].breakfast.opening_time}
                      />
                    </div>
                    {/* <select
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                    <div className="dasheds">-</div>
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, "breakfast")}
                        placeholder="Closed Time"
                        value={state[day].breakfast.closing_time}
                      />
                    </div>
                    {/* <select
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                  </div>
                </td>
                <td
                  style={{
                    width: "200px",
                    paddingLeft: "20px",
                    padding: "15px",
                  }}
                >
                  {/* OPEN AND CLOSE TIME */}
                  <h2
                    className="heading-title text-capitalize d-flex justify-content-between"
                    style={{ fontSize: "15px" }}
                  >
                    {/* {console.log(food)} */}
                    Lunch
                    {/* {state[day].lunch.open === true ? (
                      <Toggle
                        name={day}
                        currentState={state[day].open}
                        type={"checkbox"}
                        spantext1={""}
                        spantext2={""}
                        toggleclass={"toggle d-flex align-items-center mb-2"}
                        toggleinputclass={"toggle__switch ml-3 mr-3"}
                        onChange={onFoodTypeOpenClose(day, "lunch")}
                        defaultChecked={state[day].open === true ? true : false}
                      />
                    ) : (
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "lunch")}
                        checked={state[day].lunch.open}
                      />
                    )} */}
                    {state[day].open === true ? (
                      // <Toggle
                      //   name={day}
                      //   currentState={state[day].open}
                      //   type={"checkbox"}
                      //   spantext1={""}
                      //   spantext2={""}
                      //   toggleclass={"toggle d-flex align-items-center mb-2"}
                      //   toggleinputclass={"toggle__switch ml-3 mr-3"}
                      //   onChange={onFoodTypeOpenClose(day, "breakfast")}
                      //   defaultChecked={state[day].open === true ? true : false}
                      // />
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "lunch")}
                        checked={state[day].lunch.open}
                      />
                    ) : (
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "lunch")}
                        checked={state[day].lunch.open}
                      />
                    )}
                  </h2>
                  <div className="opening_time_selector">
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, "lunch")}
                        placeholder="Opening Time"
                        value={state[day].lunch.opening_time}
                      />
                    </div>
                    {/* <select
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                    <div className="dasheds">-</div>
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, "lunch")}
                        placeholder="Closed Time"
                        value={state[day].lunch.closing_time}
                      />
                    </div>
                    {/* <select
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                  </div>
                </td>
                <td
                  style={{
                    width: "200px",
                    paddingLeft: "20px",
                    padding: "15px",
                  }}
                >
                  {/* OPEN AND CLOSE TIME */}
                  <h2
                    className="heading-title text-capitalize d-flex justify-content-between"
                    style={{ fontSize: "15px" }}
                  >
                    {/* {console.log(food)} */}
                    Dinner
                    {/* {state[day].dinner.open === true ? (
                      <Toggle
                        name={day}
                        currentState={state[day].open}
                        type={"checkbox"}
                        spantext1={""}
                        spantext2={""}
                        toggleclass={"toggle d-flex align-items-center mb-2"}
                        toggleinputclass={"toggle__switch ml-3 mr-3"}
                        onChange={onFoodTypeOpenClose(day, "dinner")}
                        defaultChecked={state[day].open === true ? true : false}
                      />
                    ) : (
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "dinner")}
                        checked={state[day].dinner.open}
                      />
                    )} */}
                    {state[day].open === true ? (
                      // <Toggle
                      //   name={day}
                      //   currentState={state[day].open}
                      //   type={"checkbox"}
                      //   spantext1={""}
                      //   spantext2={""}
                      //   toggleclass={"toggle d-flex align-items-center mb-2"}
                      //   toggleinputclass={"toggle__switch ml-3 mr-3"}
                      //   onChange={onFoodTypeOpenClose(day, "breakfast")}
                      //   defaultChecked={state[day].open === true ? true : false}
                      // />
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "dinner")}
                        checked={state[day].dinner.open}
                      />
                    ) : (
                      <Switch
                        onChange={onFoodTypeOpenClose(day, "dinner")}
                        checked={state[day].dinner.open}
                      />
                    )}
                  </h2>
                  <div className="opening_time_selector">
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, "dinner")}
                        placeholder="Opening Time"
                        value={state[day].dinner.opening_time}
                      />
                    </div>
                    {/* <select
                        name="opening_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                    <div className="dasheds">-</div>
                    <div className="form-group mb-0">
                      <input
                        type="text"
                        className="form-control Selection_box"
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, "dinner")}
                        placeholder="Closed Time"
                        value={state[day].dinner.closing_time}
                      />
                    </div>
                    {/* <select
                        name="closing_time"
                        onChange={onTimeSelectHandlerMultiple(day, food)}
                        className="Selection_box"
                      >
                        {timeArray.map((time, index) => (
                          <option key={index} value={time}>
                            {time}
                          </option>
                        ))}
                      </select> */}
                  </div>
                </td>
                {/* ))} */}
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const SeatingArrangement = ({
  state,
  onSeatingAreaArraySelector,
  onSelectedAreaPrimarySelector,
  onChange,
  toggleFunction,
}) => {
  // console.log( state );
  return (
    <>
      <h2 className="heading-title mt-5">
        Do you offer table management?
        <small
          id="emailHelp"
          className="form-text text-muted input-help-textnew"
        >
          ( If you use any software or option to manage your tables )
        </small>
      </h2>
      <SlidingComponent
        name="table_management"
        // value={this.state.serve_liquor}
        currentState={state.table_management}
        type={"checkbox"}
        spantext1={"Yes"}
        spantext2={"No"}
        toggleclass={"toggle d-flex align-items-center mb-2"}
        toggleinputclass={"toggle__switch ml-3 mr-3"}
        onChange={toggleFunction}
        defaultChecked={false}
      />

      <h2 className="heading-title mt-5">
        Where is your Seating Area
        <small
          id="emailHelp"
          className="form-text text-muted input-help-textnew"
        >
          ( Select all the options that apply )
        </small>
      </h2>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {state.all_seating_area_icons.map((icon, index) => (
          <IconBox
            key={index}
            src={icon.icon}
            title={icon.title}
            classsection="main-icon-button"
            onClick={onSeatingAreaArraySelector(icon)}
          />
        ))}

        <SuggestNew title="Suggest New" classsection="main-suggest-button" />
      </div>

      {/** */}
      {!isEmpty(state.selected_seating_area) ? (
        <h2 className="heading-title" style={{ paddingTop: "30px" }}>
          Which is your Primary Type?
        </h2>
      ) : null}

      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {state.selected_seating_area.map((icon, index) => (
          <IconBox
            key={index}
            src={icon.icon}
            title={icon.title}
            classsection={
              icon === state.primary_seating_area
                ? "main-icon-button_active"
                : "main-icon-button"
            }
            onClick={onSelectedAreaPrimarySelector(icon)}
          />
        ))}
      </div>
      <h2 className="heading-title" style={{ paddingTop: "30px" }}>
        What is your total seating capacity.
        <small
          id="emailHelp"
          className="form-text text-muted input-help-textnew"
        >
          ( Put the total number of seating capacity at your place )
        </small>
      </h2>
      <div>
        <input
          name="total_seating_capacity"
          type="number"
          placeholder="eg. 100"
          style={{ width: "200px" }}
          className="curve_input_field"
          value={state.total_seating_capacity}
          onChange={onChange}
        />
      </div>
    </>
  );
};
//27-03-2020 14:37 PM
