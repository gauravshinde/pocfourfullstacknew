import React, { Component } from "react";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import SuggestNew from "./../../../reusableComponents/SuggestNew";
import IconBox from "../../../reusableComponents/IconBoxButtonComponent";
import isEmpty from "../../../store/validation/is-Empty";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  create_new_service_details,
  update_service_details_new
} from "../../../store/actions/addDetailsActions";

export class ServicesDetails extends Component {
  constructor() {
    super();
    this.state = {
      errors: {},
      all_faculty_icons: [],
      all_services_icons: [],

      selected_faculty_type: [],

      selected_services_type: [],
      primary_service_type: {}
    };
  }

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      nextProps.icons.get_faculty_icons !== nextState.all_faculty_icons ||
      nextProps.icons.get_services_icons !== nextState.all_services_icons
    ) {
      return {
        all_faculty_icons: nextProps.icons.get_faculty_icons,
        all_services_icons: nextProps.icons.get_services_icons
      };
    }
    return null;
  }

  /**************************
   * @DESC - ONSUBMIT HANDLER
   ***************************/
  onSumbit = e => {
    let formData = {
      vendor_id: this.props.auth.user._id,
      selected_faculty_type: this.state.selected_faculty_type,
      selected_services_type: this.state.selected_services_type,
      primary_service_type: this.state.primary_service_type
    };
    // this.props.create_new_service_details(formData, this.pageChangeHandle(7));
    this.props.update_service_details_new(formData, this.pageChangeHandle(7));
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - FACULTY  SELECTOR
   ****************************************/
  onFacultyArraySelector = icon => e => {
    let FacultyArray = this.state.selected_faculty_type;
    if (FacultyArray.length === 0) {
      FacultyArray.push(icon);
    } else {
      let isAlreadyPresent = FacultyArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = FacultyArray.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          FacultyArray.splice(indexOf, 1);
        }
      } else {
        FacultyArray.push(icon);
      }
    }
    this.setState({
      selected_faculty_type: FacultyArray
    });
  };
  /****************************************
   * @DESC - SERVICES  SELECTOR
   ****************************************/

  onServicesArraySelector = icon => e => {
    let ServicesArray = this.state.selected_services_type;
    if (ServicesArray.length === 0) {
      ServicesArray.push(icon);
    } else {
      let isAlreadyPresent = ServicesArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = ServicesArray.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          ServicesArray.splice(indexOf, 1);
        }
      } else {
        ServicesArray.push(icon);
      }
    }
    this.setState({
      selected_services_type: ServicesArray
    });
  };

  onServicesPrimarySelector = icon => e => {
    this.setState({
      primary_service_type: icon
    });
  };

  renderServiceDetails = () => {
    // const { errors } = this.state;

    return (
      <React.Fragment>
        <div className="cuisine-main">
          {/** */}
          <h2 className="heading-title">
            Facility is part of following premises:
            <small
              id="emailHelp"
              class="form-text text-muted input-help-textnew"
            >
              ( If you are located within any building or facility select below
              )
            </small>
          </h2>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.all_faculty_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection="main-icon-button"
                onClick={this.onFacultyArraySelector(icon)}
              />
            ))}

            <SuggestNew
              title="Suggest New"
              classsection="main-suggest-button"
            />
          </div>

          {!isEmpty(this.state.selected_faculty_type) ? (
            <h2 className="heading-title" style={{ paddingTop: "30px" }}>
              Selected Premises Facility
            </h2>
          ) : null}

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.selected_faculty_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_service_type
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
              />
            ))}
          </div>

          {/** */}
          <h2 className="heading-title" style={{ paddingTop: "30px" }}>
            What Services do you provide?
            <small
              id="emailHelp"
              class="form-text text-muted input-help-textnew"
            >
              ( Select all the services you provide or wish to provide in future
              - this is information only )
            </small>
          </h2>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.all_services_icons.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection="main-icon-button"
                onClick={this.onServicesArraySelector(icon)}
              />
            ))}
          </div>
          <SuggestNew title="Suggest New" classsection="main-suggest-button" />

          {/** */}
          {!isEmpty(this.state.selected_services_type) ? (
            <h2 className="heading-title" style={{ paddingTop: "30px" }}>
              Out of the services you selected, which one is primary?
            </h2>
          ) : null}

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.selected_services_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_service_type
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
                onClick={this.onServicesPrimarySelector(icon)}
              />
            ))}
          </div>
        </div>

        {/** */}
        <div className="w-50 ml-auto d-flex justify-content-between">
          <ButtonComponent
            buttontext="Back"
            buttontype="button"
            buttonclass="btn button-main button-white"
            onClick={this.pageChangeHandle(5)}
          />
          <ButtonComponent
            buttontext="Next"
            buttontype="button"
            buttonclass="btn button-main button-orange"
            onClick={this.onSumbit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4>Services Details</h4>
          <p>Enter the information about your Restaurant.</p>
          <hr className="hr-global" />
          <form>{this.renderServiceDetails()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  icons: state.icons,
  auth: state.auth
});

export default connect(mapStateToProps, {
  create_new_service_details,
  update_service_details_new
})(withRouter(ServicesDetails));
