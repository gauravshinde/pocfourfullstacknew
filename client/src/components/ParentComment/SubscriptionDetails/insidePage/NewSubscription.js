import React, { Component } from "react";
import axios from "axios";
import Toggle from "./../../../../reusableComponents/SlidingComponent";

export class NewSubscription extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <Services
              state={this.props.state}
              onChange={this.props.onChange}
              toggleFunction={this.props.toggleFunction}
            />
          </div>
          <div className="col-12">
            <UploadMenu
              state={this.props.state}
              onImageUploadHandler={this.props.onImageUploadHandler}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default NewSubscription;

const Services = ({ state, onChange, toggleFunction }) => {
  return (
    <div className="cuisine-main">
      <div className="col-12">
        <h2 className="heading-title">
          Do you provide services for the following?
        </h2>
        <div className="row">
          <div className="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <h5 className="service-heading-text-subscription">Highchair</h5>
            <Toggle
              name="highchair"
              currentState={state.highchair}
              type={"checkbox"}
              spantext1={"Yes"}
              spantext2={"No"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={toggleFunction}
              defaultChecked={false}
            />
          </div>
          <div className="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <h5 className="service-heading-text-subscription">Handicap</h5>
            <Toggle
              name="handicap"
              currentState={state.handicap}
              type={"checkbox"}
              spantext1={"Yes"}
              spantext2={"No"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={toggleFunction}
              defaultChecked={false}
            />
          </div>
        </div>
      </div>
      <div className="col-12 mt-4">
        <h2 className="heading-title">
          Inside a hotel need room Number for Seating?
        </h2>
        <div className="col-12 p-0">
          <h5 className="service-heading-text-subscription">
            Room number on seating request?
          </h5>
          <Toggle
            name="room_number"
            currentState={state.room_number}
            type={"checkbox"}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center mb-2"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={toggleFunction}
            defaultChecked={false}
          />
        </div>
      </div>
      <div className="col-12">
        <h2 className="heading-title" style={{ paddingTop: "30px" }}>
          What services provided by Amealio would you like to subscribe to?*
        </h2>
        <div className="row">
          <div className="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
            <h5 className="service-heading-text-subscription">Service Name</h5>
            <ul type="none" className="subscription-sevice-name">
              <li>Walk-in (Default)</li>
              <li>Waitlist</li>
              <li>Reservation</li>
              <li>Curb Side</li>
              <li>Skip The Line</li>
              <li>Self Serve</li>
              <li>Takeaway</li>
            </ul>
          </div>
          <div className="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4">
            <h5 className="service-heading-text-subscription">Subscribed?</h5>
            <ul type="none" className="subscription-sevice-name">
              <li>
                <Toggle
                  name="walk_in"
                  currentState={state.walk_in}
                  type={"checkbox"}
                  spantext1={"Active"}
                  spantext2={"Inactive"}
                  toggleclass={"toggle d-flex align-items-center mb-2"}
                  toggleinputclass={"toggle__switch ml-3 mr-3"}
                  onChange={toggleFunction}
                  defaultChecked={true}
                />
              </li>
              <li>
                <Toggle
                  name="waitlist"
                  currentState={state.waitlist}
                  type={"checkbox"}
                  spantext1={"Active"}
                  spantext2={"Inactive"}
                  toggleclass={"toggle d-flex align-items-center mb-2"}
                  toggleinputclass={"toggle__switch ml-3 mr-3"}
                  onChange={toggleFunction}
                  defaultChecked={true}
                />
              </li>
              <li>
                <Toggle
                  name="reservation"
                  currentState={state.reservation}
                  type={"checkbox"}
                  spantext1={"Active"}
                  spantext2={"Inactive"}
                  toggleclass={"toggle d-flex align-items-center mb-2"}
                  toggleinputclass={"toggle__switch ml-3 mr-3"}
                  onChange={toggleFunction}
                  defaultChecked={false}
                />
              </li>
              <li>
                <Toggle
                  name="curb_side"
                  currentState={state.curb_side}
                  type={"checkbox"}
                  spantext1={"Active"}
                  spantext2={"Inactive"}
                  toggleclass={"toggle d-flex align-items-center mb-2"}
                  toggleinputclass={"toggle__switch ml-3 mr-3"}
                  onChange={toggleFunction}
                  defaultChecked={false}
                />
              </li>
              <li>
                <Toggle
                  name="skip_line"
                  currentState={state.skip_line}
                  type={"checkbox"}
                  spantext1={"Active"}
                  spantext2={"Inactive"}
                  toggleclass={"toggle d-flex align-items-center mb-2"}
                  toggleinputclass={"toggle__switch ml-3 mr-3"}
                  onChange={toggleFunction}
                  defaultChecked={false}
                />
              </li>
              <li>
                <Toggle
                  name="self_serve"
                  currentState={state.self_serve}
                  type={"checkbox"}
                  spantext1={"Active"}
                  spantext2={"Inactive"}
                  toggleclass={"toggle d-flex align-items-center mb-2"}
                  toggleinputclass={"toggle__switch ml-3 mr-3"}
                  onChange={toggleFunction}
                  defaultChecked={false}
                />
              </li>
              <li>
                <Toggle
                  name="take_away"
                  currentState={state.take_away}
                  type={"checkbox"}
                  spantext1={"Active"}
                  spantext2={"Inactive"}
                  toggleclass={"toggle d-flex align-items-center mb-2"}
                  toggleinputclass={"toggle__switch ml-3 mr-3"}
                  onChange={toggleFunction}
                  defaultChecked={false}
                />
              </li>
            </ul>
          </div>
        </div>
        <div className="col-12 p-0">
          <h4 className="mt-4">Seating and Reservation Settings</h4>
          <div className="row mb-4">
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Seating Capacity
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Total Seating capacity No. of People )
                </small>
              </h2>
              <div className="w-50 d-flex align-items-center">
                <input
                  name="seating_capacity"
                  type="number"
                  value={state.seating_capacity}
                  onChange={onChange}
                  className="curve_input_field mb-2"
                  placeholder="eg. 100"
                />
                <span
                  style={{
                    fontSize: "15px",
                    paddingLeft: "10px",
                    fontFamily: "AvenirLTStd-Black",
                    color: "#ccc"
                  }}
                >
                  pax
                </span>
              </div>
            </div>
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Reservation Capacity
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( How much of Seating capacity can be reserved )
                </small>
              </h2>
              <div className="w-50 d-flex align-items-center">
                <input
                  name="reservation_capacity"
                  type="number"
                  value={state.reservation_capacity}
                  onChange={onChange}
                  className="curve_input_field mb-2"
                  placeholder="eg. 10 %"
                />
                <span
                  style={{
                    fontSize: "15px",
                    paddingLeft: "10px",
                    fontFamily: "AvenirLTStd-Black",
                    color: "#ccc"
                  }}
                >
                  %
                </span>
              </div>
            </div>
          </div>
          <div className="row mb-4">
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Auto accept Pax Limit
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Request with pax below this will automati cally be accepted
                  for seating, any number above this threshold will go to
                  pending for action )
                </small>
              </h2>
              <div className="w-50 d-flex align-items-center">
                <input
                  name="auto_accept_pax"
                  type="number"
                  value={state.auto_accept_pax}
                  onChange={onChange}
                  className="curve_input_field mb-2"
                  placeholder="eg. 10"
                />
                <span
                  style={{
                    fontSize: "15px",
                    paddingLeft: "10px",
                    fontFamily: "AvenirLTStd-Black",
                    color: "#ccc"
                  }}
                >
                  pax
                </span>
              </div>
            </div>
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Table turn Around Time?
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew mb-4"
                >
                  ( How much time does it take to turn around tables from
                  seating to finish dining )
                </small>
              </h2>
              <div className="input-container pt-3">
                <select
                  name="table_turn_around"
                  onChange={onChange}
                  //value={state.table_turn_around}
                  // error={errors.revenue}
                  className="subscription-custom-input"
                  // className={classnames("input-field custom-inputtwo", {
                  //   invalid: errors.revenue
                  // })}
                >
                  <option value="">Select Table Around Time</option>
                  <option value="15">15 Minutes</option>
                  <option value="30">30 Minutes</option>
                  <option value="45">45 Minutes</option>
                  <option value="60">60 Minutes</option>
                  <option value="90">90 Minutes</option>
                </select>
              </div>
            </div>
          </div>
          <div className="row mb-4">
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Reservation Time Slot
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Users will be given slots in these intervals to make
                  reservation request during open hours )
                </small>
              </h2>
              <div className="input-container pt-3">
                <select
                  name="reservation_time_slot"
                  onChange={onChange}
                  // value={state.reservation_time_slot}
                  // error={errors.revenue}
                  className="subscription-custom-input"
                  // className={classnames("input-field custom-inputtwo", {
                  //   invalid: errors.revenue
                  // })}
                >
                  <option value="">Select Reservation Time</option>
                  <option value="15">15 Minutes</option>
                  <option value="30">30 Minutes</option>
                  <option value="60">60 Minutes</option>
                </select>
              </div>
            </div>
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Cut Off Time - Reservation
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Last Reservation as per the Reservation Time slot below, No
                  is at closing time )
                </small>
              </h2>
              <div className="input-container pt-3">
                <select
                  name="cut_off_time"
                  onChange={onChange}
                  // error={errors.revenue}
                  className="subscription-custom-input"
                  // className={classnames("input-field custom-inputtwo", {
                  //   invalid: errors.revenue
                  // })}
                >
                  <option value="">Select Cut Off Time</option>
                  <option value="00">0 Minutes</option>
                  <option value="15">15 Minutes</option>
                  <option value="30">30 Minutes</option>
                  <option value="60">60 Minutes</option>
                </select>
              </div>
            </div>
          </div>
          <div className="row mb-4">
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Reservation - Same Day Allowed?
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Last Reservation as per the Reservation Time slot below, No
                  is at closing time )
                </small>
              </h2>
              <Toggle
                name="sameDayAllowed"
                currentState={state.sameDayAllowed}
                type={"checkbox"}
                spantext1={"Yes"}
                spantext2={"No"}
                toggleclass={"toggle d-flex align-items-center mb-2"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={toggleFunction}
                defaultChecked={false}
              />
            </div>
            {state.sameDayAllowed === true ? (
              <div className="col-5">
                <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                  Minimum Lead Time
                  <small
                    id="emailHelp"
                    className="form-text text-muted input-help-textnew"
                  >
                    ( Minimum lead time Reservation is to be made )
                  </small>
                </h2>
                <div className="input-container pt-3">
                  <select
                    name="minimum_lead_time"
                    onChange={onChange}
                    // error={errors.revenue}
                    className="subscription-custom-input"
                    // className={classnames("input-field custom-inputtwo", {
                    //   invalid: errors.revenue
                    // })}
                  >
                    <option value="">Select Lead Time</option>
                    <option value="60">01 Hrs</option>
                    <option value="120">02 Hrs</option>
                    <option value="180">03 Hrs</option>
                    <option value="240">04 Hrs</option>
                    <option value="300">05 Hrs</option>
                  </select>
                </div>
              </div>
            ) : (
              false
            )}
          </div>
          <div className="row mb-4">
            <div className="col-5">
              <h2 className="heading-title" style={{ paddingTop: "30px" }}>
                Maximum Days ahead
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Maximum time ahead Reservation can be made )
                </small>
              </h2>
              <div className="w-50 d-flex align-items-center">
                <input
                  name="maximum_days"
                  type="number"
                  value={state.maximum_days}
                  onChange={onChange}
                  className="curve_input_field mb-2"
                  placeholder="eg. 180"
                />
                <span
                  style={{
                    fontSize: "15px",
                    paddingLeft: "10px",
                    fontFamily: "AvenirLTStd-Black",
                    color: "#ccc"
                  }}
                >
                  Days
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const UploadMenu = ({ state, onImageUploadHandler }) => {
  return (
    <>
      <div className="cuisine-main col-12">
        <h2 className="heading-title" style={{ paddingTop: "30px" }}>
          Upload Menu
          <small
            id="emailHelp"
            className="form-text text-muted input-help-textnew"
          >
            ( You can upload the menu from restaurant in JPEG, PNG, PDF or Word
            Formats )
          </small>
        </h2>
        <div
          className="view_chainsBor overflow-auto mb-5"
          style={{ width: "100%" }}
        >
          <div className="custom_file_upload">
            <input
              type="file"
              name="icon"
              id="file"
              onChange={onImageUploadHandler}
              className="custom_input_upload"
            />
            <label className="custom_input_label newChain_add" htmlFor="file">
              <div>
                <i className="fa fa-plus" style={{ color: "#CCCCCC" }}></i>
              </div>
              <div className="add_new_text">Add New</div>
            </label>
          </div>

          <div className="newChain_addthree mx-3">
            {state.uploadMenu.length > 0
              ? state.uploadMenu.map((image, index) => (
                  <img
                    key={index}
                    src={image}
                    className="newChain_addtwo"
                    style={{ height: "100%", width: "100%" }}
                    alt="chain "
                  />
                ))
              : null}
            {/* { this.state.icons ? <img src={ this.state.icons } className="newChain_add" style={{ height:'100%', width:'100%' }} /> : null } */}
          </div>
        </div>
      </div>
    </>
  );
};
