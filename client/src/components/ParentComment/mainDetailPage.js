import React, { Component } from "react";
import MainMapYesNo from "./mapComponent/MainMapYesNo";
import YesMapSetup from "./mapComponent/YesMapSetup";
import NoMapSetup from "./mapComponent/NoMapSetup";
import MainPersonOfContact from "./PersonOfContact/MainPersonOfContact";
import RestaurantDetails from "./RestaurantDetails/RestaurantDetails";
import ServicesDetails from "./ServicesDetails/ServicesDetails";
import RestaurantFeatures from "./RestaurantFeatures/RestaurantFeatures";
import CuisineFeatures from "./CuisineFeatures/CuisineFeatures";
import CuisineFeaturesTwo from "./CuisineFeatures/CuisineFeaturesTwo";
import RestaurantTimings from "./RestaurantTimings/RestaurantTimings";
import KYCBankInfo from "./KYCBankInfo/KYCBankInfo";
import SubscriptionDetails from "./SubscriptionDetails/SubscriptionDetails";
import { connect } from "react-redux";
import { getALLICONS } from "../../store/actions/iconAction";

const pageNamesList = [
  "map setup",
  "map setup yes",
  "map setup no",
  "person of contact",
  "restaurant details",
  "services details",
  "restaurant features",
  "cuisine features",
  "cuisine features two",
  "restaurant timing",
  "kyc and bank info",
  "subscripstion details",
];

export class mainDetailPage extends Component {
  constructor() {
    super();
    this.state = {
      pageNo: 1,
      pageView: 1,
      pageName: pageNamesList[0],
    };
  }

  componentDidMount() {
    this.props.getALLICONS();
  }
  pageCompletedHandler = (value) => {
    this.setState({
      pageNo: value,
    });
  };

  pageChangeButtonHandler = (value) => {
    this.setState({
      pageView: value,
      pageName: pageNamesList[value - 1],
    });
    // console.log("MAIN DETAILS PAGE:", value - 1);
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid main-details">
          <div className="row">
            <div id="style-3" className="col-9 main-details-sectionone">
              <a
                href="https://drive.google.com/file/d/1mJooQ2J-GDhpPdql0bfrsrVjnx6yLiVS/view?usp=sharing"
                target="_blank"
                rel="noopener noreferrer"
                className="position-absolute"
                style={{ top: "3%", right: "3%" }}
              >
                <img
                  src={require("../../assets/images/dashboard/helpicon.png")}
                  alt="Help Icon"
                  className="img-fluid help-icon-main-image"
                />
              </a>
              {this.state.pageView === 1 ? (
                <MainMapYesNo pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 2 ? (
                <YesMapSetup pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 3 ? (
                <NoMapSetup pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 4 ? (
                <MainPersonOfContact
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 5 ? (
                <RestaurantDetails
                  pageCompletedHandler={this.pageCompletedHandler}
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 6 ? (
                <ServicesDetails pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 7 ? (
                <RestaurantFeatures
                  pageCompletedHandler={this.pageCompletedHandler}
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 8 ? (
                <CuisineFeatures pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {this.state.pageView === 9 ? (
                <CuisineFeaturesTwo
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 10 ? (
                <RestaurantTimings pageChanger={this.pageChangeButtonHandler} />
              ) : null}
              {/* {this.state.pageView === 11 ? (
                <RestaurantTimingsTwo
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 12 ? (
                <RestaurantTimingsThree
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 13 ? (
                <RestaurantTimingsFour
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null} */}
              {this.state.pageView === 11 ? (
                <KYCBankInfo
                  pageCompletedHandler={this.pageCompletedHandler}
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
              {this.state.pageView === 12 ? (
                <SubscriptionDetails
                  pageChanger={this.pageChangeButtonHandler}
                />
              ) : null}
            </div>
            <div className="col-3 main-details-sectiontwo">
              <h5>Logo</h5>
              <div className="d-flex justify-content-around">
                <button
                  type="button"
                  className={
                    this.state.pageNo === 1
                      ? "btn button-without-round button-withRound button-background-select"
                      : "btn button-without-round"
                  }
                >
                  1
                </button>
                <button
                  type="button"
                  className={
                    this.state.pageNo === 2
                      ? "btn button-without-round button-withRound button-background-select"
                      : "btn button-without-round"
                  }
                >
                  2
                </button>
                <button
                  type="button"
                  className={
                    this.state.pageNo === 3
                      ? "btn button-without-round button-withRound button-background-select"
                      : "btn button-without-round"
                  }
                >
                  3
                </button>
                <button
                  type="button"
                  className={
                    this.state.pageNo === 4
                      ? "btn button-without-round button-withRound button-background-select"
                      : "btn button-without-round "
                  }
                >
                  4
                </button>
              </div>

              {/*==============================
                   MAP SETUP RIGHT SIDE IMAGE
             ================================*/}

              {(this.state.pageName === "map setup" ||
                this.state.pageName === "map setup yes" ||
                this.state.pageName === "map setup no") && (
                <div className="store-setup">
                  <h4>Store Setup</h4>
                  <img
                    className="img-fluid"
                    src={require("../../../src/assets/images/map.png")}
                    alt="shop"
                  />
                  <p>
                    Enter information diligently about your food establishment.
                    Users would like to know about your restaurant listed on our
                    app. Take your time and fill information carefully
                  </p>
                </div>
              )}

              {/*====================================
                  PERSON OF CONTACT RIGHT SIDE IMAGE
             =====================================*/}
              {this.state.pageName === "person of contact" && (
                <div className="store-setup">
                  <h4>Store Setup</h4>
                  <img
                    className="img-fluid person-of-contact-right-side-img"
                    src={require("../../../src/assets/images/person_of_contact_image.svg")}
                    alt="shop"
                  />
                  <p>
                    Enter information diligently about your food establishment.
                    Users would like to know about your restaurant listed on our
                    app. Take your time and fill information carefully
                  </p>
                </div>
              )}

              {/*====================================
                  RESTAURANT DETAILS RIGHT SIDE IMAGE
             =====================================*/}
              {this.state.pageName === "restaurant details" && (
                <div className="store-setup">
                  <h4>Restaurant Setup</h4>
                  <img
                    className="img-fluid restaurant-right-side-img"
                    src={require("../../../src/assets/images/restaurant_detail_image.png")}
                    alt="shop"
                  />
                  <p>
                    Please fill the details carefully, information filled here
                    Will be shown to the user while exploring about your place
                    on our app.
                  </p>
                  <p>
                    {" "}
                    In many sections you may select multiple options which will
                    be shown on the detailed page, at the same time you will
                    have to select a default value which can be shown on the
                    main page of the user app.
                  </p>
                </div>
              )}

              {/*====================================
                  SERVICES DETAILS RIGHT SIDE IMAGE
             =====================================*/}
              {this.state.pageName === "services details" && (
                <div className="store-setup">
                  <h4>Restaurant Setup</h4>
                  <img
                    className="img-fluid restaurant-right-side-img"
                    src={require("../../../src/assets/images/restaurant_detail_image.png")}
                    alt="shop"
                  />
                  <p>
                    Please fill the details carefully, information filled here
                    Will be shown to the user while exploring about your place
                    on our app.
                  </p>
                  <p>
                    {" "}
                    In many sections you may select multiple options which will
                    be shown on the detailed page, at the same time you will
                    have to select a default value which can be shown on the
                    main page of the user app.
                  </p>
                </div>
              )}

              {/*====================================
                  RESTAURANT FEATURES RIGHT SIDE IMAGE
             =====================================*/}
              {this.state.pageName === "restaurant features" && (
                <div className="store-setup">
                  <h4>Restaurant Setup</h4>
                  <img
                    className="img-fluid restaurant-right-side-img"
                    src={require("../../../src/assets/images/restaurant_detail_image.png")}
                    alt="shop"
                  />
                  <p>
                    Please fill the details carefully, information filled here
                    Will be shown to the user while exploring about your place
                    on our app.
                  </p>
                  <p>
                    {" "}
                    In many sections you may select multiple options which will
                    be shown on the detailed page, at the same time you will
                    have to select a default value which can be shown on the
                    main page of the user app.
                  </p>
                </div>
              )}

              {/*====================================
                  CUISINE FEATURES RIGHT SIDE IMAGE
             =====================================*/}
              {this.state.pageName === "cuisine features" && (
                <div className="store-setup">
                  <h4>Cuisine Setup</h4>
                  <img
                    className="img-fluid "
                    src={require("../../../src/assets/images/cuisine_feature_image.svg")}
                    alt="shop"
                  />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce convallis tellus id malesuada faucibus. Donec dolor
                    mauris.
                  </p>
                </div>
              )}

              {/*=========================================
                  CUISINE FEATURES TWO RIGHT SIDE IMAGE
             =============================================*/}
              {this.state.pageName === "cuisine features two" && (
                <div className="store-setup">
                  <h4>Cuisine Setup</h4>
                  <img
                    className="img-fluid "
                    src={require("../../../src/assets/images/cuisine_feature_image.svg")}
                    alt="shop"
                  />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce convallis tellus id malesuada faucibus. Donec dolor
                    mauris.
                  </p>
                </div>
              )}

              {/*====================================
                  RESTAURANT TIMING RIGHT SIDE IMAGE
             =========================================*/}
              {this.state.pageName === "restaurant timing" && (
                <div className="store-setup">
                  <h4>Business and Hours of Operations</h4>
                  <img
                    className="img-fluid restaurant-timing-right-side-img"
                    src={require("../../../src/assets/images/restaurant_timing-image.svg")}
                    alt="shop"
                  />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce convallis tellus id malesuada faucibus. Donec dolor
                    mauris.
                  </p>
                </div>
              )}

              {/*====================================
                  KYC RIGHT SIDE IMAGE
             ========================================*/}
              {this.state.pageName === "kyc and bank info" && (
                <div className="store-setup">
                  <h4>Bank and Other Details</h4>
                  <img
                    className="img-fluid kyc-right-side-img"
                    src={require("../../../src/assets/images/kyc_image.svg")}
                    alt="shop"
                  />
                  <p>
                    Please provide proper information for Bank Account and Other
                    Relevant Information. Also upload Copies of the document fo
                    verification and KYC
                  </p>
                </div>
              )}

              {/*=========================================
                  SUBSCRIPTION DETAILS RIGHT SIDE IMAGE
                ===========================================*/}
              {this.state.pageName === "subscripstion details" && (
                <div className="store-setup">
                  <h4>Subscription</h4>
                  <img
                    className="img-fluid"
                    src={require("../../../src/assets/images/subscription_image.svg")}
                    alt="shop"
                  />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce convallis tellus id malesuada faucibus. Donec dolor
                    mauris.
                  </p>
                </div>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(null, { getALLICONS })(mainDetailPage);
//change done new 14-04-2020 20:38pm
