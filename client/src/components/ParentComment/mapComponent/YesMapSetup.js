import React, { Component } from "react";
import InputComponent from "./../../../reusableComponents/InputComponent";
import classnames from "classnames";
import TextAreaComponent from "./../../../reusableComponents/TextAreaComponent";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import MapYesFile from "./MapYesFile";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  create_new_restaurant,
  update_restaurant_new
} from "../../../store/actions/addDetailsActions";

export class YesMapSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      place: null,
      place_id: "",
      lat: "",
      lon: "",
      restaurant_name: "",
      restaurant_description: "",
      restaurant_area: "",
      restaurant_city: "",
      restaurant_mobile_number: "",
      restaurant_email: "",
      restaurant_address: "",
      offer_active: "",
      errors: {}
    };
  }

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    if (e.target.name === "restaurant_email") {
      this.setState({
        [e.target.name]: e.target.value
      });
    } else if (e.target.name === "restaurant_description") {
      let caps = e.target.value;
      caps = caps.charAt(0).toUpperCase() + caps.slice(1);
      let dataSet = caps;
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: dataSet
      });
    } else {
      let data = e.target.value.split(" ");
      let newarray1 = [];
      for (let x = 0; x < data.length; x++) {
        newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
      }
      let newData = newarray1.join(" ");
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: newData
      });
    }
  };

  /**************************
   * @DESC - ONSUBMIT HANDLER
   ***************************/
  onSubmit = e => {
    // console.log();
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user._id,
      is_registered_with_google: true,
      lat: localStorage.getItem("latValue"),
      lon: localStorage.getItem("lngValue"),
      google_place_id: this.props.placeId,
      restaurant_name: this.state.restaurant_name,
      restaurant_description: this.state.restaurant_description,
      restaurant_area: this.state.restaurant_area,
      restaurant_city: this.state.restaurant_city,
      restaurant_mobile_number: this.state.restaurant_mobile_number,
      restaurant_email: this.state.restaurant_email,
      restaurant_address: this.state.restaurant_address,
      offer_active: this.state.offer_active
    };
    //console.log(formData);
    // this.props.create_new_restaurant(formData, this.pageChangeHandle(4));
    this.props.update_restaurant_new(formData, this.pageChangeHandle(4));
    // this.setState({
    //   vendor_id: "",
    //   is_registered_with_google: true,
    //   lat: "",
    //   lon: "",
    //   google_place_id: "",
    //   restaurant_name: "",
    //   restaurant_description: "",
    //   restaurant_area: "",
    //   restaurant_city: "",
    //   restaurant_mobile_number: "",
    //   restaurant_email: "",
    //   restaurant_address: ""
    // });
  };

  render() {
    console.log("result return here", this.props.placeId);
    const { errors } = this.state;
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4>Map Setup</h4>
          <p>
            Restaurant names on Amealio need to be written as they appear on the
            board outside the restaurant
          </p>
          <hr className="hr-global" />
          <form onSubmit={this.onSubmit}>
            <div className="row">
              <div className="col-12">
                <InputComponent
                  labeltext="Food Establishment Name"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="restaurant_name"
                  type="text"
                  place="eg. McDonalds"
                  onChange={this.onChange}
                  required={true}
                  value={this.state.restaurant_name}
                  error={errors.restaurant_name}
                  inputclass={classnames("map-inputfield", {
                    invalid: errors.restaurant_name
                  })}
                  smalltext="( Restaurant names on Amealio need to be written as they appear on the board outside the restaurant )"
                />
              </div>
              <div className="col-12">
                <TextAreaComponent
                  labeltext={"Food Establishment Description"}
                  inputlabelclass={"input-label"}
                  name={"restaurant_description"}
                  type={"text"}
                  onChange={this.onChange}
                  required={true}
                  value={this.state.restaurant_description}
                  place={"eg. A bright Atmosphere"}
                  error={errors.restaurant_description}
                  Textareaclass={classnames("form-control textarea-custom", {
                    invalid: errors.restaurant_description
                  })}
                  smalltext="( Provide details about your restaurant be elaborate min 100 char - max 5000 )"
                />
              </div>
              <div className="col-6">
                <InputComponent
                  labeltext="Area"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="restaurant_area"
                  type="text"
                  place="eg. Koregaon Park"
                  onChange={this.onChange}
                  required={true}
                  value={this.state.restaurant_area}
                  error={errors.restaurant_area}
                  inputclass={classnames("map-inputfield", {
                    invalid: errors.restaurant_area
                  })}
                />
              </div>
              <div className="col-6">
                <InputComponent
                  labeltext="City"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="restaurant_city"
                  type="text"
                  place="eg. Pune"
                  onChange={this.onChange}
                  required={true}
                  value={this.state.restaurant_city}
                  error={errors.restaurant_city}
                  inputclass={classnames("map-inputfield", {
                    invalid: errors.restaurant_city
                  })}
                />
              </div>
              <div className="col">
                <InputComponent
                  labeltext="Phone No. /Landline"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="restaurant_mobile_number"
                  type="number"
                  place="eg. 1234567890"
                  onChange={this.onChange}
                  required={true}
                  value={this.state.restaurant_mobile_number}
                  error={errors.restaurant_mobile_number}
                  inputclass={classnames("map-inputfield", {
                    invalid: errors.restaurant_mobile_number
                  })}
                />
              </div>
              <div className="col">
                <InputComponent
                  labeltext="Enquiry Email"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="restaurant_email"
                  type="email"
                  place="eg. Vendor@restaurant.in"
                  onChange={this.onChange}
                  required={true}
                  value={this.state.restaurant_email}
                  error={errors.restaurant_email}
                  inputclass={classnames("map-inputfield", {
                    invalid: errors.restaurant_email
                  })}
                />
              </div>
              <div className="col-12">
                <TextAreaComponent
                  labeltext={"Address"}
                  inputlabelclass={"input-label"}
                  name={"restaurant_address"}
                  type={"text"}
                  onChange={this.onChange}
                  required={true}
                  value={this.state.restaurant_address}
                  place={"eg. Shop No. Floor No, Area, City"}
                  error={errors.restaurant_address}
                  Textareaclass={classnames("form-control textarea-custom", {
                    invalid: errors.restaurant_address
                  })}
                  smalltext="( Provide details about your restaurant address include your area and pin code )"
                />
              </div>
              <div className="col-12">
                <p>Enter your address</p>
                <MapYesFile
                  google={this.props.google}
                  center={{ lat: 18.4819304, lng: 73.89821039999993 }}
                  height="36.5625VW"
                  zoom={15}
                />
              </div>

              <div className="w-50 ml-auto mt-4 d-flex justify-content-between">
                <ButtonComponent
                  buttontext="Back"
                  buttontype="button"
                  buttonclass="btn button-main button-white"
                  onClick={this.pageChangeHandle(1)}
                />
                <ButtonComponent
                  buttontext="Next"
                  buttontype="submit"
                  buttonclass="btn button-main button-orange"
                  // onClick={this.onSubmit}
                />
              </div>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  placeId: state.details.place_id
});

export default connect(mapStateToProps, {
  create_new_restaurant,
  update_restaurant_new
})(withRouter(YesMapSetup));

// 23-12-19
