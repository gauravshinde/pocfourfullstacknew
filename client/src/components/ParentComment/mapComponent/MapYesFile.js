import React, { Component } from "react";
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  InfoWindow,
  Marker
} from "react-google-maps";
import Geocode from "react-geocode";
import Autocomplete from "react-google-autocomplete";

import store from "./../../../store/store";
import { SET_PLACE_ID, SET_PLACE_ID_MARKER } from "./../../../store/types";

Geocode.setApiKey("AIzaSyBKaOKvpvylo_hx-icZWxmjtQB6WJAk1Hs");
Geocode.enableDebug();
// import store from './../../../pocsrcone/store/store';

class MapYesFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      place_id: "",
      address: "",
      city: "",
      area: "",
      state: "",
      mapPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng
      },
      markerPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng
      },
      Latitude: 18.4819304,
      Longitude: 73.89821039999993
    };
  }
  /**
   * Get the current address from the default map position and set those values in the state
   */

  componentDidUpdate() {}

  componentDidMount() {
    Geocode.fromLatLng(
      this.state.mapPosition.lat,
      this.state.mapPosition.lng
    ).then(
      response => {
        console.log("res", response);
        const place_id = response.results[0].place_id;
        const address = response.results[0].formatted_address,
          addressArray = response.results[0].address_components,
          city = this.getCity(addressArray),
          area = this.getArea(addressArray),
          state = this.getState(addressArray);

        console.log("city", city, area, state, place_id);

        this.setState({
          place_id: place_id ? place_id : "",
          address: address ? address : "",
          area: area ? area : "",
          city: city ? city : "",
          state: state ? state : ""
        });
      },
      error => {
        console.error(error);
      }
    );
  }
  /**
   * Component should only update ( meaning re-render ), when the user selects the address, or drags the pin
   *
   * @param nextProps
   * @param nextState
   * @return {boolean}
   */
  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.state.markerPosition.lat !== this.props.center.lat ||
      this.state.place_id !== nextState.place_id ||
      this.state.address !== nextState.address ||
      this.state.city !== nextState.city ||
      this.state.area !== nextState.area ||
      this.state.state !== nextState.state
    ) {
      return true;
    } else if (this.props.center.lat === nextProps.center.lat) {
      return false;
    }
  }
  /**
   * Get the city and set the city input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getCity = addressArray => {
    let city = "";
    for (let i = 0; i < addressArray.length; i++) {
      if (
        addressArray[i].types[0] &&
        "administrative_area_level_2" === addressArray[i].types[0]
      ) {
        city = addressArray[i].long_name;
        return city;
      }
    }
  };
  /**
   * Get the area and set the area input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getArea = addressArray => {
    let area = "";
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0]) {
        for (let j = 0; j < addressArray[i].types.length; j++) {
          if (
            "sublocality_level_1" === addressArray[i].types[j] ||
            "locality" === addressArray[i].types[j]
          ) {
            area = addressArray[i].long_name;
            return area;
          }
        }
      }
    }
  };
  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getState = addressArray => {
    let state = "";
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (
          addressArray[i].types[0] &&
          "administrative_area_level_1" === addressArray[i].types[0]
        ) {
          state = addressArray[i].long_name;
          return state;
        }
      }
    }
  };
  /**
   * And function for city,state and address input
   * @param event
   */
  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  /**
   * This Event triggers when the marker window is closed
   *
   * @param event
   */
  onInfoWindowClose = event => {};

  /**
   * When the marker is dragged you get the lat and long using the functions available from event object.
   * Use geocode to get the address, city, area and state from the lat and lng positions.
   * And then set those values in the state.
   *
   * @param event
   */
  onMarkerDragEnd = event => {
    console.log("event", event);
    let newLat = event.latLng.lat(),
      newLng = event.latLng.lng(),
      addressArray = [];
    Geocode.fromLatLng(newLat, newLng).then(
      response => {
        const place_id = response.results[0].place_id;
        const address = response.results[0].formatted_address,
          addressArray = response.results[0].address_components,
          city = this.getCity(addressArray),
          area = this.getArea(addressArray),
          state = this.getState(addressArray);
        store.dispatch({
          type: SET_PLACE_ID_MARKER,
          payload: place_id
        });
        this.setState({
          place_id: place_id ? place_id : "",
          address: address ? address : "",
          area: area ? area : "",
          city: city ? city : "",
          state: state ? state : ""
        });
      },
      error => {
        console.error(error);
      }
    );
  };

  /**
   * When the user types an address in the search box
   * @param place
   */
  onPlaceSelected = place => {
    store.dispatch({
      type: SET_PLACE_ID,
      payload: place.place_id
    });
    // console.log('place_id', placeid);
    const place_id = place.place_id;
    const Latitude = place.geometry.location.lat();
    const Longitude = place.geometry.location.lng();
    const address = place.formatted_address,
      addressArray = place.address_components,
      city = this.getCity(addressArray),
      area = this.getArea(addressArray),
      state = this.getState(addressArray),
      latValue = place.geometry.location.lat(),
      lngValue = place.geometry.location.lng();
    localStorage.setItem("latValue", place.geometry.location.lat());
    localStorage.setItem("lngValue", place.geometry.location.lng());
    // Set these values in the state.
    this.setState({
      place_id: place_id ? place_id : "",
      address: address ? address : "",
      area: area ? area : "",
      city: city ? city : "",
      state: state ? state : "",
      Latitude: Latitude ? Latitude : "",
      Longitude: Longitude ? Longitude : "",
      markerPosition: {
        lat: latValue,
        lng: lngValue
      },
      mapPosition: {
        lat: latValue,
        lng: lngValue
      }
    });
  };

  render() {
    console.log(this.state.place_id);
    const AsyncMap = withScriptjs(
      withGoogleMap(props => (
        <GoogleMap
          google={this.props.google}
          defaultZoom={this.props.zoom}
          defaultCenter={{
            lat: this.state.mapPosition.lat,
            lng: this.state.mapPosition.lng
          }}
          radius={20000}
        >
          {/* For Auto complete Search Box */}
          <div className="input-search-map-yes-file-css">
            <Autocomplete
              style={{
                border: "none",
                borderRadius: "1.5625VW",
                boxShadow: "0px 0px 0.5208333333333333VW #0000001A",
                width: "50%",
                height: "3.125VW",
                paddingLeft: "0.833vw",
                position: "relative",
                // bottom: "-48%",
                left: "30%"
              }}
              onPlaceSelected={this.onPlaceSelected}
              types={["establishment"]}
              //componentRestrictions={{ country: "IN" }}
              // types={["(regions)"]}
            />
          </div>

          <InfoWindow
            onClose={this.onInfoWindowClose}
            position={{
              lat: this.state.markerPosition.lat + 0.0018,
              lng: this.state.markerPosition.lng
            }}
          >
            <div>
              <p style={{ marginBottom: "10px" }}>
                <span style={{ color: "#ff4d2d" }}>Address: </span>
                <span
                  style={{
                    padding: "0",
                    margin: "0",
                    fontSize: "1vw"
                  }}
                >
                  {this.state.address}
                </span>
              </p>
              <p style={{ marginBottom: "10px" }}>
                <span style={{ color: "#ff4d2d" }}>PlaceId: </span>
                <span
                  style={{
                    padding: "0",
                    margin: "0",
                    fontSize: "1vw"
                  }}
                >
                  {this.state.place_id}
                </span>
              </p>
              <div className="d-flex">
                <p className="w-50" style={{ marginBottom: "10px" }}>
                  <span style={{ color: "#ff4d2d" }}>Latitude: </span>
                  <span
                    style={{
                      padding: "0",
                      margin: "0",
                      fontSize: "1vw"
                    }}
                  >
                    {this.state.Latitude}
                  </span>
                </p>
                <p className="w-50" style={{ marginBottom: "10px" }}>
                  <span style={{ color: "#ff4d2d" }}>Longitude: </span>
                  <span
                    style={{
                      padding: "0",
                      margin: "0",
                      fontSize: "1vw"
                    }}
                  >
                    {this.state.Longitude}
                  </span>
                </p>
              </div>
            </div>
          </InfoWindow>

          <Marker
            google={this.props.google}
            name={"Dolores park"}
            // draggable={true}
            onDragEnd={this.onMarkerDragEnd}
            position={{
              lat: this.state.markerPosition.lat,
              lng: this.state.markerPosition.lng
            }}
          />
          <Marker />
        </GoogleMap>
      ))
    );
    let map;
    if (this.props.center.lat !== undefined) {
      map = (
        <div>
          <AsyncMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKaOKvpvylo_hx-icZWxmjtQB6WJAk1Hs&libraries=places"
            loadingElement={
              <div style={{ height: `100%`, borderRadius: "1.5625VW" }} />
            }
            containerElement={
              <div
                style={{ height: this.props.height, borderRadius: "1.5625VW" }}
              />
            }
            mapElement={
              <div style={{ height: `100%`, borderRadius: "1.5625VW" }} />
            }
          />
          {/* <div>
            <div className='form-group'>
              <label htmlFor=''>City</label>
              <input
                type='text'
                name='city'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.city}
              />
            </div>
            <div className='form-group'>
              <label htmlFor=''>Area</label>
              <input
                type='text'
                name='area'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.area}
              />
            </div>
            <div className='form-group'>
              <label htmlFor=''>State</label>
              <input
                type='text'
                name='state'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.state}
              />
            </div>
            <div className='form-group'>
              <label htmlFor=''>Address</label>
              <input
                type='text'
                name='address'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.address}
              />
            </div>
          </div> */}
        </div>
      );
    } else {
      map = <div style={{ height: this.props.height }} />;
    }
    return map;
  }
}
export default MapYesFile;
//changes 07-11-2019 12:26am
