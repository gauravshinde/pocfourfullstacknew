import React from 'react';
import ButtonComponent from '../../../reusableComponents/ButtonComponent';

const MainMapYesNo = ({ pageChanger }) => {
  return (
    <React.Fragment>
      <div className='map-setup'>
        <h4>Map Setup</h4>
        <p>Are you present on Google Maps?</p>
        <div className='w-50 d-flex justify-content-between'>
          <ButtonComponent
            buttontext='Yes'
            buttontype='button'
            buttonclass='btn button-main button-orange'
            onClick={() => pageChanger(2)}
          />
          <ButtonComponent
            buttontext='No'
            buttontype='button'
            buttonclass='btn button-main button-white'
            onClick={() => pageChanger(3)}
          />
        </div>
        <hr className='hr-global' />
      </div>
    </React.Fragment>
  );
};

export default MainMapYesNo;
