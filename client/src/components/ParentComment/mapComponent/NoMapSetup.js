import React, { Component } from "react";
import InputComponent from "./../../../reusableComponents/InputComponent";
import classnames from "classnames";
import TextAreaComponent from "../../../reusableComponents/TextAreaComponent";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { create_new_restaurant } from "../../../store/actions/addDetailsActions";
import MapNoFile from "./MapNoFile";
import {
  update_restaurant_new,
  get_restaurant_details
} from "./../../../store/actions/addDetailsActions";
import isEmpty from "./../../../store/validation/is-Empty";

export class NoMapSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      place_id: "",
      lat: 18.4819304,
      lng: 73.89821039999993,
      restaurant_name: "",
      restaurant_description: "",
      restaurant_area: "",
      restaurant_city: "",
      restaurant_mobile_number: "",
      restaurant_email: "",
      restaurant_address: "",
      markerPosition: "",
      offer_active: "",
      errors: {}
    };
  }

  componentDidMount() {
    this.props.get_restaurant_details();
    this.setState({
      restaurant_name: this.props.details.restaurant_name,
      restaurant_description: this.props.details.restaurant_description,
      restaurant_area: this.props.details.restaurant_area,
      restaurant_city: this.props.details.restaurant_city,
      restaurant_mobile_number: this.props.details.restaurant_mobile_number,
      restaurant_email: this.props.details.restaurant_email,
      restaurant_address: this.props.details.restaurant_address,
      // lat: this.props.details.lat,
      // lng: this.props.details.lon,
      hasSetDetails: true
    });
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    console.log(nextProps.allLang);
    if (
      !isEmpty(nextProps.allLang) &&
      nextProps.allLang !== nextState.allLang
    ) {
      return {
        markerPosition: nextProps.allLang
      };
    }

    if (!isEmpty(nextProps.details) && !nextState.hasSetDetails) {
      console.log(nextProps.details);
      return {
        restaurant_name: nextProps.details.restaurant_name,
        restaurant_description: nextProps.details.restaurant_description,
        restaurant_area: nextProps.details.restaurant_area,
        restaurant_city: nextProps.details.restaurant_city,
        restaurant_mobile_number: nextProps.details.restaurant_mobile_number,
        restaurant_email: nextProps.details.restaurant_email,
        restaurant_address: nextProps.details.restaurant_address,
        lat: nextProps.details.lat,
        lng: nextProps.details.lon,
        hasSetDetails: true
      };
    }
    // if (
    //   nextProps.details.google_place_id !== nextState.place_id ||
    //   nextProps.details.lat !== nextState.lat ||
    //   nextProps.details.lon !== nextState.lng ||
    //   nextProps.details.restaurant_address !== nextState.restaurant_address ||
    //   nextProps.details.restaurant_name !== nextState.restaurant_name ||
    //   nextProps.details.restaurant_description !==
    //     nextState.restaurant_description ||
    //   nextProps.details.restaurant_area !== nextState.restaurant_area ||
    //   nextProps.details.restaurant_city !== nextState.restaurant_city ||
    //   nextProps.details.restaurant_mobile_number !==
    //     nextState.restaurant_mobile_number ||
    //   nextProps.details.restaurant_email !== nextState.restaurant_email
    // ) {
    //   return {
    //     restaurant_name: nextProps.details.restaurant_name,
    //     restaurant_description: nextProps.details.restaurant_description,
    //     restaurant_area: nextProps.details.restaurant_area,
    //     restaurant_city: nextProps.details.restaurant_city,
    //     restaurant_mobile_number: nextProps.details.restaurant_mobile_number,
    //     restaurant_email: nextProps.details.restaurant_email,
    //     restaurant_address: nextProps.details.restaurant_address,
    //     lat: nextProps.details.lat,
    //     lng: nextProps.details.lon,
    //     hasSetDetails: true
    //   };
    // }
  }

  pageChangeHandle = value => e => {
    this.props.pageChanger(value);
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    if (e.target.name === "restaurant_email") {
      this.setState({
        [e.target.name]: e.target.value
      });
    } else if (e.target.name === "restaurant_description") {
      let caps = e.target.value;
      caps = caps.charAt(0).toUpperCase() + caps.slice(1);
      let dataSet = caps;
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: dataSet
      });
    } else {
      let data = e.target.value.split(" ");
      let newarray1 = [];
      for (let x = 0; x < data.length; x++) {
        newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
      }
      let newData = newarray1.join(" ");
      this.setState({
        // [e.target.name]: e.target.value
        [e.target.name]: newData
      });
    }
  };

  /**************************
   * @DESC - ONSUBMIT HANDLER
   ***************************/
  onSubmit = e => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user._id,
      is_registered_with_google: false,
      lat: this.state.markerPosition.lat,
      lon: this.state.markerPosition.lng,
      google_place_id: this.props.placeIdMarker,
      restaurant_name: this.state.restaurant_name,
      restaurant_description: this.state.restaurant_description,
      restaurant_area: this.state.restaurant_area,
      restaurant_city: this.state.restaurant_city,
      restaurant_mobile_number: this.state.restaurant_mobile_number,
      restaurant_email: this.state.restaurant_email,
      restaurant_address: this.state.restaurant_address,
      offer_active: this.state.offer_active
    };
    //console.log(formData);
    // this.props.create_new_restaurant(formData, this.pageChangeHandle(4));
    this.props.update_restaurant_new(formData, this.pageChangeHandle(4));
  };

  renderMapEdit = () => {
    const { errors } = this.state;
    // const { lat, lng } = this.state;
    // console.log('result return here', this.props.placeId);
    // console.log('Location here', this.props.location.location);
    return (
      <React.Fragment>
        {/* <div className="col-10 mx-auto"> */}
        <form onSubmit={this.onSubmit}>
          <InputComponent
            labeltext="Food Establishment Name"
            inputlabelclass="input-label"
            imgbox="d-none"
            name="restaurant_name"
            type="text"
            place="eg. McDonalds"
            onChange={this.onChange}
            value={this.state.restaurant_name}
            required={true}
            error={errors.restaurant_name}
            inputclass={classnames("map-inputfield", {
              invalid: errors.restaurant_name
            })}
            smalltext="( Restaurant names on Amealio need to be written as they appear on the board outside the restaurant )"
          />
          <TextAreaComponent
            labeltext={"Food Establishment Description"}
            inputlabelclass={"input-label"}
            name={"restaurant_description"}
            type={"text"}
            onChange={this.onChange}
            required={true}
            value={this.state.restaurant_description}
            place={
              "eg. A bright Atmosphere, talk about your restaurant users would like to learn about It. Be elaborate about your food establishment minimum 100 characters and max about 5000 characters."
            }
            error={errors.restaurant_description}
            Textareaclass={classnames("form-control textarea-custom", {
              invalid: errors.restaurant_description
            })}
            smalltext="( Provide details about your restaurant be elaborate min 100 char - max 5000 )"
          />
          <div className="row">
            <div className="col">
              <InputComponent
                labeltext="Area"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="restaurant_area"
                type="text"
                place="eg. Koregaon Park"
                onChange={this.onChange}
                required={true}
                value={this.state.restaurant_area}
                error={errors.restaurant_area}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.restaurant_area
                })}
              />
            </div>
            <div className="col">
              <InputComponent
                labeltext="City"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="restaurant_city"
                type="text"
                place="eg. Pune"
                onChange={this.onChange}
                required={true}
                value={this.state.restaurant_city}
                error={errors.restaurant_city}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.restaurant_city
                })}
              />
            </div>
          </div>
          <div className="row">
            <div className="col">
              <InputComponent
                labeltext="Phone No. /Landline"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="restaurant_mobile_number"
                type="number"
                place="eg. 1234567890"
                onChange={this.onChange}
                required={true}
                value={this.state.restaurant_mobile_number}
                error={errors.restaurant_mobile_number}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.restaurant_mobile_number
                })}
              />
            </div>
            <div className="col">
              <InputComponent
                labeltext="Enquiry Email"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="restaurant_email"
                type="email"
                place="eg. Vendor@restaurant.in"
                onChange={this.onChange}
                value={this.state.restaurant_email}
                required={true}
                error={errors.restaurant_email}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.restaurant_email
                })}
              />
            </div>
          </div>
          <TextAreaComponent
            labeltext={"Address"}
            inputlabelclass={"input-label"}
            name={"restaurant_address"}
            type={"text"}
            onChange={this.onChange}
            required={true}
            value={this.state.restaurant_address}
            place={"eg. Shop No. Floor No, Area, City"}
            error={errors.restaurant_address}
            Textareaclass={classnames("form-control textarea-custom", {
              invalid: errors.restaurant_address
            })}
            smalltext="( Provide details about your restaurant address include your area and pin code )"
          />
          <p>Drop a pin to your address</p>
          {/* <iframe
            title='map'
            src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3784.0380526504996!2d73.89602171436819!3d18.481935475179547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c02aa3a7e783%3A0x4f9e547980873653!2sMyrl%20Tech%20Web%20Development%20Company!5e0!3m2!1sen!2sin!4v1566625734283!5m2!1sen!2sin'
          ></iframe> */}
          <MapNoFile
            google={this.props.google}
            center={{ lat: this.state.lat, lng: this.state.lng }}
            height="36.5625VW"
            zoom={15}
          />
          <div className="col-8 ml-auto mt-4 mb-4 d-flex justify-content-end">
            <ButtonComponent
              buttontext="Back"
              buttontype="button"
              buttonclass="btn button-main button-white"
              onClick={this.pageChangeHandle(1)}
            />
            <ButtonComponent
              buttontext="Next"
              buttontype="submit"
              buttonclass="btn button-main button-orange"
              // onClick={() => this.onSubmit}
            />
          </div>
        </form>
        {/* </div> */}
      </React.Fragment>
    );
  };

  render() {
    console.log(this.state.markerPosition);
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4>Map Setup</h4>
          <p>Please enter your Google Maps details.</p>
          <hr className="hr-global" />
          <form>{this.renderMapEdit()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  placeId: state.details.place_id,
  placeIdMarker: state.details.placeIdMarker,
  location: state.details.geometry,
  details: state.details.mapDetails,
  allLang: state.allevents.markerPosition
});

export default connect(mapStateToProps, {
  create_new_restaurant,
  update_restaurant_new,
  get_restaurant_details
})(withRouter(NoMapSetup));
