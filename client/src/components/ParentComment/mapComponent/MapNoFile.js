import React, { Component } from "react";
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  InfoWindow,
  Marker
} from "react-google-maps";
import Geocode from "react-geocode";
import Autocomplete from "react-google-autocomplete";

import store from "./../../../store/store";
import {
  SET_PLACE_ID,
  SET_PLACE_ID_MARKER,
  GET_MARKER_POSITION
} from "./../../../store/types";
import { get_restaurant_details } from "./../../../store/actions/addDetailsActions";
import isEmpty from "./../../../PocTwo/store/validation/is-Empty";
import { connect } from "react-redux";

Geocode.setApiKey("AIzaSyBKaOKvpvylo_hx-icZWxmjtQB6WJAk1Hs");
Geocode.enableDebug();
// import store from './../../../pocsrcone/store/store';

class MapNoFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      place_id: "",
      address: "",
      city: "",
      area: "",
      state: "",
      mapPosition: this.props.center,
      markerPosition: this.props.center
    };
  }
  /**
   * Get the current address from the default map position and set those values in the state
   */

  // componentDidMount() {
  //   this.props.get_restaurant_details();
  //   // this.setState({
  //   //   place_id: this.props.getDetails.google_place_id,
  //   //   address: this.props.getDetails.restaurant_address
  //   // });
  // }

  static getDerivedStateFromProps(nextProps, nextState) {
    console.log(nextProps.getDetails);
    if (
      !isEmpty(nextProps.allLang) &&
      nextProps.allLang !== nextState.allLang
    ) {
      return {
        markerPosition: nextProps.allLang
      };
    }
  }

  componentDidUpdate() {}

  componentDidMount() {
    this.props.get_restaurant_details();
    Geocode.fromLatLng(
      this.state.mapPosition.lat,
      this.state.mapPosition.lng
    ).then(
      response => {
        console.log("res", response);
        const place_id = response.results[0].place_id;
        const address = response.results[0].formatted_address,
          addressArray = response.results[0].address_components,
          city = this.getCity(addressArray),
          area = this.getArea(addressArray),
          state = this.getState(addressArray);

        console.log("city", city, area, state, place_id);

        this.setState({
          place_id: place_id ? place_id : "",
          address: address ? address : "",
          area: area ? area : "",
          city: city ? city : "",
          state: state ? state : ""
        });
      },
      error => {
        console.error(error);
      }
    );
  }
  /**
   * Component should only update ( meaning re-render ), when the user selects the address, or drags the pin
   *
   * @param nextProps
   * @param nextState
   * @return {boolean}
   */
  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.state.markerPosition.lat !== this.props.center.lat ||
      this.state.place_id !== nextState.place_id ||
      this.state.address !== nextState.address ||
      this.state.city !== nextState.city ||
      this.state.area !== nextState.area ||
      this.state.state !== nextState.state
    ) {
      return true;
    } else if (this.props.center.lat === nextProps.center.lat) {
      return false;
    }
  }
  /**
   * Get the city and set the city input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getCity = addressArray => {
    let city = "";
    for (let i = 0; i < addressArray.length; i++) {
      if (
        addressArray[i].types[0] &&
        "administrative_area_level_2" === addressArray[i].types[0]
      ) {
        city = addressArray[i].long_name;
        return city;
      }
    }
  };
  /**
   * Get the area and set the area input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getArea = addressArray => {
    let area = "";
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0]) {
        for (let j = 0; j < addressArray[i].types.length; j++) {
          if (
            "sublocality_level_1" === addressArray[i].types[j] ||
            "locality" === addressArray[i].types[j]
          ) {
            area = addressArray[i].long_name;
            return area;
          }
        }
      }
    }
  };
  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getState = addressArray => {
    let state = "";
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (
          addressArray[i].types[0] &&
          "administrative_area_level_1" === addressArray[i].types[0]
        ) {
          state = addressArray[i].long_name;
          return state;
        }
      }
    }
  };
  /**
   * And function for city,state and address input
   * @param event
   */
  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  /**
   * This Event triggers when the marker window is closed
   *
   * @param event
   */
  onInfoWindowClose = event => {};

  /**
   * When the marker is dragged you get the lat and long using the functions available from event object.
   * Use geocode to get the address, city, area and state from the lat and lng positions.
   * And then set those values in the state.
   *
   * @param event
   */
  onMarkerDragEnd = event => {
    console.log("event", event);
    let newLat = event.latLng.lat(),
      newLng = event.latLng.lng(),
      addressArray = [];
    Geocode.fromLatLng(newLat, newLng).then(
      response => {
        const place_id = response.results[0].place_id;
        const address = response.results[0].formatted_address,
          addressArray = response.results[0].address_components,
          city = this.getCity(addressArray),
          area = this.getArea(addressArray),
          state = this.getState(addressArray);

        const findLatLan = response.results[0].geometry.location;
        store.dispatch({
          type: GET_MARKER_POSITION,
          payload: findLatLan
        });

        store.dispatch({
          type: SET_PLACE_ID_MARKER,
          payload: place_id
        });
        this.setState({
          place_id: place_id ? place_id : "",
          address: address ? address : "",
          area: area ? area : "",
          city: city ? city : "",
          state: state ? state : ""
        });
      },
      error => {
        console.error(error);
      }
    );
  };

  /**
   * When the user types an address in the search box
   * @param place
   */
  onPlaceSelected = place => {
    console.log("place", place);
    store.dispatch({
      type: SET_PLACE_ID,
      payload: place.place_id
    });
    // console.log('place_id', placeid);
    const place_id = place.place_id;
    const address = place.formatted_address,
      addressArray = place.address_components,
      city = this.getCity(addressArray),
      area = this.getArea(addressArray),
      state = this.getState(addressArray),
      latValue = place.geometry.location.lat(),
      lngValue = place.geometry.location.lng();
    // Set these values in the state.
    this.setState({
      place_id: place_id ? place_id : "",
      address: address ? address : "",
      area: area ? area : "",
      city: city ? city : "",
      state: state ? state : "",
      markerPosition: {
        lat: latValue,
        lng: lngValue
      },
      mapPosition: {
        lat: latValue,
        lng: lngValue
      }
    });
  };

  render() {
    console.log(this.state.place_id, this.state.address);
    const AsyncMap = withScriptjs(
      withGoogleMap(props => (
        <GoogleMap
          google={this.props.google}
          defaultZoom={this.props.zoom}
          defaultCenter={{
            lat: this.state.mapPosition.lat,
            lng: this.state.mapPosition.lng
          }}
        >
          <div className="input-search-map-yes-file-css">
            {/* For Auto complete Search Box */}
            <Autocomplete
              style={{
                border: "none",
                borderRadius: "1.5625VW",
                boxShadow: "0px 0px 0.5208333333333333VW #0000001A",
                width: "50%",
                height: "3.125VW",
                paddingLeft: "16px",
                position: "relative",
                left: "30%"
              }}
              onPlaceSelected={this.onPlaceSelected}
              types={["establishment"]}
              //componentRestrictions={{ country: "IN" }}
              // types={["(regions)"]}
            />
          </div>

          <InfoWindow
            onClose={this.onInfoWindowClose}
            position={{
              lat: this.state.markerPosition.lat + 0.0018,
              lng: this.state.markerPosition.lng
            }}
          >
            <div>
              <p style={{ marginBottom: "10px" }}>
                <span style={{ color: "#ff4d2d" }}>Address: </span>
                <span
                  style={{
                    padding: "0",
                    margin: "0",
                    fontSize: "1vw"
                  }}
                >
                  {this.state.address}
                </span>
              </p>
              <div className="d-flex">
                <p className="w-50" style={{ marginBottom: "10px" }}>
                  <span style={{ color: "#ff4d2d" }}>Latitude: </span>
                  <span
                    style={{
                      padding: "0",
                      margin: "0",
                      fontSize: "1vw"
                    }}
                  >
                    {this.state.markerPosition.lat}
                  </span>
                </p>
                <p className="w-50" style={{ marginBottom: "10px" }}>
                  <span style={{ color: "#ff4d2d" }}>Longitude: </span>
                  <span
                    style={{
                      padding: "0",
                      margin: "0",
                      fontSize: "1vw"
                    }}
                  >
                    {this.state.markerPosition.lng}
                  </span>
                </p>
              </div>
              {/* <p style={{ marginBottom: "10px" }}>
                <span style={{ color: "#ff4d2d" }}>PlaceId: </span>
                <span
                  style={{
                    padding: "0",
                    margin: "0",
                    fontSize: "1vw"
                  }}
                >
                  {this.state.place_id}
                </span>
              </p> */}
            </div>
          </InfoWindow>

          <Marker
            google={this.props.google}
            name={"Dolores park"}
            draggable={true}
            onDragEnd={this.onMarkerDragEnd}
            position={{
              lat: this.state.markerPosition.lat,
              lng: this.state.markerPosition.lng
            }}
          />
          <Marker />
        </GoogleMap>
      ))
    );
    let map;
    if (this.props.center.lat !== undefined) {
      map = (
        <div>
          <AsyncMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKaOKvpvylo_hx-icZWxmjtQB6WJAk1Hs&libraries=places"
            loadingElement={
              <div style={{ height: `100%`, borderRadius: "1.5625VW" }} />
            }
            containerElement={
              <div
                style={{ height: this.props.height, borderRadius: "1.5625VW" }}
              />
            }
            mapElement={
              <div style={{ height: `100%`, borderRadius: "1.5625VW" }} />
            }
          />
          {/* <div>
            <div className='form-group'>
              <label htmlFor=''>City</label>
              <input
                type='text'
                name='city'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.city}
              />
            </div>
            <div className='form-group'>
              <label htmlFor=''>Area</label>
              <input
                type='text'
                name='area'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.area}
              />
            </div>
            <div className='form-group'>
              <label htmlFor=''>State</label>
              <input
                type='text'
                name='state'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.state}
              />
            </div>
            <div className='form-group'>
              <label htmlFor=''>Address</label>
              <input
                type='text'
                name='address'
                className='form-control'
                onChange={this.onChange}
                readOnly='readOnly'
                value={this.state.address}
              />
            </div>
          </div> */}
        </div>
      );
    } else {
      map = <div style={{ height: this.props.height }} />;
    }
    return map;
  }
}

const mapStateToProps = state => ({
  allLang: state.allevents.markerPosition,
  getDetails: state.details.mapDetails
});

export default connect(mapStateToProps, { get_restaurant_details })(MapNoFile);
