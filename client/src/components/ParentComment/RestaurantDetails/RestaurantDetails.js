import React, { Component } from "react";
import ButtonComponent from "./../../../reusableComponents/ButtonComponent";
import SuggestNew from "./../../../reusableComponents/SuggestNew";
import InputComponent from "../../../reusableComponents/InputComponent";
import classnames from "classnames";
import IconBox from "../../../reusableComponents/IconBoxButtonComponent";
import isEmpty from "../../../store/validation/is-Empty";
import axios from "axios";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getALLICONS } from "../../../store/actions/iconAction";
import {
  create_new_restraunt_type,
  update_restaurant_details_new,
  get_restaurants_type_details,
} from "../../../store/actions/addDetailsActions";
import { serverApi } from "../../../config/Keys";

export class RestaurantDetails extends Component {
  constructor() {
    super();
    this.state = {
      cost: "",
      revenue: "",
      errors: {},
      all_restaurant_type: [],
      all_dress_code: [],
      all_payment_method: [],
      selected_restaurant_type: [],
      primary_restaurant_type: {},
      selected_dress_code_type: [],
      primary_dress_code_type: {},
      selected_payment_method_type: [],
      primary_payment_method_type: {},
      restaurant_logo: "",
      restaurant_photo: [],
      hasSetDetails: true,
    };
  }

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageLogoUploadHandler = (e) => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post(`${serverApi}/image/upload-content-images`, data)
      .then((res) => {
        this.setState({ restaurant_logo: res.data.image_URL, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageUploadHandler = (e) => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post(`${serverApi}/image/upload-content-images`, data)
      .then((res) => {
        let restaurant_photo = this.state.restaurant_photo;
        restaurant_photo.push(res.data.image_URL);
        this.setState({ restaurant_photo: restaurant_photo, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  componentDidMount() {
    this.props.get_restaurants_type_details();
    this.props.getALLICONS();
    this.setState({
      cost: this.props.restaurantDetail.cost,
      revenue: this.props.restaurantDetail.revenue,
      hasSetDetails: true,
    });
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.icons);
    console.log(nextProps.restaurantDetail);
    if (
      nextProps.icons.restaurant_type_icons !== nextState.all_restaurant_type ||
      nextProps.icons.restaurant_type_icons !== nextState.all_dress_code ||
      nextProps.icons.restaurant_type_icons !== nextState.all_payment_method
    ) {
      return {
        all_restaurant_type: nextProps.icons.restaurant_type_icons,
        all_dress_code: nextProps.icons.dress_code_icons,
        all_payment_method: nextProps.icons.payment_methods_icons,
      };
    }
    if (
      nextProps.restaurantDetail !== nextState.cost ||
      nextProps.restaurantDetail !== nextState.revenue
    ) {
      return {
        cost: nextProps.restaurantDetail.cost,
        revenue: nextProps.restaurantDetail.revenue,
        hasSetDetails: true,
      };
    }
    // if (!isEmpty(nextProps.restaurantDetail) && !nextState.hasSetDetails) {
    //   return {
    //     cost: nextProps.restaurantDetail.cost,
    //     revenue: nextProps.restaurantDetail.revenue,
    //     hasSetDetails: true
    //   };
    // }
    return null;
  }

  pageChangeHandle = (value) => (e) => {
    this.props.pageChanger(value);
    if (value === 6) {
      this.props.pageCompletedHandler(2);
    }
  };

  /**************************
   * @DESC - onSUbmit HAndler
   ***************************/
  onSubmit = (e) => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user._id,
      selected_restaurant_type: this.state.selected_restaurant_type,
      primary_restaurant_type: this.state.primary_restaurant_type,
      selected_dress_code_type: this.state.selected_dress_code_type,
      primary_dress_code_type: this.state.primary_dress_code_type,
      selected_payment_method_type: this.state.selected_payment_method_type,
      primary_payment_method_type: this.state.primary_payment_method_type,
      cost: this.state.cost,
      revenue: this.state.revenue,
      restaurant_logo: this.state.restaurant_logo,
      restaurant_photo: this.state.restaurant_photo,
    };
    //console.log(formData);
    // this.props.create_new_restraunt_type(formData, this.pageChangeHandle(6));
    this.props.update_restaurant_details_new(
      formData,
      this.pageChangeHandle(6)
    );
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  /****************************************
   * @DESC - onRestaurant Type Selector
   ****************************************/
  onRestuarantTypeArraySelector = (icon) => (e) => {
    let restaurantTypeArray = this.state.selected_restaurant_type;
    if (restaurantTypeArray.length === 0) {
      restaurantTypeArray.push(icon);
    } else {
      let isAlreadyPresent = restaurantTypeArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = restaurantTypeArray.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          restaurantTypeArray.splice(indexOf, 1);
        }
      } else {
        restaurantTypeArray.push(icon);
      }
    }
    this.setState({
      selected_restaurant_type: restaurantTypeArray,
    });
  };

  onRestuarantTypePrimarySelector = (icon) => (e) => {
    this.setState({
      primary_restaurant_type: icon,
    });
  };

  /****************************************
   * @DESC - onDRESS CODE SELECTOR
   ****************************************/
  onDressCodeArraySelector = (icon) => (e) => {
    let dressCodeTypeArray = this.state.selected_dress_code_type;
    if (dressCodeTypeArray.length === 0) {
      dressCodeTypeArray.push(icon);
    } else {
      let isAlreadyPresent = dressCodeTypeArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = dressCodeTypeArray.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          dressCodeTypeArray.splice(indexOf, 1);
        }
      } else {
        dressCodeTypeArray.push(icon);
      }
    }
    this.setState({
      selected_dress_code_type: dressCodeTypeArray,
    });
  };

  onDressCodeTypePrimarySelector = (icon) => (e) => {
    this.setState({
      primary_dress_code_type: icon,
    });
  };

  /****************************************
   * @DESC - PAYMEHT MEHTOD CODE SELECTOR
   ****************************************/
  onPaymentMethodArraySelector = (icon) => (e) => {
    let paymentMethodArray = this.state.selected_payment_method_type;
    if (paymentMethodArray.length === 0) {
      paymentMethodArray.push(icon);
    } else {
      let isAlreadyPresent = paymentMethodArray.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = paymentMethodArray.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          paymentMethodArray.splice(indexOf, 1);
        }
      } else {
        paymentMethodArray.push(icon);
      }
    }
    this.setState({
      selected_payment_method_type: paymentMethodArray,
    });
  };

  onPaymentMethodPrimarySelector = (icon) => (e) => {
    this.setState({
      primary_payment_method_type: icon,
    });
  };

  renderRestaurantDetails = () => {
    const { errors } = this.state;

    return (
      <React.Fragment>
        <div className="cuisine-main">
          {/** */}
          <h2 className="heading-title">
            Restaurant Type
            <small
              id="emailHelp"
              className="form-text text-muted input-help-textnew"
            >
              ( Select the type of food establishment you are, you can select
              multiple and define one default )
            </small>
          </h2>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.all_restaurant_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection="main-icon-button"
                onClick={this.onRestuarantTypeArraySelector(icon)}
              />
            ))}

            <SuggestNew
              title="Suggest New"
              classsection="main-suggest-button"
            />
          </div>

          {/** */}
          {!isEmpty(this.state.selected_restaurant_type) ? (
            <h2 className="heading-title" style={{ paddingTop: "30px" }}>
              Which is your Primary Restaurant Type?
              <small
                id="emailHelp"
                class="form-text text-muted input-help-textnew"
              >
                ( Select only from from above selected restaurant types, if
                selected only one no further action required )
              </small>
            </h2>
          ) : null}

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.selected_restaurant_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_restaurant_type
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
                onClick={this.onRestuarantTypePrimarySelector(icon)}
              />
            ))}
          </div>

          {/** */}
          <div className="row">
            <div className="col-6" style={{ paddingTop: "30px" }}>
              <h2 className="heading-title">
                Approx. Cost for Two?
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Cost of dining at your place )
                </small>
              </h2>
              <InputComponent
                inputlabelclass="d-none"
                imgbox="img-box custom-input"
                imgsrc={require("../../../assets/images/maindetails/rupey.png")}
                imgclass="img-fluid img"
                name="cost"
                type="number"
                place="Eg. Rs 1500"
                onChange={this.onChange}
                value={this.state.cost}
                error={errors.cost}
                inputclass={classnames("input-field custom-inputtwo", {
                  invalid: errors.cost,
                })}
              />
            </div>
            <div className="col-6" style={{ paddingTop: "30px" }}>
              <h2 className="heading-title">
                Revenue (Yearly)
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Select from the drop down approx revenue range for your
                  place )
                </small>
              </h2>
              <div className="input-container">
                <div className="img-box custom-input">
                  <img
                    src={require("../../../assets/images/maindetails/rupey.png")}
                    alt="rupay"
                    className="img-fluid img"
                  />
                </div>
                <select
                  name="revenue"
                  onChange={this.onChange}
                  error={errors.revenue}
                  className={classnames("input-field custom-inputtwo", {
                    invalid: errors.revenue,
                  })}
                >
                  <option value="">Select Revenue</option>
                  <option value="0-1 Lacs">0-1 Lacs</option>
                  <option value="1-5 Lacs">1-5 Lacs</option>
                  <option value="5-10 Lacs">5-10 Lacs</option>
                  <option value="10 Lacs and above">10 Lacs and above</option>
                </select>
              </div>
            </div>
          </div>

          {/** */}
          <h2 className="heading-title mt-4">
            Dress Code
            <small
              id="emailHelp"
              className="form-text text-muted input-help-textnew"
            >
              ( if your place has dress code restrictions, you can select them
              here, if no restrictions apply then select anything )
            </small>
          </h2>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.all_dress_code.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection="main-icon-button"
                onClick={this.onDressCodeArraySelector(icon)}
              />
            ))}
          </div>
          <SuggestNew title="Suggest New" classsection="main-suggest-button" />

          {/** */}
          {!isEmpty(this.state.selected_dress_code_type) ? (
            <h2 className="heading-title" style={{ paddingTop: "30px" }}>
              What is your primary dress code?
              <small
                id="emailHelp"
                class="form-text text-muted input-help-textnew"
              >
                ( Select one of the options from above to be shown on main page,
                rest of them will be shown on detailed page )
              </small>
            </h2>
          ) : null}

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.selected_dress_code_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_dress_code_type
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
                onClick={this.onDressCodeTypePrimarySelector(icon)}
              />
            ))}
          </div>
          {/** */}
          <h2 className="heading-title" style={{ paddingTop: "30px" }}>
            Payments Methods
            <small
              id="emailHelp"
              className="form-text text-muted input-help-textnew"
            >
              ( Select the payment types accepted at your facility )
            </small>
          </h2>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.all_payment_method.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection="main-icon-button"
                onClick={this.onPaymentMethodArraySelector(icon)}
              />
            ))}
          </div>
          <SuggestNew title="Suggest New" classsection="main-suggest-button" />

          {/** */}
          {!isEmpty(this.state.selected_payment_method_type) ? (
            <h2 className="heading-title" style={{ paddingTop: "30px" }}>
              Out of the payment methods selected, which one is primary?
              <small
                id="emailHelp"
                class="form-text text-muted input-help-textnew"
              >
                ( Select one of the option from below )
              </small>
            </h2>
          ) : null}

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {this.state.selected_payment_method_type.map((icon, index) => (
              <IconBox
                key={index}
                src={icon.icon}
                title={icon.title}
                classsection={
                  icon === this.state.primary_payment_method_type
                    ? "main-icon-button_active"
                    : "main-icon-button"
                }
                onClick={this.onPaymentMethodPrimarySelector(icon)}
              />
            ))}
          </div>

          {/** */}
          <div className="row mt-4">
            <div className="col-12">
              <h2 className="heading-title mb-0">
                Restaurant Logo
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Put only one version of logo at any time )
                </small>
              </h2>
            </div>
            <div className="col-4 d-flex align-items-center">
              <small
                id="emailHelp"
                className="form-text text-muted input-help-textnew"
              >
                ( Upload a logo which is formatted In square with 200 x 200
                pixels)
              </small>
            </div>
            <div className="col-8">
              <div
                className="view_chainsBor overflow-auto mt-0"
                style={{ width: "100%" }}
              >
                <div className="custom_file_upload">
                  <input
                    type="file"
                    name="icon"
                    id="file"
                    onChange={this.onImageLogoUploadHandler}
                    className="custom_input_upload"
                  />
                  <label
                    className="custom_input_label newChain_add"
                    htmlFor="file"
                  >
                    <div>
                      <i
                        className="fa fa-plus"
                        style={{ color: "#CCCCCC" }}
                      ></i>
                    </div>
                    <div className="add_new_text">Add New</div>
                  </label>
                </div>

                <div className="newChain_addthree mx-3">
                  {this.state.restaurant_logo ? (
                    <img
                      src={this.state.restaurant_logo}
                      className="newChain_addtwo"
                      style={{ height: "100%", width: "100%" }}
                      alt="chain"
                    />
                  ) : null}
                </div>
              </div>
            </div>
          </div>

          {/** */}
          <div className="row mt-4">
            <div className="col-3">
              <h2 className="heading-title">
                Restaurant Photos
                <small
                  id="emailHelp"
                  className="form-text text-muted input-help-textnew"
                >
                  ( Upload upto 6 images )
                </small>
              </h2>
            </div>
            <div className="col-9">
              <div
                className="view_chainsBor overflow-auto"
                style={{ width: "100%" }}
              >
                <div className="custom_file_upload">
                  <input
                    type="file"
                    name="iconone"
                    id="filetwo"
                    onChange={this.onImageUploadHandler}
                    className="custom_input_upload"
                  />
                  <label
                    className="custom_input_label newChain_add"
                    htmlFor="filetwo"
                  >
                    <div>
                      <i
                        className="fa fa-plus"
                        style={{ color: "#CCCCCC" }}
                      ></i>
                    </div>
                    <div className="add_new_text">Add New</div>
                  </label>
                </div>
                <div className="newChain_addthree mx-3">
                  {this.state.restaurant_photo.length > 0
                    ? this.state.restaurant_photo.map((image, index) => (
                        <img
                          key={index}
                          src={image}
                          className="newChain_addtwo"
                          style={{ height: "100%", width: "100%" }}
                          alt="chain "
                        />
                      ))
                    : null}
                  {/* {this.state.restaurant_photo
                    ? this.state.restaurant_photo.map((pic, index) => (
                        <img
                          src={pic}
                          key={index}
                          className='newChain_add'
                          style={{ height: '100%', width: '100%' }}
                          alt='Restaurent'
                        />
                      ))
                    : null} */}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="w-50 ml-auto mt-4 d-flex justify-content-between">
          <ButtonComponent
            buttontext="Back"
            buttontype="button"
            buttonclass="btn button-main button-white"
            onClick={this.pageChangeHandle(4)}
          />
          <ButtonComponent
            buttontext="Next"
            buttontype="button"
            buttonclass="btn button-main button-orange"
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log(this.state);
    return (
      <React.Fragment>
        <div className="map-setup-yes">
          <h4>Restaurant Details</h4>
          <p>Enter the information about your Restaurant.</p>
          <hr className="hr-global" />
          <form>{this.renderRestaurantDetails()}</form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  icons: state.icons,
  auth: state.auth,
  restaurantDetail: state.details.restaurant_details,
});

export default connect(mapStateToProps, {
  getALLICONS,
  create_new_restraunt_type,
  update_restaurant_details_new,
  get_restaurants_type_details,
})(withRouter(RestaurantDetails));

//27-03-2020
