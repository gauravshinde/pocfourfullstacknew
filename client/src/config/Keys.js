export const serverName = "http://34.204.90.1:5001";

export const serverApi = "https://vendordashboard.amealio.in";

export const socketIpAddress = "https://vendordashboard.amealio.in";

//Local Server Proxy ==> http://localhost:5001/

//Main Server Proxy ==> https://vendordashboard.amealio.in/

//Testing Server Proxy ==> https://testingserverapi.amealio.in/

//Socket Io Local Ip Address ==> http://localhost:5001/

//Socket Io Main Server Ip Address ==> https://vendordashboard.amealio.in

//Socket Io Testing Server Proxy ==> https://testingserverapi.amealio.in

// "proxy": "https://testingserverapi.amealio.in/",

//03-04-2020
