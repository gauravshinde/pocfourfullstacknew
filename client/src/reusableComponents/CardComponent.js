import React from 'react';
import Card from 'react-bootstrap/Card';

const CardComponent = ({ title, src, classes, des, onClick }) => {
  return (
    <Card className={classes} onClick={onClick}>
      <Card.Body>
        <Card.Text className='text-center'>
          <img
            src={src}
            alt='restaurent'
            style={{ width: '80px', height: '70px' }}
          />

          <span className='card_title'>{title}</span>
          <span className='hr-global px-1 py-2' />
          {/* {item.des.map((des, index) => ( */}
          <span className='card_desc'>{des}</span>
          {/* ))} */}
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default CardComponent;
