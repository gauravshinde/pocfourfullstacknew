import React, { Component } from 'react';

export class QRCodePopup extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
      errors: {}
    };
  }

  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return <div></div>;
  }
}

export default QRCodePopup;
