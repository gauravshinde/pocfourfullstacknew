import React from "react";

const InputComponent = ({
  inputlabelclass,
  labeltext,
  imgbox,
  imgsrc,
  imgclass,
  name,
  value,
  type,
  place,
  onChange,
  inputclass,
  error,
  disabled,
  required,
  smalltext,
  passwordIcon = false,
  passwordShow,
  showPassword = true
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
        <small
          id="emailHelp"
          className="form-text text-muted input-help-textnew"
        >
          {smalltext}
        </small>
      </label>
      <div className="input-container">
        <div className={imgbox}>
          <img src={imgsrc} alt="icon" className={imgclass} />
        </div>
        <input
          name={name}
          value={value}
          type={showPassword ? type : "text"}
          className={inputclass}
          placeholder={place}
          onChange={onChange}
          disabled={disabled === true ? { disabled } : false}
          required={required === true ? required : ""}
        />
        {passwordIcon ? (
          <i onClick={passwordShow} className="fa fa-eye password-icon"></i>
        ) : null}
      </div>
      {error ? <div className="error">{error}</div> : null}
    </React.Fragment>
  );
};

export default InputComponent;
