import React from 'react';

const ButtonComponent = ({
  buttontext,
  buttontype,
  buttonclass,
  onClick,
  disabled
}) => {
  return (
    <React.Fragment>
      <button
        name={buttontext}
        type={buttontype}
        className={buttonclass}
        onClick={onClick}
        disabled={disabled === true ? { disabled } : false}
      >
        {buttontext}
      </button>
    </React.Fragment>
  );
};

export default ButtonComponent;
