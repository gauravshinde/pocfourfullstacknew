import React from "react";

export const SlidingComponent = ({
  name,
  toggleclass,
  type,
  toggleinputclass,
  spantext1,
  spantext2,
  currentState,
  onChange,
  defaultChecked,
  slidingonClick,
}) => {
  return (
    <React.Fragment>
      {/* <div className={toggleclass}> */}
      <span>{currentState ? <b>{spantext1}</b> : spantext1}</span>
      <input
        type={type}
        name={name}
        className={`${toggleinputclass}`}
        onChange={onChange}
        defaultChecked={defaultChecked}
        onClick={slidingonClick}
      />
      <span>{currentState ? spantext2 : <b>{spantext2}</b>}</span>
      {/* </div> */}
    </React.Fragment>
  );
};

export default SlidingComponent;
