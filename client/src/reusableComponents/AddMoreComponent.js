import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import InputComponent from "./InputComponent";
import classnames from "classnames";
import TextAreaComponent from "./TextAreaComponent";
import ButtonComponent from "./ButtonComponent";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

export class AddMoreComponent extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
      add_title: "",
      add_category: "",
      add_comment: "",
      errors: {}
    };
  }

  modalToggler = e => {
    this.setState({
      modalShow: !this.state.modalShow
    });
  };

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <div className="suggest_new_main_div" onClick={this.modalToggler}>
          <div className="circle_icons">
            <i
              className="fa fa-plus"
              style={{ color: "red" }}
              aria-hidden="true"
            ></i>
          </div>
          <span className="suggest_new_title">Add More</span>
        </div>
        <Modal
          show={this.state.modalShow}
          size="md"
          onHide={this.modalToggler}
          centered
        >
          <Modal.Body className="p-0">
            <div className="suggest-new-title">
              <h3>Add More</h3>
            </div>
            <div className="inside-body-section">
              <InputComponent
                labeltext="Title"
                inputlabelclass="input-label-suggest"
                imgbox="d-none"
                name="add_title"
                type="text"
                place="eg. Jhatka"
                onChange={this.onChange}
                value={this.state.add_title}
                error={errors.add_title}
                inputclass={classnames("map-inputfield", {
                  invalid: errors.add_title
                })}
              />
              <div class="form-group">
                <label for="category" className="input-label-suggest">
                  Category
                </label>
                <select
                  name="add_category"
                  onChange={this.onChange}
                  error={errors.add_category}
                  class={classnames("select-category ", {
                    invalid: errors.add_category
                  })}
                >
                  <option value="Category 1">Category 1</option>
                  <option value="Category 2">Category 2</option>
                  <option value="Category 3">Category 3</option>
                  <option value="Category 4">Category 4</option>
                </select>
              </div>
              <TextAreaComponent
                labeltext="Comments"
                inputlabelclass={"input-label-suggest"}
                name={"add_comment"}
                type={"text"}
                onChange={this.onChangeHandler}
                value={this.state.add_comment}
                place={"eg. A bright Atmosphere"}
                error={errors.add_comment}
                Textareaclass={classnames("form-control textarea-custom", {
                  invalid: errors.add_comment
                })}
              />
              {/** */}
              <div className="w-75 ml-auto d-flex justify-content-between">
                <ButtonComponent
                  buttontext="Cancel"
                  buttontype="button"
                  buttonclass="btn button-main button-white"
                  onClick={this.modalToggler}
                />
                <ButtonComponent
                  buttontext="Submit"
                  buttontype="submit"
                  buttonclass="btn button-main button-orange ml-3"
                  // onClick={this.pageChangeHandle(8)}
                />
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {})(withRouter(AddMoreComponent));
