import React from "react";
import { NavLink, Link } from "react-router-dom";

const HeaderComponent = () => {
  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-sm navbar-light header-main-background">
        <Link className="navbar-brand" to="/login">
          <img
            src={require("../assets/images/logoamealio.png")}
            alt="Logo Icon"
            className="img-fluid amelio_logo_imageClass"
          />
        </Link>
        <button
          className="navbar-toggler d-none" //d-lg-none
          type="button"
          data-toggle="collapse"
          data-target="#collapsibleNavId"
          aria-controls="collapsibleNavId"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="collapsibleNavId">
          <ul className="navbar-nav ml-auto">
            {/* <li className="nav-item active">
              <Link className="nav-link" to="/login">
                Login
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/signup">
                Sign Up
              </Link>
            </li> */}
            <li className="nav-item different">
              <NavLink
                className="nav-link"
                to="/login"
                activeClassName="activeRoute"
                activeStyle={{
                  color: "#000",
                  fontSize: "1.4583vw",
                  lineHeight: "1.97916vw",
                  fontFamily: "AvenirLTStd-Black",
                  fontWeight: "bold",
                }}
              >
                Login
              </NavLink>
            </li>
            <li className="nav-item different">
              <NavLink
                className="nav-link"
                to="/signup"
                activeClassName="activeRoute"
                activeStyle={{
                  color: "#000",
                  fontSize: "1.4583vw",
                  lineHeight: "1.97916vw",
                  fontFamily: "AvenirLTStd-Black",
                  fontWeight: "bold",
                }}
              >
                Sign Up
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </React.Fragment>
  );
};

export default HeaderComponent;
