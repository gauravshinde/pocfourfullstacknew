import React from 'react';

const IconBoxButtonComponent = ({ src, title, classsection, onClick }) => {
  return (
    <React.Fragment>
      <div className={classsection} onClick={onClick}>
        {src ? <img src={src} alt='icon' /> : null}
        <span className='para-data-icon'>{title}</span>
      </div>
    </React.Fragment>
  );
};

export default IconBoxButtonComponent;
