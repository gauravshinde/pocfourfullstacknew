import axios from "axios";

import {
  DINNER_LIST,
  //GET_HIGHCHAIR_HANDICAP_DETAILS,
  GET_ERRORS,
} from "../types";
import { serverApi } from "../../../config/Keys";

//get vendor data

export const getDinner = (venderId) => async (dispatch) => {
  try {
    // let allDinerList = await axios.get(`/dinner/get-each-dinner/${venderId}`);
    let allDinerList = await axios.get(
      `${serverApi}/dinner/get-each-dinner`,
      venderId
    );
    // console.log(allDinerList.data);
    if (allDinerList.data) {
      dispatch({
        type: DINNER_LIST,
        payload: allDinerList.data,
      });
      // dispatch(getDinner());
    }
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    });
  }
  // axios
  //   .get(`/vendor/dinner_list/get_all/${venderId}`)
  //   .then(res => console.log(res))
  //   .catch(err => console.log(err));

  // .then(res => console.log(res));

  // .then(res =>
  //     dispatch({
  //       type: VENDOR_LIST,
  //       payload: res.data
  //     })
};

// export const gethighchairhandicap = venderId => async dispatch => {
//   console.log(venderId);
//   try {
//     // let allDinerList = await axios.get(`/dinner/get-each-dinner/${venderId}`);
//     let all_handicap_list = await axios.post(
//       `/details/get-highchair-handicap`,
//       venderId
//     );
//     console.log(all_handicap_list.data);
//     if (all_handicap_list.data) {
//       dispatch({
//         type: GET_HIGHCHAIR_HANDICAP_DETAILS,
//         payload: all_handicap_list.data
//       });
//       // dispatch(getDinner());
//     }
//   } catch (err) {
//     dispatch({
//       type: GET_ERRORS,
//       payload: err.response.data
//     });
//   }
// };

//.get("/dinner/all_dinner")
//29-11-19

/******************************
 * send email
 *****************************/
export const send_test_email = (formData) => async (dispatch) => {
  console.log(formData);
  // dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post(`${serverApi}/dinner/email`, formData);
    if (new_poc.data) {
      console.log(new_poc.data);
    }
  } catch (err) {
    console.log(err);
    // dispatch({ type: CLEAR_LOADER });
    // dispatch(setErrors(err));
  }
};
