import axios from "axios";

import { VENDOR_DETAILS } from "../types";
import { serverApi } from "../../../config/Keys";

//get vendor data

export const getVendordetails = () => (dispatch) => {
  axios
    .get(`${serverApi}/all_vendor/:username`)
    .then((res) =>
      dispatch({
        type: VENDOR_DETAILS,
        payload: res.data,
      })
    )
    .catch((err) => console.log(err));
};
