import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

import {
  GET_ERRORS,
  SET_CURRENT_USER,
  SET_USER_EMPTY,
  CURRENT_LOGIN_VENDOR,
} from "../types";
import { serverApi } from "../../../config/Keys";

//Vendor Register User
export const registerUser = (newUser, history) => (dispatch) => {
  // console.log(newUser);
  axios
    .post(`${serverApi}/vendor/add_vendor`, newUser)
    .then((res) => history.push("/vendor-list"))
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      })
    );
};

//Vendor Register Diner
export const registerDiner = (userData, history) => (dispatch) => {
  axios
    .post(`${serverApi}/dinner/add_dinner`, userData)
    .then((res) => history.push("/vendor-seating"))
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      })
    );
};

//Super Admin Login - get user token
export const loginUser = (userData) => (dispatch) => {
  axios
    .post(`${serverApi}/vendor/login_vendor`, userData)
    .then((res) => {
      dispatch({
        type: CURRENT_LOGIN_VENDOR,
        payload: res.data,
      });
      // Save to localStorage

      // Set token to localStorage
      const { token } = res.data;
      localStorage.setItem("jwtToken", token);
      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      dispatch(setCurrentUser(decoded));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      })
    );
};

// Set logged in user
export const setCurrentUser = (decoded) => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded,
  };
};

// // User loading
// export const setUserLoading = () => {
//   return {
//     type: USER_LOADING
//   };
// };

// Log user out
export const logoutUser = (history) => (dispatch) => {
  // Remove token from local storage
  localStorage.removeItem("jwtToken");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to empty object {} which will set isAuthenticated to false
  dispatch({
    type: SET_USER_EMPTY,
  });
  history.push("/");
};
