import axios from "axios";

import { GET_NOTSEATED_STATUS, GET_ERRORS } from "../types";
import { serverApi } from "../../../config/Keys";

//Show Status
export const notSeated = () => (dispatch) => {
  axios
    .get(`${serverApi}/status/:vendorId`)
    .then((res) => {
      dispatch({
        type: GET_NOTSEATED_STATUS,
        payload: res.data,
      });
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      })
    );
};
