import axios from "axios";

import { STATUS_CHANGE } from "../types";
import { getDinner } from "./dinerAction";
import { serverApi } from "../../../config/Keys";

//get vendor data
export const getStatusChange = (statusdata) => (dispatch) => {
  //console.log(statusdata);
  axios
    .patch(`${serverApi}/dinner/status`, statusdata)
    // .then(res => console.log(res))
    .then((res) => {
      dispatch(getDinner());
    })
    .catch((err) =>
      dispatch({
        type: STATUS_CHANGE,
        payload: err.response.data,
      })
    );
};

// Give me some time will support you ok now in office

export const set_new_waitTime = async (formData) => {
  console.log(formData, "asdas");
  let set_new_wait_time = await axios.patch(
    `${serverApi}/dinner/update_waitTime`,
    formData
  );
  if (set_new_wait_time.data) {
    console.log("New Wait Time Set SuccessFulyy");
    window.location.reload();
  }
};

export const set_new_waitTime_store = (formData) => async (dispatch) => {
  console.log(formData, "asdas");
  let set_new_wait_time = await axios.patch(
    `${serverApi}/dinner/update_waitTime`,
    formData
  );
  if (set_new_wait_time.data) {
    window.alert("New Wait Time Set SuccessFully!!!!");
    console.log("New Wait Time Set SuccessFulyy");
    window.location.reload();
  }
};

//.get("/dinner/all_dinner")
