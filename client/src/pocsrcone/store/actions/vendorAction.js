import axios from "axios";

import { VENDOR_LIST } from "../types";
import { serverApi } from "../../../config/Keys";

//get vendor data

export const getVendor = (id) => (dispatch) => {
  axios
    .get(`${serverApi}/dinner/get-each-dinner/${id}`)
    .then((res) =>
      dispatch({
        type: VENDOR_LIST,
        payload: res.data,
      })
    )
    .catch((err) => console.log(err));

  // .then(res => console.log(res));

  // .then(res =>
  //     dispatch({
  //       type: VENDOR_LIST,
  //       payload: res.data
  //     })
};
