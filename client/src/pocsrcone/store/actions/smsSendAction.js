import axios from "axios";

import { GET_ERRORS } from "../types";
import { serverApi } from "../../../config/Keys";

//Show Status
export const sendWaitTimeSms = (formData) => async (dispatch) => {
  console.log(formData);
  let sendSms = await axios.post(`${serverApi}/dinner/send`, formData);
  if (sendSms.data) {
    window.alert("SMS Sent Successfully!!!");
  }
};

export const sendSms = (newSms, history) => (dispatch) => {
  axios.post(`${serverApi}/dinner/send`).then(
    (res) => console.log(res)
    //     {
    //   dispatch({
    //     type: SMS_SEND,
    //     payload: res.data
    //   });
    // }
  );
  // .catch(err =>
  //   dispatch({
  //     type: GET_ERRORS,
  //     payload: err.response.data
  //   })
  // );
};

export const getCallAction = (callingAction) => (dispatch) => {
  console.log(callingAction);
  axios
    .post(`${serverApi}/dinner/call_twilio`, callingAction)
    .then((res) => console.log(res))
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      })
    );
};
