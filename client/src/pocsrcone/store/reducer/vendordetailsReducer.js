import { VENDOR_DETAILS } from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  vendordetails: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case VENDOR_DETAILS:
      return {
        vendordetails: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
