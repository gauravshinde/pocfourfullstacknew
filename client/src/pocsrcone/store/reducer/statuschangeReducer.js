import { STATUS_CHANGE } from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  statuschange: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case STATUS_CHANGE:
      return {
        statuschange: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
