import { DEFAULT_WAIT_TIME } from "./../types";
import { LOG_OUT } from "../../../store/types";
const initialState = {
  default_wait_time: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_WAIT_TIME:
      return {
        default_wait_time: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
