import { SET_CURRENT_USER, SET_USER_EMPTY } from '../types';
// import isEmpty from './../../../store/validation/is-Empty';

const isEmpty = require('is-empty');

const initialState = {
  isAuthenticated: false,
  user: {}
  // loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    case SET_USER_EMPTY:
      return {
        ...state,
        isAuthenticated: false,
        user: {}
      };
    // case USER_LOADING:
    //   return {
    //     ...state,
    //     loading: true
    //   };
    default:
      return state;
  }
}
