import { VENDOR_LIST, CURRENT_LOGIN_VENDOR } from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  vendor: [],
  loginVendor: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case VENDOR_LIST:
      return {
        vendor: action.payload
      };
    case CURRENT_LOGIN_VENDOR:
      return {
        loginVendor: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
