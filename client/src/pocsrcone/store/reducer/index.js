import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import vendorReducer from './vendorReducer';
import dinnerReducer from './dinnerReducer';
import vendoreditReducer from './vendoreditReducer';
import notseatedReducer from './showStatusReducer';
import vendordetails from './vendordetailsReducer';
import statuschangeReducer from './statuschangeReducer';
import smsReducer from './smsReducer';
import DefaultWaitTimeReducer from './defaultWaitTimeReducer';

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  vendor: vendorReducer,
  dinner: dinnerReducer,
  vendoredit: vendoreditReducer,
  vendordetails: vendordetails,
  notseated: notseatedReducer,
  statuschange: statuschangeReducer,
  smssend: smsReducer,
  defaultWaitTime: DefaultWaitTimeReducer
});
