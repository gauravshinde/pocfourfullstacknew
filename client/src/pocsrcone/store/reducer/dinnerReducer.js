import {
  DINNER_LIST
  //GET_HIGHCHAIR_HANDICAP_DETAILS
} from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  dinner: [],
  handicap_highChair: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case DINNER_LIST:
      return {
        dinner: action.payload
      };
    // case GET_HIGHCHAIR_HANDICAP_DETAILS:
    //   console.log(action.payload);
    //   return {
    //     handicap_highChair: action.payload
    //   };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
