import { SMS_SEND } from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  smssend: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SMS_SEND:
      return {
        smssend: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
