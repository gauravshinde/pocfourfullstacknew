import { VENDOR_EDIT } from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  vendoredit: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case VENDOR_EDIT:
      return {
        vendoredit: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
