import { GET_NOTSEATED_STATUS } from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  notseated: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_NOTSEATED_STATUS:
      return {
        notseated: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
