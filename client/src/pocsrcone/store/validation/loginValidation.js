import isEmpty from "./is-empty";
import validator from "validator";

export const loginValidate = data => {
  let errors = {};

  data.username = !isEmpty(data.username) ? data.username : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  // Username

  //   if (!validator.isLength(data.username, { min: 2, max: 30 })) {
  //     errors.username = "Username Atleast 2 characters *";
  //   }

  if (validator.isEmpty(data.username)) {
    errors.username = "Username is required *";
  }

  //passord

  if (!validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "password must be between 6 and 30 characters";
  }

  if (validator.isEmpty(data.password)) {
    errors.password = "Password field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
