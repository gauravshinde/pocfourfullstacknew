import isEmpty from "./is-empty";
import validator from "validator";

export const addDinnerValidation = data => {
  let errors = {};

  data.phone_number = !isEmpty(data.phone_number) ? data.phone_number : "";
  data.name = !isEmpty(data.name) ? data.name : "";
  data.special_occassion = !isEmpty(data.special_occassion)
    ? data.special_occassion
    : "";

  // Phone Number

  if (!validator.isLength(data.phone_number, { min: 10, max: 10 })) {
    errors.phone_number = "Enter valid phone number *";
  }

  if (validator.isEmpty(data.phone_number)) {
    errors.phone_number = "Phone number is required *";
  }

  // Name

  if (validator.isEmpty(data.name)) {
    errors.name = "Name is required *";
  }

  // Username name

  if (validator.isEmpty(data.special_occassion)) {
    errors.special_occassion = "Special Ocassion is required *";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
