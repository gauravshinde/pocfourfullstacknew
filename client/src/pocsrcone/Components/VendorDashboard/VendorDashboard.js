import React, { Component } from "react";
import { Link } from "react-router-dom";

import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
// import HeaderComponent from '../Header/HeaderComponent';
// import ButtonComponent from '../SmallComponents/ButtonComponent';
import InputComponent from "../SmallComponents/InputComponent";
import DefaultWaitTime from "../VendorDetails/DefaultWaitTime";
import VendorTableHeadRow from "../VendorDetails/VendorTableHeadRow";
import VendorTableHeadSubRow from "../VendorDetails/VendorTableHeadSubRow";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import { logoutUser } from '../../store/actions/authAction';
import isEmpty from "./../../store/validation/is-empty";
import { getDinner } from "../../store/actions/dinerAction";
import {
  getStatusChange,
  set_new_waitTime,
} from "../../store/actions/statuschangeAction";
import {
  get_default_wait_time,
  update_default_wait_time,
} from "../../../pocsrcone/store/actions/defaultwaitTimeAction";
import Loader from "../Loader/Loader";
import "loaders.css/src/animations/line-scale.scss";
import dateFns from "date-fns";
// import PopUp from "./../SmallComponents/PopUp";
import { getCallAction } from "./../../store/actions/smsSendAction";
// import NewCountDown from "./InsideViewPage/NewCountDown";

import openSocket from "socket.io-client";
import io from "socket.io-client";
import { socketIpAddress } from "../../../config/Keys";

//import { subscribeData } from "./api";

const totalRecordsInOnePage = 10;

// function demoAsyncCall() {
//   return new Promise(resolve => setTimeout(() => resolve(), 3300));
// }

var xSetInterval = [];
function onStartCountDown(waitTime, index, dinner) {
  // console.log(index, "Index");
  if (waitTime) {
    let mins = waitTime;
    mins = mins - 1;
    let seconds = 59;
    if (waitTime !== 0) {
      var x = setInterval(() => {
        if (seconds === 0) {
          let formData = {
            _id: dinner._id,
            wait_time: mins,
          };
          set_new_waitTime(formData);
          mins = mins - 1;
          seconds = 59;
        }
        if (mins === 0) {
          clearInterval(x);
          clearInterval();
        }
        seconds = seconds - 1;
        const timer = mins + " : " + seconds;
        if (index) {
          document.getElementById(index).innerHTML = timer;
        }
      }, 1000);
      xSetInterval.push(x);
    }
  }
}

const SeatStatus = ["Nonseated", "Seated", "Completed", "Cancelled"];

export class VendorDashboard extends Component {
  mySocket = openSocket(`${socketIpAddress}`);
  constructor() {
    super();
    this.state = {
      viewDinerPage: {},
      dinnerId: "",
      dinnerList: {},
      vendorId: "",
      defaultWaitTime: 0,
      partySize: "All",
      show: "all",
      status: "",
      search: "",
      loading: true,
      showPopup: false,
      newWaitTimeDefault: {},
      currentPagination: 1,
    };

    // subscribeData((err, data) =>
    //   this.setState({
    //     data
    //   })
    // );
  }

  // state = {
  //   data: "Inittial Data which is not yet defined"
  // };

  onWaitTimeChange = (e) => {
    this.setState({
      inputWaitTime: e.target.value,
    });
  };

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup,
    });
  }

  componentDidMount() {
    // this simulates an async action, after which the component will render the content
    //demoAsyncCall().then(() => this.setState({ loading: false }));
    // console.log(this.props.defaultWaitTime);
    // TO get the default wait time from backend
    this.props.get_default_wait_time();
    // this.props.getDinner(this.props.auth.user._id);
    // this.props.getDinner(this.state.vendorId);

    // this.defaultWaitTime();
    // if (this.props.getStatusChange) {
    //   this.props.getStatusChange(this.state.statusdata);
    // }

    let vendorID = this.props.auth.user._id;
    let data = this.props.auth.user._id;
    this.mySocket.emit("subscribeData", vendorID);

    this.socket = io(`${socketIpAddress}`);
    // this.socket.on("getData", msg => {
    //   //console.log("Data From Get Data", msg);
    //   this.setState({ dinnerList: msg });
    // });
    this.socket.on(["getSeatingData", data], (msg) => {
      this.setState({ dinnerList: msg });
    });
    //this.socket.on("getData", this.getData);

    // this.socket.on("dinnergetData", msg => {
    //   console.log("Dinner Status Change Data ===>", msg);
    //   this.setState({ dinnerList: msg });
    // });
    // this.getSeconds();
  }

  componentDidUpdate() {
    if (this.state.vendorId) {
      //console.log("Called", this.state.dinnerList);
      if (!this.state.hasCalledDiner) {
        this.props.getDinner(this.state.vendorId);
        this.setState({ hasCalledDiner: true });
      }
    }
  }

  onSearchChange = (e) => {
    this.setState({
      search: e.target.value,
    });
  };

  componentWillUnmount() {
    xSetInterval.forEach((xval) => {
      clearInterval(xval);
      clearInterval();
    });
    this.socket.off("getData");
  }

  // static getDerivedStateFromProps(nextProps, nextState) {
  //   if (nextProps.dinner !== nextState.dinnerList) {
  //     return {
  //       dinnerList: nextProps.dinner
  //     };
  //   }
  //   return null;
  // }

  static getDerivedStateFromProps(nextprops, nextstate) {
    xSetInterval.forEach((xval) => {
      clearInterval(xval);
      clearInterval();
    });
    // console.log("Default Wait time in REducer", nextprops.defaultWaitTime);
    if (
      nextprops.defaultWaitTime.default_wait_time !== nextstate.defaultWaitTime
    ) {
      return {
        defaultWaitTime: parseInt(nextprops.defaultWaitTime.default_wait_time),
      };
    }
    if (nextprops.auth.user.id !== nextstate.vendorId) {
      return { vendorId: nextprops.auth.user.id };
    }
    // if (nextprops.dinner !== nextstate.dinnerList) {
    //   return { dinnerList: nextprops.dinner };
    // }
    // if (nextprops.dinner !== nextstate.statusdata) {
    //   return { statusdata: nextprops.dinner };
    // }
    // if (nextprops.defaultwaittimebackend !== nextstate.newWaitTimeDefault) {
    //   return {
    //     newWaitTimeDefault:
    //       nextprops.defaultwaittimebackend.defaultwaittimebackend
    //   };
    // }

    // if (nextprops.dinner) {
    //   return {
    //     dinnerList: nextprops.dinner
    //   };
    // }

    return null;
  }

  // onLogoutClick = e => {
  //   e.preventDefault();
  //   this.props.logoutUser(this.props.history);
  // };

  /*******************************
   * @DESC - DEFAULT TIME HANDLER
   ******************************/
  onDefaultTimeHandler = (value) => {
    if (value === "00") {
      this.setState({ defaultWaitTime: 0 });
      this.props.update_default_wait_time({ default_time: 0 });
    } else if (value === "5-10") {
      this.setState({ defaultWaitTime: 10 });
      this.props.update_default_wait_time({ default_time: 10 });
    } else if (value === "10-20") {
      this.setState({ defaultWaitTime: 20 });
      this.props.update_default_wait_time({ default_time: 20 });
    } else if (value === "30-45") {
      this.setState({ defaultWaitTime: 45 });
      this.props.update_default_wait_time({ default_time: 45 });
    } else if (value === "45+") {
      this.setState({ defaultWaitTime: 50 });
      this.props.update_default_wait_time({ default_time: 50 });
    }
  };

  /************************************
   * @DESC - DEFAULT PARTY SIZE HANDLER
   ************************************/
  onDefaultPartySizeHandler = (value) => {
    if (value === "All") {
      this.setState({ partySize: "All" });
    } else if (value === "8>") {
      this.setState({ partySize: "8>" });
    } else if (value === "2") {
      this.setState({ partySize: 2 });
    } else if (value === "6") {
      this.setState({ partySize: 6 });
    } else if (value === "4") {
      this.setState({ partySize: 4 });
    }
  };

  /**************************************
   * @DESC -SHOWALL || NON SEATED TOGGLER
   **************************************/
  handleShow = (value) => (e) => {
    //console.log("Clicked");
    this.setState({
      show: value,
    });
  };

  /********************************************
   * @DESC -ON SELECT STATUS CHANGE
   ********************************************/
  onSelectStatusChange = (dinner) => (e) => {
    let newMessage = e.target.value;
    e.preventDefault();
    if (window.confirm(`Are you sure you want to ${newMessage}`)) {
      const statusUpdate = {
        vendor_id: dinner.vendor_id,
        dinnerId: dinner._id,
        user_id: dinner.user_id,
        dinner_status: e.target.value,
        token: dinner.token,
        mobileNo: dinner.phone_number,
        order_type: dinner.order_type,
      };
      this.mySocket.emit("updateData", statusUpdate);
    } else {
      window.location.reload();
    }
  };

  /********************************************
   * @DESC -ON CALL ACTION
   ********************************************/
  onCallingAction = (dinner) => (e) => {
    e.preventDefault();
    const callingAction = {
      dinnerId: dinner._id,
      mobileNo: dinner.phone_number,
    };
    // console.log(callingAction);
    this.props.getCallAction(callingAction);
  };

  /*******************************************
   * @DESC - SET TIME REVERSE COUNTER
   ******************************************/
  // onStartCountDown = (waitTime, index) => {

  // };

  shouldComponentUpdate() {
    return true;
  }

  editViewDetailsHandler = (value) => (e) => {
    // e.preventDefault();
    this.props.history.push({
      pathname: "/all-seat-details",
      // search: '?query=abc',
      state: { viewDinerPage: value },
    });
    //console.log(value);
  };

  onChangePagination = (page) => {
    // console.log(page);
    this.setState({
      currentPagination: page,
    });
  };

  renderDinnerName = () => {
    const { dinnerList, currentPagination } = this.state;
    // console.log(dinnerList);

    let renderList = [];
    // let newStr = str.substring(0, str.length - 1);
    let time = "";
    let H = "";
    let h = "";
    let ampm = "";
    let newtime = "";

    // let time = dinnerList.time;
    // let H = +time.substr(0, 2);
    // let h = H % 12 || 12;
    // let ampm = H < 12 ? " AM" : " PM";
    // let newtime = h + time.substr(2, 3) + ampm;

    let walkinImage = require("./../../assets/Images/clock-circular-outline.svg");
    let waitingImage = require("./../../assets/Images/walking-img.svg");
    let reservationImage = require("./../../assets/Images/seating/online-booking.svg");

    // console.log(this.state.partySize, this.state.defaultWaitTime);
    if (this.state.partySize === "All") {
      renderList = dinnerList;
    } else if (this.state.partySize === "8>") {
      renderList = dinnerList.filter((dinner) => dinner.total >= 8);
    } else {
      renderList = dinnerList.filter(
        (dinner) => dinner.total <= this.state.partySize
      );
    }
    //console.log(renderList);
    if (this.state.show === "Nonseated") {
      renderList = renderList.filter(
        (dinner) => dinner.dinner_status === "Nonseated"
      );
    }
    if (this.state.show === "Seated") {
      renderList = renderList.filter(
        (dinner) => dinner.dinner_status === "Seated"
      );
    }
    if (this.state.show === "reservation") {
      renderList = renderList.filter(
        (dinner) => dinner.order_type === "reservation"
      );
    }

    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      renderList = renderList.filter((dinner) => {
        if (search.test(dinner.name)) {
          return dinner;
        }
      });
    }
    xSetInterval.forEach((xval) => {
      clearInterval(xval);
      clearInterval();
    });
    if (!isEmpty(renderList)) {
      return renderList.map(
        (dinner, index) =>
          index >= (currentPagination - 1) * totalRecordsInOnePage &&
          index < currentPagination * totalRecordsInOnePage &&
          // if (dinner.wait_time < 1) {
          //   waitingImage = require("./../../assets/Images/walking-img.svg");
          // }
          // (onStartCountDown(dinner.wait_time, index, dinner),
          ((time = dinner.time),
          (H = +time.substr(0, 2)),
          (h = H % 12 || 12),
          (ampm = H < 12 ? " AM" : " PM"),
          (newtime = h + time.substr(2, 3) + ampm),
          (
            <React.Fragment key={index}>
              <VendorTableHeadSubRow
                dinner={dinner}
                dinnerID={dinner._id}
                key={index}
                timerKey={index}
                no={++index}
                Name={dinner.name.replace("null", " ")}
                Phone={dinner.phone_number}
                callingActiononclick={this.onCallingAction(dinner)}
                onClickone={this.togglePopup.bind(this)}
                Img={
                  dinner.order_type === "walkin"
                    ? waitingImage
                    : dinner.order_type === "waitlist"
                    ? walkinImage
                    : dinner.order_type === "reservation"
                    ? reservationImage
                    : ""
                }
                acceptTime={
                  dinner.order_type === "walkin" ||
                  dinner.order_type === "waitlist"
                    ? dateFns.format(dinner.Request_Date, "hh : mm A")
                    : newtime
                }
                //acceptTime={dateFns.format(dinner.Request_Date, "hh : mm A")}
                acceptDate={
                  dinner.order_type === "walkin" ||
                  dinner.order_type === "waitlist"
                    ? dateFns.format(dinner.Request_Date, "DD-MM-YYYY")
                    : dinner.Date
                }
                // acceptDate={dateFns.format(dinner.Request_Date, " DD MMM YYYY")}
                imgclass={"p-0"}
                Adults={dinner.adults}
                Kids={dinner.kids}
                HighChair={dinner.highchair}
                Handicap={dinner.handicap}
                Total={dinner.total}
                Occassion={dinner}
                WaitTime={dinner.wait_time}
                onWaitTimeChange={this.onWaitTimeChange}
                inputWaitTime={this.state.inputWaitTime}
                onWaitTimeSubmit={this.onWaitTimeSubmit}
                //Timer={"00:00"}
                Timer1={dinner.wait_time}
                // Timer1={
                //   <>
                //     <button onClick={this.getSeconds(dinner)}>click</button>
                //   </>
                // }
                seatingStatus={dinner.dinner_status}
                Status={SeatStatus}
                onSelectStatusChange={() => this.onSelectStatusChange(dinner)}
                confirm={"Confirmed by User"}
                editOnclick={this.editViewDetailsHandler(dinner)}
              />
              {/* ***** Pop Up Conditions ***** */}
            </React.Fragment>
          ))
      );
      // );
    }
  };

  render() {
    // console.log("In Render ======>");
    const { dinnerList } = this.state;
    // const { loading } = this.state;
    // const socket = openSocket("http://localhost:4000");
    // socket.emit("subscribeData", 3000);
    // socket.on("subscribeData", col => {
    //   this.renderDinnerName();
    // });

    // if (loading) {
    //   return (
    //     <React.Fragment>
    //       <Loader />
    //     </React.Fragment>
    //   );
    // }

    return (
      <React.Fragment>
        {/* <HeaderComponent
          headertext={'Logo'}
          vendorDashboard={true}
          addVendorBtn={true}
          editVendorbtn={true}
          link={'/dashboard'}
          linktwo={'/add-diner'}
          linkbutton={'Add Diner'}
          // onClick={this.onLogoutClick}
          defaultWaitTime={this.state.defaultWaitTime}
        /> */}
        <div className="container-fluid vendor-details-container">
          <div className="row">
            {/* <div>
              <p>This is the Data value: {this.state.data}</p>
            </div> */}
            <div className="col-12 col-md-4 d-flex align-items-center">
              <button
                type="button"
                className={
                  this.state.show === "all"
                    ? "btn active-seating-buttons seating-buttons"
                    : "btn seating-buttons"
                }
                onClick={this.handleShow("all")}
              >
                All
              </button>
              <button
                type="button"
                className={
                  this.state.show === "Seated"
                    ? "btn active-seating-buttons seating-buttons"
                    : "btn seating-buttons"
                }
                onClick={this.handleShow("Seated")}
              >
                <img
                  src={require("../../assets/Images/waiting-room.svg")}
                  alt="waiting"
                  className="img-fluid"
                />
              </button>
              <button
                type="button"
                className={
                  this.state.show === "Nonseated"
                    ? "btn active-seating-buttons seating-buttons"
                    : "btn seating-buttons"
                }
                onClick={this.handleShow("Nonseated")}
              >
                <img
                  src={require("../../assets/Images/waiting-room-2.svg")}
                  alt="waiting-2"
                  className="img-fluid"
                />
              </button>
              <button
                type="button"
                className={
                  this.state.show === "reservation"
                    ? "btn active-seating-buttons seating-buttons"
                    : "btn seating-buttons"
                }
                onClick={this.handleShow("reservation")}
              >
                <img
                  src={require("../../assets/Images/reservation.svg")}
                  alt="Online-booking"
                  className="img-fluid"
                />
              </button>
              {/* <ButtonComponent
                buttontype={'button'}
                buttonclass={
                  this.state.show === 'all'
                    ? 'login-button login-button--vendor-details-show mr-30 mb-30'
                    : 'login-button login-button--vendor-details-show-all mr-30 mb-30'
                }
                buttontext={'Show All'}
                handleOnClick={this.handleShow('all')}
              />
              <ButtonComponent
                buttontype={'button'}
                buttonclass={
                  this.state.show === 'Seated'
                    ? 'login-button login-button--vendor-details-show mr-30 mb-30'
                    : 'login-button login-button--vendor-details-show-all mr-30 mb-30'
                }
                buttontext={'Seated'}
                handleOnClick={this.handleShow('Seated')}
              />
              <ButtonComponent
                buttontype={'button'}
                buttonclass={
                  this.state.show === 'Nonseated'
                    ? 'login-button login-button--vendor-details-show mr-30 mb-30'
                    : 'login-button login-button--vendor-details-show-all mr-30 mb-30'
                }
                buttontext={'Show Non Seated'}
                handleOnClick={this.handleShow('Nonseated')}
              />
              <ButtonComponent
                buttontype={'button'}
                buttonclass={
                  this.state.show === 'Reservation'
                    ? 'login-button login-button--vendor-details-show mr-30 mb-30'
                    : 'login-button login-button--vendor-details-show-all mr-30 mb-30'
                }
                buttontext={'Reservation'}
                handleOnClick={this.handleShow('Reservation')}
              /> */}
            </div>

            <div className="col-12 col-md-8">
              <div className="table-time-outer-block">
                <div>
                  <h3 className="font-28-medium">
                    Default Wait Time{" "}
                    <span className="font-21-medium dark-gray-text">
                      &#40;Mins&#41;
                    </span>
                  </h3>
                  <DefaultWaitTime
                    arrayNumber={["00", "5-10", "10-20", "30-45", "45+"]}
                    onClick={this.onDefaultTimeHandler}
                    currentWaitTime={this.state.defaultWaitTime}
                  />
                </div>
                <div>
                  <h3 className="font-28-medium">Filter By Party Size </h3>

                  <DefaultWaitTime
                    arrayNumber={["2", "4", "6", "8>", "All"]}
                    onClick={this.onDefaultPartySizeHandler}
                    partySize={this.state.partySize}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid all-page-container-padding table-container ">
          <div className="col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center">
            <div>
              <h4 className={"vendor"}>
                {this.state.show === "all" ? "All Seats" : ""}
                {this.state.show === "Seated" ? "View All Seated" : ""}
                {this.state.show === "Nonseated" ? "View All Nonseated" : ""}
                {this.state.show === "Reservation"
                  ? "View All Reservation"
                  : ""}
              </h4>
            </div>

            <div className="d-flex justify-content-between">
              <Link
                to={{
                  pathname: "/add-diner",
                  state: {
                    defaultWaitTime: this.state.defaultWaitTime,
                  },
                }}
                className="btn button-width button-gradient d-flex justify-content-center align-items-center"
              >
                Add Dinner
              </Link>
              {/*w-25*/}
              <InputComponent
                img={require("./../../assets/Images/search.svg")}
                searchClass={"search-label"}
                onChange={this.onSearchChange}
                value={this.state.search}
                place={"search"}
                inputclass={
                  "form-control input-bottomblack mb-0 pl-3 input-search"
                }
              />
              {/* <img
                src={require("./../../assets/Images/sort-button-with-three-lines.svg")}
                alt="sort-button-with-three-lines"
              />
              <img
                src={require("./../../assets/Images/filter.svg")}
                alt="filter-img"
              /> */}
            </div>
          </div>
          <div className="seating-container text-left">
            <div className="table-responsive seating-table">
              {" "}
              {/**vendordetail-col  */}
              <table className="table table-striped mb-0">
                <thead>
                  <VendorTableHeadRow
                    no={"#"}
                    Name={"Details"}
                    OrderType={"Order Type"}
                    PartySize={"Party Size"}
                    WaitTime={"WaitTime"}
                    Details={"Details"}
                    Status={"Status"}
                    Blank={""}
                    Blankone={""}
                  />
                </thead>

                <tbody>{this.renderDinnerName()}</tbody>
              </table>
            </div>
          </div>
          <div className="p4v2-menu-pagination">
            <Pagination
              onChange={this.onChangePagination}
              current={this.state.currentPagination}
              defaultPageSize={totalRecordsInOnePage}
              total={this.state.dinnerList.length}
              showTitle={false}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorDashboard.propTypes = {
  // logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  dinner: PropTypes.array.isRequired,
  getDinner: PropTypes.func.isRequired,
  getStatusChange: PropTypes.func.isRequired,
  defaultWaitTime: PropTypes.object.isRequired,
  //newDefaultWaitTime: PropTypes.func.isRequired,
  vendor: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  dinner: state.dinner.dinner,
  vendor: state.vendor,
  defaultWaitTime: state.defaultWaitTime,
});

export default connect(mapStateToProps, {
  // logoutUser,
  getDinner,
  getStatusChange,
  set_new_waitTime,
  get_default_wait_time,
  update_default_wait_time,
  getCallAction,
})(withRouter(VendorDashboard));
//30-03-2020 07:14PM
