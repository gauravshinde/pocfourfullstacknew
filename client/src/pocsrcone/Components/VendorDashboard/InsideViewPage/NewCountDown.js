import React, { Component } from "react";
import Timer from "react-compound-timer";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
// import Moment from "moment";

export class NewCountDown extends Component {
  constructor() {
    super();
    this.state = {
      defaultData: ""
    };
  }

  componentDidMount() {
    this.setState({
      defaultData: this.props.waitNew
    });
  }

  render() {
    return (
      <Timer initialTime={this.props.waitNew()} direction="backward">
        {() => (
          <React.Fragment>
            {/* <Timer.Days /> days */}
            <Timer.Hours /> h :
            <Timer.Minutes /> m :
            <Timer.Seconds /> s{/* <Timer.Milliseconds /> milliseconds */}
          </React.Fragment>
        )}
      </Timer>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps, null)(withRouter(NewCountDown));

// import React from "react";
// const NewCountDown = ({ value }) => {
//   return (
//     <Timer initialTime={value} direction="backward">
//       {() => (
//         <React.Fragment>
//           {/* <Timer.Days /> days */}
//           <Timer.Hours /> h :
//           <Timer.Minutes /> m :
//           <Timer.Seconds /> s{/* <Timer.Milliseconds /> milliseconds */}
//         </React.Fragment>
//       )}
//     </Timer>
//   );
// };
// export default NewCountDown;
// //19-02-2020 06:46pm
