import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { set_new_waitTime_store } from "../../../pocsrcone/store/actions/statuschangeAction";
import buttonimg from "../../assets/Images/penimg.svg";
// import Moment from "moment";

import openSocket from "socket.io-client";
import io from "socket.io-client";
import { socketIpAddress } from "../../../config/Keys";
import Alert from "react-s-alert";

export class EditWaitTime extends Component {
  mySocket = openSocket(`${socketIpAddress}`);
  constructor() {
    super();
    this.state = {
      inputTime: "",
      // phone_number: ""
    };
  }

  componentDidMount() {
    // this.setState({
    //   inputTime: {this.props.dinner.wait_time ? Moment(this.props.dinner.wait_time, "DD/MM/YYYY hh:mm:ss A").diff(
    //     Moment(new Date(), "DD/MM/YYYY hh:mm:ss A")) : "0"}
    //   // inputTime: this.props.dinner.wait_time ? this.props.dinner.wait_time : 0
    //   // phone_number: this.props.dinner.phone_number
    //   //   ? this.props.dinner.phone_number
    //   //   : this.props.dinner.phone_number
    // });
    this.socket = io(`${socketIpAddress}`);
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    if (this.state.inputTime === "") {
      Alert.error("<h4>Please Enter Wait Time</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 2500,
        offset: 35,
      });
    } else {
      let formData = {
        _id: this.props.dinner._id,
        user_id: this.props.dinner.user_id,
        wait_time: this.state.inputTime,
        phone_number: this.props.dinner.phone_number,
        order_type: this.props.dinner.order_type,
        token: this.props.dinner.token,
      };
      // console.log(formData);
      // this.props.set_new_waitTime_store(formData);
      this.mySocket.emit("updateWaitTimeData", formData);
      Alert.success("<h4>New Wait Time Update Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 2500,
        offset: 35,
      });
      window.location.reload();
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="edit-input-main">
          <input
            name="inputTime"
            type="number"
            value={this.state.inputTime}
            onChange={this.onChange}
            className="text-center input-field-css"
            disabled={
              this.props.dinner.order_type === "reservation" ||
              this.props.dinner.order_type === "walkin"
                ? "disabled"
                : ""
            }
          />
          <img src={buttonimg} onClick={this.onSubmit} alt="edit" />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { set_new_waitTime_store })(
  withRouter(EditWaitTime)
);
//02-21-2020
