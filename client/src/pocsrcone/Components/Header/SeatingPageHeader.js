import React, { Component } from 'react';
import VendorDashboard from '../VendorDashboard/VendorDashboard';
import NavbarDashboard from '../../../components/Dassboard/NavbarDashboard';
import MainPending from './../PocFirstSecondItration/Pending/MainPending';
import MainReservation from './../PocFirstSecondItration/Reservation/MainReservation';
import MainHistory from './../PocFirstSecondItration/History/MainHistory';

export class SeatingPageHeader extends Component {
  constructor() {
    super();
    this.state = {
      selectedMenuInside: 1
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectedMenuInside: value
    });
  };

  render() {
    return (
      <React.Fragment>
        <NavbarDashboard />
        {/* <div class='container-fluid admin_margin-padding-main'> */}
        <div className='inside-main-menu d-flex justify-content-center new-poc-one-shadow'>
          <nav className='navbar'>
            <p
              onClick={this.onPageChange(2)}
              className={
                this.state.selectedMenuInside === 2
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              Pending
            </p>
            <p
              onClick={this.onPageChange(1)}
              className={
                this.state.selectedMenuInside === 1
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              Seating
            </p>
            <p
              onClick={this.onPageChange(3)}
              className={
                this.state.selectedMenuInside === 3
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              Reservations
            </p>
            <p
              onClick={this.onPageChange(4)}
              className={
                this.state.selectedMenuInside === 4
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              History
            </p>
          </nav>
        </div>
        <div className='container-fluid'>
          <div className='row'>
            {this.state.selectedMenuInside === 2 ? <MainPending /> : ''}
            {this.state.selectedMenuInside === 1 ? <VendorDashboard /> : ''}
            {this.state.selectedMenuInside === 3 ? <MainReservation /> : ''}
            {this.state.selectedMenuInside === 4 ? <MainHistory /> : ''}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SeatingPageHeader;
