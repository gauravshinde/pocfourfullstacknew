import React, { Component } from "react";
// import ButtonComponent from "./ButtonComponent";
import LargeText from "./LargeText";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { sendWaitTimeSms } from "../../store/actions/smsSendAction";

export class PopUp extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      waitTime: "",
      mobileno: "",
      showPopUp: false
    };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.dinner.name,nextProps.dinner.phone_number, nextProps.dinner.wait_time );
    if (
      nextProps.dinner.name !== nextState.name ||
      nextProps.dinner.wait_time !== nextState.waitTime
    ) {
      return {
        name: nextProps.dinner.name,
        waitTime: nextProps.dinner.wait_time,
        mobileno: nextProps.dinner.phone_number
      };
    }
  }

  onSubmit = e => {
    e.preventDefault();
    var formData = {
      mobileNo: this.state.mobileno,
      message: `Hi ${this.state.name}, Only ${this.state.waitTime} minutes remaining for you to be seated.`
    };
    this.props.sendWaitTimeSms(formData);
    this.setState({
      showPopUp: false
    });
  };

  render() {
    if (this.state.showPopUp) {
      return (
        <React.Fragment>
          <div className="popup">
            <div className="popup_inner">
              <LargeText largetext={"Send SMS"} largetextclass={"login-text"} />
              <div className="para-div">
                <p>Dear {this.state.name},</p>

                <p>
                  Only {this.state.waitTime} minutes remaining
                  <br /> for you to be seated.
                </p>

                <p>Thanks.</p>
              </div>

              <div className="order-notes-button">
                <button
                  type="submit"
                  className="login-button"
                  onClick={this.onSubmit}
                >
                  Send
                </button>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
    return (
      <img
        onClick={() => this.setState({ showPopUp: true })}
        className={this.props.imgclasspop}
        alt="msgsd_sd"
        src={this.props.imgsrc}
      />
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps, { sendWaitTimeSms })(withRouter(PopUp));
