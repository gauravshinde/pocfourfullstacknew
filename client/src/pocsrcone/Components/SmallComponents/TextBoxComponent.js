import React from "react";

const TextBoxComponent = ({
  name,
  labeltext,
  img,
  alt,
  onChange,
  imageClass,
  Textareaclass,
  place
}) => {
  return (
    <React.Fragment>
      <div className="form-group mb-0 text-left username-login-form-group">
        <label>{labeltext} </label>
        <div>
          <img src={img} alt={alt} className={imageClass} />
        </div>
        <textarea
          name={name}
          className={Textareaclass}
          placeholder={place}
          cols="40"
          rows="4"
          onChange={onChange}
        />
      </div>
      {/* <div class="form-group mb-0 text-left username-login-form-group">
        <label for="exampleFormControlTextarea1">Address</label>
        <div>
          <img src={img} alt={alt} className={imageClass} />
        </div>
        <textarea name={name} class="form-control textarea-bottomblack" onChange={onChange} id="exampleFormControlTextarea1" cols="40" rows="4" placeholder="eg. Riverdale" ></textarea>
      </div> */}
    </React.Fragment>
  );
};

export default TextBoxComponent;
