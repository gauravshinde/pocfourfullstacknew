import React from 'react';

const VendorTableHeadRow = ({
  no,
  Name,
  OrderType,
  PartySize,
  WaitTime,
  Details,
  Status,
  Blank,
  Blankone,
  Adults,
  Kids,
  Total,
  Occassion,
  Timer,
  Img,
  Alt
}) => {
  return (
    <React.Fragment>
      <tr className='seating-table-head-class'>
        <th>
          <form>
            <div className='custom-control custom-checkbox'>
              <input
                type='checkbox'
                className='custom-control-input'
                id='customCheck'
                name='example1'
              />
              <label
                className='custom-control-label'
                htmlFor='customCheck'
              ></label>
            </div>
          </form>
        </th>
        <th>{no}</th>
        <th>{Name}</th>
        <th>{OrderType}</th>
        <th>{PartySize}</th>
        <th>{WaitTime}</th>
        <th>{Details}</th>
        <th>{Status}</th>
        <th>{Blank}</th>
        <th>{Blankone}</th>
        {/* <th>{Phone}</th> */}
        {/* <th>{Adults}</th>
        <th>{Kids}</th>
        <th>{Total}</th> */}
        {/* <th>{Occassion}</th>
        <th>{Timer}</th> */}
      </tr>
    </React.Fragment>
  );
};

export default VendorTableHeadRow;
