import React, { Component } from "react";
import HeaderComponent from "../Header/HeaderComponent";
import ButtonComponent from "../SmallComponents/ButtonComponent";
import InputComponent from "../SmallComponents/InputComponent";
import DefaultWaitTime from "./DefaultWaitTime";
import VendorTableHeadRow from "../VendorDetails/VendorTableHeadRow";
import VendorTableHeadSubRow from "../VendorDetails/VendorTableHeadSubRow";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../store/actions/authAction";
// import { getVendordetails } from "./../../store/actions/vendordetailsAction";
import { getDinner } from "../../store/actions/dinerAction";
import isEmpty from "./../../store/validation/is-empty";
import { getStatusChange } from "../../store/actions/statuschangeAction";
import Loader from "../Loader/Loader";
import "loaders.css/src/animations/line-scale.scss";
import { PopUp } from "./../SmallComponents/PopUp";

function demoAsyncCall() {
  return new Promise(resolve => setTimeout(() => resolve(), 3300));
}

const SeatStatus = ["Nonseated", "Seated", "Completed", "Cancelled"];

class VendorDetails extends Component {
  constructor() {
    super();
    this.state = {
      vendorid: "",
      viewDetailsDinner: {},
      defaultWaitTime: 0,
      partySize: "All",
      show: "all",
      status: "",
      search: "",
      loading: true,
      showPopup: false
    };
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  componentDidMount() {
    // this simulates an async action, after which the component will render the content
    demoAsyncCall().then(() => this.setState({ loading: false }));

    if (this.props.getDinner) {
      this.props.getDinner(this.state.vendorid);
    }
    // console.log(this.props.location.state.detailid, this.props.vendor);
    if (this.props.vendor.length !== 0) {
      let vendorData = this.props.vendor.find(
        vendor => vendor._id === this.props.location.state.detailid
      );
      console.log(vendorData);
      this.setState({
        ...vendorData
      });
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    if (nextprops.location.state.detailid !== nextstate.vendorid) {
      return {
        vendorid: nextprops.location.state.detailid
      };
    }

    if (nextprops.dinner !== nextstate.viewDetailsDinner) {
      return { viewDetailsDinner: nextprops.dinner };
    }

    return null;
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser(this.props.history);
  };

  /*******************************
   * @DESC - DEFAULT TIME HANDLER
   ******************************/
  onDefaultTimeHandler = value => {
    if (value === "00") {
      this.setState({ defaultWaitTime: 0 });
    } else if (value === "5-10") {
      this.setState({ defaultWaitTime: 10 });
    } else if (value === "10-20") {
      this.setState({ defaultWaitTime: 20 });
    } else if (value === "30-45") {
      this.setState({ defaultWaitTime: 45 });
    } else if (value === "45+") {
      this.setState({ defaultWaitTime: 50 });
    }
  };

  /************************************
   * @DESC - DEFAULT PARTY SIZE HANDLER
   ************************************/
  onDefaultPartySizeHandler = value => {
    if (value === "All") {
      this.setState({ partySize: "All" });
    } else if (value === "8>") {
      this.setState({ partySize: "8>" });
    } else if (value === "2") {
      this.setState({ partySize: 2 });
    } else if (value === "6") {
      this.setState({ partySize: 6 });
    } else if (value === "4") {
      this.setState({ partySize: 4 });
    }
  };

  /**************************************
   * @DESC -SHOWALL || NON SEATED TOGGLER
   **************************************/
  handleShow = value => e => {
    console.log("Clicked");
    this.setState({
      show: value
    });
  };

  /********************************************
   * @DESC -ON SELECTSTATUS CHANGE
   ********************************************/
  onSelectStatusChange = dinnerId => e => {
    // console.log("alle");
    // console.log(e.target.value);
    e.preventDefault();

    const statusUpdate = {
      dinnerId: dinnerId,
      status: e.target.value
    };

    this.props.getStatusChange(statusUpdate);
  };

  renderviewdinnername = () => {
    const { viewDetailsDinner } = this.state;
    let renderList = [];
    // console.log(this.state.partySize, this.state.defaultWaitTime);
    if (this.state.partySize === "All") {
      renderList = viewDetailsDinner;
    } else if (this.state.partySize === "8>") {
      renderList = viewDetailsDinner.filter(dinner => dinner.total >= 8);
    } else {
      renderList = viewDetailsDinner.filter(
        dinner => dinner.total <= this.state.partySize
      );
    }
    if (this.state.show === "Nonseated") {
      renderList = renderList.filter(dinner => dinner.status === "Nonseated");
    }

    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      renderList = renderList.filter(dinner => {
        if (search.test(dinner.name)) {
          return dinner;
        }
      });
    }
    if (!isEmpty(renderList)) {
      return renderList.map((dinner, index) => {
        let waitingImage = require("./../../assets/Images/clock-circular-outline.svg");
        if (dinner.wait_time < 1) {
          waitingImage = require("./../../assets/Images/walking-img.svg");
        }
        // this.onStartCountDown(dinner.wait_time, index);
        // console.log(dinner.status);

        return (
          <React.Fragment key={index}>
            <VendorTableHeadSubRow
              dinnerID={dinner._id}
              key={index}
              no={++index}
              Name={dinner.name}
              Phone={dinner.phone_number}
              Img1={require("./../../assets/Images/envelope-img.svg")}
              onClickone={this.togglePopup.bind(this)}
              Img={waitingImage}
              imgclass={"p-0"}
              Adults={dinner.adults}
              Kids={dinner.kids}
              Total={dinner.total}
              Occassion={dinner.special_occassion}
              WaitTime={dinner.wait_time}
              Img2={require("./../../assets/Images/penimg.svg")}
              Timer={"00:00"}
              seatingStatus={dinner.status}
              Status={SeatStatus}
              onSelectStatusChange={this.onSelectStatusChange}
            />
            {/* ***** Pop Up Conditions ***** */}
            {this.state.showPopup ? (
              <PopUp closePopup={this.togglePopup.bind(this)} />
            ) : null}
          </React.Fragment>
        );
      });
    }
  };

  render() {
    // const { user } = this.props.auth;
    // console.log(user);

    const { loading } = this.state;

    if (loading) {
      return (
        <React.Fragment>
          <Loader />
        </React.Fragment>
      );
    }

    // const vendordata = this.props.location.state.detailid;
    // console.log(vendordata);
    // console.log(this.props.location.state.detailid);
    // const dinner_list = this.props.location.state.detailid;
    // console.log(dinner_list);

    console.log(this.state.viewDetailsDinner);

    return (
      <React.Fragment>
        <HeaderComponent
          headertext={"Logo"}
          vendorDashboard={true}
          addVendorBtn={true}
          editVendorbtn={true}
          link={"/vendor-list"}
          linktwo={"/add-vendor"}
          linkbutton={"Add Vendor"}
          onClick={this.onLogoutClick}
        />
        <div className="container-fluid vendor-details-container">
          <div className="row">
            <div className="col-12 col-md-3">
              <h1 className="font-36-black">
                {this.state.vendor_name ? this.state.vendor_name : ""}
              </h1>
              <span className="gray-border" />
              <p className="gray-text-block-p">
                {this.state.address ? this.state.address : ""}
              </p>
            </div>

            <div className="col-12 col-md-9">
              <ButtonComponent
                buttontype={"button"}
                buttonclass={
                  this.state.show === "Nonseated"
                    ? "login-button login-button--vendor-details-show"
                    : "login-button login-button--vendor-details-show-all"
                }
                buttontext={"Show Non Seated"}
                handleOnClick={this.handleShow("Nonseated")}
              />

              <ButtonComponent
                buttontype={"button"}
                buttonclass={
                  this.state.show === "all"
                    ? "login-button login-button--vendor-details-show"
                    : "login-button login-button--vendor-details-show-all"
                }
                buttontext={"Show All"}
                handleOnClick={this.handleShow("all")}
              />

              <div className="table-time-outer-block">
                <div>
                  <h3 className="font-28-medium">
                    Default Wait Time{" "}
                    <span className="font-21-medium dark-gray-text">
                      &#40;Mins&#41;
                    </span>
                  </h3>
                  <DefaultWaitTime
                    arrayNumber={["00", "5-10", "10-20", "30-45", "45+"]}
                    onClick={this.onDefaultTimeHandler}
                    currentWaitTime={this.state.defaultWaitTime}
                  />
                </div>
                <div>
                  <h3 className="font-28-medium">Filter By Party Size </h3>

                  <DefaultWaitTime
                    arrayNumber={["2", "4", "6", "8>", "All"]}
                    onClick={this.onDefaultPartySizeHandler}
                    partySize={this.state.partySize}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container table-container ">
          <div className="col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center">
            <div>
              <h4 className={"vendor"}>
                {this.state.show === "all"
                  ? "All Seats"
                  : "Viewing All Nonseated"}
              </h4>
            </div>

            <div className="d-flex justify-content-between">
              {/* w-25 */}
              <InputComponent
                img={require("./../../assets/Images/search.svg")}
                searchClass={"search-label"}
                onChange={this.onSearchChange}
                value={this.state.search}
                place={"search"}
                inputclass={
                  "form-control input-bottomblack mb-0 pl-3 input-search"
                }
              />
              {/* <img
                src={require("./../../assets/Images/sort-button-with-three-lines.svg")}
                alt="sort-button-with-three-lines"
              />
              <img
                src={require("./../../assets/Images/filter.svg")}
                alt="filter-img"
              /> */}
            </div>
          </div>
          <div className="row">
            <div className="vendordetail-col table-responsive">
              <table className="table table-striped mb-4">
                <thead>
                  <VendorTableHeadRow
                    no={"sr. No"}
                    Name={"Name"}
                    Phone={"Phone Number"}
                    OrderType={"Order Type"}
                    Adults={"Adults"}
                    Kids={"Kids"}
                    Total={"Total"}
                    Occassion={"Special Occassion"}
                    WaitTime={"WaitTime"}
                    Timer={"Timer"}
                    Status={"Status"}
                  />
                </thead>

                <tbody>{this.renderviewdinnername()}</tbody>
              </table>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorDetails.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  dinner: PropTypes.array.isRequired,
  getDinner: PropTypes.func.isRequired,
  getStatusChange: PropTypes.func.isRequired,
  vendor: PropTypes.object.isRequired
  // vendordetails: PropTypes.array.isRequired,
  // getVendordetails: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  vendor: state.vendor.vendor,
  dinner: state.dinner.dinner
  // vendordetails: state.vendordetails.vendordetails
});

export default connect(mapStateToProps, {
  logoutUser,
  getDinner,
  getStatusChange
})(VendorDetails);

//, getVendordetails
