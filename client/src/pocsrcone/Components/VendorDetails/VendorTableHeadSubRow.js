import React from "react";
// import Dropdown from "react-dropdown";
// import "react-dropdown/style.css";

// const options = ["Not Seated", "Seated", "Completed", "Cancelled"];
// const defaultOption = options[0];
import PopUp from "../SmallComponents/PopUp";
import EditWaitTime from "./../VendorDashboard/EditWaitTime";
import OcassionpopupnewSeating from "../PocFirstSecondItration/ReusableComponents/AllPopUps/OcassionpopupnewSeating";
import EditDinnerPopupSeatingPage from "../PocFirstSecondItration/ReusableComponents/AllPopUps/EditDinnerPopupSeatingPage";
import NewCountDown from "../VendorDashboard/InsideViewPage/NewCountDown";
import Moment from "moment";

const VendorTableHeadSubRow = ({
  dinner,
  dinnerID,
  no,
  Name,
  Phone,
  // OrderType,
  Adults,
  Kids,
  HighChair,
  Handicap,
  Total,
  Occassion,
  WaitTime,
  Timer,
  Timer1,
  Status,
  Img,
  Img1,
  Img2,
  Img3,
  Alt,
  onClickone,
  inputWaitTime,
  onWaitTimeChange,
  onClicktwo,
  onSelectStatusChange,
  timerKey,
  onWaitTimeSubmit,
  seatingStatus,
  checkid,
  confirm,
  eta,
  etatime,
  editOnclick,
  acceptDate,
  acceptTime,
  callingActiononclick,
  waitNew
}) => {
  return (
    <React.Fragment>
      <tr className="seating-table-body-class">
        <td>
          <form>
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                id={checkid}
                name="example1"
              />
              <label className="custom-control-label" htmlFor={checkid}></label>
            </div>
          </form>
        </td>
        <td>{no}</td>
        <td
          className="text-capitalize font-weight-bold"
          style={{ fontFamily: "AvenirLTStd-Black" }}
        >
          {Name}
          <div className="d-flex">
            <img
              src={require("../../assets/Images/seating/call.svg")}
              alt="call"
              className="seating-logo-img"
              onClick={callingActiononclick}
            />
            <PopUp
              imgsrc={require("../../assets/Images/seating/message.svg")}
              dinner={dinner}
              imgclasspop={"seating-logo-img"}
            />
            <img
              src={require("../../assets/Images/seating/bell.svg")}
              alt="bell"
              className="seating-logo-img"
            />
          </div>
        </td>
        <td>
          <img src={Img} alt={Alt} />
          <p>{acceptTime}</p>
          <p>{acceptDate}</p>
        </td>
        <td className="d-flex flex-column align-items-center">
          <div className="w-100 d-flex justify-content-around text-center ">
            <div>
              <span>{Adults}</span>
              <br />
              <span>Adult</span>
            </div>
            <div>
              <span>{Kids}</span>
              <br />
              <span>Kids</span>
            </div>
          </div>
          <div className="w-100 d-flex justify-content-around text-center ">
            <div>
              <span>{HighChair}</span>
              <br />
              <span>High-Chair</span>
            </div>
            <div>
              <span>{Handicap}</span>
              <br />
              <span>Handicap</span>
            </div>
          </div>
        </td>
        <td>
          <EditWaitTime dinner={dinner} />
          <p className="d-none" id={timerKey}>
            {Timer}
          </p>
          {/* <p id={timerKey}>{Timer1}</p> */}
          <p id={timerKey}>
            {Timer1 === "0" || Timer1 === "" || Timer1 === null ? (
              "0 h : 0 m : 0 s"
            ) : (
              <NewCountDown
                waitNew={() => {
                  const data = Timer1;
                  //console.log(data);
                  // console.log("Data Value ===>", data);
                  var ms = Moment(data, "DD/MM/YYYY hh:mm:ss A").diff(
                    Moment(new Date(), "DD/MM/YYYY hh:mm:ss A")
                  );
                  return ms;
                }}
              />
            )}
          </p>
        </td>
        <td>
          <div className="d-flex justify-content-between align-items-center">
            <div>
              <p className="bold-font">Location</p>
              <p>View Location</p>
            </div>
            <img
              src={require("../../assets/Images/seating/maps-and-flags.svg")}
              alt="location"
              className="ordertype-img"
            />
          </div>
        </td>
        <td>
          <div className="form-group mb-0">
            {/* <Dropdown options={options} onChange={onChange} value={Status} /> */}
            <select
              onChange={onSelectStatusChange(dinnerID)}
              className="form-control p-0 size-select"
            >
              {Status.map((option, index) => (
                <option
                  key={index}
                  value={option}
                  selected={seatingStatus === option ? "selected" : ""}
                >
                  {option}
                </option>
              ))}
            </select>
          </div>
          <div className="green-box">
            <p>{confirm}</p>
            <p className="bold-font">{eta}</p>
            <p>{etatime}</p>
          </div>
        </td>
        <td>
          <OcassionpopupnewSeating Occassion={Occassion} />
          {/* <img src={img5} alt="ocassion" className={imgclass} /> */}
        </td>

        <td>
          <div className="mb-3">
            <img
              src={require("../../assets/Images/seating/eye.svg")}
              alt="eye"
              className="seating-logo-img"
              onClick={editOnclick}
            />
          </div>
          <EditDinnerPopupSeatingPage />
          {/* <img
            src={require("../../assets/Images/seating/pen.svg")}
            alt="pen"
            className="seating-logo-img"
          /> */}
        </td>
      </tr>
    </React.Fragment>
  );
};

export default VendorTableHeadSubRow;
