import React from 'react';

const DefaultWaitTime = ({
  arrayNumber,
  onClick,
  currentWaitTime,
  partySize
}) => {
  const divElement = arrayNumber.map((number, index) => (
    <span
      onClick={() => onClick(number)}
      key={index}
      className={
        (partySize === 2 && number === '2') ||
        (partySize === 4 && number === '4') ||
        (partySize === 6 && number === '6') ||
        (partySize === 'All' && number === 'All') ||
        (partySize === '8>' && number === '8>') ||
        (currentWaitTime === 50 && number === '45+') ||
        (currentWaitTime === 45 && number === '30-45') ||
        (currentWaitTime === 20 && number === '10-20') ||
        (currentWaitTime === 10 && number === '5-10') ||
        (currentWaitTime === 0 && number === '00')
          ? 'font-21-medium select-current-time '
          : 'font-21-medium'
      }
    >
      {/* {console.log(partySize, number)} */}
      {number}
    </span>
  ));
  return <div className='default-wait-time-block'>{divElement}</div>;
};

export default DefaultWaitTime;
