import React, { Component } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

import InputComponent from "../SmallComponents/InputComponent";
import ButtonComponent from "../SmallComponents/ButtonComponent";
import LargeText from "../SmallComponents/LargeText";
// import HeaderComponent from '../Header/HeaderComponent';
import TextBoxComponent from "../SmallComponents/TextBoxComponent";
import IconBox from "../../../reusableComponents/IconBoxButtonComponent";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { registerDiner } from "../../store/actions/authAction";
import { addDinnerValidation } from "../../store/validation/addDinnerValidation";
import { logoutUser } from "../../store/actions/authAction";
import {
  get_subscription_details,
  get_restauranttime,
} from "../../../store/actions/addDetailsActions";
import NavbarDashboard from "../../../components/Dassboard/NavbarDashboard";
import isEmpty from "../../../store/validation/is-Empty";

export class VendorDiner extends Component {
  constructor() {
    super();
    this.state = {
      phone: "",
      name: "",
      phone_number: "",
      adults: "",
      kids: "",
      highchair: "",
      handicap: "",
      selected_seating_area: [],
      primary_seating_area: {},
      // seating_preference: '',
      special_occassion: "",
      status: "Nonseated",
      subscription_details_handicap: "",
      subscription_details_highchair: "",
      orderId: Math.floor(Math.random() * 9000000000) + 1000000000,
      seatingData: [],
      errors: {},
    };
  }

  componentDidMount() {
    var defaultWaitTime = this.props.location.state.defaultWaitTime;
    this.setState({
      wait_time: defaultWaitTime,
      seatingData: this.props.details.restauranttime_details
        .primary_seating_area,
    });
    this.props.get_subscription_details();
    this.props.get_restauranttime();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      !isEmpty(nextProps.details.subscription_details) &&
      !isEmpty(nextProps.details.restauranttime_details) &&
      !nextState.subscription_details_handicap &&
      !nextState.subscription_details_highchair
    ) {
      return {
        subscription_details_handicap:
          nextProps.details.subscription_details[0].handicap,
        subscription_details_highchair:
          nextProps.details.subscription_details[0].highchair,
        selected_seating_area:
          isEmpty(
            nextProps.details.restauranttime_details.selected_seating_area
          ) || nextProps.details.restauranttime_details.selected_seating_area,
        primary_seating_area:
          isEmpty(
            nextProps.details.restauranttime_details.primary_seating_area
          ) || nextProps.details.restauranttime_details.primary_seating_area,
      };
    }
    return null;
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  handleAddDinerChange = (e) => {
    e.preventDefault();

    let data = e.target.value.split(" ");
    let newarray1 = [];
    for (let x = 0; x < data.length; x++) {
      newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
    }
    let newData = newarray1.join(" ");
    this.setState({
      // [e.target.name]: e.target.value
      [e.target.name]: newData,
      error: {},
    });

    // this.setState({
    //   [e.target.name]: e.target.value,
    //   error: {}
    // });
  };

  /****************************************
   * @DESC - Seating Area CODE SELECTOR
   ****************************************/
  onSeatingAreaArraySelector = (icon) => (e) => {
    let SeatingArea = this.state.selected_seating_area;
    if (SeatingArea.length === 0) {
      SeatingArea.push(icon);
    } else {
      let isAlreadyPresent = SeatingArea.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = SeatingArea.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          SeatingArea.splice(indexOf, 1);
        }
      } else {
        SeatingArea.push(icon);
      }
    }
    this.setState({
      selected_seating_area: SeatingArea,
    });
  };

  onSelectedAreaPrimarySelector = (icon) => (e) => {
    this.setState({
      primary_seating_area: icon,
    });
  };

  primary_selected_icons = (iconNew) => {
    console.log(iconNew);
    this.setState({
      primary_seating_area: iconNew.title,
      seatingData: iconNew,
    });
    // console.log(icon);
  };

  handleAddDinerSubmit = (e) => {
    e.preventDefault();
    const { errors, isValid } = addDinnerValidation(this.state);
    if (!isValid) {
      this.setState({
        errors: errors,
      });
    } else {
      const newDiner = {
        name: this.state.name,
        phone_number: "+" + this.state.phone + this.state.phone_number,
        adults: this.state.adults === "" ? "0" : this.state.adults,
        kids: this.state.kids === "" ? "0" : this.state.kids,
        highchair: this.state.highchair === "" ? "0" : this.state.highchair,
        handicap: this.state.handicap === "" ? "0" : this.state.handicap,
        seating_preference: this.state.seatingData.title,
        special_occassion: this.state.special_occassion,
        wait_time: this.state.wait_time,
        status: this.state.status,
        entry_point: "manual entry",
        orderId: this.state.orderId,
      };
      //console.log(newDiner);
      this.props.registerDiner(newDiner, this.props.history);
    }
  };

  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.logoutUser(this.props.history);
  };

  render() {
    const {
      errors,
      subscription_details_highchair,
      subscription_details_handicap,
    } = this.state;

    return (
      <React.Fragment>
        {/* <HeaderComponent
          headertext={'Logo'}
          vendorDashboard={true}
          addVendorBtn={false}
          editVendorbtn={false}
          link={'/vendor-seating'}
          onClick={this.onLogoutClick}
        /> */}
        <NavbarDashboard />
        <div
          className="m-auto text-center Add-dinner-main-page"
          style={{ width: "25%" }}
        >
          {/* container */}
          <div className="row">
            <div className="m-auto add-vendor-align">
              <LargeText
                largetext={"Add Diner"}
                largetextclass={"addvendor-text"}
              />
              <hr />

              <form noValidate onSubmit={this.handleAddDinerSubmit}>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group text-left">
                      <label
                        htmlFor="Country Code"
                        className="font-weight-light"
                      >
                        Country Code
                      </label>
                      <PhoneInput
                        country={""}
                        placeholder={"Select"}
                        value={this.state.phone}
                        onChange={(phone) => this.setState({ phone })}
                      />
                    </div>
                  </div>
                  <div className="col-sm-8">
                    <InputComponent
                      img={require("./../../assets/Images/phone-img.svg")}
                      alt={"Phone-img"}
                      labeltext={"Phone Number"}
                      name={"phone_number"}
                      type={"Number"}
                      place={"eg. 123567890"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.phone_number}
                      onChange={this.handleAddDinerChange}
                    />
                    <div className="col-12 main-validation-div">
                      {errors.phone_number ? (
                        <span className="isnotvalid">
                          {errors.phone_number}
                        </span>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
                <InputComponent
                  img={require("./../../assets/Images/usernamepic.svg")}
                  alt={"user-img"}
                  labeltext={"Name"}
                  name={"name"}
                  type={"text"}
                  place={"eg. James Bond"}
                  inputclass={"form-control input-bottomblack "}
                  value={this.state.name}
                  onChange={this.handleAddDinerChange}
                  error={errors.name}
                />

                <div className="col-12 main-validation-div">
                  {errors.name ? (
                    <span className="isnotvalid">{errors.name}</span>
                  ) : (
                    ""
                  )}
                </div>

                <div className="form-row">
                  <div className="form-group mb-0 col-xl-6">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Adults"}
                      name={"adults"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.adults}
                      onChange={this.handleAddDinerChange}
                      error={errors.adults}
                    />
                  </div>

                  <div className="form-group mb-0 col-xl-6">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Kids"}
                      name={"kids"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.kids}
                      onChange={this.handleAddDinerChange}
                      error={errors.kids}
                    />
                  </div>
                </div>

                <div className="form-row">
                  {!isEmpty(subscription_details_highchair === true) ? (
                    <div className="form-group mb-0 col-xl-6">
                      <InputComponent
                        imageClass={"d-none"}
                        labeltext={"Highchair"}
                        name={"highchair"}
                        type={"number"}
                        place={"select"}
                        inputclass={"form-control pl-3 input-bottomblack"}
                        value={this.state.highchair}
                        onChange={this.handleAddDinerChange}
                        error={errors.highchair}
                      />
                    </div>
                  ) : null}

                  {!isEmpty(subscription_details_handicap === true) ? (
                    <div className="form-group mb-0 col-xl-6">
                      <InputComponent
                        imageClass={"d-none"}
                        labeltext={"Handicapped"}
                        name={"handicap"}
                        type={"number"}
                        place={"select"}
                        inputclass={"form-control pl-3 input-bottomblack"}
                        value={this.state.handicap}
                        onChange={this.handleAddDinerChange}
                        error={errors.handicap}
                      />
                    </div>
                  ) : null}
                </div>

                <div className="d-flex flex-wrap text-left">
                  <p className="col-12 p-0 restaurant-title">
                    Selected Seating Area
                  </p>
                  <div
                    style={{ display: "flex", flexWrap: "wrap", width: "100%" }}
                  >
                    {this.state.selected_seating_area.map((iconNew, index) => (
                      <IconBox
                        key={index}
                        src={iconNew.icon}
                        title={iconNew.title}
                        classsection={
                          iconNew.title === this.state.primary_seating_area
                            ? "seating-preferences_active"
                            : "seating-preferences"
                        }
                        onClick={() => this.primary_selected_icons(iconNew)}
                      />
                    ))}
                  </div>
                </div>

                <TextBoxComponent
                  imageClass={"d-none"}
                  name={"special_occassion"}
                  type={"text"}
                  labeltext={"Special Ocassion"}
                  place={"eg. Birthday "}
                  Textareaclass={
                    "form-control pl-3 textarea-bottomblack text-capitalize"
                  }
                  value={this.state.special_occassion}
                  onChange={this.handleAddDinerChange}
                  error={errors.special_occassion}
                />
                <div className="col-12 main-validation-div mb-4">
                  {errors.special_occassion ? (
                    <span className="isnotvalid">
                      {errors.special_occassion}
                    </span>
                  ) : (
                    ""
                  )}
                </div>

                <div>
                  <ButtonComponent
                    buttontype={"submit"}
                    buttonclass={"login-button mb-4"}
                    buttontext={"Save"}
                    handleOnClick={this.nextClickHandler}
                  />
                  <Link to="/vendor-seating">
                    <ButtonComponent
                      buttontype={"button"}
                      buttonclass={"cancel-button mb-4"}
                      buttontext={"Cancel"}
                      handleOnClick={this.cancelClickHandler}
                    />
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorDiner.propTypes = {
  // logoutUser: PropTypes.func.isRequired,
  registerDiner: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  details: state.details,
  errors: state.errors,
});

export default connect(mapStateToProps, {
  logoutUser,
  registerDiner,
  get_subscription_details,
  get_restauranttime,
})(withRouter(VendorDiner));

//28-03-2020 12:45
