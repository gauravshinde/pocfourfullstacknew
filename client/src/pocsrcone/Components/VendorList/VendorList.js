import React, { Component } from 'react';
import HeaderComponent from '../Header/HeaderComponent';
import InputComponent from '../SmallComponents/InputComponent';
import VendorTableRow from '../VendorList/VendorTableRow';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../store/actions/authAction';
import { getVendor } from '../../store/actions/vendorAction';
import isEmpty from './../../store/validation/is-empty';
import Loader from '../Loader/Loader';
import 'loaders.css/src/animations/line-scale.scss';

function demoAsyncCall() {
  return new Promise(resolve => setTimeout(() => resolve(), 3300));
}

export class VendorList extends Component {
  constructor() {
    super();
    this.state = {
      dtailId: '',
      vendorList: {},
      search: '',
      loading: true
    };
  }

  componentDidMount() {
    // this simulates an async action, after which the component will render the content
    demoAsyncCall().then(() => this.setState({ loading: false }));
    if (this.props.getVendor) {
      this.props.getVendor();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    // console.log(nextprops.vendor);
    if (nextprops.vendor) {
      return {
        vendorList: nextprops.vendor
      };
    }
    return null;
  }

  // shouldComponentUpdate(nextprops, nextstate) {
  //   if (nextstate.vendorList !== this.state.vendorList) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  hendleVendorList = value => e => {
    // e.preventDefault();
    this.props.history.push({
      pathname: '/vendor-details',
      // search: '?query=abc',
      state: { detailid: value }
    });
    console.log(value);
  };

  hendleVendorEdit = value => e => {
    // e.preventDefault();
    this.props.history.push({
      pathname: '/edit-vendor',
      // search: '?query=abc',
      state: { detailid: value }
    });
    console.log(value);
  };

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser(this.props.history);
  };

  renderpersonname = () => {
    const { vendorList } = this.state;
    var filteredvendorList = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, 'i');
      filteredvendorList = vendorList.filter(vendor => {
        if (search.test(vendor.vendor_name)) {
          return vendor;
        }
      });
    } else {
      filteredvendorList = vendorList;
    }
    if (!isEmpty(filteredvendorList)) {
      return filteredvendorList.map((vendor, index) => (
        <React.Fragment key={index}>
          <VendorTableRow
            key={index}
            no={++index}
            vendorName={vendor.vendor_name}
            venderUser={vendor.username}
            // vendorPassword={vendor.password}
            totalNonSeated={vendor.totalNonSeated}
            totalSeated={vendor.totalSeated}
            totalPeople={vendor.totalPeople}
            img={require('../../assets/Images/penimg.svg')}
            onClick={this.hendleVendorList(vendor._id)}
            img1={require('../../assets/Images/right-arrow.svg')}
            onClicktwo={this.hendleVendorEdit(vendor._id)}
          />
        </React.Fragment>
      ));
    } //else {
    //   return (
    //     <h3 className="isempty-text dashboard-panel__counts-title mb-0">
    //       Please add more leads to get responses
    //     </h3>
    //   );
    // }
  };

  render() {
    console.log(this.state.search, 'Search');
    const { loading } = this.state;

    if (loading) {
      return (
        <React.Fragment>
          <Loader />
        </React.Fragment>
      );
    }
    // const vendordetails = [
    //   "id1",
    //   "id2",
    //   "id3",
    //   "id4",
    //   "id5",
    //   "id6",
    //   "id7",
    //   "id8"
    // ];

    // const { user } = this.props.auth;
    // console.log(user);

    // console.log(this.state.vendorList);

    // // Destrcuturing data

    // const { vendorList } = this.state;
    // console.log(vendorList);

    return (
      <React.Fragment>
        <HeaderComponent
          headertext={'Logo'}
          vendorDashboard={true}
          addVendorBtn={true}
          editVendorbtn={true}
          link={'/vendor-list'}
          linktwo={'/add-vendor'}
          linkbutton={'Add Vendor'}
          onClick={this.onLogoutClick}
        />
        <div className='container table-container pl-md-0'>
          <div className='col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center'>
            <div>
              <h4 className={'vendor'}>All Vendors</h4>
            </div>

            <div className='d-flex justify-content-between'>
              {/* w-25 */}
              <InputComponent
                img={require('./../../assets/Images/search.svg')}
                searchClass={'search-label'}
                onChange={this.onSearchChange}
                value={this.state.search}
                place={'search'}
                inputclass={
                  'form-control input-bottomblack mb-0 pl-3 input-search'
                }
              />
              {/* <img
                src={require("./../../assets/Images/sort-button-with-three-lines.svg")}
                alt="sort-button-with-three-lines"
              />
              <img
                src={require("./../../assets/Images/filter.svg")}
                alt="filter-img"
              /> */}
            </div>
          </div>
          <div className='row'>
            <div className='vendordetail-col table-responsive'>
              <table className='table table-striped'>
                <thead>
                  <tr className='first-row'>
                    <th>sr. No.</th>
                    <th>Vendor Name</th>
                    <th>Username</th>
                    {/* <th>Password</th> */}
                    <th>Total Seated</th>
                    <th>Total Non-Seated</th>
                    <th>Total People</th>
                    <th>View Details</th>
                    <th />
                  </tr>
                </thead>
                <tbody>{this.renderpersonname()}</tbody>
              </table>
            </div>
            {/* <div className="col p-0 vendordetail-col table-responsive">
              <table className="table table-striped mb-0">
                <thead>
                  <tr className="first-row">
                    <th>sr. No.</th>
                    <th>Vendor Name</th>
                    <th>Username</th>
                    {/* <th>Password</th> *
                    <th>Total Seated</th>
                    <th>Total Non-Seated</th>
                    <th>Total People</th>
                    <th>View Details</th>
                    <th />
                  </tr>
                </thead>
                <tbody>{this.renderpersonname()}</tbody>
              </table>
            </div> */}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorList.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  vendor: PropTypes.array.isRequired,
  getVendor: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  vendor: state.vendor.vendor
});

export default connect(
  mapStateToProps,
  { logoutUser, getVendor }
)(withRouter(VendorList));
