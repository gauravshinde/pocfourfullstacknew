import React, { Component } from "react";
// import MiniNavbar from './../../PocFirstSecondItration/ReusableComponents/MiniNavbar';
import HistoryTableBodyComponent from "./../../PocFirstSecondItration/ReusableComponents/TableReusableComponent/HistoryTableComponent/HistoryTableBodyComponent";
import HistoryTableHeadComponent from "./../../PocFirstSecondItration/ReusableComponents/TableReusableComponent/HistoryTableComponent/HistoryTableHeadComponent";
// import PaginationComponent from "./../../PocFirstSecondItration/ReusableComponents/PaginationComponent";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
import ButtonComponent from "../../SmallComponents/ButtonComponent";
import InputComponent from "../../SmallComponents/InputComponent";
import dateFns from "date-fns";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../../store/validation/is-Empty";
import { get_history_details } from "../../../../store/actions/SeatingPageAction/SeatingPageAction";

const totalRecordsInOnePage = 10;

export class MainHistory extends Component {
  constructor() {
    super();
    this.state = {
      allGetHistory: [],
      show: "all",
      search: "",
      viewHistoryPage: {},
      currentPagination: 1
    };
  }

  componentDidMount() {
    if (this.props.get_history_details) {
      this.props.get_history_details();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    // console.log(nextprops.allhistory);
    if (!isEmpty(nextprops.allhistory) !== !isEmpty(nextstate.allGetHistory)) {
      return {
        allGetHistory: nextprops.allhistory
      };
    }
    return null;
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  /**************************************
   * @DESC -SHOWALL || NON SEATED TOGGLER
   **************************************/
  handleShow = value => e => {
    console.log("Clicked");
    this.setState({
      show: value
    });
  };

  /**************************************
   * @DESC - SEARCH HANDLE
   **************************************/
  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  /**************************************
   * @DESC - ON CHANGE PAGINATION HANDLE
   **************************************/

  onChangePagination = page => {
    // console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  /**************************************
   * @DESC - ONCLICK HISTORY PAGE
   **************************************/
  OnHistoryPageViewDetailsHandler = value => e => {
    // e.preventDefault();
    this.props.history.push({
      pathname: "/all-history-details",
      // search: '?query=abc',
      state: { viewHistoryPage: value }
    });
    console.log(value);
  };

  renderdinnerHistory = () => {
    const { allGetHistory, currentPagination } = this.state;
    console.log(allGetHistory);

    let filtereddata = [];
    let Request_time = "";
    let Seated_time = "";

    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      filtereddata = allGetHistory.filter(allhistory => {
        if (search.test(allhistory.name)) {
          return allhistory;
        }
      });
      // console.log(filtereddata);
    } else {
      filtereddata = allGetHistory;
    }
    if (this.state.show === "Completed") {
      filtereddata = filtereddata.filter(
        allhistory => allhistory.dinner_status === "Completed"
      );
      console.log(filtereddata);
    }
    if (this.state.show === "Cancelled") {
      filtereddata = filtereddata.filter(
        allhistory => allhistory.dinner_status === "Cancelled"
      );
      console.log(filtereddata);
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.map(
        (dinner, index) =>
          index >= (currentPagination - 1) * totalRecordsInOnePage &&
          index < currentPagination * totalRecordsInOnePage &&
          ((Request_time = dateFns.format(dinner.Request_Date, "hh : mm A")),
          (Seated_time = dateFns.format(dinner.Seating_Date, "hh : mm A")),
          (
            <React.Fragment key={index}>
              <HistoryTableBodyComponent
                key={index}
                code={++index}
                name={dinner.name.replace("null", " ")}
                no={dinner.phone_number}
                adults={dinner.adults}
                kids={dinner.kids}
                highchair={dinner.highchair}
                handicap={dinner.handicap}
                special_occassion={dinner}
                imgclass={"seating-logo-img"}
                time={Request_time}
                timeseated={Seated_time}
                statusNew={dinner.dinner_status}
                newordertype={
                  dinner.order_type === "walkin" ? (
                    <img
                      src={require("../../../../pocsrcone/assets/Images/walking-img.svg")}
                      className="img-fluid"
                      style={{ width: "20px", height: "20px" }}
                      alt="walk_in"
                    />
                  ) : dinner.order_type === "reservation" ? (
                    <img
                      src={require("../../../../pocsrcone/assets/Images/reservation.svg")}
                      className="img-fluid"
                      style={{ width: "20px", height: "20px" }}
                      alt="reservation"
                    />
                  ) : dinner.order_type === "waitlist" ? (
                    <img
                      src={require("../../../../pocsrcone/assets/Images/clock-circular-outline.svg")}
                      className="img-fluid"
                      style={{ width: "20px", height: "20px" }}
                      alt="wait_list"
                    />
                  ) : null
                }
                // img6={require("../../../assets/Images/seating/ocassion.svg")}
                viewHistory={this.OnHistoryPageViewDetailsHandler(dinner)}
              />
            </React.Fragment>
          ))
      );
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid vendor-details-container showallbutton-container">
          <div className="row">
            <div className="col-12 text-center">
              <ButtonComponent
                buttontype={"button"}
                buttonclass={
                  this.state.show === "all"
                    ? "btn active-showall-button showall-button"
                    : "btn showall-button"
                }
                buttontext={"Show All"}
                handleOnClick={this.handleShow("all")}
              />
              <ButtonComponent
                buttontype={"button"}
                buttonclass={
                  this.state.show === "Completed"
                    ? "btn active-showall-button showall-button"
                    : "btn showall-button"
                }
                buttontext={"Show Completed"}
                handleOnClick={this.handleShow("Completed")}
              />
              <ButtonComponent
                buttontype={"button"}
                buttonclass={
                  this.state.show === "Cancelled"
                    ? "btn active-showall-button showall-button"
                    : "btn showall-button"
                }
                buttontext={"Show Cancelled"}
                handleOnClick={this.handleShow("Cancelled")}
              />
            </div>
          </div>
        </div>

        {/**************************
         * TABLE SECTION
         **************************/}

        <div className="container-fluid all-page-container-padding table-container ">
          <div className="col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center">
            <div>
              <h4 className={"vendor"}>
                {this.state.show === "all" ? "All History" : ""}
                {this.state.show === "Completed" ? "All Completed" : ""}
                {this.state.show === "Cancelled" ? "All Cancelled" : ""}
              </h4>
            </div>

            <div className="d-flex justify-content-between">
              {/*w-25*/}
              <InputComponent
                img={require("./../../../assets/Images/search.svg")}
                searchClass={"search-label"}
                onChange={this.onSearchChange}
                value={this.state.search}
                place={"search"}
                inputclass={
                  "form-control input-bottomblack mb-0 pl-3 input-search"
                }
              />
              {/* <img
                src={require("./../../assets/Images/sort-button-with-three-lines.svg")}
                alt="sort-button-with-three-lines"
              />
              <img
                src={require("./../../assets/Images/filter.svg")}
                alt="filter-img"
              /> */}
            </div>
          </div>
          <div className="history-container">
            <div className="table-responsive history-table">
              <table className="table table-striped mb-0 ">
                <thead>
                  <HistoryTableHeadComponent
                    code={"#"}
                    details={"Details"}
                    ordertype={"Order Type"}
                    partysize={"Party Size"}
                    request={"Requested Time"}
                    seatedtime={"Time Seated"}
                    status={"Status"}
                  />
                </thead>

                <tbody>{this.renderdinnerHistory()}</tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="p4v2-menu-pagination">
          <Pagination
            onChange={this.onChangePagination}
            current={this.state.currentPagination}
            defaultPageSize={totalRecordsInOnePage}
            total={this.state.allGetHistory.length}
            showTitle={false}
          />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  allhistory: state.allpendingDetails.get_all_history
});

export default connect(mapStateToProps, { get_history_details })(
  withRouter(MainHistory)
);
