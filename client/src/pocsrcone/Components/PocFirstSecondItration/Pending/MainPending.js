import React, { Component } from "react";
import MiniNavbar from "./../../PocFirstSecondItration/ReusableComponents/MiniNavbar";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
// import PaginationComponent from "./../../PocFirstSecondItration/ReusableComponents/PaginationComponent";
import PendingTableBodyComponent from "./../../PocFirstSecondItration/ReusableComponents/TableReusableComponent/PendingTableComponent/PendingTableBodyComponent";
import PendingTableHeadComponent from "./../../PocFirstSecondItration/ReusableComponents/TableReusableComponent/PendingTableComponent/PendingTableHeadComponent";
// import dateFns from "date-fns";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../../store/validation/is-Empty";
import { get_pending_details } from "./../../../../store/actions/SeatingPageAction/SeatingPageAction";

import openSocket from "socket.io-client";
import io from "socket.io-client";
import { socketIpAddress } from "../../../../config/Keys";

const totalRecordsInOnePage = 10;

export class MainPending extends Component {
  mySocket = openSocket(`${socketIpAddress}`);
  constructor() {
    super();
    this.state = {
      allPendingDetails: {},
      viewPendingPage: {},
      currentPagination: 1
    };
  }

  componentDidMount() {
    // if (this.props.get_pending_details) {
    //   this.props.get_pending_details();
    // }

    let vendorID = this.props.auth.user._id;
    let data = this.props.auth.user._id;
    this.mySocket.emit("pendingData", vendorID);

    this.socket = io(`${socketIpAddress}`);
    this.socket.on(["reservationPending", data], msg => {
      console.log("Data From Get Data", msg);
      this.setState({ allPendingDetails: msg });
    });
  }

  // static getDerivedStateFromProps(nextprops, nextstate) {
  //   //console.log(nextprops.allpending);
  //   if (nextprops.allpending !== nextstate.allPendingDetails) {
  //     return { allPendingDetails: nextprops.allpending };
  //   }
  //   return null;
  // }

  onCallClickHandler = () => {
    //console.log("Hii Call");
  };

  onMessageClickHandler = () => {
    //console.log("Hii Message");
  };

  onNotificationClickHandler = () => {
    //console.log("Hii Notification");
  };

  /**************************************
   * @DESC - ONCLICK PANDING VIEW PAGE
   **************************************/
  onViewClickHandler = value => e => {
    // e.preventDefault();
    this.props.history.push({
      pathname: "/all-pending-details",
      // search: '?query=abc',
      state: { viewPendingPage: value }
    });
    //console.log(value);
  };

  onEditClickHandler = value => e => {
    //console.log("Hii Edit", value);
  };

  onChangePagination = page => {
    // console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  renderAllPendingDataDetails = () => {
    const { allPendingDetails, currentPagination } = this.state;
    //console.log(allPendingDetails);

    let filtereddata = [];
    let time = "";
    let H = "";
    let h = "";
    let ampm = "";
    let newtime = "";
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      filtereddata = allPendingDetails.filter(allpending => {
        if (search.test(allpending.name)) {
          return allpending;
        }
      });
      // console.log(filtereddata);
    } else {
      filtereddata = allPendingDetails;
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.map(
        (data, index) =>
          index >= (currentPagination - 1) * totalRecordsInOnePage &&
          index < currentPagination * totalRecordsInOnePage &&
          ((time = data.time),
          (H = +time.substr(0, 2)),
          (h = H % 12 || 12),
          (ampm = H < 12 ? " AM" : " PM"),
          (newtime = h + time.substr(2, 3) + ampm),
          (
            <React.Fragment key={index}>
              <PendingTableBodyComponent
                checkid={"customCheck1"}
                code={++index}
                name={data.name.replace("null", " ")}
                Time={newtime}
                adults={data.adults}
                onDropDownList={data}
                kids={data.kids}
                highchair={data.highchair}
                handicap={data.handicap}
                onCallClick={this.onCallClickHandler()}
                onMessageClick={this.onMessageClickHandler()}
                onNotificationClick={this.onNotificationClickHandler()}
                onOcassionClick={data}
                onViewClick={this.onViewClickHandler(data)}
                onEditClick={this.onEditClickHandler(data)}
                date={data.Date}
                // date={
                //   data.Request_Date && data.Date && data.Request_Date
                //     ? data.Date
                //     : dateFns.format(data.Request_Date, ' DD-MM-YYYY')
                // }
              />
            </React.Fragment>
          ))
      );
    }
  };

  render() {
    return (
      <React.Fragment>
        <MiniNavbar
          text={"All Pending"}
          img={require("../../../assets/Images/search.svg")}
          name={"search"}
          type={"text"}
          inputclass={"form-control pocfirst-custom-input"}
          place={"Search"}
        />
        <div className="container-fluid pending-container">
          <div className="pending-table">
            <table
              className="table table-striped mb-0"
              style={{ overflow: "auto" }}
            >
              <thead>
                <PendingTableHeadComponent
                  code={"#"}
                  details={"Details"}
                  status={"Request Status"}
                  ordertype={"Order Type"}
                  partysize={"Party Size"}
                  date={"Date"}
                />
              </thead>
              <tbody>{this.renderAllPendingDataDetails()}</tbody>
            </table>
          </div>
        </div>
        <div className="p4v2-menu-pagination">
          <Pagination
            onChange={this.onChangePagination}
            current={this.state.currentPagination}
            defaultPageSize={totalRecordsInOnePage}
            total={this.state.allPendingDetails.length}
            showTitle={false}
          />
        </div>
        {/* <PaginationComponent /> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  allpending: state.allpendingDetails.get_all_pending
});

export default connect(mapStateToProps, {
  get_pending_details
})(withRouter(MainPending));

//04-03-2020
