import React, { Component } from "react";
import AllSeatTableHeadComponent from "../../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/AllSeatDetails/AllSeatTableComponent/AllSeatTableHeadComponent";
import AllSeatTableBodyComponent from "../../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/AllSeatDetails/AllSeatTableComponent/AllSeatTableBodyComponent";
import EditDinerPopup from "../../../../Components/PocFirstSecondItration/ReusableComponents/AllPopUps/EditDinerPopup";
import CancelReservationPopup from "../../../../Components/PocFirstSecondItration/ReusableComponents/AllPopUps/CancelReservationPopup";
import LocationPopup from "../../../../Components/PocFirstSecondItration/ReusableComponents/AllPopUps/LocationPopup";
import NavbarDashboard from "../../../../../components/Dassboard/NavbarDashboard";
import dateFns from "date-fns";

import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import isEmpty from "./../../../../../PocTwo/store/validation/is-Empty";
import { get_pending_details } from "./../../../../../store/actions/SeatingPageAction/SeatingPageAction";

export class PendingViewPage extends Component {
  constructor() {
    super();
    this.state = {
      dinnernewid: {},
    };
  }

  componentDidMount() {
    if (this.props.get_pending_details) {
      this.props.get_pending_details(this.state.dinnernewid);
    }
    // console.log(this.props.location.state.viewDinerPage);
    // if (this.props.dinner.length !== 0) {
    //   let menuItem = this.props.dinner.find(
    //     dinner => dinner._id === this.props.location.state.viewDinerPage
    //   );
    //   console.log(menuItem);
    //   this.setState({
    //     ...menuItem
    //   });
    // }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    if (nextprops.location.state.viewPendingPage !== nextstate.dinnernewid) {
      return {
        dinnernewid: nextprops.location.state.viewPendingPage,
      };
    }

    return null;
  }

  /********************
    Row One Start
********************/
  renderRowOne = () => {
    const { dinnernewid } = this.state;
    const partysize = parseInt(dinnernewid.kids) + parseInt(dinnernewid.adults);
    return (
      <React.Fragment>
        <div className="top-row-class view-seating-page-class p-3 mt-3">
          <div className="row">
            <div className="col div1">
              <h5 className="boldtext">{dinnernewid.name}</h5>

              <div className="mt-3">
                <img
                  src={require("../../../../../pocsrcone/assets/Images/seating/call.svg")}
                  alt="call"
                  className="details-logo-img-seating"
                />
                <img
                  src={require("../../../../../pocsrcone/assets/Images/seating/message.svg")}
                  alt="message"
                  className="details-logo-img-seating"
                />
                <img
                  src={require("../../../../../pocsrcone/assets/Images/seating/bell.svg")}
                  alt="bell"
                  className="details-logo-img-seating"
                />
              </div>
            </div>
            <div className="col div1">
              <h5 className="boldtext">Request Status</h5>

              <div className="form-group">
                <select
                  className="select-class form-control"
                  id="exampleFormControlSelect1"
                >
                  <option>Accepted</option>
                  <option>Rejected</option>
                  <option>Pending</option>
                </select>
              </div>
              <div className="order-type-div">
                <h5 className="boldtext">Order Type</h5>

                <img
                  src={require("../../../../../pocsrcone/assets/Images/seating/online-booking.svg")}
                  alt="walk-in"
                  className="img-fluid walk-in-img"
                  style={{ width: "20px", height: "20px" }}
                />
              </div>
            </div>
            <div className="col div1">
              <h5 className="boldtext">Party Size : {partysize}</h5>
              <div className="d-flex justify-content-start">
                <h6 className="normaltext">Adults: {dinnernewid.adults}</h6>
                <h6 className="normaltext">Kids: {dinnernewid.kids}</h6>
              </div>
              <div className="d-flex justify-content-start">
                <h6 className="normaltext">
                  High-Chair: {dinnernewid.highchair}
                </h6>
                <h6 className="normaltext">Handicap: {dinnernewid.handicap}</h6>
              </div>
            </div>
            <div className="col div1">
              <h5 className="boldtext">Wait Time</h5>
              <span className="d-flex">
                <h5 className="normaltextone">25</h5>
                <img
                  src={require("../../../../../pocsrcone/assets/Images/seating/edit.svg")}
                  alt="edit"
                  className="img-fluid edit-img"
                />
              </span>
              <h5 className="normaltextone">10:00</h5>
            </div>
            <div className="col div1">
              <h5 className="boldtext">Seating Status</h5>
              <p>{dinnernewid.dinner_status}</p>
              {/* <div className='form-group'>
                <select
                  className='select-class form-control'
                  id='exampleFormControlSelect1'
                >
                  <option>Not Seated</option>
                  <option>Seated</option>
                  <option>Completed</option>
                  <option>Cancelled</option>
                </select>
              </div> */}
            </div>
            <div className="col d-flex align-items-center justify-content-around newdiv">
              <EditDinerPopup />
              {/* <img
                src={require("../../assets/Images/seating/pen.svg")}
                alt="pen"
                className="details-logo-img"
              /> */}
              <CancelReservationPopup />
              {/* <img
                src={require("../../assets/Images/seating/Menu.svg")}
                alt="cancel"
                className="img-fluid cancel-img"
              /> */}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  /********************
    Row One End
********************/

  /********************
    Row Two Start
********************/

  renderRowTwo = () => {
    const { dinnernewid } = this.state;
    let time = dinnernewid.time;
    let H = +time.substr(0, 2);
    let h = H % 12 || 12;
    let ampm = H < 12 ? " AM" : " PM";
    let newtime = h + time.substr(2, 3) + ampm;
    return (
      <React.Fragment>
        <div className="middle-row-class p-3 mt-3">
          <div className="row">
            <div className="col-12">
              <h4 className="largetext">Details</h4>
              <hr />
            </div>
            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
              <h5 className="boldtext">Phone Number</h5>
              <h6 className="normaltextone">{dinnernewid.phone_number}</h6>
            </div>
            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
              <h5 className="boldtext">ETA</h5>
              <h6 className="normaltextone">{dinnernewid.ETA}</h6>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="d-flex justify-content-center align-items-center">
                <div>
                  <h5 className="boldtext">Location</h5>
                  <h6 className="normaltextone">View Location</h6>
                </div>
                <LocationPopup />
                {/* <img
                    src={require("../../assets/Images/seating/maps-and-flags.svg")}
                    alt="location"
                    className="img-fluid ordertype-img ml-5"
                  /> */}
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
              <h5 className="boldtext">Requested Time</h5>
              <h6 className="normaltextone">
                {dateFns.format(dinnernewid.Request_Date, "HH:mm A")}
              </h6>
            </div>
            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
              <h5 className="boldtext">Seated Time</h5>
              <h6 className="normaltextone">
                {dateFns.format(dinnernewid.Seating_Date, "HH:mm A")}
              </h6>
            </div>
            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 mt-3">
              <h5 className="boldtext">Point of Entry</h5>
              <h6 className="normaltextone text-capitalize">
                {dinnernewid.entry_point}
              </h6>
            </div>
            <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 mt-3">
              <h5 className="boldtext">Ocassion</h5>
              <h6 className="normaltextone">{dinnernewid.special_occassion}</h6>
            </div>
            <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 mt-3">
              <h5 className="boldtext">Seating Preferences</h5>
              <h6 className="normaltextone">
                <div className="row">
                  {!isEmpty(dinnernewid.seating_preference) &&
                    dinnernewid.seating_preference.map((singleitem, index) => {
                      return (
                        <React.Fragment key={index}>
                          <p className="col-sm-3">{singleitem}</p>
                        </React.Fragment>
                      );
                    })}
                </div>
              </h6>
            </div>

            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 mt-3">
              <h5 className="boldtext">Requested Date</h5>
              <h6 className="normaltextone">
                {dateFns.format(dinnernewid.Request_Date, " DD MMMM YYYY")}
              </h6>
            </div>
            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 mt-3">
              <h5 className="boldtext">Seated Date</h5>
              <h6 className="normaltextone">
                {dateFns.format(dinnernewid.Seating_Date, " DD MMMM YYYY")}
              </h6>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  /********************
    Row Two End
********************/

  /********************
    Row Three Start
********************/

  renderRowThree = () => {
    return (
      <React.Fragment>
        <div className="bottom-row-class p-3">
          <div className="row ">
            <div className="col-12">
              <h4 className="largetext">Edit Log</h4>
              <hr />
            </div>
            <div className="col-12">
              <table className="table table-borderless edit-log-table">
                <thead>
                  <AllSeatTableHeadComponent
                    date={"Date"}
                    time={"Time"}
                    operation={"Operation"}
                    changeby={"Operation Change by"}
                  />
                </thead>
                <tbody>
                  <AllSeatTableBodyComponent
                    date={"13th August, 2019"}
                    time={"06:30 PM"}
                    operation={
                      "Seating status changed from not seated to seated"
                    }
                    changeby={"You"}
                  />
                  <AllSeatTableBodyComponent
                    date={"13th August, 2019"}
                    time={"06:30 PM"}
                    operation={
                      "Admin created a new request with the new timings"
                    }
                    changeby={"You"}
                  />
                  <AllSeatTableBodyComponent
                    date={"13th August, 2019"}
                    time={"06:30 PM"}
                    operation={"Admin Cancelled the request"}
                    changeby={"You"}
                  />
                  <AllSeatTableBodyComponent
                    date={"13th August, 2019"}
                    time={"06:30 PM"}
                    operation={
                      "Kinza requested a time change for the booked table"
                    }
                    changeby={"Kinza"}
                  />
                  <AllSeatTableBodyComponent
                    date={"13th August, 2019"}
                    time={"06:30 PM"}
                    operation={"Admin accepted the request for table booking."}
                    changeby={"You"}
                  />

                  <AllSeatTableBodyComponent
                    date={"13th August, 2019"}
                    time={"06:30 PM"}
                    operation={
                      "Kinza booked a table for 2 adults and 2 kids with 2 highchairs requested"
                    }
                    changeby={"Kinza"}
                  />
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  /********************
    Row Three End
********************/

  render() {
    const { dinnernewid } = this.state;
    console.log(dinnernewid);
    return (
      <React.Fragment>
        <NavbarDashboard />
        {/* <MainHeader /> */}
        <div className="container-fluid allseat-container">
          <Link to="/vendor-seating" className="text-decoration-none">
            <h4 className="largetext text-capitalize">
              All Seats > {dinnernewid.name}
            </h4>
          </Link>

          {this.renderRowOne()}
          {this.renderRowTwo()}
          {this.renderRowThree()}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
  allpending: state.allpendingDetails.get_all_pending,
});

export default connect(mapStateToProps, {
  get_pending_details,
})(withRouter(PendingViewPage));
