import React, { Component } from "react";
import { Link } from "react-router-dom";
import NavbarDashboard from "../../../../../components/Dassboard/NavbarDashboard";
import LargeText from "../../../SmallComponents/LargeText";
import InputComponent from "../../../SmallComponents/InputComponent";
import ButtonComponent from "../../../SmallComponents/ButtonComponent";
import TextBoxComponent from "../../../SmallComponents/TextBoxComponent";

export class AddReservation extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      phone_number: "",
      adults: "",
      kids: "",
      highchair: "",
      handicapped: "",
      special_occassion: "",
      errors: {}
    };
  }

  handleAddDinerChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
      error: {}
    });
  };

  handleAddDinerSubmit = e => {
    e.preventDefault();
    const newDiner = {
      name: this.state.name,
      phone_number: this.state.phone_number,
      adults: this.state.adults,
      kids: this.state.kids,
      highchair: this.state.highchair,
      handicapped: this.state.handicapped,
      special_occassion: this.state.special_occassion
    };
    console.log(newDiner);
    //this.props.registerDiner(newDiner, this.props.history);
  };

  render() {
    const { errors } = this.state;

    return (
      <React.Fragment>
        <NavbarDashboard />
        <div className="container">
          <div className="row">
            <div className="col-md-5 m-auto">
              <LargeText
                largetext={"Add Reservation"}
                largetextclass={"addvendor-text"}
              />
              <form noValidate onSubmit={this.handleAddDinerSubmit}>
                <div className="form-row">
                  <div className="form-group col-xl-6">
                    <InputComponent
                      img={require("./../../../../assets/Images/usernamepic.svg")}
                      alt={"user-img"}
                      labeltext={"Name"}
                      name={"name"}
                      type={"text"}
                      place={"eg. James Bond"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.name}
                      onChange={this.handleAddDinerChange}
                    />
                  </div>
                  <div className="form-group col-xl-6">
                    <InputComponent
                      img={require("./../../../../assets/Images/phone-img.svg")}
                      alt={"Phone-img"}
                      labeltext={"Phone Number"}
                      name={"phone_number"}
                      type={"Number"}
                      place={"eg. 123567890"}
                      inputclass={"form-control input-bottomblack "}
                      value={this.state.phone_number}
                      onChange={this.handleAddDinerChange}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-xl-3">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Adults"}
                      name={"adults"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.adults}
                      onChange={this.handleAddDinerChange}
                    />
                  </div>

                  <div className="form-group col-xl-3">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Kids"}
                      name={"kids"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.kids}
                      onChange={this.handleAddDinerChange}
                    />
                  </div>

                  <div className="form-group col-xl-3">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Highchair"}
                      name={"highchair"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.highchair}
                      onChange={this.handleAddDinerChange}
                      zerror={errors.highchair}
                    />
                  </div>

                  <div className="form-group col-xl-3">
                    <InputComponent
                      imageClass={"d-none"}
                      labeltext={"Handicapped"}
                      name={"handicapped"}
                      type={"number"}
                      place={"select"}
                      inputclass={"form-control pl-3 input-bottomblack"}
                      value={this.state.handicapped}
                      onChange={this.handleAddDinerChange}
                      zerror={errors.handicapped}
                    />
                  </div>
                </div>

                <TextBoxComponent
                  imageClass={"d-none"}
                  name={"special_occassion"}
                  type={"text"}
                  labeltext={"Special Ocassion"}
                  place={"eg. Birthday "}
                  Textareaclass={"form-control pl-3 textarea-bottomblack"}
                  value={this.state.special_occassion}
                  onChange={this.handleAddDinerChange}
                  zerror={errors.special_occassion}
                />
                <div>
                  <ButtonComponent
                    buttontype={"submit"}
                    buttonclass={"login-button mb-4"}
                    buttontext={"Save"}
                    handleOnClick={this.nextClickHandler}
                  />
                  <Link to="/vendor-seating">
                    <ButtonComponent
                      buttontype={"button"}
                      buttonclass={"cancel-button mb-4"}
                      buttontext={"Cancel"}
                      handleOnClick={this.cancelClickHandler}
                    />
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AddReservation;
