import React, { Component } from "react";
// import { Link } from 'react-router-dom';
import MiniNavbar from "../../PocFirstSecondItration/ReusableComponents/MiniNavbar";
import Calender from "./InsidePage/Calender";

export class MainReservation extends Component {
  render() {
    return (
      <React.Fragment>
        <MiniNavbar
          text={"All Reservations"}
          img={require("../../../assets/Images/search.svg")}
          name={"search"}
          type={"text"}
          inputclass={"form-control pocfirst-custom-input"}
          place={"Search"}
        />

        {/* <div id='main-calender'>
          <Link
            to={{
              pathname: '/add-reservation'
            }}
            className='btn button-width button-gradient d-flex justify-content-center align-items-center'
          >
            Add Reservation
          </Link>
          <br />
        </div> */}
        <Calender />
      </React.Fragment>
    );
  }
}

export default MainReservation;
