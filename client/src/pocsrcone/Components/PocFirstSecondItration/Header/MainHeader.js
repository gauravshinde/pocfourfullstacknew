import React, { Component } from "react";
import ButtonComponent from "./../../ReusableComponents/SmallReusableComponents/ButtonComponent";
import { NavLink } from "react-router-dom";
import { MainPending } from "./../Pending/MainPending";
import { MainSeating } from "./../Seating/MainSeating";
import { MainHistory } from "./../History/MainHistory";
import { MainReservation } from "./../Reservation/MainReservation";

export class MainHeader extends Component {
  constructor() {
    super();
    this.state = {
      activeMenu: "seating"
    };
  }

  /**********************************
        active Menubar Handler Css
*************************************/
  btnonClickHandler = linkText => {
    this.setState({
      activeMenu: linkText
    });
  };

  /**********************************
        Render  Pending Page
  *************************************/
  renderPending = () => {
    return (
      <>
        <MainPending />
      </>
    );
  };

  /**********************************
        Render  Seating Page
  *************************************/
  renderSeating = () => {
    return (
      <>
        <MainSeating />
      </>
    );
  };

  /**********************************
        Render  History Page
  *************************************/
  renderHistory = () => {
    return (
      <>
        <MainHistory />
      </>
    );
  };

  /**********************************
        Render  Reservation Page
  *************************************/
  renderReservation = () => {
    return (
      <>
        <MainReservation />
      </>
    );
  };

  /**********************************
        Render Menubar
*************************************/
  renderMenubar = () => {
    const { activeMenu } = this.state;
    return (
      <>
        <div className="container-fluid main-header">
          <NavLink to="/" className="logo-name">
            <h6>Logo</h6>
          </NavLink>

          <div className="menubar">
            <p
              className={
                activeMenu === "pending" ? "active menutext" : "menutext"
              }
              onClick={() => this.btnonClickHandler("pending")}
            >
              Pending
            </p>
            <p
              className={
                activeMenu === "seating" ? "active menutext" : "menutext"
              }
              onClick={() => this.btnonClickHandler("seating")}
            >
              Seating
            </p>
            <p
              className={
                activeMenu === "reservation" ? "active menutext" : "menutext"
              }
              onClick={() => this.btnonClickHandler("reservation")}
            >
              Reservation
            </p>
            <p
              className={
                activeMenu === "history" ? "active menutext" : "menutext"
              }
              onClick={() => this.btnonClickHandler("history")}
            >
              History
            </p>

            <ButtonComponent
              type={"button"}
              buttontext={"Add Diner"}
              buttonclass={"btn button-width button-red"}
            />

            <img
              src={require("../../assets/Images/logout.svg")}
              alt="logout"
              className="img-fluid"
            />
          </div>
        </div>
      </>
    );
  };
  render() {
    const { activeMenu } = this.state;
    return (
      <React.Fragment>
        {this.renderMenubar()}
        {activeMenu === "pending"
          ? this.renderPending()
          : activeMenu === "seating"
          ? this.renderSeating()
          : activeMenu === "history"
          ? this.renderHistory()
          : activeMenu === "reservation"
          ? this.renderReservation()
          : null}
      </React.Fragment>
    );
  }
}

export default MainHeader;
