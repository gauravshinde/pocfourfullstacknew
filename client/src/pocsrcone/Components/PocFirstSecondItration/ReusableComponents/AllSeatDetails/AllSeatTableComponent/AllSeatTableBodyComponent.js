import React from "react";

const AllSeatTableBodyComponent = ({ date, time, operation, changeby }) => {
  return (
    <React.Fragment>
      <tr className="all-seat-table-body-class">
        <td></td>
        <td>{date}</td>
        <td>{time}</td>
        <td>{operation}</td>
        <td>{changeby}</td>
      </tr>
    </React.Fragment>
  );
};

export default AllSeatTableBodyComponent;
