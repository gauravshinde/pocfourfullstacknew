import React from "react";

const AllSeatTableHeadComponent = ({ date, time, operation, changeby }) => {
  return (
    <React.Fragment>
      <tr className="all-seat-table-head-class">
        <th></th>
        <th>{date}</th>
        <th>{time}</th>
        <th>{operation}</th>
        <th>{changeby}</th>
      </tr>
    </React.Fragment>
  );
};

export default AllSeatTableHeadComponent;
