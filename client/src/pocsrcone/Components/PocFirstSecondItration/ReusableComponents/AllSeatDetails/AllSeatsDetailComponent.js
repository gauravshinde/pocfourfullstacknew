import React, { Component } from "react";
import AllSeatTableHeadComponent from "./AllSeatTableComponent/AllSeatTableHeadComponent";
import AllSeatTableBodyComponent from "./AllSeatTableComponent/AllSeatTableBodyComponent";
import { EditDinerPopup } from "./../AllPopUps/EditDinerPopup";
import { CancelReservationPopup } from "./../AllPopUps/CancelReservationPopup";

export class AllSeatsDetailComponent extends Component {
  /********************
    Row One Start
********************/
  renderRowOne = () => {
    return (
      <>
        <div className="row top-row-class p-3 mt-3">
          <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 div1">
            <h5 className="boldtext">Kinza</h5>

            <div className="mt-3">
              <img
                src={require("../../assets/Images/seating/call.svg")}
                alt="call"
                className="details-logo-img"
              />
              <img
                src={require("../../assets/Images/seating/message.svg")}
                alt="message"
                className="details-logo-img"
              />
              <img
                src={require("../../assets/Images/seating/bell.svg")}
                alt="bell"
                className="details-logo-img"
              />
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 div1">
            <h5 className="boldtext">Request Status</h5>
            {/* <MainDropDown /> */}
            <div className="form-group">
              <select
                className="select-class form-control"
                id="exampleFormControlSelect1"
              >
                <option>Accepted</option>
                <option>Rejected</option>
                <option>Pending</option>
              </select>
            </div>
            <div className="order-type-div">
              <h5 className="boldtext">Order Type</h5>

              <img
                src={require("../../assets/Images/seating/pedestrian-walking.svg")}
                alt="walk-in"
                className="img-fluid walk-in-img"
              />
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 div1">
            <h5 className="boldtext">Party Size : 4</h5>
            <div className="d-flex justify-content-around">
              <h6 className="normaltext">Adults: 2</h6>
              <h6 className="normaltext">Kids: 2</h6>
            </div>
            <div className="d-flex justify-content-around">
              <h6 className="normaltext">High-Chair: 2</h6>
              <h6 className="normaltext">Handicap: 0</h6>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 div1">
            <h5 className="boldtext">Wait Time</h5>
            <span className="d-flex">
              <h5 className="normaltext">25</h5>
              <img
                src={require("../../assets/Images/seating/edit.svg")}
                alt="edit"
                className="img-fluid edit-img"
              />
            </span>
            <h5 className="normaltext">10:00</h5>
          </div>
          <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 div1">
            <h5 className="boldtext">Seating Status</h5>
            <div className="form-group">
              <select
                className="select-class form-control"
                id="exampleFormControlSelect1"
              >
                <option>Not Seated</option>
                <option>Seated</option>
                <option>Completed</option>
                <option>Cancelled</option>
              </select>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 div2">
            <div className="mt-4">
              <EditDinerPopup />
              {/* <img
                src={require("../../assets/Images/seating/pen.svg")}
                alt="pen"
                className="details-logo-img"
              /> */}
              <CancelReservationPopup />
              {/* <img
                src={require("../../assets/Images/seating/cancel.svg")}
                alt="cancel"
                className="details-logo-img"
              /> */}
            </div>
          </div>
        </div>
      </>
    );
  };

  /********************
    Row One End
********************/

  /********************
    Row Two Start
********************/

  renderRowTwo = () => {
    return (
      <>
        <div className="row middle-row-class">
          <div className="container-fluid p-3">
            <div className="row">
              <div className="col-12">
                <h4 className="largetext">Details</h4>
                <hr />
              </div>
            </div>
            <div className="row mb-3">
              <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <h5 className="boldtext">Phone Number</h5>
                <h6 className="normaltext">+911234567890</h6>
              </div>
              <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <h5 className="boldtext">ETA</h5>
                <h6 className="normaltext">2 Hrs 25 Mins</h6>
              </div>
              <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"></div>

              <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <h5 className="boldtext">Requested Time</h5>
                <h6 className="normaltext">06:30 PM</h6>
              </div>
              <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <h5 className="boldtext">Seated Time</h5>
                <h6 className="normaltext">06:30 PM</h6>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <h5 className="boldtext">Point of Entry</h5>
                <h6 className="normaltext">Manually Added</h6>
              </div>
              <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <h5 className="boldtext">Ocassion</h5>
                <h6 className="normaltext">25th Wedding Anniversary</h6>
              </div>
              <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <h5 className="boldtext">Seating Preferences</h5>
                <h6 className="normaltext">Outdoor</h6>
              </div>

              <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <h5 className="boldtext">Requested Date</h5>
                <h6 className="normaltext">13Th August, 2019</h6>
              </div>
              <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <h5 className="boldtext">Seated Date</h5>
                <h6 className="normaltext">15Th August, 2019</h6>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  /********************
    Row Two End
********************/

  /********************
    Row Three Start
********************/

  renderRowThree = () => {
    return (
      <>
        <div className="row bottom-row-class">
          <div className="container-fluid p-3">
            <div className="row">
              <div className="col-12">
                <h4 className="largetext">Edit Log</h4>
                <hr />
              </div>
            </div>
            <table className="table table-borderless edit-log-table">
              <thead>
                <AllSeatTableHeadComponent
                  date={"Date"}
                  time={"Time"}
                  operation={"Operation"}
                  changeby={"Operation Change by"}
                />
              </thead>
              <tbody>
                <AllSeatTableBodyComponent
                  date={"13th August, 2019"}
                  time={"06:30 PM"}
                  operation={"Seating status changed from not seated to seated"}
                  changeby={"You"}
                />
                <AllSeatTableBodyComponent
                  date={"13th August, 2019"}
                  time={"06:30 PM"}
                  operation={"Admin created a new request with the new timings"}
                  changeby={"You"}
                />
                <AllSeatTableBodyComponent
                  date={"13th August, 2019"}
                  time={"06:30 PM"}
                  operation={"Admin Cancelled the request"}
                  changeby={"You"}
                />
                <AllSeatTableBodyComponent
                  date={"13th August, 2019"}
                  time={"06:30 PM"}
                  operation={
                    "Kinza requested a time change for the booked table"
                  }
                  changeby={"Kinza"}
                />
                <AllSeatTableBodyComponent
                  date={"13th August, 2019"}
                  time={"06:30 PM"}
                  operation={"Admin accepted the request for table booking."}
                  changeby={"You"}
                />

                <AllSeatTableBodyComponent
                  date={"13th August, 2019"}
                  time={"06:30 PM"}
                  operation={
                    "Kinza booked a table for 2 adults and 2 kids with 2 highchairs requested"
                  }
                  changeby={"Kinza"}
                />
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  };

  /********************
    Row Three End
********************/

  render() {
    return (
      <React.Fragment>
        {/* <MainHeader /> */}
        <div className="container-fluid allseat-container">
          <div className="row">
            <div className="col-12 p-0">
              <h4 className="largetext">All Seats > Kinza</h4>
            </div>
          </div>
          {this.renderRowOne()}
          {this.renderRowTwo()}
          {this.renderRowThree()}
        </div>
      </React.Fragment>
    );
  }
}

export default AllSeatsDetailComponent;
