import React from 'react';

const SeatingTableHeadComponent = ({
  code,
  details,
  ordertype,
  partysize,
  waittime,
  status,
  detailsone
}) => {
  return (
    <React.Fragment>
      <tr className='seating-table-head-class'>
        <th>
          <form>
            <div className='custom-control custom-checkbox'>
              <input
                type='checkbox'
                className='custom-control-input'
                id='customCheck'
                name='example1'
              />
              <label
                className='custom-control-label'
                htmlFor='customCheck'
              ></label>
            </div>
          </form>
        </th>
        <th>{code}</th>
        <th>{details}</th>
        <th className='text-center'>{ordertype}</th>
        <th className='text-center'>{partysize}</th>
        <th>{waittime}</th>
        <th>{detailsone}</th>
        <th>{status}</th>
        <th></th>
        <th></th>
      </tr>
    </React.Fragment>
  );
};

export default SeatingTableHeadComponent;
