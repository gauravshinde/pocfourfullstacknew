import React from 'react';
import { NavLink } from 'react-router-dom';
import SendSMS from '../../AllPopUps/SendSMS';
import OcassionPopup from '../../AllPopUps/OcassionPopup';
import 'react-dropdown/style.css';
import { Dropdown } from './../../MainDropDown/Dropdown';

const countryCodeList = ['Not Seated', 'Seated'];

const SeatingTableBodyComponent = ({
  checkid,
  code,
  name,
  img1,
  img2,
  img3,
  img4,
  img5,
  img6,
  img7,
  img8,
  img9,
  imgclass,
  imgclass1,
  imgclass2,
  TotalTime,
  Timer,
  Time,
  location,
  view,
  confirm,
  eta,
  etatime
}) => {
  return (
    <React.Fragment>
      <tr className='seating-table-body-class'>
        <td>
          <form>
            <div className='custom-control custom-checkbox'>
              <input
                type='checkbox'
                className='custom-control-input'
                id={checkid}
                name='example1'
              />
              <label className='custom-control-label' htmlFor={checkid}></label>
            </div>
          </form>
        </td>
        <td>{code}</td>
        <td>
          <div className='bold-font'>{name}</div>

          <div className='mt-2'>
            <img src={img1} alt='call' className={imgclass} />
            <SendSMS />
            {/* <img src={img2} alt="message" className={imgclass} /> */}
            <img src={img3} alt='bell' className={imgclass} />
          </div>
        </td>
        <td className='text-center'>
          <img src={img4} alt='order type img' className={imgclass1} />
          <div>{Time}</div>
        </td>
        <td className='d-flex flex-column align-items-center'>
          <div className='w-100 d-flex justify-content-around text-center '>
            <div>
              <span>2</span>
              <br />
              <span>Adult</span>
            </div>
            <div>
              <span>2</span>
              <br />
              <span>Kids</span>
            </div>
          </div>
          <div className='w-100 d-flex justify-content-around text-center '>
            <div>
              <span>2</span>
              <br />
              <span>High-Chair</span>
            </div>
            <div>
              <span>2</span>
              <br />
              <span>Handicap</span>
            </div>
          </div>
        </td>
        <td>
          <span>
            {TotalTime}
            <img src={img8} alt='pen-img' className={imgclass2} />
          </span>
          <br />
          <span>{Timer}</span>
        </td>
        <td>
          <div className='d-flex justify-content-between align-items-center'>
            <div>
              <p className='bold-font'>{location}</p>
              <p>{view}</p>
            </div>
            <img src={img9} alt='location' className={imgclass1} />
          </div>
        </td>
        <td>
          <Dropdown list={countryCodeList} />
          <div className='green-box'>
            <p>{confirm}</p>
            <p className='bold-font'>{eta}</p>
            <p>{etatime}</p>
          </div>

          {/* <div class="form-group">
            <select
              className="select-class form-control"
              id="exampleFormControlSelect1"
            >
              <option>Not Seated</option>
              <option>Seated</option>
              <option>Completed</option>
              <option>Cancelled</option>
            </select>
          </div> */}
        </td>
        <td>
          <OcassionPopup />
          {/* <img src={img5} alt="ocassion" className={imgclass} /> */}
        </td>

        <td>
          <div className='mb-3'>
            <NavLink to='/all-seat-details'>
              <img src={img6} alt='eye' className={imgclass} />
            </NavLink>
          </div>

          <img src={img7} alt='pen' className={imgclass} />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default SeatingTableBodyComponent;
