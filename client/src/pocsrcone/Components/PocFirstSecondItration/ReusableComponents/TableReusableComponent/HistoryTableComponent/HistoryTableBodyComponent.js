import React from "react";
// import { NavLink } from "react-router-dom";
import OcassionHistoryPopup from "../../AllPopUps/OcassionHistoryPopup";

const HistoryTableBodyComponent = ({
  code,
  name,
  no,
  adults,
  kids,
  highchair,
  handicap,
  special_occassion,
  time,
  timeseated,
  statusNew,
  viewHistory,
  newordertype
}) => {
  return (
    <React.Fragment>
      <tr className="history-table-body-class">
        <td>{code}</td>
        <td className="align-middle">
          <div className="bold-font">{name}</div>
          <div>{no}</div>
        </td>
        <td className="text-center align-middle">
          {newordertype}
          {/* <img
            src={require("../../../../../assets/Images/seating/pedestrian-walking.svg")}
            alt="order type img"
            className="ordertype-img"
          /> */}
        </td>
        <td className="d-flex flex-column align-items-center">
          <div className="w-100 d-flex justify-content-around text-center ">
            <div>
              <span>{adults}</span>
              <br />
              <span>Adult</span>
            </div>
            <div>
              <span>{kids}</span>
              <br />
              <span>Kids</span>
            </div>
          </div>
          <div className="w-100 d-flex justify-content-around text-center ">
            <div>
              <span>{highchair}</span>
              <br />
              <span>High-Chair</span>
            </div>
            <div>
              <span>{handicap}</span>
              <br />
              <span>Handicap</span>
            </div>
          </div>
        </td>
        <td className="align-middle">{time}</td>
        <td className="align-middle">{timeseated}</td>
        <td className="align-middle">
          {statusNew}
          {/* <Dropdown list={countryCodeList} /> */}
        </td>

        <td className="align-middle">
          <OcassionHistoryPopup dinner={special_occassion} />
          <img
            src={require("../../../../../assets/Images/seating/eye.svg")}
            alt="eye"
            className="seating-logo-img"
            onClick={viewHistory}
          />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default HistoryTableBodyComponent;
