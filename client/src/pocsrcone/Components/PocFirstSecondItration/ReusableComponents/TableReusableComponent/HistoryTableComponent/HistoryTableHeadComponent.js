import React from "react";

const HistoryTableHeadComponent = ({
  code,
  details,
  ordertype,
  partysize,
  request,
  seatedtime,
  status
}) => {
  return (
    <React.Fragment>
      <tr className="history-table-head-class">
        <th>{code}</th>
        <th>{details}</th>
        <th className="text-center">{ordertype}</th>
        <th className="text-center">{partysize}</th>
        <th>{request}</th>
        <th>{seatedtime}</th>
        <th>{status}</th>

        <th></th>
      </tr>
    </React.Fragment>
  );
};

export default HistoryTableHeadComponent;
