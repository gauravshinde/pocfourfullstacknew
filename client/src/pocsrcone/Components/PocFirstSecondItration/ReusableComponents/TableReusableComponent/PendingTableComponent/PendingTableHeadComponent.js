import React from "react";

const PendingTableHeadComponent = ({
  code,
  details,
  ordertype,
  partysize,
  date,
  status
}) => {
  return (
    <React.Fragment>
      <tr className="pending-table-head-class">
        <th>
          <form>
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                id="customCheck"
                name="example1"
              />
              <label
                className="custom-control-label"
                htmlFor="customCheck"
              ></label>
            </div>
          </form>
        </th>
        <th>{code}</th>
        <th>{details}</th>
        <th>{status}</th>
        <th className="text-center">{ordertype}</th>
        <th className="text-center">{partysize}</th>
        <th>{date}</th>

        <th></th>
        <th></th>
      </tr>
    </React.Fragment>
  );
};

export default PendingTableHeadComponent;
