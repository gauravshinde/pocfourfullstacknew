import React from "react";
//import { NavLink } from "react-router-dom";
import { SendSMS } from "./../../AllPopUps/SendSMS";
import OcassionPopup from "../../AllPopUps/OcassionPopup";
import "react-dropdown/style.css";
import Dropdown from "./../../MainDropDown/Dropdown";

const countryCodeList = ["Pending", "Accept", "Reject"];

const PendingTableBodyComponent = ({
  checkid,
  code,
  name,
  adults,
  kids,
  handicap,
  highchair,
  date,
  Time,
  onCallClick,
  onMessageClick,
  onNotificationClick,
  onOcassionClick,
  onViewClick,
  onEditClick,
  onDropDownList
}) => {
  // console.log(onDropDownList);
  return (
    <React.Fragment>
      <tr className="pending-table-body-class">
        <td>
          <form>
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                id={checkid}
                name="example1"
              />
              <label className="custom-control-label" htmlFor={checkid}></label>
            </div>
          </form>
        </td>
        <td>{code}</td>
        <td>
          <div className="bold-font">{name}</div>

          <div className="mt-2">
            <img
              src={require("../../../../../assets/Images/seating/call.svg")}
              alt="call"
              className="seating-logo-img"
              onClick={onCallClick}
            />
            <SendSMS onMessageClick={onMessageClick} />
            {/* <img src={require('../../../../../assets/Images/seating/message.svg')} alt="message" className='seating-logo-img'
          onClick={onMessageClick} /> */}
            <img
              src={require("../../../../../assets/Images/seating/bell.svg")}
              alt="bell"
              className="seating-logo-img"
              onClick={onNotificationClick}
            />
          </div>
        </td>
        <td className="align-middle">
          <Dropdown list={countryCodeList} data={onDropDownList} />
          {/* <div class="form-group">
            <select
              className="select-class form-control"
              id="exampleFormControlSelect1"
            >
              <option>Pending</option>
              <option>Accept</option>
              <option>Reject</option>
            </select>
          </div> */}
        </td>
        <td className="text-center align-middle">
          <img
            src={require("../../../../../assets/Images/seating/online-booking.svg")}
            alt="order type img"
            className="ordertype-img"
          />
          <div>{Time}</div>
        </td>
        <td className="d-flex flex-column align-items-center">
          <div className="w-100 d-flex justify-content-around text-center ">
            <div>
              <span>{adults}</span>
              <br />
              <span>Adult</span>
            </div>
            <div>
              <span>{kids}</span>
              <br />
              <span>Kids</span>
            </div>
          </div>
          <div className="w-100 d-flex justify-content-around text-center ">
            <div>
              <span>{highchair}</span>
              <br />
              <span>High-Chair</span>
            </div>
            <div>
              <span>{handicap}</span>
              <br />
              <span>Handicap</span>
            </div>
          </div>
        </td>
        <td className="align-middle">{date}</td>

        <td>
          <OcassionPopup onOcassionClick={onOcassionClick} />
          {/* <img src={require('../../../../../assets/Images/seating/ocassion.svg')} alt="ocassion" className='seating-logo-img' />
        onClick={onOcassionClick} */}
        </td>

        <td>
          <div className="mb-3">
            <img
              src={require("../../../../../assets/Images/seating/eye.svg")}
              alt="eye"
              className="seating-logo-img"
              onClick={onViewClick}
            />
          </div>

          <img
            src={require("../../../../../assets/Images/seating/pen.svg")}
            alt="pen"
            className="seating-logo-img"
            onClick={onEditClick}
          />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default PendingTableBodyComponent;

//07-03-2020
