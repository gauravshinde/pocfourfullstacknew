import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "./../SmallReusableComponents/ButtonComponent";
import TextAreaComponent from "./../SmallReusableComponents/TextAreaComponent";
import BigButtonOneComponent from "./../SmallReusableComponents/BigButtonOneComponent";

export class CancelReservationPopup extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      reason: "",
      CancelReservationData: ""
    };
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  /***********************
   * @DESC OnChange Handler
   ***********************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
    console.log(this.state);
  };
  /***************************
   * @DESC Big Button Handler
   ***************************/
  bigButtonHandler = (e, type) => {
    e.preventDefault();
    this.setState({
      CancelReservationData: type
    });
    console.log(type);
  };
  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        {/* <img
          src={require('../../../../assets/Images/seating/Menu.svg')}
          alt='cancel'
          className='img-fluid cancel-img'
          onClick={this.onOpenModal}
        /> */}
        <img
          src={require("../../../../assets/Images/seating/cancel.svg")}
          alt="cancel"
          className="details-logo-img"
          onClick={this.onOpenModal}
        />

        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "cancel-reservation-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="cancel-reservation-popup">
            <div className="popup-head">Cancel Reservation</div>

            <div className="container mt-2">
              <form>
                <div className="row">
                  <label
                    htmlFor="Reasons For Cancellation"
                    className="label-class col-12 mt-4"
                  >
                    Reasons For Cancellation
                  </label>
                  <div className="col-sm-4">
                    <BigButtonOneComponent
                      buttontext={"No Show"}
                      Bigbuttonclass={"btn custom-bigbuttonone"}
                      onClick={e => this.bigButtonHandler(e, "No Show")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonOneComponent
                      buttontext={"Walkaway"}
                      Bigbuttonclass={"btn custom-bigbuttonone"}
                      onClick={e => this.bigButtonHandler(e, "Walkaway")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonOneComponent
                      buttontext={"Left Before Seating"}
                      Bigbuttonclass={"btn custom-bigbuttonone"}
                      onClick={e =>
                        this.bigButtonHandler(e, "Left Before Seating")
                      }
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonOneComponent
                      buttontext={"Left After Seating"}
                      Bigbuttonclass={"btn custom-bigbuttonone"}
                      onClick={e =>
                        this.bigButtonHandler(e, "Left After Seating")
                      }
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonOneComponent
                      buttontext={"Other"}
                      Bigbuttonclass={
                        "btn custom-bigbuttonone custom-bigbuttonone-border-orange"
                      }
                    />
                  </div>
                </div>
                <div className="row">
                  <label
                    htmlFor="Enter Reason"
                    className="label-class col-12 mt-4"
                  >
                    Enter Reason
                  </label>
                  <div className="col">
                    <TextAreaComponent
                      name={"reason"}
                      type={"text"}
                      value={this.state.reason}
                      onChange={this.onChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. Birthday"}
                    />
                  </div>
                </div>
                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseModal}
                  />
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Save"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onCloseModal}
                  />
                </div>
              </form>
              {/* <p className='col-12'>
                  <BigButtonOneComponent
                    labeltext={'Reasons For Cancellation'}
                    inputlabelclass={'label-class'}
                  />
                </p> */}
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default CancelReservationPopup;
