import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "./../SmallReusableComponents/ButtonComponent";
import InputComponent from "./../SmallReusableComponents/InputComponent";

export class WaitTimePopup extends Component {
  constructor() {
    super();
    this.state = {
      waittime: "",
      selectactivebtn: "1",
      open: false
    };
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  /***********************************
    Select Active  Button Handler
***************************************/
  selectActivebtn = no => e => {
    this.setState({
      selectactivebtn: no
    });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
    console.log(this.state);
  };

  render() {
    const { open, selectactivebtn } = this.state;
    return (
      <React.Fragment>
        {/* <img
          src={require("../../assets/Images/seating/call.svg")}
          alt="call"
          className="details-logo-img"
          onClick={this.onOpenModal}
        /> */}
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "waittime-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="waittime-popup">
            <div className="popup-head">Enter Wait Time</div>
            <div className="p-3">
              <InputComponent
                labeltext={"Select Wait Time"}
                inputlabelclass={"label-class"}
                name={"waittime"}
                type={"text"}
                value={this.state.waittime}
                onChange={this.onChange}
                place={"Select"}
                inputclass={"form-control custom-input"}
              />
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 pl-0">
                <div className="btn-group">
                  <button
                    type="button"
                    className={
                      selectactivebtn === "1"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("1")}
                  >
                    00
                  </button>
                  <button
                    type="button"
                    className={
                      selectactivebtn === "2"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("2")}
                  >
                    5-10
                  </button>
                  <button
                    type="button"
                    className={
                      selectactivebtn === "3"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("3")}
                  >
                    10-20
                  </button>
                  <button
                    type="button"
                    className={
                      selectactivebtn === "4"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("4")}
                  >
                    30-45
                  </button>
                  <button
                    type="button"
                    className={
                      selectactivebtn === "5"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("5")}
                  >
                    45+
                  </button>
                </div>
              </div>
            </div>
            <div className="button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Cancel"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.onCloseModal}
              />
              <ButtonComponent
                type={"button"}
                buttontext={"Save"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onCloseModal}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default WaitTimePopup;
