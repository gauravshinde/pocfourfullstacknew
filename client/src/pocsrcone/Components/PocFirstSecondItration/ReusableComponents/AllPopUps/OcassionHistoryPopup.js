import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "./../SmallReusableComponents/ButtonComponent";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { get_history_details } from "./../../../../../store/actions/SeatingPageAction/SeatingPageAction";

export class OcassionHistoryPopup extends Component {
  state = {
    open: false,
    ocassion: ""
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    console.log(nextProps.dinner);
    if (nextProps.dinner !== nextState.ocassion) {
      return {
        ocassion: nextProps.dinner
      };
    }
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  render() {
    const { open, ocassion } = this.state;
    // const { onOcassionClick } = this.props;
    // console.log(ocassion);
    return (
      <React.Fragment>
        <img
          src={require("../../../../assets/Images/seating/ocassion.svg")}
          alt="ocassion"
          className="img-fluid seating-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="ocassion-popup">
            <div className="popup-head">Occasion</div>
            <div className="text">
              <p>{ocassion ? ocassion.special_occassion : null}</p>
            </div>
            <div className="button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Okay"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onCloseModal}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps, {
  get_history_details
})(withRouter(OcassionHistoryPopup));
