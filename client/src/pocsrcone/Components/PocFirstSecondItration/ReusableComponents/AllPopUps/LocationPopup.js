import React, { Component } from 'react';
// import GoogleMapReact from 'google-map-react';
import Modal from 'react-responsive-modal';
import ButtonComponent from './../SmallReusableComponents/ButtonComponent';

export class LocationPopup extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };
  state = {
    open: false
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <img
          src={require('../../../../assets/Images/seating/maps-and-flags.svg')}
          alt='location'
          className='img-fluid ordertype-img ml-5'
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: 'customOverlay',
            modal: 'location-Modal',
            closeButton: 'customCloseButton'
          }}
        >
          <div className='location-popup'>
            <div className='popup-head'>User Location</div>
            <div className='location-div'>
              <div className='row'>
                <div className='col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2'>
                  <h6 className='size-18-black'>Username</h6>
                  <h5 className='size-28-black'>Kinza</h5>
                </div>
                <div className='col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3'>
                  <div className='mt-3'>
                    <img
                      src={require('../../../../assets/Images/seating/call.svg')}
                      alt='call'
                      className='seating-logo-img'
                    />

                    <img
                      src={require('../../../../assets/Images/seating/message.svg')}
                      alt='message'
                      className='seating-logo-img'
                    />
                    <img
                      src={require('../../../../assets/Images/seating/bell.svg')}
                      alt='bell'
                      className='seating-logo-img'
                    />
                  </div>
                </div>
                <div className='col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2'></div>

                <div className='col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3'>
                  <h5 className='size-18-black'>Location</h5>
                  <h6 className='size-18-medium'>Koregaon Park</h6>
                </div>
                <div className='col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2'>
                  <h5 className='size-18-black'>ETA</h5>
                  <h6 className='size-18-medium'>25 Minutes</h6>
                </div>
              </div>
              <div className='map-div'>
                <div style={{ height: '66vh', width: '100%' }}>
                  {/* <GoogleMapReact
                    bootstrapURLKeys={{
                      key: 'AIzaSyBKaOKvpvylo_hx-icZWxmjtQB6WJAk1Hs'
                    }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                  >
                    {/* <AnyReactComponent
                    lat={59.955413}
                    lng={30.337844}
                    text="My Marker"
                  /> *
                  </GoogleMapReact> */}
                </div>
              </div>
            </div>
            <div className='button-div'>
              <ButtonComponent
                type={'button'}
                buttontext={'Close'}
                buttonclass={'btn button-width button-orange'}
                onClick={this.onCloseModal}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default LocationPopup;
