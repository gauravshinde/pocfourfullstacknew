import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "./../SmallReusableComponents/ButtonComponent";
import InputComponent from "../../../../../reusableComponents/InputComponent";
import BigButtonComponent from "./../SmallReusableComponents/BigButtonComponent";
import TextAreaComponent from "../../../../../reusableComponents/TextAreaComponent";
import classnames from "classnames";

export class EditDinnerPopupSeatingPage extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      adult: "",
      kids: "",
      highchair: "",
      handicapped: "",
      ocassion: "",
      EditDinerData: "",
      errors: {}
    };
  }

  /***************************
   * @DESC Pop Up Handlers
   ***************************/

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  /***********************
   * @DESC OnChange Handler
   ***********************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
    console.log(this.state);
  };

  /***************************
   * @DESC Big Button Handler
   ***************************/
  bigButtonHandler = (e, type) => {
    e.preventDefault();
    this.setState({
      EditDinerData: type
    });
    console.log(type);
  };
  render() {
    const { open, errors } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../assets/Images/seating/pen.svg")}
          alt="pen"
          className="img-fluid seating-logo-img"
          onClick={this.onOpenModal}
        />

        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "edit-diner-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Edit Diner</div>

            <div className="container mt-3">
              <form>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext="Adults"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      name="adult"
                      type="text"
                      place="Select"
                      onChange={this.onChange}
                      value={this.state.adult}
                      error={errors.adult}
                      inputclass={classnames("map-inputfield", {
                        invalid: errors.adult
                      })}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext="Kids"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      name="Kids"
                      type="text"
                      place="Select"
                      onChange={this.onChange}
                      value={this.state.Kids}
                      error={errors.Kids}
                      inputclass={classnames("map-inputfield", {
                        invalid: errors.Kids
                      })}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext="Highchair"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      name="highchair"
                      type="text"
                      place="Select"
                      onChange={this.onChange}
                      value={this.state.highchair}
                      error={errors.highchair}
                      inputclass={classnames("map-inputfield", {
                        invalid: errors.highchair
                      })}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext="Handicapped"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      name="handicapped"
                      type="text"
                      place="Select"
                      onChange={this.onChange}
                      value={this.state.handicapped}
                      error={errors.handicapped}
                      inputclass={classnames("map-inputfield", {
                        invalid: errors.handicapped
                      })}
                    />
                  </div>
                </div>
                <div className="row">
                  <label
                    htmlFor="Seating Preference"
                    className="label-class col-12"
                  >
                    Seating Preference
                  </label>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Indoor"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/burger-grey.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Indoor")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Patio"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/healthy-food-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Patio")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Outdoor"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/cafe-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Outdoor")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Bar"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/cafe-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Bar")}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Special Ocassion"}
                      inputlabelclass={"input-label mt-4"}
                      name={"ocassion"}
                      type={"text"}
                      onChange={this.onChange}
                      value={this.state.ocassion}
                      place={"eg. Birthday"}
                      error={errors.ocassion}
                      Textareaclass={classnames(
                        "form-control textarea-custom",
                        {
                          invalid: errors.ocassion
                        }
                      )}
                    />
                  </div>
                </div>
              </form>
            </div>

            <div className="button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Cancel"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.onCloseModal}
              />
              <ButtonComponent
                type={"button"}
                buttontext={"Save"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onCloseModal}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default EditDinnerPopupSeatingPage;
