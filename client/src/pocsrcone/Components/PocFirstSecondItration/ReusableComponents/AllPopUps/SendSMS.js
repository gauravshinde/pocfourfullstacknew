import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import ButtonComponent from './../SmallReusableComponents/ButtonComponent';

export class SendSMS extends Component {
  state = {
    open: false
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <img
          src={require('../../../../assets/Images/seating/message.svg')}
          alt='message'
          className='img-fluid seating-logo-img'
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: 'customOverlay',
            modal: 'sendsms-Modal ',
            closeButton: 'customCloseButton'
          }}
        >
          <div className='sms-popup'>
            <div className='popup-head'>Send SMS</div>
            <div className='text'>
              <p> Dear xyz,</p>
              <p> Only 15 minutes remaining for you to be seated.</p>

              <span>
                Thanks,
                <br />
                Restaurant Name.
              </span>
            </div>
            <div className='button-div'>
              <ButtonComponent
                type={'button'}
                buttontext={'Cancel'}
                buttonclass={'btn button-width button-border-orange'}
                onClick={this.onCloseModal}
              />
              <ButtonComponent
                type={'button'}
                buttontext={'Send'}
                buttonclass={'btn button-width button-orange'}
                onClick={this.onCloseModal}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default SendSMS;
