import React, { Component } from "react";
import ButtonComponent from "../SmallReusableComponents/ButtonComponent";
import Modal from "react-responsive-modal";
import InputComponent from "./../SmallReusableComponents/InputComponent";
import BigButtonComponent from "./../SmallReusableComponents/BigButtonComponent";
import TextAreaComponent from "./../SmallReusableComponents/TextAreaComponent";

export class AddDinerPopup extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      name: "",
      number: "",
      adult: "",
      kids: "",
      highchair: "",
      handicapped: "",
      ocassion: "",
      AddDinerData: ""
      // count: 0
    };
  }

  /****************************
   * @DESC Input Count Handler
   *****************************/

  incrementCount = () => {
    this.setState({
      count: this.state.count + 1
    });
  };

  decrementCount = () => {
    if (this.state.count > 0) {
      this.setState({ count: this.state.count - 1 });
    }
  };

  /**************************
   * @DESC Pop up Handlers
   *************************/

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
    console.log(this.state);
  };

  /***************************
   * @DESC Big Button Handler
   ***************************/
  bigButtonHandler = (e, type) => {
    e.preventDefault();
    this.setState({
      AddDinerData: type
    });
    console.log(type);
  };

  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <ButtonComponent
          type={"button"}
          buttontext={"Add Diner"}
          buttonclass={"btn button-width button-gradient"}
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-diner-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="add-diner-popup">
            <div className="popup-head">Add Diner</div>

            <div className="container mt-3">
              <form>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Name"}
                      inputlabelclass={"label-class"}
                      name={"name"}
                      type={"text"}
                      value={this.state.name}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control custom-input"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Phone Number"}
                      inputlabelclass={"label-class"}
                      name={"number"}
                      type={"number"}
                      value={this.state.number}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control custom-input"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Adults"}
                      inputlabelclass={"label-class"}
                      name={"adult"}
                      type={"text"}
                      value={this.state.count}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control custom-input"}
                    />
                    {/* <div className="count-logo">
                      <i
                        className="fa fa-caret-up"
                        onClick={this.incrementCount}
                      ></i>
                      <i
                        className="fa fa-caret-down"
                        onClick={this.decrementCount}
                      ></i>
                    </div> */}
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Kids"}
                      inputlabelclass={"label-class"}
                      name={"kids"}
                      type={"text"}
                      value={this.state.kids}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control custom-input"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext={"Highchair"}
                      inputlabelclass={"label-class"}
                      name={"highchair"}
                      type={"text"}
                      value={this.state.highchair}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control custom-input"}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext={"Handicapped"}
                      inputlabelclass={"label-class"}
                      name={"handicapped"}
                      type={"text"}
                      value={this.state.handicapped}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control custom-input"}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <BigButtonComponent
                      labeltext={"Seating Preference"}
                      inputlabelclass={"label-class"}
                      buttontext={"Indoor"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/burger-grey.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Indoor")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Patio"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/healthy-food-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Patio")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Outdoor"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/cafe-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Outdoor")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Bar"}
                      Bigbuttonclass={"btn custom-bigbutton"}
                      img={require("../../../../assets/Images/seating/cafe-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      onClick={e => this.bigButtonHandler(e, "Bar")}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Special Ocassion"}
                      inputlabelclass={"label-class"}
                      name={"ocassion"}
                      type={"text"}
                      value={this.state.ocassion}
                      onChange={this.onChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. Birthday"}
                    />
                  </div>
                </div>
              </form>
            </div>

            <div className="button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Cancel"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.onCloseModal}
              />
              <ButtonComponent
                type={"button"}
                buttontext={"Save"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onCloseModal}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default AddDinerPopup;
