import React, { Component } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Modal from "react-responsive-modal";
import InputComponent from "./../SmallReusableComponents/InputComponent";
import ButtonComponent from "./../SmallReusableComponents/ButtonComponent";
import BigButtonComponent from "./../SmallReusableComponents/BigButtonComponent";
import TextAreaComponent from "./../SmallReusableComponents/TextAreaComponent";
// import Dropdown from '../MainDropDown/Dropdown';
// import TimePicker from 'react-time-picker';
import format from "date-fns/format";
import IconBox from "../../../../../reusableComponents/IconBoxButtonComponent";

/********************
 * @DESC API CONNECT
 *******************/
// import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { create_reservation } from "./../../../../../store/actions/addReservationAction";
import {
  get_subscription_details,
  get_restauranttime,
} from "../../../../../store/actions/addDetailsActions";
import isEmpty from "./../../../../../store/validation/is-Empty";

// const countryCodeList = ['AM', 'PM'];
export class AddReservationPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      open: false,
      name: "",
      phone_number: "",
      adults: "",
      kids: "",
      highchair: "",
      handicap: "",
      time: "",
      Date: "",
      // seating_preference: '',
      special_occassion: "",
      AddReservationData: [],
      selected_seating_area: [],
      primary_seating_area: {},
      subscription_details_handicap: "",
      subscription_details_highchair: "",
      entry_point: "manual entry",
      errors: {},
    };
    // this.bigButtonHandler = this.bigButtonHandler.bind(this);
  }

  componentDidMount() {
    this.setState({
      AddReservationData: this.props.seating.restauranttime_details
        .primary_seating_area,
    });
    if (!isEmpty(this.props.get_subscription_details)) {
      this.props.get_subscription_details();
    }
    if (!isEmpty(this.props.get_restauranttime)) {
      this.props.get_restauranttime();
    }
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      !isEmpty(nextProps.details) &&
      !isEmpty(nextProps.seating.restauranttime_details) &&
      !nextState.subscription_details_handicap &&
      !nextState.subscription_details_highchair
    ) {
      return {
        subscription_details_handicap: nextProps.details.handicap,
        subscription_details_highchair: nextProps.details.highchair,
        selected_seating_area:
          nextProps.seating.restauranttime_details.selected_seating_area,
        primary_seating_area:
          isEmpty(
            nextProps.seating.restauranttime_details.primary_seating_area
          ) || nextProps.seating.restauranttime_details.primary_seating_area,
      };
    }
    return null;
  }

  /**************************
   * @DESC Pop Up Handlers
   *************************/
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = (e) => {
    e.preventDefault();

    let data = e.target.value.split(" ");
    let newarray1 = [];
    for (let x = 0; x < data.length; x++) {
      newarray1.push(data[x].charAt(0).toUpperCase() + data[x].slice(1));
    }
    let newData = newarray1.join(" ");
    this.setState({
      // [e.target.name]: e.target.value
      [e.target.name]: newData,
    });
    // this.setState({
    //   [e.target.name]: e.target.value
    // });
  };

  // /***************************
  //  * @DESC Big Button Handler
  //  ***************************/
  // bigButtonHandler = (e, type) => {
  //   e.preventDefault();
  //   this.setState({
  //     AddReservationData: type,
  //   });
  //   console.log(type);
  // };

  /****************************************
   * @DESC - Seating Area CODE SELECTOR
   ****************************************/
  onSeatingAreaArraySelector = (icon) => (e) => {
    let SeatingArea = this.state.selected_seating_area;
    if (SeatingArea.length === 0) {
      SeatingArea.push(icon);
    } else {
      let isAlreadyPresent = SeatingArea.includes(icon);
      if (isAlreadyPresent) {
        let indexOf = SeatingArea.indexOf(icon);
        if (indexOf === 0 || indexOf) {
          SeatingArea.splice(indexOf, 1);
        }
      } else {
        SeatingArea.push(icon);
      }
    }
    this.setState({
      selected_seating_area: SeatingArea,
    });
  };

  onSelectedAreaPrimarySelector = (icon) => (e) => {
    this.setState({
      primary_seating_area: icon,
    });
  };

  primary_selected_icons = (iconNew) => {
    // console.log(iconNew);
    this.setState({
      primary_seating_area: iconNew.title,
      AddReservationData: iconNew,
    });
    // console.log(icon);
  };

  /********************************************
   * @DESC ADD RESERVATION POPUP HANDLE SUBMIT
   ********************************************/
  handleAddReservationSubmit = (e) => {
    const { datepassin } = this.props;
    // console.log(datepassin);

    e.preventDefault();
    let formData = {
      // vendor_user_id: this.props.auth.user._id,
      vendor_id: this.props.auth.user._id,
      name: this.state.name,
      phone_number: "+" + this.state.phone + this.state.phone_number,
      adults: this.state.adults === "" ? "0" : this.state.adults,
      kids: this.state.kids === "" ? "0" : this.state.kids,
      highchair: this.state.highchair === "" ? "0" : this.state.highchair,
      handicap: this.state.handicap === "" ? "0" : this.state.handicap,
      time: this.state.time,
      Date: format(datepassin, "DD-MM-YYYY"),
      seating_preference: this.state.AddReservationData.title,
      special_occassion: this.state.special_occassion,
      entry_point: this.state.entry_point,
    };
    //console.log(formData);
    this.props.create_reservation(formData);
    this.setState({
      name: "",
      phone: "",
      phone_number: "",
      adults: "",
      kids: "",
      highchair: "",
      handicap: "",
      time: "",
      Date: "",
      AddReservationData: "",
      special_occassion: "",
    });
  };

  render() {
    const {
      open,
      AddReservationData,
      subscription_details_highchair,
      subscription_details_handicap,
    } = this.state;

    return (
      <React.Fragment>
        <ButtonComponent
          type={"button"}
          buttontext={"Add Reservation"}
          buttonclass={"btn reservation-button"}
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-reservation-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="add-reservation-popup new-popup-datacss">
            <div className="popup-head">Add Reservation</div>

            <div className="container mt-3">
              <form noValidate onSubmit={this.handleAddReservationSubmit}>
                <div className="form-row">
                  <div className="col-sm-4">
                    <InputComponent
                      labeltext={"Name"}
                      inputlabelclass={"label-class"}
                      name={"name"}
                      type={"text"}
                      value={this.state.name}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control curve_input_field"}
                    />
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group reservation-data text-left">
                      <label
                        htmlFor="Country Code"
                        className="font-weight-light"
                      >
                        Country Code
                      </label>
                      <PhoneInput
                        country={""}
                        placeholder={"Select"}
                        value={this.state.phone}
                        onChange={(phone) => this.setState({ phone })}
                      />
                    </div>
                  </div>
                  <div className="col-sm-5">
                    <InputComponent
                      labeltext={"Phone Number"}
                      inputlabelclass={"label-class"}
                      name={"phone_number"}
                      type={"number"}
                      value={this.state.phone_number}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control curve_input_field"}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-sm-3">
                    <InputComponent
                      labeltext={"Adults"}
                      inputlabelclass={"label-class"}
                      name={"adults"}
                      type={"number"}
                      value={this.state.adults}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  <div className="col-sm-3">
                    <InputComponent
                      labeltext={"Kids"}
                      inputlabelclass={"label-class"}
                      name={"kids"}
                      type={"number"}
                      value={this.state.kids}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control curve_input_field new-width"}
                    />
                  </div>
                  {subscription_details_highchair === true ? (
                    <div className="col-sm-3">
                      <InputComponent
                        labeltext={"Highchair"}
                        inputlabelclass={"label-class"}
                        name={"highchair"}
                        type={"number"}
                        value={this.state.highchair}
                        onChange={this.onChange}
                        place={"Select"}
                        inputclass={"form-control curve_input_field new-width"}
                      />
                    </div>
                  ) : null}
                  {subscription_details_handicap === true ? (
                    <div className="col-sm-3">
                      <InputComponent
                        labeltext={"Handicapped"}
                        inputlabelclass={"label-class"}
                        name={"handicap"}
                        type={"number"}
                        value={this.state.handicap}
                        onChange={this.onChange}
                        place={"Select"}
                        inputclass={"form-control curve_input_field new-width"}
                      />
                    </div>
                  ) : null}
                </div>
                <div className="form-row">
                  <div className="col-sm-6">
                    <InputComponent
                      labeltext={"Time"}
                      inputlabelclass={"label-class"}
                      name={"time"}
                      type={"time"}
                      value={this.state.time}
                      onChange={this.onChange}
                      place={"Select"}
                      inputclass={"form-control curve_input_field"}
                    />
                  </div>
                  <div className="col-sm-2"></div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label
                        htmlFor="Date Of Reservation"
                        className="label-class"
                      >
                        Date Of Reservation
                      </label>
                      <input
                        type="text"
                        className="form-control curve_input_field bg-white"
                        value={format(this.props.datepassin, "DD-MM-YYYY")}
                        disabled
                      />
                    </div>
                  </div>
                </div>

                <div className="form-row">
                  <div className="d-flex flex-wrap text-left">
                    <p className="col-12 p-0 restaurant-title">
                      Selected Seating Area
                    </p>
                    <div
                      style={{
                        display: "flex",
                        flexWrap: "wrap",
                        width: "100%",
                      }}
                    >
                      {this.state.selected_seating_area.map(
                        (iconNew, index) => (
                          <IconBox
                            key={index}
                            src={iconNew.icon}
                            title={iconNew.title}
                            classsection={
                              iconNew.title === this.state.primary_seating_area
                                ? "seating-preferences-reservation-active"
                                : "seating-preferences-reservation"
                            }
                            onClick={() => this.primary_selected_icons(iconNew)}
                          />
                        )
                      )}
                    </div>
                  </div>
                  {/* <div className="col-sm-4">
                    <BigButtonComponent
                      labeltext={"Seating Preference"}
                      inputlabelclass={"label-class"}
                      buttontext={"Indoor"}
                      Bigbuttonclass={
                        AddReservationData === "Indoor"
                          ? "active-custom-bigbutton btn custom-bigbutton"
                          : "btn custom-bigbutton"
                      }
                      img={require("../../../../assets/Images/seating/burger-grey.png")}
                      imageClass={"big-buttom-img-class"}
                      // value={'Indoor'}
                      onClick={(e) => this.bigButtonHandler(e, "Indoor")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Patio"}
                      Bigbuttonclass={
                        AddReservationData === "Patio"
                          ? "active-custom-bigbutton btn custom-bigbutton"
                          : "btn custom-bigbutton"
                      }
                      img={require("../../../../assets/Images/seating/healthy-food-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      // value={'Patio'}
                      onClick={(e) => this.bigButtonHandler(e, "Patio")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Outdoor"}
                      Bigbuttonclass={
                        AddReservationData === "Outdoor"
                          ? "active-custom-bigbutton btn custom-bigbutton"
                          : "btn custom-bigbutton"
                      }
                      img={require("../../../../assets/Images/seating/cafe-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      // value={'Outdoor'}
                      onClick={(e) => this.bigButtonHandler(e, "Outdoor")}
                    />
                  </div>
                  <div className="col-sm-4">
                    <BigButtonComponent
                      buttontext={"Bar"}
                      Bigbuttonclass={
                        AddReservationData === "Bar"
                          ? "active-custom-bigbutton btn custom-bigbutton"
                          : "btn custom-bigbutton"
                      }
                      img={require("../../../../assets/Images/seating/cafe-icon.png")}
                      imageClass={"big-buttom-img-class"}
                      // value={'Bar'}
                      onClick={(e) => this.bigButtonHandler(e, "Bar")}
                    />
                  </div> */}
                </div>
                <div className="row">
                  <div className="col">
                    <TextAreaComponent
                      labeltext={"Special Occasion"}
                      inputlabelclass={"label-class mt-4"}
                      name={"special_occassion"}
                      type={"text"}
                      value={this.state.special_occassion}
                      onChange={this.onChange}
                      textareaclass={"custom-textarea"}
                      place={"eg. Birthday"}
                    />
                  </div>
                </div>
                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseModal}
                  />
                  <ButtonComponent
                    type={"submit"}
                    buttontext={"Save"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onCloseModal}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  details: state.details.subscription_details[0],
  seating: state.details,
  errors: state.errors,
});

export default connect(mapStateToProps, {
  create_reservation,
  get_subscription_details,
  get_restauranttime,
})(withRouter(AddReservationPopup));
//19-12-2019
