import React, { Component } from "react";

export class SubNavbar extends Component {
  constructor() {
    super();
    this.state = {
      Allbtn: "1",
      DefaultWaitbtn: "1",
      Filterbtn: "1"
    };
  }

  /*******************************
    Seatin All Button Handler
********************************/
  seatingAllbtn = no => e => {
   
    this.setState({
      Allbtn: no
    });
  };

  /***********************************************
    Seating Default Wait Time Button Handler
***************************************************/
  seatingDefaultWaitbtn = no => e => {
   
    this.setState({
      DefaultWaitbtn: no
    });
  };

  /***********************************************
    Seating Filter By Party Size Button Handler
***************************************************/
  seatingFilterbtn = no => e => {
    
    this.setState({
      Filterbtn: no
    });
  };

  render() {
    const { Allbtn, DefaultWaitbtn, Filterbtn } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid sub-navbar">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="seating-button-padding">
                <button
                  type="button"
                  className={
                    Allbtn === "1"
                      ? "btn active-seating-buttons seating-buttons"
                      : "btn seating-buttons"
                  }
                  onClick={this.seatingAllbtn("1")}
                >
                  All
                </button>
                <button
                  type="button"
                  className={
                    Allbtn === "2"
                      ? "btn active-seating-buttons seating-buttons"
                      : "btn seating-buttons"
                  }
                  onClick={this.seatingAllbtn("2")}
                >
                  <img
                    src={require("../assets/Images/waiting-room.svg")}
                    alt="waiting"
                    className="img-fluid"
                  />
                </button>
                <button
                  type="button"
                  className={
                    Allbtn === "3"
                      ? "btn active-seating-buttons seating-buttons"
                      : "btn seating-buttons"
                  }
                  onClick={this.seatingAllbtn("3")}
                >
                  <img
                    src={require("../assets/Images/waiting-room-2.svg")}
                    alt="waiting-2"
                    className="img-fluid"
                  />
                </button>
                <button
                  type="button"
                  className={
                    Allbtn === "4"
                      ? "btn active-seating-buttons seating-buttons"
                      : "btn seating-buttons"
                  }
                  onClick={this.seatingAllbtn("4")}
                >
                  <img
                    src={require("../assets/Images/reservation.svg")}
                    alt="Online-booking"
                    className="img-fluid"
                  />
                </button>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <h5>
                Default Wait time <span>(Mins)</span>
              </h5>
              <div className="btn-group">
                <button
                  type="button"
                  className={
                    DefaultWaitbtn === "1"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingDefaultWaitbtn("1")}
                >
                  00
                </button>
                <button
                  type="button"
                  className={
                    DefaultWaitbtn === "2"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingDefaultWaitbtn("2")}
                >
                  5-10
                </button>
                <button
                  type="button"
                  className={
                    DefaultWaitbtn === "3"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingDefaultWaitbtn("3")}
                >
                  10-20
                </button>
                <button
                  type="button"
                  className={
                    DefaultWaitbtn === "4"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingDefaultWaitbtn("4")}
                >
                  30-45
                </button>
                <button
                  type="button"
                  className={
                    DefaultWaitbtn === "5"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingDefaultWaitbtn("5")}
                >
                  45+
                </button>
              </div>
            </div>
            <div className="col-md-4">
              <h5>Filter by party size</h5>
              <div className="btn-group">
                <button
                  type="button"
                  className={
                    Filterbtn === "1"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingFilterbtn("1")}
                >
                  2
                </button>
                <button
                  type="button"
                  className={
                    Filterbtn === "2"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingFilterbtn("2")}
                >
                  4
                </button>
                <button
                  type="button"
                  className={
                    Filterbtn === "3"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingFilterbtn("3")}
                >
                  6
                </button>
                <button
                  type="button"
                  className={
                    Filterbtn === "4"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingFilterbtn("4")}
                >
                  8
                </button>
                <button
                  type="button"
                  className={
                    Filterbtn === "5"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingFilterbtn("5")}
                >
                  8 <i className="fa fa-angle-left"></i>
                </button>
                <button
                  type="button"
                  className={
                    Filterbtn === "6"
                      ? "btn active-wait-time-button wait-time-button"
                      : "btn wait-time-button"
                  }
                  onClick={this.seatingFilterbtn("6")}
                >
                  All
                </button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SubNavbar;
