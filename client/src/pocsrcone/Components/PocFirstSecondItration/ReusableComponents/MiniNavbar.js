import React from "react";
// import ButtonComponent from "./SmallReusableComponents/ButtonComponent";
import AddDinerPopup from "./AllPopUps/AddDinerPopup";

const MiniNavbar = ({
  text,
  img,
  name,
  value,
  type,
  inputclass,
  place,
  onChange,
  addDinerShow
}) => {
  return (
    <React.Fragment>
      <div className="container-fluid mini-navbar">
        <div className="row">
          <div className="col-12">
            <div className="search-div">
              <h5>{text}</h5>
              <div className="search-button-div">
                {addDinerShow === true ? <AddDinerPopup /> : ""}

                <div className="input-search-css">
                  <img
                    src={img}
                    alt="search-img"
                    className="img-fluid search-img"
                  />
                  <input
                    name={name}
                    value={value}
                    type={type}
                    className={inputclass}
                    placeholder={place}
                    onChange={onChange}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default MiniNavbar;
