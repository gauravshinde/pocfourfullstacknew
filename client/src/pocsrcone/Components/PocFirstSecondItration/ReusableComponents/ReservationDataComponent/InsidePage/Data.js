import React from 'react';
import { CancelReservationPopup } from './../../AllPopUps/CancelReservationPopup';

const Data = ({
  name,
  confirm,
  adult,
  kids,
  total,
  highchair,
  handicap,
  eta,
  etatime
}) => {
  return (
    <React.Fragment>
      <div className='row details'>
        <div className='col-sm-5'>
          <div className='first-div'>
            <h6 className='details-bold-text'>{name}</h6>

            <img
              src={require('../../../../../assets/Images/seating/call.svg')}
              alt='call'
              className='img-fluid details-logo-img'
            />
            <img
              src={require('../../../../../assets/Images/seating/message.svg')}
              alt='message'
              className='img-fluid details-logo-img'
            />
            <img
              src={require('../../../../../assets/Images/seating/pen.svg')}
              alt='pen'
              className='img-fluid details-logo-img'
            />
          </div>
        </div>
        <div className='col-sm-5 p-0'>
          <div className='d-flex justify-content-between'>
            <div>
              <h6 className='details-bold-text'>Party Size </h6>
              <p>{adult} Adults</p>
              <p> {kids} Kids</p>
            </div>
            <div>
              <h6 className='details-bold-text'>Total: {total}</h6>
              <p>{highchair} HighChair</p>
              <p>{handicap} Handicap</p>
            </div>
          </div>
        </div>
        <div className='col-sm-2 d-flex justify-content-end'>
          <CancelReservationPopup />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Data;
