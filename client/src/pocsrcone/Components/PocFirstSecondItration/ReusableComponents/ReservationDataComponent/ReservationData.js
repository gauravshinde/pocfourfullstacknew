import React, { Component } from "react";
import Data from "./InsidePage/Data";
import AddReservationPopup from "../AllPopUps/AddReservationPopup";
import dateFns from "date-fns";

import isEmpty from "../../../../../store/validation/is-Empty";

export class ReservationData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      all_reservationlist: []
    };
    // console.log(props);
  }

  renderAllReservation = () => {
    const { get_all_reservationclickdata } = this.props;
    // console.log(get_all_reservationclickdata);

    let data = [];
    let total = "";
    let time = "";
    let H = "";
    let h = "";
    let ampm = "";
    let newtime = "";

    if (!isEmpty(get_all_reservationclickdata)) {
      data = get_all_reservationclickdata.map((data, index) => {
        total = parseInt(data.adults) + parseInt(data.kids);
        // time = dateFns.format(data.time, 'HH:mm aa');
        time = data.time;
        H = +time.substr(0, 2);
        h = H % 12 || 12;
        ampm = H < 12 ? " AM" : " PM";
        newtime = h + time.substr(2, 3) + ampm;
        return (
          <React.Fragment key={index}>
            <div className="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 ">
              <div className="left-col">
                <p>{newtime}</p>
                <hr />
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
              <div className="right-col">
                <Data
                  name={data.name}
                  confirm={"Confirmed by User"}
                  adult={data.adults}
                  kids={data.kids}
                  total={total}
                  highchair={data.highchair}
                  handicap={data.handicap}
                />
              </div>
            </div>
          </React.Fragment>
        );
      });
    }
    return data;
  };

  render() {
    const {
      onCloseData,
      selected_date,
      get_all_reservationclickdata
    } = this.props;
    const todayDate = selected_date;
    // console.log(todayDate);

    return (
      <React.Fragment>
        <div className="main-reservation-data">
          <div className="container head">
            <div className="row">
              <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 border-right-class">
                {/* <span className="day-text">Today,</span> */}
                <p className="head-text">
                  {dateFns.format(todayDate, " DD MMM YYYY")}
                </p>
              </div>
              <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 border-right-class">
                <span className="day-text orange">
                  {get_all_reservationclickdata.length}
                </span>
                <p className="head-text">Reservations</p>
              </div>
              <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <AddReservationPopup datepassin={todayDate} />
              </div>
              <div className="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 ml-auto">
                <img
                  src={require("../../../../assets/Images/seating/reservation-cross.svg")}
                  alt="cross"
                  className="img-fluid cross-img"
                  onClick={onCloseData}
                />
              </div>
            </div>
          </div>

          <div className="main-details">
            <div className="row">{this.renderAllReservation()}</div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ReservationData;

// import React from 'react';
// import Data from './InsidePage/Data';
// import AddReservationPopup from '../AllPopUps/AddReservationPopup';

// import { withRouter } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
// import isEmpty from '../../../../../store/validation/is-Empty';
// import { get_all_reservation } from '../../../../../store/actions/addReservationAction';

// const ReservationData = ({ onCloseData }) => {

//   return (
//     <>
//       <div className='main-reservation-data'>
//         <div className='container head'>
//           <div className='row'>
//             <div className='col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 border-right-class'>
//               <span className='day-text'>Today,</span>
//               <p className='head-text'>21st November, 2019</p>
//             </div>
//             <div className='col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 border-right-class'>
//               <span className='day-text orange'>08</span>
//               <p className='head-text'>Reservations</p>
//             </div>
//             <div className='col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3'>
//               <AddReservationPopup />
//               {/* <ButtonComponent
//                    type={"button"}
//                    buttontext={"Add Reservation"}
//                    buttonclass={"btn reservation-button"}
//                  /> */}
//             </div>
//             <div className='col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 ml-auto'>
//               <img
//                 src={require('../../../../assets/Images/seating/reservation-cross.svg')}
//                 alt='cross'
//                 className='img-fluid cross-img'
//                 onClick={onCloseData}
//               />
//             </div>
//           </div>
//         </div>

//         <div className='main-details'>
//           <div className='row'>
//             <div className='col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 '>
//               <div className='left-col'>
//                 <p>09:00 pm</p>
//                 <hr />
//               </div>
//             </div>
//             <div className='col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10'>
//               <div className='right-col'>
//                 <Data
//                   name={'Zeeshan Sayyed'}
//                   confirm={'Confirmed by User'}
//                   adult={'2 Adults'}
//                   kids={'2 Kids'}
//                   total={'Total:4'}
//                   highchair={'2 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//                 <Data
//                   name={'Akshay Kumar'}
//                   confirm={'Confirmed by User'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//               </div>
//             </div>
//           </div>
//           <div className='row'>
//             <div className='col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 '>
//               <div className='left-col'>
//                 <p>10:30 pm</p>
//                 <hr />
//               </div>
//             </div>
//             <div className='col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10'>
//               <div className='right-col'>
//                 <Data
//                   name={'Akhil Sharma'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//                 <Data
//                   name={'Gaurav Shinde'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//                 <Data
//                   name={'Tushar Ghayal'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//                 <Data
//                   name={'Tushar Rathod'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//               </div>
//             </div>
//           </div>

//           <div className='row'>
//             <div className='col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 '>
//               <div className='left-col'>
//                 <p>11:30 pm</p>
//                 <hr />
//               </div>
//             </div>
//             <div className='col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10'>
//               <div className='right-col'>
//                 <Data
//                   name={'Akhil Sharma'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//                 <Data
//                   name={'Gaurav Shinde'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//                 <Data
//                   name={'Tushar Ghayal'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//                 <Data
//                   name={'Tushar Rathod'}
//                   eta={'ETA:'}
//                   etatime={'25 Mins'}
//                   adult={'5 Adults'}
//                   kids={'0 Kids'}
//                   total={'Total:5'}
//                   highchair={'0 Highchairs '}
//                   handicap={'0 Handicap'}
//                 />
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </>
//   );
// };

// const mapStateToProps = state => ({
//   auth: state.auth,
//   reservation: state.reservation.get_all_reservation
// });

// export default connect(
//   mapStateToProps,
//   {
//     get_all_reservation
//   }
// )(withRouter(ReservationData));
