import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Modal from "react-responsive-modal";
import ButtonComponent from "../SmallReusableComponents/ButtonComponent";
// import InputComponent from "../../../../../reusableComponents/InputComponent";
import BigButtonComponent from "./../SmallReusableComponents/BigButtonComponent";
import TextAreaComponent from "./../SmallReusableComponents/TextAreaComponent";
// import classnames from "classnames";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { update_new_accept } from "../../../../../store/actions/SeatingPageAction/SeatingPageAction";
import isEmpty from "./../../../../../PocTwo/store/validation/is-Empty";

import openSocket from "socket.io-client";
import io from "socket.io-client";
import { socketIpAddress } from "../../../../../config/Keys";

export class Dropdown extends Component {
  mySocket = openSocket(`${socketIpAddress}`);
  constructor() {
    super();
    this.state = {
      isOpen: false,
      labelItem: null,
      typeDropdown: null,
      waittime: "",
      selectactivebtn: "1",
      reason: "",
      CancelReservationData: "",
      waitTimeopen: false,
      cancelReservationopen: false,
      errors: {}
    };
  }

  componentDidMount() {
    this.socket = io(`${socketIpAddress}`);
    // this.socket.on("getData", msg => {
    //   console.log("Data From Get Data", msg);
    //   this.setState({ dinnerList: msg });
    // });
  }

  componentWillMount() {
    const { label } = this.props.list[0];
    //console.log(!isEmpty(this.props.data) && this.props.data);
    this.setState({
      pendingAllData: !isEmpty(this.props.data) && this.props.data
    });
    let firstItem = null;
    if (typeof label != "undefined") {
      this.checkType(false);
      firstItem = label;
    } else {
      this.checkType(true);
      firstItem = this.props.list[0];
    }
    this.setState({
      labelItem: firstItem
    });
  }
  checkType = value => {
    this.setState({
      typeDropdown: value
    });
  };

  showDropdown = () => {
    this.setState({ isOpen: true });
    document.addEventListener("click", this.hideDropdown);
  };

  hideDropdown = () => {
    this.setState({ isOpen: false });
    document.removeEventListener("click", this.hideDropdown);
  };
  chooseItem = value => {
    // e.preventDefault();
    if (this.state.labelItem !== value) {
      this.setState({
        labelItem: value
      });
    }
    if (value === "Accept") {
      //console.log(value);

      this.setState({
        waitTimeopen: true
      });
    }
    if (value === "Reject") {
      //console.log(value);
      this.setState({
        cancelReservationopen: true
      });
    }
  };

  /***********************************
   * 
    Select Active  Button Handler
***************************************/
  selectActivebtn = no => e => {
    this.setState({
      selectactivebtn: no
    });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /***************************
   * @DESC Big Button Handler
   ***************************/
  bigButtonHandler = (e, type) => {
    e.preventDefault();
    this.setState({
      CancelReservationData: type
    });
  };

  /*********************************************
        Wait Time Popup Handler 
************************************************/

  waitTimeonOpenModal = () => {
    this.setState({ waitTimeopen: true });
  };

  waitTimeonCloseModal = () => {
    this.setState({ waitTimeopen: false, cancelReservationopen: false });
  };

  /*********************************************
        Cancel Reservation Popup Handler 
************************************************/

  ReservationonOpenModal = () => {
    this.setState({ cancelReservationopen: true });
  };

  ReservationonCloseModal = () => {
    this.setState({ cancelReservationopen: false });
  };

  onWaitTimeSubmit = pendingAllData => e => {
    e.preventDefault();
    const newid = pendingAllData._id;
    let formData = {
      id: newid,
      user_id: pendingAllData.user_id,
      ETA: this.state.waittime,
      status: "Accepted",
      token: pendingAllData.token,
      phone_number: pendingAllData.phone_number
      // Date: pendingAllData.Date,
      // Request_Date: Date(pendingAllData.Date),
      // Seating_Date: pendingAllData.Seating_Date,
      // adults: pendingAllData.adults,
      // dinner_status: pendingAllData.dinner_status,
      // handicap: pendingAllData.handicap,
      // highchair: pendingAllData.highchair,
      // kids: pendingAllData.kids,
      // name: pendingAllData.name,
      // order_type: pendingAllData.order_type,
      // phone_number: pendingAllData.phone_number,
      // reasons_for_Cancel: pendingAllData.reasons_for_Cancel,
      // reservation_status: pendingAllData.reservation_status,
      // seating_preference: pendingAllData.seating_preference,
      // special_occassion: pendingAllData.special_occassion,
      // time: pendingAllData.time,
      // total: pendingAllData.adults + pendingAllData.kids,
      // user_id: pendingAllData.user_id,
      // vendor_id: pendingAllData.vendor_user_id,
      // wait_time: pendingAllData.wait_time
    };
    console.log(formData);
    this.mySocket.emit("reservationStatusUpdate", formData);
    // this.props.update_new_accept(formData);
    this.setState({
      waitTimeopen: false
    });
  };

  /*******************************
        Wait Time Modal 
********************************/
  WaitTimeModal = () => {
    const { errors, pendingAllData } = this.state;

    return (
      <React.Fragment>
        <Modal
          open={this.state.waitTimeopen}
          onClose={this.waitTimeonCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton"
            //overlay: "customOverlay",
            //modal: "waittime-Modal",
            //closeButton: "customCloseButton"
          }}
        >
          <div className="waittime-popup">
            <div className="popup-head">Accept Reservation</div>
            <div className="p-3">
              <h5 className="mt-3 mb-5 inside-accept-reservation-text">
                Are you sure you want to accept this request
              </h5>
              <div className="d-flex w-50 float-right mb-4">
                <ButtonComponent
                  type={"button"}
                  buttontext={"Cancel"}
                  buttonclass={"btn button-width button-border-orange"}
                  onClick={this.waitTimeonCloseModal}
                />
                <ButtonComponent
                  type={"button"}
                  buttontext={"Yes"}
                  buttonclass={"btn button-width button-orange"}
                  onClick={this.onWaitTimeSubmit(pendingAllData)}
                />
              </div>
            </div>
            {/*<div className="p-3">
              <form>
                <InputComponent
                  labeltext="Select Wait Time"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="waittime"
                  type="text"
                  place="Enter WaitTime"
                  onChange={this.onChange}
                  value={this.state.waittime}
                  error={errors.waittime}
                  inputclass={classnames("map-inputfield col-4", {
                    invalid: errors.waittime
                  })}
                />
                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.waitTimeonCloseModal}
                  />
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Save"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onWaitTimeSubmit(pendingAllData)}
                  />
                </div>
              </form>

              {/* <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 pl-0">
                <div className="btn-group">
                  <button
                    type="button"
                    className={
                      this.state.selectactivebtn === "1"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("1")}
                  >
                    00
                  </button>
                  <button
                    type="button"
                    className={
                      this.state.selectactivebtn === "2"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("2")}
                  >
                    5-10
                  </button>
                  <button
                    type="button"
                    className={
                      this.state.selectactivebtn === "3"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("3")}
                  >
                    10-20
                  </button>
                  <button
                    type="button"
                    className={
                      this.state.selectactivebtn === "4"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("4")}
                  >
                    30-45
                  </button>
                  <button
                    type="button"
                    className={
                      this.state.selectactivebtn === "5"
                        ? "btn active-wait-time-button wait-time-button"
                        : "btn wait-time-button"
                    }
                    onClick={this.selectActivebtn("5")}
                  >
                    45+
                  </button>
                </div>
              </div> *
            </div> */}
          </div>
        </Modal>
      </React.Fragment>
    );
  };

  onCancelReservationSubmit = pendingAllData => e => {
    e.preventDefault();
    const newId = pendingAllData._id;
    let formData = {
      id: newId,
      enter_reasons: this.state.reason,
      status: "Cancelled",
      reject_reasons: this.state.CancelReservationData,
      token: pendingAllData.token,
      phone_number: pendingAllData.phone_number
    };
    // console.log(formData);
    this.mySocket.emit("reservationStatusUpdate", formData);
    // this.props.update_new_accept(formData);
    this.setState({
      reason: "",
      CancelReservationData: "",
      cancelReservationopen: false
    });
  };

  /*********************************
        Cancel Reservation Modal 
************************************/
  CancelReservationModal = () => {
    const { errors, pendingAllData, CancelReservationData } = this.state;

    return (
      <React.Fragment>
        <Modal
          open={this.state.cancelReservationopen}
          onClose={this.ReservationonCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "cancel-reservation-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="cancel-reservation-popup">
            <div className="popup-head">Cancel Reservation</div>

            <div className="container mt-2">
              <div className="row">
                <div className="col-sm-4">
                  <BigButtonComponent
                    labeltext={"Reasons For Cancellation"}
                    inputlabelclass={"label-class"}
                    buttontext={"No Show"}
                    Bigbuttonclass={
                      CancelReservationData === "No Show"
                        ? "active-custom-bigbutton btn custom-bigbutton"
                        : "btn custom-bigbutton"
                    }
                    //img={require("../../../../assets/Images/seating/burger-grey.png")}
                    imageClass={"big-buttom-img-class d-none"}
                    // value={'No Show'}
                    onClick={e => this.bigButtonHandler(e, "No Show")}
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonComponent
                    buttontext={"Walkaway"}
                    Bigbuttonclass={
                      CancelReservationData === "Walkaway"
                        ? "active-custom-bigbutton btn custom-bigbutton"
                        : "btn custom-bigbutton"
                    }
                    //img={require("../../../../assets/Images/seating/healthy-food-icon.png")}
                    imageClass={"big-buttom-img-class d-none"}
                    // value={'Walkaway'}
                    onClick={e => this.bigButtonHandler(e, "Walkaway")}
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonComponent
                    buttontext={"Left Before Seating"}
                    Bigbuttonclass={
                      CancelReservationData === "Left Before Seating"
                        ? "active-custom-bigbutton btn custom-bigbutton"
                        : "btn custom-bigbutton"
                    }
                    //img={require("../../../../assets/Images/seating/cafe-icon.png")}
                    imageClass={"big-buttom-img-class d-none"}
                    // value={'Left Before Seating'}
                    onClick={e =>
                      this.bigButtonHandler(e, "Left Before Seating")
                    }
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonComponent
                    buttontext={"Left After Seating"}
                    Bigbuttonclass={
                      CancelReservationData === "Left After Seating"
                        ? "active-custom-bigbutton btn custom-bigbutton"
                        : "btn custom-bigbutton"
                    }
                    //img={require("../../../../assets/Images/seating/cafe-icon.png")}
                    imageClass={"big-buttom-img-class d-none"}
                    // value={'Left After Seating'}
                    onClick={e =>
                      this.bigButtonHandler(e, "Left After Seating")
                    }
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonComponent
                    buttontext={"Other"}
                    Bigbuttonclass={
                      CancelReservationData === "Other"
                        ? "active-custom-bigbutton btn custom-bigbutton"
                        : "btn custom-bigbutton"
                    }
                    //img={require("../../../../assets/Images/seating/cafe-icon.png")}
                    imageClass={"big-buttom-img-class d-none"}
                    // value={'Other'}
                    onClick={e => this.bigButtonHandler(e, "Other")}
                  />
                </div>
              </div>
              {/* <div className="row">
                <div className="col-sm-4">
                  <BigButtonOneComponent
                    labeltext={"Reasons For Cancellation"}
                    inputlabelclass={"label-class"}
                    buttontext={"No Show"}
                    Bigbuttonclass={"btn custom-bigbuttonone"}
                    onClick={e => this.bigButtonHandler(e, "No Show")}
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonOneComponent
                    buttontext={"Walkaway"}
                    Bigbuttonclass={"btn custom-bigbuttonone"}
                    onClick={e => this.bigButtonHandler(e, "Walkaway")}
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonOneComponent
                    buttontext={"Left Before Seating"}
                    Bigbuttonclass={"btn custom-bigbuttonone"}
                    onClick={e =>
                      this.bigButtonHandler(e, "Left Before Seating")
                    }
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonOneComponent
                    buttontext={"Left After Seating"}
                    Bigbuttonclass={"btn custom-bigbuttonone"}
                    onClick={e =>
                      this.bigButtonHandler(e, "Left After Seating")
                    }
                  />
                </div>
                <div className="col-sm-4">
                  <BigButtonOneComponent
                    buttontext={"Other"}
                    Bigbuttonclass={
                      "btn custom-bigbuttonone custom-bigbuttonone-border-orange"
                    }
                  />
                </div>
              </div> */}
              <div className="row">
                <div className="col">
                  <TextAreaComponent
                    labeltext={"Enter Reason"}
                    inputlabelclass={"label-class"}
                    name={"reason"}
                    type={"text"}
                    value={this.state.reason}
                    onChange={this.onChange}
                    textareaclass={"custom-textarea"}
                    place={"eg. Birthday"}
                  />
                </div>
              </div>
            </div>
            <div className="button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Cancel"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.ReservationonCloseModal}
              />
              <ButtonComponent
                type={"button"}
                buttontext={"Save"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onCancelReservationSubmit(pendingAllData)}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  };

  renderDataDropDown = (item, index) => {
    const { value, label } = this.state.typeDropdown
      ? { value: index, label: item }
      : item;
    return (
      <li key={index} value={value} onClick={() => this.chooseItem(label)}>
        <Link to="/vendor-seating">{label}</Link>
      </li>
    );
  };

  render() {
    const { list } = this.props;
    // console.log(this.state.pendingAllData);
    return (
      <div>
        {this.WaitTimeModal()}
        {this.CancelReservationModal()}
        <div className={`dropdown ${this.state.isOpen ? "open" : ""}`}>
          <button
            className="dropdown-toggle"
            type="button"
            onClick={this.showDropdown}
          >
            {this.state.labelItem}
            <span className="caret" />
          </button>
          <ul className="dropdown-menu">{list.map(this.renderDataDropDown)}</ul>
        </div>
      </div>
    );
  }
}
Dropdown.propTypes = {
  list: PropTypes.array
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps, { update_new_accept })(
  withRouter(Dropdown)
);
