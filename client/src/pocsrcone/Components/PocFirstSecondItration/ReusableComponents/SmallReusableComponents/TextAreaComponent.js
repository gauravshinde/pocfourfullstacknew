import React from "react";

const TextAreaComponent = ({
  name,
  labeltext,
  type,
  value,
  onChange,
  textareaclass,
  place,
  inputlabelclass
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <div className="form-group">
        <textarea
          id={name}
          name={name}
          type={type}
          value={value}
          className={textareaclass}
          placeholder={place}
          cols="40"
          rows="4"
          onChange={onChange}
        />
      </div>
    </React.Fragment>
  );
};

export default TextAreaComponent;
