import React from "react";

const BigButtonOneComponent = ({
  buttontext,
  name,
  type,
  value,
  Bigbuttonclass,
  onClick,
  inputlabelclass,
  labeltext
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <button
        name={name}
        type={type}
        value={value}
        className={Bigbuttonclass}
        onClick={onClick}
      >
        {buttontext}
      </button>
    </React.Fragment>
  );
};

export default BigButtonOneComponent;
