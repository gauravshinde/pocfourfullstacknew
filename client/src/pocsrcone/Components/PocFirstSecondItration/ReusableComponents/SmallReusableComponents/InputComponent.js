import React from 'react';

const InputComponent = ({
  name,
  type,
  value,
  place,
  inputclass,
  onChange,
  inputlabelclass,
  labeltext
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <div className='form-group'>
        <input
          name={name}
          type={type}
          value={value}
          placeholder={place}
          className={inputclass}
          onChange={onChange}
        />
      </div>
    </React.Fragment>
  );
};

export default InputComponent;
