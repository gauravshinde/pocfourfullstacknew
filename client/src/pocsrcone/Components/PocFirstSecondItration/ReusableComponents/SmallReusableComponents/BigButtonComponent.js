import React from "react";

const BigButtonComponent = ({
  buttontext,
  name,
  type,
  value,
  Bigbuttonclass,
  onClick,
  inputlabelclass,
  labeltext,
  img,
  imageClass
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <div className="button-image-class">
        <img src={img} alt="coffee" className={imageClass} />
        <button
          name={name}
          type={type}
          value={value}
          className={Bigbuttonclass}
          onClick={onClick}
        >
          {buttontext}
        </button>
      </div>
    </React.Fragment>
  );
};

export default BigButtonComponent;
