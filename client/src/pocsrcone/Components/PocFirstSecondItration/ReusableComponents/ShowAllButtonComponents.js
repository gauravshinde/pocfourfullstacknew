import React, { Component } from "react";

export class ShowAllButtonComponents extends Component {
  constructor() {
    super();
    this.state = {
      ShowAllbtn: "1"
    };
  }

  /************************************
   History Show All Button Handler
**************************************/
  historyShowAllbtn = no => e => {
    console.log("data");
    this.setState({
      ShowAllbtn: no
    });
  };
  render() {
    const { ShowAllbtn } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid showallbutton-container">
          <div className="row">
            <div className="col-12 text-center">
              <button
                type="button"
                className={
                  ShowAllbtn === "1"
                    ? "btn active-showall-button showall-button"
                    : "btn showall-button"
                }
                onClick={this.historyShowAllbtn("1")}
              >
                Show All
              </button>
              <button
                type="button"
                className={
                  ShowAllbtn === "2"
                    ? "btn active-showall-button showall-button"
                    : "btn showall-button"
                }
                onClick={this.historyShowAllbtn("2")}
              >
                Show Completed
              </button>
              <button
                type="button"
                className={
                  ShowAllbtn === "3"
                    ? "btn active-showall-button showall-button"
                    : "btn showall-button"
                }
                onClick={this.historyShowAllbtn("3")}
              >
                Show Cancelled
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ShowAllButtonComponents;
