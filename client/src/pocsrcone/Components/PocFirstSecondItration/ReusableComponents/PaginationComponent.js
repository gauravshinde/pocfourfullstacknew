import React, { Component } from "react";

export class PaginationComponent extends Component {
  constructor() {
    super();
    this.state = {
      activebtn: false,
      pageNo: "1"
    };
  }
  paginationbtn = no => e => {
    console.log("data");
    this.setState({
      pageNo: no
    });
  };
  render() {
    const { pageNo } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid pagination-section">
          <div className="row">
            <div className="col-12">
              <nav aria-label="Page navigation example" id="page-list">
                <ul className="pagination">
                  <li className="page-item">
                    <i className="fa fa-angle-left"></i>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "1"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("1")}
                    >
                      01
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "2"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("2")}
                    >
                      02
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "3"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("3")}
                    >
                      03
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "4"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("4")}
                    >
                      04
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "5"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("5")}
                    >
                      05
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "6"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("6")}
                    >
                      06
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "7"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("7")}
                    >
                      07
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "8"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("8")}
                    >
                      08
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "9"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      onClick={this.paginationbtn("9")}
                    >
                      09
                    </p>
                  </li>
                  <li className="page-item">
                    <p
                      className={
                        pageNo === "10"
                          ? "active-page-link page-link"
                          : "page-link"
                      }
                      href="#"
                      onClick={this.paginationbtn("10")}
                    >
                      10
                    </p>
                  </li>
                  <li className="page-item">
                    <i className="fa fa-angle-right"></i>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default PaginationComponent;
