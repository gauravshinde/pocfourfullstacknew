import React, { Component } from "react";
import MiniNavbar from "./../../ReusableComponents/MiniNavbar";
import SubNavbar from "../../ReusableComponents/SubNavbar";
import SeatingTableHeadComponent from "../../ReusableComponents/TableReusableComponent/SeatingTableComponent/SeatingTableHeadComponent";
import SeatingTableBodyComponent from "../../ReusableComponents/TableReusableComponent/SeatingTableComponent/SeatingTableBodyComponent";
import { PaginationComponent } from "./../../ReusableComponents/PaginationComponent";

export class MainSeating extends Component {
  render() {
    return (
      <React.Fragment>
        <SubNavbar />
        <MiniNavbar
        addDinerShow={true}
          text={"All Seats"}
          img={require("../../assets/Images/search.svg")}
          name={"search"}
          type={"text"}
          inputclass={"form-control custom-input"}
          place={"Search"}
        />
        <div className="container-fluid seating-container">
          <div className="table-responsive seating-table">
            <table className="table table-striped mb-0 ">
              <thead>
                <SeatingTableHeadComponent
                  code={"#"}
                  details={"Details"}
                  ordertype={"Order Type"}
                  partysize={"Party Size"}
                  waittime={"Wait Time"}
                  detailsone={"Details"}
                  status={"Status"}
                />
              </thead>
              <tbody>
                <SeatingTableBodyComponent
                  checkid={"customCheck1"}
                  code={"101"}
                  name={"Kinza"}
                  imgclass={"seating-logo-img"}
                  img1={require("../../assets/Images/seating/call.svg")}
                  img2={require("../../assets/Images/seating/message.svg")}
                  img3={require("../../assets/Images/seating/bell.svg")}
                  imgclass1={"ordertype-img"}
                  img4={require("../../assets/Images/seating/pedestrian-walking.svg")}
                  TotalTime={"25"}
                  Timer={"10:00"}
                  imgclass2={"edit-img"}
                  img8={require("../../assets/Images/seating/edit.svg")}
                  img5={require("../../assets/Images/seating/ocassion.svg")}
                  img6={require("../../assets/Images/seating/eye.svg")}
                  img7={require("../../assets/Images/seating/pen.svg")}
                  location={"Location"}
                  view={"View Location"}
                  img9={require("../../assets/Images/seating/maps-and-flags.svg")}
                  confirm={"Confirmed by User"}
                />
                <SeatingTableBodyComponent
                  checkid={"customCheck2"}
                  code={"102"}
                  name={"Akhil"}
                  imgclass={"seating-logo-img"}
                  img1={require("../../assets/Images/seating/call.svg")}
                  img2={require("../../assets/Images/seating/message.svg")}
                  img3={require("../../assets/Images/seating/bell.svg")}
                  imgclass1={"ordertype-img"}
                  img4={require("../../assets/Images/seating/online-booking.svg")}
                  Time={"9:00 PM"}
                  TotalTime={"25"}
                  Timer={"10:00"}
                  imgclass2={"edit-img"}
                  img8={require("../../assets/Images/seating/edit.svg")}
                  img5={require("../../assets/Images/seating/ocassion.svg")}
                  img6={require("../../assets/Images/seating/eye.svg")}
                  img7={require("../../assets/Images/seating/pen.svg")}
                  location={"Location"}
                  view={"View Location"}
                  img9={require("../../assets/Images/seating/maps-and-flags.svg")}
                  confirm={"Confirmed by User"}
                />
                <SeatingTableBodyComponent
                  checkid={"customCheck3"}
                  code={"103"}
                  name={"Gaurav"}
                  imgclass={"seating-logo-img"}
                  img1={require("../../assets/Images/seating/call.svg")}
                  img2={require("../../assets/Images/seating/message.svg")}
                  img3={require("../../assets/Images/seating/bell.svg")}
                  imgclass1={"ordertype-img"}
                  img4={require("../../assets/Images/seating/pedestrian-walking.svg")}
                  TotalTime={"25"}
                  Timer={"10:00"}
                  imgclass2={"edit-img"}
                  img8={require("../../assets/Images/seating/edit.svg")}
                  img5={require("../../assets/Images/seating/ocassion.svg")}
                  img6={require("../../assets/Images/seating/eye.svg")}
                  img7={require("../../assets/Images/seating/pen.svg")}
                  location={"Location"}
                  view={"View Location"}
                  img9={require("../../assets/Images/seating/maps-and-flags.svg")}
                  eta={"ETA"}
                  etatime={"25 Minutes"}
                />
              </tbody>
            </table>
          </div>
        </div>
        <PaginationComponent />
      </React.Fragment>
    );
  }
}

export default MainSeating;
