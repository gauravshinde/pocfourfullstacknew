import React, { Component } from "react";
import InputComponent from "../SmallComponents/InputComponent";
import ButtonComponent from "../SmallComponents/ButtonComponent";
import HeaderComponent from "../Header/HeaderComponent";
import Largetext from "../SmallComponents/LargeText";
// import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../store/actions/authAction";
import { loginValidate } from "../../store/validation/loginValidation";

export class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      errors: {}
    };
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      if (this.props.auth.user.role === "superadmin") {
        this.props.history.push("/vendor-list");
      } else if (this.props.auth.user.role === "vendor") {
        this.props.history.push("/vendor-dashboard");
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      if (nextProps.auth.user.role === "superadmin") {
        nextProps.history.push("/vendor-list");
      } else if (nextProps.auth.user.role === "vendor") {
        nextProps.history.push("/vendor-dashboard");
      }
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
      error: {}
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { errors, isValid } = loginValidate(this.state);
    if (!isValid) {
      this.setState({
        errors: errors
      });
    } else {
      const userData = {
        username: this.state.username,
        password: this.state.password
      };
      this.props.loginUser(userData);
    }
  };

  render() {
    // const { errors } = this.state;
    let errors = this.state.errors;
    console.log(this.state.errors);

    return (
      <React.Fragment>
        <HeaderComponent headertext={"Logo"} link={""} />
        <div className="container text-center">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-sm-none" />
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 login-align">
              <Largetext
                largetext={"Welcome!"}
                largetextclass={"welcome-text mb-4 mt-3"}
              />

              <p>
                {/* It's nice to see you again!
                <br /> */}
                Login to continue to your account
              </p>

              <form noValidate onSubmit={this.handleSubmit}>
                <Largetext
                  largetext={"Login"}
                  largetextclass={"login-text"}
                  mainclass={"col-12 my-3 px-0"}
                />

                <InputComponent
                  img={require("./../../assets/Images/usernamepic.svg")}
                  alt={"username"}
                  labeltext={"Username"}
                  name={"username"}
                  type={"text"}
                  value={this.state.username}
                  place={"eg. James Bond"}
                  inputclass={"form-control input-bottomblack "}
                  onChange={this.handleChange}
                />
                <div className="col-12 main-validation-div">
                  {errors.username ? (
                    <span className="isnotvalid">{errors.username}</span>
                  ) : (
                    ""
                  )}
                </div>

                <InputComponent
                  img={require("./../../assets/Images/passwordpic.svg")}
                  alt={"password"}
                  labeltext={"Password"}
                  name={"password"}
                  type={"password"}
                  value={this.state.password}
                  place={"*****"}
                  inputclass={"form-control input-bottomblack"}
                  onChange={this.handleChange}
                />
                <div className="col-12 main-validation-div">
                  {errors.password ? (
                    <span className="isnotvalid">{errors.password}</span>
                  ) : (
                    ""
                  )}
                </div>

                {/* <Link to="/vendor-list"> */}
                <ButtonComponent
                  buttontype={"submit"}
                  buttonclass={"login-button"}
                  buttontext={"Login"}
                  // handleOnClick={this.handleShowAll}
                />
                {/* </Link> */}
              </form>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-sm-none" />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
