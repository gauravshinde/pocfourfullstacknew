import {
  GET_PENDING_REVIEWS_DETAILS,
  GET_APPROVED_REVIEWS_DETAILS,
  EDIT_PENDING_REVIEWS_DETAILS,
  LOG_OUT
} from "./../../types";

const initialState = {
  get_pending_reviews_details: [],
  get_approved_reviews_details: [],
  edit_pending_reviews_details: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PENDING_REVIEWS_DETAILS:
      //   console.log(action.payload);
      return {
        ...state,
        get_pending_reviews_details: action.payload
      };
    case GET_APPROVED_REVIEWS_DETAILS:
      //   console.log(action.payload);
      return {
        ...state,
        get_approved_reviews_details: action.payload
      };
    case EDIT_PENDING_REVIEWS_DETAILS:
      //   console.log(action.payload);
      return {
        ...state,
        edit_pending_reviews_details: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
