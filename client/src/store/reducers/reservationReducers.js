import { GET_RESERVATION_DETAILS, LOG_OUT } from "../types";

const initialState = {
  get_all_reservation: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_RESERVATION_DETAILS:
      console.log(action.payload);
      return {
        ...state,
        get_all_reservation: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
