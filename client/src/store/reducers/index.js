import { combineReducers } from "redux";
import errorReducer from "./errorReducers";
import authReducers from "./authReducers";
import iconReducers from "./iconReducers";
import restaurantReducers from "./restaurantReducers";
import dinnerReducer from "../../pocsrcone/store/reducer/dinnerReducer";
import vendoreditReducer from "../../pocsrcone/store/reducer/vendoreditReducer";
import notseatedReducer from "../../pocsrcone/store/reducer/showStatusReducer";
import vendordetails from "../../pocsrcone/store/reducer/vendordetailsReducer";
import statuschangeReducer from "../../pocsrcone/store/reducer/statuschangeReducer";
import smsReducer from "../../pocsrcone/store/reducer/smsReducer";
import DefaultWaitTimeReducer from "../../pocsrcone/store/reducer/defaultWaitTimeReducer";

import QrCode from "./qrcodeReducers";

import viewAllImages from "../reducers/ViewAllUserImagesReducers";

/*************** @DESC POC FOUR OFFERS ALL REDUCERS ****************/
import OffersReducers from "./allOffersReducers";
import EventReducers from "./allEventsReducers";
import PendingDetailReducers from "./SeatingPageReducers/SeatingPageReducers";
import RatingAndReviewsReducers from "./ReviewsAndRatingReducers/ReviewsAndRatingReducers";

/*****************************************
 * @DESC POC ONE DASHBOARD ALL REDUCERS
 ******************************************/
import reservationReducer from "./reservationReducers";

/*****************************************
 * @DESC POC TWO DASHBOARD ALL REDUCERS
 ******************************************/
import getallReducers from "../../PocTwo/store/reducres/allGetReducers";
import editallReducers from "../../PocTwo/store/reducres/allEditReducers";

/*****************************************
 * @DESC SUPER ADMIN ALL REDUCERS
 ******************************************/
import superAdminVendorDetails from "./SuperAdminReducers/SuperAdminAllGetReducers";
import superadminvendoreditdetails from "./SuperAdminReducers/SuperAdminAllPatchReducers";
import menuAllGetReduers from "./SuperAdminReducers/menu/MenuAllGetReduers";
import NominationAllReducers from "./SuperAdminReducers/nomination/NominationAllReducers";
import OpenUserIssues from "./SuperAdminReducers/OpenUserIssues/SuperAdminOpenUserTicket";
import AllUsersReducer from "./SuperAdminReducers/AllUsers/AllUsersReducer";

export default combineReducers({
  errors: errorReducer,
  auth: authReducers,
  icons: iconReducers,
  details: restaurantReducers,
  dinner: dinnerReducer,
  vendoredit: vendoreditReducer,
  vendordetails: vendordetails,
  notseated: notseatedReducer,
  statuschange: statuschangeReducer,
  smssend: smsReducer,
  defaultWaitTime: DefaultWaitTimeReducer,
  qrCode: QrCode,

  /*************** @DESC POC FOUR VIEW ALL USERS IMAGES **********************/
  viewAllUserImages: viewAllImages,

  /*************** @DESC POC FOUR OFFERS ALL REDUCERS ****************/
  alloffers: OffersReducers,
  allevents: EventReducers,
  allpendingDetails: PendingDetailReducers,
  allRatingReviews: RatingAndReviewsReducers,

  /*****************************************
   * @DESC POC ONE DASHBOARD ALL REDUCERS
   ******************************************/
  reservation: reservationReducer,

  /*****************************************
   * @DESC POC TWO DASHBOARD ALL REDUCERS
   ******************************************/
  getall: getallReducers,
  alledit: editallReducers,

  /*****************************************
   * @DESC SUPER ADMIN ALL REDUCERS
   ******************************************/
  superAdminGetDetails: superAdminVendorDetails,
  superadminediydetails: superadminvendoreditdetails,
  menuAllGetReduers: menuAllGetReduers,
  nominationReducers: NominationAllReducers,
  openUserIssues: OpenUserIssues,
  AllUsersReducer: AllUsersReducer,
});
