import { GET_NOMINATION, LOG_OUT } from "../../../types";

const initialState = {
  getNomination: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_NOMINATION:
      //   console.log(action.payload);
      return {
        ...state,
        getNomination: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
