//

import { GET_ALL_USER_ORDER_DATA } from "../../../types";

const initialState = {
  orderData: {},
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_ALL_USER_ORDER_DATA:
      return {
        ...state,
        orderData: payload,
      };

    default:
      return state;
  }
}
