import {
  GET_SUPER_ADMIN_USERS_DETAILS,
  GET_SUPER_ADMIN_VENDOR_DETAILS,
  GET_SUPER_ADMIN_VENDOR_NOT_APPROVED_DETAILS,
  LOG_OUT
} from "../../types";

const initialState = {
  SuperAdminUserDetails: {},
  SuperAdminVendorDetails: {},
  SuperAdminVendorNotApprovedDetails: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_SUPER_ADMIN_USERS_DETAILS:
      // console.log(action.payload);
      return {
        ...state,
        SuperAdminUserDetails: action.payload
      };
    case GET_SUPER_ADMIN_VENDOR_DETAILS:
      // console.log(action.payload);
      return {
        ...state,
        SuperAdminVendorDetails: action.payload
      };
    case GET_SUPER_ADMIN_VENDOR_NOT_APPROVED_DETAILS:
      //console.log(action.payload);
      return {
        ...state,
        SuperAdminVendorNotApprovedDetails: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
