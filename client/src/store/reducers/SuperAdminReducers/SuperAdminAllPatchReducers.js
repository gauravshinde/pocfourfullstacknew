import {
  SUPER_ADMIN_UPDATE_VENDOR_STATUS,
  SUPER_ADMIN_UPDATE_PENDING_VENDOR_STATUS,
  LOG_OUT
} from "../../types";

const initialState = {
  superAdminVendorStatus: {},
  superAdminPendingVendorStatus: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SUPER_ADMIN_UPDATE_VENDOR_STATUS:
      return {
        ...state,
        superAdminVendorStatus: action.payload
      };
    case SUPER_ADMIN_UPDATE_PENDING_VENDOR_STATUS:
      return {
        ...state,
        superAdminPendingVendorStatus: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
