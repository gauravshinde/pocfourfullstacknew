import {
  GET_MENU_ITEMS,
  LOG_OUT
  // GET_SPECIFIC_MENU
} from "../../../types";

const initialState = {
  menuItems: {}
  // specificMenu: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_MENU_ITEMS:
      return {
        ...state,
        menuItems: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    // case GET_SPECIFIC_MENU:
    //   return {
    //     ...state,
    //     specificMenu: action.payload
    //   };
    default:
      return state;
  }
}
