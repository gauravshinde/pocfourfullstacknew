import {
  SUPER_ADMIN_GET_USER_OPEN_ISSUES_RAISE_TICKET,
  SUPER_ADMIN_GET_USER_CLOSED_ISSUES_RAISE_TICKET,
  SUPER_ADMIN_UPDATE_USER_OPEN_ISSUES_RAISE_TICKET,
  SUPER_ADMIN_UPDATE_USER_CLOSED_ISSUES_RAISE_TICKET,
  LOG_OUT
} from "../../../types";

const initialState = {
  SuperAdminUserOpenIssuesTicket: {},
  SuperAdminUserClosedIssuesTicket: {},
  SuperAdminUserUpdateOpenIssuesTicket: {},
  SuperAdminUserUpdateClosedIssuesTicket: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SUPER_ADMIN_GET_USER_OPEN_ISSUES_RAISE_TICKET:
      // console.log(action.payload);
      return {
        ...state,
        SuperAdminUserOpenIssuesTicket: action.payload
      };
    case SUPER_ADMIN_GET_USER_CLOSED_ISSUES_RAISE_TICKET:
      console.log(action.payload);
      return {
        ...state,
        SuperAdminUserClosedIssuesTicket: action.payload
      };
    case SUPER_ADMIN_UPDATE_USER_OPEN_ISSUES_RAISE_TICKET:
      console.log(action.payload);
      return {
        ...state,
        SuperAdminUserUpdateOpenIssuesTicket: action.payload
      };
    case SUPER_ADMIN_UPDATE_USER_CLOSED_ISSUES_RAISE_TICKET:
      console.log(action.payload);
      return {
        ...state,
        SuperAdminUserUpdateClosedIssuesTicket: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
//1/29/2020
