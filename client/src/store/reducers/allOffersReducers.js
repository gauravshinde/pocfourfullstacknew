import { GET_OFFERS_DETAILS, EDIT_OFFERS_DETAILS, LOG_OUT } from "./../types";

const initialState = {
  get_all_offersdetails: [],
  all_edit_offers: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_OFFERS_DETAILS:
      //   console.log(action.payload);
      return {
        ...state,
        get_all_offersdetails: action.payload
      };
    case EDIT_OFFERS_DETAILS:
      //   console.log(action.payload);
      return {
        ...state,
        all_edit_offers: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
