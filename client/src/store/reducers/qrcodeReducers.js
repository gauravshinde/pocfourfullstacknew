import { GET_QR_CODE, LOG_OUT } from "./../types";

const initialState = {
  get_qrcode: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_QR_CODE:
      // console.log(action.payload);
      return {
        ...state,
        get_qrcode: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
