import {
  GET_EVENTS_DETAILS,
  EDIT_EVENTS_DETAILS,
  GET_MARKER_POSITION,
  LOG_OUT
} from "./../types";

const initialState = {
  get_all_eventsdetails: [],
  all_edit_events: [],
  markerPosition: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_EVENTS_DETAILS:
      // console.log(action.payload);
      return {
        ...state,
        get_all_eventsdetails: action.payload
      };
    case EDIT_EVENTS_DETAILS:
      //   console.log(action.payload);
      return {
        ...state,
        all_edit_events: action.payload
      };
    case GET_MARKER_POSITION:
      return {
        ...state,
        markerPosition: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
