import {
  PENDING_GET_DETAILS,
  PENDING_ACCEPT_STATUS,
  HISTORY_GET_DETAILS,
  LOG_OUT
} from "../../types";

const initialState = {
  get_all_pending: [],
  update_status_reservation: [],
  get_all_history: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PENDING_GET_DETAILS:
      // console.log(action.payload);
      return {
        ...state,
        get_all_pending: action.payload
      };
    case PENDING_ACCEPT_STATUS:
      // console.log(action.payload);
      return {
        ...state,
        update_status_reservation: action.payload
      };
    case HISTORY_GET_DETAILS:
      // console.log(action.payload);
      return {
        ...state,
        get_all_history: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
