import {
  GET_RESTRAUNT_TYPE_ICON,
  GET_DRESS_CODE_ICON,
  GET_PAYMENT_ICON,
  GET_FACULTY_ICONS,
  GET_SERVICES_ICONS,
  GET_RESTRAUNT_FEATURES_ICONS,
  GET_RESTRAUNT_ACCESS_ICONS,
  GET_PARKING_SERVICES_ICONS,
  GET_FOOD_CATEGORY_ICONS,
  GET_FOOD_ITEMS_ICONS,
  GET_FOOD_ITEMS_ALLERGY_ICONS,
  GET_CUSINE_TYPE_ICONS,
  GET_SEATING_AREA_ICONS,
  GET_SUBSCRIPTION_ICON,
  LOG_OUT
} from "../types";

const initialState = {
  restaurant_type_icons: [],
  dress_code_icons: [],
  payment_methods_icons: [],
  get_faculty_icons: [],
  get_services_icons: [],
  get_restrant_features_icons: [],
  get_restraunt_access_icons: [],
  get_parking_icons: [],
  get_food_category_icons: [],
  get_food_item_icons: [],
  get_food_item_allergy_icons: [],
  get_cusine_types_icons: [],
  get_seating_area_icons: [],
  get_subscription_icon: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_RESTRAUNT_TYPE_ICON:
      return {
        ...state,
        restaurant_type_icons: action.payload
      };
    case GET_DRESS_CODE_ICON:
      return {
        ...state,
        dress_code_icons: action.payload
      };
    case GET_PAYMENT_ICON:
      return {
        ...state,
        payment_methods_icons: action.payload
      };
    case GET_FACULTY_ICONS:
      return {
        ...state,
        get_faculty_icons: action.payload
      };
    case GET_SERVICES_ICONS:
      return {
        ...state,
        get_services_icons: action.payload
      };
    case GET_RESTRAUNT_FEATURES_ICONS:
      return {
        ...state,
        get_restrant_features_icons: action.payload
      };
    case GET_RESTRAUNT_ACCESS_ICONS:
      return {
        ...state,
        get_restraunt_access_icons: action.payload
      };
    case GET_PARKING_SERVICES_ICONS:
      return {
        ...state,
        get_parking_icons: action.payload
      };
    case GET_FOOD_CATEGORY_ICONS:
      return {
        ...state,
        get_food_category_icons: action.payload
      };
    case GET_FOOD_ITEMS_ICONS:
      return {
        ...state,
        get_food_item_icons: action.payload
      };
    case GET_FOOD_ITEMS_ALLERGY_ICONS:
      return {
        ...state,
        get_food_item_allergy_icons: action.payload
      };
    case GET_CUSINE_TYPE_ICONS:
      return {
        ...state,
        get_cusine_types_icons: action.payload
      };
    case GET_SEATING_AREA_ICONS:
      return {
        ...state,
        get_seating_area_icons: action.payload
      };
    case GET_SUBSCRIPTION_ICON:
      // console.log(action.payload);
      return {
        ...state,
        get_subscription_icon: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
