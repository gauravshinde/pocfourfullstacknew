import {
  GET_VENDOR_USER_RESTAURANT_PHOTOS,
  GET_VENDOR_USER_RESTAURANT_KYC,
  GET_VENDOR_USER_RESTAURANT_MENU,
  LOG_OUT
} from "../types";

const initialState = {
  restaurant_photos: {},
  restaurant_kyc: {},
  restaurant_menu: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_VENDOR_USER_RESTAURANT_PHOTOS:
      // console.log(action.payload);
      return {
        ...state,
        restaurant_photos: action.payload
      };
    case GET_VENDOR_USER_RESTAURANT_KYC:
      return {
        ...state,
        restaurant_kyc: action.payload
      };
    case GET_VENDOR_USER_RESTAURANT_MENU:
      return {
        ...state,
        restaurant_menu: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
