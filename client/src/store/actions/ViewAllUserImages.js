import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_VENDOR_USER_RESTAURANT_PHOTOS,
  GET_VENDOR_USER_RESTAURANT_KYC,
  GET_VENDOR_USER_RESTAURANT_MENU,
} from "../types";
import { serverApi } from "../../config/Keys";
import { setErrors } from "./errorActions";
// import Alert from "react-s-alert";

/*******************************************
 * @DESC - GET VENDOR USER RESTAURANT PHOTO
 *******************************************/
export const get_all_user_restaurant_photo = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant_photo = await axios.get(
      `${serverApi}/details/kyc-image`
    );
    if (get_restaurant_photo.data) {
      // console.log(get_eventsDetails.data);
      dispatch({
        type: GET_VENDOR_USER_RESTAURANT_PHOTOS,
        payload: get_restaurant_photo.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/*******************************************
 * @DESC - GET VENDOR USER RESTAURANT KYC
 *******************************************/
export const get_all_user_restaurant_kyc = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant_kyc = await axios.get(`${serverApi}/details/kyc-image`);
    if (get_restaurant_kyc.data) {
      // console.log(get_eventsDetails.data);
      dispatch({
        type: GET_VENDOR_USER_RESTAURANT_KYC,
        payload: get_restaurant_kyc.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/*******************************************
 * @DESC - GET VENDOR USER RESTAURANT MENU
 *******************************************/
export const get_all_user_restaurant_menu = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant_menu = await axios.get(`${serverApi}/details/kyc-image`);
    if (get_restaurant_menu.data) {
      // console.log(get_eventsDetails.data);
      dispatch({
        type: GET_VENDOR_USER_RESTAURANT_MENU,
        payload: get_restaurant_menu.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - DELETE ACTION
 *****************************************/
// export const delete_newEvents = deleteData => async dispatch => {
//   console.log(deleteData);
//   try {
//     let delete_data = await axios.post("/event/delete-event", deleteData);
//     if (delete_data.data) {
//       Alert.success("<h4>Event Deleted Successfully</h4>", {
//         position: "top-right",
//         effect: "bouncyflip",
//         beep: true,
//         html: true,
//         timeout: 5000,
//         offset: 30
//       });
//       dispatch(get_events_details());
//     }
//   } catch (err) {
//     console.log(err);
//   }
// };
