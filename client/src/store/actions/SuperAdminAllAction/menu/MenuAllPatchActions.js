import axios from "axios";
import { SET_LOADER, CLEAR_LOADER } from "../../../types";
import { setErrors } from "../../errorActions";
import { serverApi } from "../../../../config/Keys";

/***************************************
 * @DESC - UPDATE RESTAURANT SETTING DETAILS
 *****************************************/

export const edit_menu_item = (newData, history) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let edit_menu_item = await axios.patch(
      `${serverApi}/menu/update-menu`,
      newData
    );
    // console.log(edit_menu_item.data);
    dispatch({ type: CLEAR_LOADER });
    history.push("viewvendordetails");
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
