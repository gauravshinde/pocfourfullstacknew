import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_MENU_ITEMS,
  // GET_SPECIFIC_MENU
} from "../../../types";
import { setErrors } from "../../errorActions";
import { serverApi } from "../../../../config/Keys";

/*****************************************
 * @DESC - MENU GET ITEMS
 *****************************************/

export const menu_get_items = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_items = await axios.get(`${serverApi}/menu/get-menu`);
    if (get_items.data) {
      // console.log(get_items.data);
      dispatch({
        type: GET_MENU_ITEMS,
        payload: get_items.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

// /*****************************************
//  * @DESC - MENU GET SPECIFIC MENU
//  *****************************************/

// export const get_specific_menu = formData => async dispatch => {
//   // console.log(formData);
//   dispatch({ type: SET_LOADER });
//   try {
//     let get_menu = await axios.post(`${serverApi}/menu/get-each-menu`, formData._id);
//     if (get_menu.data) {
//       console.log(get_menu.data);
//       dispatch({
//         type: GET_SPECIFIC_MENU,
//         payload: get_menu.data
//       });
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };
