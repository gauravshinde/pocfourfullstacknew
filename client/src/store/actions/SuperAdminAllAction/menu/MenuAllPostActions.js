import axios from "axios";
import { SET_LOADER, CLEAR_LOADER } from "../../../types";
import { setErrors } from "../../errorActions";
import { serverApi } from "../../../../config/Keys";

/*****************************************
 * @DESC - POST MENU ITEM
 *****************************************/

export const add_menu_item = (formData, history) => async (dispatch) => {
  // console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post(`${serverApi}/menu/add-menu`, formData);
    if (new_poc.data) {
      // console.log("SUCCESS");
      dispatch({ type: CLEAR_LOADER });
      history.push("viewvendordetails");
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
