import axios from "axios";
import { SET_LOADER, CLEAR_LOADER } from "./../../../types";
import { setErrors } from "./../../errorActions";
import { serverApi } from "../../../../config/Keys";
import { GET_NOMINATION } from "./../../../types";

export const superAdmin_get_nomination = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_nominations = await axios.get(
      `${serverApi}/nomination/get-nomination`
    );
    if (get_nominations.data) {
      //   console.log(get_nominations.data);
      dispatch({
        type: GET_NOMINATION,
        payload: get_nominations.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
