import axios from "axios";
import Alert from "react-s-alert";
import { superAdmin_get_users_details } from "../SuperAdminAllGetActions";
import {
  GET_ALL_USER_ORDER_DATA,
  EDIT_ALL_USER_PROFILE_DATA,
  EDIT_ALL_USER_RESET_PASSWORD,
} from "../../../types";
import { serverApi } from "../../../../config/Keys";

// Message Popup

export const messageUser = (formdata) => async (dispatch) => {
  try {
    let res = await axios.post(`${serverApi}/dinner/send`, formdata);

    //window.alert("Offer Deleted Successfully");
    Alert.success("<h4>Messege sent Successfully</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 2000,
      offset: 30,
    });
  } catch (error) {
    console.log(error);
  }
};

//Call Popup

export const callUser = (formdata) => (async) => {
  try {
    const res = axios.post(`${serverApi}/dinner/call_twilio`, formdata);
    console.log(res);
  } catch (error) {
    console.log(error);
  }
};

// Notification Popup
export const sendNotification = (formdata) => (async) => {
  try {
    const res = axios.post(`${serverApi}/dinner/push`, formdata);
    console.log(res);
  } catch (error) {
    console.log(error);
  }
};

// Delete Popup

export const deleteUser = (formdata, history) => async (dispatch) => {
  try {
    let delete_data = await axios.post(
      `${serverApi}/amaeliousers/delete_user_account`,
      formdata
    );
    if (delete_data.data) {
      //window.alert("Offer Deleted Successfully");
      console.log(delete_data);
      Alert.success("<h4>User Deleted Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 30,
      });

      dispatch(superAdmin_get_users_details());
      history.push("/allusers");
    }
  } catch (err) {
    console.log(err);
  }
};

/*********************************
 * @DESC Lock User Button Action
 *********************************/

export const lockUser = (formdata) => async (dispatch) => {
  //console.log(formdata);
  try {
    let lock_user = await axios.patch(
      `${serverApi}/amaeliousers/user_locked`,
      formdata
    );
    if (lock_user.data) {
      // console.log(lock_user.data);
      Alert.success("<h4>User Locked Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 30,
      });
      dispatch(superAdmin_get_users_details());
    }
  } catch (err) {
    console.log(err);
  }
};

/*********************************
 * @DESC GET USER ORDER
 *********************************/

export const getUserOrder = (user_id) => async (dispatch) => {
  // console.log("hioii", user_id);

  const formData = {
    user_id,
  };

  // console.log("val", formData);

  try {
    const res = await axios.post(
      `${serverApi}/amaeliousers/get_user_order`,
      formData
    );
    console.log("yooo", res);
    dispatch({
      type: GET_ALL_USER_ORDER_DATA,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};

/**************************************
 *   SUPER_ADMIN_EDIT_USER_POPUP
 ***************************************/
export const superAdminEditUser = (formData) => async (dispatch) => {
  //console.log(formData);
  try {
    let edit_user = await axios.patch(
      `${serverApi}/amaeliousers/user_update`,
      formData
    );
    if (edit_user.data) {
      //console.log(edit_user.data);
      Alert.success("<h4>User Profile Update Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 30,
      });
      dispatch(superAdmin_get_users_details());
    }
  } catch (err) {
    dispatch({
      type: EDIT_ALL_USER_PROFILE_DATA,
      payload: err.response.data,
    });
  }
};

/**************************************
 *   SUPER_ADMIN_USER_RESET_PASSWORD
 ***************************************/
export const superAdminUserResetPassword = (formData) => async (dispatch) => {
  //console.log(formData);
  try {
    let reset_password = await axios.patch(
      `${serverApi}/amaeliousers/user_password_update`,
      formData
    );
    if (reset_password.data) {
      console.log(reset_password.data);
      Alert.success("<h4>User Password Reset Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 3000,
        offset: 30,
      });
      dispatch(superAdmin_get_users_details());
    }
  } catch (err) {
    dispatch({
      type: EDIT_ALL_USER_RESET_PASSWORD,
      payload: err.response.data,
    });
  }
};
