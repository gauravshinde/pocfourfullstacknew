import axios from "axios";
import {
  SUPER_ADMIN_UPDATE_VENDOR_STATUS,
  SUPER_ADMIN_UPDATE_PENDING_VENDOR_STATUS,
} from "../../types";
import Alert from "react-s-alert";
import { serverApi } from "../../../config/Keys";
// import { setErrors } from "../errorActions";
// import { superAdmin_get_vendor_details } from "../SuperAdminAllAction/SuperAdminAllGetActions";

/**************************************
 *   SUPER_ADMIN_UPDATE_VENDOR_STATUS
 ***************************************/
export const superAdmin_update_vendor_status = (formData) => (dispatch) => {
  console.log(formData);
  axios
    .patch(`${serverApi}/users/vendor_approved`, formData)
    .then(
      (res) =>
        Alert.success("<h4>Vendor Status Changed</h4>", {
          position: "top-right",
          effect: "bouncyflip",
          beep: true,
          html: true,
          timeout: 5000,
          offset: 35,
        }),
      window.location.reload()
      // dispatch(superAdmin_get_vendor_details())
    )
    .catch((err) =>
      dispatch({
        type: SUPER_ADMIN_UPDATE_VENDOR_STATUS,
        payload: err.response.data,
      })
    );
};

/**************************************
 *   SUPER_ADMIN_UPDATE_PENDING_VENDOR_STATUS
 ***************************************/
export const superAdmin_update_pending_vendor_status = (formData, history) => (
  dispatch
) => {
  console.log(formData);
  axios
    .patch(`${serverApi}/add_item/update_item`, formData)
    // .patch("/all_vendor/:vendorId", editUser)
    .then((res) => history.push("/vendor-Menu"))
    .catch((err) =>
      dispatch({
        type: SUPER_ADMIN_UPDATE_PENDING_VENDOR_STATUS,
        payload: err.response.data,
      })
    );
};
