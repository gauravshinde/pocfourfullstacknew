import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_SUPER_ADMIN_USERS_DETAILS,
  GET_SUPER_ADMIN_VENDOR_DETAILS,
  GET_SUPER_ADMIN_VENDOR_NOT_APPROVED_DETAILS,
} from "./../../types";
import { setErrors } from "./../errorActions";
import { serverApi } from "../../../config/Keys";
// import { serverName } from "./../../../config/Keys";

/***************************************
 * @DESC - SUPER ADMIN GET ALL USERS DETAILS
 *****************************************/

export const superAdmin_get_users_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_users_details = await axios.get(
      `${serverApi}/amaeliousers/get-all-user`
    ); //`${serverName}/users/get-all-user`
    if (get_users_details.data) {
      //console.log(get_users_details.data);
      dispatch({
        type: GET_SUPER_ADMIN_USERS_DETAILS,
        payload: get_users_details.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - SUPER ADMIN GET ALL VENDOR DETAILS
 *****************************************/

export const superAdmin_get_vendor_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_vendor_details = await axios.get(
      `${serverApi}/details/get_approve_vendor_details`
    );
    if (get_vendor_details.data) {
      //console.log(get_vendor_details.data);
      dispatch({
        type: GET_SUPER_ADMIN_VENDOR_DETAILS,
        payload: get_vendor_details.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - SUPER ADMIN GET ALL PENDING VENDOR DETAILS
 *****************************************/

export const superAdmin_get_not_approved_vendor_details = () => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_not_approved_vendor_details = await axios.get(
      `${serverApi}/details/get_not_approve_vendor_details`
    );
    if (get_not_approved_vendor_details.data) {
      // console.log(get_not_approved_vendor_details.data);
      dispatch({
        type: GET_SUPER_ADMIN_VENDOR_NOT_APPROVED_DETAILS,
        payload: get_not_approved_vendor_details.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
