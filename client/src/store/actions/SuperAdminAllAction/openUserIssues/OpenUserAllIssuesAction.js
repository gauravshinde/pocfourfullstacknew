import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  SUPER_ADMIN_GET_USER_OPEN_ISSUES_RAISE_TICKET,
  SUPER_ADMIN_GET_USER_CLOSED_ISSUES_RAISE_TICKET,
  SUPER_ADMIN_UPDATE_USER_OPEN_ISSUES_RAISE_TICKET,
  SUPER_ADMIN_UPDATE_USER_CLOSED_ISSUES_RAISE_TICKET,
} from "../../../types";
import { setErrors } from "../../errorActions";
import { serverApi } from "../../../../config/Keys";
import Alert from "react-s-alert";

/************************************************
 * @DESC - SUPER ADMIN GET ALL USERS OPEN ISSUES
 *************************************************/

export const super_admin_get_open_users_issues_tickets = () => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_open_issues_users_ticket = await axios.get(
      `${serverApi}/raise/get-raise`
    );
    if (get_open_issues_users_ticket.data) {
      console.log(get_open_issues_users_ticket.data);
      dispatch({
        type: SUPER_ADMIN_GET_USER_OPEN_ISSUES_RAISE_TICKET,
        payload: get_open_issues_users_ticket.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/************************************************
 * @DESC - SUPER ADMIN GET ALL USERS OPEN ISSUES
 *************************************************/

export const super_admin_get_closed_users_issues_tickets = () => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_closed_issues_users_ticket = await axios.get(
      `${serverApi}/raise/get-raise-closed`
    );
    if (get_closed_issues_users_ticket.data) {
      console.log(get_closed_issues_users_ticket.data);
      dispatch({
        type: SUPER_ADMIN_GET_USER_CLOSED_ISSUES_RAISE_TICKET,
        payload: get_closed_issues_users_ticket.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/************************************************
 * @DESC - SUPER ADMIN UPDATE ALL USERS OPEN ISSUES
 *************************************************/

export const super_admin_update_open_users_issues_tickets = (update) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  axios
    .patch(`${serverApi}/raise/update-issue`, update)
    // .patch("/all_vendor/:vendorId", editUser)
    .then((res) => {
      Alert.success("<h4>User Issues UnSolved Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      window.location.href = "/openissues-users";
      // history.push("/openissues-users");
      dispatch(super_admin_get_closed_users_issues_tickets);
    })
    .catch((err) =>
      dispatch({
        type: SUPER_ADMIN_UPDATE_USER_CLOSED_ISSUES_RAISE_TICKET,
        payload: err.response.data,
      })
    );
};

/************************************************
 * @DESC - SUPER ADMIN UPDATE ALL USERS CLOSED ISSUES
 *************************************************/

export const super_admin_update_closed_users_issues_tickets = (
  update,
  history
) => async (dispatch) => {
  // console.log(update);
  dispatch({ type: SET_LOADER });
  axios
    .patch(`${serverApi}/raise/update-issue`, update)
    // .patch("/all_vendor/:vendorId", editUser)
    .then((res) => {
      Alert.success("<h4>User Issues Solved Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      window.location.href = "/openissues-users";
      // history.push("/openissues-users");
      dispatch(super_admin_get_open_users_issues_tickets);
    })
    .catch((err) =>
      dispatch({
        type: SUPER_ADMIN_UPDATE_USER_OPEN_ISSUES_RAISE_TICKET,
        payload: err.response.data,
      })
    );
};
