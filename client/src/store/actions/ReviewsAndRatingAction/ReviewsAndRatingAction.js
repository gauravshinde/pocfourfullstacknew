import axios from "axios";
import {
  GET_PENDING_REVIEWS_DETAILS,
  GET_APPROVED_REVIEWS_DETAILS,
  EDIT_PENDING_REVIEWS_DETAILS,
  SET_LOADER,
  CLEAR_LOADER,
} from "./../../types";
import { setErrors } from "../errorActions";
import { serverApi } from "../../../config/Keys";

import Alert from "react-s-alert";

/***************************************
 * @DESC - GET PENDING REVIEWS DETAILS
 *****************************************/

export const get_vendor_pending_reviews = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_vendor_pending_reviews = await axios.get(
      `${serverApi}/review/get-restaurant-review`
    );
    if (get_vendor_pending_reviews.data) {
      //   console.log(get_vendor_pending_reviews.data);
      dispatch({
        type: GET_PENDING_REVIEWS_DETAILS,
        payload: get_vendor_pending_reviews.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET PENDING REVIEWS DETAILS
 *****************************************/

export const get_vendor_approved_reviews = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_vendor_approved_reviews = await axios.get(
      `${serverApi}/review/get-restaurant-review-approve`
    );
    if (get_vendor_approved_reviews.data) {
      //   console.log(get_vendor_approved_reviews.data);
      dispatch({
        type: GET_APPROVED_REVIEWS_DETAILS,
        payload: get_vendor_approved_reviews.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE ACTION
 *****************************************/
export const update_vendor_pending_status = (updateReview) => async (
  dispatch
) => {
  console.log(updateReview);
  try {
    let update_vendor_pending_status = await axios.patch(
      `${serverApi}/review/approve-restaurant-review`,
      updateReview
    );
    if (update_vendor_pending_status.data) {
      Alert.success("<h4>Review Successfully Approved</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: false,
        html: true,
        timeout: 5000,
        // offset: 100
      });
      dispatch(get_vendor_pending_reviews());
    }
  } catch (err) {
    dispatch({
      type: EDIT_PENDING_REVIEWS_DETAILS,
      payload: err.response.data,
    });
  }
  //   axios
  //     .patch(`/review/approve-restaurant-review`, editOffers)
  //     // .patch("/all_vendor/:vendorId", editUser)
  //     .then(res => dispatch(get_vendor_pending_reviews()))
  //     .catch(err =>
  //       dispatch({
  //         type: EDIT_PENDING_REVIEWS_DETAILS,
  //         payload: err.response.data
  //       })
  //     );
};

/***************************************
 * @DESC - DELETE ACTION
 *****************************************/
export const delete_vendor_pending_status = (deleteData) => async (
  dispatch
) => {
  //   console.log(deleteData);
  try {
    let delete_vendor_pending_status = await axios.post(
      `${serverApi}/review/reject-restaurant-review`,
      deleteData
    );
    if (delete_vendor_pending_status.data) {
      Alert.success("<h4>Review Deleted Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: false,
        html: true,
        timeout: 5000,
        // offset: 100
      });
      dispatch(get_vendor_pending_reviews());
    }
  } catch (err) {
    console.log(err);
  }
};
