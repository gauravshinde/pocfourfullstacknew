import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_RESERVATION_DETAILS,
  //GET_PENDINGRESERVATION_DETAILS
} from "./../types";
import { setErrors } from "./errorActions";
import { serverApi } from "../../config/Keys";

/***************************************
 * @DESC - ADD RESERVATION POST API
 **************************************/
export const create_reservation = (formData) => async (dispatch) => {
  // console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    // let new_reservation = await axios.post('/dinner/add_dinner', formData);
    let new_reservation = await axios.post(
      `${serverApi}/reservation/reservation`,
      formData
    );
    if (new_reservation.data) {
      console.log('reservationadded', new_reservation);
      dispatch({ type: CLEAR_LOADER });
      dispatch(get_all_reservation());
      //   callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_all_reservation = (formData) => async (dispatch) => {
  // console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    // let get_reservation = await axios.post('/dinner/get-reservation', formData);
    let get_reservation = await axios.post(
      `${serverApi}/reservation/get-reservation`,
      formData
    );
    if (get_reservation.data) {
      dispatch({
        type: GET_RESERVATION_DETAILS,
        payload: get_reservation.data,
      });
      // dispatch(get_all_reservation());
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

// /***************************************
//  * @DESC - ADD RESERVATION GET API
//  **************************************/
// export const get_allPending_reservation = () => async dispatch => {
//   dispatch({ type: SET_LOADER });
//   try {
//     let get_allpendingreservation = await axios.get(
//       '/reservation/get-reservation'
//     );
//     if (get_allpendingreservation.data) {
//       console.log(get_allcategory.data);
//       dispatch({
//         type: GET_PENDINGRESERVATION_DETAILS,
//         payload: get_allpendingreservation.data
//       });
//     }
//   } catch (err) {
//     console.log(err);
//     dispatch({ type: CLEAR_LOADER });
//     dispatch(setErrors(err));
//   }
// };
