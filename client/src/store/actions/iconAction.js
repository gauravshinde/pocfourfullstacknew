import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_RESTRAUNT_TYPE_ICON,
  GET_DRESS_CODE_ICON,
  GET_PAYMENT_ICON,
  GET_SERVICES_ICONS,
  GET_FACULTY_ICONS,
  GET_RESTRAUNT_FEATURES_ICONS,
  GET_RESTRAUNT_ACCESS_ICONS,
  GET_PARKING_SERVICES_ICONS,
  GET_FOOD_CATEGORY_ICONS,
  GET_FOOD_ITEMS_ICONS,
  GET_FOOD_ITEMS_ALLERGY_ICONS,
  GET_CUSINE_TYPE_ICONS,
  GET_SUBSCRIPTION_ICON,
} from "../types";
import { serverApi } from "../../config/Keys";
import { setErrors } from "./errorActions";

export const getALLICONS = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let allICONS = await axios.get(`${serverApi}/icons/`);
    if (allICONS.data) {
      let allIcons = allICONS.data;
      dispatch(get_restaurant_type_icons(allIcons));
      dispatch(dress_code_icons(allIcons));
      dispatch(payment_methods_icons(allIcons));
      dispatch(faculty_icons(allIcons));
      dispatch(servics_icons(allIcons));
      dispatch(restraunt_access_icons(allIcons));
      dispatch(restraunt_features_icons(allIcons));
      dispatch(parking_icons(allIcons));
      dispatch(food_category_icons(allIcons));
      dispatch(food_item_icons(allIcons));
      dispatch(food_item_allergy_icons(allIcons));
      dispatch(cusinie_types_icons(allIcons));
      dispatch(subscription_icon(allIcons));
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_restaurant_type_icons = (data) => async (dispatch) => {
  let restaurant_type_icons = data.filter(
    (icons) => icons.category === "Restaurant Type"
  );
  dispatch({ type: GET_RESTRAUNT_TYPE_ICON, payload: restaurant_type_icons });
};

export const dress_code_icons = (data) => async (dispatch) => {
  let dress_code_icon = data.filter((icons) => icons.category === "Dress Code");
  dispatch({ type: GET_DRESS_CODE_ICON, payload: dress_code_icon });
};

export const payment_methods_icons = (data) => async (dispatch) => {
  let payment_methods_icon = data.filter(
    (icons) => icons.category === "Payment Method"
  );
  dispatch({ type: GET_PAYMENT_ICON, payload: payment_methods_icon });
};

export const faculty_icons = (data) => async (dispatch) => {
  let faculty_icon = data.filter((icons) => icons.category === "Premises");
  dispatch({ type: GET_FACULTY_ICONS, payload: faculty_icon });
};

export const servics_icons = (data) => async (dispatch) => {
  let servics_icon = data.filter((icons) => icons.category === "Services");
  dispatch({ type: GET_SERVICES_ICONS, payload: servics_icon });
};

export const restraunt_features_icons = (data) => async (dispatch) => {
  let payment_methods_icon = data.filter(
    (icons) => icons.category === "Restaurant Features"
  );
  dispatch({
    type: GET_RESTRAUNT_FEATURES_ICONS,
    payload: payment_methods_icon,
  });
};

export const restraunt_access_icons = (data) => async (dispatch) => {
  let faculty_icon = data.filter(
    (icons) => icons.category === "Accessible restaurant"
  );
  dispatch({ type: GET_RESTRAUNT_ACCESS_ICONS, payload: faculty_icon });
};

export const parking_icons = (data) => async (dispatch) => {
  let servics_icon = data.filter(
    (icons) => icons.category === "Parking Services"
  );
  dispatch({ type: GET_PARKING_SERVICES_ICONS, payload: servics_icon });
};

export const food_category_icons = (data) => async (dispatch) => {
  let food_category_icon = data.filter(
    (icons) => icons.category === "Food Category"
  );
  dispatch({ type: GET_FOOD_CATEGORY_ICONS, payload: food_category_icon });
};

export const food_item_icons = (data) => async (dispatch) => {
  let food_item_icon = data.filter((icons) => icons.category === "Food Item");
  dispatch({ type: GET_FOOD_ITEMS_ICONS, payload: food_item_icon });
};

export const food_item_allergy_icons = (data) => async (dispatch) => {
  let food_item_allergy_icons = data.filter(
    (icons) => icons.category === "Allergy info"
  );
  dispatch({
    type: GET_FOOD_ITEMS_ALLERGY_ICONS,
    payload: food_item_allergy_icons,
  });
};

export const cusinie_types_icons = (data) => async (dispatch) => {
  let cusinie_types_icon = data.filter(
    (icons) => icons.category === "Cuisine Type"
  );
  dispatch({ type: GET_CUSINE_TYPE_ICONS, payload: cusinie_types_icon });
};

export const seating_area_icons = (data) => async (dispatch) => {
  let seating_area_icon = data.filter(
    (icons) => icons.category === "Cuisine Type"
  );
  dispatch({ type: GET_CUSINE_TYPE_ICONS, payload: seating_area_icon });
};

export const subscription_icon = (data) => async (dispatch) => {
  // console.log(data);
  let subscription_icon = data.filter(
    (icons) => icons.category === "Seating Area"
  );
  // console.log(subscription_icon);
  dispatch({ type: GET_SUBSCRIPTION_ICON, payload: subscription_icon });
};

export const create_new_chain = (formdata) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_item_create = await axios.post(
      `${serverApi}/icons/chains`,
      formdata
    );
    if (new_item_create.data) {
      dispatch({ type: CLEAR_LOADER });
      window.alert("Chain Successfully Created!");
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_all_chains = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let getAllIcons = await axios.get(`${serverApi}/icons/chains`);
    if (getAllIcons.data) {
      dispatch({ type: CLEAR_LOADER });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
