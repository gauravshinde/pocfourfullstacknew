import axios from "axios";
import { SET_LOADER, CLEAR_LOADER, GET_QR_CODE } from "../types";
import { setErrors } from "./errorActions";
// import { serverApi } from "../../config/Keys";
import Alert from "react-s-alert";

/***************************************
 * @DESC - GET QR CODE ACTION
 *****************************************/
export const get_qr_code = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_qr_code = await axios.get("");
    if (get_qr_code.data) {
      // console.log(get_qr_code.data);
      dispatch({
        type: GET_QR_CODE,
        payload: get_qr_code.data,
      });
      // dispatch(get_events_details());
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

//10-01-2020
