import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  PENDING_GET_DETAILS,
  HISTORY_GET_DETAILS,
  PENDING_ACCEPT_STATUS,
} from "../../types";
import { setErrors } from "../errorActions";
import { serverApi } from "../../../config/Keys";

/***************************************
 * @DESC - GET PENDING DETAILS
 *****************************************/

export const get_pending_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    // let get_pending = await axios.get('/dinner/get-each-dinner-pending');
    let get_pending = await axios.get(
      `${serverApi}/reservation/get-reservation`
    );
    if (get_pending.data) {
      console.log(get_pending.data);
      dispatch({
        type: PENDING_GET_DETAILS,
        payload: get_pending.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET HISTORY DETAILS
 *****************************************/

export const get_history_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_history = await axios.get(`${serverApi}/dinner/get-history`);
    if (get_history.data) {
      // console.log(get_history.data);
      dispatch({
        type: HISTORY_GET_DETAILS,
        payload: get_history.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_new_accept = (formData) => (dispatch) => {
  console.log(formData);
  axios
    .patch(`${serverApi}/reservation/status-update`, formData)
    // .then(res => window.location.reload())
    .then((res) => {
      dispatch(get_pending_details());
    })
    .catch((err) =>
      dispatch({
        type: PENDING_ACCEPT_STATUS,
        payload: err.response.data,
      })
    );
};
