import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_RESTAURANT_DISPLAY_STATUS,
  UPDATE_RESTAURANT_DISPLAY_STATUS,
  GET_MAP_DETAILS,
  GET_POC_LIST,
  GET_RESTAURANT_DETAILS,
  GET_SERVICE_DETAILS,
  GET_RESTAURANT_FEATURES,
  GET_CUSINE_ONE,
  GET_CUSINE_TWO,
  GET_RESTAURANTTIME_DETAILS,
  GET_KYC_DETAILS,
  GET_SUBSCRIPTION_DETAILS,
  SET_STATUS,
} from "../types";
import { setErrors } from "./errorActions";
import { serverApi } from "../../config/Keys";
import Alert from "react-s-alert";

export const create_restaraunttime = (formData, callback) => async (
  dispatch
) => {
  //console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post(
      `${serverApi}/details/add-restaurantTime`,
      formData
    );
    if (new_poc.data) {
      // console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_Kycdetails = (formData, callback) => async (dispatch) => {
  //console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post(
      `${serverApi}/details/add-kycbank-details`,
      formData
    );
    if (new_poc.data) {
      //console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

// export const create_new_subscription = formData => dispatch => {
//   console.log(formData);
//   axios
//     .post('/details/add-subscription-details')
//     // .then(res => console.log(res.data))
//     .then(res => {
//       if (res.status === 200) {
//         dispatch({
//           type: SET_STATUS,
//           payload: '200'
//         });
//       }

//       console.log(res.status);
//     })
//     .catch(err => console.log(err));
// };

export const create_new_subscription = (formData, callback) => async (
  dispatch
) => {
  //console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post(
      `${serverApi}/details/add-subscription-details`,
      formData
    );
    if (new_poc.data) {
      //console.log(new_poc.status);

      dispatch({ type: SET_STATUS, payload: new_poc.status });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_new_poc = (formData, callback) => async (dispatch) => {
  //console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_poc = await axios.post(`${serverApi}/details/add-poc`, formData);
    if (new_poc.data) {
      //console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_new_restaurant = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      `${serverApi}/details/add-restaurant`,
      formData
    );
    if (new_restaurant.data) {
      //console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/**************************************************
 * @DESC - GET RESTAURANT OPEN AND CLOSED DETAILS
 *************************************************/

export const getRestaurantOpenClosed = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let getRestaurantOpenClosed = await axios.get(
      `${serverApi}/users/restaurant_current_status`
    );
    if (getRestaurantOpenClosed.data) {
      //console.log(getRestaurantOpenClosed.data);
      dispatch({
        type: GET_RESTAURANT_DISPLAY_STATUS,
        payload: getRestaurantOpenClosed.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/**************************************************
 * @DESC - UPDATE RESTAURANT OPEN AND CLOSED DETAILS
 *************************************************/

export const updateRestaurantOpenClosed = (formData) => (dispatch) => {
  // console.log(formData);
  axios
    .patch(`${serverApi}/users/resto_open_close`, formData)
    .then((res) => {
      Alert.success("<h4>Restaurant Status Update Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      dispatch(getRestaurantOpenClosed());
      // window.location.reload();
    })
    .catch((err) =>
      dispatch({
        type: UPDATE_RESTAURANT_DISPLAY_STATUS,
        payload: err.response.data,
      })
    );
};

/***************************************
 * @DESC - GET RESTAURANT DETAILS
 *****************************************/

export const get_restaurant_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get(`${serverApi}/details/get-restaurant`);
    if (get_restaurant.data) {
      //console.log(get_restaurant.data);
      dispatch({
        type: GET_MAP_DETAILS,
        payload: get_restaurant.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE RESTAURANT DETAILS
 *****************************************/

export const update_restaurant_details = (formData) => (dispatch) => {
  //console.log(formData);
  axios
    .patch(`${serverApi}/details/update-restaurant-details`, formData)
    // .patch("/all_vendor/:vendorId", editUser)
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err));
};

/***************************************
 * @DESC - POC DETAILS SECTION
 *****************************************/

export const get_poc_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get(`${serverApi}/details/get-poc`);
    if (get_restaurant.data) {
      //console.log(get_restaurant.data);
      dispatch({
        type: GET_POC_LIST,
        payload: get_restaurant.data,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const delete_poc_contact_details = (deleteData) => (dispatch) => {
  console.log(deleteData);
  axios
    .patch(`${serverApi}/details/delete-poc`, deleteData)
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err));
};

export const create_new_service_details = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      `${serverApi}/details/add-service-details`,
      formData
    );
    if (new_restaurant.data) {
      //console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_new_resto_features = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      `${serverApi}/details/add-restoFeatures`,
      formData
    );
    if (new_restaurant.data) {
      //console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_cusine_one = (formData, callback) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      `${serverApi}/details/add-cusineOne`,
      formData
    );
    if (new_restaurant.data) {
      //console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const create_cusine_two = (formData, callback) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      `${serverApi}/details/add-cusineTwo`,
      formData
    );
    if (new_restaurant.data) {
      //console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_restaurants_type_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get(`${serverApi}/details/get-restoType`);
    if (get_restaurant.data) {
      dispatch({
        type: GET_RESTAURANT_DETAILS,
        payload: get_restaurant.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_services_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get(
      `${serverApi}/details/get-service-details`
    );
    if (get_restaurant.data) {
      dispatch({
        type: GET_SERVICE_DETAILS,
        payload: get_restaurant.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_restaurant_features = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get(
      `${serverApi}/details/get-restoFeatures`
    );
    if (get_restaurant.data) {
      dispatch({
        type: GET_RESTAURANT_FEATURES,
        payload: get_restaurant.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_cusine_one = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get(`${serverApi}/details/get-cusineOne`);
    if (get_restaurant.data) {
      dispatch({
        type: GET_CUSINE_ONE,
        payload: get_restaurant.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const get_cusine_two = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restaurant = await axios.get(`${serverApi}/details/get-cusineTwo`);
    if (get_restaurant.data) {
      dispatch({
        type: GET_CUSINE_TWO,
        payload: get_restaurant.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET RESTAURANTTIME DETAILS
 *****************************************/
export const get_restauranttime = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_restauranttime = await axios.get(
      `${serverApi}/details/get-restaurantTime`
    );
    if (get_restauranttime.data) {
      dispatch({
        type: GET_RESTAURANTTIME_DETAILS,
        payload: get_restauranttime.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET KYC DETAILS
 *****************************************/
export const get_kyc_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_kyc_details = await axios.get(
      `${serverApi}/details/get-kycbank-details`
    );
    if (get_kyc_details.data) {
      dispatch({
        type: GET_KYC_DETAILS,
        payload: get_kyc_details.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE KYC DETAILS
 *****************************************/
export const update_kyc_details = (updateData) => (dispatch) => {
  console.log(updateData);
  axios
    .patch(`${serverApi}/details/update-restaurantTime`, updateData)
    // .patch("/all_vendor/:vendorId", editUser)
    .then((res) => console.log(res.data))
    .catch((err) => console.log(err));
};

/***************************************
 * @DESC - GET SUBSCRIPTION DETAILS
 *****************************************/
export const get_subscription_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_subscription_detailsone = await axios.get(
      `${serverApi}/details/get-subscription-details`
    );
    if (get_subscription_detailsone.data) {
      dispatch({
        type: GET_SUBSCRIPTION_DETAILS,
        payload: get_subscription_detailsone.data,
      });
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

// get.('icons/get-chain')

/***************************************
 * @DESC - UPDATE RESTAURANT SETTING DETAILS
 *****************************************/
export const update_restaurant_setting = (formData) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let updatenew_restaurant_setting = await axios.patch(
      `${serverApi}/details/update-restaurant`,
      formData
    );
    console.log(updatenew_restaurant_setting.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_new = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let updatenew_restaurant_new = await axios.patch(
      `${serverApi}/details/update-restaurant_new`,
      formData
    );
    console.log(updatenew_restaurant_new.data);
    callback();
    dispatch(get_restaurant_details());
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_person_contacts_new = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_person_contacts_new = await axios.patch(
      `${serverApi}/details/update-poc`,
      formData
    );
    console.log(update_person_contacts_new.data);
    callback();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_details_new = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_details_new = await axios.patch(
      `${serverApi}/details/update-restaurant-detailsone`,
      formData
    );
    console.log(update_restaurant_details_new.data);
    callback();
    dispatch(get_restaurants_type_details());
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/********************************
 * @DESC Settings page popup button
 *******************************/
export const update_restaurant_details_popup = (formData) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_details_popup = await axios.patch(
      `${serverApi}/details/update-restaurant-detailsone`,
      formData
    );
    window.location.reload(get_restaurants_type_details);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_service_details_new = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_service_details_new = await axios.patch(
      `${serverApi}/details/update-service-details`,
      formData
    );
    console.log(update_service_details_new.data);
    callback();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_service_details_new_popup = (formData) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_service_details_new_pop = await axios.patch(
      `${serverApi}/details/update-service-details`,
      formData
    );
    window.location.reload(get_restaurants_type_details);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_features_new = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_features_new = await axios.patch(
      `${serverApi}/details/update-restoFeatures`,
      formData
    );
    console.log(update_restaurant_features_new.data);
    callback();
    dispatch(get_restaurant_features());
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_cuisine_features_new = (
  formData,
  callback
) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_cuisine_features_new = await axios.patch(
      `${serverApi}/details/update-cusineOnedata`,
      formData
    );
    console.log(update_restaurant_cuisine_features_new.data);
    callback();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_cuisine_features_newPopup = (formData) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_cuisine_features_newPopup = await axios.patch(
      `${serverApi}/details/update-cusineOnedata`,
      formData
    );
    window.location.reload();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/*****************************************
 * @DESC Update Restaurent Features type
 ****************************************/
export const update_restaurant_features_popup = (formData) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_features_popup = await axios.patch(
      `${serverApi}/details/update-restoFeatures`,
      formData
    );
    window.location.reload();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_cuisine_features_newone = (
  formData,
  callback
) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_cuisine_features_newone = await axios.patch(
      `${serverApi}/details/update-cusineTwo_data`,
      formData
    );
    console.log(update_restaurant_cuisine_features_newone.data);
    callback();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_cuisine_features_newonePopup = (
  formData
) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_cuisine_features_newonePopup = await axios.patch(
      `${serverApi}/details/update-cusineTwo_data`,
      formData
    );
    window.location.reload();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_timings_new = (formData, callback) => async (
  dispatch
) => {
  //console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_timings_new = await axios.patch(
      `${serverApi}/details/update-restaurantTime-data`,
      formData
    );
    console.log(update_restaurant_timings_new.data);
    callback();
    dispatch(get_restauranttime());
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC Update Data PopUp
 ***************************************/

export const update_restaurant_timings_Popup = (formData) => async (
  dispatch
) => {
  //console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_timings_new = await axios.patch(
      `${serverApi}/details/update-restaurantTime-data`,
      formData
    );
    window.location.reload();
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_restaurant_kyc_new = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_kyc_new = await axios.patch(
      `${serverApi}/details/update-kycbank-details`,
      formData
    );
    console.log(update_restaurant_kyc_new.data);
    callback();
    dispatch(get_kyc_details());
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

export const update_subscription_new = (formData) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_subscription_new = await axios.patch(
      `${serverApi}/details/update-subscription`,
      formData
    );
    console.log(update_subscription_new.data);
    dispatch(get_subscription_details());
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE RESTAURANT SETTING DETAILS
 *****************************************/
export const update_restaurant_features = (newData) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_restaurant_features = await axios.patch(
      `${serverApi}/details/update-restoFeatures`,
      newData
    );
    console.log(update_restaurant_features.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE CUISINE ONE DETAILS
 *****************************************/
export const update_cuisine_one = (formData) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_cuisine_one = await axios.patch(
      `${serverApi}/details/update-cusineOne`,
      formData
    );
    console.log(update_cuisine_one.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - UPDATE CUISINE TWO DETAILS
 *****************************************/
export const update_cuisine_two = (newData) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let update_cuisine_two = await axios.patch(
      `${serverApi}/details/update-cusineTwo`,
      newData
    );
    console.log(update_cuisine_two.data);
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - ALL SUGGEST NEW BUTTON ACTION
 *****************************************/
export const create_new_suggest_category = (formData) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_SuggestButton = await axios.post(
      `${serverApi}/details/suggest-new`,
      formData
    );
    if (new_SuggestButton.status === 200) {
      dispatch({ type: CLEAR_LOADER });
      Alert.success("<h4>Your Suggestion Add Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    Alert.error("<h4>Your Suggestion Failed To Add</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 5000,
      offset: 30,
    });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - ALL SUGGEST NEW BUTTON ACTION
 *****************************************/
export const create_new_addmore_category = (formData) => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_suggestbutton = await axios.post(
      `${serverApi}/details/suggest-new`,
      formData
    );
    if (new_suggestbutton.data) {
      console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      window.alert("Add More Submited Successfully");
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - RESTAURANT DETAILS
 *****************************************/
export const create_new_restraunt_type = (formData, callback) => async (
  dispatch
) => {
  dispatch({ type: SET_LOADER });
  try {
    let new_restaurant = await axios.post(
      `${serverApi}/details/add-restoType`,
      formData
    );
    if (new_restaurant.data) {
      console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      callback();
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
//done
