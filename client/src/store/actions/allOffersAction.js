import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_OFFERS_DETAILS,
  EDIT_OFFERS_DETAILS,
} from "../types";
import { setErrors } from "./errorActions";
import { serverApi } from "../../config/Keys";
import Alert from "react-s-alert";

/***************************************
 * @DESC - ADD OFFERS ACTION
 *****************************************/
export const add_newOffers = (formData) => async (dispatch) => {
  //console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    // const headers = {
    //   "Content-Type": "application/json"
    // };
    let new_category = await axios.patch(
      `${serverApi}/details/add-offer`,
      formData
      // , {
      //   headers: headers
      // }
    );
    if (new_category.data) {
      //console.log(new_category.data);
      dispatch({ type: CLEAR_LOADER });
      Alert.success("<h4>New Offer Add Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      dispatch(get_offers_details());
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET OFFERS ACTION
 *****************************************/
export const get_offers_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_offersDetails = await axios.get(`${serverApi}/details/offer-list`);
    if (get_offersDetails.data) {
      //   console.log(get_offersDetails.data);
      dispatch({
        type: GET_OFFERS_DETAILS,
        payload: get_offersDetails.data,
      });
      // dispatch(get_offers_details());
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - DELETE ACTION
 *****************************************/
export const delete_newOffers = (deleteData) => async (dispatch) => {
  console.log(deleteData);
  try {
    let delete_data = await axios.post(
      `${serverApi}/details/delete-offer`,
      deleteData
    );
    if (delete_data.data) {
      //window.alert("Offer Deleted Successfully");
      Alert.success("<h4>Offer Deleted Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      dispatch(get_offers_details());
    }
  } catch (err) {
    console.log(err);
  }
};

export const edit_offers_details = (editOffers, history) => (dispatch) => {
  console.log(editOffers);
  axios
    .patch(`${serverApi}/details/update-offer`, editOffers)
    // .patch("/all_vendor/:vendorId", editUser)
    .then((res) => {
      Alert.success("<h4>Offer Update Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      dispatch(get_offers_details());
    })
    .catch((err) =>
      dispatch({
        type: EDIT_OFFERS_DETAILS,
        payload: err.response.data,
      })
    );
};

/****************************************************
 * @DESC TOGGLE BUTTON CHANGE PATCH API
 ****************************************************/
export const offers_update_availability_data = (formData) => async (
  dispatch
) => {
  console.log(formData, "asdas");
  let update_availability = await axios.patch(
    `${serverApi}/details/update_offer_status`,
    formData
  );
  if (update_availability.data) {
    //window.alert("Data Update Successfully");
    Alert.success("<h4>Offer Status Update Successfully</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 5000,
      offset: 30,
    });
    dispatch(get_offers_details());
  }
};
