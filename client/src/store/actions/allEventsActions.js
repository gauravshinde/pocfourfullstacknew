import axios from "axios";
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_EVENTS_DETAILS,
  EDIT_EVENTS_DETAILS,
} from "../types";
import { setErrors } from "./errorActions";
import { serverApi } from "../../config/Keys";
import Alert from "react-s-alert";

/***************************************
 * @DESC - ADD OFFERS ACTION
 *****************************************/
export const add_newEvent = (formData) => async (dispatch) => {
  console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_event = await axios.post(`${serverApi}/event/add-event`, formData);
    if (new_event.data) {
      console.log(new_event.data);
      dispatch({ type: CLEAR_LOADER });
      //window.alert("New Offer Add Successfully");
      Alert.success("<h4>New Event Add Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      dispatch(get_events_details());
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET EVENTS ACTION
 *****************************************/
export const get_events_details = () => async (dispatch) => {
  dispatch({ type: SET_LOADER });
  try {
    let get_eventsDetails = await axios.get(`${serverApi}/event/get-event`);
    if (get_eventsDetails.data) {
      // console.log(get_eventsDetails.data);
      dispatch({
        type: GET_EVENTS_DETAILS,
        payload: get_eventsDetails.data,
      });
      // dispatch(get_events_details());
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - DELETE ACTION
 *****************************************/
export const delete_newEvents = (deleteData) => async (dispatch) => {
  console.log(deleteData);
  try {
    let delete_data = await axios.post(
      `${serverApi}/event/delete-event`,
      deleteData
    );
    if (delete_data.data) {
      Alert.success("<h4>Event Deleted Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      dispatch(get_events_details());
    }
  } catch (err) {
    console.log(err);
  }
};

export const edit_events_details = (editEvents, history) => (dispatch) => {
  console.log(editEvents);
  axios
    .patch(`${serverApi}/event/update-event`, editEvents)
    // .patch("/all_vendor/:vendorId", editUser)
    .then((res) => {
      Alert.success("<h4>Event Update Successfully</h4>", {
        position: "top-right",
        effect: "bouncyflip",
        beep: true,
        html: true,
        timeout: 5000,
        offset: 30,
      });
      dispatch(get_events_details);
    })
    .catch((err) =>
      dispatch({
        type: EDIT_EVENTS_DETAILS,
        payload: err.response.data,
      })
    );
};

/****************************************************
 * @DESC TOGGLE BUTTON CHANGE PATCH API
 ****************************************************/
export const event_update_availability_data = (formData) => async (
  dispatch
) => {
  console.log(formData, "asdas");
  let update_availability = await axios.patch(
    `${serverApi}/event/update-availability`,
    formData
  );
  if (update_availability.data) {
    Alert.success("<h4>Event Status Update Successfully</h4>", {
      position: "top-right",
      effect: "bouncyflip",
      beep: true,
      html: true,
      timeout: 5000,
      offset: 30,
    });
    dispatch(get_events_details());
  }
};
//10-01-2020
