// Store Js File
import { applyMiddleware, compose, createStore } from "redux";
import rootReducer from "./reducers";
import thunk from "redux-thunk";
const middleware = [thunk];

const intialState = {};

let composeEnhancers = compose(applyMiddleware(...middleware));

if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
  composeEnhancers = compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
}

const store = createStore(rootReducer, intialState, composeEnhancers);

export default store;
//07-01-2020

// import { applyMiddleware, compose, createStore } from 'redux';
// import rootReducer from './reducers';
// import thunk from 'redux-thunk';
// const middleware = [thunk];

// const intialState = {};

// const store = createStore(
//   rootReducer,
//   intialState,
//   compose(
//     applyMiddleware(...middleware)
//     // ,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
//   )
// );

// export default store;
// //new commit 18-10-19 19:18pm
