import React from 'react';

const TableHeadComponent = ({
  code,
  category,
  image,
  name,
  price,
  time,
  availability,
  edit
}) => {
  return (
    <React.Fragment>
      <tr className='table-headclass'>
        <th>{code}</th>
        <th>{category}</th>
        <th>{image}</th>
        <th>{name}</th>
        <th>{price}</th>
        <th>{time}</th>
        <th>{availability}</th>
        <th>{edit}</th>
      </tr>
    </React.Fragment>
  );
};

export default TableHeadComponent;
