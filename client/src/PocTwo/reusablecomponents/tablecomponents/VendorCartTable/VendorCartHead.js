import React from 'react';

const VendorCartHead = ({
  sno,
  code,
  category,
  name,
  qty,
  price,
  totalitemprice
}) => {
  return (
    <React.Fragment>
      <tr className='table-headclass'>
        <th>{code}</th>
        <th>{category}</th>
        <th>{name}</th>
        <th>{price}</th>
        <th className='text-center'>{qty}</th>
        <th className='text-center'>{totalitemprice}</th>
      </tr>
    </React.Fragment>
  );
};

export default VendorCartHead;
