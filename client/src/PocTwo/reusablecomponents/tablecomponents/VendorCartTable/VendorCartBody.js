import React from 'react';

const VendorCartBody = ({
  sno,
  code,
  category,
  name,
  qty,
  price,
  decrementclick,
  incrementclick,
  totalitemprice
}) => {
  return (
    <React.Fragment>
      <tr className='table-rowclass'>
        <td>{code}</td>
        <td>{category}</td>
        <td>{name}</td>
        <td>Rs. {price}</td>
        <td>
          <div className='d-flex justify-content-center'>
            <div>
              <span className='btn btn-black mx-1' onClick={decrementclick}>
                <img
                  src={require('../../../assets/Images/minus.svg')}
                  className='img-fluid'
                  alt='minus icon'
                />
              </span>
              <span className='btn btn-black mx-1'>{qty}</span>
              <span className='btn btn-black mx-1' onClick={incrementclick}>
                <img
                  src={require('../../../assets/Images/plus.svg')}
                  className='img-fluid'
                  alt='plus icon'
                />
              </span>
            </div>
          </div>
        </td>
        <td className='text-center'>Rs. {totalitemprice}</td>
      </tr>
    </React.Fragment>
  );
};

export default VendorCartBody;
