import React from 'react';

const VendorOpenOrderHead = () => {
  return (
    <React.Fragment>
      <tr className='table-header'>
        <th>Sr. No</th>
        <th>User Details</th>
        <th>Items</th>
        <th>Price</th>
        <th>Qty.</th>
        <th>Total amount</th>
        <th>Total Time (Buff+Proc)</th>
        <th>Order type</th>
        <th>Paid/Unpaid</th>
        <th>Status</th>
      </tr>
    </React.Fragment>
  );
};

export default VendorOpenOrderHead;
