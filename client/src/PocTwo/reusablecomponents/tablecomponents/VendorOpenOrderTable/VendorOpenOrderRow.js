import React from "react";
import isEmpty from "./../../../store/validation/is-Empty";

const VendorOpenOrderRow = ({
  userId,
  srno,
  username,
  address,
  phone,
  imgmsg,
  items,
  ordernotes,
  price,
  qty,
  totalamount,
  totaltime,
  editimg,
  totaltimer,
  ordertypeimg,
  paidunpaid,
  status
}) => {
  return (
    <React.Fragment>
      <tr className="table-main-headecss">
        <th scope="row" className="scope-css">
          {srno}
        </th>
        <td>
          <h3 className="mb-3">{username}</h3>
          <p>{address}</p>
          <p>{phone}</p>
          <img src={imgmsg} alt="msg Icon" className="img-fluid message-icon" />
        </td>
        <td>
          <div className="col-12 p-0">
            {/* {!isEmpty(items) &&
              items.map((singleitem, index) => {
                return (
                  <React.Fragment key={index}>
                    <p>{singleitem} </p>
                  </React.Fragment>
                );
              })} */}
            Pancake
          </div>
          <hr />
          <h3>{ordernotes}</h3>
        </td>
        <td>
          {/* {!isEmpty(price) &&
            price.map((singlePrice, index) => {
              return (
                <React.Fragment key={index}>
                  <p>Rs. {singlePrice} </p>
                </React.Fragment>
              );
            })} */}
          Rs. 200
        </td>
        <td>
          {/* {!isEmpty(qty) &&
            qty.map((singleqty, index) => {
              return (
                <React.Fragment key={index}>
                  <p>{singleqty} </p>
                </React.Fragment>
              );
            })} */}
          1
        </td>
        <td>
          <p>Rs. {totalamount}</p>
        </td>
        <td>
          <div className="d-flex">
            <p>Rs. {totaltime}</p>
            <img
              src={editimg}
              alt="edit Icon"
              className="img-fluid edit-icon ml-3"
            />
          </div>
          <h3>Timer: {totaltimer}</h3>
        </td>
        <td className="text-center">
          <img
            src={ordertypeimg}
            alt="order Icon"
            className="img-fluid hand-icon"
          />
        </td>
        <td>
          <p>{paidunpaid}</p>
        </td>
        <td>
          <div className="form-group">
            <select
              className="form-control border-0 bg-transparent"
              name={status}
            >
              <option value="Processing">Processing</option>
              <option value="Completed">Completed</option>
              <option value="Cancelled">Cancelled</option>
            </select>
          </div>
        </td>
      </tr>
    </React.Fragment>
  );
};

export default VendorOpenOrderRow;
//10-01-2020
