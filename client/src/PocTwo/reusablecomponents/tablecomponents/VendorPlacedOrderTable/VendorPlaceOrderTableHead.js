import React from 'react';

const VendorPlaceOrderTableHead = ({
  code,
  category,
  image,
  name,
  price,
  time,
  availability,
  cart,
  paid,
  status
}) => {
  return (
    <React.Fragment>
      <tr className='table-headclass'>
        <th>{code}</th>
        <th>{category}</th>
        <th>{image}</th>
        <th>{name}</th>
        <th>{price}</th>
        <th>{time}</th>
        <th>{availability}</th>
        <th>{cart}</th>
        <th>{paid}</th>
        <th>{status}</th>
      </tr>
    </React.Fragment>
  );
};

export default VendorPlaceOrderTableHead;
