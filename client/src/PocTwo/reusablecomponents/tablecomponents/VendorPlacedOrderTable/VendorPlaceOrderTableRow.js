import React from 'react';

const VendorPlaceOrderTableRow = ({
  code,
  category,
  name,
  price,
  time,
  img,
  alt,
  img1,
  alt1,
  imgclass,
  togglediv,
  editClick,
  addtocartclick
}) => {
  return (
    <React.Fragment>
      <tr className='table-rowclass'>
        <td>{code}</td>
        <td>{category}</td>
        <td>
          <img src={img1} alt={alt1} className={imgclass} />
        </td>
        <td>{name}</td>
        <td>{price}</td>
        <td>{time}</td>
        <td>{togglediv}</td>
        <td className='text-center'>
          <button
            type='button'
            className='btn btn-success btn-circle'
            onClick={addtocartclick}
          >
            <i className='fa fa-plus' />
          </button>
        </td>
        <td></td>
        <td>
          <img src={img} alt={alt} onClick={editClick} />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default VendorPlaceOrderTableRow;
