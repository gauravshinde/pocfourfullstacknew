import React from 'react';

const TableRowComponent = ({
  code,
  category,
  name,
  price,
  time,
  img,
  alt,
  img1,
  alt1,
  imgclass,
  togglediv,
  onClickeditimg
}) => {
  return (
    <React.Fragment>
      <tr className='table-rowclass'>
        <td>{code}</td>
        <td>{category}</td>
        <td>
          <img src={img1} alt={alt1} className={imgclass} />
        </td>
        <td>{name}</td>
        <td>{price}</td>
        <td>{time}</td>
        <td>
          <div className={togglediv}>
            <label>Yes</label>
            <label className='switch'>
              <input type='checkbox' />
              <div className='slider round' />
            </label>
            <label>No</label>
          </div>
        </td>
        {/* <td>
          <button type='button' className='btn btn-success btn-circle'>
            <i className='fa fa-plus' />
          </button>
        </td> */}
        <td>
          <img src={img} alt={alt} onClick={onClickeditimg} />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default TableRowComponent;
