import React from 'react';
import isEmpty from './../../store/validation/is-Empty';

const InputComponent = ({
  labeltext,
  inputlabelclass,
  imgsrc,
  imgsrcone,
  type,
  name,
  inputClass,
  place,
  onChange,
  value,
  error,
  disabled
}) => {
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <div className='poctwo-input-container'>
        {imgsrcone === true ? <img src={imgsrc} alt='input icon' /> : false}
        <input
          className={inputClass}
          type={type}
          placeholder={place}
          name={name}
          onChange={onChange}
          value={value}
        />
      </div>
      {error ? <div className='error'>{error}</div> : null}
    </React.Fragment>
  );
};

export default InputComponent;

//{
/****************************
 * @DESC Select Category Box
 *****************************/
//}

export const SelectCategory = ({
  inputlabelclass,
  labeltext,
  imgsrc,
  name,
  inputClass,
  onChange,
  error,
  allcategoryList,
  selectedname
}) => {
  // console.log(allcategoryList);
  return (
    <React.Fragment>
      <label className={inputlabelclass} htmlFor={name}>
        {labeltext}
      </label>
      <div className='input-container'>
        <img src={imgsrc} alt='input icon' />
        <select className={inputClass} name={name} onChange={onChange}>
          <option>{selectedname}</option>
          {!isEmpty(allcategoryList) &&
            allcategoryList.map((categorydata, index) => {
              return (
                <option key={index} value={categorydata.category_name}>
                  {categorydata.category_name}
                </option>
              );
            })}
        </select>
      </div>
      {error ? <div className='error'>{error}</div> : null}
    </React.Fragment>
  );
};

//{
/****************************
 * @DESC Text Box Description
 *****************************/
//}
export const TextboxComponent = ({
  inputlabelclass,
  textareaLabel,
  name,
  type,
  place,
  onChange,
  value,
  textareadescription,
  error
}) => {
  return (
    <React.Fragment>
      <div className='form-group'>
        <label htmlFor={name} className={inputlabelclass}>
          {textareaLabel}
        </label>
        <textarea
          type={type}
          className={textareadescription}
          name={name}
          rows='4'
          value={value}
          onChange={onChange}
          placeholder={place}
        ></textarea>
        {error ? <div className='error'>{error}</div> : null}
      </div>
    </React.Fragment>
  );
};
