import React from 'react';
import { NavLink } from 'react-router-dom';
import LargeTextComponent from '../textcomponents/LargeTextComponent';
import { Dropdown, DropdownButton } from 'react-bootstrap';

const SubHeaderComponent = ({
  DisplayDropDown,
  text,
  onChange,
  value,
  name,
  type
}) => {
  return (
    <React.Fragment>
      <div className='container-fluid sub-header'>
        <LargeTextComponent largeclass='large-text' largetext={text} />
        <div className='sub-header-right-side'>
          <div className='search-div'>
            <img
              src={require('../../assets/Images/search.svg')}
              alt='search'
              className='search-img'
            />
            <input
              name={name}
              type={type}
              onChange={onChange}
              value={value}
              placeholder='search'
              className='searchclass'
            />
          </div>
          {DisplayDropDown ? DropDown() : ''}
          <img
            src={require('../../assets/Images/sort-button-with-three-lines.svg')}
            alt='sort'
            className='sort-img'
          />
          <img
            src={require('../../assets/Images/filter.svg')}
            alt='filter'
            className='filter-img'
          />
        </div>
      </div>
    </React.Fragment>
  );
};

export default SubHeaderComponent;

export const DropDown = () => {
  return (
    <>
      <div id='menu-dropdown'>
        <DropdownButton id='add-dropdown' title='Add'>
          <Dropdown.Item>
            <NavLink to='/vendor-addmenu'>Add Item</NavLink>
          </Dropdown.Item>
          <Dropdown.Item>
            <NavLink to='/vendor-addcategory'>Add Category</NavLink>
          </Dropdown.Item>
        </DropdownButton>
      </div>
    </>
  );
};

//comment test 19-03-2020 08:45Am
