import React from 'react';
import { NavLink } from 'react-router-dom';
import LargeTextComponent from './../textcomponents/LargeTextComponent';

const HeaderComponent = ({ LoginPage, onClick, notification }) => {
  return (
    <React.Fragment>
      <div className='container-fluid main-header'>
        <NavLink to='/'>
          <LargeTextComponent largeclass='size-28px' largetext='Logo' />
        </NavLink>
        {LoginPage ? (
          ''
        ) : (
          <div className='menubar'>
            <NavLink
              to='/vendor-Menu'
              className='menu'
              activeClassName='selected'
            >
              Menu
            </NavLink>
            <NavLink
              to='/vendor-openorder'
              className='menu'
              activeClassName='selected'
            >
              Open Orders
            </NavLink>
            <NavLink
              to='/vendor-placeorder'
              className='menu'
              activeClassName='selected'
            >
              Place Order
            </NavLink>
            <NavLink to='/cart'>
              <img
                src={require('../../assets/Images/shopping-cart.svg')}
                alt='shopping-cart'
                className='cart-img'
              />
              <span
                className='badge badge-light'
                style={{
                  position: 'absolute',
                  right: '11%',
                  backgroundColor: 'transparent'
                }}
              >
                {notification}
              </span>
            </NavLink>
            <img
              src={require('../../assets/Images/logout.svg')}
              alt='logout'
              className='logout-img'
              onClick={onClick}
            />
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

export default HeaderComponent;

// export const HeaderMenuBar = ({onClick}) => {
//   return (
//     <>
//       <div className='menubar'>
//         <NavLink to='/vendor-Menu' className='menu' activeClassName='selected'>
//           Menu
//         </NavLink>
//         <NavLink
//           to='/vendor-openorder'
//           className='menu'
//           activeClassName='selected'
//         >
//           Open Orders
//         </NavLink>
//         <NavLink
//           to='/vendor-placeorder'
//           className='menu'
//           activeClassName='selected'
//         >
//           Place Order
//         </NavLink>
//         <img
//           src={require('../../assets/Images/shopping-cart.svg')}
//           alt='shopping-cart'
//           className='cart-img'
//         />
//         <img
//           src={require('../../assets/Images/Logout.svg')}
//           alt='logout'
//           className='logout-img'
//           onClick={onClick}
//         />
//       </div>
//     </>
//   );
// };
