import React from 'react';

const LargeTextComponent = ({ largeclass, largetext }) => {
  return (
    <React.Fragment>
      <h1 className={largeclass}>{largetext}</h1>
    </React.Fragment>
  );
};

export default LargeTextComponent;
