import { combineReducers } from 'redux';
import errorReducer from './errorReducers';
import authReducers from './authReducers';
import getallReducers from './allGetReducers';
import editallReducers from './allEditReducers';

export default combineReducers({
  errors: errorReducer,
  auth: authReducers,
  getall: getallReducers,
  alledit: editallReducers
});
