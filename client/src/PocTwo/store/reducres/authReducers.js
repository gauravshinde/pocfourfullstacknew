import {
  SET_LOADER,
  CLEAR_LOADER,
  SET_CURRENT_USER,
  SET_STATUS
} from "../types";
import { LOG_OUT } from "../../../store/types";
import isEmpty from "../validation/is-Empty";

const initialState = {
  loader: false,
  user: {},
  isAuthenticated: false,
  status: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_LOADER:
      return {
        ...state,
        loader: true
      };
    case CLEAR_LOADER:
      return {
        ...state,
        loader: false
      };
    case SET_CURRENT_USER: {
      return {
        ...state,
        user: action.payload,
        isAuthenticated: !isEmpty(action.payload)
      };
    }
    case SET_STATUS: {
      console.log(action.payload);
      return {
        ...state,
        status: action.payload
      };
    }
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
