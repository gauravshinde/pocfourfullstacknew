import { EDIT_MENU_ITEM } from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  menuedit: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case EDIT_MENU_ITEM:
      return {
        ...state,
        menuedit: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
