import {
  GET_OPEN_ORDER,
  GET_ALL_CATEGORY,
  GET_PLACE_ORDER,
  GET_ALL_MENU_ORDER,
  GET_ALL_CART_DETAILS
} from "../types";
import { LOG_OUT } from "../../../store/types";

const initialState = {
  get_allcategory: {},
  menuorder: {},
  openorder: {},
  placeorder: {},
  getallcart: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_CATEGORY:
      return {
        ...state,
        get_allcategory: action.payload
      };
    case GET_ALL_MENU_ORDER:
      return {
        ...state,
        menuorder: action.payload
      };
    case GET_OPEN_ORDER:
      return {
        ...state,
        openorder: action.payload
      };
    case GET_PLACE_ORDER:
      return {
        ...state,
        placeorder: action.payload
      };
    case GET_ALL_CART_DETAILS:
      console.log(action.payload);
      return {
        ...state,
        getallcart: action.payload
      };
    case LOG_OUT: {
      return {
        state: undefined
      };
    }
    default:
      return state;
  }
}
