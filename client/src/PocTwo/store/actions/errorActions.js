import { SET_ERRORS } from '../types';

export const setErrors = err => async dispatch => {
  if (err.response) {
    if (err.response.status === 500 || err.response.status === 500) {
      console.log('Server Error', err);
    } else {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    }
  } else {
    dispatch({
      type: SET_ERRORS,
      payload: err
    });
  }
};

export const clear_error = () => async dispatch => {
  dispatch({ type: SET_ERRORS, payload: {} });
};
