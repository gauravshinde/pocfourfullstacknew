import axios from 'axios';
import { SET_LOADER, CLEAR_LOADER, SET_CURRENT_USER } from '../types';
import { setErrors } from './errorActions';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

export const user_login = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let user_login = await axios.post('/vendor/login', formData);
    if (user_login.data.success) {
      dispatch({ type: CLEAR_LOADER });
      localStorage.clear();
      const { token } = user_login.data;
      // SET Data to Local Storage
      localStorage.setItem('jwtToken', token);
      // Set token To Auth Headers
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      dispatch(set_current_user(decoded));
    }
  } catch (err) {
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/*****************************
 * @DESC - SET CURRRENT USER
 ****************************/
export const set_current_user = decoded => async dispatch => {
  dispatch({
    type: SET_CURRENT_USER,
    payload: decoded
  });
};

/******************************
 * @DESC - LOGOUT USER
 ****************************/
export const logOutUser = () => async dispatch => {
  // REMOVE THE TOKEN
  await localStorage.clear();
  await setAuthToken(false);
  dispatch(set_current_user({}));
  window.location.href = '/';
};
