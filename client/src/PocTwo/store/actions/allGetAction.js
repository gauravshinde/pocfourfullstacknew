import axios from 'axios';
import {
  SET_LOADER,
  CLEAR_LOADER,
  GET_ALL_CATEGORY,
  GET_ALL_MENU_ORDER,
  GET_OPEN_ORDER,
  GET_PLACE_ORDER,
  GET_ALL_CART_DETAILS
} from './../types';
import { setErrors } from './errorActions';

/***************************************
 * @DESC - GET CATEGORY DETAILS
 *****************************************/
export const get_all_category = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_allcategory = await axios.get('/add_category/get_category');
    if (get_allcategory.data) {
      // console.log(get_allcategory.data);
      dispatch({
        type: GET_ALL_CATEGORY,
        payload: get_allcategory.data
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET MENU ORDER DETAILS
 *****************************************/
export const get_menu_order = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_menuorder = await axios.get('/add_item/item_list');
    if (get_menuorder.data) {
      //console.log(get_menuorder.data);
      dispatch({
        type: GET_ALL_MENU_ORDER,
        payload: get_menuorder.data
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET OPEN ORDER DETAILS
 *****************************************/

export const get_open_order = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_openorder = await axios.get('/add_cart/get-order');
    if (get_openorder.data) {
      console.log(get_openorder.data);
      dispatch({
        type: GET_OPEN_ORDER,
        payload: get_openorder.data
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET OPEN PLACE ORDER DETAILS
 *****************************************/

export const get_place_order = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_placeorder = await axios.get('/add_item/item_list');
    if (get_placeorder.data) {
      // console.log(get_placeorder.data);
      dispatch({
        type: GET_PLACE_ORDER,
        payload: get_placeorder.data
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - GET ALL CART DETAILS
 *****************************************/

export const get_all_cart_details = () => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let get_allcart = await axios.get('/add_cart/processing_order');
    if (get_allcart.data) {
      // console.log(get_allcart.data);
      dispatch({
        type: GET_ALL_CART_DETAILS,
        payload: get_allcart.data
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
