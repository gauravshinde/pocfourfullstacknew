import axios from 'axios';
import { EDIT_MENU_ITEM } from '../types';

export const edit_menu_item = (formData, history) => dispatch => {
  console.log(formData);
  axios
    .patch(`/add_item/update_item`, formData)
    // .patch("/all_vendor/:vendorId", editUser)
    .then(res => history.push('/vendor-Menu'))
    .catch(err =>
      dispatch({
        type: EDIT_MENU_ITEM,
        payload: err.response.data
      })
    );
};
