import axios from "axios";
import { SET_LOADER, CLEAR_LOADER } from "./../types";
import { setErrors } from "./errorActions";

/***************************************
 * @DESC - ADD MENU CATEGORY
 *****************************************/

export const add_category_item = formData => async dispatch => {
  console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_category = await axios.post("/add_category/add_category", formData);
    if (new_category.data) {
      console.log("adasd");
      dispatch({ type: CLEAR_LOADER });
      window.alert("Category Added Successfully");
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - ADD MENU ITEM
 *****************************************/

export const add_menu_item = formData => async dispatch => {
  // console.log(formData);
  dispatch({ type: SET_LOADER });
  try {
    let new_category = await axios.post("/add_item/add_item", formData);
    if (new_category.data) {
      // console.log(new_category);
      dispatch({ type: CLEAR_LOADER });
      window.alert("Menu Added Successfully");
    }
  } catch (err) {
    // console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - ADD TO CART ACTION
 *****************************************/
export const add_to_cart = formData => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let addtocart = await axios.post("/add_cart/add-to-cart", formData);
    if (addtocart.data) {
      dispatch({ type: CLEAR_LOADER });
      window.alert("Item Add Successfully");
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - CHECKOUT CART ACTION
 *****************************************/
export const checkout_order = (formData, history) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let checkout_order = await axios.post("/add_cart/confirm_order", formData);
    if (checkout_order.data) {
      dispatch({ type: CLEAR_LOADER });
      history.push("/checkout");
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};

/***************************************
 * @DESC - CONFIRM ORDER ACTION
 *****************************************/
export const confirm_order = (formData, history) => async dispatch => {
  dispatch({ type: SET_LOADER });
  try {
    let confirm_order = await axios.post("/add_cart/add-checkout", formData);
    if (confirm_order.data) {
      dispatch({ type: CLEAR_LOADER });
      history.push("vendor-openorder");
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: CLEAR_LOADER });
    dispatch(setErrors(err));
  }
};
