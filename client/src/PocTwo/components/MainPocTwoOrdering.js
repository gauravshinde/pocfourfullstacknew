import React, { Component } from "react";
import NavbarDashboard from "../../components/Dassboard/NavbarDashboard";
import VendorOpenOrder from "../components/vendor/VendorDashBoard/VendorOpenOrder";
import VendorPlaceOrder from "../components/vendor/VendorDashBoard/VendorPlaceOrder";
import VendorCart from "../components/vendor/VendorInsidePage/VendorCart";

export class MainPocTwoOrdering extends Component {
  constructor() {
    super();
    this.state = {
      selectedOrderingMenu: 1
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectedOrderingMenu: value
    });
  };

  render() {
    return (
      <React.Fragment>
        <NavbarDashboard />
        <div className="inside-main-menu d-flex justify-content-center new-poc-one-shadow">
          <nav className="navbar">
            <p
              onClick={this.onPageChange(1)}
              className={
                this.state.selectedOrderingMenu === 1
                  ? "navbar__link_active"
                  : "navbar__link"
              }
            >
              Open Orders
            </p>
            <p
              onClick={this.onPageChange(2)}
              className={
                this.state.selectedOrderingMenu === 2
                  ? "navbar__link_active"
                  : "navbar__link"
              }
            >
              Place Order
            </p>
            <p
              onClick={this.onPageChange(3)}
              className={
                this.state.selectedOrderingMenu === 3
                  ? "navbar__link_active"
                  : "navbar__link"
              }
            >
              My Cart
            </p>
          </nav>
        </div>
        <div className="container-fluid">
          <div className="row">
            {this.state.selectedOrderingMenu === 1 ? <VendorOpenOrder /> : ""}
            {this.state.selectedOrderingMenu === 2 ? <VendorPlaceOrder /> : ""}
            {this.state.selectedOrderingMenu === 3 ? <VendorCart /> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MainPocTwoOrdering;
