import React, { Component } from 'react';
import LargeTextComponent from '../../reusablecomponents/textcomponents/LargeTextComponent';
import HeaderComponent from './../../reusablecomponents/headercomponents/HeaderComponent';
import InputComponent from '../../reusablecomponents/inputcomponents/InputComponent';
import classnames from 'classnames';
import ButtonComponent from './../../reusablecomponents/buttoncomponents/ButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { user_login } from './../../store/actions/authActions';

export class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      errors: {}
    };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.auth.isAuthenticated) {
      nextProps.history.push('/vendor-menu');
    }

    if (nextProps.errors !== nextState.errors) {
      return { errors: nextProps.errors };
    }
    return null;
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   *  @DESC - ON DATA SUBMIT
   ***************************/
  onSubmit = e => {
    e.preventDefault();
    let formData = {
      username: this.state.username,
      password: this.state.password
    };
    this.props.user_login(formData);
  };

  renderLoginForm = () => {
    const { errors } = this.state;
    // console.log(this.state);
    return (
      <React.Fragment>
        <InputComponent
          inputlabelclass='input-lableclass mt-0'
          labeltext='username'
          imgsrcone={true}
          imgsrc={require('../../assets/Images/user.png')}
          type='text'
          name='username'
          place='eg. James Bond'
          value={this.state.username}
          onChange={this.onChange}
          error={errors.username}
          inputClass={classnames('input-field', { invalid: errors.username })}
        />
        <InputComponent
          inputlabelclass='input-lableclass'
          labeltext='password'
          imgsrcone={true}
          imgsrc={require('../../assets/Images/passwordpic.svg')}
          type='password'
          name='password'
          place='******'
          value={this.state.password}
          onChange={this.onChange}
          error={errors.password}
          inputClass={classnames('input-field', { invalid: errors.password })}
        />
        <div className='float-right mt-3 mb-4'>
          <ButtonComponent
            type='submit'
            buttontext='login'
            buttonclass='btn button-width button-brown'
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <HeaderComponent LoginPage={true} />
        <div className='container-fluid main-margin-padding login-main'>
          <div className='row'>
            <div className='login-section-one'>
              <LargeTextComponent
                largeclass='large-text text-center'
                largetext='Welcome!'
              />
              <p>
                It's nice to see you again!
                <br />
                Login to continue to your account
              </p>
              <hr />
              <LargeTextComponent largeclass='large-text' largetext='Login' />
              <form noValidate onSubmit={this.onSubmit}>
                {this.renderLoginForm()}
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors.errors
});

export default connect(
  mapStateToProps,
  { user_login }
)(withRouter(Login));
