import React, { Component } from "react";
// import HeaderComponent from '../../../reusablecomponents/headercomponents/HeaderComponent';
import SubHeaderComponent from "../../../reusablecomponents/headercomponents/SubHeaderComponent";
// import TableHeadComponent from '../../../reusablecomponents/tablecomponents/TableHeadComponent';
// import TableRowComponent from '../../../reusablecomponents/tablecomponents/TableRowComponent';
import ButtonComponent from "./../../../reusablecomponents/buttoncomponents/ButtonComponent";
import VendorOpenOrderHead from "../../../reusablecomponents/tablecomponents/VendorOpenOrderTable/VendorOpenOrderHead";
import VendorOpenOrderRow from "../../../reusablecomponents/tablecomponents/VendorOpenOrderTable/VendorOpenOrderRow";

import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logOutUser } from "./../../../store/actions/authActions";
import { get_open_order } from "./../../../store/actions/allGetAction";
import isEmpty from "./../../../store/validation/is-Empty";

// const SeatStatus = ['Processing', 'Completed', 'Cancelled'];

export class VendorOpenOrder extends Component {
  constructor() {
    super();
    this.state = {
      vendorOrderList: {},
      search: ""
    };
  }

  componentDidMount() {
    if (this.props.get_open_order) {
      this.props.get_open_order();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    // console.log(nextprops.getall);
    if (nextprops.getall !== nextstate.vendorOrderList) {
      return { vendorOrderList: nextprops.getall };
    }
    return null;
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logOutUser(this.props.history);
  };

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  renderOperationBufferTime = () => {
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-4">
            <h4>Vendor Operations</h4>
            <div className="d-flex justify-content-between margin-vendor-operation">
              <ButtonComponent
                buttonclass="btn button-width button-blue"
                buttontext="Show Processing"
              />
              <ButtonComponent
                buttonclass="btn button-width button-border-blue"
                buttontext="Show all"
              />
            </div>
          </div>
          <div className="col-4">
            <h4>Buffer Time</h4>
            <div className="btn-group" role="group" aria-label="Basic example">
              <button
                type="button"
                className="btn button-group-new button-selected"
              >
                05
              </button>
              <span className="button-middle-line"></span>
              <button type="button" className="btn button-group-new">
                10
              </button>
              <span className="button-middle-line"></span>
              <button type="button" className="btn button-group-new">
                20
              </button>
              <span className="button-middle-line"></span>
              <button type="button" className="btn button-group-new">
                30
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };

  rendorVenderOpenorderTableRow = () => {
    return (
      <React.Fragment>
        <VendorOpenOrderRow
          srno="1"
          username={"name"}
          address={"data.room"}
          phone={"data.phone_number"}
          imgmsg={require("../../../assets/Images/envelope-img.svg")}
          items={"data.item_name"}
          ordernotes={"data.notes"}
          price={"data.item_price"}
          qty={"data.item_qty"}
          totalamount={"data.total_price_all_product"}
          totaltime="25"
          editimg={require("../../../assets/Images/penimg.svg")}
          totaltimer="10:00"
          ordertypeimg={require("../../../assets/Images/hand-img.svg")}
          paidunpaid={"data.payment_status"}
          status={"data.status"}
        />
        <VendorOpenOrderRow
          srno="2"
          username={"name"}
          address={"data.room"}
          phone={"data.phone_number"}
          imgmsg={require("../../../assets/Images/envelope-img.svg")}
          items={"data.item_name"}
          ordernotes={"data.notes"}
          price={"data.item_price"}
          qty={"data.item_qty"}
          totalamount={"data.total_price_all_product"}
          totaltime="25"
          editimg={require("../../../assets/Images/penimg.svg")}
          totaltimer="10:00"
          ordertypeimg={require("../../../assets/Images/hand-img.svg")}
          paidunpaid={"data.payment_status"}
          status={"data.status"}
        />
        <VendorOpenOrderRow
          srno="3"
          username={"name"}
          address={"data.room"}
          phone={"data.phone_number"}
          imgmsg={require("../../../assets/Images/envelope-img.svg")}
          items={"data.item_name"}
          ordernotes={"data.notes"}
          price={"data.item_price"}
          qty={"data.item_qty"}
          totalamount={"data.total_price_all_product"}
          totaltime="25"
          editimg={require("../../../assets/Images/penimg.svg")}
          totaltimer="10:00"
          ordertypeimg={require("../../../assets/Images/hand-img.svg")}
          paidunpaid={"data.payment_status"}
          status={"data.status"}
        />
      </React.Fragment>
    );
    // const { vendorOrderList } = this.state;
    // let filtereddata = [];
    // if (this.state.search) {
    //   let search = new RegExp(this.state.search, "i");
    //   filtereddata = vendorOrderList.filter(getall => {
    //     if (search.test(getall.name)) {
    //       return getall;
    //     }
    //     if (search.test(getall.room)) {
    //       return getall;
    //     }
    //     if (search.test(getall.phone_number)) {
    //       return getall;
    //     }
    //     if (search.test(getall.payment_status)) {
    //       return getall;
    //     }
    //   });
    //   // console.log(filtereddata);
    // } else {
    //   filtereddata = vendorOrderList;
    // }
    // if (!isEmpty(filtereddata)) {
    //   return filtereddata.map((data, index) => (
    //     <React.Fragment key={index}>
    //       <VendorOpenOrderRow
    //         key={index}
    //         srno={++index}
    //         username={data.name}
    //         address={data.room}
    //         phone={data.phone_number}
    //         imgmsg={require("../../../assets/Images/envelope-img.svg")}
    //         items={data.item_name}
    //         ordernotes={data.notes}
    //         price={data.item_price}
    //         qty={data.item_qty}
    //         totalamount={data.total_price_all_product}
    //         totaltime="25"
    //         editimg={require("../../../assets/Images/penimg.svg")}
    //         totaltimer="10:00"
    //         ordertypeimg={require("../../../assets/Images/hand-img.svg")}
    //         paidunpaid={data.payment_status}
    //         status={data.status}
    //       />
    //     </React.Fragment>
    //   ));
    // } else {
    //   return (
    //     <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
    //       Loading...
    //     </h3>
    //   );
    // }
  };

  render() {
    return (
      <React.Fragment>
        {/* <HeaderComponent onClick={this.onLogoutClick} /> */}
        <div className="container-fluid main-margin-padding vendor-open-order-sectionone">
          <form>{this.renderOperationBufferTime()}</form>
        </div>
        <SubHeaderComponent
          text="All Orders"
          name="search"
          type="text"
          onChange={this.onSearchChange}
          value={this.state.search}
        />
        <div className="container-fluid main-margin-padding vendor-open-order-sectiontwo">
          <div className="row">
            <div className="table-responsive table-striped">
              <table className="table">
                <thead>
                  <VendorOpenOrderHead />
                </thead>
                <tbody>{this.rendorVenderOpenorderTableRow()}</tbody>
              </table>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorOpenOrder.propTypes = {
  logOutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  get_open_order: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  getall: state.getall.openorder
});

export default connect(mapStateToProps, {
  logOutUser,
  get_open_order
})(withRouter(VendorOpenOrder));
