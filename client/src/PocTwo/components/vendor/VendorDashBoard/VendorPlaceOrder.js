import React, { Component } from "react";
// import HeaderComponent from '../../../reusablecomponents/headercomponents/HeaderComponent';
import SubHeaderComponent from "../../../reusablecomponents/headercomponents/SubHeaderComponent";
// import TableHeadComponent from '../../../reusablecomponents/tablecomponents/TableHeadComponent';
import VendorPlaceOrderTableHead from "../../../reusablecomponents/tablecomponents/VendorPlacedOrderTable/VendorPlaceOrderTableHead";
import VendorPlaceOrderTableRow from "../../../reusablecomponents/tablecomponents/VendorPlacedOrderTable/VendorPlaceOrderTableRow";

import { logOutUser } from "./../../../store/actions/authActions";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import isEmpty from "./../../../store/validation/is-Empty";
import { get_place_order } from "./../../../store/actions/allGetAction";
import { add_to_cart } from "./../../../store/actions/allPostAction";

export class VendorPlaceOrder extends Component {
  constructor() {
    super();
    this.state = {
      vendorPlaceOrderList: {},
      detailid: "",
      search: ""
    };
  }

  componentDidMount() {
    if (this.props.get_place_order) {
      this.props.get_place_order();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    // console.log(nextprops.getall);
    if (nextprops.getall !== nextstate.vendorPlaceOrderList) {
      return { vendorPlaceOrderList: nextprops.getall };
    }
    return null;
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logOutUser(this.props.history);
  };

  onAddToCartClickHandler = value => e => {
    e.preventDefault();
    const formData = {
      // state: { detailid: value },
      item_name: value.item_name,
      item_code: value.item_code,
      item_price: value.item_price,
      category: value.category,
      vendor_id: this.props.auth.user.id,
      item_qty: "1"
    };
    console.log(formData);
    this.props.add_to_cart(formData);
  };

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  renderTableRowPlaceOrder = () => {
    const { vendorPlaceOrderList } = this.state;
    console.log(vendorPlaceOrderList);
    let filtereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      filtereddata = vendorPlaceOrderList.filter(getall => {
        if (search.test(getall.item_name)) {
          return getall;
        }
        if (search.test(getall.category)) {
          return getall;
        }
        if (search.test(getall.item_code)) {
          return getall;
        }
      });
      console.log(filtereddata);
    } else {
      filtereddata = vendorPlaceOrderList;
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.map((data, index) => (
        <React.Fragment key={index}>
          <VendorPlaceOrderTableRow
            key={index}
            code={data.item_code}
            category={data.category}
            name={data.item_name}
            img1={data.item_image}
            imgclass={"item-img-class"}
            price={data.item_price}
            time={data.processing_time}
            togglediv={data.availability}
            addtocartclick={this.onAddToCartClickHandler(data)}
          />
        </React.Fragment>
      ));
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  render() {
    console.log(this.state.search, "Search");
    return (
      <React.Fragment>
        {/* <HeaderComponent onClick={this.onLogoutClick} /> notification='4' */}
        <SubHeaderComponent
          text="All Items"
          name="search"
          type="text"
          onChange={this.onSearchChange}
          value={this.state.search}
        />
        <div className="container p-0">
          <div className="row">
            <table className="table table-striped">
              <thead>
                <VendorPlaceOrderTableHead
                  code={"Item code"}
                  category={"Category"}
                  image={"Item Image"}
                  name={"Item Name"}
                  price={"Item Price"}
                  time={"Processing Time"}
                  availability={"Availability"}
                  cart={"Add To Cart"}
                />
              </thead>
              <tbody>{this.renderTableRowPlaceOrder()}</tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorPlaceOrder.propTypes = {
  logOutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  get_place_order: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  getall: state.getall.placeorder
});

export default connect(mapStateToProps, {
  logOutUser,
  get_place_order,
  add_to_cart
})(withRouter(VendorPlaceOrder));
