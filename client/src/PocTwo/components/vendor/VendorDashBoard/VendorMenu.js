import React, { Component } from 'react';
import HeaderComponent from '../../../reusablecomponents/headercomponents/HeaderComponent';
import SubHeaderComponent from '../../../reusablecomponents/headercomponents/SubHeaderComponent';
import TableHeadComponent from '../../../reusablecomponents/tablecomponents/TableHeadComponent';
import TableRowComponent from '../../../reusablecomponents/tablecomponents/TableRowComponent';

import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logOutUser } from './../../../store/actions/authActions';
import { get_menu_order } from '../../../store/actions/allGetAction';
import isEmpty from './../../../store/validation/is-Empty';

export class VendorMenu extends Component {
  constructor() {
    super();
    this.state = {
      vendormenuList: {},
      detailid: '',
      search: ''
    };
  }

  componentDidMount() {
    if (this.props.get_menu_order) {
      this.props.get_menu_order();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    // console.log(nextprops.getall);
    if (nextprops.getall !== nextstate.vendormenuList) {
      return { vendormenuList: nextprops.getall };
    }
    return null;
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logOutUser(this.props.history);
  };

  hendleMenuEdit = value => e => {
    // e.preventDefault();
    this.props.history.push({
      pathname: '/vendor-addedit',
      // search: '?query=abc',
      state: { detailid: value }
    });
    console.log(value);
  };

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  rendervendorMenuOrder = () => {
    const { vendormenuList } = this.state;
    console.log(vendormenuList);
    let filtereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, 'i');
      filtereddata = vendormenuList.filter(getall => {
        if (search.test(getall.item_name)) {
          return getall;
        }
        if (search.test(getall.category)) {
          return getall;
        }
        if (search.test(getall.item_code)) {
          return getall;
        }
      });
      // console.log(filtereddata);
    } else {
      filtereddata = vendormenuList;
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.map((data, index) => (
        <React.Fragment key={index}>
          <TableRowComponent
            key={index}
            code={data.item_code}
            category={data.category}
            name={data.item_name}
            img1={data.item_image}
            imgclass={'item-img-class'}
            price={data.item_price}
            time={data.processing_time}
            togglediv={data.availability}
            // s
            // togglediv={'toggle-div-class'}
            img={require('../../../assets/Images/penimg.svg')}
            onClickeditimg={this.hendleMenuEdit(data)}
          />
        </React.Fragment>
      ));
    } else {
      return (
        <h3 className='isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5'>
          Loading...
        </h3>
      );
    }
  };

  render() {
    // console.log(this.state.search, 'Search');
    return (
      <React.Fragment>
        <HeaderComponent onClick={this.onLogoutClick} />
        <SubHeaderComponent
          DisplayDropDown={true}
          text='All Items'
          name='search'
          type='text'
          onChange={this.onSearchChange}
          value={this.state.search}
        />
        <div className='container p-0'>
          <div className='row'>
            <table className='table table-striped'>
              <thead>
                <TableHeadComponent
                  code={'Item code'}
                  category={'Category'}
                  image={'Item Image'}
                  name={'Item Name'}
                  price={'Item Price'}
                  time={'Processing Time'}
                  availability={'Availability'}
                  edit={''}
                />
              </thead>
              <tbody>{this.rendervendorMenuOrder()}</tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorMenu.propTypes = {
  logOutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  get_menu_order: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  getall: state.getall.menuorder
});

export default connect(
  mapStateToProps,
  {
    logOutUser,
    get_menu_order
  }
)(withRouter(VendorMenu));
