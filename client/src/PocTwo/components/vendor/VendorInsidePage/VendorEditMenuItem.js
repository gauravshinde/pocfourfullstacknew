import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import HeaderComponent from '../../../reusablecomponents/headercomponents/HeaderComponent';
import LargeTextComponent from '../../../reusablecomponents/textcomponents/LargeTextComponent';
import InputComponent, {
  TextboxComponent,
  SelectCategory
} from '../../../reusablecomponents/inputcomponents/InputComponent';
import classnames from 'classnames';
import ButtonComponent from '../../../reusablecomponents/buttoncomponents/ButtonComponent';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { logOutUser } from './../../../store/actions/authActions';
import { edit_menu_item } from './../../../store/actions/allEditAction';
import { get_all_category } from './../../../store/actions/allGetAction';

export class VendorEditMenuItem extends Component {
  constructor() {
    super();
    this.state = {
      item_name: '',
      category: 'hello',
      item_code: '',
      item_price: '',
      processingt_ime: '',
      Description: '',
      photo: null,
      _id: '',
      errors: {},
      allcategory: {},
      selectedname: '',
      availability: true
    };
  }

  componentDidMount() {
    console.log(
      this.props.location.state.detailid,
      this.props.getall.menuorder
    );
    this.setState({
      item_name: this.props.location.state.detailid.item_name,
      Description: this.props.location.state.detailid.Description,
      item_code: this.props.location.state.detailid.item_code,
      item_price: this.props.location.state.detailid.item_price,
      processing_time: this.props.location.state.detailid.processing_time,
      _id: this.props.location.state.detailid._id,
      selectedname: this.props.location.state.detailid.category,
      category: this.props.location.state.detailid.category,
      availability: this.props.location.state.detailid.availability
    });
    // if (this.props.getall.length !== 0) {
    //   let menuItem = this.props.getall.find(
    //     getall => getall._id === this.props.location.state.detailid
    //   );
    //   console.log(menuItem);
    //   this.setState({
    //     ...menuItem
    //   });
    // }
    if (this.props.get_all_category) {
      this.props.get_all_category();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    if (nextprops.getall.get_allcategory !== nextstate.allcategory) {
      return { allcategory: nextprops.getall.get_allcategory };
    }
    return null;
  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.errors) {
  //     this.setState({
  //       errors: nextProps.errors
  //     });
  //   }
  // }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - ONCHANGE IMAGE UPLOAD HANDLER
   ****************************************/
  onChangeHandler = event => {
    this.setState({
      photo: event.target.files[0],
      loaded: 0
    });
  };

  /****************************************
   * @DESC - ONCLICK LOGOUT HANDLER
   ****************************************/
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logOutUser(this.props.history);
  };

  /****************************************
   * @DESC - ONCLICK EDIT HANDLER
   ****************************************/
  handleEditMenuSubmit = e => {
    e.preventDefault();
    const formData = {
      id: this.state._id,
      item_name: this.state.item_name,
      category: this.state.category,
      item_code: this.state.item_code,
      item_price: this.state.item_price,
      processing_time: this.state.processing_time,
      Description: this.state.Description,
      availability: this.state.availability
    };
    // console.log(this.state);
    // console.log(edititem);
    this.props.edit_menu_item(formData, this.props.history);
  };

  /**************************
   * @DESC - VENDOR ADD MENU ITEM
   ***************************/
  renderVendorEditMenuItem = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <InputComponent
          inputlabelclass='input-lableclass mt-0'
          labeltext='Item Name'
          imgsrcone={true}
          imgsrc={require('../../../assets/Images/user.png')}
          type='text'
          name='item_name'
          place='eg. Chicken Burger'
          value={this.state.item_name}
          onChange={this.onChange}
          error={errors.item_name}
          inputClass={classnames('input-field', { invalid: errors.item_name })}
        />
        <SelectCategory
          selectedname={this.state.selectedname}
          allcategoryList={this.state.allcategory}
          inputlabelclass='input-lableclass'
          labeltext='select category'
          imgsrc={require('../../../assets/Images/Grouplist.svg')}
          name='category'
          value={this.state.category}
          onChange={this.onChange}
          error={errors.category}
          inputClass={classnames('input-field p-0 pl-1', {
            invalid: errors.category
          })}
        />
        <div className='d-flex justify-content-between'>
          <div className=''>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='itemcode'
              imgsrcone={false}
              type='text'
              name='item_code'
              place=''
              value={this.state.item_code}
              onChange={this.onChange}
              error={errors.item_code}
              inputClass={classnames('input-field', {
                invalid: errors.item_code
              })}
            />
          </div>
          <div className='pl-2 pr-2'>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='itemprice'
              imgsrcone={true}
              imgsrc={require('../../../assets/Images/rupee.svg')}
              type='text'
              name='item_price'
              place=''
              value={this.state.item_price}
              onChange={this.onChange}
              error={errors.item_price}
              inputClass={classnames('input-field', {
                invalid: errors.item_price
              })}
            />
          </div>
          <div className=''>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='processingtime'
              imgsrcone={false}
              type='text'
              name='processing_time'
              place=''
              value={this.state.processing_time}
              onChange={this.onChange}
              error={errors.processing_time}
              inputClass={classnames('input-field', {
                invalid: errors.processing_time
              })}
            />
          </div>
        </div>
        <TextboxComponent
          inputlabelclass='input-lableclass w-100'
          textareaLabel='Description'
          name='Description'
          value={this.state.Description}
          onChange={this.onChange}
          place='eg. Ingredients'
          error={errors.Description}
          textareadescription={classnames('text-area-description', {
            invalid: errors.Description
          })}
        />
        {/* <div className='row'>
          <div className='col-sm-10'>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='photo'
              imgsrcone={true}
              imgsrc={require('../../../assets/Images/photo.svg')}
              type='file'
              name='photo'
              place='eg. Riverdale'
              onChange={this.onChangeHandler}
              error={errors.photo}
              inputClass={classnames('custom-file-input p-0', {
                invalid: errors.photo
              })}
            />
          </div>
          <div className='col-sm-2'></div>
        </div> */}
        <div className='d-flex justify-content-between mt-4 mb-5'>
          <Link to='/vendor-menu'>
            <ButtonComponent
              buttonclass='btn button-width button-border-brown'
              buttontext='cancel'
            />
          </Link>
          <ButtonComponent
            buttonclass='btn button-width button-brown'
            buttontext='save'
            type='submit'
            onClick={this.handleEditMenuSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <HeaderComponent onClick={this.onLogoutClick} />
        <div className='container-fluid main-margin-padding vendor-edit-menu-main'>
          <div className='row'>
            <div className='vendor-edit-menu-sectionone'>
              <LargeTextComponent
                largetext='Edit Menu Item'
                largeclass='large-text'
              />
              <form>{this.renderVendorEditMenuItem()}</form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorEditMenuItem.propTypes = {
  logOutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  edit_menu_item: PropTypes.func.isRequired,
  get_all_category: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  getall: state.getall, //.menuorder,
  // getall: state.getall.get_allcategory,
  alledit: state.alledit.menuedits
});

export default connect(
  mapStateToProps,
  {
    logOutUser,
    edit_menu_item,
    get_all_category
  }
)(withRouter(VendorEditMenuItem));
