import React, { Component } from 'react';
import InputComponent, {
  TextboxComponent
} from '../../../reusablecomponents/inputcomponents/InputComponent';
import classnames from 'classnames';
import ButtonComponent from './../../../reusablecomponents/buttoncomponents/ButtonComponent';
import HeaderComponent from './../../../reusablecomponents/headercomponents/HeaderComponent';
import LargeTextComponent from './../../../reusablecomponents/textcomponents/LargeTextComponent';

import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmpty from './../../../store/validation/is-Empty';
import { logOutUser } from './../../../store/actions/authActions';
import { confirm_order } from '../../../store/actions/allPostAction';

export class VendorCheckout extends Component {
  constructor() {
    super();
    this.state = {
      phone_number: '',
      name: '',
      room: '',
      notes: '',
      errors: {}
    };
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    console.log(this.state);
  };

  onConfirmOrder = e => {
    e.preventDefault();
    const formData = {
      phone_number: this.state.phone_number,
      name: this.state.name,
      room: this.state.room,
      notes: this.state.notes
    };
    console.log(formData);
    this.props.confirm_order(formData, this.props.history);
  };

  renderCheckout = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <InputComponent
          inputlabelclass='input-lableclass'
          labeltext='number'
          imgsrcone={true}
          imgsrc={require('../../../assets/Images/phone-img.svg')}
          type='number'
          name='phone_number'
          place='eg. 1234567890'
          onChange={this.onChange}
          value={this.state.phone_number}
          error={errors.phone_number}
          inputClass={classnames('input-field', {
            invalid: errors.phone_number
          })}
        />
        <InputComponent
          inputlabelclass='input-lableclass'
          labeltext='name'
          imgsrcone={true}
          imgsrc={require('../../../assets/Images/user.png')}
          type='text'
          name='name'
          place='eg. James Bond'
          onChange={this.onChange}
          value={this.state.name}
          error={errors.name}
          inputClass={classnames('input-field', {
            invalid: errors.name
          })}
        />
        <InputComponent
          inputlabelclass='input-lableclass'
          labeltext='room'
          imgsrcone={false}
          type='text'
          name='room'
          place='eg. Room 205'
          onChange={this.onChange}
          value={this.state.room}
          error={errors.room}
          inputClass={classnames('input-field', {
            invalid: errors.room
          })}
        />
        <TextboxComponent
          inputlabelclass='input-lableclass w-100'
          textareaLabel='notes'
          name='notes'
          value={this.state.notes}
          onChange={this.onChange}
          place='eg. Ingredients'
          error={errors.notes}
          textareadescription={classnames('text-area-description', {
            invalid: errors.notes
          })}
        />
        <div className='d-flex justify-content-between mt-4 mb-5'>
          <Link to='/cart'>
            <ButtonComponent
              buttonclass='btn button-width button-border-brown'
              buttontext='Back to cart'
            />
          </Link>
          <ButtonComponent
            buttonclass='btn button-width button-brown'
            buttontext='Confirm Order'
            onClick={this.onConfirmOrder}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <HeaderComponent />
        <div className='container-fluid main-margin-padding vendor-checkout-main'>
          <div className='row'>
            <div className='vendor-checkout-sectionone'>
              <LargeTextComponent
                largetext='My Cart > Checkout'
                largeclass='large-text'
              />
              <p>
                total price &nbsp;<span>Rs. 2500</span>
              </p>
              <hr />
              <form>{this.renderCheckout()}</form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorCheckout.propTypes = {
  logOutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  confirm_order: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  getall: state.getall
});

export default connect(
  mapStateToProps,
  {
    logOutUser,
    confirm_order
  }
)(withRouter(VendorCheckout));
