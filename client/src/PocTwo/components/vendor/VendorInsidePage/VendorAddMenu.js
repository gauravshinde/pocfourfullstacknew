import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import axios from 'axios';
import HeaderComponent from '../../../reusablecomponents/headercomponents/HeaderComponent';
import LargeTextComponent from '../../../reusablecomponents/textcomponents/LargeTextComponent';
import InputComponent, {
  TextboxComponent
} from '../../../reusablecomponents/inputcomponents/InputComponent';
import classnames from 'classnames';
import { SelectCategory } from '../../../reusablecomponents/inputcomponents/InputComponent';
import ButtonComponent from '../../../reusablecomponents/buttoncomponents/ButtonComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { add_menu_item } from './../../../store/actions/allPostAction';
import { clear_error } from '../../../store/actions/errorActions';
import { get_all_category } from '../../../store/actions/allGetAction';

export class VendorAddMenu extends Component {
  constructor() {
    super();
    this.state = {
      item_name: '',
      category: '',
      item_code: '',
      item_price: '',
      processing_time: '',
      Description: '',
      item_image: '',
      availability: true,
      errors: {},
      allcategory: {}
    };
  }

  componentDidMount() {
    if (this.props.get_all_category) {
      this.props.get_all_category();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    if (nextprops.getall !== nextstate.allcategory) {
      return { allcategory: nextprops.getall };
    }
    return null;
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.props.clear_error();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /****************************************
   * @DESC - ONCHANGE IMAGE UPLOAD HANDLER
   ****************************************/
  onChangeHandler = event => {
    this.props.clear_error();
    this.setState({
      item_image: event.target.files[0],
      loaded: 0
    });
  };

  /**************************
   * @DESC - ONSUBMIT HANDLER
   ***************************/
  onSubmit = e => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user.id,
      item_name: this.state.item_name,
      category: this.state.category,
      item_code: this.state.item_code,
      item_price: this.state.item_price,
      processing_time: this.state.processing_time,
      Description: this.state.Description,
      // item_image: this.state.item_image,
      availability: this.state.availability
    };
    // console.log(formData);
    this.props.add_menu_item(formData);
  };

  /**************************
   * @DESC - VENDOR ADD MENU ITEM
   ***************************/
  renderVendorAddMenuItem = () => {
    const { errors } = this.state;
    // const { allcategory } = this.state;
    return (
      <React.Fragment>
        <InputComponent
          inputlabelclass='input-lableclass mt-0'
          labeltext='item name'
          imgsrcone={true}
          imgsrc={require('../../../assets/Images/user.png')}
          type='text'
          name='item_name'
          place='eg. Chicken Burger'
          value={this.state.item_name}
          onChange={this.onChange}
          error={errors.item_name}
          inputClass={classnames('input-field', { invalid: errors.item_name })}
        />
        <SelectCategory
          allcategoryList={this.state.allcategory}
          inputlabelclass='input-lableclass'
          labeltext='select category'
          imgsrc={require('../../../assets/Images/Grouplist.svg')}
          name='category'
          value={this.state.category}
          onChange={this.onChange}
          error={errors.category}
          inputClass={classnames('input-field p-0 pl-1', {
            invalid: errors.category
          })}
        />
        <div className='d-flex justify-content-between'>
          <div className=''>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='item code'
              imgsrcone={false}
              type='text'
              name='item_code'
              place=''
              value={this.state.item_code}
              onChange={this.onChange}
              error={errors.item_code}
              inputClass={classnames('input-field', {
                invalid: errors.item_code
              })}
            />
          </div>
          <div className='pl-2 pr-2'>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='item price'
              imgsrcone={true}
              imgsrc={require('../../../assets/Images/rupee.svg')}
              type='text'
              name='item_price'
              place=''
              value={this.state.item_price}
              onChange={this.onChange}
              error={errors.item_price}
              inputClass={classnames('input-field', {
                invalid: errors.item_price
              })}
            />
          </div>
          <div className=''>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='processing time'
              imgsrcone={false}
              type='text'
              name='processing_time'
              place=''
              value={this.state.processing_time}
              onChange={this.onChange}
              error={errors.processing_time}
              inputClass={classnames('input-field', {
                invalid: errors.processing_time
              })}
            />
          </div>
        </div>
        <TextboxComponent
          inputlabelclass='input-lableclass w-100'
          textareaLabel='Description'
          name='Description'
          value={this.state.Description}
          onChange={this.onChange}
          place='eg. Ingredients'
          error={errors.Description}
          textareadescription={classnames('text-area-description', {
            invalid: errors.Description
          })}
        />
        {/* <div className='row'>
          <div className='col-sm-10'>
            <InputComponent
              inputlabelclass='input-lableclass'
              labeltext='item image'
              imgsrcone={true}
              imgsrc={require('../../../assets/Images/photo.svg')}
              id='file'
              value={this.state.item_image}
              onChange={this.onChange}
              type='file'
              name='item_image'
              place='eg. Riverdale'
              error={errors.item_image}
              inputClass={classnames('custom-file-input p-0', {
                invalid: errors.item_image
              })}
            />
          </div>
          <div className='col-sm-2'></div>
        </div> */}
        <div className='d-flex justify-content-between mt-4 mb-5'>
          <Link to='/vendor-menu'>
            <ButtonComponent
              buttonclass='btn button-width button-border-brown'
              buttontext='cancel'
            />
          </Link>
          <ButtonComponent
            buttonclass='btn button-width button-brown'
            buttontext='save'
            // type='submit'
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    // console.log(this.state);
    return (
      <React.Fragment>
        <HeaderComponent />
        <div className='container-fluid main-margin-padding vendor-add-menu-item-main'>
          <div className='row'>
            <div className='vendor-add-menu-sectionone'>
              <LargeTextComponent
                largetext='Add Menu Item'
                largeclass='large-text'
              />
              <form>{this.renderVendorAddMenuItem()}</form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  errors: state.errors.errors,
  auth: state.auth,
  getall: state.getall.get_allcategory
});

export default connect(
  mapStateToProps,
  { add_menu_item, clear_error, get_all_category }
)(withRouter(VendorAddMenu));
