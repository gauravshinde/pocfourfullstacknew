import React, { Component } from 'react';
import InputComponent from '../../../reusablecomponents/inputcomponents/InputComponent';
import { Link } from 'react-router-dom';
import LargeTextComponent from './../../../reusablecomponents/textcomponents/LargeTextComponent';
import ButtonComponent from './../../../reusablecomponents/buttoncomponents/ButtonComponent';
import classnames from 'classnames';
import HeaderComponent from '../../../reusablecomponents/headercomponents/HeaderComponent';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { add_category_item } from './../../../store/actions/allPostAction';
import { clear_error } from '../../../store/actions/errorActions';

export class VendorAddCategory extends Component {
  constructor() {
    super();
    this.state = {
      category_name: '',
      errors: {}
    };
  }

  /**************************
   * @DESC - ONCHANGE HANDLER
   ***************************/
  onChange = e => {
    this.props.clear_error();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /**************************
   * @DESC - ONSUBMIT HANDLER
   ***************************/
  onSubmit = e => {
    e.preventDefault();
    let formData = {
      vendor_id: this.props.auth.user.id,
      category_name: this.state.category_name
    };
    this.props.add_category_item(formData);
  };

  renderVendorAddCategory = () => {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <InputComponent
          inputlabelclass='input-lableclass'
          labeltext=''
          imgsrcone={true}
          imgsrc={require('../../../assets/Images/Grouplist.svg')}
          type='text'
          name='category_name'
          place='eg. Desert'
          onChange={this.onChange}
          value={this.state.category_name}
          error={errors.category_name}
          inputClass={classnames('input-field', {
            invalid: errors.category_name
          })}
        />
        <div className='d-flex justify-content-between mt-4 mb-5'>
          <Link to='/vendor-menu'>
            <ButtonComponent
              buttonclass='btn button-width button-border-brown'
              buttontext='cancel'
            />
          </Link>

          <ButtonComponent
            buttonclass='btn button-width button-brown'
            buttontext='save'
            type='submit'
            onClick={this.onSubmit}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <HeaderComponent />
        <div className='container-fluid main-margin-padding vendor-add-category-main'>
          <div className='row'>
            <div className='vendor-add-category-sectionone'>
              <LargeTextComponent
                largetext='Add Category'
                largeclass='large-text'
              />
              <hr />
              <form>{this.renderVendorAddCategory()}</form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  errors: state.errors.errors,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { add_category_item, clear_error }
)(withRouter(VendorAddCategory));
