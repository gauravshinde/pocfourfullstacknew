import React, { Component } from 'react';
import HeaderComponent from './../../../reusablecomponents/headercomponents/HeaderComponent';
import VendorCartHead from '../../../reusablecomponents/tablecomponents/VendorCartTable/VendorCartHead';
import VendorCartBody from '../../../reusablecomponents/tablecomponents/VendorCartTable/VendorCartBody';
import ButtonComponent from './../../../reusablecomponents/buttoncomponents/ButtonComponent';

import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmpty from './../../../store/validation/is-Empty';
import { logOutUser } from './../../../store/actions/authActions';
import { get_all_cart_details } from './../../../store/actions/allGetAction';
import { checkout_order } from '../../../store/actions/allPostAction';

export class VendorCart extends Component {
  constructor() {
    super();
    this.state = {
      allAddCartList: {},
      cartdata: ''
    };
  }

  componentDidMount() {
    if (this.props.get_all_cart_details) {
      this.props.get_all_cart_details();
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    console.log(nextprops.getall);
    if (nextprops.getall !== nextstate.allAddCartList) {
      return { allAddCartList: nextprops.getall };
    }
    return null;
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logOutUser(this.props.history);
  };

  /********************************
   * @DESC DECREMENT HANDLER
   ********************************/
  decrementClickHandler = e => {
    e.preventDefault();
    console.log('decrement');
  };

  /********************************
   * @DESC INCREMENT HANDLER
   ********************************/
  incrementClickHandler = e => {
    e.preventDefault();
    console.log('increment');
  };

  rendervendorCartDetails = () => {
    let data = [];
    let a = [];
    const { allAddCartList } = this.state;
    if (!isEmpty(allAddCartList)) {
      data = allAddCartList.map((data, index) => {
        a = data.processing;
        if (a.length > 0) {
          return a.map((article, index) => (
            <React.Fragment key={index}>
              <VendorCartBody
                key={index}
                code={article.item_code}
                category={article.category}
                name={article.item_name}
                price={article.item_price}
                decrementclick={this.decrementClickHandler}
                qty={article.item_qty}
                incrementclick={this.incrementClickHandler}
                totalitemprice={article.total_price}
              />
            </React.Fragment>
          ));
        } else return [];
        // c = a.length;
        // for (let b = 0; b < c; b++) {
        //   const d = a[b];
        //   console.log(d);
        //   return (
        //     <React.Fragment>
        //
        //     </React.Fragment>
        //   );
        // }
        // console.log(a);
        // console.log(a.length);
        // console.log(a[0].item_qty);
        // console.log(a[1]);
        // console.log(data.processing);
        // console.log(data.total_price_all_product);
      });
    }
    return data;
  };

  onCartSubmitData = value => e => {
    e.preventDefault();

    let allData = value.processing;
    let allItem_name = [];
    {
      for (const [index, value] of allData.entries()) {
        allItem_name.push(value.item_name);
      }
    }
    let allItem_code = [];
    {
      for (const [index, value] of allData.entries()) {
        allItem_code.push(value.item_code);
      }
    }
    let allItem_qty = [];
    {
      for (const [index, value] of allData.entries()) {
        allItem_qty.push(value.item_qty);
      }
    }
    let allItem_price = [];
    {
      for (const [index, value] of allData.entries()) {
        allItem_price.push(value.item_price);
      }
    }

    let allItem_totalprice = value.total_price_all_product;
    let allItem_category = [];
    {
      for (const [index, value] of allData.entries()) {
        allItem_category.push(value.category);
      }
    }

    const formData = {
      vendor_id: this.props.auth.user.id,
      item_name: allItem_name,
      item_code: allItem_code,
      item_qty: allItem_qty,
      item_price: allItem_price,
      total_price_all_product: allItem_totalprice,
      category: allItem_category
    };
    console.log(formData);
    this.props.checkout_order(formData, this.props.history);
  };

  renderVendorCartButton = () => {
    let data = [];
    let a = [];
    let c = [];
    const { allAddCartList } = this.state;
    if (!isEmpty(allAddCartList)) {
      data = allAddCartList.map((data, index) => {
        a = data.total_item_qty;
        c = data.total_price_all_product;
        return (
          <React.Fragment key={index}>
            <div className='col-6 ml-auto d-flex vendor-cart-price'>
              <div className='vendor-cart-left'>
                <h5>
                  total qty: <span>{a}</span>
                </h5>
              </div>
              <div className='vendor-cart-right'>
                <h5>
                  total price: Rs. <span>{c}</span>
                </h5>
              </div>
            </div>
            <div className='col-6 ml-auto d-flex justify-content-between mb-5'>
              <Link to='/vendor-menu'>
                <ButtonComponent
                  buttonclass='btn button-width button-border-blue'
                  buttontext='Back to Menu'
                />
              </Link>
              <ButtonComponent
                buttonclass='btn button-width button-blue'
                buttontext='Checkout'
                type='button'
                onClick={this.onCartSubmitData(data)}
              />
            </div>
          </React.Fragment>
        );
      });
    }
    return data;
  };

  render() {
    return (
      <React.Fragment>
        {/* <HeaderComponent onClick={this.onLogoutClick} /> */}
        <div className='col-7 vendor-cart-main'>
          <div className='row'>
            <form className='w-100'>
              <h3>my cart</h3>
              <table className='table table-striped'>
                <thead>
                  <VendorCartHead
                    code={'Item Code'}
                    category={'Category'}
                    name={'Item Name'}
                    price={'Item Price'}
                    qty={'Qty.'}
                    totalitemprice={'Total Item Price'}
                  />
                </thead>
                <tbody>{this.rendervendorCartDetails()}</tbody>
              </table>
              {this.renderVendorCartButton()}
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorCart.propTypes = {
  logOutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  get_all_cart_details: PropTypes.func.isRequired,
  checkout_order: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  getall: state.getall.getallcart
});

export default connect(
  mapStateToProps,
  {
    logOutUser,
    get_all_cart_details,
    checkout_order
  }
)(withRouter(VendorCart));
