import React, { Component } from 'react';
import VendorsPendingComponents from './ViewVendor/Seating/Pending/VendorsPendingComponents';
import VendorSeatsComponents from './ViewVendor/Seating/Seats/VendorSeatsComponents';
import VendorReservationsComponents from './ViewVendor/Seating/Reservations/VendorReservationsComponents';
import VendorHistoryComponents from './ViewVendor/Seating/History/VendorHistoryComponents';

export class InsideSubHeader extends Component {
  constructor() {
    super();
    this.state = {
      selectedMenuInside: 1
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectedMenuInside: value
    });
  };

  render() {
    return (
      <React.Fragment>
        {/* <div class='container-fluid admin_margin-padding-main'> */}
        <div className='inside-main-menu d-flex justify-content-center'>
          <nav className='navbar'>
            <p
              onClick={this.onPageChange(1)}
              className={
                this.state.selectedMenuInside === 1
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              Pending
            </p>
            <p
              onClick={this.onPageChange(2)}
              className={
                this.state.selectedMenuInside === 2
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              Seats
            </p>
            <p
              onClick={this.onPageChange(3)}
              className={
                this.state.selectedMenuInside === 3
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              Reservations
            </p>
            <p
              onClick={this.onPageChange(4)}
              className={
                this.state.selectedMenuInside === 4
                  ? 'navbar__link_active'
                  : 'navbar__link'
              }
            >
              History
            </p>
          </nav>
        </div>
        <div className='container-fluid'>
          <div className='row'>
            {this.state.selectedMenuInside === 1 ? (
              <VendorsPendingComponents />
            ) : (
              ''
            )}
            {this.state.selectedMenuInside === 2 ? (
              <VendorSeatsComponents />
            ) : (
              ''
            )}
            {this.state.selectedMenuInside === 3 ? (
              <VendorReservationsComponents />
            ) : (
              ''
            )}
            {this.state.selectedMenuInside === 4 ? (
              <VendorHistoryComponents />
            ) : (
              ''
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default InsideSubHeader;
