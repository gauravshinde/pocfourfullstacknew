import React, { Component } from "react";
import AdminNavbar from "../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import SearchboxName from "../../AdminReusableComponent/SearchBoxandPageName/SearchboxName";
import HeaderTableVendor from "../../AdminReusableComponent/AllTableReusableComponent/VendorPageTable/HeaderTableVendor";
import BodyTableVendor from "../../AdminReusableComponent/AllTableReusableComponent/VendorPageTable/BodyTableVendor";

import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
import dateFns from "date-fns";

import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
// import { PropTypes } from "prop-types";
import isEmpty from "./../../../store/validation/is-Empty";
import { superAdmin_get_vendor_details } from "./../../../store/actions/SuperAdminAllAction/SuperAdminAllGetActions";
import SlidingComponent from "./../../../reusableComponents/SlidingComponent";
import { superAdmin_update_vendor_status } from "./../../../store/actions/SuperAdminAllAction/SuperAdminAllPatchAction";

const totalRecordsInOnePage = 10;

export class MainAllVendorsComponents extends Component {
  constructor() {
    super();
    this.state = {
      allVendorDetailsData: {},
      viewDetailPage: "",
      search: "",
      currentPagination: 1,
      currentState: true
    };
  }

  componentDidMount() {
    this.props.superAdmin_get_vendor_details();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.superadmingetdetails);
    if (nextProps.superAdminGetDetails !== nextState.allVendorDetailsData) {
      return {
        allVendorDetailsData: nextProps.superAdminGetDetails
      };
    }
    return null;
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  onChangePagination = page => {
    // console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  onClickVendorViewDetails = value => e => {
    this.props.history.push({
      pathname: "/viewvendordetails",
      state: { viewDetailPage: value }
    });
    console.log(value);
  };

  /********************************************
   * @DESC -ON CHANGE TOGGLE STATUS CHANGE
   ********************************************/
  onSelectToggleStatusChange = data => e => {
    // e.preventDefault();
    const formData = {
      vendor_id: data.vendor_id,
      has_admin_approved: data.has_admin_approved === true ? false : true
    };
    //console.log(formData);
    this.props.superAdmin_update_vendor_status(formData);
  };

  renderTableBody = () => {
    const { allVendorDetailsData, currentPagination } = this.state;
    //console.log(allVendorDetailsData);
    let Vendorfiltereddata = [];
    let title = [];

    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      Vendorfiltereddata = allVendorDetailsData.filter(getall => {
        if (search.test(getall.restaurant_name)) {
          return getall;
        }
        if (search.test(getall.primary_restaurant_type.title)) {
          return getall;
        }
        if (search.test(getall.restaurant_mobile_number)) {
          return getall;
        }
        if (search.test(getall.restaurant_city)) {
          return getall;
        }
        if (search.test(getall.restaurant_address)) {
          return getall;
        }
      });
      //console.log(Vendorfiltereddata);
    } else {
      Vendorfiltereddata = allVendorDetailsData;
    }
    if (!isEmpty(Vendorfiltereddata)) {
      return Vendorfiltereddata.map(
        (data, index) =>
          index >= (currentPagination - 1) * totalRecordsInOnePage &&
          index < currentPagination * totalRecordsInOnePage &&
          ((title = data),
          (
            <React.Fragment key={index}>
              <BodyTableVendor
                key={index}
                srno={++index}
                vendorname={data.restaurant_name}
                namepara="Kinza"
                emailpara={data.restaurant_email}
                phonepara={data.restaurant_mobile_number}
                addresspara={data.restaurant_address}
                citypara={data.restaurant_city}
                typepara={title.primary_restaurant_type.title}
                Transactionspara="20,000"
                MemberSincepara={dateFns.format(
                  data.register_date,
                  " DD MMM YYYY"
                )}
                eyeonclick={this.onClickVendorViewDetails(data)}
                slidingButton={
                  <SlidingComponent
                    name="has_admin_approved"
                    currentState={data.has_admin_approved}
                    type={"checkbox"}
                    spantext1={"Active"}
                    spantext2={"Inactive"}
                    toggleclass={"toggle d-flex align-items-center mb-2"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={this.toggleFunction}
                    slidingonClick={this.onSelectToggleStatusChange(data)}
                    defaultChecked={
                      data.has_admin_approved === true ? true : false
                    }
                  />
                }
              />
            </React.Fragment>
          ))
      );
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  render() {
    const { allVendorDetailsData } = this.state;
    //console.log(allVendorDetailsData);
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className="container-fluid admin_margin-padding-main">
          <SearchboxName
            headingtext="All Vendors"
            name="search"
            type="text"
            onChange={this.onSearchChange}
            value={this.state.search}
          />
          <div className="table-responsive newtable-vendor">
            <table className="table table-striped">
              <thead>
                <HeaderTableVendor
                  srno="Sr. No"
                  name="Name"
                  contactDetails="Contact Details"
                  address="Address"
                  restaurantDetails="Restaurant Details"
                  actions="Actions"
                />
              </thead>
              <tbody>{this.renderTableBody()}</tbody>
            </table>
          </div>
          <div className="col-12 mt-5 d-flex justify-content-center pagination-main-css p4v2-menu-pagination">
            {/* {this.renderVendorTablePagination()} */}
            <Pagination
              onChange={this.onChangePagination}
              current={this.state.currentPagination}
              defaultPageSize={totalRecordsInOnePage}
              total={
                this.state.allVendorDetailsData
                  ? this.state.allVendorDetailsData.length
                  : ""
              }
              showTitle={false}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  superAdminGetDetails: state.superAdminGetDetails.SuperAdminVendorDetails
});

export default connect(mapStateToProps, {
  superAdmin_get_vendor_details,
  superAdmin_update_vendor_status
})(withRouter(MainAllVendorsComponents));
