import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MainViewDetailsDashboard from './ViewVendor/Dashboard/MainViewDetailsDashboard';
import MainViewDetailsOrdering from './ViewVendor/Ordering/MainViewDetailsOrdering';
import MainViewDetailsSeating from './ViewVendor/Seating/MainViewDetailsSeating';
import MainViewDetailsMenu from './ViewVendor/Menu/MainViewDetailsMenu';
import MainViewDetailsMisc from './ViewVendor/Misc/MainViewDetailsMisc';
import MainViewDetailsEventsPackages from './ViewVendor/Events&Packages/MainViewDetailsEventsPackages';
import MainViewDetailsSettings from './ViewVendor/Settings/MainViewDetailsSettings';

export class SubHeaderVendor extends Component {
  constructor() {
    super();
    this.state = {
      selectedmenu: 1
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectedmenu: value
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className='container-fluid admin_margin-padding-main sub-header-main'>
          <div className='row'>
            <div className='col-4 d-flex align-items-center'>
              <Link to='/allvendors' className='back-button-css'>
                <i className='fa fa-caret-left' aria-hidden='true'></i>
                <span>Back</span>
              </Link>
              <div className='d-flex image-vendor-view'>
                <img
                  src={require('../../../assets/images/AdminDashboard/home/vendorimage.png')}
                  alt='icon vendor'
                  className='img-fluid'
                />
                <div className='inside-view-info'>
                  <h5>McDonalds</h5>
                  <p>View info</p>
                </div>
              </div>
            </div>
            <div className='col-8 '>
              <nav className='navbar'>
                <p
                  onClick={this.onPageChange(1)}
                  className={
                    this.state.selectedmenu === 1
                      ? 'navbar__link_active'
                      : 'navbar__link'
                  }
                >
                  Dashboard
                </p>
                <p
                  onClick={this.onPageChange(2)}
                  className={
                    this.state.selectedmenu === 2
                      ? 'navbar__link_active'
                      : 'navbar__link'
                  }
                >
                  Ordering
                </p>
                <p
                  onClick={this.onPageChange(3)}
                  className={
                    this.state.selectedmenu === 3
                      ? 'navbar__link_active'
                      : 'navbar__link'
                  }
                >
                  Seating
                </p>
                <p
                  onClick={this.onPageChange(4)}
                  className={
                    this.state.selectedmenu === 4
                      ? 'navbar__link_active'
                      : 'navbar__link'
                  }
                >
                  Menu
                </p>
                <p
                  onClick={this.onPageChange(5)}
                  className={
                    this.state.selectedmenu === 5
                      ? 'navbar__link_active'
                      : 'navbar__link'
                  }
                >
                  Misc
                </p>
                <p
                  onClick={this.onPageChange(6)}
                  className={
                    this.state.selectedmenu === 6
                      ? 'navbar__link_active'
                      : 'navbar__link'
                  }
                >
                  Events & Packages
                </p>
                <p
                  onClick={this.onPageChange(7)}
                  className={
                    this.state.selectedmenu === 7
                      ? 'navbar__link_active'
                      : 'navbar__link'
                  }
                >
                  Settings
                </p>
              </nav>
            </div>
          </div>
        </div>
        <div className='container-fluid'>
          <div className='row'>
            {this.state.selectedmenu === 1 ? <MainViewDetailsDashboard /> : ''}
            {this.state.selectedmenu === 2 ? <MainViewDetailsOrdering /> : ''}
            {this.state.selectedmenu === 3 ? <MainViewDetailsSeating /> : ''}
            {this.state.selectedmenu === 4 ? <MainViewDetailsMenu /> : ''}
            {this.state.selectedmenu === 5 ? <MainViewDetailsMisc /> : ''}
            {this.state.selectedmenu === 6 ? (
              <MainViewDetailsEventsPackages />
            ) : (
              ''
            )}
            {this.state.selectedmenu === 7 ? <MainViewDetailsSettings /> : ''}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SubHeaderVendor;
