import React, { Component } from 'react';
// import HeaderComponent from '../Header/HeaderComponent';
import ButtonComponent from '../../../../../../pocsrcone/Components/SmallComponents/ButtonComponent';
import InputComponent from '../../../../../../pocsrcone/Components/SmallComponents/InputComponent';
import DefaultWaitTime from '../../../../../../pocsrcone/Components/VendorDetails/DefaultWaitTime';
import VendorTableHeadRow from '../../../../../../pocsrcone/Components/VendorDetails/VendorTableHeadRow';
import VendorTableHeadSubRow from '../../../../../../pocsrcone/Components/VendorDetails/VendorTableHeadSubRow';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../../../../../pocsrcone/store/actions/authAction';
import isEmpty from '../../../../../../pocsrcone/store/validation/is-empty';
import { getDinner } from '../../../../../../pocsrcone/store/actions/dinerAction';
import {
  getStatusChange,
  set_new_waitTime
} from '../../../../../../pocsrcone/store/actions/statuschangeAction';
import {
  get_default_wait_time,
  update_default_wait_time
} from '../../../../../../pocsrcone/store/actions/defaultwaitTimeAction';
// import Loader from '../Loader/Loader';
// import 'loaders.css/src/animations/line-scale.scss';
// import PopUp from "./../SmallComponents/PopUp";
function demoAsyncCall() {
  return new Promise(resolve => setTimeout(() => resolve(), 3300));
}

var xSetInterval = [];
function onStartCountDown(waitTime, index, dinner) {
  // console.log(index, "Index");
  if (waitTime) {
    let mins = waitTime;
    mins = mins - 1;
    let seconds = 59;
    if (waitTime !== 0) {
      var x = setInterval(() => {
        if (seconds === 0) {
          let formData = {
            _id: dinner._id,
            wait_time: mins
          };
          set_new_waitTime(formData);
          mins = mins - 1;
          seconds = 59;
        }
        if (mins === 0) {
          clearInterval(x);
          clearInterval();
        }
        seconds = seconds - 1;
        const timer = mins + ' : ' + seconds;
        if (index) {
          document.getElementById(index).innerHTML = timer;
        }
      }, 1000);
      xSetInterval.push(x);
    }
  }
}

const SeatStatus = ['Nonseated', 'Seated', 'Completed', 'Cancelled'];

export class VendorSeatsComponents extends Component {
  constructor() {
    super();
    this.state = {
      dinnerId: '',
      dinnerList: {},
      vendorId: '',
      defaultWaitTime: 0,
      partySize: 'All',
      show: 'all',
      status: '',
      search: '',
      loading: true,
      showPopup: false,
      newWaitTimeDefault: {}
    };
  }

  onWaitTimeChange = e => {
    this.setState({
      inputWaitTime: e.target.value
    });
  };

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  componentDidMount() {
    // this simulates an async action, after which the component will render the content
    // demoAsyncCall().then(() => this.setState({ loading: false }));
    // console.log(this.props.defaultWaitTime);
    // TO get the default wait time from backend
    this.props.get_default_wait_time();
    this.props.getDinner(this.props.auth.user._id);
    // this.props.getDinner(this.state.vendorId);

    // this.defaultwaittime();
    // if (this.props.getStatusChange) {
    //   this.props.getStatusChange(this.state.statusdata);
    // }
  }

  componentDidUpdate() {
    if (this.state.vendorId) {
      console.log('Called', this.state.dinnerList);
      if (!this.state.hasCalledDiner) {
        this.props.getDinner(this.state.vendorId);
        this.setState({ hasCalledDiner: true });
      }
    }
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  componentWillUnmount() {
    xSetInterval.forEach(xval => {
      clearInterval(xval);
      clearInterval();
    });
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.dinner !== nextState.dinnerList) {
      return {
        dinnerList: nextProps.dinner
      };
    }
    return null;
  }

  // static getDerivedStateFromProps(nextprops, nextstate) {
  //   xSetInterval.forEach(xval => {
  //     clearInterval(xval);
  //     clearInterval();
  //   });
  //   // console.log("Default Wait time in REducer", nextprops.defaultWaitTime);
  //   if (
  //     nextprops.defaultWaitTime.default_wait_time !== nextstate.defaultWaitTime
  //   ) {
  //     return {
  //       defaultWaitTime: parseInt(nextprops.defaultWaitTime.default_wait_time)
  //     };
  //   }
  //   if (nextprops.auth.user.id !== nextstate.vendorId) {
  //     return { vendorId: nextprops.auth.user.id };
  //   }
  //   if (nextprops.dinner !== nextstate.dinnerList) {
  //     return { dinnerList: nextprops.dinner };
  //   }
  //   if (nextprops.dinner !== nextstate.statusdata) {
  //     return { statusdata: nextprops.dinner };
  //   }
  //   // if (nextprops.defaultwaittimebackend !== nextstate.newWaitTimeDefault) {
  //   //   return {
  //   //     newWaitTimeDefault:
  //   //       nextprops.defaultwaittimebackend.defaultwaittimebackend
  //   //   };
  //   // }

  //   if (nextprops.dinner) {
  //     return {
  //       dinnerList: nextprops.dinner
  //     };
  //   }

  //   return null;
  // }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser(this.props.history);
  };

  /*******************************
   * @DESC - DEFAULT TIME HANDLER
   ******************************/
  onDefaultTimeHandler = value => {
    if (value === '00') {
      this.setState({ defaultWaitTime: 0 });
      this.props.update_default_wait_time({ default_time: 0 });
    } else if (value === '5-10') {
      this.setState({ defaultWaitTime: 10 });
      this.props.update_default_wait_time({ default_time: 10 });
    } else if (value === '10-20') {
      this.setState({ defaultWaitTime: 20 });
      this.props.update_default_wait_time({ default_time: 20 });
    } else if (value === '30-45') {
      this.setState({ defaultWaitTime: 45 });
      this.props.update_default_wait_time({ default_time: 45 });
    } else if (value === '45+') {
      this.setState({ defaultWaitTime: 50 });
      this.props.update_default_wait_time({ default_time: 50 });
    }
  };

  /************************************
   * @DESC - DEFAULT PARTY SIZE HANDLER
   ************************************/
  onDefaultPartySizeHandler = value => {
    if (value === 'All') {
      this.setState({ partySize: 'All' });
    } else if (value === '8>') {
      this.setState({ partySize: '8>' });
    } else if (value === '2') {
      this.setState({ partySize: 2 });
    } else if (value === '6') {
      this.setState({ partySize: 6 });
    } else if (value === '4') {
      this.setState({ partySize: 4 });
    }
  };

  /**************************************
   * @DESC -SHOWALL || NON SEATED TOGGLER
   **************************************/
  handleShow = value => e => {
    console.log('Clicked');
    this.setState({
      show: value
    });
  };

  /********************************************
   * @DESC -ON SELECTSTATUS CHANGE
   ********************************************/
  onSelectStatusChange = dinnerId => e => {
    // console.log("alle");
    // console.log(e.target.value);
    e.preventDefault();

    const statusUpdate = {
      dinnerId: dinnerId,
      status: e.target.value
    };

    this.props.getStatusChange(statusUpdate);
  };

  /*******************************************
   * @DESC - SET TIME REVERSE COUNTER
   ******************************************/
  // onStartCountDown = (waitTime, index) => {

  // };

  shouldComponentUpdate() {
    return true;
  }

  renderdinnername = () => {
    const { dinnerList } = this.state;
    let renderList = [];
    // console.log(this.state.partySize, this.state.defaultWaitTime);
    if (this.state.partySize === 'All') {
      renderList = dinnerList;
    } else if (this.state.partySize === '8>') {
      renderList = dinnerList.filter(dinner => dinner.total >= 8);
    } else {
      renderList = dinnerList.filter(
        dinner => dinner.total <= this.state.partySize
      );
    }
    if (this.state.show === 'Nonseated') {
      renderList = renderList.filter(dinner => dinner.status === 'Nonseated');
    }

    if (this.state.search) {
      let search = new RegExp(this.state.search, 'i');
      renderList = renderList.filter(dinner => {
        if (search.test(dinner.name)) {
          return dinner;
        }
      });
    }
    xSetInterval.forEach(xval => {
      clearInterval(xval);
      clearInterval();
    });
    if (!isEmpty(renderList)) {
      return renderList.map((dinner, index) => {
        let waitingImage = require('../../../../../../pocsrcone/assets/Images/clock-circular-outline.svg');
        if (dinner.wait_time < 1) {
          waitingImage = require('../../../../../../pocsrcone/assets/Images/walking-img.svg');
        }
        onStartCountDown(dinner.wait_time, index, dinner);
        // console.log(dinner.status);

        return (
          <React.Fragment key={index}>
            <VendorTableHeadSubRow
              dinner={dinner}
              dinnerID={dinner._id}
              key={index}
              timerKey={index}
              no={++index}
              Name={dinner.name}
              Phone={dinner.phone_number}
              Img1={require('../../../../../../pocsrcone/assets/Images/envelope-img.svg')}
              onClickone={this.togglePopup.bind(this)}
              Img={waitingImage}
              imgclass={'p-0'}
              Adults={dinner.adults}
              Kids={dinner.kids}
              Total={dinner.total}
              Occassion={dinner.special_occassion}
              WaitTime={dinner.wait_time}
              onWaitTimeChange={this.onWaitTimeChange}
              inputWaitTime={this.state.inputWaitTime}
              onWaitTimeSubmit={this.onWaitTimeSubmit}
              Img2={require('../../../../../../pocsrcone/assets/Images/penimg.svg')}
              Timer={'00:00'}
              seatingStatus={dinner.status}
              Status={SeatStatus}
              onSelectStatusChange={this.onSelectStatusChange}
            />
            {/* ***** Pop Up Conditions ***** */}
          </React.Fragment>
        );
      });
    }
  };

  render() {
    // const { loading } = this.state;

    // if (loading) {
    //   return (
    //     <React.Fragment>
    //       <Loader />
    //     </React.Fragment>
    //   );
    // }

    return (
      <React.Fragment>
        {/* <HeaderComponent
          headertext={'Logo'}
          vendorDashboard={true}
          addVendorBtn={true}
          editVendorbtn={true}
          link={'/dashboard'}
          linktwo={'/add-diner'}
          linkbutton={'Add Diner'}
          onClick={this.onLogoutClick}
          defaultWaitTime={this.state.defaultWaitTime}
        /> */}
        <div className='container-fluid vendor-details-container shadow-none mt-5'>
          <div className='row'>
            <div className='col-12 col-md-4 d-flex align-items-center'>
              <ButtonComponent
                buttontype={'button'}
                buttonclass={
                  this.state.show === 'Nonseated'
                    ? 'login-button login-button--vendor-details-show mr-30 mb-30'
                    : 'login-button login-button--vendor-details-show-all mr-30 mb-30'
                }
                buttontext={'Show Non Seated'}
                handleOnClick={this.handleShow('Nonseated')}
              />

              <ButtonComponent
                buttontype={'button'}
                buttonclass={
                  this.state.show === 'all'
                    ? 'login-button login-button--vendor-details-show mr-30 mb-30'
                    : 'login-button login-button--vendor-details-show-all mr-30 mb-30'
                }
                buttontext={'Show All'}
                handleOnClick={this.handleShow('all')}
              />
            </div>

            <div className='col-12 col-md-8'>
              <div className='table-time-outer-block'>
                <div>
                  <h3 className='font-28-medium'>
                    Default Wait Time{' '}
                    <span className='font-21-medium dark-gray-text'>
                      &#40;Mins&#41;
                    </span>
                  </h3>
                  <DefaultWaitTime
                    arrayNumber={['00', '5-10', '10-20', '30-45', '45+']}
                    onClick={this.onDefaultTimeHandler}
                    currentWaitTime={this.state.defaultWaitTime}
                  />
                </div>
                <div>
                  <h3 className='font-28-medium'>Filter By Party Size </h3>

                  <DefaultWaitTime
                    arrayNumber={['2', '4', '6', '8>', 'All']}
                    onClick={this.onDefaultPartySizeHandler}
                    partySize={this.state.partySize}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='container-fluid vendor-details-container shadow-none table-container '>
          <div className='col-12 vendor-search my-3 px-0 d-flex justify-content-between align-items-center'>
            <div>
              <h4 className={'vendor'}>
                {this.state.show === 'all'
                  ? 'All Seats'
                  : 'Viewing All Nonseated'}
              </h4>
            </div>

            <div className='d-flex justify-content-between'>
              {/*w-25*/}
              <InputComponent
                img={require('../../../../../../pocsrcone/assets/Images/search.svg')}
                searchClass={'search-label'}
                onChange={this.onSearchChange}
                value={this.state.search}
                place={'search'}
                inputclass={
                  'form-control input-bottomblack mb-0 pl-3 input-search'
                }
              />
              {/* <img
                src={require("./../../assets/Images/sort-button-with-three-lines.svg")}
                alt="sort-button-with-three-lines"
              />
              <img
                src={require("./../../assets/Images/filter.svg")}
                alt="filter-img"
              /> */}
            </div>
          </div>
          <div className='vendordetail-col table-responsive'>
            <table className='table table-striped mb-0'>
              <thead>
                <VendorTableHeadRow
                  no={'sr. No'}
                  Name={'Name'}
                  Phone={'Phone Number'}
                  OrderType={'Order Type'}
                  Adults={'Adults'}
                  Kids={'Kids'}
                  Total={'Total'}
                  Occassion={'Special Occassion'}
                  WaitTime={'WaitTime'}
                  Timer={'Timer'}
                  Status={'Status'}
                />
              </thead>

              <tbody>{this.renderdinnername()}</tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

VendorSeatsComponents.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  dinner: PropTypes.array.isRequired,
  getDinner: PropTypes.func.isRequired,
  getStatusChange: PropTypes.func.isRequired,
  newDefaultWaitTime: PropTypes.func.isRequired,
  vendor: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  dinner: state.dinner.dinner,
  vendor: state.vendor,
  defaultWaitTime: state.defaultWaitTime
});

export default connect(
  mapStateToProps,
  {
    logoutUser,
    getDinner,
    getStatusChange,
    set_new_waitTime,
    get_default_wait_time,
    update_default_wait_time
  }
)(withRouter(VendorSeatsComponents));
