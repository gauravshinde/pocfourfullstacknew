import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
// api
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getALLICONS } from "../../../../../store/actions/iconAction";
import { edit_menu_item } from "../../../../../store/actions/SuperAdminAllAction/menu/MenuAllPatchActions";
import isEmpty from "../../../../../store/validation/is-Empty";
import axios from "axios";

import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
import InputFieldReusable from "./InputFieldReusable";
import TextareaReusable from "./TextareaReusable";
import SelectInputDropdownReusable from "./SelectInputDropdownReusable";
import ToggleReusable from "./ToggleReusable";
import InputFieldSpiceLevelReusable from "./InputFieldSpiceLevelReusable";
// import { CompanyDetails } from "./CompanyDetails";

const foodServingTypeDropdownOptions = ["People", "Gram", "Oz"];

// FETCH THE LIST FROM THE BACKEND
const list = ["Main Course", "Dessert"];

const foodTime = ["Breakfast", "Lunch", "Dinner"];

const dayArray = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thrusday",
  "Friday",
  "Saturday",
  "Sunday"
];

const timeArray = [
  "00:00",
  "01:00",
  "02:00",
  "03:00",
  "04:00",
  "05:00",
  "06:00",
  "07:00",
  "08:00",
  "09:00",
  "10:00",
  "11:00",
  "12:00",
  "13:00",
  "14:00",
  "15:00",
  "16:00",
  "17:00",
  "18:00",
  "19:00",
  "20:00",
  "21:00",
  "22:00",
  "23:00",
  "24:00"
];

class MainViewDetailsMenuEditItemForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // menu items details fold
      foodItemName: this.props.location.propData.data.item_name,
      selectedFoodCategory: this.props.location.propData.data.category,
      dropdown: false,
      listOfFoodCategories: list,
      foodItemCode: this.props.location.propData.data.item_code,
      foodItemPrice: this.props.location.propData.data.item_price,
      processingTime: this.props.location.propData.data.processing_time,
      calorificValue: this.props.location.propData.data.calorific_value,
      foodServingSize: this.props.location.propData.data.serving_size,
      foodServingTypeDropdown: this.props.location.propData.data.item_unit,
      foodDescription: this.props.location.propData.data.description,
      fileNames: this.props.location.propData.data.photo,
      // renderSpecificTimingsBlock
      isSpecificTimingToggle: this.props.location.propData.data
        .specific_time_item,
      // remainign
      multiple_opening_time: this.props.location.propData.data
        .multiple_opening_time,
      foodItemAvailableTags: this.props.location.propData.data
        .item_available_for,
      Monday: this.props.location.propData.data.monday,
      Tuesday: this.props.location.propData.data.tuesday,
      Wednesday: this.props.location.propData.data.wednesday,
      Thrusday: this.props.location.propData.data.thursday,
      Friday: this.props.location.propData.data.friday,
      Saturday: this.props.location.propData.data.saturday,
      Sunday: this.props.location.propData.data.sunday,
      // renderFoodDetailsBlock
      foodDetailsTypeTags: this.props.location.propData.data.food_type,
      food_details_type: [],
      foodDetailsAllergyTags: this.props.location.propData.data.allergy_info,
      food_details_allergy: [],
      // renderTogglesAndTextBlock
      isTakeAwayToggle: this.props.location.propData.data.available_take_away
        .isTakeAwayToggle,
      isTakeAwayPrice: this.props.location.propData.data.available_take_away
        .isTakeAwayPrice,
      isCurbSideToggle: this.props.location.propData.data.available_curb_side
        .isCurbSideToggle,
      isCurbSidePrice: this.props.location.propData.data.available_curb_side
        .isCurbSidePrice,
      isAutoAcceptToggle: this.props.location.propData.data.item_auto_accept,
      isFoodItemSpicyToggle: this.props.location.propData.data.item_spicy,
      foodSpiceLevel: this.props.location.propData.data.level_of_spiciness,
      isSpiceLevelCustomizableToggle: this.props.location.propData.data
        .spice_level_customizable,
      spiceLevelDefaultValue:
        this.props.location.propData.data.level_of_spiciness === "mild"
          ? "1"
          : this.props.location.propData.data.level_of_spiciness === "medium"
          ? "2"
          : this.props.location.propData.data.level_of_spiciness === "hot"
          ? "3"
          : this.props.location.propData.data.level_of_spiciness === "veryHot"
          ? "4"
          : "2",
      spiceLevelLowestValue: "1",
      // renderNutritionInfoBlock
      foodFat: this.props.location.propData.data.nutrition_info.Fat.Fat,
      foodSaturated: this.props.location.propData.data.nutrition_info.Fat
        .Saturated,
      foodUnsaturated: this.props.location.propData.data.nutrition_info.Fat
        .UnSaturated,
      foodCarbohydrates: this.props.location.propData.data.nutrition_info
        .Carbohydrates.Carbohydrates,
      foodDietaryFibers: this.props.location.propData.data.nutrition_info
        .Carbohydrates.Fiber,
      foodTotalSugars: this.props.location.propData.data.nutrition_info
        .Carbohydrates.Suger,
      foodProtiens: this.props.location.propData.data.nutrition_info.Protiens,
      foodCholestrol: this.props.location.propData.data.nutrition_info
        .Cholestrol,
      foodSodium: this.props.location.propData.data.nutrition_info.Sodium,
      oneTagSelected: this.props.location.propData.data.select_tag
    };
  }

  /***************************************************
   * SelectInputDropdownReusable lifecycle
   ***************************************************/
  componentDidMount() {
    // get all icons
    this.props.getALLICONS();
    // dropdown
    document.addEventListener("click", this.onDropdownClick);
    document.addEventListener("keypress", this.onDropdownKeyPress);
  }

  componentWillUnmount() {
    // dropdown
    document.removeEventListener("click", this.onDropdownClick);
    document.removeEventListener("keypress", this.onDropdownKeyPress);
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      nextProps.icons.get_food_item_icons !== nextState.food_details_type ||
      nextProps.icons.get_food_item_allergy_icons !==
        nextState.food_details_allergy
    ) {
      return {
        food_details_type: nextProps.icons.get_food_item_icons,
        food_details_allergy: nextProps.icons.get_food_item_allergy_icons
      };
    }
    return null;
  }
  /***************************************************
   * SelectInputDropdownReusable handlers start
   ***************************************************/
  onDropdownKeyPress = e => {
    if (this.state.dropdown) {
      if (e.keyCode === 13) {
        e.preventDefault();
        this.dropDownToggler();
      }
    }
  };

  onDropdownClick = e => {
    if (this.state.dropdown) {
      if (!document.getElementById("selectedFoodCategory").contains(e.target)) {
        this.dropDownToggler();
      }
    }
  };

  onDropdownChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  dropDownToggler = e => {
    this.setState({
      dropdown: !this.state.dropdown
    });
  };

  dropDownSelect = value => e => {
    this.setState({
      selectedFoodCategory: value,
      dropdown: !this.state.dropdown
    });
  };

  /***************************************************
   * SelectInputDropdownReusable end
   ***************************************************/

  /**************************************************
   * specific timing handlers
   **************************************************/
  onDayOpenCloseHanlder = day => e => {
    let state = this.state;
    let dayData = state[day];
    dayData.open = !dayData.open;
    this.setState({
      state: state
    });
  };

  onTimeSelectHandlerSingle = day => e => {
    console.log(day);
    let state = this.state;
    let dayData = state[day];
    dayData[e.target.name] = e.target.value;
    this.setState({
      state: state
    });
  };

  onFoodTypeOpenClose = (day, food) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData.open = !foodTimeData.open;
    this.setState({
      state: state
    });
  };

  onTimeSelectHandlerMultiple = (day, food) => e => {
    let state = this.state;
    let dayData = state[day];
    let foodTimeData = dayData[food];
    foodTimeData[e.target.name] = e.target.value;
    this.setState({
      state: state
    });
  };

  /**************************************************
   * specific timing handlers end
   **************************************************/

  /***************************************************
   * handlers
   ***************************************************/
  handleOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onfoodServingTypeDropdownSelect = e => {
    // console.log("Selected: " + e.value);
    this.setState({
      foodServingTypeDropdown: e.value
    });
  };

  handleFilesOnChange = e => {
    const data = new FormData();
    // this.setState({ loader: true });
    data.append("image", e.target.files[0]);
    axios
      .post("/image/upload-content-images", data)
      .then(res => {
        console.log(res);
        this.setState({ fileNames: res.data.image_URL });
      })
      .catch(err => {
        // this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  handleSpiceLevel = val => {
    let defaultVal =
      val === "mild"
        ? "1"
        : val === "medium"
        ? "2"
        : val === "hot"
        ? "3"
        : val === "veryHot"
        ? "4"
        : "2";
    this.setState({
      foodSpiceLevel: val,
      spiceLevelDefaultValue: defaultVal
    });
  };

  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
    // console.log(e.target.name, e.target.checked);
  };

  handleSelectTag = val => e => {
    e.preventDefault();
    this.setState({
      oneTagSelected: val
    });
    console.log(val);
  };

  handleFoodItemAvailableTags = val => e => {
    e.preventDefault();
    let foodItem = this.state.foodItemAvailableTags;
    if (foodItem.length === 0) {
      foodItem.push(val);
    } else {
      let isAlreadyPresent = foodItem.includes(val);
      if (isAlreadyPresent) {
        let indexOf = foodItem.indexOf(val);
        if (indexOf === 0 || indexOf) {
          foodItem.splice(indexOf, 1);
        }
      } else {
        foodItem.push(val);
      }
    }
    this.setState({
      foodItemAvailableTags: foodItem
    });
  };

  handleFoodDetailsTypeTags = data => e => {
    e.preventDefault();
    let foodItem = this.state.foodDetailsTypeTags;
    if (foodItem.length === 0) {
      foodItem.push(data);
    } else {
      let isAlreadyPresent = foodItem.some(findVal => findVal._id === data._id);
      if (isAlreadyPresent) {
        let indexOf = foodItem.findIndex(findVal => findVal._id === data._id);
        if (indexOf === 0 || indexOf) {
          foodItem.splice(indexOf, 1);
        }
      } else {
        foodItem.push(data);
      }
    }
    this.setState({
      foodDetailsTypeTags: foodItem
    });
  };

  handleFoodDetailsAllergyTags = data => e => {
    e.preventDefault();
    let foodItem = this.state.foodDetailsAllergyTags;
    if (foodItem.length === 0) {
      foodItem.push(data);
    } else {
      let isAlreadyPresent = foodItem.some(findVal => findVal._id === data._id);
      if (isAlreadyPresent) {
        let indexOf = foodItem.findIndex(findVal => findVal._id === data._id);
        if (indexOf === 0 || indexOf) {
          foodItem.splice(indexOf, 1);
        }
      } else {
        foodItem.push(data);
      }
    }
    this.setState({
      foodDetailsAllergyTags: foodItem
    });
  };

  handleOnSubmit = e => {
    e.preventDefault();
    // console.log(this.state.foodServingTypeDropdown);
    let take_away = {
      isTakeAwayToggle: this.state.isTakeAwayToggle,
      isTakeAwayPrice: this.state.isTakeAwayToggle
        ? this.state.isTakeAwayPrice
        : ""
    };

    let curb_side = {
      isCurbSideToggle: this.state.isCurbSideToggle,
      isCurbSidePrice: this.state.isCurbSideToggle
        ? this.state.isCurbSidePrice
        : ""
    };

    let nutrition_info_local = {
      Fat: {
        Fat: this.state.foodFat,
        Saturated: this.state.foodSaturated,
        UnSaturated: this.state.foodUnsaturated
      },
      Carbohydrates: {
        Carbohydrates: this.state.foodCarbohydrates,
        Fiber: this.state.foodDietaryFibers,
        Suger: this.state.foodTotalSugars
      },
      Protiens: this.state.foodProtiens,
      Cholestrol: this.state.foodCholestrol,
      Sodium: this.state.foodSodium
    };
    let formData = {
      id: this.props.location.propData.data._id,
      vendor_id: this.props.auth.user._id,
      item_name: this.state.foodItemName,
      category: this.state.selectedFoodCategory,
      item_code: this.state.foodItemCode,
      item_price: this.state.foodItemPrice,
      processing_time: this.state.processingTime,
      calorific_value: this.state.calorificValue,
      serving_size: this.state.foodServingSize,
      item_unit: this.state.foodServingTypeDropdown,
      description: this.state.foodDescription,
      photo: this.state.fileNames,
      specific_time_item: this.state.isSpecificTimingToggle,
      multiple_opening_time: this.state.multiple_opening_time,
      item_available_for: this.state.foodItemAvailableTags,
      monday: this.state.Monday,
      tuesday: this.state.Tuesday,
      wednesday: this.state.Wednesday,
      thursday: this.state.Thrusday,
      friday: this.state.Friday,
      saturday: this.state.Saturday,
      sunday: this.state.Sunday,
      food_type: this.state.foodDetailsTypeTags,
      allergy_info: this.state.foodDetailsAllergyTags,
      available_take_away: take_away,
      available_curb_side: curb_side,
      item_auto_accept: this.state.isAutoAcceptToggle,
      item_spicy: this.state.isFoodItemSpicyToggle,
      level_of_spiciness: this.state.isFoodItemSpicyToggle
        ? this.state.foodSpiceLevel
        : "medium",
      spice_level_customizable: this.state.isFoodItemSpicyToggle
        ? this.state.isSpiceLevelCustomizableToggle
        : true,
      nutrition_info: nutrition_info_local,
      select_tag: this.state.oneTagSelected
    };
    // console.log(formData);
    this.props.edit_menu_item(formData, this.props.history);
  };

  /***************************************************
   * renderFoodItemDetailsBlock
   ***************************************************/
  renderFoodItemDetailsBlock = () => {
    return (
      <div className="p4v2-add-menu-item-form__mb-30">
        <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
          Item Details
        </h3>
        {/* Item Name */}
        <InputFieldReusable
          labeltext="Item Name"
          inputlabelclass="input-label"
          imgbox="d-none"
          name="foodItemName"
          type="text"
          place="eg. Chicken Burger"
          onChange={this.handleOnChange}
          value={this.state.foodItemName}
          inputclass={"map-inputfield"}
        />

        {/* select category */}
        <label className="input-label" htmlFor="selectedFoodCategory">
          Select Category
        </label>
        <SelectInputDropdownReusable
          id="selectedFoodCategory"
          name="selectedFoodCategory"
          value={this.state.selectedFoodCategory}
          onInputChangeHandler={this.onDropdownChange}
          dropDownToggler={this.dropDownToggler}
          dropDown={this.state.dropdown}
          suggestionList={this.state.listOfFoodCategories}
          dropDownSelect={this.dropDownSelect}
          placeholder={"eg. Main Course"}
        />

        <div className="row">
          <div className="col-6">
            {/* Item Code */}
            <InputFieldReusable
              labeltext="Item Code"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="foodItemCode"
              type="text"
              place=""
              onChange={this.handleOnChange}
              value={this.state.foodItemCode}
              inputclass={"map-inputfield"}
            />
          </div>
          <div className="col-6">
            {/* Item Price */}
            <InputFieldReusable
              labeltext="Item Price"
              inputlabelclass="input-label"
              imgbox="img-box img-box--addMenuItem"
              imgsrc={require("../../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/rupee.png")}
              imgclass="img-fluid img"
              name="foodItemPrice"
              id="foodItemPrice"
              type="text"
              place=""
              onChange={this.handleOnChange}
              value={this.state.foodItemPrice}
              inputclass={"input-field"}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-3">
            {/* Serving Size */}
            <InputFieldReusable
              labeltext="Serving Size"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="foodServingSize"
              type="text"
              place=""
              onChange={this.handleOnChange}
              value={this.state.foodServingSize}
              inputclass={"map-inputfield"}
            />
          </div>
          <div className="col-3">
            {/* foodServingTypeDropdown */}
            <span className="input-label invisible">Serving Size</span>
            <Dropdown
              className="poc4v2-menulist-foodServingTypeDropdown"
              options={foodServingTypeDropdownOptions}
              value={this.state.foodServingTypeDropdown}
              onChange={this.onfoodServingTypeDropdownSelect}
              placeholder="eg. People"
            />
          </div>
          <div className="col-6">
            {/* Processing Time */}
            <InputFieldReusable
              labeltext="Processing time"
              inputlabelclass="input-label"
              imgbox="d-none"
              name="processingTime"
              type="text"
              place=""
              onChange={this.handleOnChange}
              value={this.state.processingTime}
              inputclass={"map-inputfield"}
            />
          </div>
        </div>

        {/* Description */}
        <TextareaReusable
          labeltext={"Description"}
          inputlabelclass={"input-label"}
          name={"foodDescription"}
          type={"text"}
          onChange={this.handleOnChange}
          value={this.state.foodDescription}
          place={"eg. Ingredients"}
          Textareaclass={
            "form-control textarea-custom p4v2-add-menu-item-textarea"
          }
        />

        {/* Photo */}
        <div className="p4-v2-cuisine-main col-12 px-0">
          <label className="input-label" htmlFor="fileNames">
            Photo
          </label>
          <div className="view_chainsBor mt-0" style={{ width: "100%" }}>
            <div className="custom_file_upload">
              <input
                type="file"
                name="fileNames"
                id="fileNames"
                onChange={this.handleFilesOnChange}
                className="custom_input_upload"
              />

              {/* {this.state.fileNames.length > 2 ? ( */}
              <label
                className="custom_input_label newChain_add"
                htmlFor="fileNames"
              >
                <div>
                  <i className="fa fa-plus"></i>
                </div>
                <div className="add_new_text">Add New</div>
              </label>
              {/* ) : (
                ""
              )} */}
            </div>

            <div className="newChain_addthree">
              {/* {console.log(this.state.fileNames)} */}
              {/* {this.state.fileNames.length > 2 ? ( */}
              <img
                src={this.state.fileNames}
                className="newChain_addtwo"
                alt="food"
              />
              {/* ) : null} */}
            </div>
          </div>
        </div>
      </div>
    );
  };

  /****************************************************
   * renderSpecificTimingsBlock
   ***************************************************/
  renderSpecificTimingsBlock = () => {
    return (
      <>
        <Restauranttiming
          state={this.state}
          toggleFunction={this.toggleFunction}
        />
        {!this.state.multiple_opening_time &&
        this.state.isSpecificTimingToggle ? (
          <NoMultipleTime
            state={this.state}
            onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
            onTimeSelectHandlerSingle={this.onTimeSelectHandlerSingle}
          />
        ) : null}
        {this.state.multiple_opening_time &&
        this.state.isSpecificTimingToggle ? (
          <YesMultipleTime
            state={this.state}
            onDayOpenCloseHanlder={this.onDayOpenCloseHanlder}
            onFoodTypeOpenClose={this.onFoodTypeOpenClose}
            onTimeSelectHandlerMultiple={this.onTimeSelectHandlerMultiple}
          />
        ) : null}

        {/* <CompanyDetails /> */}
        <div className="p4v2-pb-20">
          <span className="input-label p4v2-pb-5">Item available for</span>
          <div className="row m-0">
            <button
              className={
                this.state.foodItemAvailableTags.includes("All Day")
                  ? "p4v2-text-tag p4v2-text-tag--active"
                  : "p4v2-text-tag"
              }
              onClick={this.handleFoodItemAvailableTags("All Day")}
            >
              All Day
            </button>
            <button
              className={
                this.state.foodItemAvailableTags.includes("Breakfast")
                  ? "p4v2-text-tag p4v2-text-tag--active"
                  : "p4v2-text-tag"
              }
              onClick={this.handleFoodItemAvailableTags("Breakfast")}
            >
              Breakfast
            </button>
            <button
              className={
                this.state.foodItemAvailableTags.includes("Brunch")
                  ? "p4v2-text-tag p4v2-text-tag--active"
                  : "p4v2-text-tag"
              }
              onClick={this.handleFoodItemAvailableTags("Brunch")}
            >
              Brunch
            </button>
            <button
              className={
                this.state.foodItemAvailableTags.includes("Lunch")
                  ? "p4v2-text-tag p4v2-text-tag--active"
                  : "p4v2-text-tag"
              }
              onClick={this.handleFoodItemAvailableTags("Lunch")}
            >
              Lunch
            </button>
            <button
              className={
                this.state.foodItemAvailableTags.includes("Dinner")
                  ? "p4v2-text-tag p4v2-text-tag--active"
                  : "p4v2-text-tag"
              }
              onClick={this.handleFoodItemAvailableTags("Dinner")}
            >
              Dinner
            </button>
            <button
              className={
                this.state.foodItemAvailableTags.includes("Special")
                  ? "p4v2-text-tag p4v2-text-tag--active"
                  : "p4v2-text-tag"
              }
              onClick={this.handleFoodItemAvailableTags("Special")}
            >
              Special
            </button>
          </div>
        </div>
      </>
    );
  };

  /*****************************************************
   * renderFoodDetailsBlock
   ******************************************************/
  renderFoodDetailsBlock = () => {
    const {
      foodDetailsTypeTags,
      food_details_type,
      foodDetailsAllergyTags,
      food_details_allergy
    } = this.state;
    return (
      <div className="p4v2-pb-20">
        <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
          Food Details
        </h3>
        <span className="input-label p4v2-pb-5">Type</span>
        <div className="row m-0 p4v2-pb-5">
          {!isEmpty(food_details_type) &&
            food_details_type.map((val, index) => (
              <button
                key={index}
                className={
                  foodDetailsTypeTags.some(findVal => findVal._id === val._id)
                    ? "p4v2-icon-text-tag p4v2-icon-text-tag--active"
                    : "p4v2-icon-text-tag"
                }
                onClick={this.handleFoodDetailsTypeTags(val)}
              >
                <img src={val.icon} alt="food" className="p4v2-tags-icon" />
                {val.title}
              </button>
            ))}
        </div>
        <span className="input-label p4v2-pb-5">Allergy Info</span>
        <div className="row m-0 p4v2-pb-5">
          {!isEmpty(food_details_allergy) &&
            food_details_allergy.map((val, index) => (
              <button
                key={index}
                className={
                  foodDetailsAllergyTags.some(
                    findVal => findVal._id === val._id
                  )
                    ? "p4v2-icon-text-tag p4v2-icon-text-tag--active"
                    : "p4v2-icon-text-tag"
                }
                onClick={this.handleFoodDetailsAllergyTags(val)}
              >
                <img src={val.icon} alt="food" className="p4v2-tags-icon" />
                {val.title}
              </button>
            ))}
        </div>
      </div>
    );
  };

  /***************************************************
   * renderTogglesAndTextBlock
   ***************************************************/
  renderTogglesAndTextBlock = () => {
    return (
      <div>
        {/* Available for take-away */}
        <div className="p4v2-pb-10">
          <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
            Available for take-away
          </h3>
          <ToggleReusable
            name="isTakeAwayToggle"
            currentState={this.state.isTakeAwayToggle}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
          />
          {this.state.isTakeAwayToggle && (
            <div className="col-5 col-md-4 col-lg-3 px-0">
              {/* Enter Price */}
              <InputFieldReusable
                labeltext="Enter Price"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="isTakeAwayPrice"
                type="text"
                place="eg. 20%"
                onChange={this.handleOnChange}
                value={this.state.isTakeAwayPrice}
                inputclass={"map-inputfield"}
              />
            </div>
          )}
        </div>
        {/* Available for curb-side */}
        <div className="p4v2-pb-10">
          <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
            Available for curb-side
          </h3>
          <ToggleReusable
            name="isCurbSideToggle"
            currentState={this.state.isCurbSideToggle}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
          />
          {this.state.isCurbSideToggle && (
            <div className="col-5 col-md-4 col-lg-3 px-0">
              {/* Enter Price */}
              <InputFieldReusable
                labeltext="Enter Price"
                inputlabelclass="input-label"
                imgbox="d-none"
                name="isCurbSidePrice"
                type="text"
                place="eg. 20%"
                onChange={this.handleOnChange}
                value={this.state.isCurbSidePrice}
                inputclass={"map-inputfield"}
              />
            </div>
          )}
        </div>
        {/* Item available for auto accept? */}
        <div className="p4v2-pb-10">
          <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
            Item available for auto accept?
          </h3>
          <ToggleReusable
            name="isAutoAcceptToggle"
            currentState={this.state.isAutoAcceptToggle}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
          />
        </div>
        {/* Is this item spicy? */}
        <div>
          <div className="row mx-0 p4v2-pb-10">
            <div className="col-6 pl-0">
              <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
                Is this item spicy?
              </h3>
              <ToggleReusable
                name="isFoodItemSpicyToggle"
                currentState={this.state.isFoodItemSpicyToggle}
                spantext1={"Yes"}
                spantext2={"No"}
                toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
                toggleinputclass={"toggle__switch ml-3 mr-3"}
                onChange={this.toggleFunction}
              />
            </div>
            <div className="col-6 pr-0">
              {this.state.isFoodItemSpicyToggle && (
                <>
                  {/* Is the spice level customizable? */}
                  <div className="p4v2-pb-10">
                    <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
                      Is the spice level customizable?
                    </h3>
                    <ToggleReusable
                      name="isSpiceLevelCustomizableToggle"
                      currentState={this.state.isSpiceLevelCustomizableToggle}
                      spantext1={"Yes"}
                      spantext2={"No"}
                      toggleclass={
                        "toggle d-flex align-items-center p4v2-pb-20"
                      }
                      toggleinputclass={"toggle__switch ml-3 mr-3"}
                      onChange={this.toggleFunction}
                    />
                  </div>
                  {this.state.isSpiceLevelCustomizableToggle && (
                    <div className="row mx-0">
                      <div className="col-6 px-0">
                        {/* Default Value */}
                        <InputFieldSpiceLevelReusable
                          labeltext="Default Value"
                          inputlabelclass="input-label"
                          name="spiceLevelDefaultValue"
                          type="text"
                          place="eg. 3"
                          onChange={this.handleOnChange}
                          value={this.state.spiceLevelDefaultValue}
                          inputclass={
                            "map-inputfield map-inputfield--spiceLevelDisabled text-center"
                          }
                          disabled={true}
                        />
                      </div>
                      <div className="col-6 px-0">
                        {/* Lowest Value */}
                        <InputFieldSpiceLevelReusable
                          labeltext="Lowest Value"
                          inputlabelclass="input-label"
                          name="spiceLevelLowestValue"
                          type="text"
                          place="eg. 1"
                          onChange={this.handleOnChange}
                          value={this.state.spiceLevelLowestValue}
                          inputclass={"map-inputfield text-center"}
                        />
                      </div>
                    </div>
                  )}
                </>
              )}
            </div>
          </div>
          {this.state.isFoodItemSpicyToggle && (
            <>
              <div className="col-12 px-0 p4v2-pb-10">
                <h4 className="p4v2-font-21-black p4v2-pb-10">
                  Select a level for spiciness.
                </h4>
                <div className="p4v2-add-menu-item-form__mb-30 spice-level-outer-block">
                  <div className="spice-level-bg-block p4v2-font-21-medium">
                    {this.state.foodSpiceLevel === "mild" ? (
                      <div className="spice-level-pointer spice-level-pointer--1"></div>
                    ) : this.state.foodSpiceLevel === "medium" ? (
                      <div className="spice-level-pointer spice-level-pointer--2"></div>
                    ) : this.state.foodSpiceLevel === "hot" ? (
                      <div className="spice-level-pointer spice-level-pointer--3"></div>
                    ) : this.state.foodSpiceLevel === "veryHot" ? (
                      <div className="spice-level-pointer spice-level-pointer--4"></div>
                    ) : (
                      ""
                    )}
                    <span
                      className="spice-level-text1"
                      onClick={() => this.handleSpiceLevel("mild")}
                    >
                      Mild
                    </span>
                    <span
                      className="spice-level-text2"
                      onClick={() => this.handleSpiceLevel("medium")}
                    >
                      Medium
                    </span>
                    <span
                      className="spice-level-text3"
                      onClick={() => this.handleSpiceLevel("hot")}
                    >
                      Hot
                    </span>
                    <span
                      className="spice-level-text4"
                      onClick={() => this.handleSpiceLevel("veryHot")}
                    >
                      Very Hot
                    </span>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    );
  };

  /***************************************************
   * renderNutritionInfoBlock
   ***************************************************/
  renderNutritionInfoBlock = () => {
    return (
      <div className="p4v2-add-menu-item-form__mb-30">
        <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
          Nutrition info
        </h3>
        <div className="row mx-0">
          <div className="col-6 px-0">
            {/* Calorific Value */}
            <InputFieldReusable
              labeltext="Calorific Value"
              inputlabelclass="input-label p4v2-font-21-black"
              imgbox="d-none"
              name="calorificValue"
              type="text"
              place="eg. 10gms"
              onChange={this.handleOnChange}
              value={this.state.calorificValue}
              inputclass={"map-inputfield"}
            />
          </div>
        </div>
        {/* fats and carbohydrates row */}
        <div className="row m-0">
          {/* fats block */}
          <div className="col-6 pl-0">
            <div className="row m-0">
              <div className="col-12 px-0">
                {/* Fat */}
                <InputFieldReusable
                  labeltext="Fat"
                  inputlabelclass="input-label p4v2-font-21-black"
                  imgbox="d-none"
                  name="foodFat"
                  type="text"
                  place="eg. 10gms"
                  onChange={this.handleOnChange}
                  value={this.state.foodFat}
                  inputclass={"map-inputfield"}
                />
              </div>
            </div>
            {/* Saturated block */}
            <div className="row m-0 align-items-center">
              <div className="col-6 px-0">
                <label
                  htmlFor="foodSaturated"
                  className="p4v2-font-21-medium p4v2-mb-15"
                >
                  Saturated
                </label>
              </div>
              <div className="col-6 px-0">
                {/* Saturated */}
                <InputFieldReusable
                  labeltext=""
                  inputlabelclass="d-none"
                  imgbox="d-none"
                  name="foodSaturated"
                  type="text"
                  place="eg. 5gms"
                  onChange={this.handleOnChange}
                  value={this.state.foodSaturated}
                  inputclass={"map-inputfield"}
                />
              </div>
            </div>
            {/* Unsaturated block */}
            <div className="row m-0 align-items-center">
              <div className="col-6 px-0">
                <label
                  htmlFor="foodUnsaturated"
                  className="p4v2-font-21-medium p4v2-mb-15"
                >
                  Unsaturated
                </label>
              </div>
              <div className="col-6 px-0">
                {/* Unsaturated */}
                <InputFieldReusable
                  labeltext=""
                  inputlabelclass="d-none"
                  imgbox="d-none"
                  name="foodUnsaturated"
                  type="text"
                  place="eg. 5gms"
                  onChange={this.handleOnChange}
                  value={this.state.foodUnsaturated}
                  inputclass={"map-inputfield"}
                />
              </div>
            </div>
          </div>
          {/* fats block end */}

          {/* carbohydrates block */}
          <div className="col-6 pr-0">
            <div className="row m-0">
              <div className="col-12 px-0">
                {/* Carbohydrates */}
                <InputFieldReusable
                  labeltext="Carbohydrates"
                  inputlabelclass="input-label p4v2-font-21-black"
                  imgbox="d-none"
                  name="foodCarbohydrates"
                  type="text"
                  place="eg. 10gms"
                  onChange={this.handleOnChange}
                  value={this.state.foodCarbohydrates}
                  inputclass={"map-inputfield"}
                />
              </div>
            </div>
            {/* Dietary Fibers block */}
            <div className="row m-0 align-items-center">
              <div className="col-6 px-0">
                <label
                  htmlFor="foodDietaryFibers"
                  className="p4v2-font-21-medium p4v2-mb-15"
                >
                  Dietary Fibers
                </label>
              </div>
              <div className="col-6 px-0">
                {/* DietaryFibers */}
                <InputFieldReusable
                  labeltext=""
                  inputlabelclass="d-none"
                  imgbox="d-none"
                  name="foodDietaryFibers"
                  type="text"
                  place="eg. 5gms"
                  onChange={this.handleOnChange}
                  value={this.state.foodDietaryFibers}
                  inputclass={"map-inputfield"}
                />
              </div>
            </div>
            {/* Total Sugars block */}
            <div className="row m-0 align-items-center">
              <div className="col-6 px-0">
                <label
                  htmlFor="foodTotalSugars"
                  className="p4v2-font-21-medium p4v2-mb-15"
                >
                  Total Sugars
                </label>
              </div>
              <div className="col-6 px-0">
                {/* TotalSugars */}
                <InputFieldReusable
                  labeltext=""
                  inputlabelclass="d-none"
                  imgbox="d-none"
                  name="foodTotalSugars"
                  type="text"
                  place="eg. 5gms"
                  onChange={this.handleOnChange}
                  value={this.state.foodTotalSugars}
                  inputclass={"map-inputfield"}
                />
              </div>
            </div>
          </div>
          {/* carbohydrates block end */}
        </div>
        {/* fats and carbohydrates row end */}

        {/* Protiens, Cholestrol and Sodium fields */}
        <div className="row mx-0 p4v2-add-menu-item-form__mb-30">
          <div className="col-6 col-sm-4 pl-sm-0 col-lg-3">
            {/* Protiens */}
            <InputFieldReusable
              labeltext="Protiens"
              inputlabelclass="input-label p4v2-font-21-black"
              imgbox="d-none"
              name="foodProtiens"
              type="text"
              place="eg. 10gms"
              onChange={this.handleOnChange}
              value={this.state.foodProtiens}
              inputclass={"map-inputfield"}
            />
          </div>
          <div className="col-6 col-sm-4 col-lg-3">
            {/* Cholestrol */}
            <InputFieldReusable
              labeltext="Cholestrol"
              inputlabelclass="input-label p4v2-font-21-black"
              imgbox="d-none"
              name="foodCholestrol"
              type="text"
              place="eg. 10gms"
              onChange={this.handleOnChange}
              value={this.state.foodCholestrol}
              inputclass={"map-inputfield"}
            />
          </div>
          <div className="col-6 col-sm-4 pr-sm-0 col-lg-3">
            {/* Sodium */}
            <InputFieldReusable
              labeltext="Sodium"
              inputlabelclass="input-label p4v2-font-21-black"
              imgbox="d-none"
              name="foodSodium"
              type="text"
              place="eg. 10gms"
              onChange={this.handleOnChange}
              value={this.state.foodSodium}
              inputclass={"map-inputfield"}
            />
          </div>
        </div>
        {/* Protiens, Cholestrol and Sodium fields end */}

        {/* Select a tag (only one) */}
        <div className="row m-0">
          <h3 className="p4v2-font-21-black p4v2-pb-10">
            Select a tag &nbsp;
            <span className="p4v2-font-21-book p4v2-text-gray">(only one)</span>
          </h3>
        </div>
        <div className="row m-0">
          <button
            className={
              this.state.oneTagSelected === "Recommended"
                ? "p4v2-text-tag p4v2-text-tag--active"
                : "p4v2-text-tag"
            }
            onClick={this.handleSelectTag("Recommended")}
          >
            Recommended
          </button>
          <button
            className={
              this.state.oneTagSelected === "Chef Special"
                ? "p4v2-text-tag p4v2-text-tag--active"
                : "p4v2-text-tag"
            }
            onClick={this.handleSelectTag("Chef Special")}
          >
            Chef Special
          </button>
          <button
            className={
              this.state.oneTagSelected === "Newly Added"
                ? "p4v2-text-tag p4v2-text-tag--active"
                : "p4v2-text-tag"
            }
            onClick={this.handleSelectTag("Newly Added")}
          >
            Newly Added
          </button>
          <button
            className={
              this.state.oneTagSelected === "Today’s Special"
                ? "p4v2-text-tag p4v2-text-tag--active"
                : "p4v2-text-tag"
            }
            onClick={this.handleSelectTag("Today’s Special")}
          >
            Today’s Special
          </button>
          <button
            className={
              this.state.oneTagSelected === "Best Seller"
                ? "p4v2-text-tag p4v2-text-tag--active"
                : "p4v2-text-tag"
            }
            onClick={this.handleSelectTag("Best Seller")}
          >
            Best Seller
          </button>
          <button
            className={
              this.state.oneTagSelected === "Pocket Friendly"
                ? "p4v2-text-tag p4v2-text-tag--active"
                : "p4v2-text-tag"
            }
            onClick={this.handleSelectTag("Pocket Friendly")}
          >
            Pocket Friendly
          </button>
        </div>
        {/* Select a tag (only one) end */}
      </div>
    );
  };

  /***************************************************
   * main
   ***************************************************/
  render() {
    return (
      <div className="p4v2-add-menu-item-form">
        <h1 className="p4v2-add-menu-item-form__title">Add Item</h1>
        <form onSubmit={this.handleOnSubmit}>
          {/* Is this Item veg or non-veg? */}
          <div className="p4v2-pb-10">
            <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
              Is this Item veg or non-veg?
            </h3>
            <ToggleReusable
              name="isMenuItemVegOrNonvegToggle"
              currentState={this.state.isMenuItemVegOrNonvegToggle}
              spantext1={"Veg"}
              spantext2={"Non-veg"}
              toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={this.toggleFunction}
            />
          </div>
          {/* Item Details */}
          {this.renderFoodItemDetailsBlock()}
          {/* SpecificTimingsBlock */}
          {this.renderSpecificTimingsBlock()}
          {/* FoodDetailsBlock */}
          {this.renderFoodDetailsBlock()}
          {/* TogglesAndTextBlock */}
          {this.renderTogglesAndTextBlock()}
          {/* NutritionInfoBlock */}
          {this.renderNutritionInfoBlock()}
          {/* Buttons */}
          <div className="text-right">
            <Link to="/viewvendordetails">
              <button className="p4v2-menu-slide-edit-btn p4v2-add-menu-item-form__mr-30">
                back
              </button>
            </Link>
            <button
              type="submit"
              className="p4v2-menu-slide-edit-btn p4v2-menu-slide-edit-btn--bg-orange"
            >
              save
            </button>
          </div>
          {/* Buttons end */}
        </form>
      </div>
    );
  }
}

MainViewDetailsMenuEditItemForm.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  icons: state.icons,
  errors: state.errors
});

export default connect(mapStateToProps, { getALLICONS, edit_menu_item })(
  withRouter(MainViewDetailsMenuEditItemForm)
);

const Restauranttiming = ({ state, toggleFunction }) => {
  return (
    <>
      <div className="cuisine-main">
        <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
          Do you have specific timings for the item?
        </h3>
        <ToggleReusable
          name="isSpecificTimingToggle"
          currentState={state.isSpecificTimingToggle}
          spantext1={"Yes"}
          spantext2={"No"}
          toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
          toggleinputclass={"toggle__switch ml-3 mr-3"}
          onChange={toggleFunction}
        />

        {state.isSpecificTimingToggle && (
          <>
            <h3 className="p4v2-add-menu-item-form__block-title p4v2-pb-10">
              Do you have multiple opening timings ?
            </h3>
            <ToggleReusable
              name="multiple_opening_time"
              currentState={state.multiple_opening_time}
              spantext1={"Yes"}
              spantext2={"No"}
              toggleclass={"toggle d-flex align-items-center mb-2"}
              toggleinputclass={"toggle__switch ml-3 mr-3"}
              onChange={toggleFunction}
            />
          </>
        )}
      </div>
    </>
  );
};

const NoMultipleTime = ({
  state,
  onDayOpenCloseHanlder,
  onTimeSelectHandlerSingle
}) => {
  return (
    <>
      <div className="cuisine-main p4v2-add-menu-item-form__mb-30">
        {dayArray.map((day, index) => (
          <table key={index} style={{ borderRadius: 0, width: "100%" }}>
            <tbody>
              <tr className="p4v2-timing-table-border-bottom">
                <td>
                  <p className="p4v2-add-menu-item-form__block-title p4v2-pb-20">
                    {day}
                  </p>
                  <ToggleReusable
                    name={day}
                    currentState={state[day].open}
                    spantext1={"Open"}
                    spantext2={"Close"}
                    toggleclass={"toggle d-flex align-items-center"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                  />
                </td>
                <td style={{ width: "50%" }}>
                  {/* OPEN AND CLOSE TIME */}
                  <p className="col-12 p-0 p4v2-font-24-medium">Opening Time</p>
                  <div className="opening_time_selector opening_time_selector--p4v2">
                    {state[day].open ? (
                      <>
                        <select
                          name="main_opening_time"
                          onChange={onTimeSelectHandlerSingle(day)}
                          className="Selection_box Selection_box--p4v2"
                          value={state[day].main_opening_time}
                        >
                          {timeArray.map((time, index) => (
                            <option key={index} value={time}>
                              {time}
                            </option>
                          ))}
                        </select>
                        <div className="dasheds">-</div>
                        <select
                          name="main_closing_time"
                          onChange={onTimeSelectHandlerSingle(day)}
                          className="Selection_box Selection_box--p4v2"
                          value={state[day].main_closing_time}
                        >
                          {timeArray.map((time, index) => (
                            <option key={index} value={time}>
                              {time}
                            </option>
                          ))}
                        </select>
                      </>
                    ) : (
                      <>
                        <select
                          name="main_opening_time"
                          onChange={onTimeSelectHandlerSingle(day)}
                          className="Selection_box Selection_box--p4v2"
                          disabled
                        >
                          {timeArray.map((time, index) => (
                            <option key={index} value={time}>
                              {time}
                            </option>
                          ))}
                        </select>
                        <div className="dasheds">-</div>
                        <select
                          name="main_closing_time"
                          onChange={onTimeSelectHandlerSingle(day)}
                          className="Selection_box Selection_box--p4v2"
                          disabled
                        >
                          {timeArray.map((time, index) => (
                            <option key={index} value={time}>
                              {time}
                            </option>
                          ))}
                        </select>
                      </>
                    )}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};

const YesMultipleTime = ({
  state,
  onDayOpenCloseHanlder,
  onFoodTypeOpenClose,
  onTimeSelectHandlerMultiple
}) => {
  return (
    <>
      <div className="cuisine-main p4v2-add-menu-item-form__mb-30">
        {dayArray.map((day, index) => (
          <table key={index} style={{ borderRadius: 0, width: "100%" }}>
            <tbody>
              <tr className="p4v2-timing-table-border-bottom">
                <td style={{ width: "18%" }}>
                  <p className="p4v2-add-menu-item-form__block-title p4v2-pb-20">
                    {day}
                  </p>
                  <ToggleReusable
                    name={day}
                    currentState={state[day].open}
                    spantext1={"Open"}
                    spantext2={"Close"}
                    toggleclass={"toggle d-flex align-items-center"}
                    toggleinputclass={"toggle__switch ml-3 mr-3"}
                    onChange={onDayOpenCloseHanlder(day)}
                  />
                </td>
                {foodTime.map((food, index) => (
                  <td key={index} className="p4v2-yes-multiple-td">
                    <div className="row m-0">
                      {/* OPEN AND CLOSE TIME */}
                      <span className="col-7 p-0 p4v2-font-24-medium">
                        {food}
                      </span>
                      <div className="col-5 p-0 text-right">
                        {state[day].open ? (
                          <ToggleReusable
                            name="open"
                            currentState={state[day][food].open}
                            spantext1={""}
                            spantext2={""}
                            toggleclass={
                              "toggle d-flex align-items-center p4v2-pb-20"
                            }
                            toggleinputclass={"toggle__switch ml-3 mr-3"}
                            onChange={onFoodTypeOpenClose(day, food)}
                          />
                        ) : (
                          <ToggleReusable
                            // name="open"
                            currentState={false}
                            spantext1={""}
                            spantext2={""}
                            toggleclass={
                              "toggle d-flex align-items-center p4v2-pb-20"
                            }
                            toggleinputclass={"toggle__switch ml-3 mr-3"}
                            // onChange={onFoodTypeOpenClose(day, food)}
                            disabled
                          />
                        )}
                      </div>
                    </div>
                    <div className="opening_time_selector">
                      {state[day].open && state[day][food].open ? (
                        <>
                          <select
                            name="opening_time"
                            onChange={onTimeSelectHandlerMultiple(day, food)}
                            className="Selection_box Selection_box--p4v2"
                            value={state[day][food].opening_time}
                          >
                            {timeArray.map((time, index) => (
                              <option key={index} value={time}>
                                {time}
                              </option>
                            ))}
                          </select>
                          <div className="dasheds">-</div>
                          <select
                            name="closing_time"
                            onChange={onTimeSelectHandlerMultiple(day, food)}
                            className="Selection_box Selection_box--p4v2"
                            value={state[day][food].closing_time}
                          >
                            {timeArray.map((time, index) => (
                              <option key={index} value={time}>
                                {time}
                              </option>
                            ))}
                          </select>
                        </>
                      ) : (
                        <>
                          <select
                            name="opening_time"
                            // onChange={onTimeSelectHandlerMultiple(day, food)}
                            className="Selection_box Selection_box--p4v2"
                            disabled
                          >
                            {timeArray.map((time, index) => (
                              <option
                                key={index}
                                value={time}
                                className="p4v2-text-gray"
                              >
                                {time}
                              </option>
                            ))}
                          </select>
                          <div className="dasheds">-</div>
                          <select
                            name="closing_time"
                            // onChange={onTimeSelectHandlerMultiple(day, food)}
                            className="Selection_box Selection_box--p4v2"
                            disabled
                          >
                            {timeArray.map((time, index) => (
                              <option
                                key={index}
                                value={time}
                                className="p4v2-text-gray"
                              >
                                {time}
                              </option>
                            ))}
                          </select>
                        </>
                      )}
                    </div>
                  </td>
                ))}
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </>
  );
};
