import React, { Component } from "react";
import { AdminNavbar } from "./../../../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import { MainViewDetailsMenuAddCategoryForm } from "./MainViewDetailsMenuAddCategoryForm";

export class MainViewDetailsMenuAddCategory extends Component {
  render() {
    return (
      <>
        <AdminNavbar />
        <MainViewDetailsMenuAddCategoryForm />
      </>
    );
  }
}

export default MainViewDetailsMenuAddCategory;
