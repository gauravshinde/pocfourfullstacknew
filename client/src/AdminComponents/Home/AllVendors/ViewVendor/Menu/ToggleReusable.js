import React from "react";
import { PropTypes } from "prop-types";

const ToggleReusable = ({
  name,
  toggleclass,
  toggleinputclass,
  spantext1,
  spantext2,
  currentState,
  onChange,
  disabled
}) => {
  return (
    <React.Fragment>
      <div className={toggleclass}>
        <span>{currentState ? <b>{spantext1}</b> : spantext1}</span>
        {disabled ? (
          <input
            type={"checkbox"}
            className={`${toggleinputclass}`}
            defaultChecked={currentState}
            disabled
          />
        ) : (
          <input
            type={"checkbox"}
            name={name}
            className={`${toggleinputclass}`}
            onChange={onChange}
            defaultChecked={currentState}
          />
        )}
        <span>{currentState ? spantext2 : <b>{spantext2}</b>}</span>
      </div>
    </React.Fragment>
  );
};

ToggleReusable.defaultProps = {
  disabled: false
};

ToggleReusable.propTypes = {
  disabled: PropTypes.bool
};

export default ToggleReusable;
