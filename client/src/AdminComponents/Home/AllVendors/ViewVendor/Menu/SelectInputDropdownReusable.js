import React from "react";

const SelectInputDropdownReusable = ({
  id,
  name,
  value,
  onInputChangeHandler,
  dropDownToggler,
  dropDown,
  suggestionList,
  dropDownSelect,
  placeholder
}) => {
  return (
    <div className="main-dropdown-class" id={id}>
      <span onClick={dropDownToggler}>
        <i className="fa fa-sort-down" aria-hidden="true"></i>
      </span>
      <input
        type="text"
        className="dropdown-input-class"
        placeholder={placeholder}
        onFocus={dropDownToggler}
        name={name}
        value={value}
        onChange={onInputChangeHandler}
        autoComplete="off"
      />
      <div className="dropdown-list">
        {dropDown ? (
          <ul className="dropdown-ul">
            {suggestionList.map((element, index) => (
              <li
                key={index}
                onClick={dropDownSelect(element)}
                className={
                  element === value ? "dropdown-li_selected" : "dropdown-li"
                }
              >
                {element}
              </li>
            ))}
          </ul>
        ) : null}
      </div>
    </div>
  );
};

export default SelectInputDropdownReusable;
