import React, { Component } from "react";
import { AdminNavbar } from "../../../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import MainViewDetailsMenuAddItemForm from "./MainViewDetailsMenuAddItemForm";

class MainViewDetailsMenuAddItem extends Component {
  render() {
    return (
      <>
        <AdminNavbar />
        <MainViewDetailsMenuAddItemForm />
      </>
    );
  }
}

export default MainViewDetailsMenuAddItem;
