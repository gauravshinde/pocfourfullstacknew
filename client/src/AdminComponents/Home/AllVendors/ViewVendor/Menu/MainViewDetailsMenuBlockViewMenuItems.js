import React, { Component } from "react";
// api
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { menu_get_items } from "../../../../../store/actions/SuperAdminAllAction/menu/MenuAllGetActions";
import isEmpty from "../../../../../store/validation/is-Empty";

import Pagination from "rc-pagination";
import MainViewDetailsMenuRightSlideReusable from "./MainViewDetailsMenuRightSlideReusable";

// pagination
const totalRecordsInOnePage = 8;

class MainViewDetailsMenuBlockViewMenuItems extends Component {
  constructor() {
    super();
    this.state = {
      isRightSlideOpen: false,
      currentPagination: 1,
      // api
      getItemsList: [],
      specificMenuLocal: []
    };
  }
  /*
   *  lifecycle methods
   */
  componentDidMount() {
    this.props.menu_get_items();
  }

  // api
  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.getMenuItems !== nextState.getItemsList) {
      return {
        getItemsList: nextProps.getMenuItems.menuItems
      };
    }
    return null;
  }

  /*
   * handlers
   */

  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
    console.log(e.target.name, e.target.checked);
  };

  handleOpenRightSlideBlock = data => e => {
    e.preventDefault();
    this.setState({
      isRightSlideOpen: true,
      specificMenuLocal: data
    });
  };

  handleCloseRightSlideBlock = () => {
    this.setState({
      isRightSlideOpen: false
    });
  };

  onChangePagination = page => {
    console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  /*
   * renderSearchList
   */

  renderSearchList = () => {
    const { getItemsList } = this.state;
    let searchList = [];
    if (!isEmpty(getItemsList)) {
      if (this.props.search) {
        let search = new RegExp(this.props.search, "i");
        searchList = getItemsList.filter(getall => {
          if (search.test(getall.item_name)) {
            return getall;
          }
          if (search.test(getall.item_code)) {
            return getall;
          }
          if (search.test(getall.category)) {
            return getall;
          }
          if (search.test(getall.select_tag)) {
            return getall;
          }
          if (search.test(getall.processing_time)) {
            return getall;
          }
        });
        return searchList;
      }
      return getItemsList;
    }
  };

  /*
   * renderMenuCard
   */
  renderMenuCard = () => {
    const { getItemsList, currentPagination } = this.state;
    return (
      <>
        {!isEmpty(getItemsList) &&
          this.renderSearchList().map(
            (data, index) =>
              index >= (currentPagination - 1) * totalRecordsInOnePage &&
              index < currentPagination * totalRecordsInOnePage && (
                <div
                  key={index}
                  className="p4v2-menu-block-view-menu-item__block"
                  onClick={this.handleOpenRightSlideBlock(data)}
                >
                  <div className="p4v2-menu-block-view-menu-item__img">
                    <img
                      // src={require("../../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/menu-item-block-view.png")}
                      src={data.photo}
                      alt="food"
                      className="p4v2-menu-block-view-menu-item__img"
                    />
                  </div>
                  <div className="p4v2-menu-block-view-menu-item__text-block">
                    {/* column1 */}
                    <div>
                      <h5 className="p4v2-menu-block-view-menu-item__text-block-title">
                        {data.category}
                      </h5>
                      <h5 className="p4v2-menu-block-view-menu-item__text-medium">
                        Rs{" "}
                        <span className="p4v2-menu-block-view-menu-item__text-black">
                          {data.item_price}
                        </span>
                      </h5>
                    </div>
                    {/* column2 */}
                    <div>
                      <h5 className="p4v2-menu-block-view-menu-item__text-black">
                        Serves:{" "}
                      </h5>
                      <h5 className="p4v2-menu-block-view-menu-item__text-medium">
                        {data.serving_size} People
                      </h5>
                    </div>
                  </div>
                </div>
              )
          )}
      </>
    );
  };
  render() {
    return (
      <>
        {this.state.isRightSlideOpen && (
          <MainViewDetailsMenuRightSlideReusable
            handleCloseRightSlideBlock={this.handleCloseRightSlideBlock}
            specificMenuLocal={this.state.specificMenuLocal}
          />
        )}

        {this.state.getItemsList.length === undefined ||
        isEmpty(this.renderSearchList()) ? (
          <p className="p4v2-font-21-medium">Empty</p>
        ) : (
          <>
            <div className="p4v2-menu-block-view-menu-item">
              {this.renderMenuCard()}
            </div>
            <div className="p4v2-menu-pagination">
              <Pagination
                onChange={this.onChangePagination}
                current={this.state.currentPagination}
                defaultPageSize={totalRecordsInOnePage}
                total={this.state.getItemsList.length}
                showTitle={false}
              />
            </div>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  getMenuItems: state.menuAllGetReduers
});

export default connect(mapStateToProps, { menu_get_items })(
  withRouter(MainViewDetailsMenuBlockViewMenuItems)
);
