import React from "react";
import { Link } from "react-router-dom";
import isEmpty from "../../../../../store/validation/is-Empty";
import ToggleReusable from "./ToggleReusable";

const MainViewDetailsMenuRightSlideReusable = ({
  handleCloseRightSlideBlock,
  specificMenuLocal
}) => {
  /*
   * renderContent1
   */
  const renderContent1 = specificMenuLocal => {
    return (
      <div className="row m-0 p4v2-right-slide-block__table1">
        {/* {console.log(specificMenuLocal)} */}
        <div className="col-4">
          <img
            src={specificMenuLocal.photo}
            alt="burger"
            className="p4v2-right-slide-block__table1-img"
          />
        </div>
        <div className="col-8">
          <h1 className="p4v2-right-slide-block__table1-title">
            {specificMenuLocal.item_name}
          </h1>
          <div className="row">
            <div className="col-6">
              <p className="p4v2-right-slide-block__table1-title">
                Price:{" "}
                <span className="p4v2-right-slide-block__table1-title-medium">
                  Rs. {specificMenuLocal.item_price}
                </span>
              </p>
              <p className="p4v2-right-slide-block__table1-title">
                Code:{" "}
                <span className="p4v2-right-slide-block__table1-title-medium">
                  {specificMenuLocal.item_code}
                </span>
              </p>
            </div>
            <div className="col-6">
              <p className="p4v2-right-slide-block__table1-title-medium">
                {specificMenuLocal.category}
              </p>
              <p className="p4v2-right-slide-block__table1-title">
                Serves:{" "}
                <span className="p4v2-right-slide-block__table1-title-medium">
                  {specificMenuLocal.serving_size} {specificMenuLocal.item_unit}
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  /*
   * renderContent2
   */
  const renderContent2 = specificMenuLocal => {
    return (
      <div className="col-12 px-0 p4v2-right-slide-block-content2">
        <p className="p4v2-right-slide-block-content2-desc">
          {specificMenuLocal.description}
        </p>
      </div>
    );
  };

  /*
   * renderContent3
   */
  const renderContent3 = specificMenuLocal => {
    return (
      <div className="row m-0 p4v2-right-slide-block-content3">
        <div className="col-5 col-lg-4 px-0">
          <p className="p4v2-right-slide-block__table1-title">Availability</p>
          <ToggleReusable
            currentState={specificMenuLocal.item_auto_accept}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={
              "toggle d-flex align-items-center toggle--p4v2-right-slide-block"
            }
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            disabled={true}
          />
        </div>
        <div className="col-5 col-lg-4 px-0">
          <p className="p4v2-right-slide-block__table1-title">
            Processing Time
          </p>
          <p className="p4v2-right-slide-block__table1-title-medium">
            {specificMenuLocal.processing_time} Mins
          </p>
        </div>
        <div className="col-2 col-lg-4">
          <p className="p4v2-right-slide-block__table1-title">Tags</p>
          <p className="p4v2-right-slide-block__table1-title-medium">
            {specificMenuLocal.select_tag}
          </p>
        </div>
      </div>
    );
  };

  /*
   * renderContent4
   */
  const renderContent4 = specificMenuLocal => {
    return (
      <div className="row m-0 p4v2-right-slide-block-content3">
        <div className="col-12 px-0">
          <p className="p4v2-right-slide-block__table1-title">
            Nutritional Info
          </p>
        </div>
        <table className="p4v2-right-slide-block-content4-table">
          <tbody>
            <tr>
              <th>Fat</th>
              <td>{specificMenuLocal.nutrition_info.Fat.Fat} gm</td>
            </tr>
            <tr>
              <td>Saturated</td>
              <td>{specificMenuLocal.nutrition_info.Fat.Saturated} gm</td>
            </tr>
            <tr>
              <td>Unsaturated</td>
              <td>{specificMenuLocal.nutrition_info.Fat.UnSaturated} gm</td>
            </tr>
            <tr>
              <th>Carbohydrates</th>
              <td>
                {specificMenuLocal.nutrition_info.Carbohydrates.Carbohydrates}{" "}
                gm
              </td>
            </tr>
            <tr>
              <td>Dietary Fibers</td>
              <td>{specificMenuLocal.nutrition_info.Carbohydrates.Fiber} gm</td>
            </tr>
            <tr>
              <td>Total Sugars</td>
              <td>{specificMenuLocal.nutrition_info.Carbohydrates.Suger} gm</td>
            </tr>
            <tr>
              <th>Protiens</th>
              <td>{specificMenuLocal.nutrition_info.Protiens} gm</td>
            </tr>
            <tr>
              <th>Cholestrol</th>
              <td>{specificMenuLocal.nutrition_info.Cholestrol} mg</td>
            </tr>
            <tr>
              <th>Sodium</th>
              <td>{specificMenuLocal.nutrition_info.Sodium} mg</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  };

  /*
   * renderContent5
   */
  const renderContent5 = specificMenuLocal => {
    return (
      <div className="row m-0 p4v2-right-slide-block-content5">
        <div className="col-6 px-0">
          <p className="p4v2-right-slide-block__table1-title">
            Calorific Value
          </p>
          <p className="p4v2-right-slide-block__table1-title-medium-mb-30">
            {specificMenuLocal.calorific_value} cal.
          </p>
        </div>
        <div className="col-6 px-0">
          <p className="p4v2-right-slide-block__table1-title">Type</p>
          {!isEmpty(specificMenuLocal.food_type) &&
            specificMenuLocal.food_type.map((val, index) => (
              <p
                className="p4v2-right-slide-block__table1-title-medium-mb-30"
                key={index}
              >
                {val.title}
              </p>
            ))}
        </div>
        <div className="col-6 px-0">
          <p className="p4v2-right-slide-block__table1-title">Spice Level</p>
          <p className="p4v2-right-slide-block__table1-title-medium-mb-30">
            {specificMenuLocal.level_of_spiciness}
          </p>
        </div>
        <div className="col-6 px-0">
          <p className="p4v2-right-slide-block__table1-title">Allergy Info</p>
          {!isEmpty(specificMenuLocal.allergy_info) &&
            specificMenuLocal.allergy_info.map((val, index) => (
              <p
                className="p4v2-right-slide-block__table1-title-medium-mb-30"
                key={index}
              >
                {val.title}
              </p>
            ))}
        </div>
      </div>
    );
  };

  /*
   * renderContent6
   */
  const renderContent6 = specificMenuLocal => {
    return (
      <div className="row m-0 p4v2-right-slide-block-content6">
        <div className="col-12 px-0">
          <p className="p4v2-right-slide-block__table1-title">
            Available Timing
          </p>
          {!isEmpty(specificMenuLocal.item_available_for) &&
            specificMenuLocal.item_available_for.map((val, index) => (
              <p
                className="p4v2-right-slide-block__table1-title-medium"
                key={index}
              >
                {val}
              </p>
            ))}
        </div>
      </div>
    );
  };

  return (
    <div className="p4v2-right-slide-block-overlay poc4v2-menu-block-scrollbar">
      <div className="p4v2-right-slide-block">
        {/* close */}
        <span onClick={handleCloseRightSlideBlock}>
          <img
            src={require("../../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/close-menu-icon.svg")}
            alt="close"
            className="p4v2-menu-slide-icon-close"
          />
        </span>

        {/* content */}
        {renderContent1(specificMenuLocal)}
        {renderContent2(specificMenuLocal)}
        <div className="p4v2-menu-slide-overflow-div">
          {renderContent3(specificMenuLocal)}
          {renderContent4(specificMenuLocal)}
          {renderContent5(specificMenuLocal)}
          {renderContent6(specificMenuLocal)}
        </div>
        <div className="col-12 text-center">
          <Link
            to={{
              pathname: "/editMenuItem",
              propData: {
                data: specificMenuLocal
              }
            }}
          >
            <button className="p4v2-menu-slide-edit-btn">Edit</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default MainViewDetailsMenuRightSlideReusable;
