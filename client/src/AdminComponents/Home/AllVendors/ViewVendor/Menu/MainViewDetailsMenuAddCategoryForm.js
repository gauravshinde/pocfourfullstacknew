import React, { Component } from "react";
import InputFieldReusable from "./InputFieldReusable";
import TextareaReusable from "./TextareaReusable";
import ToggleReusable from "./ToggleReusable";
import { Link } from "react-router-dom";
// import AddSubCategoryPopup from "./MainViewDetailsMenuPopUps/AddSubCategoryPopup";
// import ButtonYellowTagReusable from "./ButtonYellowTagReusable";
// import PrivateRoute from "./../../../../../PocTwo/store/utils/PrivateRoute";

export class MainViewDetailsMenuAddCategoryForm extends Component {
  constructor() {
    super();
    this.state = {
      // menu category details fold
      foodCategoryName: "",
      foodDescription: "",
      // renderTogglesAndTextBlock
      isTaxesToggle: true,
      isTaxesPrice: "",
      isPackingToggle: false,
      isPackingPrice: "",
      isCustomisableToggle: false
    };
  }

  /***************************************************
   * handlers
   ***************************************************/
  handleOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  toggleFunction = e => {
    this.setState({
      [e.target.name]: e.target.checked
    });
    console.log(e.target.name, e.target.checked);
  };

  handleAddCategoryOnSubmit = e => {
    e.preventDefault();
    console.log(this.state);
  };

  /***************************************************
   * renderFoodCategoryDetailsBlock
   ***************************************************/
  renderFoodCategoryDetailsBlock = () => {
    return (
      <>
        <div className="p4v2-add-menu-category-form__mb-30">
          <h3 className="p4v2-add-menu-category-form__block-title p4v2-pb-10">
            Category Details
          </h3>
          {/* Category Name */}
          <InputFieldReusable
            labeltext="Category Name"
            inputlabelclass="input-label"
            imgbox="d-none"
            name="foodCategoryName"
            type="text"
            place="eg. Desert"
            onChange={this.handleOnChange}
            value={this.state.foodCategoryName}
            inputclass={"map-inputfield"}
          />

          {/* Description */}
          <TextareaReusable
            labeltext={"Description"}
            inputlabelclass={"input-label"}
            name={"foodDescription"}
            type={"text"}
            onChange={this.handleOnChange}
            value={this.state.foodDescription}
            place={"eg. Ingredients"}
            Textareaclass={
              "form-control textarea-custom p4v2-add-menu-item-textarea"
            }
          />

          {/* Cross Selection Category */}
          <div className="cross-selection-category-section">
            <span>Cross Selection Category </span>
            <div className="row">
              <div className="col-md-3">
                <button type="button" className="cross-selection-buttons">
                  <img
                    src={require("../../../../../assets/images/button-icons/burger.png")}
                    alt="button-icon"
                    className="img-fluid cross-selection-button-icon"
                  />
                  Pizzas
                </button>
              </div>
              <div className="col-md-3">
                <button type="button" className="cross-selection-buttons">
                  <img
                    src={require("../../../../../assets/images/button-icons/dining.png")}
                    alt="button-icon"
                    className="img-fluid cross-selection-button-icon"
                  />
                  Soups
                </button>
              </div>
              <div className="col-md-3">
                <button type="button" className="cross-selection-buttons">
                  <img
                    src={require("../../../../../assets/images/button-icons/coffee.png")}
                    alt="button-icon"
                    className="img-fluid cross-selection-button-icon"
                  />
                  Appetizers
                </button>
              </div>
              <div className="col-md-3">
                <button type="button" className="cross-selection-buttons">
                  <img
                    src={require("../../../../../assets/images/button-icons/coffee.png")}
                    alt="button-icon"
                    className="img-fluid cross-selection-button-icon"
                  />
                  Pastas
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  /***************************************************
   * renderTogglesAndTextBlock
   ***************************************************/
  renderTogglesAndTextBlock = () => {
    return (
      <div>
        {/* Taxes */}
        <div className="p4v2-pb-10">
          <h3 className="p4v2-add-menu-category-form__block-title p4v2-pb-10">
            Taxes
          </h3>
          <ToggleReusable
            name="isTaxesToggle"
            currentState={this.state.isTaxesToggle}
            spantext1={"Category Level"}
            spantext2={"General Level"}
            toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
          />
          {this.state.isTaxesToggle && (
            <div className="col-5 col-md-4 col-lg-3 px-0 d-flex align-items-center">
              {/* Enter Price */}
              <div>
                <InputFieldReusable
                  labeltext="Enter Tax"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="isTaxesPrice"
                  type="text"
                  place="eg. 20"
                  onChange={this.handleOnChange}
                  value={this.state.isTaxesPrice}
                  inputclass={"map-inputfield"}
                />
              </div>
              <div className="ml-3">
                <i className="fa fa-percent" aria-hidden="true"></i>
              </div>
            </div>
          )}
        </div>

        {/* Packing Charges */}
        <div className="p4v2-pb-10">
          <h3 className="p4v2-add-menu-category-form__block-title p4v2-pb-10">
            Packing Charges
          </h3>
          <ToggleReusable
            name="isPackingToggle"
            currentState={this.state.isPackingToggle}
            spantext1={"Category Level"}
            spantext2={"General Level"}
            toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
          />
          {this.state.isPackingToggle && (
            <div className="col-5 col-md-4 col-lg-3 px-0 d-flex align-items-center">
              {/* Enter Price */}
              <div>
                <InputFieldReusable
                  labeltext="Enter Price"
                  inputlabelclass="input-label"
                  imgbox="d-none"
                  name="isPackingPrice"
                  type="text"
                  place="eg. 20"
                  onChange={this.handleOnChange}
                  value={this.state.isPackingPrice}
                  inputclass={"map-inputfield"}
                />
              </div>

              <div className="ml-3">
                <i className="fa fa-inr" aria-hidden="true"></i>
              </div>
            </div>
          )}
        </div>

        {/* Is this category customisable ? */}
        {/* <div className="p4v2-pb-10">
          <h3 className="p4v2-add-menu-category-form__block-title p4v2-pb-10">
            Is this category customisable ?
          </h3>
          <ToggleReusable
            name="isCustomisableToggle"
            currentState={this.state.isCustomisableToggle}
            spantext1={"Yes"}
            spantext2={"No"}
            toggleclass={"toggle d-flex align-items-center p4v2-pb-20"}
            toggleinputclass={"toggle__switch ml-3 mr-3"}
            onChange={this.toggleFunction}
          />
          {this.state.isCustomisableToggle && (
            <>
              <div className="col-12 col-md-12 col-lg-12 d-flex justify-content-between px-0">
                <h3 className="p4v2-add-menu-category-form__block-title p4v2-pb-10">
                  Subcategories
                </h3>
                <AddSubCategoryPopup />
              </div>
              <div className="row mb-3">
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    labeltext="Rice"
                    inputlabelclass="label-class"
                    buttontext="White Rice"
                    tagtext="Rs 100"
                  />
                </div>
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    buttontext="Brown Rice"
                    tagtext="Rs 100"
                  />
                </div>
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    buttontext="Jeera Rice"
                    tagtext="Rs 100"
                  />
                </div>
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    buttontext="Basmati Rice"
                    tagtext="Rs 100"
                  />
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    labeltext="Add-ons"
                    inputlabelclass="label-class"
                    buttontext="Chicken"
                    tagtext="Rs 100"
                  />
                </div>
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    buttontext="Mutton"
                    tagtext="Rs 100"
                  />
                </div>
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    buttontext="Cheese"
                    tagtext="Rs 100"
                  />
                </div>
                <div className="col-md-3">
                  <ButtonYellowTagReusable
                    buttontext="Chicken Tikka"
                    tagtext="Rs 100"
                  />
                </div>
              </div>
            </>
          )}
        </div> */}
      </div>
    );
  };

  /***************************************************
   * main
   ***************************************************/
  render() {
    return (
      <React.Fragment>
        <div className="p4v2-add-menu-category-form">
          <h1 className="p4v2-add-menu-category-form__title">Add Category</h1>
          <form onSubmit={this.handleAddCategoryOnSubmit}>
            {/* Category Details */}
            {this.renderFoodCategoryDetailsBlock()}

            {/* TogglesAndTextBlock */}
            {this.renderTogglesAndTextBlock()}

            {/* Buttons */}
            <div className="text-right">
              <Link to="/viewvendordetails">
                <button className="p4v2-menu-slide-edit-btn p4v2-add-menu-item-form__mr-30">
                  Cancel
                </button>
              </Link>
              <button
                type="submit"
                className="p4v2-menu-slide-edit-btn p4v2-menu-slide-edit-btn--bg-orange"
              >
                Save
              </button>
            </div>
            {/* Buttons end */}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default MainViewDetailsMenuAddCategoryForm;
