import React, { Component } from "react";
import Pagination from "rc-pagination";

const totalRecordsInOnePage = 8;

class MainViewDetailsMenuBlockViewCategories extends Component {
  constructor() {
    super();
    this.state = {
      currentPagination: 1
    };
  }

  /*
   * handlers
   */

  onChangePagination = page => {
    console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  /*
   * renderMenuCard
   */
  renderMenuCard = () => {
    return (
      <div className="p4v2-categories-block-view">
        <div className="p4v2-categories-block-view__titleblock">
          <h3 className="p4v2-font-24-black">Main Course</h3>
          <img
            src={require("../../../../../../src/assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/edit-icon-table.svg")}
            alt="customize"
            className="p4v2-categories-edit-block-view__icon"
          />
        </div>
        <div className="p4v2-pb-20">
          <h4 className="p4v2-font-21-black">Cross Selection Category:</h4>
          <p className="p4v2-font-21-medium"> None</p>
        </div>
        <div className="p4v2-pb-20">
          <h4 className="p4v2-font-21-black">Sub Categories:</h4>
          <p className="p4v2-font-21-medium"> 4</p>
        </div>
        <div className="p4v2-pb-20 p4v2-categories-block-view__lastblock">
          <div className="p4v2-pb-20 p4v2-categories-block-view__lastblock-mr">
            <h4 className="p4v2-font-21-black">Tax Level: </h4>
            <p className="p4v2-font-21-medium"> Category Level</p>
          </div>
          <div>
            <h4 className="p4v2-font-21-black">Tax Charge: </h4>
            <p className="p4v2-font-21-medium"> ₹ 120</p>
          </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <>
        <div className="p4v2-menu-block-view-menu-item">
          {this.renderMenuCard()}
          {this.renderMenuCard()}
          {this.renderMenuCard()}
          {this.renderMenuCard()}
          {this.renderMenuCard()}
          {this.renderMenuCard()}
        </div>
        <div className="p4v2-menu-pagination">
          <Pagination
            onChange={this.onChangePagination}
            current={this.state.currentPagination}
            defaultPageSize={totalRecordsInOnePage}
            total={40}
            showTitle={false}
          />
        </div>
      </>
    );
  }
}

export default MainViewDetailsMenuBlockViewCategories;
