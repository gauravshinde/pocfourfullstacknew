import React, { Component } from 'react';
import AdminNavbar from '../../../AdminReusableComponent/NavBarComponent/AdminNavbar';
import SubHeaderVendor from '../SubHeaderVendor';

export class MainViewVendorDetail extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <SubHeaderVendor
          img={require('../../../../assets/images/AdminDashboard/home/vendorimage.png')}
          vendorName='McDonalds'
          vendrorPara='View info'
        />
      </React.Fragment>
    );
  }
}

export default MainViewVendorDetail;
