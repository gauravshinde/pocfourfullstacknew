import React, { Component } from "react";
import AdminNavbar from "../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import SearchboxName from "../../AdminReusableComponent/SearchBoxandPageName/SearchboxName";
import HeaderTableUser from "./../../AdminReusableComponent/AllTableReusableComponent/AllUsersPageTable/HeaderTableUser";
import BodyTableUser from "./../../AdminReusableComponent/AllTableReusableComponent/AllUsersPageTable/BodyTableUser";

import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
//import dateFns from "date-fns";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
// import { PropTypes } from 'prop-types';
import isEmpty from "./../../../store/validation/is-Empty";
import { superAdmin_get_users_details } from "./../../../store/actions/SuperAdminAllAction/SuperAdminAllGetActions";
import {
  lockUser,
  getUserOrder,
} from "../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";

const totalRecordsInOnePage = 10;

export class MainAllUsersComponent extends Component {
  constructor() {
    super();
    this.state = {
      allUsersDetailsdata: {},
      Userviewdetailpage: "",
      activebtn: false,
      pageNo: "1",
      search: "",
      currentPagination: 1,
    };
  }

  componentDidMount() {
    this.props.superAdmin_get_users_details();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.superAdminGetDetails);
    if (nextProps.superAdminGetDetails !== nextState.allUsersDetailsdata) {
      return {
        allUsersDetailsdata:
          nextProps.superAdminGetDetails.SuperAdminUserDetails,
      };
    }
    return null;
  }

  onSearchChange = (e) => {
    this.setState({
      search: e.target.value,
    });
  };

  onChangePagination = (page) => {
    // console.log(page);
    this.setState({
      currentPagination: page,
    });
  };

  // paginationbtn = no => e => {
  //   console.log("data");
  //   this.setState({
  //     pageNo: no
  //   });
  // };

  onClickUserLock = (value) => (e) => {
    const formData = {
      user_id: value._id,
      user_verified:
        value.user_verified === true
          ? false
          : "" || value.user_verified === false
          ? true
          : "",
    };
    this.props.lockUser(formData);
  };

  onClickUsersViewDetails = (value) => (e) => {
    this.props.history.push({
      pathname: "/viewuserdetails",
      state: { Userviewdetailpage: value },
    });
  };

  renderTableBody = () => {
    const { allUsersDetailsdata, currentPagination } = this.state;
    // console.log(allUsersDetailsdata);
    let data = [];
    let Usersfiltereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      Usersfiltereddata = allUsersDetailsdata.filter((getall) => {
        if (search.test(getall.first_name)) {
          return getall;
        }
        if (search.test(getall.email)) {
          return getall;
        }
        if (search.test(getall.mobile_number)) {
          return getall;
        }
      });
      // console.log(Usersfiltereddata);
    } else {
      Usersfiltereddata = allUsersDetailsdata;
    }
    if (!isEmpty(Usersfiltereddata)) {
      return Usersfiltereddata.map(
        (data, index) =>
          index >= (currentPagination - 1) * totalRecordsInOnePage &&
          index < currentPagination * totalRecordsInOnePage && (
            <React.Fragment key={index}>
              <BodyTableUser
                key={index}
                no={++index}
                firstname={data.first_name}
                lastname={data.last_name}
                ID={data}
                email={data.email}
                code={data.country_code}
                phone={data.mobile_number}
                city={"Pune"}
                member={"25/08/2019"}
                transactions={"Rs. 20,000"}
                password={data.password}
                messageAllData={data}
                lockUser={this.onClickUserLock(data)}
                eyeonclick={this.onClickUsersViewDetails(data)}
              />
            </React.Fragment>
          )
      );
    }
    return data;
  };

  // renderUsersTablePagination = () => {
  //   const { pageNo } = this.state;
  //   return (
  //     <React.Fragment>
  //       <div className="container-fluid pagination-section">
  //         <div className="row">
  //           <div className="col-12">
  //             <nav aria-label="Page navigation example" id="page-list">
  //               <ul className="pagination">
  //                 <li className="page-item">
  //                   <i className="fa fa-angle-left"></i>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "1"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("1")}
  //                   >
  //                     01
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "2"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("2")}
  //                   >
  //                     02
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "3"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("3")}
  //                   >
  //                     03
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "4"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("4")}
  //                   >
  //                     04
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "5"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("5")}
  //                   >
  //                     05
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "6"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("6")}
  //                   >
  //                     06
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "7"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("7")}
  //                   >
  //                     07
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "8"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("8")}
  //                   >
  //                     08
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "9"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("9")}
  //                   >
  //                     09
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <Link
  //                     className={
  //                       pageNo === "10"
  //                         ? "active-page-link page-link"
  //                         : "page-link"
  //                     }
  //                     to="#"
  //                     onClick={this.paginationbtn("10")}
  //                   >
  //                     10
  //                   </Link>
  //                 </li>
  //                 <li className="page-item">
  //                   <i className="fa fa-angle-right"></i>
  //                 </li>
  //               </ul>
  //             </nav>
  //           </div>
  //         </div>
  //       </div>
  //     </React.Fragment>
  //   );
  // };

  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className="container-fluid admin_margin-padding-main">
          <SearchboxName
            headingtext="All Users"
            name="search"
            type="text"
            onChange={this.onSearchChange}
            value={this.state.search}
          />
          <div className="table-responsive user-table-radius">
            <table className="table table-striped">
              <thead>
                <HeaderTableUser
                  no={"Sr.No"}
                  name={"Name"}
                  email={"Email"}
                  phone={"Phone"}
                  city={"City"}
                  member={"Member Since"}
                  transactions={"Transactions"}
                  action={"Actions"}
                />
              </thead>
              <tbody>{this.renderTableBody()}</tbody>
            </table>
          </div>
          <div className="col-12 mt-5 d-flex justify-content-center pagination-main-css p4v2-menu-pagination">
            {/* {this.renderUsersTablePagination()} */}
            <Pagination
              onChange={this.onChangePagination}
              current={this.state.currentPagination}
              defaultPageSize={totalRecordsInOnePage}
              total={
                this.state.allUsersDetailsdata
                  ? this.state.allUsersDetailsdata.length
                  : ""
              }
              showTitle={false}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  superAdminGetDetails: state.superAdminGetDetails,
});

export default connect(mapStateToProps, {
  superAdmin_get_users_details,
  lockUser,
  getUserOrder,
})(withRouter(MainAllUsersComponent));
