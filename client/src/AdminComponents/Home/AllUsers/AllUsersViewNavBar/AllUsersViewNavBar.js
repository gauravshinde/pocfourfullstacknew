import React, { Component } from "react";
import { AllUserViewSeatingTableBody } from "../AllUsersSeatingTable/AllUserViewSeatingTableBody";
import { SeatingTableViewPage } from "../AllUsersSeatingViewPage/SeatingTableViewPage";

class AllUsersViewNavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      linkNo: "1",
    };
  }

  toggleClass = (no) => (e) => {
    this.setState({
      linkNo: no,
    });
  };

  seatingViewClick = (data) => {
    console.log(data);
    let changeLink = this.state.linkNo;
    changeLink = "5";
    this.setState({
      linkNo: changeLink,
    });
  };

  render() {
    const { linkNo } = this.state;

    return (
      <React.Fragment>
        <div className="main-navigation-all-users">
          <nav>
            <ul className="nav-links ">
              <li className="border-bottom">
                <p
                  className={
                    linkNo === "1"
                      ? "active-headertext headertext"
                      : "headertext"
                  }
                  onClick={this.toggleClass("1")}
                >
                  Ordering
                </p>
              </li>
              <li className="border-bottom">
                <p
                  className={
                    linkNo === "2"
                      ? "active-headertext headertext"
                      : "headertext"
                  }
                  onClick={this.toggleClass("2")}
                >
                  Seating
                </p>
              </li>

              <li className="border-bottom">
                <p
                  className={
                    linkNo === "3"
                      ? "active-headertext headertext"
                      : "headertext"
                  }
                  onClick={this.toggleClass("3")}
                >
                  Misc
                </p>
              </li>

              <li className="border-bottom">
                <p
                  className={
                    linkNo === "4"
                      ? "active-headertext headertext"
                      : "headertext"
                  }
                  onClick={this.toggleClass("4")}
                >
                  Settings
                </p>
              </li>
            </ul>
          </nav>
        </div>
        {this.state.linkNo === "1" ? <h1>Ordering</h1> : null}
        {this.state.linkNo === "2" ? (
          <AllUserViewSeatingTableBody
            onClickChange={this.state.linkNo}
            sendFetchData={this.props.allData}
            userOrderingData={this.props.userOrderingData}
          />
        ) : null}
        {this.state.linkNo === "3" ? <h1>Misc</h1> : null}
        {this.state.linkNo === "4" ? <h1>Settings</h1> : null}
        {this.state.linkNo === "5" ? <SeatingTableViewPage /> : null}
      </React.Fragment>
    );
  }
}

export default AllUsersViewNavBar;
