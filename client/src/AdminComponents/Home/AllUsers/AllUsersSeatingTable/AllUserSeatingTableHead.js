import React from "react";

export const AllUserSeatingTableHead = ({
  restDetails,
  orderType,
  partySize,
  waitTime,
  details,
  services,
  date,
  status,
  extra
}) => {
  return (
    <React.Fragment>
      <tr className="table-head-section">
        <th>{restDetails}</th>
        <th style={{ width: "157px" }}>{orderType}</th>
        <th>{partySize}</th>
        <th>{waitTime}</th>
        <th>{details}</th>
        <th>{services}</th>
        <th>{date}</th>
        <th>{status}</th>
        <th>{extra}</th>
      </tr>
    </React.Fragment>
  );
};
