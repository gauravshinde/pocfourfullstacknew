import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import isEmpty from "../../../../../store/validation/is-Empty";
import InputComponent from "../../../../../reusableComponents/InputComponent";
import classnames from "classnames";

export default class SeatingOccasionPopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, allData: "" };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (!isEmpty(nextProps.occasion !== nextState.allData)) {
      return {
        allData: nextProps.occasion,
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  onDelete = () => {
    alert("user deleted sucessfully");
    window.location.reload();
  };

  render() {
    const { open, allData } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/file-icon-table.svg")}
          alt="message img"
          className="user-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Occasion</div>

            <div className="container mt-3">
              <p className="pb-2">{allData}</p>
              {/* <form>
                <div className="form-row">
                  <div className="col-12">
                    <InputComponent
                      labeltext={this.props.Name}
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="eg- Name"
                      value={this.state.message}
                      name="message"
                      placeholder="first-name"
                      onChange={this.onChange}
                      inputclass={classnames("map-inputfield input-wide")}
                    />
                  </div>
                </div>
              </form> */}
            </div>
            <div className="mt-2 button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Okay"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.onCloseModal}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}
