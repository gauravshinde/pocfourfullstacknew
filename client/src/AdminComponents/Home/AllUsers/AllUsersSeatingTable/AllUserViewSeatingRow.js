import React from "react";
import SeatingOccasionPopup from "./AllUserSeatingPopup/SeatingOccasionPopup";
import dateFns from "date-fns";

export const AllUserViewSeatingRow = ({
  onClickChange,
  restaurantName,
  partySize,
  waitTime,
  date,
  dinerStatus,
  ETA,
  occasion,
  orderData,
}) => {
  return (
    <React.Fragment>
      <tr className="table-rowclass user-view-seating-row">
        <td>
          <img
            style={{ width: "30%" }}
            src={require("../../../../assets/images/AdminDashboard/home/nomination/mcdonalds.png")}
            alt="img"
          />
          <span className="bold-text ml-3">{restaurantName}</span>
        </td>
        <td>
          {orderData === "walkin" ? (
            <img
              src={require("../../../../assets/images/AdminDashboard/home/walk.svg")}
              alt="img"
              style={{ width: "12%" }}
              className="ml-3"
            />
          ) : null || orderData === "waitlist" ? (
            <img
              src={require("../../../../assets/images/AdminDashboard/home/clock.png")}
              alt="img"
              style={{ width: "15%" }}
              className="ml-3"
            />
          ) : null || orderData === "reservation" ? (
            <img
              src={require("../../../../assets/images/AdminDashboard/home/reservation.png")}
              alt="img"
              style={{ width: "15%" }}
              className="ml-3"
            />
          ) : null}
        </td>
        <td>
          <span>{partySize}</span>
        </td>
        <td>
          <span className="bold-text ">{waitTime}</span>
          {/* {format(new Date({waitTime}), 'yyyy-MM-dd')} */}
          <img
            src={require("../../../../assets/images/AdminDashboard/openissues/penimg.svg")}
            alt="img"
            className="mr-4"
            style={{ float: "right", width: "15%" }}
          />
        </td>
        <td>
          <span className="bold-text">Location</span>
          <img
            src={require("../../../../assets/images/AdminDashboard/home/maps.svg")}
            alt="img"
            className="ml-4 mt-2"
            style={{ width: "13%", float: "right" }}
          />
          <p>Koregaon park</p>
          <span className="bold-text">ETA</span>

          <p className={ETA === "" ? "text-danger" : "text-success"}>
            {ETA === "" ? "ETA Not Available" : `${ETA} minutes`}
          </p>
        </td>
        <td>
          <img
            style={{ width: "45%" }}
            src={require("../../../../assets/images/AdminDashboard/home/driver.svg")}
            alt="img"
            className="ml-3 mb-2"
          />
        </td>
        <td>
          <span className="mb-3 col-12">
            {dateFns.format(date, "DD-MM-YYYY")}
          </span>
          <p className="mb-3 col-12">{dateFns.format(date, "HH:mm A ")}</p>
        </td>
        <td>
          <select className="form-control  dropdown-seating-view" id="sel1">
            <option>{dinerStatus}</option>
            <option>Non Seated</option>
            <option>Seated</option>
            <option>Completed</option>
            <option>Cancelled</option>
          </select>
        </td>

        <td>
          <div className="row edit-view-page-icons ml-3 ">
            <div className="col-6 mt-3 p-0">
              <SeatingOccasionPopup occasion={occasion} />
            </div>
            <div className="col-6 mt-3  p-0">
              <img
                className="icon-image"
                src={require("../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/eye-icon-table.svg")}
                alt="img"
                onClick={onClickChange}
              />
              <div>
                <img
                  src={require("../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/edit-icon-table.svg")}
                  alt="img"
                  className="mt-3 icon-image"
                />
              </div>
            </div>
          </div>
        </td>
      </tr>
    </React.Fragment>
  );
};
