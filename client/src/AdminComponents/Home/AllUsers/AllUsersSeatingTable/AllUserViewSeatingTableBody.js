import React, { Component } from "react";
import { AllUserViewSeatingRow } from "./AllUserViewSeatingRow";
import { AllUserSeatingTableHead } from "./AllUserSeatingTableHead";
import SearchboxName from "../../../AdminReusableComponent/SearchBoxandPageName/SearchboxName";
import isEmpty from "../../../../store/validation/is-Empty";
import { SeatingTableViewPage } from "../AllUsersSeatingViewPage/SeatingTableViewPage";

export class AllUserViewSeatingTableBody extends Component {
  constructor() {
    super();
    this.state = {
      linkNo: true,
      individualData: {},
      sendFetchData: {},
      userOrderingData: {},
    };
  }

  onChangeLink = (data) => (e) => {
    e.preventDefault();
    this.setState({
      linkNo: false,
      individualData: data,
    });
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    if (
      !isEmpty(nextProps.sendFetchData) !== nextState.sendFetchData &&
      !isEmpty(nextProps.userOrderingData) !== nextState.userOrderingData
    ) {
      return {
        sendFetchData: nextProps.sendFetchData,
        userOrderingData: nextProps.userOrderingData,
      };
    }
    return null;
  }

  render() {
    const { userOrderingData, individualData } = this.state;
    console.log(individualData);
    return (
      <React.Fragment>
        {/* {console.log("propsdata", userOrderingData)}{console.log("propsdata", userOrderingData)} */}
        {this.state.linkNo === true ? (
          <div className=" admin_margin-padding-main mt-4 mb-5">
            <SearchboxName headingtext={"All Seats"} />
            <div className="table-responsive  newtable-vendor">
              <table className="table table-striped">
                <thead>
                  <AllUserSeatingTableHead
                    restDetails={"Restaurant Details"}
                    orderType={"Order Type"}
                    partySize={"Party Size"}
                    waitTime={"Wait Time"}
                    details={"Details"}
                    services={"Services"}
                    date={"Date"}
                    status={"Status"}
                  />
                </thead>
                <tbody>
                  {!isEmpty(userOrderingData)
                    ? userOrderingData.map((data, index) => {
                        return (
                          <AllUserViewSeatingRow
                            key={index}
                            orderData={data.order_type}
                            onClickChange={this.onChangeLink(data)}
                            restaurantName={data.restaurant_name}
                            orderType={data.order_type}
                            partySize={data.total}
                            waitTime={data.default_wait_time}
                            date={data.Seating_Date}
                            dinerStatus={data.dinner_status}
                            ETA={data.ETA}
                            occasion={data.special_occassion}
                          />
                        );
                      })
                    : null}
                </tbody>
              </table>
            </div>
          </div>
        ) : (
          <SeatingTableViewPage orderData={individualData} />
        )}
      </React.Fragment>
    );
  }
}

export default AllUserViewSeatingTableBody;
