import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import InputComponent from "../../../../reusableComponents/InputComponent";
import classnames from "classnames";
import isEmpty from "../../../../store/validation/is-Empty";

export class UsersViewNotificationPopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, allData: {} };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.notificationData !== nextState.allData) {
      return {
        allData: nextProps.notificationData
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/

  onSendNotification = () => {
    alert("Sucess");
    window.location.reload();
  };
  render() {
    const { open, allData } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../assets/images/AdminDashboard/home/notification.png")}
          alt="message img"
          className="user-logo-img mr-0"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Push Notification</div>

            <div className="container mt-3">
              <p className="pt-4 pb-0">
                Send notification to "{" "}
                {!isEmpty(allData)
                  ? allData.first_name + " " + allData.last_name
                  : null}
                "
              </p>
              <form>
                <div className="form-row">
                  <div className="col-12">
                    <InputComponent
                      labeltext={this.props.Name}
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="eg- Name"
                      value={this.state.notification}
                      name="notification"
                      placeholder="first-name"
                      onChange={this.onChange}
                      inputclass={classnames("map-inputfield input-wide")}
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className="mr-2 button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Cancel"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.onCloseModal}
              />
              <ButtonComponent
                type={"button"}
                buttontext={"Send"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onSendNotification}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default UsersViewNotificationPopup;
