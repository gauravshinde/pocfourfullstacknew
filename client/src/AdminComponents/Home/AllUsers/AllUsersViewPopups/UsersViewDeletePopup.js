import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import isEmpty from "../../../../store/validation/is-Empty";
import { deleteUser } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class UsersViewDeletePopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, allData: "", changeText: true };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.deleteData !== nextState.allData) {
      return {
        allData: nextProps.deleteData,
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false, changeText: true });
  };

  onDelete = () => {
    this.setState({
      changeText: false,
      open: false,
      message: "",
    });
    const formdata = {
      _id: this.state.allData._id,
    };
    // console.log(formdata);
    this.props.deleteUser(formdata, this.props.history);
  };

  render() {
    const { open, allData } = this.state;
    return (
      <React.Fragment>
        {/* {console.log(this.state.allData)} */}
        <img
          src={require("../../../../assets/images/AdminDashboard/home/delete.png")}
          alt="message img"
          className="user-logo-img "
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Message</div>

            <div className="container mt-3">
              <form>
                <div className="form-row">
                  <div className="col-12 pt-4 pb-4">
                    {this.state.changeText ? (
                      <h5>
                        Are you sure want to delete "
                        {!isEmpty(allData)
                          ? " " + allData.first_name + " " + allData.last_name
                          : null}
                        "
                      </h5>
                    ) : (
                      <h5>User Deleted SucessFully</h5>
                    )}
                  </div>
                </div>
              </form>
            </div>

            <div className="mr-2 button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"No"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.onCloseModal}
              />
              <ButtonComponent
                type={"button"}
                buttontext={"Yes"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onDelete}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { deleteUser })(
  withRouter(UsersViewDeletePopup)
);
