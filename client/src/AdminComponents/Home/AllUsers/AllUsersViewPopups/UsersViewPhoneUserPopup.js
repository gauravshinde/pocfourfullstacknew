import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import isEmpty from "../../../../store/validation/is-Empty";
import { callUser } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";
import { connect } from "react-redux";

class UsersViewPhoneUserPopup extends Component {
  state = {
    allData: {},
    open: false,
    phoneData: "",
    changeText: "true",
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.phoneData !== nextState.allData) {
      return {
        allData: nextProps.phoneData,
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({
      open: false,
      changeText: true,
    });
  };

  onCall = () => {
    this.setState({
      changeText: false,
    });
    const formdata = {
      mobileNo: this.state.allData.mobile_number,
    };
    this.props.callUser(formdata);
    console.log(formdata);
  };

  render() {
    const { open, allData } = this.state;

    return (
      <React.Fragment>
        {console.log(this.state.allData)}
        <img
          src={require("../../../../assets/images/AdminDashboard/home/phone.png")}
          alt="message img"
          className="user-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Call Now</div>

            <div className="container mt-3 mb-3 text-center">
              {this.state.changeText ? (
                <h4 className="pt-4 pb-0 mb-3">
                  Call{" "}
                  {!isEmpty(allData)
                    ? allData.first_name + " " + allData.last_name
                    : null}
                </h4>
              ) : (
                <h4>Calling {allData.first_name}......</h4>
              )}

              <h5>{this.props.phone}</h5>
            </div>
            {this.state.changeText ? (
              <div className="mr-2 button-div">
                <ButtonComponent
                  type={"button"}
                  buttontext={"Cancel"}
                  buttonclass={"btn button-width button-border-orange"}
                  onClick={this.onCloseModal}
                />
                <ButtonComponent
                  type={"button"}
                  buttontext={"Call"}
                  buttonclass={"btn button-width button-orange"}
                  onClick={this.onCall}
                />
              </div>
            ) : (
              <div style={{ textAlign: "center", paddingBottom: "2%" }}>
                <ButtonComponent
                  type={"button"}
                  buttontext={"Ok"}
                  buttonclass={"btn button-width button-orange"}
                  onClick={this.onCloseModal}
                />
              </div>
            )}
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { callUser })(UsersViewPhoneUserPopup);
