import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import InputComponent from "../../../../reusableComponents/InputComponent";
import classnames from "classnames";
import isEmpty from "../../../../store/validation/is-Empty";
import { connect } from "react-redux";
import { messageUser } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";

class UsersViewMessagePopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, allData: {}, message: "", changeText: true };
  }
  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.messageData !== nextState.allData) {
      return {
        allData: nextProps.messageData,
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false, changeText: true });
  };
  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = (e) => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSendMessage = () => {
    this.setState({
      changeText: false,
      message: "",
      open: false,
    });
    const formdata = {
      mobileNo: this.state.allData.mobile_number,
      message: this.state.message,
    };
    console.log(formdata);
    this.props.messageUser(formdata);
  };

  render() {
    const { open, allData } = this.state;
    return (
      <React.Fragment>
        {console.log(this.state.allData)}
        <img
          src={require("../../../../assets/images/AdminDashboard/home/message.png")}
          alt="message img"
          className="user-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Message</div>

            <div className="container mt-3">
              <p className="pt-4 pb-0">
                Send message to "
                {!isEmpty(allData)
                  ? allData.first_name + " " + allData.last_name
                  : null}
                "
              </p>
              <form>
                <div className="form-row">
                  <div className="col-12">
                    <InputComponent
                      labeltext={this.props.Name}
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="eg- Name"
                      value={this.state.message}
                      name="message"
                      placeholder="first-name"
                      onChange={this.onChange}
                      inputclass={classnames("map-inputfield input-wide")}
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className="mr-2 button-div">
              <ButtonComponent
                type={"button"}
                buttontext={"Cancel"}
                buttonclass={"btn button-width button-border-orange"}
                onClick={this.onCloseModal}
              />
              <ButtonComponent
                type={"button"}
                buttontext={"Send"}
                buttonclass={"btn button-width button-orange"}
                onClick={this.onSendMessage}
              />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { messageUser })(UsersViewMessagePopup);
