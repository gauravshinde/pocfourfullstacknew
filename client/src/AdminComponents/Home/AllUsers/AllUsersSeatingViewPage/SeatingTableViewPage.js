import React from "react";
import { Link } from "react-router-dom";
import dateFns from "date-fns";

export const SeatingTableViewPage = ({ orderData }) => {
  return (
    <React.Fragment>
      {console.log("adasdasdsad", orderData)}
      <div className="seating-view-icon-main admin_margin-padding-main">
        <div className="navigation-button mt-4 mb-4">
          <Link className="link-css" to="/allusers">
            <img
              src={require("../../../../assets/images/AdminDashboard/home/left.svg")}
              alt="img"
            />
            <span className="link-text "> Back To Orders</span>
          </Link>
        </div>
        <div className="user-top-order mb-3">
          <div className="row  h-100">
            <div className="col-2 justify-content-center align-self-center border-right ">
              <img
                style={{ width: "25%" }}
                src={require("../../../../assets/images/AdminDashboard/home/vendorimage.png")}
                alt="img"
                className="mr-3"
              />
              <span>{orderData.restaurant_name}</span>
            </div>
            <div className="col-2 border-right">
              <h2>Request Status</h2>
              <select className="selectpicker">
                <option>Accepted</option>
                <option>Cancelled</option>
              </select>
              <div className="mt-3">
                <span>Order Type</span>
                {orderData.order_type === "walkin" ? (
                  <img
                    src={require("../../../../assets/images/AdminDashboard/home/walk.svg")}
                    alt="img"
                    style={{ width: "8%" }}
                    className="ml-3"
                  />
                ) : null || orderData.order_type === "waitlist" ? (
                  <img
                    src={require("../../../../assets/images/AdminDashboard/home/clock.png")}
                    alt="img"
                    style={{ width: "12%" }}
                    className="ml-3"
                  />
                ) : null || orderData.order_type === "reservation" ? (
                  <img
                    src={require("../../../../assets/images/AdminDashboard/home/reservation.png")}
                    alt="img"
                    style={{ width: "12%" }}
                    className="ml-3"
                  />
                ) : null}
              </div>
            </div>
            <div className="col-3 border-right">
              <h2>Party Size : {orderData.total}</h2>
              <div className="row mt-2">
                <div className="col-6">
                  <p>Adults : {orderData.adults}</p>
                </div>
                <div className="col-6">
                  <p>Kids : {orderData.kids}</p>
                </div>
                <div className="col-6">
                  <p>High-Chair : {orderData.handicap}</p>
                </div>
                <div className="col-6">
                  <p>Handicap : {orderData.highchair}</p>
                </div>
              </div>
            </div>
            <div className="col-1 border-right p-0">
              <div className="ml-2">
                <h2 className="mb-2">Wait Time</h2>
                <p>
                  {orderData.default_wait_time}
                  <img
                    src={require("../../../../assets/images/AdminDashboard/openissues/penimg.svg")}
                    alt="img"
                    className="ml-4"
                  />
                </p>
                <p>10:00</p>
              </div>
            </div>
            <div className="col-2 border-right">
              <h2>Seating Status</h2>
              <p>{orderData.dinner_status}</p>
              {/* <select className="selectpicker">
                <option>Non Seated</option>
                <option>Seated</option>
                <option>Cancelled</option>
              </select> */}
            </div>
            <div className="col-2 justify-content-center align-self-center">
              <div style={{ float: "right" }}>
                <img
                  src={require("../../../../assets/images/AdminDashboard/home/edit.png")}
                  alt="img"
                  className="square-img"
                />
                <img
                  src={require("../../../../assets/images/dashboard/reviews/remove.png")}
                  alt="img"
                  className="square-img"
                />
              </div>
            </div>
          </div>
        </div>
        {/* Middle div details */}
        <div className="user-details-part">
          <div className="details-heading">
            <h1 className="border-bottom">Details</h1>
          </div>
          <div className="row mt-2">
            <div className="col-2">
              <h2>Phone Number</h2>
              <p>{orderData.phone_number}</p>
            </div>
            <div className="col-3">
              <h2>ETA</h2>
              <p
                className={
                  orderData.ETA === "" ? "text-danger" : "text-success"
                }
              >
                {orderData.ETA === ""
                  ? "ETA Not Available"
                  : `${orderData.ETA} minutes`}
              </p>
            </div>
            <div className="col-3">
              <h2>Location</h2>

              <p>
                Koregaon Park
                <img
                  style={{ width: "5%" }}
                  src={require("../../../../assets/images/AdminDashboard/home/maps.svg")}
                  alt="img"
                  className="ml-5"
                />
              </p>
            </div>

            <div className="col-2">
              <h2>Requested Time</h2>
              <p>{dateFns.format(orderData.Request_Date, "HH:mm A")}</p>
            </div>
            <div className="col-2">
              <h2>Seated Time</h2>
              <p>{dateFns.format(orderData.Seating_Date, "HH:mm A")}</p>
            </div>
          </div>
          {/* Beloooowwww part */}
          <div className="row mt-4">
            <div className="col-2">
              <h2>Point Of Entry</h2>
              <p>{orderData.entry_point}</p>
            </div>
            <div className="col-3">
              <h2>Ocassion</h2>
              <p>{orderData.special_occassion}</p>
            </div>
            <div className="col-3">
              <h2>Seating Preferences</h2>
              <p>{orderData.seating_preference}</p>
            </div>

            <div className="col-2">
              <h2>Requested Date</h2>
              <p>{dateFns.format(orderData.Request_Date, "DD-MM-YYYY")}</p>
            </div>
            <div className="col-2">
              <h2>Seated Date</h2>
              <p>{dateFns.format(orderData.Seating_Date, "DD-MM-YYYY")}</p>
            </div>
          </div>
        </div>
        {/* Bottom Part Edit Log */}
        <div className="edit-log-part mt-3">
          <div className="details-heading">
            <h1 className="border-bottom">Edit Logs</h1>
          </div>
          <div className="row mt-4 header-part">
            <div className="col-3">
              <h2>Date</h2>
            </div>
            <div className="col-2">
              <h2>Time</h2>
            </div>
            <div className="col-5">
              <h2>Operation</h2>
            </div>
            <div className="col-2">
              <h2>Operation Changed By</h2>
            </div>
          </div>
          <div className="bottom-header-part">
            <div className="row padding-inside mt-3 align-items-center h-100">
              <div className="col-3">
                <p>13th August</p>
              </div>
              <div className="col-2">
                <p>06:30 PM</p>
              </div>
              <div className="col-5">
                <p>Seating status changed from non seated to seated</p>
              </div>
              <div className="col-2">
                <p>you</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
