import React from "react";
import { Link } from "react-router-dom";
import UsersViewNotificationPopup from "../AllUsersViewPopups/UsersViewNotificationPopup";
import UsersViewDeletePopup from "../AllUsersViewPopups/UsersViewDeletePopup";
import UsersViewMessagePopup from "../AllUsersViewPopups/UsersViewMessagePopup";
import UsersViewPhoneUserPopup from "../AllUsersViewPopups/UsersViewPhoneUserPopup";
import dateFns from "date-fns";

export const UserDetailTop = ({ sendData }) => {
  return (
    <React.Fragment>
      <div className="user-details-main ml-3container-fluid ">
        <div className="row  user-detail-top admin_margin-padding-main align-items-center">
          <div className="col-sm-4 p-0">
            <div className="row">
              <div className="col-sm-2 p-0 mt-4 back-button">
                <Link className="link-css" to="/allusers">
                  <img
                    src={require("../../../../assets/images/AdminDashboard/home/left.svg")}
                    alt="img"
                    style={{ width: "17%" }}
                    className="ml-4"
                  />
                  <span className="link-text"> Back</span>
                </Link>
              </div>
              <div className="col-4 mt-2 ">
                <img
                  style={{ width: "60%" }}
                  alt="img"
                  src={require("../../../../assets/images/AdminDashboard/home/ladyImage.png")}
                  className="ml-3"
                ></img>
              </div>
              <div className="col-sm-5 pr-0 pl-0 mt-2 userDetail-topPopup">
                <h4 className="mb-2">
                  {sendData.first_name + " " + sendData.last_name}
                </h4>
                <UsersViewPhoneUserPopup phoneData={sendData} />
                <UsersViewMessagePopup messageData={sendData} />
                <UsersViewNotificationPopup notificationData={sendData} />
              </div>
            </div>
          </div>
          <div className="col-sm-6 p-0 ">
            <div className="row">
              <div className="col-sm-4 border-left pl-3 p-0 text-break">
                <h5>Email Id</h5>
                <p>{sendData.email}</p>
              </div>
              <div className="col-sm-1 pl-2 p-0">
                <h5>City</h5>
                <p>Pune</p>
              </div>
              <div className="col-sm-3">
                <h5>Phone</h5>
                <p>{sendData.mobile_number}</p>
              </div>
              <div className="col-sm-2 pr-0 pl-0">
                <h5>Member Since</h5>
                <p>{dateFns.format(sendData.register_date, " DD-MM-YYYY")}</p>
              </div>
              <div className="col-sm-2 border-right">
                <h5>Transactions</h5>
                <p>250</p>
              </div>
            </div>
          </div>
          <div className="col-sm-2 text-right   userDetail-topPopup">
            <UsersViewDeletePopup deleteData={sendData} />
            <img
              style={{ width: "18%" }}
              className="user-logo-img "
              alt="img"
              src={require("../../../../assets/images/AdminDashboard/home/flag.png")}
            />
            <img
              style={{ width: "18%" }}
              alt="img"
              className="user-logo-img "
              src={require("../../../../assets/images/AdminDashboard/home/lock.png")}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
