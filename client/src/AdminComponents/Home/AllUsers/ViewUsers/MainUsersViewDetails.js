import React, { Component } from "react";
import AdminNavbar from "../../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import { UserDetailTop } from "./UserDetailTop";

import isEmpty from "../../../../store/validation/is-Empty";
import AllUsersViewNavBar from "../AllUsersViewNavBar/AllUsersViewNavBar";
import { getUserOrder } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

export class MainUsersViewDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Userviewdetailpage: {},
      user_id: "",
      userOrder: {},
    };
  }

  componentDidMount() {
    this.props.getUserOrder(this.state.Userviewdetailpage._id);
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    // console.log(nextProps.location.state.Userviewdetailpage);
    if (
      !isEmpty(nextProps.location.state.Userviewdetailpage) !==
      nextState.Userviewdetailpage
    )
      if (
        !isEmpty(nextProps.AllUsersReducer.orderData) !== nextState.userOrder
      ) {
        return {
          Userviewdetailpage: nextProps.location.state.Userviewdetailpage,
          userOrder: nextProps.AllUsersReducer.orderData,
        };
      }

    return null;
  }

  render() {
    const { Userviewdetailpage } = this.state;
    return (
      <React.Fragment>
        {console.log("hola", this.state.userOrder)}
        <AdminNavbar />
        <UserDetailTop sendData={Userviewdetailpage} />
        <AllUsersViewNavBar
          allData={Userviewdetailpage._id}
          userOrderingData={this.state.userOrder}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  AllUsersReducer: state.AllUsersReducer,
});

export default connect(mapStateToProps, { getUserOrder })(
  withRouter(MainUsersViewDetails)
);
