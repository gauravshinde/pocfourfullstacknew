import React, { Component } from 'react';
import AdminNavbar from '../../AdminReusableComponent/NavBarComponent/AdminNavbar';

export class MainOpenIssuesVendorsComponents extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className='container-fluid admin_margin-padding-main'>
          <h1>ALL Open Issues Vendors Page</h1>
        </div>
      </React.Fragment>
    );
  }
}

export default MainOpenIssuesVendorsComponents;
