// import React from "react";

// export const TableHeadAddReview = ({
//   restaurantCode,
//   restaurantId,
//   restaurantName,
//   restaurantAddress,
//   restaurantCity,
//   restaurantMember,
//   restaurantAction
// }) => {
//   return (
//     <React.Fragment>
//       <thead>
//         <tr>
//           <th>{restaurantCode}</th>
//           <th>{restaurantId}</th>
//           <th>{restaurantName}</th>
//           <th>{restaurantAddress}</th>
//           <th>{restaurantCity}</th>
//           <th>{restaurantMember}</th>
//           <th>{restaurantAction}</th>
//         </tr>
//       </thead>
//     </React.Fragment>
//   );
// };

import React from "react";

export const TableHeadAddReview = ({
  restaurantCode,
  restaurantId,
  restaurantName,
  restaurantAddress,
  restaurantCity,
  restaurantMember
}) => {
  return (
    <React.Fragment>
      <tr className="addreview-head-table-class">
        <th style={{ width: "7%" }}>{restaurantCode}</th>
        <th style={{ width: "13%" }}>{restaurantId}</th>
        <th style={{ width: "17%" }}>{restaurantName}</th>
        <th style={{ width: "18%" }}>{restaurantAddress}</th>
        <th style={{ width: "12%", textAlign: "center" }}>{restaurantCity}</th>
        <th style={{ width: "15%" }}>{restaurantMember}</th>
        <th style={{ width: "12%" }}></th>
      </tr>
    </React.Fragment>
  );
};
