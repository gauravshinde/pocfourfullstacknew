import React from "react";

export const TableBodyAddReview = ({
  code,
  restaurantid,
  name,
  address,
  city,
  member,
  img,
  eyeonClick
}) => {
  return (
    <React.Fragment>
      <tr className="addreview-body-table-class">
        <td>{code}</td>
        <td>{restaurantid}</td>
        <td>
          <div className="row">
            <div className="col-4">
              {" "}
              <img
                src={img}
                alt="restaurant-img"
                className="img-fluid restaurant-image-td"
              />
            </div>
            <div className="col-8">
              <span className="name-restaurant-dark">{name}</span>
            </div>
          </div>
        </td>
        <td>{address}</td>
        <td style={{ textAlign: "center" }}>{city}</td>
        <td>{member}</td>
        <td>
          <img
            src={require("../../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/eye.png")}
            alt="eye-img"
            className="img-fluid eye-img"
            onClick={eyeonClick}
          />
          <img
            src={require("../../../../../assets/images/AdminDashboard/home/all-vendors/view-vendor/menu/edit-icon-table.svg")}
            alt="edit-img"
            className="img-fluid edit-img"
          />
        </td>
      </tr>
    </React.Fragment>
  );
};
