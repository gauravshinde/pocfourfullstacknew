import React, { Component } from "react";
import BeautyStars from "beauty-stars";

export class ApprovedReviews extends Component {
  constructor() {
    super();
    this.state = {
      value: 3
    };
  }

  renderApprovedReviews = () => {
    const { value } = this.state;
    return (
      <React.Fragment>
        <div className="col-12 main-offers-inside">
          <div className="row">
            <div className="col-sm-2 d-flex justify-content-start align-items-center">
              <div className="">
                <img
                  src={require("../../../../assets/images/dashboard/reviews/user.png")}
                  alt="product"
                  className="img-fluid"
                  style={{ border: "1px solid #000" }}
                />
              </div>
              <div className="">
                <h3>nameone</h3>
              </div>
            </div>
            <div className="col-sm-10 d-flex align-items-center">
              <div className="col-8 main-offers-sectiontwo-divone main-offers-sectiontwo-divone-hight">
                <span>
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                  id odio eget turpis pretium euismod. Lorem Ipsum Dolor Sit
                  Amet, Consectetur Adipiscing Elit. Sed Id Odio Eget Turpis
                  Pretium Euismod.""Lorem ipsum dolor sit amet, consectetur
                  adipiscing elit. Sed id odio eget turpis pretium euismod.
                  Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Sed
                  Id Odio Eget Turpis Pretium Euismod."
                </span>
              </div>
              <div className="col-4 main-offers-sectiontwo-divtwo main-offers-sectiontwo-divone-hight">
                <div
                  className="d-flex align-items-center"
                  style={{ height: "8vh" }}
                >
                  <BeautyStars
                    value={this.state.value}
                    size="20px"
                    inactiveColor="rgba(255, 213, 0, 0.25)"
                    activeColor="#FFD500"
                    editable={false}
                    onChange={value => this.setState({ value })}
                  />
                  {/* <div className="col-sm-8">hfgdgdgdfgfd</div>
                  <div className="col-sm-4 d-flex">
                    <img
                      src={require("../../../assets/images/dashboard/reviews/done.png")}
                      alt="edit"
                      className="img-fluid"
                      // onClick={() => this.onOpenEditModal(data)}
                    />
                    <img
                      src={require("../../../assets/images/dashboard/reviews/remove.png")}
                      alt="delete"
                      className="img-fluid"
                      //onClick={() => this.onClickDeleteOffer(data)}
                    />
                  </div> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  };
  render() {
    return (
      <React.Fragment>
        <div className="container-fluid all-page-container-padding main-offers-sectiontwo">
          {this.renderApprovedReviews()}
          {this.renderApprovedReviews()}
          {this.renderApprovedReviews()}
        </div>
      </React.Fragment>
    );
  }
}

export default ApprovedReviews;
