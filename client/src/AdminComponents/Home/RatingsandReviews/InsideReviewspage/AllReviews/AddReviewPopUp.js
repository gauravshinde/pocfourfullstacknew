import React, { Component } from "react";
import StarRatings from "react-star-ratings";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../../pocsrcone/Components/SmallComponents/ButtonComponent";
import axios from "axios";

export class AddReviewPopUp extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      name: "",
      description: "",
      restaurant_logo: ""
    };
  }

  /**************************************
   * @DESC - STAR RATING HANDLER
   **************************************/
  changeRating = (newRating, name) => {
    this.setState({
      rating: newRating
    });
    // console.log(newRating);
  };

  starratingvalue = () => {
    this.setState({});
  };

  /**************************************
   * @DESC - ON IMAGE UPLOAD HANDLER
   * @DESC - USER DEFINED METHODS
   **************************************/
  onImageLogoUploadHandler = e => {
    this.setState({ loader: true });
    const data = new FormData();
    data.append("image", e.target.files[0]);
    axios
      .post("/image/upload-content-images", data)
      .then(res => {
        this.setState({ restaurant_logo: res.data.image_URL, loader: false });
      })
      .catch(err => {
        this.setState({ loader: false });
        window.alert("Error while uploading the image");
      });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  /********************************************
   * @DESC ADD REVIEW POPUP HANDLE SUBMIT
   ********************************************/
  handleAddReviewSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    let FormData = {
      name: this.state.name,
      description: this.state.description,
      restaurant_logo: this.state.restaurant_logo,
      rating: this.state.rating
    };
    console.log(FormData);
    this.onCloseModal();
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <button
          type="button"
          className="add-review-button"
          onClick={this.onOpenModal}
        >
          Add Review
        </button>

        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "add-review-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="add-review-popup">
            <div className="popup-head">Add Review</div>
            <div className="container mt-3 mb-3">
              <form onSubmit={this.handleAddReviewSubmit}>
                <div className="form-group">
                  <label className="add-review-label-class" htmlFor="name">
                    Name
                  </label>
                  <input
                    id="name"
                    type="text"
                    name="name"
                    className="add-review-input"
                    placeholder="eg. James Bond"
                    value={this.state.name}
                    onChange={this.onChange}
                  ></input>
                </div>

                <div className="form-group">
                  <label className="add-review-label-class" htmlFor="file">
                    Image
                  </label>
                  <div className="col-12 pl-0">
                    <div
                      className="view_chainsBor mt-0"
                      style={{ width: "100%" }}
                    >
                      <div className="custom_file_upload">
                        <input
                          type="file"
                          name="icon"
                          id="file"
                          onChange={this.onImageLogoUploadHandler}
                          className="custom_input_upload"
                        />
                        <label
                          className="custom_input_label newChain_add"
                          htmlFor="file"
                        >
                          <div>
                            <i
                              className="fa fa-plus"
                              style={{ color: "#CCCCCC" }}
                            ></i>
                          </div>
                          <div className="add_new_text">Add New</div>
                        </label>
                      </div>

                      <div className="newChain_addthree mx-3">
                        {this.state.restaurant_logo ? (
                          <img
                            src={this.state.restaurant_logo}
                            className="newChain_addtwo"
                            style={{ height: "100%", width: "100%" }}
                            alt="chain"
                          />
                        ) : null}
                      </div>
                    </div>
                  </div>
                </div>

                <div className="form-group">
                  <label
                    className="add-review-label-class"
                    htmlFor="description"
                  >
                    Description
                  </label>
                  <textarea
                    id="description"
                    type="text"
                    name="description"
                    className="add-review-textarea"
                    placeholder="eg. Ingredients"
                    value={this.state.description}
                    onChange={this.onChange}
                  ></textarea>
                </div>

                <div className="form-group">
                  <label className="add-review-label-class" htmlFor="rating">
                    Select Rating
                  </label>
                  <div className="">
                    <StarRatings
                      name="rating"
                      rating={this.state.rating}
                      changeRating={this.changeRating}
                      // onClick={this.starratingvalue}
                      numberOfStars={5}
                      starHoverColor="#FFD500"
                      starRatedColor="#FFD500"
                      starEmptyColor="rgba(255, 213, 0, 0.25)"
                      starDimension="40px"
                      isAggregateRating="true"
                    />
                  </div>
                </div>

                <div className="button-div">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseModal}
                  />
                  <ButtonComponent
                    type={"submit"}
                    buttontext={"Save"}
                    buttonclass={"btn button-width button-orange"}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default AddReviewPopUp;
