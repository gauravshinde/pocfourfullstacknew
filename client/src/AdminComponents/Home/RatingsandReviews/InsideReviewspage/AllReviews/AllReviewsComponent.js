import React, { Component } from "react";
import { Link } from "react-router-dom";
import BeautyStars from "beauty-stars";
import AddReviewPopUp from "./AddReviewPopUp";
import { ProgressBar } from "react-bootstrap";

export class AllReviewsComponent extends Component {
  constructor() {
    super();
    this.state = {
      value: 2
    };
  }

  renderAllReviewsLeftCol = () => {
    return (
      <>
        <div className="left-col">
          <div className="d-flex align-items-center mb-4">
            <div className="">
              {/* <img
                src={require("../../../../../assets/images/dashboard/reviews/reviewimage.png")}
                alt="review img"
                className="img-fluid review-img"
              /> */}
            </div>
            <div className="">
              <h4>McDonalds</h4>
              <h5>281 Reviews</h5>
            </div>
          </div>
          <div className=" mt-3 mb-3">
            <div className="d-flex justify-content-between">
              <div className="">
                <h4>5.0 stars</h4>
              </div>
              <div className="">
                <ProgressBar
                  variant="warning"
                  now={80}
                  className="progressbar-class"
                />
              </div>
            </div>

            <div className="d-flex justify-content-between">
              <div className="">
                <h4>4.0 stars</h4>
              </div>
              <div className="">
                <ProgressBar
                  variant="warning"
                  now={60}
                  className="progressbar-class"
                />
              </div>
            </div>

            <div className="d-flex justify-content-between">
              <div className="">
                <h4>3.0 stars</h4>
              </div>
              <div className="">
                <ProgressBar
                  variant="warning"
                  now={30}
                  className="progressbar-class"
                />
              </div>
            </div>

            <div className="d-flex justify-content-between">
              <div className="">
                <h4>2.0 stars</h4>
              </div>
              <div className="">
                <ProgressBar
                  variant="warning"
                  now={20}
                  className="progressbar-class"
                />
              </div>
            </div>

            <div className="d-flex justify-content-between">
              <div className="">
                <h4>1.0 stars</h4>
              </div>
              <div className="">
                <ProgressBar
                  variant="warning"
                  now={10}
                  className="progressbar-class"
                />
              </div>
            </div>
          </div>
          <div className="text-center">
            <AddReviewPopUp />
          </div>
        </div>
      </>
    );
  };

  renderAllReviewsRightCol = () => {
    return (
      <>
        <div className="right-col">
          <div className="row ">
            <div className="col-sm-5  right-col-section-one">
              <span>
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id
                odio eget turpis pretium euismod. Lorem Ipsum Dolor Sit Amet,
                Consectetur Adipiscing Elit. Sed Id Odio Eget Turpis Pretium
                Euismod."
              </span>
            </div>
            <div className="col-sm-5 right-col-section-two">
              <div className="">
                <BeautyStars
                  value={this.state.value}
                  size="20px"
                  inactiveColor="rgba(255, 213, 0, 0.25)"
                  activeColor="#FFD500"
                  editable={false}
                  onChange={value => this.setState({ value })}
                />
              </div>

              <div className="user-img-name ">
                <div className="">
                  <img
                    src={require("../../../../../assets/images/dashboard/reviews/user.png")}
                    alt="product"
                    className="img-fluid user-img"
                  />
                </div>
                <div className="">
                  <h3>name</h3>
                </div>
              </div>
            </div>
            <div className="col-sm-2 right-col-section-three">
              <div className="">
                <img
                  src={require("../../../../../assets/images/dashboard/reviews/done.png")}
                  alt="edit"
                  className="img-fluid edit-img"
                />
                <img
                  src={require("../../../../../assets/images/dashboard/reviews/remove.png")}
                  alt="delete"
                  className="img-fluid delete-img"
                />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };
  render() {
    return (
      <React.Fragment>
        <div className="container-fluid main-allreview p-0">
          <div className="col-12 p-0 pb-3">
            <div className="back-button-restaurant">
              <i className="fa fa-caret-left" aria-hidden="true"></i>
              <span onClick={this.props.onClick}>Back to Restaurants</span>
            </div>
          </div>

          <div className="row">
            <div className="col-3 ">{this.renderAllReviewsLeftCol()}</div>
            <div className="col-9">
              <h4 className="review-text">All Reviews</h4>
              {this.renderAllReviewsRightCol()}
              {this.renderAllReviewsRightCol()}
              {this.renderAllReviewsRightCol()}
              {this.renderAllReviewsRightCol()}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AllReviewsComponent;
