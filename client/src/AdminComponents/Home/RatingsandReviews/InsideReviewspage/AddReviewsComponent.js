import React, { Component } from "react";
import { TableHeadAddReview } from "./tableData/TableHeadAddReview";
import { TableBodyAddReview } from "./tableData/TableBodyAddReview";
import AllReviewsComponent from "./AllReviews/AllReviewsComponent";

export class AddReviewsComponent extends Component {
  constructor() {
    super();
    this.state = {
      isActive: false
    };
  }

  allReviewHandler = () => {
    this.setState({
      isActive: true
    });
  };

  backToAllRestaurantsHandler = () => {
    this.setState({
      isActive: false
    });
  };

  renderAddReviewTable = () => {
    return (
      <>
        <div className="table-responsive addreview-table ">
          <table className="table table-striped">
            <thead>
              <TableHeadAddReview
                restaurantCode="Code"
                restaurantId="Restaurant Id"
                restaurantName="Name"
                restaurantAddress="Address"
                restaurantCity="City"
                restaurantMember="Member Since"
              />
            </thead>
            <tbody>
              <TableBodyAddReview
                eyeonClick={this.allReviewHandler}
                code="101"
                restaurantid="C137"
                img={require("../../../../assets/images/AdminDashboard/home/nomination/mcdonalds.png")}
                name="McDonalds"
                address="Survey No:32/2, Gurukrupa hall, near Dmart, Ambegaon BK, Pune, Maharashtra 411046"
                city="Pune"
                member="25/08/19"
              />

              <TableBodyAddReview
                eyeonClick={this.allReviewHandler}
                code="101"
                restaurantid="C137"
                img={require("../../../../assets/images/AdminDashboard/home/nomination/mcdonalds.png")}
                name="McDonalds"
                address="Survey No:32/2, Gurukrupa hall, near Dmart, Ambegaon BK, Pune, Maharashtra 411046"
                city="Pune"
                member="25/08/19"
              />
            </tbody>
          </table>
        </div>
      </>
    );
  };

  render() {
    const { isActive } = this.state;
    return (
      <React.Fragment>
        <div className="container-fluid main-addreview">
          {isActive ? (
            <AllReviewsComponent onClick={this.backToAllRestaurantsHandler} />
          ) : (
            this.renderAddReviewTable()
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default AddReviewsComponent;
// import React, { Component } from "react";
// import { TableHeadAddReview } from "./tableData/TableHeadAddReview";

// export class AddReviewsComponent extends Component {
//   render() {
//     return (
//       <React.Fragment>
//         <div className="container-fluid all-page-container-padding main-offers-sectiontwo">
//           <div className="col-12 main-offers-inside">
//             <div className="row">
//               <table class="table">
//                 <TableHeadAddReview
//                   restaurantCode="fdsv sdsdv s"
//                   restaurantId="fdsv sdsdv s"
//                   restaurantName="fdsv sdsdv s"
//                   restaurantAddress="fdsv sdsdv s"
//                   restaurantCity="fdsv sdsdv s"
//                   restaurantMember="fdsv sdsdv s"
//                   restaurantActio="fdsv sdsdv s"
//                 />
//                 <tbody>
//                   <tr>
//                     <td scope="row">ssss</td>
//                     <td>sss</td>
//                     <td>ssjdjdjdjdjfd</td>
//                   </tr>
//                   <tr>
//                     <td scope="row">egjerkg</td>
//                     <td>regegire</td>
//                     <td>epergrep</td>
//                   </tr>
//                 </tbody>
//               </table>
//             </div>
//           </div>
//         </div>
//       </React.Fragment>
//     );
//   }
// }

// export default AddReviewsComponent;
