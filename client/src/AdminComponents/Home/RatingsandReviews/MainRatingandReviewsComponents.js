import React, { Component } from "react";
import AdminNavbar from "../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import InputComponent from "../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";
// import AddReviewsComponent from "./InsideReviewspage/AddReviewsComponent";
// import ApprovedReviews from "./InsideReviewspage/ApprovedReviews";
// import PendingReviews from "./InsideReviewspage/PendingReviews";
import { TableHeadAddReview } from "./InsideReviewspage/tableData/TableHeadAddReview";
import { TableBodyAddReview } from "./InsideReviewspage/tableData/TableBodyAddReview";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../store/validation/is-Empty";
import { superAdmin_get_vendor_details } from "../../../store/actions/SuperAdminAllAction/SuperAdminAllGetActions";

export class MainRatingandReviewsComponents extends Component {
  constructor() {
    super();
    this.state = {
      // selectedPAge: 1,
      allRestaurantDetails: {},
      search: ""
    };
  }

  componentDidMount() {
    this.props.superAdmin_get_vendor_details();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.superAdminGetDetails);
    if (
      !isEmpty(
        nextProps.superAdminGetDetails !== nextState.allRestaurantDetails
      )
    ) {
      return {
        allRestaurantDetails: nextProps.superAdminGetDetails
      };
    }
    return null;
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  // onPageChange = value => e => {
  //   this.setState({
  //     selectedPAge: value
  //   });
  // };

  renderAddReviewsBody = () => {
    const { allRestaurantDetails } = this.state;
    let data = [];
    let Vendorfiltereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      Vendorfiltereddata = allRestaurantDetails.filter(getall => {
        if (search.test(getall.restaurant_name)) {
          return getall;
        }
      });
      // console.log(Vendorfiltereddata);
    } else {
      Vendorfiltereddata = allRestaurantDetails;
    }
    if (!isEmpty(Vendorfiltereddata)) {
      data = Vendorfiltereddata.map((data, index) => {
        return (
          <React.Fragment key={index}>
            <TableBodyAddReview
              eyeonClick={this.allReviewHandler}
              code={++index}
              restaurantid="C137"
              img={data.restaurant_logo}
              name={data.restaurant_name}
              address={data.restaurant_address}
              city="Pune"
              member="25/08/19"
            />
          </React.Fragment>
        );
      });
    }
    return data;
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid p-0">
          <AdminNavbar />
          <div className="container-fluid all-page-container-padding table-container">
            <div className="col-12 vendor-search mt-3 px-0 d-flex justify-content-between align-items-center">
              <div className="d-flex justify-content-start align-items-center">
                <h4 className={"vendor"}>All Reviews</h4>
              </div>
              <div className="d-flex justify-content-between main-offers-sectionone">
                <InputComponent
                  img={require("../../../assets/images/search.png")}
                  labelClass={"mylabel"}
                  imageClass={"offers-image-class"}
                  name={"search"}
                  onChange={this.onSearchChange}
                  value={this.state.search}
                  place={"search"}
                  inputclass={"form-control main-offers-input"}
                />
              </div>
            </div>
          </div>
          <div className="container-fluid main-addreview">
            {/* <div className="container-fluid all-page-container-padding d-flex"> */}
            <div className="table-responsive addreview-table ">
              <table className="table table-striped">
                <thead>
                  <TableHeadAddReview
                    restaurantCode="Code"
                    restaurantId="Restaurant Id"
                    restaurantName="Name"
                    restaurantAddress="Address"
                    restaurantCity="City"
                    restaurantMember="Member Since"
                  />
                </thead>
                <tbody>{this.renderAddReviewsBody()}</tbody>
              </table>
            </div>
          </div>
          {/* </div> */}

          {/* <div className="row">
            <div className="container-fluid all-page-container-padding d-flex">
              <tr
                onClick={this.onPageChange(1)}
                className={
                  this.state.selectedPAge === 1
                    ? "menu_container-review_active"
                    : "menu_container-review"
                }
              >
                <td className="circle_label">Add Review</td>
              </tr>
              <tr
                onClick={this.onPageChange(2)}
                className={
                  this.state.selectedPAge === 2
                    ? "menu_container-review_active"
                    : "menu_container-review"
                }
              >
                <td className="circle_label">Pending Reviews</td>
              </tr>
              <tr
                onClick={this.onPageChange(3)}
                className={
                  this.state.selectedPAge === 3
                    ? "menu_container-review_active"
                    : "menu_container-review"
                }
              >
                <td className="circle_label">Approved Reviews</td>
              </tr>
            </div>
          </div>
          <div className="row">
            {this.state.selectedPAge === 1 ? <AddReviewsComponent /> : ""}
            {this.state.selectedPAge === 2 ? <PendingReviews /> : ""}
            {this.state.selectedPAge === 3 ? <ApprovedReviews /> : ""}
          </div> */}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  superAdminGetDetails: state.superAdminGetDetails.SuperAdminVendorDetails
});

export default connect(mapStateToProps, {
  superAdmin_get_vendor_details
})(withRouter(MainRatingandReviewsComponents));
