import React, { Component } from 'react';
import AdminNavbar from '../../AdminReusableComponent/NavBarComponent/AdminNavbar';

export class MainAllSmsComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className='container-fluid admin_margin-padding-main'>
          <h1>ALL SMS Page</h1>
        </div>
      </React.Fragment>
    );
  }
}

export default MainAllSmsComponent;
