import React, { Component } from "react";
import AdminNavbar from "../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import Refunds from "./InsidePages/Refunds";
import Tickets from "./InsidePages/Tickets";
import ClosedIssues from "./InsidePages/ClosedIssues";

export class MainOpenIssuesUsersComponents extends Component {
  constructor() {
    super();
    this.state = {
      selectedPage: 2
    };
  }

  onPageChange = value => e => {
    this.setState({
      selectedPage: value
    });
  };

  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className="container-fluid admin_margin-padding-main superAdmin-main-open-issues-body">
          <h2 className="open-issues-title">Open Issues</h2>
          <div className="row">
            <div className="container-fluid d-flex mb-3">
              <tr
                onClick={this.onPageChange(1)}
                className={
                  this.state.selectedPage === 1
                    ? "open-issues-Active"
                    : "open-issues-notActive"
                }
              >
                <td className="circle_label">Refunds</td>
              </tr>
              <tr
                onClick={this.onPageChange(2)}
                className={
                  this.state.selectedPage === 2
                    ? "open-issues-Active"
                    : "open-issues-notActive"
                }
              >
                <td className="circle_label">Tickets</td>
              </tr>
              <tr
                onClick={this.onPageChange(3)}
                className={
                  this.state.selectedPage === 3
                    ? "open-issues-Active"
                    : "open-issues-notActive"
                }
              >
                <td className="circle_label">Closed Issues</td>
              </tr>
            </div>
          </div>
          <div className="row">
            <div className="container-fluid">
              {this.state.selectedPage === 1 ? <Refunds /> : ""}
              {this.state.selectedPage === 2 ? <Tickets /> : ""}
              {this.state.selectedPage === 3 ? <ClosedIssues /> : ""}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MainOpenIssuesUsersComponents;
