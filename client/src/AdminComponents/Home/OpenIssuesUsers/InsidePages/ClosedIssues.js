import React, { Component } from "react";
import InputComponent from "./../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/InputComponent";
import { TicketTableHead } from "./TicketsTable/TicketTableHead";
import { TicketTableData } from "./TicketsTable/TicketTableData";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
import dateFns from "date-fns";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../../store/validation/is-Empty";
import {
  super_admin_get_closed_users_issues_tickets,
  super_admin_update_open_users_issues_tickets
} from "../../../../store/actions/SuperAdminAllAction/openUserIssues/OpenUserAllIssuesAction";

const totalRecordsInOnePage = 10;

export class ClosedIssues extends Component {
  constructor() {
    super();
    this.state = {
      search: "",
      getUserIssuesTickets: [],
      currentPagination: 1,
      ViewOpenIssuesData: []
    };
  }

  componentDidMount() {
    this.props.super_admin_get_closed_users_issues_tickets();
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (!isEmpty(nextProps.closedTickets) !== nextState.getUserIssuesTickets) {
      return {
        getUserIssuesTickets: nextProps.closedTickets
      };
    }
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  onChangePagination = page => {
    this.setState({
      currentPagination: page
    });
  };

  ViewOpenIssues = value => e => {
    this.props.history.push({
      pathname: "/viewusersclosedissues",
      state: { ViewOpenIssuesData: value }
    });
  };

  ticketBodyTable = () => {
    const { getUserIssuesTickets, currentPagination } = this.state;
    let filtereddata = [];
    let Str = "";
    let lastFive = "";
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      filtereddata = getUserIssuesTickets.filter(getall => {
        if (search.test(getall.restaurant_name)) {
          return getall;
        }
        if (search.test(getall.first_name)) {
          return getall;
        }
        if (search.test(getall.last_name)) {
          return getall;
        }
      });
    } else {
      filtereddata = getUserIssuesTickets;
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.map(
        (data, index) =>
          // var res = str.slice(0, 5);
          index >= (currentPagination - 1) * totalRecordsInOnePage &&
          index < currentPagination * totalRecordsInOnePage &&
          ((Str = data.seating_id),
          (lastFive = Str.substr(Str.length - 5)),
          (
            <React.Fragment key={index}>
              <TicketTableData
                serialNo={++index}
                seatingId={lastFive}
                restaurantImage={data.restaurant_logo}
                restaurantName={data.restaurant_name}
                issues={data.issue}
                issuesStatus={data.issue_status === "Solved" ? "Closed" : ""}
                userImage={data.profile_photo}
                userName={data.first_name + " " + data.last_name}
                issuesDate={dateFns.format(data.issue_date, "DD/MM/YYYY")}
                orderType={data.order_type}
                ViewOpenIssues={this.ViewOpenIssues(data)}
              />
            </React.Fragment>
          ))
      );
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="col-2 open-issues-search-box">
          <div className="main-offers-sectionone open-issues-search">
            <InputComponent
              // img={require("../../../../assets/images/search.png")}
              labelClass={"mylabel d-none"}
              imageClass={"offers-image-class d-none"}
              name={"search"}
              onChange={this.onSearchChange}
              value={this.state.search}
              place={"search"}
              inputclass={"form-control open-issues-input-search-box"}
            />
          </div>
        </div>
        <div className="table-responsive open-issues-table-body">
          <table className="table table-striped">
            <thead className="open-issues-table-back">
              <TicketTableHead
                no="Sr. No"
                seatingId="seating ID"
                restaurantName="Restaurant Name"
                issues="Issue"
                status="Status"
                userName="User Name"
                issuesDate="Issue Date"
                orderType="Order Type"
              />
            </thead>
            <tbody className="open-issues-table-main-body">
              {this.ticketBodyTable()}
            </tbody>
          </table>
        </div>
        <div className="p4v2-menu-pagination">
          <Pagination
            onChange={this.onChangePagination}
            current={this.state.currentPagination}
            defaultPageSize={totalRecordsInOnePage}
            total={
              this.state.getUserIssuesTickets.length
                ? this.state.getUserIssuesTickets.length
                : ""
            }
            showTitle={false}
          />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  closedTickets: state.openUserIssues.SuperAdminUserClosedIssuesTicket
});

export default connect(mapStateToProps, {
  super_admin_get_closed_users_issues_tickets,
  super_admin_update_open_users_issues_tickets
})(withRouter(ClosedIssues));
