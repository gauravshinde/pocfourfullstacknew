import React, { Component } from "react";
import Modal from "react-responsive-modal";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
// import isEmpty from "./../../../../../store/validation/is-Empty";
import { super_admin_get_open_users_issues_tickets } from "./../../../../../store/actions/SuperAdminAllAction/openUserIssues/OpenUserAllIssuesAction";

export class PhoneUserPopup extends Component {
  state = {
    open: false,
    phoneData: ""
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextProps.phonePopupData !== nextState.phoneData) {
      return {
        phoneData: nextProps.phonePopupData
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({
      open: false
    });
  };

  render() {
    const { open, phoneData } = this.state;
    console.log(phoneData);
    var mainStr = phoneData.phone_number,
      vis = mainStr.slice(-2),
      countNum = "";
    for (var i = mainStr.length - 2; i > 0; i--) {
      countNum += "*";
    }

    return (
      <React.Fragment>
        <img
          src={require("../../../../../assets/images/AdminDashboard/openissues/phone.png")}
          alt="occasion"
          className="img-fluid open-user-popup-icon-button mr-4"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="open-issues-model-popup-body">
            <div className="open-issues-model-popup-head">
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/closebutton.png")}
                className="img-fluid open-issues-popup-close"
                alt="Close Popup"
                onClick={this.onCloseModal}
              />
              {/* <h4></h4> */}
            </div>
            <div className="open-issues-modal-body">
              <div className="row">
                <div className="col-4">
                  <img
                    src={phoneData.profile_photo}
                    className="img-fluid phone-open-issues-modal"
                    alt="User Profile"
                  />
                </div>
                <div className="col-8">
                  <h2>Call Now</h2>
                  <span>
                    {phoneData.first_name + " " + phoneData.last_name}
                  </span>
                  <h2>{countNum + vis}</h2>
                </div>
              </div>
              <div className="d-flex mt-4 justify-content-between">
                <button
                  type="button"
                  className="btn button-width button-orange ml-0"
                  onClick={this.onCloseModal}
                >
                  <i
                    className="fa fa-phone"
                    style={{ transform: "rotate(135deg)" }}
                    aria-hidden="true"
                  ></i>
                </button>
                <button
                  type="button"
                  className="btn button-width button-green"
                  onClick={this.onCloseModal}
                >
                  <i
                    className="fa fa-volume-control-phone"
                    style={{ transform: "rotate(90deg)" }}
                    aria-hidden="true"
                  ></i>
                </button>
                {/* <ButtonComponent
                  type={"button"}
                  buttontext={"Okay"}
                  buttonclass={"btn button-width button-orange ml-0"}
                  onClick={this.onCloseModal}
                />
                <ButtonComponent
                  type={"button"}
                  buttontext={"Okay"}
                  buttonclass={"btn button-width button-green"}
                  onClick={this.onCloseModal}
                /> */}
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  super_admin_get_open_users_issues_tickets
)(withRouter(PhoneUserPopup));
//1-27-2020
