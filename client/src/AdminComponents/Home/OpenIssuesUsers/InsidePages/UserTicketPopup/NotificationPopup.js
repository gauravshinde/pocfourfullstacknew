import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "./../../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";

export class NotificationPopup extends Component {
  state = {
    open: false,
    occasion: "",
    notification: ""
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({
      open: false
    });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSendNotification = () => {
    const formData = {
      notification: this.state.notification
    };
    console.log(formData);
  };

  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../../assets/images/AdminDashboard/openissues/notification.png")}
          alt="occasion"
          className="img-fluid open-user-popup-icon-button mr-4"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="open-issues-model-popup-body">
            <div className="open-issues-model-popup-head">
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/closebutton.png")}
                className="img-fluid open-issues-popup-close"
                alt="Close Popup"
                onClick={this.onCloseModal}
              />
              <h4>Push Notification</h4>
            </div>
            <div className="open-issues-modal-body">
              <form>
                <p>Akhil Sharma</p>
                <textarea
                  className="form-control open-issues-modal-body-occasion"
                  name="notification"
                  onChange={this.onChange}
                  value={this.state.notification}
                  rows="3"
                  placeholder="eg. Narnia"
                ></textarea>
                <div className="button-div mt-4 text-center">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Send"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onSendNotification}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default NotificationPopup;
