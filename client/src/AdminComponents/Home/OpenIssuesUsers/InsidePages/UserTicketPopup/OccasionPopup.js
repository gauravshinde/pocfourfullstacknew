import React, { Component } from "react";
import Modal from "react-responsive-modal";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { super_admin_get_open_users_issues_tickets } from "./../../../../../store/actions/SuperAdminAllAction/openUserIssues/OpenUserAllIssuesAction";

export class OccasionPopup extends Component {
  state = {
    open: false,
    occasionPopupData: []
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    //console.log(nextProps.occasionPopupData);
    if (nextProps.occasionPopupData !== nextState.occasionPopupData) {
      return {
        occasionPopupData: nextProps.occasionPopupData
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({
      open: false
    });
  };

  render() {
    const { open, occasionPopupData } = this.state;
    console.log(occasionPopupData);
    return (
      <React.Fragment>
        <img
          src={require("../../../../../assets/images/AdminDashboard/openissues/occasion.png")}
          alt="occasion"
          className="img-fluid open-user-popup-icon-button mr-1"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton"
          }}
        >
          <div className="open-issues-model-popup-body">
            <div className="open-issues-model-popup-head">
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/closebutton.png")}
                className="img-fluid open-issues-popup-close"
                alt="Close Popup"
                onClick={this.onCloseModal}
              />
              <h4>Suggestions and Comments</h4>
            </div>
            <div className="open-issues-modal-body">
              <p>
                {occasionPopupData.first_name +
                  " " +
                  occasionPopupData.last_name}
              </p>
              <div className="open-issues-modal-body-occasion">
                <p>{occasionPopupData.special_occassion}</p>
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  super_admin_get_open_users_issues_tickets
)(withRouter(OccasionPopup));
