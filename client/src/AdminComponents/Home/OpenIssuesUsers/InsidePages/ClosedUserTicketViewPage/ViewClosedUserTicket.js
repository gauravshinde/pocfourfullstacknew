import React, { Component } from "react";
import { AdminNavbar } from "../../../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import { NavLink } from "react-router-dom";
import { ViewUserLeftTicket } from "../UserTicketViewPage/ViewUserLeftTicket";
import { ViewUserRightTicket } from "../UserTicketViewPage/ViewUserRightTicket";
import dateFns from "date-fns";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../../../store/validation/is-Empty";
import {
  super_admin_get_open_users_issues_tickets,
  super_admin_update_open_users_issues_tickets
} from "./../../../../../store/actions/SuperAdminAllAction/openUserIssues/OpenUserAllIssuesAction";

export class ViewClosedUserTicket extends Component {
  constructor() {
    super();
    this.state = {
      viewOpenIssuesData: {}
    };
  }

  componentDidMount() {
    if (!isEmpty(this.props.super_admin_get_open_users_issues_tickets)) {
      this.props.super_admin_get_open_users_issues_tickets(
        this.state.viewOpenIssuesData
      );
    }
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    if (
      nextprops.location.state.ViewOpenIssuesData !==
      nextstate.viewOpenIssuesData
    ) {
      return {
        viewOpenIssuesData: nextprops.location.state.ViewOpenIssuesData
      };
    }
    return null;
  }

  /***********************************
   * @DESC ON RESOLVED ISSUES
   **********************************/
  onOpenIssues = value => e => {
    const update = {
      id: value._id,
      issue_status: "Unsolved"
    };
    //console.log(update);
    this.props.super_admin_update_open_users_issues_tickets(update);
  };

  render() {
    const { viewOpenIssuesData } = this.state;
    console.log(viewOpenIssuesData);
    let str = viewOpenIssuesData.seating_id;
    let newStr = str.substr(str.length - 5);
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className="container-fluid admin_margin-padding-main superAdmin-main-open-issues-body">
          <h2 className="open-issues-title">Open Issues</h2>
          <NavLink
            to="/openissues-users"
            className="d-flex align-items-center text-decoration-none mb-4"
          >
            <i className="fa fa-caret-left icon-font" aria-hidden="true"></i>
            <p className="superAdmin-user-ticket-back-button">back</p>
          </NavLink>
        </div>
        <div className="container-fluid admin_margin-padding-main superAdmin-main-open-issues-body mb-5">
          <div className="row">
            <div className="col-sm-5">
              <ViewUserLeftTicket
                seatingId={newStr}
                seatingDate={dateFns.format(
                  viewOpenIssuesData.Seating_Date,
                  "DD/MM/YYYY"
                )}
                restaurantName={viewOpenIssuesData.restaurant_name}
                restaurantImage={viewOpenIssuesData.restaurant_logo}
                status={viewOpenIssuesData.issue_status}
                restaurantNumber={viewOpenIssuesData.restaurant_mobile_number}
                vendorNumber={viewOpenIssuesData.restaurant_mobile_number}
                restaurantAddress={viewOpenIssuesData.restaurant_address}
                restaurantCity={viewOpenIssuesData.restaurant_city}
                userIssuesDate={dateFns.format(
                  viewOpenIssuesData.issue_date,
                  "DD/MM/YYYY"
                )}
                userName={
                  viewOpenIssuesData.first_name +
                  " " +
                  viewOpenIssuesData.last_name
                }
                userImages={viewOpenIssuesData.profile_photo}
                userIssuesImages={viewOpenIssuesData.issue_image}
                userOrderType={viewOpenIssuesData.order_type}
                userIssues={viewOpenIssuesData.issue}
                userComment={viewOpenIssuesData.description}
                openIssuesClick={this.onOpenIssues(viewOpenIssuesData)}
              />
            </div>
            <div className="col-sm-7">
              <ViewUserRightTicket
                userName={
                  viewOpenIssuesData.first_name +
                  " " +
                  viewOpenIssuesData.last_name
                }
                total={viewOpenIssuesData.adults + viewOpenIssuesData.kids}
                adults={viewOpenIssuesData.adults}
                kids={viewOpenIssuesData.kids}
                highChair={viewOpenIssuesData.highchair}
                handiCap={viewOpenIssuesData.handicap}
                userNumber={viewOpenIssuesData.phone_number}
                etaTime={"Not found"}
                requestedDate={dateFns.format(
                  viewOpenIssuesData.Request_Date,
                  "DD/MM/YYYY"
                )}
                requestedTime={dateFns.format(
                  viewOpenIssuesData.Request_Date,
                  "HH : MM A"
                )}
                entryPoint={"Not found"}
                seatingPreference={viewOpenIssuesData.seating_preference}
                seatedDate={dateFns.format(
                  viewOpenIssuesData.Seating_Date,
                  "DD/MM/YYYY"
                )}
                seatedTime={dateFns.format(
                  viewOpenIssuesData.Seating_Date,
                  "HH : MM A"
                )}
                phonePopupData={viewOpenIssuesData}
                messagePopupData={viewOpenIssuesData}
                notificationPopupData={viewOpenIssuesData}
                occasionPopupData={viewOpenIssuesData}
                editLogsPopupData={viewOpenIssuesData}
                displayData={
                  viewOpenIssuesData.order_type === "waitlist" ? true : false
                }
                wait_time={viewOpenIssuesData.wait_time}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  viewTicket: state.openUserIssues.SuperAdminUserOpenIssuesTicket
});

export default connect(mapStateToProps, {
  super_admin_get_open_users_issues_tickets,
  super_admin_update_open_users_issues_tickets
})(withRouter(ViewClosedUserTicket));
