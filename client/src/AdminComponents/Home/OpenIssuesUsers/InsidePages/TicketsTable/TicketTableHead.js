import React from "react";

export const TicketTableHead = ({
  no,
  seatingId,
  restaurantName,
  issues,
  status,
  userName,
  issuesDate,
  orderType
}) => {
  return (
    <React.Fragment>
      <tr>
        <th>{no}</th>
        <th>{seatingId}</th>
        <th>{restaurantName}</th>
        <th>{issues}</th>
        <th>{status}</th>
        <th>{userName}</th>
        <th>{issuesDate}</th>
        <th>{orderType}</th>
      </tr>
    </React.Fragment>
  );
};
