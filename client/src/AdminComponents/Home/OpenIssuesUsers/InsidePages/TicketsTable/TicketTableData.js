import React from "react";
// import { NavLink } from "react-router-dom";

export const TicketTableData = ({
  serialNo,
  seatingId,
  restaurantImage,
  restaurantName,
  issues,
  issuesStatus,
  userImage,
  userName,
  issuesDate,
  orderType,
  ViewOpenIssues
}) => {
  return (
    <React.Fragment>
      <tr className="open-issues-table-body-data">
        <td>
          <p>{serialNo}</p>
        </td>
        <td>
          <p>{seatingId}</p>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <img
              src={restaurantImage}
              className="img-fluid profile-image"
              alt="Restaurant Pic"
            />
            <h2>{restaurantName}</h2>
          </div>
        </td>
        <td>
          <p>{issues}</p>
        </td>
        <td>
          <p className="danger-open-issues">{issuesStatus}</p>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <img
              src={userImage}
              className="img-fluid profile-image"
              alt="User Pic"
            />
            <h2>{userName}</h2>
          </div>
        </td>
        <td>{issuesDate}</td>
        <td>
          <div className="d-flex align-items-center">
            {orderType === "reservation" ? (
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/reservation.png")}
                className="img-fluid order-type-image"
                alt="User Pic"
              />
            ) : "" || orderType === "walkin" ? (
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/walking.png")}
                className="img-fluid order-type-image"
                alt="User Pic"
              />
            ) : "" || orderType === "waitlist" ? (
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/clock.png")}
                className="img-fluid order-type-image"
                alt="User Pic"
              />
            ) : (
              ""
            )}
            {/* <NavLink to="/viewusersopenissues"> */}
            <img
              src={require("../../../../../assets/images/AdminDashboard/home/eye.png")}
              className="img-fluid view-image"
              alt="View Button Pic"
              onClick={ViewOpenIssues}
            />
            {/* </NavLink> */}
          </div>
        </td>
      </tr>
    </React.Fragment>
  );
};
