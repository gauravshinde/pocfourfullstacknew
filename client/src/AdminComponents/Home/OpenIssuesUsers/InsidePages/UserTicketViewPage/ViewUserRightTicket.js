import React from "react";
import PhoneUserPopup from "../UserTicketPopup/PhoneUserPopup";
import EditLogsPopup from "../UserTicketPopup/EditLogsPopup";
import OccasionPopup from "../UserTicketPopup/OccasionPopup";
import NotificationPopup from "../UserTicketPopup/NotificationPopup";
import MessagePopup from "../UserTicketPopup/MessagePopup";
import ViewOpenIssuesTimer from "./ViewOpenIssuesTimer";

export const ViewUserRightTicket = ({
  userName,
  total,
  adults,
  kids,
  highChair,
  handiCap,
  userNumber,
  etaTime,
  requestedDate,
  requestedTime,
  entryPoint,
  seatingPreference,
  seatedDate,
  seatedTime,
  phonePopupData,
  messagePopupData,
  notificationPopupData,
  occasionPopupData,
  editLogsPopupData,
  displayData,
  wait_time
}) => {
  return (
    <React.Fragment>
      <div className="superAdmin-open-issues-user-ticket">
        <div className="row">
          <div className="col-sm-7">
            <h2>{userName}</h2>
          </div>
          <div className="col-sm-3">
            <h2>Details</h2>
          </div>
          <div className="col-sm-2 p-lg-0 p-md-0"></div>
        </div>
        <hr />
        <div className="row">
          {/* Left Side Section */}

          <div className="col-sm-7">
            <div className="w-100">
              <PhoneUserPopup phonePopupData={phonePopupData} />
              <MessagePopup messagePopupData={messagePopupData} />
              <NotificationPopup
                notificationPopupData={notificationPopupData}
              />
            </div>
            <div className="row mt-3">
              <div className="col">
                <h2>Total</h2>
                <p>{total}</p>
              </div>
              <div className="col">
                <h2>Adults</h2>
                <p>{adults}</p>
              </div>
              <div className="col">
                <h2>Kids</h2>
                <p>{kids}</p>
              </div>
              <div className="col">
                <h2>Highchair</h2>
                <p>{highChair}</p>
              </div>
              <div className="col">
                <h2>Handicap</h2>
                <p>{handiCap}</p>
              </div>
            </div>
            {displayData === true ? (
              <div className="row mt-3">
                <div className="col-5">
                  <h2>Wait time</h2>
                  <div className="d-flex align-items-end mt-1">
                    <input
                      type="number"
                      name="edit"
                      className="form-control open-issues-input-wait-time"
                    />
                    <img
                      src={require("../../../../../assets/images/AdminDashboard/openissues/penimg.svg")}
                      className="img-fluid edit-button-icon"
                      alt="Edit Button"
                    />
                    <span>&nbsp;mins.</span>
                  </div>
                </div>
                <div className="col-5">
                  <h2>Timer</h2>
                  <span style={{ color: "#FF0000" }}>
                    <ViewOpenIssuesTimer value={wait_time * 60 * 1000} />
                  </span>
                </div>
                <div className="col-12">
                  <h2 className="mt-2">Status</h2>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>

          {/* Right Side Section */}

          <div className="col-sm-3">
            <h3>Phone Number</h3>
            <p>{userNumber}</p>
            <h3 className="mt-3">ETA</h3>
            <p>{etaTime}</p>
            <h3 className="mt-3">Requested Time</h3>
            <p>{requestedDate}</p>
            <h3 className="mt-3">Requested Date</h3>
            <p>{requestedTime}</p>
            <div className="d-flex align-items-end mt-3">
              <OccasionPopup occasionPopupData={occasionPopupData} />
              <h3>Ocassion</h3>
            </div>
          </div>
          <div className="col-sm-2 p-lg-0 p-md-0">
            <h3>Point Of Entry</h3>
            <p>{entryPoint}</p>
            <h3 className="mt-3">Seating Preferences</h3>
            <p>{seatingPreference}</p>
            <h3 className="mt-3">Seated Time</h3>
            <p>{seatedDate}</p>
            <h3 className="mt-3">Seated Date</h3>
            <p>{seatedTime}</p>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-sm-12">
            <div className="d-flex align-items-end mb-3">
              <EditLogsPopup editLogsPopupData={editLogsPopupData} />
              <h2>Edit Logs</h2>
            </div>
          </div>
          <div className="edit-logs-section-height">
            <table className="table table-responsive borderless">
              <thead>
                <tr>
                  <th>
                    <h3>Date</h3>
                  </th>
                  <th>
                    <h3>Operation</h3>
                  </th>
                  <th>
                    <h3>by</h3>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>13th August, 2019</p>
                    <p className="mt-1">06:30 PM</p>
                  </td>
                  <td>
                    <p>Seating status changed from not seated to seated</p>
                  </td>
                  <td>
                    <p>you</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
