import React from "react";
import isEmpty from "./../../../../../store/validation/is-Empty";

export const ViewUserLeftTicket = ({
  seatingId,
  seatingDate,
  restaurantName,
  restaurantImage,
  status,
  restaurantNumber,
  vendorNumber,
  restaurantAddress,
  restaurantCity,
  userIssuesDate,
  userName,
  userImages,
  userIssuesImages,
  userOrderType,
  userIssues,
  userComment,
  openIssuesClick,
  closedIssuesClick,
  buttonDisplay
}) => {
  return (
    <React.Fragment>
      <div className="superAdmin-open-issues-user-ticket">
        <div className="row">
          <div className="col-sm-5">
            <h2>
              Seating ID <span>{seatingId}</span>
            </h2>
          </div>
          <div className="col-sm-4">
            <h2>Seating Date</h2>
          </div>
          <div className="col-sm-3">
            <h2>
              <span>{seatingDate}</span>
            </h2>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-sm-5">
            <h2>Restaurant Name</h2>
            <div className="col-12 user-ticket-image-name">
              <img
                src={restaurantImage}
                className="img-fluid restaurant-image"
                alt="Restaurant Name"
              />
              <span>{restaurantName}</span>
            </div>
            <h2>Status</h2>
            <span>{status}</span>
          </div>
          <div className="col-sm-4">
            <h3>Restaurant Number</h3>
            <p>{restaurantNumber}</p>
            <h3 className="mt-3">Address</h3>
            <p>{restaurantAddress}</p>
          </div>
          <div className="col-sm-3">
            <h3>Vendor Number</h3>
            <p>{vendorNumber}</p>
            <h3 className="mt-3">City</h3>
            <p>{restaurantCity}</p>
            <h3 className="mt-3">Issue Date</h3>
            <p>{userIssuesDate}</p>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-sm-5">
            <h2>User Name</h2>
            <div className="col-12 user-ticket-image-name">
              <img
                src={userImages}
                className="img-fluid restaurant-image"
                alt="User pic"
              />
              <span>{userName}</span>
            </div>
          </div>
          <div className="col-sm-4">
            <h3>Images</h3>
            {!isEmpty(userIssuesImages) &&
              userIssuesImages.map((singleitem, index) => {
                return (
                  <React.Fragment key={index}>
                    <img
                      src={singleitem}
                      className="img-fluid restaurant-issues-image img-thumbnail"
                      alt="Issues Pic"
                    />
                  </React.Fragment>
                );
              })}
            {/* <img
              src={userIssuesImages}
              className="img-fluid restaurant-issues-image img-thumbnail"
              alt="Issues Photo"
            /> */}
          </div>
          <div className="col-sm-3">
            <h3>Order Type</h3>
            {userOrderType === "reservation" ? (
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/reservation.png")}
                className="img-fluid order-type-user-issues"
                alt="Order-type"
              />
            ) : "" || userOrderType === "walkin" ? (
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/walking.png")}
                className="img-fluid order-type-user-issues"
                alt="Order-type"
              />
            ) : "" || userOrderType === "waitlist" ? (
              <img
                src={require("../../../../../assets/images/AdminDashboard/openissues/clock.png")}
                className="img-fluid order-type-user-issues"
                alt="Order-type"
              />
            ) : (
              ""
            )}
            <p className="text-capitalize mt-2">{userOrderType}</p>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h2>Issue</h2>
            <p className="mt-2">{userIssues}</p>
            <div className="summary-user-issues">
              <span>
                User Comment:
                <p>{userComment}</p>
              </span>
            </div>
            <div className="col-12 d-flex justify-content-center">
              {buttonDisplay === true ? (
                <button
                  type="button"
                  className="btn open-user-issues-green"
                  onClick={closedIssuesClick}
                >
                  Resolved Issues
                </button>
              ) : (
                <button
                  type="button"
                  className="btn open-user-issues"
                  onClick={openIssuesClick}
                >
                  Reopen Issues
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
