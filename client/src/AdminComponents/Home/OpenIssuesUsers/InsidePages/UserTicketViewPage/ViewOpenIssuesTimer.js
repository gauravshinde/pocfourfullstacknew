import React from "react";
import Timer from "react-compound-timer";

const ViewOpenIssuesTimer = ({ value }) => {
  return (
    <Timer initialTime={value} direction="backward">
      {() => (
        <React.Fragment>
          {/* <Timer.Days /> days */}
          <Timer.Hours /> h :
          <Timer.Minutes /> m :
          <Timer.Seconds /> s{/* <Timer.Milliseconds /> milliseconds */}
        </React.Fragment>
      )}
    </Timer>
  );
};
export default ViewOpenIssuesTimer;
