import React, { Component } from 'react';
import AdminNavbar from '../AdminReusableComponent/NavBarComponent/AdminNavbar';
import HomepageComponent from '../AdminReusableComponent/HomePageReusable/HomepageComponent';

export class AdminHomeComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className='container-fluid admin_margin-padding-main mt-4'>
          <div className='row'>
            <HomepageComponent
              linkbutton='/allusers'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Users'
            />
            <HomepageComponent
              linkbutton='/allvendors'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Vendors'
            />
            <HomepageComponent
              linkbutton='/allsubadmins'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Sub Admins'
            />
            <HomepageComponent
              linkbutton='/allfinances'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='All Finances'
            />
            <HomepageComponent
              linkbutton='/openissues-users'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Open Issues - Users'
            />
          </div>
          <div className='row'>
            <HomepageComponent
              linkbutton='/openissues-vendors'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Open issues - Vendors'
            />
            <HomepageComponent
              linkbutton='/ratingsandreviews'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Ratings and Reviews'
            />
            <HomepageComponent
              linkbutton='/nominations'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Nominations'
            />
            <HomepageComponent
              linkbutton='/suggestions'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Suggestions'
            />
            <HomepageComponent
              linkbutton='/listoficons'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='List of Icons'
            />
          </div>
          <div className='row'>
            <HomepageComponent
              linkbutton='/admin-reports'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Reports'
            />
            <HomepageComponent
              linkbutton='/marketingoffers'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Marketing(offers)'
            />
            <HomepageComponent
              linkbutton='/pushnotifications'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Push Notifications'
            />
            <HomepageComponent
              linkbutton='/emails'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='Emails'
            />
            <HomepageComponent
              linkbutton='/sms'
              rounddiv='round-circle-div'
              linkclass='col home-main-button-tab'
              icon='glass icon'
              buttontext='SMS'
            />
          </div>
          <div className='row'>
            <HomepageComponent
              linkbutton='/pendingvendors'
              rounddiv='round-circle-div'
              linkclass='home-main-button-tab single-button-css'
              icon='glass icon'
              buttontext='Pending Vendors'
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminHomeComponent;
