import React, { Component } from 'react';
import AdminNavbar from '../../AdminReusableComponent/NavBarComponent/AdminNavbar';

export class MainMarketingOffersComponents extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className='container-fluid admin_margin-padding-main'>
          <h1>ALL Marketing Offers Page</h1>
        </div>
      </React.Fragment>
    );
  }
}

export default MainMarketingOffersComponents;
