import React from "react";

export const NominationBodyTable = ({
  code,
  restaurantname,
  address,
  count,
  img
}) => {
  return (
    <React.Fragment>
      <tr className="nomination-body-table-class">
        <td className="align-middle ">{code}</td>
        <td className="align-middle ">
          <img src={img} alt="restaurant-img" className="img-fluid pr-3" />
          {restaurantname}
        </td>
        <td className="align-middle ">{address}</td>
        <td className="align-middle text-center">{count}</td>
      </tr>
    </React.Fragment>
  );
};
