import React from "react";

export const NominationHeadTable = () => {
  return (
    <React.Fragment>
      <tr className="nomination-head-table-class">
        <th style={{ width: "10%" }}>Sr.No</th>
        <th style={{ width: "32%" }}>Restaurant Name</th>
        <th style={{ width: "18%" }}>Address</th>
        <th style={{ width: "40%", textAlign: "center" }}>Count</th>
      </tr>
    </React.Fragment>
  );
};
