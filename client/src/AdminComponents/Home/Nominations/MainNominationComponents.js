import React, { Component } from "react";
// import { Link } from "react-router-dom";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";

import AdminNavbar from "../../AdminReusableComponent/NavBarComponent/AdminNavbar";
import SearchboxName from "../../AdminReusableComponent/SearchBoxandPageName/SearchboxName";
import { NominationHeadTable } from "../Nominations/insidePages/NominationHeadTable";
import { NominationBodyTable } from "../Nominations/insidePages/NominationBodyTable";

import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "./../../../store/validation/is-Empty";
import { superAdmin_get_nomination } from "./../../../store/actions/SuperAdminAllAction/nomination/NominationAllActions";

const totalRecordsInOnePage = 10;

export class MainNominationComponents extends Component {
  constructor() {
    super();
    this.state = {
      nominationDetails: [],
      search: "",
      currentPagination: 1
    };
  }

  componentDidMount() {
    if (this.props.superAdmin_get_nomination) {
      this.props.superAdmin_get_nomination();
    }
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    console.log(nextProps.allNomination);
    if (!isEmpty(nextProps.allNomination) !== nextState.allNomination) {
      return {
        nominationDetails: nextProps.allNomination
      };
    }
  }

  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };

  onChangePagination = page => {
    // console.log(page);
    this.setState({
      currentPagination: page
    });
  };

  renderNominationTableBody = () => {
    const { nominationDetails, currentPagination } = this.state;
    console.log(currentPagination);
    let filtereddata = [];
    if (this.state.search) {
      let search = new RegExp(this.state.search, "i");
      filtereddata = nominationDetails.filter(getall => {
        if (search.test(getall.restaurant_name)) {
          return getall;
        }
        if (search.test(getall.address)) {
          return getall;
        }
        if (search.test(getall.count)) {
          return getall;
        }
      });
      console.log(filtereddata);
    } else {
      filtereddata = nominationDetails;
    }
    if (!isEmpty(filtereddata)) {
      return filtereddata.map(
        (data, index) =>
          index >= (currentPagination - 1) * totalRecordsInOnePage &&
          index < currentPagination * totalRecordsInOnePage && (
            <React.Fragment key={index}>
              <NominationBodyTable
                code={++index}
                restaurantname={data.restaurant_name}
                img={data.restaurant_logo}
                address={data.address}
                count={data.count}
              />
            </React.Fragment>
          )
      );
    } else {
      return (
        <h3 className="isempty-text dashboard-panel__counts-title mb-5 mt-5 pl-5">
          Loading...
        </h3>
      );
    }
  };

  renderNominationTable = () => {
    return (
      <React.Fragment>
        <div className="table-responsive nomination-table">
          <table className="table table-striped">
            <thead>
              <NominationHeadTable />
            </thead>
            <tbody>{this.renderNominationTableBody()}</tbody>
          </table>
        </div>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className="container-fluid nomination-main">
          <SearchboxName
            headingtext="Nominations"
            name="search"
            type="text"
            onChange={this.onSearchChange}
            value={this.state.search}
          />
          {this.renderNominationTable()}
        </div>
        <div className="p4v2-menu-pagination">
          <Pagination
            onChange={this.onChangePagination}
            current={this.state.currentPagination}
            defaultPageSize={totalRecordsInOnePage}
            total={this.state.nominationDetails.length}
            showTitle={false}
          />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToFromProps = state => ({
  auth: state.auth,
  allNomination: state.nominationReducers.getNomination
});

export default connect(mapStateToFromProps, {
  superAdmin_get_nomination
})(withRouter(MainNominationComponents));
