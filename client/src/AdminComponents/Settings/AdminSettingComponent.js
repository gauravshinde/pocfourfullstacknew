import React, { Component } from 'react';
import AdminNavbar from '../AdminReusableComponent/NavBarComponent/AdminNavbar';

export class AdminSettingComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className='container-fluid admin_margin-padding-main mt-4'>
          <h1>Settings page</h1>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminSettingComponent;
