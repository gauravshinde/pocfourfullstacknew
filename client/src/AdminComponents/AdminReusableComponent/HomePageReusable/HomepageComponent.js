import React from 'react';
import { Link } from 'react-router-dom';

const HomepageComponent = ({
  linkbutton,
  linkclass,
  rounddiv,
  headingclass,
  buttontext
}) => {
  return (
    <React.Fragment>
      <Link to={linkbutton} className={linkclass}>
        <div className='d-flex justify-center align-items-center'>
          <div className={rounddiv}></div>
          <h4 className={headingclass}>{buttontext}</h4>
        </div>
      </Link>
    </React.Fragment>
  );
};

export default HomepageComponent;
