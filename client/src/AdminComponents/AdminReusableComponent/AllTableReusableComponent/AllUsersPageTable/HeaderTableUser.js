import React from "react";

const HeaderTableUser = ({
  no,
  name,
  email,
  phone,
  city,
  member,
  transactions,
  action
}) => {
  return (
    <React.Fragment>
      <tr className="table-headclass">
        <th>{no}</th>
        <th style={{ width: "157px" }}>{name}</th>
        <th>{email}</th>
        <th>{phone}</th>
        <th>{city}</th>
        <th>{member}</th>
        <th>{transactions}</th>
        <th>{action}</th>
        <th></th>
        <th></th>
      </tr>
    </React.Fragment>
  );
};

export default HeaderTableUser;
