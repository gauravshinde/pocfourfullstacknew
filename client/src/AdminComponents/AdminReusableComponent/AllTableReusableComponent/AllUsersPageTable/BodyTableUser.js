import React from "react";
import AdminMessagePopup from "../../AdminAllPopup/AdminAllUserPopUps/AdminMessagePopup";
import AdminNotificationPopup from "../../AdminAllPopup/AdminAllUserPopUps/AdminNotificationPopup";
import AdminPhoneUserPopup from "../../AdminAllPopup/AdminAllUserPopUps/AdminPhoneUserPopup";
import AdminDeletePopup from "../../AdminAllPopup/AdminAllUserPopUps/AdminDeletePopup";
import EditUserPopup from "../../AdminAllPopup/AdminAllUserPopUps/AllEditPopUps/EditUserPopup";

const BodyTableUser = ({
  no,
  firstname,
  lastname,
  email,
  phone,
  city,
  member,
  transactions,
  eyeonclick,
  ID,
  lockUser,
  messageAllData,
}) => {
  return (
    <React.Fragment>
      <tr className="table-rowclass">
        <td>{no}</td>
        <td>
          {firstname} {lastname}
          <div className="mt-2">
            <AdminPhoneUserPopup
              firstName={firstname}
              lastName={lastname}
              phone={phone}
            />
            <AdminMessagePopup
              firstName={firstname}
              lastName={lastname}
              phonenumber={phone}
            />
            <AdminNotificationPopup firstName={firstname} />
          </div>
        </td>
        <td>{email}</td>
        <td>
          {/* +{code}  */}
          {phone}
        </td>
        <td>{city}</td>
        <td>{member}</td>
        <td>{transactions}</td>
        <td>
          <AdminDeletePopup firstName={firstname} ID={ID} />
          <img
            src={require("../../../../assets/images/AdminDashboard/home/flag.png")}
            alt="flag img"
            className="user-logo-img"
          />
          {ID.user_verified === true ? (
            <img
              src={require("../../../../assets/images/AdminDashboard/home/unlock.png")}
              alt="lock img"
              className="user-logo-img bg-white"
              onClick={lockUser}
            />
          ) : (
            <img
              src={require("../../../../assets/images/AdminDashboard/home/lock.png")}
              alt="lock img"
              className="user-logo-img"
              onClick={lockUser}
            />
          )}
        </td>
        <td></td>
        <td>
          <img
            src={require("../../../../assets/images/AdminDashboard/home/eye.png")}
            alt="eye img"
            className="user-logo-img"
            onClick={eyeonclick}
          />
          <EditUserPopup editData={messageAllData} />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default BodyTableUser;
