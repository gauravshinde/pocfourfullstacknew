import React from "react";

const HeaderTableVendor = ({
  srno,
  name,
  contactDetails,
  address,
  restaurantDetails,
  actions
}) => {
  return (
    <React.Fragment>
      <tr className="table-head-section">
        <th style={{ width: "3%" }}>{srno}</th>
        <th style={{ width: "12%" }}>{name}</th>
        <th>{contactDetails}</th>
        <th style={{ width: "25%" }}>{address}</th>
        <th>{restaurantDetails}</th>
        <th>{actions}</th>
        <th style={{ color: "transparent" }}>View Images</th>
      </tr>
    </React.Fragment>
  );
};

export default HeaderTableVendor;
