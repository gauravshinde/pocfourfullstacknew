import React from "react";
// import { Link } from 'react-router-dom';

const BodyTableVendor = ({
  srno,
  vendorname,
  phoneonclick,
  messageonclick,
  notificationonclick,
  deleteonclick,
  flagonclick,
  lockonclick,
  eyeonclick,
  editonclick,
  namepara,
  emailpara,
  phonepara,
  addresspara,
  citypara,
  typepara,
  Transactionspara,
  MemberSincepara,
  slidingButton
}) => {
  return (
    <React.Fragment>
      <tr className="vendor-table-body">
        <td>
          <p>{srno}</p>
        </td>
        <td>
          <p>{vendorname}</p>
          <div className="d-flex">
            <img
              src={require("../../../../assets/images/AdminDashboard/home/phone.png")}
              alt="icon phone"
              className="img-fluid "
              onClick={phoneonclick}
            />
            <img
              src={require("../../../../assets/images/AdminDashboard/home/message.png")}
              alt="icon message"
              className="img-fluid "
              onClick={messageonclick}
            />
            <img
              src={require("../../../../assets/images/AdminDashboard/home/notification.png")}
              alt="icon notification"
              className="img-fluid "
              onClick={notificationonclick}
            />
          </div>
        </td>
        <td>
          <h5>Name</h5>
          <p>{namepara}</p>
          <h5>Email</h5>
          <p>{emailpara}</p>
          <h5>Phone</h5>
          <p>{phonepara}</p>
        </td>
        <td>
          <p>{addresspara}</p>
          <h5>City</h5>
          <p>{citypara}</p>
        </td>
        <td>
          <h5>Restaurant Type</h5>
          <p>{typepara}</p>
          <h5>Transactions</h5>
          <p>Rs. {Transactionspara}</p>
          <h5>Member Since</h5>
          <p>{MemberSincepara}</p>
        </td>
        <td>
          <div className="d-flex">
            <img
              src={require("../../../../assets/images/AdminDashboard/home/delete.png")}
              alt="icon delete"
              className="img-fluid"
              onClick={deleteonclick}
            />
            <img
              src={require("../../../../assets/images/AdminDashboard/home/flag.png")}
              alt="icon flag"
              className="img-fluid"
              onClick={flagonclick}
            />
            <img
              src={require("../../../../assets/images/AdminDashboard/home/lock.png")}
              alt="icon lock"
              className="img-fluid"
              onClick={lockonclick}
            />
          </div>
          <div className="superAdmin-vendor-toggle">{slidingButton}</div>
        </td>
        <td>
          <div className="d-flex">
            <img
              src={require("../../../../assets/images/AdminDashboard/home/eye.png")}
              alt="icon eye"
              className="img-fluid"
              onClick={eyeonclick}
            />
            <img
              src={require("../../../../assets/images/AdminDashboard/home/edit.png")}
              alt="icon eye"
              className="img-fluid"
              onClick={editonclick}
            />
          </div>
        </td>
      </tr>
    </React.Fragment>
  );
};

export default BodyTableVendor;
