import React from "react";

const SearchboxName = ({ headingtext, onChange, value, type, name }) => {
  return (
    <React.Fragment>
      <div className="d-flex subheader-two">
        <h4>{headingtext}</h4>
        <div className="search-div">
          <img
            src={require("../../../assets/images/search.png")}
            alt="search"
            className="search-img"
          />
          <input
            name={name}
            type={type}
            onChange={onChange}
            value={value}
            placeholder="search"
            className="searchclass"
          />
        </div>
      </div>
    </React.Fragment>
  );
};

export default SearchboxName;
