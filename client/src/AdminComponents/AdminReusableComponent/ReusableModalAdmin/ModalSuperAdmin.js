import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";

export default class ModalSuperAdmin extends Component {
  render() {
    return (
      <div>
        <Modal.Dialog>
          <Modal.Header closeButton>
            <Modal.Title>Modal title</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Modal body text goes here.</p>
          </Modal.Body>
        </Modal.Dialog>
      </div>
    );
  }
}
