import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { logOutUser } from "../../../store/actions/authActions";
import Alert from "react-s-alert";

export class AdminNavbar extends Component {
  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-light admin_navigation_main admin_margin-padding-main">
          <Link className="navbar-brand" to="/admindashboard">
            <img
              src={require("../../../assets/images/logoamealio.png")}
              alt="Logo Image"
              className="img-fluid amelio_logo_imageClass"
            />
          </Link>
          <button
            className="navbar-toggler d-lg-none"
            type="button"
            data-toggle="collapse"
            data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="collapsibleNavId">
            <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
              <li className="nav-item different">
                <NavLink
                  className="nav-link"
                  to="/adminoverview"
                  activeClassName="activeRoute"
                  activeStyle={{ color: "#000" }}
                >
                  Overview
                </NavLink>
              </li>
              <li className="nav-item different">
                <NavLink
                  className="nav-link"
                  to="/adminhome"
                  activeClassName="activeRoute"
                  activeStyle={{ color: "#000" }}
                >
                  Home
                </NavLink>
              </li>
              <li className="nav-item different">
                <NavLink
                  className="nav-link"
                  to="/adminsettings"
                  activeClassName="activeRoute"
                  activeStyle={{ color: "#000" }}
                >
                  Settings
                </NavLink>
              </li>
              <li className="nav-item different">
                <NavLink
                  className="nav-link"
                  to=""
                  onClick={() =>
                    this.props.logOutUser(
                      Alert.success("<h4>Logout Successfully</h4>", {
                        position: "top-right",
                        effect: "bouncyflip",
                        beep: true,
                        html: true,
                        timeout: 5000,
                        offset: 35
                      })
                    )
                  }
                  activeStyle={{ color: "#000" }}
                  title="Logout Button"
                >
                  <img
                    src={require("../../../assets/images/logout.svg")}
                    className="img-fluid logout-button-img"
                    alt="Logout img"
                  />
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logOutUser })(
  withRouter(AdminNavbar)
);
