import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import { callUser } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";
import { connect } from "react-redux";

class AdminPhoneUserPopup extends Component {
  state = {
    open: false,
    phoneData: "",
    changeText: "true",
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({
      open: false,
      changeText: true,
    });
  };

  onCall = () => {
    this.setState({
      changeText: false,
    });
    const formdata = {
      mobileNo: this.props.phone,
    };
    this.props.callUser(formdata);
  };

  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../assets/images/AdminDashboard/home/phone.png")}
          alt="message img"
          className="user-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Call Now</div>

            <div className="container mt-3 mb-3 text-center">
              {this.state.changeText ? (
                <h4 className="pt-4 pb-0 mb-3">
                  Call {this.props.firstName + " " + this.props.lastName}
                </h4>
              ) : (
                <h4>Calling {this.props.firstName}......</h4>
              )}

              <h5 className="mt-3">{this.props.phone}</h5>
              {this.state.changeText ? (
                <div className="d-flex justify-content-between mt-3">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseModal}
                  />
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Call"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onCall}
                  />
                </div>
              ) : (
                <div className="mt-4">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Ok"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onCloseModal}
                  />
                </div>
              )}
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { callUser })(AdminPhoneUserPopup);
