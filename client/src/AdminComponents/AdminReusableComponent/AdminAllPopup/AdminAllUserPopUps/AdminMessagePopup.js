import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import InputComponent from "../../../../reusableComponents/InputComponent";
import classnames from "classnames";
import { messageUser } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";
import { connect } from "react-redux";

class AdminMessagePopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, allData: {}, message: "", changeText: true };
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false, changeText: true });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/
  onChange = (e) => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSendMessage = (e) => {
    e.preventDefault();
    this.setState({
      changeText: false,
      message: "",
      open: false,
    });
    const formdata = {
      mobileNo: this.props.phonenumber,
      message: this.state.message,
    };
    //console.log(formdata);
    this.props.messageUser(formdata);
  };

  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../assets/images/AdminDashboard/home/message.png")}
          alt="message img"
          className="user-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Message</div>

            <div className="container mt-3 mb-3">
              {this.state.changeText ? (
                <p className="pt-4 pb-0">
                  Send message to "
                  {this.props.firstName + " " + this.props.lastName}"
                </p>
              ) : (
                <p>Message sent</p>
              )}

              <form noValidate onSubmit={this.onSendMessage}>
                <div className="form-row">
                  <div className="col-12">
                    <InputComponent
                      labeltext={this.props.Name}
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="eg- Name"
                      value={this.state.message}
                      name="message"
                      placeholder="first-name"
                      onChange={this.onChange}
                      inputclass={classnames("map-inputfield input-wide")}
                    />
                  </div>
                </div>
                <div className="d-flex justify-content-between">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Cancel"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseModal}
                  />
                  <ButtonComponent
                    type="button"
                    buttontext={"Send"}
                    buttonclass={"btn button-width button-orange"}
                    // onClick={this.onSendMessage}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { messageUser })(AdminMessagePopup);
