import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import InputComponent from "../../../../reusableComponents/InputComponent";
import classnames from "classnames";
import { connect } from "react-redux";
import { sendNotification } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";

export class AdminNotificationPopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, allData: {}, message: "", changeText: true };
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false, changeText: true });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/

  onSendNotification = () => {
    this.setState({
      changeText: false,
      message: "",
    });
    const formdata = {
      token: this.props.phonenumber,
      message: this.state.message,
    };
    console.log(formdata);
    // this.props.messageUser(formdata);
  };
  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../assets/images/AdminDashboard/home/notification.png")}
          alt="message img"
          className="user-logo-img mr-0"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Push Notification</div>

            <div className="container mt-3 mb-3">
              {this.state.changeText ? (
                <p className="pt-4 pb-0">
                  Send notification to "{this.props.firstName}"
                </p>
              ) : (
                <p>Notificaton sent successfully</p>
              )}

              <form>
                <div className="form-row">
                  <div className="col-12">
                    <InputComponent
                      labeltext={this.props.Name}
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="eg- Name"
                      value={this.state.notification}
                      name="notification"
                      placeholder="first-name"
                      onChange={this.onChange}
                      inputclass={classnames("map-inputfield input-wide")}
                    />
                  </div>
                </div>
              </form>
              <div className="d-flex justify-content-between">
                <ButtonComponent
                  type={"button"}
                  buttontext={"Cancel"}
                  buttonclass={"btn button-width button-border-orange"}
                  onClick={this.onCloseModal}
                />
                <ButtonComponent
                  type={"button"}
                  buttontext={"Send"}
                  buttonclass={"btn button-width button-orange"}
                  onClick={this.onSendNotification}
                />
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default AdminNotificationPopup;
