import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import { deleteUser } from "../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";
import { connect } from "react-redux";

class AdminDeletePopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, allData: {}, message: "", changeText: true };
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false, changeText: true });
  };

  onDelete = (e) => {
    e.preventDefault();
    this.setState({
      changeText: false,
      open: false,
      message: "",
    });
    const formdata = {
      _id: this.props.ID._id,
    };
    //console.log(formdata);
    this.props.deleteUser(formdata);
  };

  render() {
    const { open } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../assets/images/AdminDashboard/home/delete.png")}
          alt="message img"
          className="user-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Message</div>

            <div className="container mt-3 mb-3">
              <form>
                <div className="form-row">
                  <div className="col-12 pt-4 pb-4 text-center">
                    {/* {this.state.changeText ? ( */}
                    <h5>
                      Are you sure want to delete "{this.props.firstName}"
                    </h5>
                    {/* ) : (
                      <h5>User Deleted SucessFully</h5>
                    )} */}
                  </div>
                </div>
                <div className="d-flex justify-content-between">
                  <ButtonComponent
                    type={"button"}
                    buttontext={"No"}
                    buttonclass={"btn button-width button-border-orange"}
                    onClick={this.onCloseModal}
                  />
                  <ButtonComponent
                    type={"button"}
                    buttontext={"Yes"}
                    buttonclass={"btn button-width button-orange"}
                    onClick={this.onDelete}
                  />
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { deleteUser })(AdminDeletePopup);
