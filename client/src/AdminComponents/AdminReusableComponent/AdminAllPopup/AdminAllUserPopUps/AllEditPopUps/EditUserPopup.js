import React, { Component } from "react";
import Modal from "react-responsive-modal";
import ButtonComponent from "../../../../../pocsrcone/Components/PocFirstSecondItration/ReusableComponents/SmallReusableComponents/ButtonComponent";
import InputComponent from "../../../../../reusableComponents/InputComponent";
import classnames from "classnames";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  superAdminEditUser,
  superAdminUserResetPassword,
} from "../../../../../store/actions/SuperAdminAllAction/AllUsers/AllUsersAction";

class EditUserPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      open1: false,
      allData: {},
      newPassword: "",
      confirmPassword: "",
      modalShowPassChange: false,
      changePass: false,
      showPassword: true,
    };
  }

  static getDerivedStateFromProps(nextprops, nextstate) {
    //console.log(nextprops.editData);
    if (nextprops.editData !== nextstate.allData) {
      return {
        allData: nextprops.editData,
      };
    }
    return null;
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  onOpenModalPass = () => {
    this.setState({ open1: true, open: false });
  };

  onCloseModalPass = () => {
    this.setState({ open1: false });
  };

  /**************************
   * @DESC OnChange Handler
   *************************/

  passwordShow = (e) => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  onChangeHandler = (e) => {
    let editData = this.state.allData;

    editData[e.target.name] = e.target.value;

    // e.preventDefault();
    this.setState({
      // [e.target.name]: e.target.value,
      allData: editData,
    });
  };

  onPasswordChangeHandler = (e) => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmitHandler = (e) => {
    let editNewData = this.state.allData;
    const formData = {
      user_id: editNewData._id,
      first_name: editNewData.first_name,
      last_name: editNewData.last_name,
      email: editNewData.email,
      mobile_number: editNewData.mobile_number,
      country_code: editNewData.country_code,
    };
    this.props.superAdminEditUser(formData);
  };

  onPassSubmitHandler = () => {
    let userData = this.state.allData;
    if (this.state.newPassword !== this.state.confirmPassword) {
      alert("Password did not match");
    } else {
      const formData = {
        user_id: userData._id,
        password: this.state.newPassword,
      };
      this.props.superAdminUserResetPassword(formData);
      this.onCloseModalPass();
    }
  };

  render() {
    const { open, open1, allData } = this.state;
    return (
      <React.Fragment>
        <img
          src={require("../../../../../assets/images/AdminDashboard/home/edit.png")}
          alt="message img"
          className="user-logo-img"
          onClick={this.onOpenModal}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Edit User Details</div>

            <div className="container mt-3 mb-3">
              <form>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext="First Name"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="Enter first name"
                      value={allData.first_name}
                      name="first_name"
                      onChange={this.onChangeHandler}
                      inputclass={classnames("map-inputfield")}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext="Last Name"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="Enter Last name"
                      value={allData.last_name}
                      name="last_name"
                      onChange={this.onChangeHandler}
                      inputclass={classnames("map-inputfield")}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col">
                    <InputComponent
                      labeltext="Email"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="email"
                      place="Enter email"
                      value={allData.email}
                      name="email"
                      onChange={this.onChangeHandler}
                      inputclass={classnames("map-inputfield")}
                    />
                  </div>
                  <div className="col">
                    <InputComponent
                      labeltext="Mobile Number"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="text"
                      place="Enter mobile number"
                      name="mobile_number"
                      value={allData.mobile_number}
                      onChange={this.onChangeHandler}
                      inputclass={classnames("map-inputfield")}
                    />
                  </div>
                </div>
              </form>
              <div className="col">
                <div className="text-right">
                  <button
                    style={{
                      backgroundColor: "transparent",
                      color: "#f85032",
                      fontWeight: "bold",
                    }}
                    onClick={this.onOpenModalPass}
                  >
                    Change Password
                  </button>
                </div>
              </div>

              <div className="d-flex justify-content-between mt-2">
                <ButtonComponent
                  type={"button"}
                  buttontext={"Cancel"}
                  buttonclass={"btn button-width button-border-orange"}
                  onClick={this.onCloseModal}
                />
                <ButtonComponent
                  type={"button"}
                  buttontext={"Save"}
                  buttonclass={"btn button-width button-orange"}
                  onClick={this.onSubmitHandler}
                />
              </div>
            </div>
          </div>
        </Modal>

        {/************************************** * CHANGE PASSWORD MODAL *****************************************/}

        <Modal
          open={open1}
          onClose={this.onCloseModalPass}
          classNames={{
            overlay: "customOverlay",
            modal: "ocassion-Modal",
            closeButton: "customCloseButton",
          }}
        >
          <div className="edit-diner-popup">
            <div className="popup-head">Change Password</div>

            <div className="container mt-3 mb-3">
              <form>
                <div className="form-row">
                  <div className="col-12">
                    <InputComponent
                      id="1"
                      labeltext="New Password"
                      inputlabelclass="input-label"
                      imgbox="d-none"
                      type="password"
                      place="Type new passwod"
                      value={this.state.newPassword}
                      name="newPassword"
                      placeholder="first-name"
                      onChange={this.onPasswordChangeHandler}
                      inputclass={classnames("map-inputfield")}
                      passwordIcon={true}
                      passwordShow={(e) => this.passwordShow(e)}
                      showPassword={this.state.showPassword}
                    />
                  </div>
                  <div className="col-12">
                    <div className="password_change_sty">
                      <InputComponent
                        labeltext="Confirm Password"
                        inputlabelclass="input-label"
                        imgbox="d-none"
                        type="password"
                        place="Type again to confirm"
                        value={this.state.confirmPassword}
                        name="confirmPassword"
                        placeholder="last-name"
                        onChange={this.onPasswordChangeHandler}
                        inputclass={classnames("map-inputfield")}
                        passwordIcon={false}
                        passwordShow={(e) => this.passwordShow(e)}
                        showPassword={this.state.showPassword}
                      />
                    </div>
                  </div>
                </div>
              </form>
              <div className="d-flex justify-content-between">
                <ButtonComponent
                  type={"button"}
                  buttontext={"Cancel"}
                  buttonclass={"btn button-width button-border-orange"}
                  onClick={this.onCloseModalPass}
                />
                <ButtonComponent
                  type={"button"}
                  buttontext={"Save"}
                  buttonclass={"btn button-width button-orange"}
                  onClick={this.onPassSubmitHandler}
                />
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {
  superAdminEditUser,
  superAdminUserResetPassword,
})(withRouter(EditUserPopup));

//10-04-2020
