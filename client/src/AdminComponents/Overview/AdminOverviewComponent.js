import React, { Component } from 'react';
import AdminNavbar from '../AdminReusableComponent/NavBarComponent/AdminNavbar';

export class AdminOverviewComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminNavbar />
        <div className='container-fluid admin_margin-padding-main mt-4'>
          <h1>Overview Page</h1>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminOverviewComponent;
