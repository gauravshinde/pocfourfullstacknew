import React, { Component } from "react";
import AdminOverviewComponent from "./Overview/AdminOverviewComponent";

export class MainAdminPanelDashboard extends Component {
  render() {
    return (
      <React.Fragment>
        <AdminOverviewComponent />
      </React.Fragment>
    );
  }
}

export default MainAdminPanelDashboard;
//new update
