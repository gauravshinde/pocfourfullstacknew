import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Alert from "react-s-alert";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import "react-s-alert/dist/s-alert-css-effects/bouncyflip.css";

import Login from "./components/AuthSection/Login/Login";
import SignUp from "./components/AuthSection/SignUp/SignUp";
import Otp from "./components/AuthSection/Otp/Otp";
import Forgot from "./components/AuthSection/Forgot/Forgot";
import mainDetailPage from "./components/ParentComment/mainDetailPage";
import MainDashboard from "./components/Dassboard/MainDashboard";
import PrivateRoute from "./store/utils/PrivateRoute";
import DetailsPrivateRoute from "./store/utils/DetailsPrivateRoute";

import MYSettins from "./components/Settings/settingsMain";

import VendorDashboard from "./pocsrcone/Components/VendorDashboard/VendorDashboard";
import VendorDiner from "./pocsrcone/Components/VendorDiner/VendorDiner";

// Provider and store
import store from "./store/store";
import { Provider } from "react-redux";
import setAuthToken from "./store/utils/setAuthToken";
import jwt_decode from "jwt-decode";
import { logOutUser, set_current_user } from "./store/actions/authActions";

/******************************************
 *@DESC ALL ADMIN PANEL DASHBOARD LINKS & IMPORTS
 *******************************************/
import MainAdminPanelDashboard from "./AdminComponents/MainAdminPanelDashboard";
import AdminOverviewComponent from "./AdminComponents/Overview/AdminOverviewComponent";
import AdminHomeComponent from "./AdminComponents/Home/AdminHomeComponent";
import AdminSettingComponent from "./AdminComponents/Settings/AdminSettingComponent";

import MainAllUsersComponent from "./AdminComponents/Home/AllUsers/MainAllUsersComponent";
import MainAllVendorsComponents from "./AdminComponents/Home/AllVendors/MainAllVendorsComponents";
import MainAllSubAdminComponents from "./AdminComponents/Home/AllSubAdmins/MainAllSubAdminComponents";
import MainAllFinancesComponent from "./AdminComponents/Home/AllFinances/MainAllFinancesComponent";
import MainOpenIssuesUsersComponents from "./AdminComponents/Home/OpenIssuesUsers/MainOpenIssuesUsersComponents";
import MainOpenIssuesVendorsComponents from "./AdminComponents/Home/OpenIssuesVendors/MainOpenIssuesVendorsComponents";
import MainRatingandReviewsComponents from "./AdminComponents/Home/RatingsandReviews/MainRatingandReviewsComponents";
import MainNominationComponents from "./AdminComponents/Home/Nominations/MainNominationComponents";
import MainAllSuggestionComponent from "./AdminComponents/Home/Suggestions/MainAllSuggestionComponent";
import MainListOfIconComponents from "./AdminComponents/Home/ListOfIcons/MainListOfIconComponents";
import MainAllReportsComponents from "./AdminComponents/Home/Reports/MainAllReportsComponents";
import MainMarketingOffersComponents from "./AdminComponents/Home/MarketingOffers/MainMarketingOffersComponents";
import MainPushNotificationComponents from "./AdminComponents/Home/PushNotifications/MainPushNotificationComponents";
import MainEmailsComponents from "./AdminComponents/Home/Emails/MainEmailsComponents";
import MainAllSmsComponent from "./AdminComponents/Home/SMS/MainAllSmsComponent";
import MainPendingVendorsComponents from "./AdminComponents/Home/PendingVendors/MainPendingVendorsComponents";
import MainViewVendorDetail from "./AdminComponents/Home/AllVendors/ViewVendor/MainViewVendorDetail";
import MainUsersViewDetails from "./AdminComponents/Home/AllUsers/ViewUsers/MainUsersViewDetails";
import SeatingPageHeader from "./pocsrcone/Components/Header/SeatingPageHeader";
import AddReservation from "./pocsrcone/Components/PocFirstSecondItration/Reservation/InsidePage/AddReservation";
import MainViewDetailsMenuAddItem from "./AdminComponents/Home/AllVendors/ViewVendor/Menu/MainViewDetailsMenuAddItem";
import MainOffersComponets from "./components/Offers/MainOffersComponets";
import MainReportsComponents from "./components/Reports/MainReportsComponents";
// import MainPocTwoOrdering from "./PocTwo/components/MainPocTwoOrdering";
import ViewDetailsInsidePage from "./pocsrcone/Components/VendorDashboard/InsideViewPage/ViewDetailsInsidePage";
import MainViewDetailsMenuEditItemForm from "./AdminComponents/Home/AllVendors/ViewVendor/Menu/MainViewDetailsMenuEditItemForm";
import ViewHistoryPage from "./pocsrcone/Components/PocFirstSecondItration/History/insidepage/ViewHistoryPage";
import PendingViewPage from "./pocsrcone/Components/PocFirstSecondItration/Pending/insidepage/PendingViewPage";
import MainEventsComponents from "./components/Events/MainEventsComponents";
import MainViewDetailsMenuAddCategory from "./AdminComponents/Home/AllVendors/ViewVendor/Menu/MainViewDetailsMenuAddCategory";
import VendorMainViewDetailsMenuAddCategory from "./components/Menu/InsidePages/MainViewDetailsMenuAddCategoryForm";
import MainVendorMenu from "./components/Menu/MainVendorMenu";
import MainTeamComponent from "./components/Team/MainTeamComponent";
import MainRatingandReviews from "./components/RatingsandReviews/MainRatingandReviews";
import PageNotFound from "./components/PageNotFound/PageNotFound";
import ViewUserTicket from "./AdminComponents/Home/OpenIssuesUsers/InsidePages/UserTicketViewPage/ViewUserTicket";
import ViewClosedUserTicket from "./AdminComponents/Home/OpenIssuesUsers/InsidePages/ClosedUserTicketViewPage/ViewClosedUserTicket";

/***************************************
 *  JWT AUTHENTICATION
 ***************************************/
if (localStorage.jwtToken) {
  // SET AUTH TOKEN
  setAuthToken(localStorage.jwtToken);
  // SET CURRENT USER
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(set_current_user(decoded));
  const currentTime = Date.now() / 1000;
  // console.log(currentTime);
  // console.log(decoded);
  if (decoded.exp <= currentTime) {
    store.dispatch(logOutUser());
    localStorage.clear();
    window.location.href = "/";
  } else {
    setAuthToken(localStorage.jwtToken);
    store.dispatch(set_current_user(decoded));
  }
}
/***************************************
 *  JWT AUTHENTICATION ENDS
 ***************************************/

export class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Route exact path="/" component={Login} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/otp" component={Otp} />
          <Route exact path="/forgotpassword" component={Forgot} />
          {/* <Redirect to="/404" /> */}

          {/* ADMIN DASHBOARD LINK */}
          <Switch>
            <PrivateRoute
              exact
              path="/admindashboard"
              component={MainAdminPanelDashboard}
            />
            <PrivateRoute
              exact
              path="/adminoverview"
              component={AdminOverviewComponent}
            />
            <PrivateRoute
              exact
              path="/adminhome"
              component={AdminHomeComponent}
            />
            <PrivateRoute
              exact
              path="/adminsettings"
              component={AdminSettingComponent}
            />

            {/******************************************
             *@DESC All Pages Off Users Start
             *******************************************/}
            <PrivateRoute
              exact
              path="/allusers"
              component={MainAllUsersComponent}
            />

            <PrivateRoute
              exact
              path="/viewuserdetails"
              component={MainUsersViewDetails}
            />

            {/******************************************
             *@DESC All Pages Off Users Start
             *******************************************/}

            {/******************************************
             *@DESC All Pages Off Vendor Start
             *******************************************/}

            <PrivateRoute
              exact
              path="/allvendors"
              component={MainAllVendorsComponents}
            />
            <PrivateRoute
              exact
              path="/viewvendordetails"
              component={MainViewVendorDetail}
            />

            <PrivateRoute
              exact
              path="/addMenuItem"
              component={MainViewDetailsMenuAddItem}
            />

            <PrivateRoute
              exact
              path="/addmenucategory"
              component={MainViewDetailsMenuAddCategory}
            />

            <PrivateRoute
              exact
              path="/Vendor-addmenucategory"
              component={VendorMainViewDetailsMenuAddCategory}
            />

            <PrivateRoute
              exact
              path="/editMenuItem"
              component={MainViewDetailsMenuEditItemForm}
            />

            {/******************************************
             *@DESC All Pages Off Vendor End
             *******************************************/}

            <PrivateRoute
              exact
              path="/allsubadmins"
              component={MainAllSubAdminComponents}
            />
            <PrivateRoute
              exact
              path="/allfinances"
              component={MainAllFinancesComponent}
            />
            <PrivateRoute
              exact
              path="/openissues-users"
              component={MainOpenIssuesUsersComponents}
            />
            <PrivateRoute
              exact
              path="/viewusersopenissues"
              component={ViewUserTicket}
            />

            <PrivateRoute
              exact
              path="/viewusersclosedissues"
              component={ViewClosedUserTicket}
            />

            <PrivateRoute
              exact
              path="/openissues-vendors"
              component={MainOpenIssuesVendorsComponents}
            />
            <PrivateRoute
              exact
              path="/ratingsandreviews"
              component={MainRatingandReviewsComponents}
            />
            <PrivateRoute
              exact
              path="/nominations"
              component={MainNominationComponents}
            />
            <PrivateRoute
              exact
              path="/suggestions"
              component={MainAllSuggestionComponent}
            />
            <PrivateRoute
              exact
              path="/listoficons"
              component={MainListOfIconComponents}
            />
            <PrivateRoute
              exact
              path="/admin-reports"
              component={MainAllReportsComponents}
            />
            <PrivateRoute
              exact
              path="/marketingoffers"
              component={MainMarketingOffersComponents}
            />
            <PrivateRoute
              exact
              path="/pushnotifications"
              component={MainPushNotificationComponents}
            />
            <PrivateRoute
              exact
              path="/emails"
              component={MainEmailsComponents}
            />
            <PrivateRoute exact path="/sms" component={MainAllSmsComponent} />
            <PrivateRoute
              exact
              path="/pendingvendors"
              component={MainPendingVendorsComponents}
            />
            {/* </Switch>

          {/* VENDOR DASHBOARD LINK *
          <Switch> */}
            <PrivateRoute exact path="/dashboard" component={MainDashboard} />
            <PrivateRoute exact path="/mysettings" component={MYSettins} />
            <DetailsPrivateRoute
              exact
              path="/vendordashboard"
              component={VendorDashboard}
            />
            <DetailsPrivateRoute
              exact
              path="/vendor-seating"
              component={SeatingPageHeader}
            />
            <DetailsPrivateRoute
              exact
              path="/all-seat-details"
              component={ViewDetailsInsidePage}
            />
            <DetailsPrivateRoute
              exact
              path="/all-history-details"
              component={ViewHistoryPage}
            />
            <DetailsPrivateRoute
              exact
              path="/all-pending-details"
              component={PendingViewPage}
            />
            <DetailsPrivateRoute
              exact
              path="/add-diner"
              component={VendorDiner}
            />
            <DetailsPrivateRoute
              exact
              path="/add-reservation"
              component={AddReservation}
            />

            {/******************************
                @DESC POC TWO ORDERING Component
            *******************************/}
            <DetailsPrivateRoute
              exact
              path="/ordering"
              component={MainDashboard} //changes
              //component={MainPocTwoOrdering}
            />

            {/******************************
                @DESC VENDOR MENU COMPONENT
            *******************************/}
            <DetailsPrivateRoute
              exact
              path="/vendor-menu"
              component={MainVendorMenu}
            />

            {/******************************
                @DESC VENDOR MENU COMPONENT
            *******************************/}
            <DetailsPrivateRoute
              exact
              path="/team"
              component={MainTeamComponent}
            />

            {/******************************
                @DESC REPORTS Component
            *******************************/}
            <DetailsPrivateRoute
              exact
              path="/reports"
              component={MainReportsComponents}
            />

            {/******************************
                @DESC VENDOR MENU COMPONENT
            *******************************/}
            <DetailsPrivateRoute
              exact
              path="/reviews&ratings"
              component={MainRatingandReviews}
            />

            {/******************************
                @DESC OFFERS COMPONENT
            *******************************/}
            <DetailsPrivateRoute
              exact
              path="/offers"
              component={MainOffersComponets}
            />

            {/******************************
                @DESC EVENTS COMPONENT
            *******************************/}
            <DetailsPrivateRoute
              exact
              path="/events"
              component={MainEventsComponents}
            />

            <DetailsPrivateRoute
              exact
              path="/maindetailpage"
              component={mainDetailPage}
            />
            <PrivateRoute exact path="*" component={PageNotFound} />
          </Switch>
        </Router>
        <span>{this.props.children}</span>
        <Alert stack={{ limit: 3 }} />
      </Provider>
    );
  }
}

export default App;

//30-01-20
